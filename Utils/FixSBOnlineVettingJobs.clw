   PROGRAM



   INCLUDE('ABERROR.INC'),ONCE
   INCLUDE('ABFILE.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ERRORS.CLW'),ONCE
   INCLUDE('KEYCODES.CLW'),ONCE
   INCLUDE('ABFUZZY.INC'),ONCE

   MAP
     MODULE('FIXSBONLINEVETTINGJOBS_BC.CLW')
DctInit     PROCEDURE                                      ! Initializes the dictionary definition module
DctKill     PROCEDURE                                      ! Kills the dictionary definition module
     END
!--- Application Global and Exported Procedure Definitions --------------------------------------------
     MODULE('FIXSBONLINEVETTINGJOBS001.CLW')
Main                   PROCEDURE   !
     END
         MODULE('WINAPI')
             BHOutputToDebugView(LONG lp_Cstr),RAW,PASCAL,NAME('OutputDebugStringA')
         END
   END

glo:ErrorText        STRING(1000)
glo:owner            STRING(20)
glo:TimeModify       TIME
glo:Notes_Global     STRING(1000)
glo:Q_Invoice        QUEUE,PRE(glo)
Invoice_Number         REAL
Account_Number         STRING(15)
                     END
glo:afe_path         STRING(255)
glo:DateModify       DATE
glo:Password         STRING(20)
glo:PassAccount      STRING(30)
glo:Preview          STRING('True')
glo:q_JobNumber      QUEUE,PRE(GLO)
Job_Number_Pointer     REAL
                     END
glo:Q_PartsOrder     QUEUE,PRE(GLO)
Q_Order_Number         REAL
Q_Part_Number          STRING(30)
Q_Description          STRING(30)
Q_Supplier             STRING(30)
Q_Quantity             LONG
Q_Purchase_Cost        REAL
Q_Sale_Cost            REAL
                     END
glo:G_Select1        GROUP,PRE(GLO)
Select1                STRING(30)
Select2                STRING(40)
Select3                STRING(40)
Select4                STRING(40)
Select5                STRING(40)
Select6                STRING(40)
Select7                STRING(40)
Select8                STRING(40)
Select9                STRING(40)
Select10               STRING(40)
Select11               STRING(40)
Select12               STRING(40)
Select13               STRING(40)
Select14               STRING(40)
Select15               STRING(40)
Select16               STRING(40)
Select17               STRING(40)
Select18               STRING(40)
Select19               STRING(40)
Select20               STRING(40)
Select21               STRING(40)
Select22               STRING(40)
Select23               STRING(40)
Select24               STRING(40)
Select25               STRING(40)
Select26               STRING(40)
Select27               STRING(40)
Select28               STRING(40)
Select29               STRING(40)
Select30               STRING(40)
Select31               STRING(40)
Select32               STRING(40)
Select33               STRING(40)
Select34               STRING(40)
Select35               STRING(40)
Select36               STRING(40)
Select37               STRING(40)
Select38               STRING(40)
Select39               STRING(40)
Select40               STRING(40)
Select41               STRING(40)
Select42               STRING(40)
Select43               STRING(40)
Select44               STRING(40)
Select45               STRING(40)
Select46               STRING(40)
Select47               STRING(40)
Select48               STRING(40)
Select49               STRING(40)
Select50               STRING(40)
                     END
glo:q_RepairType     QUEUE,PRE(GLO)
Repair_Type_Pointer    STRING(30)
                     END
glo:Q_UnitType       QUEUE,PRE(GLO)
Unit_Type_Pointer      STRING(30)
                     END
glo:Q_ChargeType     QUEUE,PRE(GLO)
Charge_Type_Pointer    STRING(30)
                     END
glo:Q_ModelNumber    QUEUE,PRE(GLO)
Model_Number_Pointer   STRING(30)
                     END
glo:Q_Accessory      QUEUE,PRE(GLO)
Accessory_Pointer      STRING(30)
                     END
glo:Q_AccessLevel    QUEUE,PRE(GLO)
Access_Level_Pointer   STRING(30)
                     END
glo:Q_AccessAreas    QUEUE,PRE(GLO)
Access_Areas_Pointer   STRING(30)
                     END
glo:Q_Notes          QUEUE,PRE(GLO)
notes_pointer          STRING(80)
                     END
glo:EnableFlag       STRING(3)
glo:TimeLogged       TIME
glo:GLO:ApplicationName STRING(8)
glo:GLO:ApplicationCreationDate LONG
glo:GLO:ApplicationCreationTime LONG
glo:GLO:ApplicationChangeDate LONG
glo:GLO:ApplicationChangeTime LONG
glo:GLO:Compiled32   BYTE
glo:GLO:AppINIFile   STRING(255)
glo:Queue            QUEUE,PRE(GLO)
Pointer                STRING(40)
                     END
glo:Queue2           QUEUE,PRE(GLO)
Pointer2               STRING(40)
                     END
glo:Queue3           QUEUE,PRE(GLO)
Pointer3               STRING(40)
                     END
glo:Queue4           QUEUE,PRE(GLO)
Pointer4               STRING(40)
                     END
glo:Queue5           QUEUE,PRE(GLO)
Pointer5               STRING(40)
                     END
glo:Queue6           QUEUE,PRE(GLO)
Pointer6               STRING(40)
                     END
glo:Queue7           QUEUE,PRE(GLO)
Pointer7               STRING(40)
                     END
glo:Queue8           QUEUE,PRE(GLO)
Pointer8               STRING(40)
                     END
glo:Queue9           QUEUE,PRE(GLO)
Pointer9               STRING(40)
                     END
glo:Queue10          QUEUE,PRE(GLO)
Pointer10              STRING(40)
                     END
glo:Queue11          QUEUE,PRE(GLO)
Pointer11              STRING(40)
                     END
glo:Queue12          QUEUE,PRE(GLO)
Pointer12              STRING(40)
                     END
glo:Queue13          QUEUE,PRE(GLO)
Pointer13              STRING(40)
                     END
glo:Queue14          QUEUE,PRE(GLO)
Pointer14              STRING(40)
                     END
glo:Queue15          QUEUE,PRE(GLO)
Pointer15              STRING(40)
                     END
glo:Queue16          QUEUE,PRE(GLO)
Pointer16              STRING(40)
                     END
glo:Queue17          QUEUE,PRE(GLO)
Pointer17              STRING(40)
                     END
glo:Queue18          QUEUE,PRE(GLO)
Pointer18              STRING(40)
                     END
glo:Queue19          QUEUE,PRE(GLO)
Pointer19              STRING(40)
                     END
glo:Queue20          QUEUE,PRE(GLO)
Pointer20              STRING(40)
                     END
glo:WebJob           BYTE(0)
glo:ARCAccountNumber STRING(30)
glo:ARCSiteLocation  STRING(30)
glo:FaultCodeGroup   GROUP,PRE(glo)
FaultCode1             STRING(30)
FaultCode2             STRING(30)
FaultCode3             STRING(30)
FaultCode4             STRING(30)
FaultCode5             STRING(30)
FaultCode6             STRING(30)
FaultCode7             STRING(30)
FaultCode8             STRING(30)
FaultCode9             STRING(30)
FaultCode10            STRING(255)
FaultCode11            STRING(255)
FaultCode12            STRING(255)
FaultCode13            STRING(30)
FaultCode14            STRING(30)
FaultCode15            STRING(30)
FaultCode16            STRING(30)
FaultCode17            STRING(30)
FaultCode18            STRING(30)
FaultCode19            STRING(30)
FaultCode20            STRING(30)
                     END
glo:IPAddress        STRING(30)
glo:HostName         STRING(60)
glo:ReportName       STRING(255)
glo:ExportReport     BYTE(0)
glo:EstimateReportName STRING(255)
glo:ReportCopies     BYTE
glo:Vetting          BYTE(0)
glo:TagFile          STRING(255)
glo:sbo_outfaultparts STRING(255)
glo:UserAccountNumber STRING(30)
glo:UserCode         STRING(3)
glo:Location         STRING(30)
glo:EDI_Reason       STRING(60)
glo:Insert_Global    STRING(3)
glo:sbo_outparts     STRING(255)
glo:StockReceiveTmp  STRING(255)
glo:SBO_GenericFile  STRING(255)
glo:RelocateStore    BYTE
glo:SBO_GenericTagFile STRING(255)
glo:SBO_DupCheck     STRING(255)
SilentRunning        BYTE(0)                               ! Set true when application is running in 'silent mode'

 MAP
BHDebugMessage		Procedure(String fMessage)
BHAddToDebugLog		Procedure(String fMessage,<String fOverrideFilename>)
 END
!region File Declaration
STDCHRGE             FILE,DRIVER('Btrieve'),NAME('STDCHRGE.DAT'),PRE(sta),CREATE,BINDABLE,THREAD !Standard Charges    
Model_Number_Charge_Key  KEY(sta:Model_Number,sta:Charge_Type,sta:Unit_Type,sta:Repair_Type),NOCASE,PRIMARY !By Charge Type      
Charge_Type_Key          KEY(sta:Model_Number,sta:Charge_Type),DUP,NOCASE !By Charge Type      
Unit_Type_Key            KEY(sta:Model_Number,sta:Unit_Type),DUP,NOCASE !By Unit Type        
Repair_Type_Key          KEY(sta:Model_Number,sta:Repair_Type),DUP,NOCASE !By Repair Type      
Cost_Key                 KEY(sta:Model_Number,sta:Cost),DUP,NOCASE !By Cost             
Charge_Type_Only_Key     KEY(sta:Charge_Type),DUP,NOCASE   !                    
Repair_Type_Only_Key     KEY(sta:Repair_Type),DUP,NOCASE   !                    
Unit_Type_Only_Key       KEY(sta:Unit_Type),DUP,NOCASE     !                    
Record                   RECORD,PRE()
Charge_Type                 STRING(30)                     !                    
Model_Number                STRING(30)                     !                    
Unit_Type                   STRING(30)                     !                    
Repair_Type                 STRING(30)                     !                    
Cost                        REAL                           !                    
dummy                       BYTE                           !                    
WarrantyClaimRate           REAL                           !Warranty Claim Rate 
HandlingFee                 REAL                           !HandlingFee         
Exchange                    REAL                           !Exchange            
RRCRate                     REAL                           !R.R.C. Rate         
                         END
                     END                       

JOBOUTFL             FILE,DRIVER('Btrieve'),OEM,NAME('JOBOUTFL.DAT'),PRE(joo),CREATE,BINDABLE,THREAD !Out Fault List      
RecordNumberKey          KEY(joo:RecordNumber),NOCASE,PRIMARY !By Record Number    
JobNumberKey             KEY(joo:JobNumber,joo:FaultCode),DUP,NOCASE !By Code             
LevelKey                 KEY(joo:JobNumber,joo:Level),DUP,NOCASE !By Level            
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
JobNumber                   LONG                           !Job Number          
FaultCode                   STRING(30)                     !Fault Code          
Description                 STRING(255)                    !                    
Level                       LONG                           !Level               
                         END
                     END                       

AUDITE               FILE,DRIVER('Btrieve'),OEM,NAME('AUDITE.DAT'),PRE(aude),CREATE,BINDABLE,THREAD !AUDIT Extension     
RecordNumberKey          KEY(aude:RecordNumber),NOCASE,PRIMARY !Record Number Key   
RefNumberKey             KEY(aude:RefNumber),DUP,NOCASE    !By Ref Number       
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Link to AUDIT Record Number
IPAddress                   STRING(30)                     !IP Address          
HostName                    STRING(60)                     !Host Name           
                         END
                     END                       

JOBSENG              FILE,DRIVER('Btrieve'),OEM,NAME('JOBSENG.DAT'),PRE(joe),CREATE,BINDABLE,THREAD !Engineers Attached To Jobs
RecordNumberKey          KEY(joe:RecordNumber),NOCASE,PRIMARY !By Record Number    
UserCodeKey              KEY(joe:JobNumber,joe:UserCode,joe:DateAllocated),DUP,NOCASE !By Date             
UserCodeOnlyKey          KEY(joe:UserCode,joe:DateAllocated),DUP,NOCASE !By Date             
AllocatedKey             KEY(joe:JobNumber,joe:AllocatedBy,joe:DateAllocated),DUP,NOCASE !By Date             
JobNumberKey             KEY(joe:JobNumber,-joe:DateAllocated,-joe:TimeAllocated),DUP,NOCASE !By Date             
UserCodeStatusKey        KEY(joe:UserCode,joe:StatusDate,joe:Status),DUP,NOCASE !By Date             
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
JobNumber                   LONG                           !Job Number          
UserCode                    STRING(3)                      !User Code           
DateAllocated               DATE                           !Date Allocated      
TimeAllocated               STRING(20)                     !Time Allocated      
AllocatedBy                 STRING(3)                      !Allocated By        
EngSkillLevel               LONG                           !Engineer Skill Level
JobSkillLevel               STRING(30)                     !Job Skill Level     
Status                      STRING(30)                     !Status              
StatusDate                  DATE                           !Status Date         
StatusTime                  TIME                           !Status Time         
                         END
                     END                       

STMASAUD             FILE,DRIVER('Btrieve'),NAME('STMASAUD.DAT'),PRE(stom),CREATE,BINDABLE,THREAD !Stock Master Audit  
AutoIncrement_Key        KEY(-stom:Audit_No),NOCASE,PRIMARY !                    
Compeleted_Key           KEY(stom:Complete),DUP,NOCASE     !                    
Sent_Key                 KEY(stom:Send),DUP,NOCASE         !                    
Record                   RECORD,PRE()
Audit_No                    LONG                           !                    
branch                      STRING(30)                     !                    
branch_id                   LONG                           !                    
Audit_Date                  LONG                           !                    
Audit_Time                  LONG                           !                    
End_Date                    LONG                           !                    
End_Time                    LONG                           !                    
Audit_User                  STRING(3)                      !                    
Complete                    STRING(1)                      !                    
Send                        STRING(1)                      !                    
CompleteType                STRING(1)                      !                    
                         END
                     END                       

AUDSTATS             FILE,DRIVER('Btrieve'),OEM,NAME('AUDSTATS.DAT'),PRE(aus),CREATE,BINDABLE,THREAD !Audit For Status Changes
RecordNumberKey          KEY(aus:RecordNumber),NOCASE,PRIMARY !By Record Number    
DateChangedKey           KEY(aus:RefNumber,aus:Type,aus:DateChanged),DUP,NOCASE !By Date Changed     
NewStatusKey             KEY(aus:RefNumber,aus:Type,aus:NewStatus),DUP,NOCASE !By New Status       
StatusTimeKey            KEY(aus:RefNumber,aus:Type,aus:DateChanged,aus:TimeChanged),DUP,NOCASE !By Time             
StatusDateKey            KEY(aus:RefNumber,aus:DateChanged,aus:TimeChanged),DUP,NOCASE !By Time             
StatusDateRecordKey      KEY(aus:RefNumber,aus:DateChanged,aus:TimeChanged,aus:RecordNumber),DUP,NOCASE !By Record Number    
StatusTypeRecordKey      KEY(aus:RefNumber,aus:Type,aus:DateChanged,aus:TimeChanged,aus:RecordNumber),DUP,NOCASE !By Record Number    
RefRecordNumberKey       KEY(aus:RefNumber,aus:RecordNumber),DUP,NOCASE !By Record Number    
RefDateRecordKey         KEY(aus:RefNumber,aus:DateChanged,aus:RecordNumber),DUP,NOCASE !By Record Number    
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Job Number          
Type                        STRING(3)                      !Type                
DateChanged                 DATE                           !Date Changed        
TimeChanged                 TIME                           !Time Changed        
OldStatus                   STRING(30)                     !Old Status          
NewStatus                   STRING(30)                     !New Status          
UserCode                    STRING(3)                      !User Code           
                         END
                     END                       

JOBTHIRD             FILE,DRIVER('Btrieve'),OEM,NAME('JOBTHIRD.DAT'),PRE(jot),CREATE,BINDABLE,THREAD !Job Third Party Despatched
RecordNumberKey          KEY(jot:RecordNumber),NOCASE,PRIMARY !By Record Number    
RefNumberKey             KEY(jot:RefNumber),DUP,NOCASE     !By Ref Number       
OutIMEIKey               KEY(jot:OutIMEI),DUP,NOCASE       !By Outgoing IMEI Number
InIMEIKEy                KEY(jot:InIMEI),DUP,NOCASE        !By Incoming IMEI Number
OutDateKey               KEY(jot:RefNumber,jot:DateOut,jot:RecordNumber),DUP,NOCASE !By Outgoing Date    
ThirdPartyKey            KEY(jot:ThirdPartyNumber),DUP,NOCASE !                    
OriginalIMEIKey          KEY(jot:OriginalIMEI),DUP,NOCASE  !By Original I.M.E.I. No
Record                   RECORD,PRE()
RecordNumber                LONG                           !                    
RefNumber                   LONG                           !Link To Job's File  
OriginalIMEI                STRING(30)                     !Original IMEI Number
OutIMEI                     STRING(30)                     !Outgoing I.M.E.I. Number
InIMEI                      STRING(30)                     !Incoming I.M.E.I. Number
DateOut                     DATE                           !                    
DateDespatched              DATE                           !Date Despatched     
DateIn                      DATE                           !                    
ThirdPartyNumber            LONG                           !Link To Third Party Entry
OriginalMSN                 STRING(30)                     !Original MSN        
OutMSN                      STRING(30)                     !Outgoing M.S.N.     
InMSN                       STRING(30)                     !Incoming M.S.N.     
                         END
                     END                       

LOCATLOG             FILE,DRIVER('Btrieve'),OEM,NAME('LOCATLOG.DAT'),PRE(lot),CREATE,BINDABLE,THREAD !Location Change Log 
RecordNumberKey          KEY(lot:RecordNumber),NOCASE,PRIMARY !By Record Number    
DateKey                  KEY(lot:RefNumber,lot:TheDate),DUP,NOCASE !By Date             
NewLocationKey           KEY(lot:RefNumber,lot:NewLocation),DUP,NOCASE !By Location         
DateNewLocationKey       KEY(lot:NewLocation,lot:TheDate,lot:RefNumber),DUP,NOCASE !By New Location     
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Ref Number          
TheDate                     DATE                           !Date                
TheTime                     TIME                           !The Time            
UserCode                    STRING(30)                     !User Code           
PreviousLocation            STRING(30)                     !Previous Location   
NewLocation                 STRING(30)                     !New Location        
                         END
                     END                       

PRODCODE             FILE,DRIVER('Btrieve'),OEM,NAME('PRODCODE.DAT'),PRE(prd),CREATE,BINDABLE,THREAD !Handset Part Numbers
RecordNumberKey          KEY(prd:RecordNumber),NOCASE,PRIMARY !By Record Number    
ProductCodeKey           KEY(prd:ProductCode),DUP,NOCASE   !By Product Code     
ModelProductKey          KEY(prd:ModelNumber,prd:ProductCode),NOCASE !By Product Code     
Record                   RECORD,PRE()
RecordNumber                LONG                           !                    
ModelNumber                 STRING(30)                     !                    
ProductCode                 STRING(30)                     !                    
HandsetReplacementValue     REAL                           !Handset Replacement Value
                         END
                     END                       

STOAUDIT             FILE,DRIVER('Btrieve'),NAME('STOAUDIT.DAT'),PRE(stoa),CREATE,BINDABLE,THREAD !                    
Internal_AutoNumber_Key  KEY(stoa:Internal_AutoNumber),NOCASE,PRIMARY !                    
Audit_Ref_No_Key         KEY(stoa:Audit_Ref_No),DUP,NOCASE !                    
Cellular_Key             KEY(stoa:Site_Location,stoa:Stock_Ref_No),DUP,NOCASE !                    
Stock_Ref_No_Key         KEY(stoa:Stock_Ref_No),DUP,NOCASE !                    
Stock_Site_Number_Key    KEY(stoa:Audit_Ref_No,stoa:Site_Location),DUP,NOCASE !                    
Lock_Down_Key            KEY(stoa:Audit_Ref_No,stoa:Stock_Ref_No),DUP,NOCASE !                    
Main_Browse_Key          KEY(stoa:Audit_Ref_No,stoa:Confirmed,stoa:Site_Location,stoa:Shelf_Location,stoa:Second_Location,stoa:Internal_AutoNumber),DUP,NOCASE !                    
Report_Key               KEY(stoa:Audit_Ref_No,stoa:Shelf_Location),DUP,NOCASE !                    
Record                   RECORD,PRE()
Internal_AutoNumber         LONG                           !                    
Site_Location               STRING(30)                     !                    
Stock_Ref_No                LONG                           !                    
Original_Level              LONG                           !                    
New_Level                   LONG                           !                    
Audit_Reason                STRING(255)                    !                    
Audit_Ref_No                LONG                           !                    
Preliminary                 STRING(1)                      !                    
Shelf_Location              STRING(30)                     !                    
Second_Location             STRING(30)                     !                    
Confirmed                   STRING(1)                      !                    
                         END
                     END                       

BOUNCER              FILE,DRIVER('Btrieve'),OEM,NAME('BOUNCER.DAT'),PRE(bou),CREATE,BINDABLE,THREAD !Bouncer Files       
Record_Number_Key        KEY(bou:Record_Number),NOCASE,PRIMARY !By Record Number    
Bouncer_Job_Number_Key   KEY(bou:Original_Ref_Number,bou:Bouncer_Job_Number),NOCASE !By Job Number       
Bouncer_Job_Only_Key     KEY(bou:Bouncer_Job_Number),DUP,NOCASE !By Job Number       
Record                   RECORD,PRE()
Record_Number               REAL                           !                    
Original_Ref_Number         REAL                           !                    
Bouncer_Job_Number          REAL                           !                    
                         END
                     END                       

WIPAMF               FILE,DRIVER('Btrieve'),OEM,NAME('WIPAMF.DAT'),PRE(wim),CREATE,BINDABLE,THREAD !                    
Audit_Number_Key         KEY(wim:Audit_Number),NOCASE,PRIMARY !                    
Secondary_Key            KEY(wim:Complete_Flag,wim:Audit_Number),DUP,NOCASE !                    
Record                   RECORD,PRE()
Audit_Number                LONG                           !                    
Date                        DATE                           !                    
Time                        LONG                           !                    
User                        STRING(3)                      !                    
Status                      STRING(30)                     !                    
Site_location               STRING(30)                     !                    
Complete_Flag               BYTE                           !                    
Ignore_IMEI                 BYTE                           !                    
Ignore_Job_Number           BYTE                           !                    
Date_Completed              DATE                           !                    
Time_Completed              TIME                           !                    
                         END
                     END                       

TEAMS                FILE,DRIVER('Btrieve'),OEM,NAME('TEAMS.DAT'),PRE(tea),CREATE,BINDABLE,THREAD !Teams               
Record_Number_Key        KEY(tea:Record_Number),NOCASE,PRIMARY !By Record Number    
Team_Key                 KEY(tea:Team),NOCASE              !By Team             
KeyTraceAccount_number   KEY(tea:TradeAccount_Number),DUP,NOCASE !                    
Record                   RECORD,PRE()
Record_Number               REAL                           !                    
Team                        STRING(30)                     !                    
TradeAccount_Number         STRING(15)                     !                    
Associated                  STRING(1)                      !                    
                         END
                     END                       

WIPAUI               FILE,DRIVER('Btrieve'),OEM,NAME('WIPAUI.DAT'),PRE(wia),CREATE,BINDABLE,THREAD !                    
Internal_No_Key          KEY(wia:Internal_No),NOCASE,PRIMARY !                    
Audit_Number_Key         KEY(wia:Audit_Number),DUP,NOCASE  !                    
Ref_Number_Key           KEY(wia:Ref_Number),DUP,NOCASE    !                    
Exch_Browse_Key          KEY(wia:Audit_Number),DUP,NOCASE  !                    
AuditIMEIKey             KEY(wia:Audit_Number,wia:New_In_Status),DUP,NOCASE !By I.M.E.I. Number  
Locate_IMEI_Key          KEY(wia:Audit_Number,wia:Site_Location,wia:Ref_Number),DUP,NOCASE !                    
Main_Browse_Key          KEY(wia:Audit_Number,wia:Confirmed,wia:Site_Location,wia:Status,wia:Ref_Number),DUP,NOCASE !                    
AuditRefNumberKey        KEY(wia:Audit_Number,wia:Ref_Number),DUP,NOCASE !By Ref Number       
AuditStatusRefNoKey      KEY(wia:Audit_Number,wia:Status,wia:Ref_Number),DUP,NOCASE !By Ref Number       
Record                   RECORD,PRE()
Internal_No                 LONG                           !                    
Site_Location               STRING(30)                     !                    
Status                      STRING(30)                     !                    
Audit_Number                LONG                           !                    
Ref_Number                  LONG                           !Unit Number         
New_In_Status               BYTE                           !                    
Confirmed                   BYTE                           !Confirmed           
IsExchange                  BYTE                           !                    
                         END
                     END                       

MODELCOL             FILE,DRIVER('Btrieve'),OEM,NAME('MODELCOL.DAT'),PRE(moc),CREATE,BINDABLE,THREAD !Model Colours       
Record_Number_Key        KEY(moc:Record_Number),NOCASE,PRIMARY !                    
Colour_Key               KEY(moc:Model_Number,moc:Colour),NOCASE !By Colour           
Record                   RECORD,PRE()
Record_Number               REAL                           !                    
Model_Number                STRING(30)                     !                    
Colour                      STRING(30)                     !                    
                         END
                     END                       

CONTHIST             FILE,DRIVER('Btrieve'),NAME('CONTHIST.DAT'),PRE(cht),CREATE,BINDABLE,THREAD !Contact History     
Ref_Number_Key           KEY(cht:Ref_Number,-cht:Date,-cht:Time,cht:Action),DUP,NOCASE !By Date             
Action_Key               KEY(cht:Ref_Number,cht:Action,-cht:Date),DUP,NOCASE !By Action           
User_Key                 KEY(cht:Ref_Number,cht:User,-cht:Date),DUP,NOCASE !By User             
Record_Number_Key        KEY(cht:Record_Number),NOCASE,PRIMARY !                    
KeyRefSticky             KEY(cht:Ref_Number,cht:SN_StickyNote),DUP,NOCASE !                    
Record                   RECORD,PRE()
Record_Number               LONG                           !Record Number       
Ref_Number                  LONG                           !Ref Number          
Date                        DATE                           !                    
Time                        TIME                           !                    
User                        STRING(3)                      !                    
Action                      STRING(80)                     !                    
Notes                       STRING(2000)                   !                    
SystemHistory               BYTE                           !System History      
SN_StickyNote               STRING(1)                      !                    
SN_Completed                STRING(1)                      !                    
SN_EngAlloc                 STRING(1)                      !                    
SN_EngUpdate                STRING(1)                      !                    
SN_CustService              STRING(1)                      !                    
SN_WaybillConf              STRING(1)                      !                    
SN_Despatch                 STRING(1)                      !                    
                         END
                     END                       

JOBSTAMP             FILE,DRIVER('Btrieve'),OEM,NAME('JOBSTAMP.DAT'),PRE(jos),CREATE,BINDABLE,THREAD !Date/Time Stamp when "JOB" changes
RecordNumberKey          KEY(jos:RecordNumber),NOCASE,PRIMARY !By Record Number    
JOBSRefNumberKey         KEY(jos:JOBSRefNumber),DUP,NOCASE !By RefNumber        
DateTimeKey              KEY(jos:DateStamp,jos:TimeStamp),DUP,NOCASE !By Date Time        
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
JOBSRefNumber               LONG                           !Link To JOBS File   
DateStamp                   DATE                           !Date Stamp          
TimeStamp                   TIME                           !Time Stamp          
                         END
                     END                       

ACCESDEF             FILE,DRIVER('Btrieve'),NAME('ACCESDEF.DAT'),PRE(acd),CREATE,BINDABLE,THREAD !Accessories - Default List
Accessory_Key            KEY(acd:Accessory),NOCASE,PRIMARY !By Accessory        
Record                   RECORD,PRE()
Accessory                   STRING(30)                     !                    
                         END
                     END                       

JOBACCNO             FILE,DRIVER('Btrieve'),OEM,NAME('JOBACCNO.DAT'),PRE(joa),CREATE,BINDABLE,THREAD !Job Accessory Numbers
RecordNumberKey          KEY(joa:RecordNumber),NOCASE,PRIMARY !By Record Number    
AccessoryNumberKey       KEY(joa:RefNumber,joa:AccessoryNumber),DUP,NOCASE !By Accessory Number 
AccessoryNoOnlyKey       KEY(joa:AccessoryNumber),DUP,NOCASE !By Accessory Number 
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Ref Number          
AccessoryNumber             STRING(30)                     !Accessory Number    
                         END
                     END                       

JOBRPNOT             FILE,DRIVER('Btrieve'),OEM,NAME('JOBRPNOT.DAT'),PRE(jrn),CREATE,BINDABLE,THREAD !Job Repair Notes    
RecordNumberKey          KEY(jrn:RecordNumber),NOCASE,PRIMARY !By Record Number    
TheDateKey               KEY(jrn:RefNumber,jrn:TheDate,jrn:TheTime),DUP,NOCASE !By Date             
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Link to JOBS Ref_Number
TheDate                     DATE                           !Date                
TheTime                     TIME                           !Time                
User                        STRING(3)                      !User                
Notes                       STRING(255)                    !Notes               
                         END
                     END                       

JOBSOBF              FILE,DRIVER('Btrieve'),OEM,NAME('JOBSOBF.DAT'),PRE(jof),CREATE,BINDABLE,THREAD !Processing OBF Jobs 
RecordNumberKey          KEY(jof:RecordNumber),NOCASE,PRIMARY !By Record Number    
RefNumberKey             KEY(jof:RefNumber),DUP,NOCASE     !By Job Number       
StatusRefNumberKey       KEY(jof:Status,jof:RefNumber),DUP,NOCASE !By Job Number       
StatusIMEINumberKey      KEY(jof:Status,jof:IMEINumber),DUP,NOCASE !By I.M.E.I. Number  
HeadAccountCompletedKey  KEY(jof:HeadAccountNumber,jof:DateCompleted,jof:RefNumber),DUP,NOCASE !By Date Completed   
HeadAccountProcessedKey  KEY(jof:HeadAccountNumber,jof:DateProcessed,jof:RefNumber),DUP,NOCASE !By Date Processed   
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Like to JOBS RefNumber
IMEINumber                  STRING(30)                     !I.M.E.I. Number     
Status                      BYTE                           !Processed Status    
Replacement                 BYTE                           !Replacement         
StoreReferenceNumber        STRING(30)                     !Stock Reference Number
RNumber                     STRING(30)                     !R Number            
RejectionReason             STRING(255)                    !Rejection Reason    
ReplacementIMEI             STRING(30)                     !Replacement I.M.E.I. Number
LAccountNumber              STRING(30)                     !L/Account Number    
UserCode                    STRING(3)                      !User Code           
HeadAccountNumber           STRING(30)                     !Head Account Number 
DateCompleted               DATE                           !Date Completed      
TimeCompleted               TIME                           !Time Completed      
DateProcessed               DATE                           !Date Process        
TimeProcessed               TIME                           !Time Processed      
                         END
                     END                       

JOBSINV              FILE,DRIVER('Btrieve'),OEM,NAME('JOBSINV.DAT'),PRE(jov),CREATE,BINDABLE,THREAD !Jobs Invoice/Credit Notes
RecordNumberKey          KEY(jov:RecordNumber),NOCASE,PRIMARY !By Record Number    
RefNumberKey             KEY(jov:RefNumber,jov:RecordNumber),DUP,NOCASE !By Record Number    
DateCreatedKey           KEY(jov:RefNumber,jov:DateCreated,jov:TimeCreated),DUP,NOCASE !By Date Created     
DateCreatedOnlyKey       KEY(jov:DateCreated,jov:TimeCreated,jov:RefNumber),DUP,NOCASE !By Job Number       
TypeRecordKey            KEY(jov:RefNumber,jov:Type,jov:RecordNumber),DUP,NOCASE !By Record Number    
TypeSuffixKey            KEY(jov:RefNumber,jov:Type,jov:Suffix),DUP,NOCASE !By Suffix           
InvoiceNumberKey         KEY(jov:InvoiceNumber,jov:RecordNumber),DUP,NOCASE !By Invoice Number   
InvoiceTypeKey           KEY(jov:InvoiceNumber,jov:Type,jov:RecordNumber),DUP,NOCASE !By Invoice Number   
BookingDateTypeKey       KEY(jov:BookingAccount,jov:Type,jov:DateCreated,jov:TimeCreated,jov:RefNumber),DUP,NOCASE !By Type             
TypeDateKey              KEY(jov:Type,jov:DateCreated,jov:TimeCreated,jov:RefNumber),DUP,NOCASE !By Type             
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Link To JOBS RefNumber
InvoiceNumber               LONG                           !Link to INVOICE Invoice_Number
Type                        STRING(1)                      !Type (Invoice / Credit)
Suffix                      STRING(1)                      !Suffix              
DateCreated                 DATE                           !Date Created        
TimeCreated                 TIME                           !Time Created        
UserCode                    STRING(3)                      !User Code           
OriginalTotalCost           REAL                           !Original Total Cost 
NewTotalCost                REAL                           !New Total Cost      
CreditAmount                REAL                           !Credit Amount       
BookingAccount              STRING(30)                     !Booking Account Number
NewInvoiceNumber            STRING(30)                     !New Invoice Number  
ChargeType                  STRING(30)                     !Charge Type         
RepairType                  STRING(30)                     !Repair Type         
HandlingFee                 REAL                           !Handling Fee        
ExchangeRate                REAL                           !Exchange Rate       
ARCCharge                   REAL                           !ARC Charge          
RRCLostLoanCost             REAL                           !Lost Loan Cost      
RRCPartsCost                REAL                           !RRC Parts Cost      
RRCPartsSelling             REAL                           !RRC Parts Selling   
RRCLabour                   REAL                           !RRC Labour Cost     
ARCMarkUp                   REAL                           !ARC Markup          
RRCVAT                      REAL                           !RRCVAT              
Paid                        REAL                           !Paid                
Outstanding                 REAL                           !Outstanding         
Refund                      REAL                           !Refund              
                         END
                     END                       

JOBSCONS             FILE,DRIVER('Btrieve'),OEM,NAME('JOBSCONS.DAT'),PRE(joc),CREATE,BINDABLE,THREAD !Jobs Consignment History
RecordNumberKey          KEY(joc:RecordNumber),NOCASE,PRIMARY !By Record Number    
DateKey                  KEY(joc:RefNumber,joc:TheDate,joc:TheTime),DUP,NOCASE !By Date             
ConsignmentNumberKey     KEY(joc:ConsignmentNumber,joc:RefNumber),DUP,NOCASE !By Consignment Number
DespatchFromDateKey      KEY(joc:DespatchFrom,joc:TheDate,joc:RefNumber),DUP,NOCASE !By Date             
DateOnlyKey              KEY(joc:TheDate,joc:RefNumber),DUP,NOCASE !By Date             
CourierKey               KEY(joc:Courier,joc:RefNumber),DUP,NOCASE !By Courier          
DespatchFromCourierKey   KEY(joc:DespatchFrom,joc:Courier,joc:RefNumber),DUP,NOCASE !By Job Number       
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Ref Number (Link To Jobs)
TheDate                     DATE                           !Date                
TheTime                     TIME                           !Time                
UserCode                    STRING(3)                      !User Code           
DespatchFrom                STRING(30)                     !Despatch From       
DespatchTo                  STRING(30)                     !Despatch To         
Courier                     STRING(30)                     !Courier             
ConsignmentNumber           STRING(30)                     !Consignment Number  
DespatchType                STRING(3)                      !Despatch Type       
                         END
                     END                       

JOBSWARR             FILE,DRIVER('Btrieve'),OEM,NAME('JOBSWARR.DAT'),PRE(jow),CREATE,BINDABLE,THREAD !Warranty Jobs List  
RecordNumberKey          KEY(jow:RecordNumber),NOCASE,PRIMARY !By Record Number    
RefNumberKey             KEY(jow:RefNumber),DUP,NOCASE     !By Ref Number       
StatusManFirstKey        KEY(jow:Status,jow:Manufacturer,jow:FirstSecondYear,jow:RefNumber),DUP,NOCASE !By Job Number       
StatusManKey             KEY(jow:Status,jow:Manufacturer,jow:RefNumber),DUP,NOCASE !By Job Number       
ClaimStatusManKey        KEY(jow:Status,jow:Manufacturer,jow:ClaimSubmitted,jow:RefNumber),DUP,NOCASE !By Job Number       
ClaimStatusManFirstKey   KEY(jow:Status,jow:Manufacturer,jow:FirstSecondYear,jow:ClaimSubmitted,jow:RefNumber),DUP,NOCASE !By Job Number       
RRCStatusKey             KEY(jow:BranchID,jow:RepairedAt,jow:RRCStatus,jow:RefNumber),DUP,NOCASE !By Job Number       
RRCStatusManKey          KEY(jow:BranchID,jow:RepairedAt,jow:RRCStatus,jow:Manufacturer,jow:RefNumber),DUP,NOCASE !By Job Number       
RRCReconciledManKey      KEY(jow:BranchID,jow:RepairedAt,jow:RRCStatus,jow:Manufacturer,jow:RRCDateReconciled,jow:RefNumber),DUP,NOCASE !By Job Number       
RRCReconciledKey         KEY(jow:BranchID,jow:RepairedAt,jow:RRCStatus,jow:RRCDateReconciled,jow:RefNumber),DUP,NOCASE !By Job Number       
RepairedRRCStatusKey     KEY(jow:RepairedAt,jow:RRCStatus,jow:RefNumber),DUP,NOCASE !By RRC Status       
RepairedRRCStatusManKey  KEY(jow:RepairedAt,jow:RRCStatus,jow:Manufacturer,jow:RefNumber),DUP,NOCASE !By Job Number       
RepairedRRCReconciledKey KEY(jow:RepairedAt,jow:RRCStatus,jow:DateReconciled,jow:RefNumber),DUP,NOCASE !By Job Number       
RepairedRRCReconManKey   KEY(jow:RepairedAt,jow:RRCStatus,jow:Manufacturer,jow:RRCDateReconciled,jow:RefNumber),DUP,NOCASE !By Job Number       
SubmittedRepairedBranchKey KEY(jow:BranchID,jow:RepairedAt,jow:ClaimSubmitted,jow:RefNumber),DUP,NOCASE !By Branch           
SubmittedBranchKey       KEY(jow:BranchID,jow:ClaimSubmitted,jow:RefNumber),DUP,NOCASE !By Job Number       
ClaimSubmittedKey        KEY(jow:ClaimSubmitted,jow:RefNumber),DUP,NOCASE !By Job Number       
RepairedRRCAccManKey     KEY(jow:BranchID,jow:RepairedAt,jow:RRCStatus,jow:Manufacturer,jow:DateAccepted,jow:RefNumber),DUP,NOCASE !By Job Number       
AcceptedBranchKey        KEY(jow:BranchID,jow:DateAccepted,jow:RefNumber),DUP,NOCASE !By Job Number       
DateAcceptedKey          KEY(jow:DateAccepted,jow:RefNumber),DUP,NOCASE !By Job Number       
RejectedBranchKey        KEY(jow:BranchID,jow:DateRejected,jow:RefNumber),DUP,NOCASE !By Job Number       
RejectedKey              KEY(jow:DateRejected,jow:RefNumber),DUP,NOCASE !By Job Number       
FinalRejectionBranchKey  KEY(jow:BranchID,jow:DateFinalRejection,jow:RefNumber),DUP,NOCASE !By JobNumber        
FinalRejectionKey        KEY(jow:DateFinalRejection,jow:RefNumber),DUP,NOCASE !By Job Number       
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Job Number          
BranchID                    STRING(2)                      !BranchID            
RepairedAt                  STRING(3)                      !Repaired At         
Manufacturer                STRING(30)                     !Manufacturer        
FirstSecondYear             BYTE                           !First or Second Year
Status                      STRING(3)                      !Status              
Submitted                   LONG                           !Number Of Times Submitted
FromApproved                BYTE                           !Resubmitted From Approved Batch
ClaimSubmitted              DATE                           !Claim Submitted     
DateAccepted                DATE                           !Date Accepted       
DateReconciled              DATE                           !Date Reconciled     
RRCDateReconciled           DATE                           !Date Reconciled     
RRCStatus                   STRING(3)                      !RRC Status          
DateRejected                DATE                           !                    
DateFinalRejection          DATE                           !                    
Orig_Sub_Date               DATE                           !                    
                         END
                     END                       

JOBSE2               FILE,DRIVER('Btrieve'),OEM,NAME('JOBSE2.DAT'),PRE(jobe2),CREATE,BINDABLE,THREAD !Jobs Extension (2)  
RecordNumberKey          KEY(jobe2:RecordNumber),NOCASE,PRIMARY !By Record Number    
RefNumberKey             KEY(jobe2:RefNumber),DUP,NOCASE   !By Ref Number       
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Link To JOBS Ref_Number
IDNumber                    STRING(13)                     !I.D. Number         
InPendingDate               DATE                           !In Pending Date     
Contract                    BYTE                           !Contract            
Prepaid                     BYTE                           !Prepaid             
WarrantyRefNo               STRING(30)                     !Warranty Ref No     
SIDBookingName              STRING(60)                     !SID Booking Name    
XAntenna                    BYTE                           !Antenna             
XLens                       BYTE                           !Lens                
XFCover                     BYTE                           !F/Cover             
XBCover                     BYTE                           !B/Cover             
XKeypad                     BYTE                           !Keypad              
XBattery                    BYTE                           !Battery             
XCharger                    BYTE                           !Charger             
XLCD                        BYTE                           !LCD                 
XSimReader                  BYTE                           !Sim Reader          
XSystemConnector            BYTE                           !System Connector    
XNone                       BYTE                           !None                
XNotes                      STRING(255)                    !Notes               
ExchangeTerms               BYTE                           !Exchange Terms Explained To Customer
WaybillNoFromPUP            LONG                           !Waybill Number From PUP
WaybillNoToPUP              LONG                           !Waybill Number To PUP
SMSNotification             BYTE                           !SMS Notification    
EmailNotification           BYTE                           !Email Notification  
SMSAlertNumber              STRING(30)                     !SMS Alert Mobile Number
EmailAlertAddress           STRING(255)                    !Email Alert Address 
DateReceivedAtPUP           DATE                           !Date Received At PUP From RRC
TimeReceivedAtPUP           TIME                           !Time Received At PUP From RRC
DateDespatchFromPUP         DATE                           !Date Despatch From PUP
TimeDespatchFromPUP         TIME                           !Time Despatch From PUP
LoanIDNumber                STRING(13)                     !Loan ID Number      
CourierWaybillNumber        STRING(30)                     !Courier Waybill Number
HubCustomer                 STRING(30)                     !Hub                 
HubCollection               STRING(30)                     !Hub                 
HubDelivery                 STRING(30)                     !Hub                 
WLabourPaid                 REAL                           !Warranty Labour Paid
WPartsPaid                  REAL                           !Warranty Parts Paid 
WOtherCosts                 REAL                           !Warranty Other Costs
WSubTotal                   REAL                           !Warranty Sub Total  
WVAT                        REAL                           !Warranty VAT        
WTotal                      REAL                           !Warranty Total      
WarrantyReason              STRING(255)                    !Reason For Not Reconciling
WInvoiceRefNo               STRING(255)                    !Warranty Invoice Ref Number
SMSDate                     DATE                           !                    
JobDiscountAmnt             REAL                           !                    
InvDiscountAmnt             REAL                           !                    
POPConfirmed                BYTE                           !                    
ThirdPartyHandlingFee       REAL                           !                    
InvThirdPartyHandlingFee    REAL                           !                    
                         END
                     END                       

TRDSPEC              FILE,DRIVER('Btrieve'),NAME('TRDSPEC.DAT'),PRE(tsp),CREATE,BINDABLE,THREAD !Third Party Special Instructions
Short_Description_Key    KEY(tsp:Short_Description),NOCASE,PRIMARY !By Short Description
Record                   RECORD,PRE()
Short_Description           STRING(30)                     !                    
Long_Description            STRING(255)                    !                    
                         END
                     END                       

ORDPEND              FILE,DRIVER('Btrieve'),NAME('ORDPEND.DAT'),PRE(ope),CREATE,BINDABLE,THREAD !Orders - Pending Parts
Ref_Number_Key           KEY(ope:Ref_Number),NOCASE,PRIMARY !By Ref Number       
Supplier_Key             KEY(ope:Supplier,ope:Part_Number),DUP,NOCASE !By Supplier         
DescriptionKey           KEY(ope:Supplier,ope:Description),DUP,NOCASE !By Description      
Supplier_Name_Key        KEY(ope:Supplier),DUP,NOCASE      !By Supplier         
Part_Ref_Number_Key      KEY(ope:Part_Ref_Number),DUP,NOCASE !By Stock Ref Number 
Awaiting_Supplier_Key    KEY(ope:Awaiting_Stock,ope:Supplier,ope:Part_Number),DUP,NOCASE !By Part Number      
PartRecordNumberKey      KEY(ope:PartRecordNumber),DUP,NOCASE !By Record Number    
ReqPartNumber            KEY(ope:StockReqNumber,ope:Part_Number),DUP,NOCASE !By Part Number      
ReqDescriptionKey        KEY(ope:StockReqNumber,ope:Description),DUP,NOCASE !By Description      
Record                   RECORD,PRE()
Ref_Number                  LONG                           !                    
Part_Ref_Number             LONG                           !                    
Job_Number                  LONG                           !                    
Part_Type                   STRING(3)                      !                    
Supplier                    STRING(30)                     !                    
Part_Number                 STRING(30)                     !                    
Description                 STRING(30)                     !                    
Quantity                    LONG                           !                    
Account_Number              STRING(15)                     !                    
Awaiting_Stock              STRING(3)                      !                    
Reason                      STRING(255)                    !Reason              
PartRecordNumber            LONG                           !Part Record Number  
StockReqNumber              LONG                           !Stock Requisition Number
                         END
                     END                       

STOHIST              FILE,DRIVER('Btrieve'),NAME('STOHIST.DAT'),PRE(shi),CREATE,BINDABLE,THREAD !Stock History       
Ref_Number_Key           KEY(shi:Ref_Number,shi:Date),DUP,NOCASE !By Ref_Number       
record_number_key        KEY(shi:Record_Number),NOCASE,PRIMARY !                    
Transaction_Type_Key     KEY(shi:Ref_Number,shi:Transaction_Type,shi:Date),DUP,NOCASE !By Transaction      
DateKey                  KEY(shi:Date),DUP,NOCASE          !By Date             
JobNumberKey             KEY(shi:Ref_Number,shi:Transaction_Type,shi:Job_Number),DUP,NOCASE !By Job Number       
Record                   RECORD,PRE()
Record_Number               LONG                           !Record Number       
Ref_Number                  LONG                           !                    
User                        STRING(3)                      !                    
Transaction_Type            STRING(3)                      !                    
Despatch_Note_Number        STRING(30)                     !                    
Job_Number                  LONG                           !                    
Sales_Number                LONG                           !                    
Quantity                    REAL                           !                    
Date                        DATE                           !                    
Purchase_Cost               REAL                           !                    
Sale_Cost                   REAL                           !                    
Retail_Cost                 REAL                           !                    
Notes                       STRING(255)                    !                    
Information                 STRING(255)                    !                    
StockOnHand                 LONG                           !Stock On Hand       
                         END
                     END                       

REPTYDEF             FILE,DRIVER('Btrieve'),NAME('REPTYDEF.DAT'),PRE(rtd),CREATE,BINDABLE,THREAD !Manufacturer Repair Types
RecordNumberKey          KEY(rtd:RecordNumber),NOCASE,PRIMARY !By Record Number    
ManRepairTypeKey         KEY(rtd:Manufacturer,rtd:Repair_Type),NOCASE !By Repair Type      
Repair_Type_Key          KEY(rtd:Repair_Type),DUP,NOCASE   !By Repair Type      
Chargeable_Key           KEY(rtd:Chargeable,rtd:Repair_Type),DUP,NOCASE !                    
Warranty_Key             KEY(rtd:Warranty,rtd:Repair_Type),DUP,NOCASE !                    
ChaManRepairTypeKey      KEY(rtd:Manufacturer,rtd:Chargeable,rtd:Repair_Type),DUP,NOCASE !By Repair Type      
WarManRepairTypeKey      KEY(rtd:Manufacturer,rtd:Warranty,rtd:Repair_Type),DUP,NOCASE !By Repair Type      
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
Repair_Type                 STRING(30)                     !Repair Type         
Chargeable                  STRING(3)                      !Chargeable Repair Type
Warranty                    STRING(3)                      !Warranty Repair Type
WarrantyCode                STRING(5)                      !Warranty Code       
CompFaultCoding             BYTE                           !Force Fault Codes   
ExcludeFromEDI              BYTE                           !Exclude From EDI Process
ExcludeFromInvoicing        BYTE                           !Exclude From Invoicing
BER                         BYTE                           !Type Of Repair Type 
ExcludeFromBouncer          BYTE                           !Exclude From Bouncer Table
PromptForExchange           BYTE                           !Prompt For Exchange 
JobWeighting                LONG                           !Job Weighting       
SkillLevel                  LONG                           !Skill Level         
Manufacturer                STRING(30)                     !Manufacturer        
RepairLevel                 LONG                           !Repair Index        
ForceAdjustment             BYTE                           !Force Adjustment If No Parts Used
ScrapExchange               BYTE                           !                    
ExcludeHandlingFee          BYTE                           !Exclude RRC Handling Fee
NotAvailable                BYTE                           !Not Available       
NoParts                     STRING(1)                      !                    
SMSSendType                 STRING(1)                      !                    
                         END
                     END                       

PRIORITY             FILE,DRIVER('Btrieve'),NAME('PRIORITY.DAT'),PRE(pri),CREATE,BINDABLE,THREAD !Priority            
Priority_Type_Key        KEY(pri:Priority_Type),NOCASE,PRIMARY !By Priority Type    
Record                   RECORD,PRE()
Priority_Type               STRING(30)                     !                    
Time                        REAL                           !                    
Book_Before                 TIME                           !                    
                         END
                     END                       

JOBSE                FILE,DRIVER('Btrieve'),OEM,NAME('JOBSE.DAT'),PRE(jobe),CREATE,BINDABLE,THREAD !Jobs Extension      
RecordNumberKey          KEY(jobe:RecordNumber),NOCASE,PRIMARY !By Record Number    
RefNumberKey             KEY(jobe:RefNumber),DUP,NOCASE    !By Ref Number       
WarrStatusDateKey        KEY(jobe:WarrantyStatusDate),DUP,NOCASE !By Warranty Status Date
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Refence To Job Number
JobMark                     BYTE                           !Job Mark            
TraFaultCode1               STRING(30)                     !Fault Code 1        
TraFaultCode2               STRING(30)                     !Fault Code 2        
TraFaultCode3               STRING(30)                     !Fault Code 3        
TraFaultCode4               STRING(30)                     !Fault Code 4        
TraFaultCode5               STRING(30)                     !Fault Code 5        
TraFaultCode6               STRING(30)                     !Fault Code 6        
TraFaultCode7               STRING(30)                     !Fault Code 7        
TraFaultCode8               STRING(30)                     !Fault Code 8        
TraFaultCode9               STRING(30)                     !Fault Code 9        
TraFaultCode10              STRING(30)                     !Fault Code 10       
TraFaultCode11              STRING(30)                     !Fault Code 11       
TraFaultCode12              STRING(30)                     !Fault Code 12       
SIMNumber                   STRING(30)                     !SIM Number          
JobReceived                 BYTE                           !Job Received        
SkillLevel                  LONG                           !Skill Level         
UPSFlagCode                 STRING(1)                      !UPS Flag Code       
FailedDelivery              BYTE                           !Failed Delivery     
CConfirmSecondEntry         STRING(3)                      !Chargeable Confirm Second Entry
WConfirmSecondEntry         STRING(3)                      !Warranty Confirm Second Entry
EndUserEmailAddress         STRING(255)                    !Email Address       
HubRepair                   BYTE                           !Hub Repair          
Network                     STRING(30)                     !Network             
POPConfirmed                BYTE                           !POPConfirmed        
HubRepairDate               DATE                           !HubRepairDate       
HubRepairTime               TIME                           !Hub Repair Time     
ClaimValue                  REAL                           !ClaimValue          
HandlingFee                 REAL                           !Handling Fee        
ExchangeRate                REAL                           !Exchange Rate       
InvoiceClaimValue           REAL                           !Invoice Claim Value 
InvoiceHandlingFee          REAL                           !Invoice Handling Fee
InvoiceExchangeRate         REAL                           !Invoice Exchange Rate
BoxESN                      STRING(20)                     !                    
ValidPOP                    STRING(3)                      !                    
ReturnDate                  DATE                           !                    
TalkTime                    REAL                           !                    
OriginalPackaging           BYTE                           !                    
OriginalBattery             BYTE                           !                    
OriginalCharger             BYTE                           !                    
OriginalAntenna             BYTE                           !                    
OriginalManuals             BYTE                           !                    
PhysicalDamage              BYTE                           !                    
OriginalDealer              CSTRING(255)                   !                    
BranchOfReturn              STRING(30)                     !                    
COverwriteRepairType        BYTE                           !Overwrite Repair Type
WOverwriteRepairType        BYTE                           !Overwrite Repair Type
LabourAdjustment            REAL                           !Labour Adjustment   
PartsAdjustment             REAL                           !Parts Adjustment    
SubTotalAdjustment          REAL                           !Sub Total Adjustment
IgnoreClaimCosts            BYTE                           !Ignore Claim Costs  
RRCCLabourCost              REAL                           !Labour Cost         
RRCCPartsCost               REAL                           !Parts Cost          
RRCCPartsSale               REAL                           !Parts Sale Cost     
RRCCSubTotal                REAL                           !RRC Sub Total       
InvRRCCLabourCost           REAL                           !Invoice Labour Cost 
InvRRCCPartsCost            REAL                           !Invoice Parts Cost  
InvRRCCPartsSale            REAL                           !Invoice Parts Sale Cost
InvRRCCSubTotal             REAL                           !Invoice Sub Total   
RRCWLabourCost              REAL                           !Labour Cost         
RRCWPartsCost               REAL                           !Parts Cost          
RRCWPartsSale               REAL                           !Parts Sale Cost     
RRCWSubTotal                REAL                           !RRC Sub Total       
InvRRCWLabourCost           REAL                           !Invoice Labour Cost 
InvRRCWPartsCost            REAL                           !Invoice Parts Cost  
InvRRCWPartsSale            REAL                           !Invoice Parts Sale Cost
InvRRCWSubTotal             REAL                           !Invoice Sub Total   
ARC3rdPartyCost             REAL                           !3rd Party Cost      
InvARC3rdPartCost           REAL                           !Invoice 3rd Party Cost
WebJob                      BYTE                           !Is this job a web job?
RRCELabourCost              REAL                           !RRC Estimate Labour Cost
RRCEPartsCost               REAL                           !RRC Estimate Parts Cost
RRCESubTotal                REAL                           !RRC Estimate Sub Total
IgnoreRRCChaCosts           REAL                           !Ignore Default Costs
IgnoreRRCWarCosts           REAL                           !Ignore Default Costs
IgnoreRRCEstCosts           REAL                           !Ignore Estimate Costs
OBFvalidated                BYTE                           !Out of Box Failure - Validation
OBFvalidateDate             DATE                           !OBF Validation Date 
OBFvalidateTime             TIME                           !OBF Validation Time 
DespatchType                STRING(3)                      !Despatch Type       
Despatched                  STRING(3)                      !Despatched          
WarrantyClaimStatus         STRING(30)                     !Warranty Claim Status
WarrantyStatusDate          DATE                           !Warranty Claim Status Date
InSecurityPackNo            STRING(30)                     !Incoming Security Pack Number
JobSecurityPackNo           STRING(30)                     !Outgoing Security Pack Number - Job
ExcSecurityPackNo           STRING(30)                     !Outgoing Security Pack Number - Exchange
LoaSecurityPackNo           STRING(30)                     !Outgoing Security Pack Number - Loan
ExceedWarrantyRepairLimit   BYTE                           !Warranty Repair Authorised To Exceed Repair Limit
BouncerClaim                BYTE                           !Set true (1) if this was submitted from Bouncer Browse
Sub_Sub_Account             STRING(15)                     !                    
ConfirmClaimAdjustment      BYTE                           !Confirm Warranty Claim Adjustment Values
ARC3rdPartyVAT              REAL                           !3rd Party Vat       
ARC3rdPartyInvoiceNumber    STRING(30)                     !3rd Party Invoice Number
ExchangeAdjustment          REAL                           !Exchange Adjustment 
ExchangedATRRC              BYTE                           !Exchanged At RRC    
EndUserTelNo                STRING(15)                     !                    
ClaimColour                 BYTE                           !Colour Warranty Claim
ARC3rdPartyMarkup           REAL                           !3rd Party Markup    
Ignore3rdPartyCosts         BYTE                           !Ignore 3rd Party Costs
POPType                     STRING(30)                     !POP Type            
OBFProcessed                BYTE                           !OBF Routine Processed
LoanReplacementValue        REAL                           !Loan Replacement Value
PendingClaimColour          BYTE                           !Pending Claim Colour
AccessoryNotes              STRING(255)                    !Accessory Notes     
ClaimPartsCost              REAL                           !Claim Parts Cost    
InvClaimPartsCost           REAL                           !Invoice Parts Claim Cost
Booking48HourOption         BYTE                           !48 Hour Exchange (Booking)
Engineer48HourOption        BYTE                           !48 Hour Exchange (Engineering)
ExcReplcamentCharge         BYTE                           !Exchange Replacement Charge
SecondExchangeNumber        LONG                           !Second Exchange Unit Number
SecondExchangeStatus        STRING(30)                     !2nd Exchange Status 
VatNumber                   STRING(30)                     !Vat Number          
ExchangeProductCode         STRING(30)                     !Exchange Handset Part Number
SecondExcProdCode           STRING(30)                     !Second Exchange Handset Part Number
ARC3rdPartyInvoiceDate      DATE                           !                    
ARC3rdPartyWaybillNo        STRING(30)                     !                    
ARC3rdPartyRepairType       STRING(30)                     !                    
ARC3rdPartyRejectedReason   STRING(255)                    !                    
ARC3rdPartyRejectedAmount   REAL                           !                    
VSACustomer                 BYTE                           !VSA Customer        
HandsetReplacmentValue      REAL                           !Handset Replacement Value
SecondHandsetRepValue       REAL                           !Handset Replacement Value
t                           STRING(20)                     !                    
                         END
                     END                       

MANFAULT             FILE,DRIVER('Btrieve'),NAME('MANFAULT.DAT'),PRE(maf),CREATE,BINDABLE,THREAD !Manufacturer Fault Coding On Jobs
Field_Number_Key         KEY(maf:Manufacturer,maf:Field_Number),NOCASE,PRIMARY !By Field Number     
MainFaultKey             KEY(maf:Manufacturer,maf:MainFault),DUP,NOCASE !By Main Fault       
InFaultKey               KEY(maf:Manufacturer,maf:InFault),DUP,NOCASE !By In Fault         
ScreenOrderKey           KEY(maf:Manufacturer,maf:ScreenOrder),DUP,NOCASE !By Screen Order     
Record                   RECORD,PRE()
Manufacturer                STRING(30)                     !                    
Field_Number                REAL                           !Fault Code Field Number
Field_Name                  STRING(30)                     !                    
Field_Type                  STRING(30)                     !                    
Lookup                      STRING(3)                      !                    
Force_Lookup                STRING(3)                      !                    
Compulsory                  STRING(3)                      !                    
Compulsory_At_Booking       STRING(3)                      !                    
ReplicateFault              STRING(3)                      !Replicate To Fault Description Field
ReplicateInvoice            STRING(3)                      !Replicate To Fault Description Field
RestrictLength              BYTE                           !Restrict Length     
LengthFrom                  LONG                           !Length From         
LengthTo                    LONG                           !Length To           
ForceFormat                 BYTE                           !Force Format        
FieldFormat                 STRING(30)                     !Field Format        
DateType                    STRING(30)                     !Date Type           
MainFault                   BYTE                           !Main Fault          
PromptForExchange           BYTE                           !Prompt For Exchange 
InFault                     BYTE                           !In Fault            
CompulsoryIfExchange        BYTE                           !Compulsory If Exchange Issued
NotAvailable                BYTE                           !Not Available       
CharCompulsory              BYTE                           !Compulsory At Completion
CharCompulsoryBooking       BYTE                           !Compulsory At Booking
ScreenOrder                 LONG                           !Screen Order        
RestrictAvailability        BYTE                           !Restrict Availability
RestrictServiceCentre       BYTE                           !Available For Service Centres
GenericFault                BYTE                           !Generic Fault       
NotCompulsoryThirdParty     BYTE                           !Not Compulsory For 3rd Party Job
HideThirdParty              BYTE                           !Hide For 3rd Party Jobs
HideRelatedCodeIfBlank      BYTE                           !Hide When Related Code Is Blank
BlankRelatedCode            LONG                           !Blank Related Code  
FillFromDOP                 BYTE                           !Fill From Date Of Purchase
CompulsoryForRepairType     BYTE                           !Compulsory For Repair Type
CompulsoryRepairType        STRING(30)                     !Repair Type         
                         END
                     END                       

TRACHAR              FILE,DRIVER('Btrieve'),NAME('TRACHAR.DAT'),PRE(tch),CREATE,BINDABLE,THREAD !Trade Charge Types For Priced Despatch
Account_Number_Key       KEY(tch:Account_Number,tch:Charge_Type),NOCASE,PRIMARY !By Charge Type      
Record                   RECORD,PRE()
Account_Number              STRING(15)                     !                    
Charge_Type                 STRING(30)                     !                    
                         END
                     END                       

LOCVALUE             FILE,DRIVER('Btrieve'),OEM,NAME('LOCVALUE.DAT'),PRE(lov),CREATE,BINDABLE,THREAD !Location, End Of Period Values
RecordNumberKey          KEY(lov:RecordNumber),NOCASE,PRIMARY !By Record Number    
DateKey                  KEY(lov:Location,lov:TheDate),DUP,NOCASE !By Date             
DateOnly                 KEY(lov:TheDate),DUP,NOCASE,OPT   !                    
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
Location                    STRING(30)                     !Location            
TheDate                     DATE                           !The Date            
PurchaseTotal               REAL                           !Purchase Cost Total 
SaleCostTotal               REAL                           !SaleCostTotal       
RetailCostTotal             REAL                           !RetailCostTotal     
QuantityTotal               LONG                           !QuantityTotal       
                         END
                     END                       

COURIER              FILE,DRIVER('Btrieve'),NAME('COURIER.DAT'),PRE(cou),CREATE,BINDABLE,THREAD !Couriers            
Courier_Key              KEY(cou:Courier),NOCASE,PRIMARY   !By Courier          
Courier_Type_Key         KEY(cou:Courier_Type,cou:Courier),DUP,NOCASE !By Courier_Type     
Record                   RECORD,PRE()
Courier                     STRING(30)                     !                    
Account_Number              STRING(15)                     !                    
Postcode                    STRING(10)                     !                    
Address_Line1               STRING(30)                     !                    
Address_Line2               STRING(30)                     !                    
Address_Line3               STRING(30)                     !                    
Telephone_Number            STRING(15)                     !                    
Fax_Number                  STRING(15)                     !                    
Contact_Name                STRING(60)                     !                    
Export_Path                 STRING(255)                    !                    
Include_Estimate            STRING(3)                      !                    
Include_Chargeable          STRING(3)                      !                    
Include_Warranty            STRING(3)                      !                    
Service                     STRING(15)                     !                    
Import_Path                 STRING(255)                    !                    
Courier_Type                STRING(30)                     !                    
Last_Despatch_Time          TIME                           !                    
ICOLSuffix                  STRING(3)                      !ANC ICOL File Suffix
ContractNumber              STRING(20)                     !ANC Contract Number 
ANCPath                     STRING(255)                    !Path to ANCPAPER.EXE  (I.e. s:\apps\service)
ANCCount                    LONG                           !Count Of Each ANC Export
DespatchClose               STRING(3)                      !Despatch At Closing 
LabGOptions                 STRING(60)                     !Label G Command Line Options
CustomerCollection          BYTE                           !Customer Collection 
NewStatus                   STRING(30)                     !On Despatch Change Status To
NoOfDespatchNotes           LONG                           !No Of Despatch Notes
AutoConsignmentNo           BYTE                           !Auto Allocate Consignment Number
LastConsignmentNo           LONG                           !Last Allocated Consignment Number
PrintLabel                  BYTE                           !Print Label         
PrintWaybill                BYTE                           !Print Waybill       
StartWorkHours              TIME                           !Start Time          
EndWorkHours                TIME                           !End Time            
IncludeSaturday             BYTE                           !Include Saturday    
IncludeSunday               BYTE                           !Include Sunday      
SatStartWorkHours           TIME                           !Start Time          
SatEndWorkHours             TIME                           !End Time            
SunStartWorkHours           TIME                           !Start Time          
SunEndWorkHours             TIME                           !End Time            
EmailAddress                STRING(255)                    !Email Address       
FromEmailAddress            STRING(255)                    !From Email Address  
                         END
                     END                       

TRDPARTY             FILE,DRIVER('Btrieve'),NAME('TRDPARTY.DAT'),PRE(trd),CREATE,BINDABLE,THREAD !Third Party Repairer
Company_Name_Key         KEY(trd:Company_Name),NOCASE,PRIMARY !By Company Name     
Account_Number_Key       KEY(trd:Account_Number),NOCASE    !By Account_Number   
Special_Instructions_Key KEY(trd:Special_Instructions),DUP,NOCASE !By Special Instructions
Courier_Only_Key         KEY(trd:Courier),DUP,NOCASE       !                    
DeactivateCompanyKey     KEY(trd:Deactivate,trd:Company_Name),DUP,NOCASE !By Company Name     
ASCIDKey                 KEY(trd:ASCID),DUP,NOCASE         !By ASC ID           
Record                   RECORD,PRE()
Company_Name                STRING(30)                     !                    
Account_Number              STRING(30)                     !                    
Contact_Name                STRING(60)                     !                    
Postcode                    STRING(15)                     !                    
Address_Line1               STRING(30)                     !                    
Address_Line2               STRING(30)                     !                    
Address_Line3               STRING(30)                     !                    
Telephone_Number            STRING(15)                     !                    
Fax_Number                  STRING(15)                     !                    
Turnaround_Time             REAL                           !                    
Courier_Direct              STRING(3)                      !                    
Courier                     STRING(30)                     !                    
Print_Order                 STRING(15)                     !                    
Special_Instructions        STRING(30)                     !                    
Batch_Number                LONG                           !                    
BatchLimit                  LONG                           !                    
VATRate                     REAL                           !V.A.T. Rate         
Markup                      REAL                           !Markup              
MOPSupplierNumber           STRING(30)                     !MOP Supplier Number 
MOPItemID                   STRING(30)                     !MOP Item ID         
Deactivate                  BYTE                           !Deactivate Account  
LHubAccount                 BYTE                           !L-Hub Account       
ASCID                       STRING(30)                     !ASC ID              
PreCompletionClaiming       BYTE                           !                    
EVO_AccNumber               STRING(20)                     !                    
EVO_VendorNumber            STRING(20)                     !                    
EVO_Profit_Centre           STRING(30)                     !                    
EVO_Excluded                BYTE                           !                    
                         END
                     END                       

JOBNOTES             FILE,DRIVER('Btrieve'),OEM,NAME('JOBNOTES.DAT'),PRE(jbn),CREATE,BINDABLE,THREAD !Notes Attached To Jobs
RefNumberKey             KEY(jbn:RefNumber),NOCASE,PRIMARY !By Job Number       
Record                   RECORD,PRE()
RefNumber                   LONG                           !Related Job Number  
Fault_Description           STRING(255)                    !                    
Engineers_Notes             STRING(255)                    !                    
Invoice_Text                STRING(255)                    !                    
Collection_Text             STRING(255)                    !                    
Delivery_Text               STRING(255)                    !                    
ColContatName               STRING(30)                     !Contact Name        
ColDepartment               STRING(30)                     !Department          
DelContactName              STRING(30)                     !Contact Name        
DelDepartment               STRING(30)                     !Department          
                         END
                     END                       

MANFAURL             FILE,DRIVER('Btrieve'),OEM,NAME('MANFAURL.DAT'),PRE(mnr),CREATE,BINDABLE,THREAD !Related Fault Code  
RecordNumberKey          KEY(mnr:RecordNumber),NOCASE,PRIMARY !By Record Number    
FieldKey                 KEY(mnr:MANFAULORecordNumber,mnr:PartFaultCode,mnr:FieldNumber),DUP,NOCASE !By Field Number     
LinkedRecordNumberKey    KEY(mnr:MANFAULORecordNumber,mnr:PartFaultCode,mnr:LinkedRecordNumber),DUP,NOCASE !By Linked Record Number
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
MANFAULORecordNumber        LONG                           !MANFPALO Record Number
FieldNumber                 LONG                           !Field Number        
LinkedRecordNumber          LONG                           !Linked Record Number
PartFaultCode               BYTE                           !Is This For A Part Fault Code?
RelatedPartFaultCode        LONG                           !Related Part Fault Code
                         END
                     END                       

MODELCCT             FILE,DRIVER('Btrieve'),OEM,NAME('MODELCCT.DAT'),PRE(mcc),CREATE,BINDABLE,THREAD !CCT Reference / Model File
RecordNumberKey          KEY(mcc:RecordNumber),NOCASE,PRIMARY !By Record Number    
CCTRefKey                KEY(mcc:ModelNumber,mcc:CCTReferenceNumber),DUP,NOCASE !By CCT Reference Number
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
ModelNumber                 STRING(30)                     !Model Number        
CCTReferenceNumber          STRING(30)                     !CCT Reference Number
                         END
                     END                       

MANFAUEX             FILE,DRIVER('Btrieve'),OEM,NAME('MANFAUEX.DAT'),PRE(max),CREATE,BINDABLE,THREAD !Fault Code Model Exceptions
RecordNumberKey          KEY(max:RecordNumber),NOCASE,PRIMARY !By Record Number    
ModelNumberKey           KEY(max:MANFAULORecordNumber,max:ModelNumber),DUP,NOCASE !By Model Number..   
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
MANFAULORecordNumber        LONG                           !Link To MANFAULO File
ModelNumber                 STRING(30)                     !Model Number        
                         END
                     END                       

MANFPARL             FILE,DRIVER('Btrieve'),OEM,NAME('MANFPARL.DAT'),PRE(mpr),CREATE,BINDABLE,THREAD !Related Part Fault Code
RecordNumberKey          KEY(mpr:RecordNumber),NOCASE,PRIMARY !By Record Number    
FieldKey                 KEY(mpr:MANFPALORecordNumber,mpr:JobFaultCode,mpr:FieldNumber),DUP,NOCASE !By Field Number     
LinkedRecordNumberKey    KEY(mpr:MANFPALORecordNumber,mpr:JobFaultCode,mpr:LinkedRecordNumber),DUP,NOCASE !By Linked Record Number
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
MANFPALORecordNumber        LONG                           !MANFAULO Record Number
FieldNumber                 LONG                           !Field Number        
LinkedRecordNumber          LONG                           !Linked Record Number
JobFaultCode                BYTE                           !Is This For A Job Fault Code?
                         END
                     END                       

JOBPAYMT             FILE,DRIVER('Btrieve'),NAME('JOBPAYMT.DAT'),PRE(jpt),CREATE,BINDABLE,THREAD !Job Payments        
Record_Number_Key        KEY(jpt:Record_Number),NOCASE,PRIMARY !By Record Number    
All_Date_Key             KEY(jpt:Ref_Number,jpt:Date),DUP,NOCASE !By Date             
Loan_Deposit_Key         KEY(jpt:Ref_Number,jpt:Loan_Deposit,jpt:Date),DUP,NOCASE !                    
Record                   RECORD,PRE()
Record_Number               REAL                           !                    
Ref_Number                  REAL                           !                    
Date                        DATE                           !                    
Payment_Type                STRING(30)                     !                    
Credit_Card_Number          STRING(20)                     !                    
Expiry_Date                 STRING(5)                      !                    
Issue_Number                STRING(5)                      !                    
Amount                      REAL                           !                    
User_Code                   STRING(3)                      !                    
Loan_Deposit                BYTE                           !Loan Deposit        
                         END
                     END                       

MANREJR              FILE,DRIVER('Btrieve'),OEM,NAME('MANREJR.DAT'),PRE(mar),CREATE,BINDABLE,THREAD !Manufacturer Rejection Reasons
RecordNumberKey          KEY(mar:RecordNumber),NOCASE,PRIMARY !By Record Number    
CodeKey                  KEY(mar:MANRecordNumber,mar:CodeNumber),NOCASE !By Code             
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
MANRecordNumber             LONG                           !MANUFACT Record Number
CodeNumber                  STRING(60)                     !CodeNumber          
Description                 STRING(255)                    !Description         
                         END
                     END                       

STOCK                FILE,DRIVER('Btrieve'),NAME('STOCK.DAT'),PRE(sto),CREATE,BINDABLE,THREAD !Stock Control       
Ref_Number_Key           KEY(sto:Ref_Number),NOCASE,PRIMARY !By Ref Number       
Sundry_Item_Key          KEY(sto:Sundry_Item),DUP,NOCASE   !By Sundry Item      
Description_Key          KEY(sto:Location,sto:Description),DUP,NOCASE !By Description      
Description_Accessory_Key KEY(sto:Location,sto:Suspend,sto:Accessory,sto:Description),DUP,NOCASE !By Description      
Shelf_Location_Key       KEY(sto:Location,sto:Shelf_Location),DUP,NOCASE !By Shelf Location   
Shelf_Location_Accessory_Key KEY(sto:Location,sto:Suspend,sto:Accessory,sto:Shelf_Location),DUP,NOCASE !By Shelf Location   
Supplier_Accessory_Key   KEY(sto:Location,sto:Suspend,sto:Accessory,sto:Shelf_Location),DUP,NOCASE !By Supplier         
Manufacturer_Key         KEY(sto:Manufacturer,sto:Part_Number),DUP,NOCASE !By Manufacturer     
Supplier_Key             KEY(sto:Location,sto:Suspend,sto:Supplier),DUP,NOCASE !By Supplier         
Location_Key             KEY(sto:Location,sto:Part_Number),DUP,NOCASE !By Location         
Part_Number_Accessory_Key KEY(sto:Location,sto:Suspend,sto:Accessory,sto:Part_Number),DUP,NOCASE !By Part Number      
Ref_Part_Description_Key KEY(sto:Location,sto:Ref_Number,sto:Part_Number,sto:Description),DUP,NOCASE !Why?                
Location_Manufacturer_Key KEY(sto:Location,sto:Manufacturer,sto:Part_Number),DUP,NOCASE !By Manufacturer     
Manufacturer_Accessory_Key KEY(sto:Location,sto:Suspend,sto:Accessory,sto:Manufacturer,sto:Part_Number),DUP,NOCASE !By Part Number      
Location_Part_Description_Key KEY(sto:Location,sto:Part_Number,sto:Description),DUP,NOCASE !By Part Number      
Ref_Part_Description2_Key KEY(sto:Ref_Number,sto:Part_Number,sto:Description),DUP,NOCASE !CASCADE Key         
Minimum_Part_Number_Key  KEY(sto:Minimum_Stock,sto:Location,sto:Part_Number),DUP,NOCASE !By Part Number      
Minimum_Description_Key  KEY(sto:Minimum_Stock,sto:Location,sto:Description),DUP,NOCASE !                    
SecondLocKey             KEY(sto:Location,sto:Shelf_Location,sto:Second_Location,sto:Part_Number),DUP,NOCASE !By Part Number      
RequestedKey             KEY(sto:QuantityRequested,sto:Part_Number),DUP,NOCASE !By Part Number      
ExchangeAccPartKey       KEY(sto:ExchangeUnit,sto:Location,sto:Part_Number),DUP,NOCASE !By Part Number      
ExchangeAccDescKey       KEY(sto:ExchangeUnit,sto:Location,sto:Description),DUP,NOCASE !By Description      
DateBookedKey            KEY(sto:DateBooked),DUP,NOCASE    !By Date Booked      
Supplier_Only_Key        KEY(sto:Supplier),DUP,NOCASE      !                    
LocPartSuspendKey        KEY(sto:Location,sto:Suspend,sto:Part_Number),DUP,NOCASE !By Part Number      
LocDescSuspendKey        KEY(sto:Location,sto:Suspend,sto:Description),DUP,NOCASE !By Description      
LocShelfSuspendKey       KEY(sto:Location,sto:Suspend,sto:Shelf_Location),DUP,NOCASE !By Shelf Location   
LocManSuspendKey         KEY(sto:Location,sto:Suspend,sto:Manufacturer,sto:Part_Number),DUP,NOCASE !By Part Number      
ExchangeModelKey         KEY(sto:Location,sto:Manufacturer,sto:ExchangeModelNumber),DUP,NOCASE !                    
LoanModelKey             KEY(sto:Location,sto:Manufacturer,sto:LoanModelNumber),DUP,NOCASE !By Loan Model Number
Record                   RECORD,PRE()
Sundry_Item                 STRING(3)                      !                    
Ref_Number                  LONG                           !Reference Number    
Part_Number                 STRING(30)                     !                    
Description                 STRING(30)                     !                    
Supplier                    STRING(30)                     !                    
Purchase_Cost               REAL                           !                    
Sale_Cost                   REAL                           !                    
Retail_Cost                 REAL                           !                    
AccessoryCost               REAL                           !Accessory Cost      
Percentage_Mark_Up          REAL                           !                    
Shelf_Location              STRING(30)                     !                    
Manufacturer                STRING(30)                     !                    
Location                    STRING(30)                     !                    
Second_Location             STRING(30)                     !                    
Quantity_Stock              REAL                           !                    
Quantity_To_Order           REAL                           !                    
Quantity_On_Order           REAL                           !                    
Minimum_Level               REAL                           !                    
Reorder_Level               REAL                           !                    
Use_VAT_Code                STRING(3)                      !                    
Service_VAT_Code            STRING(2)                      !                    
Retail_VAT_Code             STRING(2)                      !                    
Superceeded                 STRING(3)                      !                    
Superceeded_Ref_Number      REAL                           !                    
Pending_Ref_Number          REAL                           !                    
Accessory                   STRING(3)                      !                    
Minimum_Stock               STRING(3)                      !                    
Assign_Fault_Codes          STRING(3)                      !                    
Individual_Serial_Numbers   STRING(3)                      !                    
ExchangeUnit                STRING(3)                      !                    
QuantityRequested           LONG                           !Quantity Requested  
Suspend                     BYTE                           !Suspend Part        
Fault_Code1                 STRING(30)                     !                    
Fault_Code2                 STRING(30)                     !                    
Fault_Code3                 STRING(30)                     !                    
Fault_Code4                 STRING(30)                     !                    
Fault_Code5                 STRING(30)                     !                    
Fault_Code6                 STRING(30)                     !                    
Fault_Code7                 STRING(30)                     !                    
Fault_Code8                 STRING(30)                     !                    
Fault_Code9                 STRING(30)                     !                    
Fault_Code10                STRING(30)                     !                    
Fault_Code11                STRING(30)                     !                    
Fault_Code12                STRING(30)                     !                    
E1                          BYTE                           !E1                  
E2                          BYTE                           !E2                  
E3                          BYTE                           !E3                  
ReturnFaultySpare           BYTE                           !Return Faulty Spare 
ChargeablePartOnly          BYTE                           !Chargeable Part Only
AttachBySolder              BYTE                           !Attach By Solder    
AllowDuplicate              BYTE                           !Allow Duplicate Part On Jobs
RetailMarkup                REAL                           !Percentage Mark Up  
VirtPurchaseCost            REAL                           !Purchase Cost       
VirtTradeCost               REAL                           !Trade Price         
VirtRetailCost              REAL                           !Retail Price        
VirtTradeMarkup             REAL                           !Percent Mark Up     
VirtRetailMarkup            REAL                           !Percentage Mark Up  
AveragePurchaseCost         REAL                           !Average Purchase Cost
PurchaseMarkUp              REAL                           !Percentage Mark Up  
RepairLevel                 LONG                           !Repair Index        
SkillLevel                  LONG                           !Skill Level         
DateBooked                  DATE                           !Date Booked         
AccWarrantyPeriod           LONG                           !Accessory Warranty Period
ExcludeLevel12Repair        BYTE                           !Exclude From Level 1 && 2 Repairs
ExchangeOrderCap            LONG                           !Exchange Order Cap  
ExchangeModelNumber         STRING(30)                     !                    
LoanUnit                    BYTE                           !                    
LoanModelNumber             STRING(30)                     !                    
                         END
                     END                       

MANMARK              FILE,DRIVER('Btrieve'),OEM,NAME('MANMARK.DAT'),PRE(mak),CREATE,BINDABLE,THREAD !Manufacturer In Warranty Markups
RecordNumberKey          KEY(mak:RecordNumber),NOCASE,PRIMARY !By Record Number    
SiteLocationKey          KEY(mak:RefNumber,mak:SiteLocation),NOCASE !By Site Location    
SiteLocationOnlyKey      KEY(mak:SiteLocation),DUP,NOCASE  !By Site Location    
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Ref Number          
SiteLocation                STRING(30)                     !Site Location       
InWarrantyMarkup            LONG                           !In Warranty Markup  
                         END
                     END                       

MODPROD              FILE,DRIVER('Btrieve'),OEM,NAME('MODPROD.DAT'),PRE(mop),CREATE,BINDABLE,THREAD !Model Related Product Code
RecordNumberKey          KEY(mop:RecordNumber),NOCASE,PRIMARY !By Record Number    
ProductCodeKey           KEY(mop:ModelNumber,mop:ProductCode),NOCASE !By Model No/Product Code
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
ModelNumber                 STRING(30)                     !Model Number        
ProductCode                 STRING(30)                     !Product Code        
                         END
                     END                       

TRAFAULO             FILE,DRIVER('Btrieve'),NAME('TRAFAULO.DAT'),PRE(tfo),CREATE,BINDABLE,THREAD !Trade Account Fault Coding Lookup Table
Field_Key                KEY(tfo:AccountNumber,tfo:Field_Number,tfo:Field,tfo:Description),NOCASE,PRIMARY !By Field            
DescriptionKey           KEY(tfo:AccountNumber,tfo:Field_Number,tfo:Description),DUP,NOCASE !By Description      
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
AccountNumber               STRING(30)                     !Account Number      
Field_Number                REAL                           !                    
Field                       STRING(30)                     !                    
Description                 STRING(60)                     !                    
                         END
                     END                       

TRAFAULT             FILE,DRIVER('Btrieve'),NAME('TRAFAULT.DAT'),PRE(taf),CREATE,BINDABLE,THREAD !Trade Account Fault Coding On Jobs
RecordNumberKey          KEY(taf:RecordNumber),NOCASE,PRIMARY !                    
Field_Number_Key         KEY(taf:AccountNumber,taf:Field_Number),NOCASE !By Field Number     
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
AccountNumber               STRING(30)                     !Account Number      
Field_Number                REAL                           !Fault Code Field Number
Field_Name                  STRING(30)                     !                    
Field_Type                  STRING(30)                     !                    
Lookup                      STRING(3)                      !                    
Force_Lookup                STRING(3)                      !                    
Compulsory                  STRING(3)                      !                    
Compulsory_At_Booking       STRING(3)                      !                    
ReplicateFault              STRING(3)                      !Replicate To Fault Description Field
ReplicateInvoice            STRING(3)                      !Replicate To Fault Description Field
RestrictLength              BYTE                           !Restrict Length     
LengthFrom                  LONG                           !Length From         
LengthTo                    LONG                           !Length To           
                         END
                     END                       

JOBLOHIS             FILE,DRIVER('Btrieve'),NAME('JOBLOHIS.DAT'),PRE(jlh),CREATE,BINDABLE,THREAD !Job Loan History    
Ref_Number_Key           KEY(jlh:Ref_Number,jlh:Date,jlh:Time),DUP,NOCASE !By Date             
record_number_key        KEY(jlh:record_number),NOCASE,PRIMARY !                    
Record                   RECORD,PRE()
Ref_Number                  REAL                           !                    
record_number               REAL                           !                    
Loan_Unit_Number            REAL                           !                    
Date                        DATE                           !                    
Time                        TIME                           !                    
User                        STRING(3)                      !                    
Status                      STRING(60)                     !                    
                         END
                     END                       

ACCESSOR             FILE,DRIVER('Btrieve'),NAME('ACCESSOR.DAT'),PRE(acr),CREATE,BINDABLE,THREAD !Accessories         
Accesory_Key             KEY(acr:Model_Number,acr:Accessory),NOCASE,PRIMARY !By Accessory        
Model_Number_Key         KEY(acr:Accessory,acr:Model_Number),NOCASE !By Model Number     
AccessOnlyKey            KEY(acr:Accessory),DUP,NOCASE     !By Accessory        
Record                   RECORD,PRE()
Accessory                   STRING(30)                     !                    
Model_Number                STRING(30)                     !                    
                         END
                     END                       

SUBACCAD             FILE,DRIVER('Btrieve'),OEM,NAME('SUBACCAD.DAT'),PRE(sua),CREATE,BINDABLE,THREAD !Sub Account Addresses
RecordNumberKey          KEY(sua:RecordNumber),NOCASE,PRIMARY !By Record Number    
AccountNumberKey         KEY(sua:RefNumber,sua:AccountNumber),DUP,NOCASE !By Account Number   
CompanyNameKey           KEY(sua:RefNumber,sua:CompanyName),DUP,NOCASE !By Company Name     
AccountNumberOnlyKey     KEY(sua:AccountNumber),NOCASE     !By Account Number   
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !RefNumber           
AccountNumber               STRING(15)                     !                    
CompanyName                 STRING(30)                     !                    
Postcode                    STRING(15)                     !                    
AddressLine1                STRING(30)                     !                    
AddressLine2                STRING(30)                     !                    
AddressLine3                STRING(30)                     !                    
TelephoneNumber             STRING(15)                     !                    
FaxNumber                   STRING(15)                     !                    
EmailAddress                STRING(255)                    !Email Address       
ContactName                 STRING(30)                     !                    
                         END
                     END                       

WARPARTS             FILE,DRIVER('Btrieve'),NAME('WARPARTS.DAT'),PRE(wpr),CREATE,BINDABLE,THREAD !Warrant Parts On A Job
Part_Number_Key          KEY(wpr:Ref_Number,wpr:Part_Number),DUP,NOCASE !By Part Number      
RecordNumberKey          KEY(wpr:Record_Number),NOCASE,PRIMARY !                    
PartRefNoKey             KEY(wpr:Part_Ref_Number),DUP,NOCASE !By Part Ref Number  
Date_Ordered_Key         KEY(wpr:Date_Ordered),DUP,NOCASE  !By Date Ordered     
RefPartRefNoKey          KEY(wpr:Ref_Number,wpr:Part_Ref_Number),DUP,NOCASE !By Part Ref Number  
PendingRefNoKey          KEY(wpr:Ref_Number,wpr:Pending_Ref_Number),DUP,NOCASE !                    
Order_Number_Key         KEY(wpr:Ref_Number,wpr:Order_Number),DUP,NOCASE !                    
Supplier_Key             KEY(wpr:Supplier),DUP,NOCASE      !By Supplier         
Order_Part_Key           KEY(wpr:Ref_Number,wpr:Order_Part_Number),DUP,NOCASE !By Order Part Number
SuppDateOrdKey           KEY(wpr:Supplier,wpr:Date_Ordered),DUP,NOCASE !By Date Ordered     
SuppDateRecKey           KEY(wpr:Supplier,wpr:Date_Received),DUP,NOCASE !By Date Received    
RequestedKey             KEY(wpr:Requested,wpr:Part_Number),DUP,NOCASE !By Part Number      
WebOrderKey              KEY(wpr:WebOrder,wpr:Part_Number),DUP,NOCASE !By Part Number      
PartAllocatedKey         KEY(wpr:PartAllocated,wpr:Part_Number),DUP,NOCASE !By Part Number      
StatusKey                KEY(wpr:Status,wpr:Part_Number),DUP,NOCASE !By Part Number      
AllocatedStatusKey       KEY(wpr:PartAllocated,wpr:Status,wpr:Part_Number),DUP,NOCASE !By Part Number      
Record                   RECORD,PRE()
Ref_Number                  LONG                           !Job Number          
Record_Number               LONG                           !Record Number       
Adjustment                  STRING(3)                      !                    
Part_Ref_Number             LONG                           !Stock Reference Number
Part_Number                 STRING(30)                     !                    
Description                 STRING(30)                     !                    
Supplier                    STRING(30)                     !                    
Purchase_Cost               REAL                           !                    
Sale_Cost                   REAL                           !                    
Retail_Cost                 REAL                           !                    
Quantity                    REAL                           !                    
Warranty_Part               STRING(3)                      !                    
Exclude_From_Order          STRING(3)                      !                    
Despatch_Note_Number        STRING(30)                     !                    
Date_Ordered                DATE                           !                    
Pending_Ref_Number          LONG                           !Pending Reference Number
Order_Number                LONG                           !Order Number        
Order_Part_Number           REAL                           !                    
Date_Received               DATE                           !                    
Status_Date                 DATE                           !                    
Main_Part                   STRING(3)                      !                    
Fault_Codes_Checked         STRING(3)                      !                    
InvoicePart                 LONG                           !Link to Record Number of Invoice(Credit) Part
Credit                      STRING(3)                      !Has this part been credited?
Requested                   BYTE                           !Requested For Order 
Fault_Code1                 STRING(30)                     !                    
Fault_Code2                 STRING(30)                     !                    
Fault_Code3                 STRING(30)                     !                    
Fault_Code4                 STRING(30)                     !                    
Fault_Code5                 STRING(30)                     !                    
Fault_Code6                 STRING(30)                     !                    
Fault_Code7                 STRING(30)                     !                    
Fault_Code8                 STRING(30)                     !                    
Fault_Code9                 STRING(30)                     !                    
Fault_Code10                STRING(30)                     !                    
Fault_Code11                STRING(30)                     !                    
Fault_Code12                STRING(30)                     !                    
WebOrder                    BYTE                           !Web Order           
PartAllocated               BYTE                           !Part Allocated      
Status                      STRING(3)                      !Status              
CostAdjustment              REAL                           !Cost Adjustment     
AveragePurchaseCost         REAL                           !Purchase Cost       
InWarrantyMarkup            LONG                           !% Markup            
OutWarrantyMarkup           LONG                           !% Markup            
RRCPurchaseCost             REAL                           !RRC Purchase Cost   
RRCSaleCost                 REAL                           !RRC Sale Cost       
RRCInWarrantyMarkup         LONG                           !RRC In Warranty Markup
RRCOutWarrantyMarkup        LONG                           !RRC Out Warranty Markup
RRCAveragePurchaseCost      REAL                           !RRC Average Purchase Cost
ExchangeUnit                BYTE                           !Exchange Unit Part  
SecondExchangeUnit          BYTE                           !Second Exchange Unit
Correction                  BYTE                           !Part Correction     
                         END
                     END                       

PARTS                FILE,DRIVER('Btrieve'),NAME('PARTS.DAT'),PRE(par),CREATE,BINDABLE,THREAD !Parts On A Job      
Part_Number_Key          KEY(par:Ref_Number,par:Part_Number),DUP,NOCASE !By Part Number      
recordnumberkey          KEY(par:Record_Number),NOCASE,PRIMARY !                    
PartRefNoKey             KEY(par:Part_Ref_Number),DUP,NOCASE !By Part_Ref_Number  
Date_Ordered_Key         KEY(par:Date_Ordered),DUP,NOCASE  !By Date Ordered     
RefPartRefNoKey          KEY(par:Ref_Number,par:Part_Ref_Number),DUP,NOCASE !By Part Ref Number  
PendingRefNoKey          KEY(par:Ref_Number,par:Pending_Ref_Number),DUP,NOCASE !                    
Order_Number_Key         KEY(par:Ref_Number,par:Order_Number),DUP,NOCASE !                    
Supplier_Key             KEY(par:Supplier),DUP,NOCASE      !By Supplier         
Order_Part_Key           KEY(par:Ref_Number,par:Order_Part_Number),DUP,NOCASE !By Order Part Number
SuppDateOrdKey           KEY(par:Supplier,par:Date_Ordered),DUP,NOCASE !By Date Ordered     
SuppDateRecKey           KEY(par:Supplier,par:Date_Received),DUP,NOCASE !By Date Received    
RequestedKey             KEY(par:Requested,par:Part_Number),DUP,NOCASE !By Part Number      
WebOrderKey              KEY(par:WebOrder,par:Part_Number),DUP,NOCASE !By Part Number      
PartAllocatedKey         KEY(par:PartAllocated,par:Part_Number),DUP,NOCASE !By Part Number      
StatusKey                KEY(par:Status,par:Part_Number),DUP,NOCASE !By Part Number      
AllocatedStatusKey       KEY(par:PartAllocated,par:Status,par:Part_Number),DUP,NOCASE !By Part Number      
Record                   RECORD,PRE()
Ref_Number                  LONG                           !Job Number          
Record_Number               LONG                           !Record Number       
Adjustment                  STRING(3)                      !                    
Part_Ref_Number             LONG                           !Stock Reference Number
Part_Number                 STRING(30)                     !                    
Description                 STRING(30)                     !                    
Supplier                    STRING(30)                     !                    
Purchase_Cost               REAL                           !In Warranty Cost    
Sale_Cost                   REAL                           !Out Warranty Cost   
Retail_Cost                 REAL                           !                    
Quantity                    REAL                           !                    
Warranty_Part               STRING(3)                      !                    
Exclude_From_Order          STRING(3)                      !                    
Despatch_Note_Number        STRING(30)                     !                    
Date_Ordered                DATE                           !                    
Pending_Ref_Number          LONG                           !Pending Reference Number
Order_Number                LONG                           !Order Number        
Order_Part_Number           LONG                           !Order Reference Number
Date_Received               DATE                           !                    
Status_Date                 DATE                           !                    
Fault_Codes_Checked         STRING(3)                      !                    
InvoicePart                 LONG                           !Link to Record Number of Invoice(Credit) Part
Credit                      STRING(3)                      !Has this part been credited?
Requested                   BYTE                           !Parts Requested For Order
Fault_Code1                 STRING(30)                     !                    
Fault_Code2                 STRING(30)                     !                    
Fault_Code3                 STRING(30)                     !                    
Fault_Code4                 STRING(30)                     !                    
Fault_Code5                 STRING(30)                     !                    
Fault_Code6                 STRING(30)                     !                    
Fault_Code7                 STRING(30)                     !                    
Fault_Code8                 STRING(30)                     !                    
Fault_Code9                 STRING(30)                     !                    
Fault_Code10                STRING(30)                     !                    
Fault_Code11                STRING(30)                     !                    
Fault_Code12                STRING(30)                     !                    
WebOrder                    BYTE                           !Web Order           
PartAllocated               BYTE                           !Part Allocated      
Status                      STRING(3)                      !Status              
AveragePurchaseCost         REAL                           !Purchase Cost       
InWarrantyMarkup            LONG                           !% Markup            
OutWarrantyMarkup           LONG                           !% Markup            
RRCPurchaseCost             REAL                           !RRC Purchase Cost   
RRCSaleCost                 REAL                           !RRC Sale Cost       
RRCInWarrantyMarkup         LONG                           !RRC In Warranty Markup
RRCOutWarrantyMarkup        LONG                           !RRC Out Warranty Markup
RRCAveragePurchaseCost      REAL                           !RRC Average Purchase Cost
ExchangeUnit                BYTE                           !Exchange Unit       
SecondExchangeUnit          BYTE                           !Second Exchange Unit
Correction                  BYTE                           !Part Correction     
                         END
                     END                       

JOBACC               FILE,DRIVER('Btrieve'),NAME('JOBACC.DAT'),PRE(jac),CREATE,BINDABLE,THREAD !Job Accesories      
Ref_Number_Key           KEY(jac:Ref_Number,jac:Accessory),NOCASE,PRIMARY !By Accessory        
DamagedKey               KEY(jac:Ref_Number,jac:Damaged,jac:Accessory),DUP,NOCASE !By Accessory        
DamagedPirateKey         KEY(jac:Ref_Number,jac:Damaged,jac:Pirate,jac:Accessory),DUP,NOCASE !By Accessory        
Record                   RECORD,PRE()
Ref_Number                  REAL                           !                    
Accessory                   STRING(30)                     !                    
Damaged                     BYTE                           !Damaged             
Pirate                      BYTE                           !Pirate              
Attached                    BYTE                           !Attached At RRC     
                         END
                     END                       

AUDIT                FILE,DRIVER('Btrieve'),NAME('AUDIT.DAT'),PRE(aud),CREATE,BINDABLE,THREAD !Audit Trail         
Ref_Number_Key           KEY(aud:Ref_Number,-aud:Date,-aud:Time,aud:Action),DUP,NOCASE !By Date             
Action_Key               KEY(aud:Ref_Number,aud:Action,-aud:Date),DUP,NOCASE !By Action           
User_Key                 KEY(aud:Ref_Number,aud:User,-aud:Date),DUP,NOCASE !By User             
Record_Number_Key        KEY(aud:record_number),NOCASE,PRIMARY !                    
ActionOnlyKey            KEY(aud:Action,aud:Date),DUP,NOCASE !By Action           
TypeRefKey               KEY(aud:Ref_Number,aud:Type,-aud:Date,-aud:Time,aud:Action),DUP,NOCASE !By Date             
TypeActionKey            KEY(aud:Ref_Number,aud:Type,aud:Action,-aud:Date),DUP,NOCASE !By Action           
TypeUserKey              KEY(aud:Ref_Number,aud:Type,aud:User,-aud:Date),DUP,NOCASE !By User             
DateActionJobKey         KEY(aud:Date,aud:Action,aud:Ref_Number),DUP,NOCASE !By Job Number       
DateJobKey               KEY(aud:Date,aud:Ref_Number),DUP,NOCASE !By Job Number       
DateTypeJobKey           KEY(aud:Date,aud:Type,aud:Ref_Number),DUP,NOCASE !By Job Number       
DateTypeActionKey        KEY(aud:Date,aud:Type,aud:Action,aud:Ref_Number),DUP,NOCASE !By Job Number       
Record                   RECORD,PRE()
record_number               REAL                           !                    
Ref_Number                  REAL                           !                    
Date                        DATE                           !                    
Time                        TIME                           !                    
User                        STRING(3)                      !                    
Action                      STRING(80)                     !                    
Type                        STRING(3)                      !Job/Exchange/Loan   
                         END
                     END                       

USERS                FILE,DRIVER('Btrieve'),NAME('USERS.DAT'),PRE(use),CREATE,BINDABLE,THREAD !Users               
User_Code_Key            KEY(use:User_Code),NOCASE,PRIMARY !By User Code        
User_Type_Active_Key     KEY(use:User_Type,use:Active,use:Surname),DUP,NOCASE !By Surname          
Surname_Active_Key       KEY(use:Active,use:Surname),DUP,NOCASE !By Surname          
User_Code_Active_Key     KEY(use:Active,use:User_Code),DUP,NOCASE !                    
User_Type_Key            KEY(use:User_Type,use:Surname),DUP,NOCASE !By Surname          
surname_key              KEY(use:Surname),DUP,NOCASE       !                    
password_key             KEY(use:Password),NOCASE          !                    
Logged_In_Key            KEY(use:Logged_In,use:Surname),DUP,NOCASE !By Name             
Team_Surname             KEY(use:Team,use:Surname),DUP,NOCASE !Surname             
Active_Team_Surname      KEY(use:Team,use:Active,use:Surname),DUP,NOCASE !By Surname          
LocationSurnameKey       KEY(use:Location,use:Surname),DUP,NOCASE !By Surname          
LocationForenameKey      KEY(use:Location,use:Forename),DUP,NOCASE !By Forename         
LocActiveSurnameKey      KEY(use:Active,use:Location,use:Surname),DUP,NOCASE !By Surname          
LocActiveForenameKey     KEY(use:Active,use:Location,use:Forename),DUP,NOCASE !By Forename         
TeamStatusKey            KEY(use:IncludeInEngStatus,use:Team,use:Surname),DUP,NOCASE !By Surname          
Record                   RECORD,PRE()
User_Code                   STRING(3)                      !                    
Forename                    STRING(30)                     !                    
Surname                     STRING(30)                     !                    
Password                    STRING(20)                     !                    
User_Type                   STRING(15)                     !                    
Job_Assignment              STRING(15)                     !                    
Supervisor                  STRING(3)                      !                    
User_Level                  STRING(30)                     !                    
Logged_In                   STRING(1)                      !                    
Logged_in_date              DATE                           !                    
Logged_In_Time              TIME                           !                    
Restrict_Logins             STRING(3)                      !                    
Concurrent_Logins           REAL                           !                    
Active                      STRING(3)                      !                    
Team                        STRING(30)                     !                    
Location                    STRING(30)                     !                    
Repair_Target               REAL                           !                    
SkillLevel                  LONG                           !Skill Level         
StockFromLocationOnly       BYTE                           !Pick Stock From Users Location Only
EmailAddress                STRING(255)                    !Email Address       
RenewPassword               LONG                           !Renew Password      
PasswordLastChanged         DATE                           !Password Last Changed
RestrictParts               BYTE                           !Restrict Parts Usage To Skill Level
RestrictChargeable          BYTE                           !Chargeable          
RestrictWarranty            BYTE                           !Warranty            
IncludeInEngStatus          BYTE                           !Include In Engineer Status (True/False)
MobileNumber                STRING(20)                     !                    
                         END
                     END                       

LOCSHELF             FILE,DRIVER('Btrieve'),NAME('LOCSHELF.DAT'),PRE(los),CREATE,BINDABLE,THREAD !Locations - Shelf   
Shelf_Location_Key       KEY(los:Site_Location,los:Shelf_Location),NOCASE,PRIMARY !By Shelf Location   
Record                   RECORD,PRE()
Site_Location               STRING(30)                     !                    
Shelf_Location              STRING(30)                     !                    
                         END
                     END                       

REPTYCAT             FILE,DRIVER('Btrieve'),OEM,NAME('REPTYCAT.DAT'),PRE(repc),CREATE,BINDABLE,THREAD !Repair Type Categories
RecordNumberKey          KEY(repc:RecordNumber),NOCASE,PRIMARY !By Record Number    
RepairTypeKey            KEY(repc:RepairType),DUP,NOCASE   !By Repair Type      
CategoryKey              KEY(repc:Category,repc:RepairType),DUP,NOCASE !By Repair Type      
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RepairType                  STRING(30)                     !Repair Type         
Category                    STRING(30)                     !Category            
                         END
                     END                       

MANFPALO             FILE,DRIVER('Btrieve'),NAME('MANFPALO.DAT'),PRE(mfp),CREATE,BINDABLE,THREAD !Manufacturer Fault Coding Lookup Table
RecordNumberKey          KEY(mfp:RecordNumber),NOCASE,PRIMARY !By Record Number    
Field_Key                KEY(mfp:Manufacturer,mfp:Field_Number,mfp:Field),NOCASE !By Field            
DescriptionKey           KEY(mfp:Manufacturer,mfp:Field_Number,mfp:Description),DUP,NOCASE !By Description.     
AvailableFieldKey        KEY(mfp:NotAvailable,mfp:Manufacturer,mfp:Field_Number,mfp:Field),DUP,NOCASE !By Field            
AvailableDescriptionKey  KEY(mfp:Manufacturer,mfp:NotAvailable,mfp:Field_Number,mfp:Description),DUP,NOCASE !By Description      
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
Manufacturer                STRING(30)                     !                    
Field_Number                REAL                           !                    
Field                       STRING(30)                     !                    
Description                 STRING(60)                     !                    
RestrictLookup              BYTE                           !Restrict Lookup To  
RestrictLookupType          BYTE                           !Restrict Lookup Type
NotAvailable                BYTE                           !Not Available       
ForceJobFaultCode           BYTE                           !Force Job Fault Code
ForceFaultCodeNumber        LONG                           !Job Fault Code Number
SetPartFaultCode            BYTE                           !Set Part Fault Code 
SelectPartFaultCode         LONG                           !Select Part Fault Code
PartFaultCodeValue          STRING(30)                     !Part Fault Code Value
JobTypeAvailability         BYTE                           !Job Type Availability
                         END
                     END                       

JOBSTAGE             FILE,DRIVER('Btrieve'),NAME('JOBSTAGE.DAT'),PRE(jst),CREATE,BINDABLE,THREAD !Job Stage           
Ref_Number_Key           KEY(jst:Ref_Number,-jst:Date),DUP,NOCASE !By Ref Number       
Job_Stage_Key            KEY(jst:Ref_Number,jst:Job_Stage),NOCASE,PRIMARY !                    
Record                   RECORD,PRE()
Ref_Number                  REAL                           !                    
Job_Stage                   STRING(30)                     !                    
Date                        DATE                           !                    
Time                        TIME                           !                    
User                        STRING(3)                      !                    
                         END
                     END                       

MANFAUPA             FILE,DRIVER('Btrieve'),NAME('MANFAUPA.DAT'),PRE(map),CREATE,BINDABLE,THREAD !Manufacturer Fault Coding on Parts
Field_Number_Key         KEY(map:Manufacturer,map:Field_Number),NOCASE,PRIMARY !By Field Number     
MainFaultKey             KEY(map:Manufacturer,map:MainFault),DUP,NOCASE !By Main Fault       
ScreenOrderKey           KEY(map:Manufacturer,map:ScreenOrder),DUP,NOCASE !By Screen Order     
KeyRepairKey             KEY(map:Manufacturer,map:KeyRepair),DUP,NOCASE !By KeyRepair        
Record                   RECORD,PRE()
Manufacturer                STRING(30)                     !                    
Field_Number                REAL                           !Fault Code Field Number
Field_Name                  STRING(30)                     !                    
Field_Type                  STRING(30)                     !                    
Lookup                      STRING(3)                      !                    
Force_Lookup                STRING(3)                      !                    
Compulsory                  STRING(3)                      !                    
RestrictLength              BYTE                           !Restrict Length     
LengthFrom                  LONG                           !Length From         
LengthTo                    LONG                           !Length To           
ForceFormat                 BYTE                           !Force Format        
FieldFormat                 STRING(30)                     !Field Format        
DateType                    STRING(30)                     !Date Type           
MainFault                   BYTE                           !Main Fault          
UseRelatedJobCode           BYTE                           !Use Related Job Code
NotAvailable                BYTE                           !Not Available       
ScreenOrder                 LONG                           !Screen Order        
CopyFromJobFaultCode        BYTE                           !Copy From Job Fault Code
CopyJobFaultCode            LONG                           !Job Fault Code      
NAForAccessory              BYTE                           !N/A For Accessory   
CompulsoryForAdjustment     BYTE                           !Compulsory For Adjustment
KeyRepair                   BYTE                           !Key Repair          
CCTReferenceFaultCode       BYTE                           !"CCT Reference Fault Code"
NAForSW                     BYTE                           !Fill With "N/A" For Software Upgrade
                         END
                     END                       

LOCINTER             FILE,DRIVER('Btrieve'),NAME('LOCINTER.DAT'),PRE(loi),CREATE,BINDABLE,THREAD !Locations - Internal
Location_Key             KEY(loi:Location),NOCASE,PRIMARY  !By Location         
Location_Available_Key   KEY(loi:Location_Available,loi:Location),DUP,NOCASE !By Location         
Record                   RECORD,PRE()
Location                    STRING(30)                     !                    
Location_Available          STRING(3)                      !                    
Allocate_Spaces             STRING(3)                      !                    
Total_Spaces                REAL                           !                    
Current_Spaces              STRING(6)                      !                    
                         END
                     END                       

NOTESFAU             FILE,DRIVER('Btrieve'),NAME('NOTESFAU.DAT'),PRE(nof),CREATE,BINDABLE,THREAD !Notes - Fault Description
Notes_Key                KEY(nof:Manufacturer,nof:Reference,nof:Notes),NOCASE,PRIMARY !By Notes            
Record                   RECORD,PRE()
Manufacturer                STRING(30)                     !                    
Reference                   STRING(30)                     !                    
Notes                       STRING(80)                     !                    
                         END
                     END                       

MANFAULO             FILE,DRIVER('Btrieve'),NAME('MANFAULO.DAT'),PRE(mfo),CREATE,BINDABLE,THREAD !Manufacturer Fault Coding Lookup Table O
RecordNumberKey          KEY(mfo:RecordNumber),NOCASE,PRIMARY !By Record Number    
RelatedFieldKey          KEY(mfo:Manufacturer,mfo:RelatedPartCode,mfo:Field_Number,mfo:Field),DUP,NOCASE !By Field            
Field_Key                KEY(mfo:Manufacturer,mfo:Field_Number,mfo:Field),DUP,NOCASE !By Field            
DescriptionKey           KEY(mfo:Manufacturer,mfo:Field_Number,mfo:Description),DUP,NOCASE !By Description      
ManFieldKey              KEY(mfo:Manufacturer,mfo:Field),DUP,NOCASE !By Field            
HideFieldKey             KEY(mfo:NotAvailable,mfo:Manufacturer,mfo:Field_Number,mfo:Field),DUP,NOCASE !By Field            
HideDescriptionKey       KEY(mfo:NotAvailable,mfo:Manufacturer,mfo:Field_Number,mfo:Description),DUP,NOCASE !By Description      
RelatedDescriptionKey    KEY(mfo:Manufacturer,mfo:RelatedPartCode,mfo:Field_Number,mfo:Description),DUP,NOCASE !By Description      
HideRelatedFieldKey      KEY(mfo:NotAvailable,mfo:Manufacturer,mfo:RelatedPartCode,mfo:Field_Number,mfo:Field),DUP,NOCASE !By Field            
HideRelatedDescKey       KEY(mfo:NotAvailable,mfo:Manufacturer,mfo:RelatedPartCode,mfo:Field_Number,mfo:Description),DUP,NOCASE !By Description      
FieldNumberKey           KEY(mfo:Manufacturer,mfo:Field_Number),DUP,NOCASE !By Field Number     
PrimaryLookupKey         KEY(mfo:Manufacturer,mfo:Field_Number,mfo:PrimaryLookup),DUP,NOCASE !By Primary Lookup   
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
Manufacturer                STRING(30)                     !                    
Field_Number                REAL                           !                    
Field                       STRING(30)                     !                    
Description                 STRING(60)                     !                    
ImportanceLevel             LONG                           !Repair Index        
SkillLevel                  LONG                           !Skill Level         
RepairType                  STRING(30)                     !Repair Type         
RepairTypeWarranty          STRING(30)                     !Repair Type         
HideFromEngineer            BYTE                           !Hide From Engineer  
ForcePartCode               LONG                           !Force Related Part Code
RelatedPartCode             LONG                           !Related Part Fault Code
PromptForExchange           BYTE                           !                    
ExcludeFromBouncer          BYTE                           !Exclude From Bouncer Table
ReturnToRRC                 BYTE                           !Description         
RestrictLookup              BYTE                           !Restrict Lookup To  
RestrictLookupType          BYTE                           !Restrict Lookup Type
NotAvailable                BYTE                           !Not Available       
PrimaryLookup               BYTE                           !Primary Lookup      
SetJobFaultCode             BYTE                           !Set Job Fault Code  
SelectJobFaultCode          LONG                           !Select Job Fault Code
JobFaultCodeValue           STRING(30)                     !Job Fault Code Value
JobTypeAvailability         BYTE                           !Job Type Availability
                         END
                     END                       

JOBEXHIS             FILE,DRIVER('Btrieve'),NAME('JOBEXHIS.DAT'),PRE(jxh),CREATE,BINDABLE,THREAD !Job Exchange History
Ref_Number_Key           KEY(jxh:Ref_Number,jxh:Date,jxh:Time),DUP,NOCASE !By Date             
record_number_key        KEY(jxh:record_number),NOCASE,PRIMARY !                    
Record                   RECORD,PRE()
Ref_Number                  REAL                           !                    
record_number               REAL                           !                    
Loan_Unit_Number            REAL                           !                    
Date                        DATE                           !                    
Time                        TIME                           !                    
User                        STRING(3)                      !                    
Status                      STRING(60)                     !                    
                         END
                     END                       

STOPARTS             FILE,DRIVER('Btrieve'),OEM,NAME('STOPARTS.DAT'),PRE(spt),CREATE,BINDABLE,THREAD !Stock Part History  
RecordNumberKey          KEY(spt:RecordNumber),NOCASE,PRIMARY !By Record Number    
DateChangedKey           KEY(spt:STOCKRefNumber,spt:DateChanged,spt:RecordNumber),DUP,NOCASE !By Date Changed     
LocationDateKey          KEY(spt:Location,spt:DateChanged,spt:RecordNumber),DUP,NOCASE !By Date Changed     
LocationNewPartKey       KEY(spt:Location,spt:NewPartNumber,spt:NewDescription),DUP,NOCASE !By New Part Number  
LocationOldPartKey       KEY(spt:Location,spt:OldPartNumber,spt:OldDescription),DUP,NOCASE !By Old Part Number  
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
STOCKRefNumber              LONG                           !Link To STOCK Ref Number
Location                    STRING(30)                     !Location            
DateChanged                 DATE                           !Date Changed        
TimeChanged                 TIME                           !Time Changed        
UserCode                    STRING(3)                      !User Code           
OldPartNumber               STRING(30)                     !Old Part Number     
OldDescription              STRING(30)                     !Old Description     
NewPartNumber               STRING(30)                     !New Part Number     
NewDescription              STRING(30)                     !New Description     
Notes                       STRING(255)                    !Notes               
                         END
                     END                       

ORDPARTS             FILE,DRIVER('Btrieve'),NAME('ORDPARTS.DAT'),PRE(orp),CREATE,BINDABLE,THREAD !Orders - Parts Attached
Order_Number_Key         KEY(orp:Order_Number,orp:Part_Ref_Number),DUP,NOCASE !By Order Number     
record_number_key        KEY(orp:Record_Number),NOCASE,PRIMARY !                    
Ref_Number_Key           KEY(orp:Part_Ref_Number,orp:Part_Number,orp:Description),DUP,NOCASE !By Ref Number       
Part_Number_Key          KEY(orp:Order_Number,orp:Part_Number,orp:Description),DUP,NOCASE !By Part Number      
Description_Key          KEY(orp:Order_Number,orp:Description,orp:Part_Number),DUP,NOCASE !By Description      
Received_Part_Number_Key KEY(orp:All_Received,orp:Part_Number),DUP,NOCASE !By Part Number      
Account_Number_Key       KEY(orp:Account_Number,orp:Part_Type,orp:All_Received,orp:Part_Number),DUP,NOCASE !                    
Allocated_Key            KEY(orp:Part_Type,orp:All_Received,orp:Allocated_To_Sale,orp:Part_Number),DUP,NOCASE !                    
Allocated_Account_Key    KEY(orp:Part_Type,orp:All_Received,orp:Allocated_To_Sale,orp:Account_Number,orp:Part_Number),DUP,NOCASE !                    
Order_Type_Received      KEY(orp:Order_Number,orp:Part_Type,orp:All_Received,orp:Part_Number,orp:Description),DUP,NOCASE !By Part Number      
PartRecordNumberKey      KEY(orp:PartRecordNumber),DUP,NOCASE !By Record Number    
Record                   RECORD,PRE()
Order_Number                LONG                           !Order Number        
Record_Number               LONG                           !Record Number       
Part_Ref_Number             LONG                           !Part Ref Number     
Quantity                    LONG                           !Quantity            
Part_Number                 STRING(30)                     !                    
Description                 STRING(30)                     !                    
Purchase_Cost               REAL                           !                    
Sale_Cost                   REAL                           !                    
Retail_Cost                 REAL                           !                    
Job_Number                  LONG                           !Job Number          
Part_Type                   STRING(3)                      !                    
Number_Received             LONG                           !Number Received     
Date_Received               DATE                           !                    
All_Received                STRING(3)                      !                    
Allocated_To_Sale           STRING(3)                      !                    
Account_Number              STRING(15)                     !                    
DespatchNoteNumber          STRING(30)                     !Despatch Note Number
Reason                      STRING(255)                    !Reason              
PartRecordNumber            LONG                           !Part Record Number  
GRN_Number                  LONG                           !Goods Received Number
OrderedCurrency             STRING(30)                     !Currency On Ordering
OrderedDailyRate            REAL                           !Daily Rate On Ordering
OrderedDivideMultiply       STRING(1)                      !Ordered Divide Multiply
ReceivedCurrency            STRING(30)                     !Currency On Order Received
ReceivedDailyRate           REAL                           !Daily Rate On Order Received
ReceivedDivideMultiply      STRING(1)                      !Divide Multiply Order Received
FreeExchangeStock           BYTE                           !Free Exchange Stock 
TimeReceived                TIME                           !Time Received       
DatePriceCaptured           DATE                           !                    
TimePriceCaptured           TIME                           !                    
UncapturedGRNNumber         LONG                           !                    
                         END
                     END                       

LOCATION             FILE,DRIVER('Btrieve'),NAME('LOCATION.DAT'),PRE(loc),CREATE,BINDABLE,THREAD !Locations - Site    
RecordNumberKey          KEY(loc:RecordNumber),NOCASE,PRIMARY !By Record Number    
Location_Key             KEY(loc:Location),NOCASE          !By Location         
Main_Store_Key           KEY(loc:Main_Store,loc:Location),DUP,NOCASE !By Location         
ActiveLocationKey        KEY(loc:Active,loc:Location),DUP,NOCASE !By Location         
ActiveMainStoreKey       KEY(loc:Active,loc:Main_Store,loc:Location),DUP,NOCASE !By Location         
VirtualLocationKey       KEY(loc:VirtualSite,loc:Location),DUP,NOCASE !By Location         
VirtualMainStoreKey      KEY(loc:VirtualSite,loc:Main_Store,loc:Location),DUP,NOCASE !By Location         
FaultyLocationKey        KEY(loc:FaultyPartsLocation),DUP,NOCASE !By Location         
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
Location                    STRING(30)                     !                    
Main_Store                  STRING(3)                      !                    
Active                      BYTE                           !Active              
VirtualSite                 BYTE                           !Virtual Site        
Level1                      BYTE                           !Level 1             
Level2                      BYTE                           !Level 2             
Level3                      BYTE                           !Level 3             
InWarrantyMarkUp            REAL                           !In Warranty Markup  
OutWarrantyMarkUp           REAL                           !Out Warranty Markup 
UseRapidStock               BYTE                           !Use Rapid Stock Allocation
FaultyPartsLocation         BYTE                           !                    
                         END
                     END                       

STOMPFAU             FILE,DRIVER('Btrieve'),OEM,NAME('STOMPFAU.DAT'),PRE(stu),CREATE,BINDABLE,THREAD !Model Stock Part Fault Codes
RecordNumberKey          KEY(stu:RecordNumber),NOCASE,PRIMARY !By Record Number    
FieldKey                 KEY(stu:RefNumber,stu:FieldNumber,stu:Field),DUP,NOCASE !By Field            
DescriptionKey           KEY(stu:RefNumber,stu:FieldNumber,stu:Description),DUP,NOCASE !By Description      
ManufacturerFieldKey     KEY(stu:Manufacturer,stu:FieldNumber,stu:Field),DUP,NOCASE !By Field            
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Link to STOMODEL RecordNumber
Manufacturer                STRING(30)                     !Manufacturer        
FieldNumber                 LONG                           !Field Number        
Field                       STRING(30)                     !Field               
Description                 STRING(60)                     !                    
                         END
                     END                       

STOMJFAU             FILE,DRIVER('Btrieve'),OEM,NAME('STOMJFAU.DAT'),PRE(stj),CREATE,BINDABLE,THREAD !Model Stock Job Fault Codes
RecordNumberKey          KEY(stj:RecordNumber),NOCASE,PRIMARY !By Record Number    
FieldKey                 KEY(stj:RefNumber),DUP,NOCASE     !By Field            
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Link to STOMODEL RecordNumber
FaultCode1                  STRING(30)                     !Fault Code 1        
FaultCode2                  STRING(30)                     !Fault Code 2        
FaultCode3                  STRING(30)                     !Fault Code 3        
FaultCode4                  STRING(30)                     !Fault Code 4        
FaultCode5                  STRING(30)                     !Fault Code 5        
FaultCode6                  STRING(30)                     !Fault Code 6        
FaultCode7                  STRING(30)                     !Fault Code 7        
FaultCode8                  STRING(30)                     !Faul Code 8         
FaultCode9                  STRING(30)                     !FaultCode9          
FaultCode10                 STRING(255)                    !Fault Code 10       
FaultCode11                 STRING(255)                    !Fault Code 11       
FaultCode12                 STRING(255)                    !FaultCode12         
FaultCode13                 STRING(30)                     !FaultCode13         
FaultCode14                 STRING(30)                     !FaultCode14         
FaultCode15                 STRING(30)                     !FaultCode15         
FaultCode16                 STRING(30)                     !FaultCode16         
FaultCode17                 STRING(30)                     !FaultCode17         
FaultCode18                 STRING(30)                     !FaultCode18         
FaultCode19                 STRING(30)                     !FaultCode19         
FaultCode20                 STRING(30)                     !FaultCode20         
                         END
                     END                       

STOESN               FILE,DRIVER('Btrieve'),OEM,NAME('STOESN.DAT'),PRE(ste),CREATE,BINDABLE,THREAD !Stock Serial Number File
Record_Number_Key        KEY(ste:Record_Number),NOCASE,PRIMARY !By Record Number    
Sold_Key                 KEY(ste:Ref_Number,ste:Sold,ste:Serial_Number),DUP,NOCASE !By Serial Number    
Record                   RECORD,PRE()
Record_Number               REAL                           !                    
Ref_Number                  REAL                           !                    
Serial_Number               STRING(16)                     !                    
Sold                        STRING(3)                      !                    
                         END
                     END                       

SUPPLIER             FILE,DRIVER('Btrieve'),NAME('SUPPLIER.DAT'),PRE(sup),CREATE,BINDABLE,THREAD !Suppliers           
RecordNumberKey          KEY(sup:RecordNumber),NOCASE,PRIMARY !                    
AccountNumberKey         KEY(sup:Account_Number),DUP,NOCASE !By Account Number   
Company_Name_Key         KEY(sup:Company_Name),NOCASE      !By Company Name     
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
Company_Name                STRING(30)                     !                    
Postcode                    STRING(10)                     !                    
Address_Line1               STRING(30)                     !                    
Address_Line2               STRING(30)                     !                    
Address_Line3               STRING(30)                     !                    
Telephone_Number            STRING(15)                     !                    
Fax_Number                  STRING(15)                     !                    
EmailAddress                STRING(255)                    !Email Address       
Contact_Name                STRING(60)                     !                    
Account_Number              STRING(15)                     !                    
Order_Period                REAL                           !                    
Normal_Supply_Period        REAL                           !                    
Minimum_Order_Value         REAL                           !                    
HistoryUsage                LONG                           !History Usage       
Factor                      REAL                           !Multiply By Factor Of
Notes                       STRING(255)                    !                    
UseForeignCurrency          BYTE                           !                    
CurrencyCode                STRING(30)                     !                    
MOPSupplierNumber           STRING(30)                     !MOP Supplier Number 
MOPItemID                   STRING(30)                     !MOP Item ID         
UseFreeStock                BYTE                           !                    
EVO_GL_Acc_No               STRING(10)                     !                    
EVO_Vendor_Number           STRING(10)                     !                    
EVO_TaxExempt               BYTE                           !                    
EVO_Profit_Centre           STRING(30)                     !                    
EVO_Excluded                BYTE                           !                    
                         END
                     END                       

COUBUSHR             FILE,DRIVER('Btrieve'),OEM,NAME('COUBUSHR.DAT'),PRE(cbh),CREATE,BINDABLE,THREAD !Courier Hours Of Business Exceptions
RecordNumberKey          KEY(cbh:RecordNumber),NOCASE,PRIMARY !By Record Number    
TypeDateKey              KEY(cbh:Courier,cbh:TheDate),NOCASE !By Date             
EndTimeKey               KEY(cbh:Courier,cbh:TheDate,cbh:StartTime,cbh:EndTime),DUP,NOCASE !By End Time         
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
Courier                     STRING(30)                     !Courier             
TheDate                     DATE                           !Date                
ExceptionType               BYTE                           !Exception Type      
StartTime                   TIME                           !Start Time          
EndTime                     TIME                           !End Time            
                         END
                     END                       

SUPVALA              FILE,DRIVER('Btrieve'),OEM,NAME('SUPVALA.DAT'),PRE(suva),CREATE,BINDABLE,THREAD !Suppliers End Of Day Calcs A
RecordNumberKey          KEY(suva:RecordNumber),NOCASE,PRIMARY !                    
RunDateKey               KEY(suva:Supplier,suva:RunDate),DUP,NOCASE !By Run Date         
DateOnly                 KEY(suva:RunDate),DUP,NOCASE,OPT  !                    
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
Supplier                    STRING(30)                     !Supplier            
RunDate                     DATE                           !Date                
OldBackOrder                DATE                           !Date Of Oldest Back Order
OldOutOrder                 DATE                           !Date Of Oldest Outstanding Order
                         END
                     END                       

ESTPARTS             FILE,DRIVER('Btrieve'),NAME('ESTPARTS.DAT'),PRE(epr),CREATE,BINDABLE,THREAD !Estimated Parts On A Job
Part_Number_Key          KEY(epr:Ref_Number,epr:Part_Number),DUP,NOCASE !By Part Number      
record_number_key        KEY(epr:Record_Number),NOCASE,PRIMARY !                    
Part_Ref_Number2_Key     KEY(epr:Part_Ref_Number),DUP,NOCASE !By Part Ref Number  
Date_Ordered_Key         KEY(epr:Date_Ordered),DUP,NOCASE  !By Date Ordered     
Part_Ref_Number_Key      KEY(epr:Ref_Number,epr:Part_Ref_Number),DUP,NOCASE !By Part Ref Number  
Order_Number_Key         KEY(epr:Ref_Number,epr:Order_Number),DUP,NOCASE !                    
Supplier_Key             KEY(epr:Supplier),DUP,NOCASE      !                    
Order_Part_Key           KEY(epr:Ref_Number,epr:Order_Part_Number),DUP,NOCASE !By Order Part Number
PartAllocatedKey         KEY(epr:PartAllocated,epr:Part_Number),DUP,NOCASE !By Part Number      
StatusKey                KEY(epr:Status,epr:Part_Number),DUP,NOCASE !By Part Number      
AllocatedStatusKey       KEY(epr:PartAllocated,epr:Status,epr:Part_Number),DUP,NOCASE !By Part Number      
Record                   RECORD,PRE()
Ref_Number                  LONG                           !Ref Number          
Record_Number               LONG                           !Record Number       
Adjustment                  STRING(3)                      !                    
Part_Ref_Number             LONG                           !Part Ref Number     
Part_Number                 STRING(30)                     !                    
Description                 STRING(30)                     !                    
Supplier                    STRING(30)                     !                    
Purchase_Cost               REAL                           !                    
Sale_Cost                   REAL                           !                    
Retail_Cost                 REAL                           !                    
Quantity                    LONG                           !Quantity            
Warranty_Part               STRING(3)                      !                    
Exclude_From_Order          STRING(3)                      !                    
Despatch_Note_Number        STRING(30)                     !                    
Date_Ordered                DATE                           !                    
Pending_Ref_Number          LONG                           !Pending Ref Number  
Order_Number                LONG                           !Order Number        
Date_Received               DATE                           !                    
Order_Part_Number           LONG                           !Order Part Number   
Status_Date                 DATE                           !                    
Fault_Code1                 STRING(30)                     !                    
Fault_Code2                 STRING(30)                     !                    
Fault_Code3                 STRING(30)                     !                    
Fault_Code4                 STRING(30)                     !                    
Fault_Code5                 STRING(30)                     !                    
Fault_Code6                 STRING(30)                     !                    
Fault_Code7                 STRING(30)                     !                    
Fault_Code8                 STRING(30)                     !                    
Fault_Code9                 STRING(30)                     !                    
Fault_Code10                STRING(30)                     !                    
Fault_Code11                STRING(30)                     !                    
Fault_Code12                STRING(30)                     !                    
Fault_Codes_Checked         STRING(3)                      !                    
InvoicePart                 LONG                           !Link to Record Number of Invoice(Credit) Part
Credit                      STRING(3)                      !Has this part been credited?
Requested                   BYTE                           !Parts Requested For Order
UsedOnRepair                BYTE                           !Used On Repair      
PartAllocated               BYTE                           !Part Allocated      
Status                      STRING(3)                      !Status              
AveragePurchaseCost         REAL                           !Purchase Cost       
InWarrantyMarkup            LONG                           !% Markup            
OutWarrantyMarkup           LONG                           !% Markup            
RRCPurchaseCost             REAL                           !RRC Purchase Cost   
RRCSaleCost                 REAL                           !RRC Sale Cost       
RRCInWarrantyMarkup         LONG                           !RRC In Warranty Markup
RRCOutWarrantyMarkup        LONG                           !RRC Out Warranty Markup
RRCAveragePurchaseCost      REAL                           !RRC Average Purchase Cost
                         END
                     END                       

TRAHUBS              FILE,DRIVER('Btrieve'),OEM,NAME('TRAHUBS.DAT'),PRE(trh),CREATE,BINDABLE,THREAD !Trade Account Hubs  
RecordNumberKey          KEY(trh:RecordNumber),NOCASE,PRIMARY !By Record Number    
HubKey                   KEY(trh:TRADEACCAccountNumber,trh:Hub),NOCASE !By Hub              
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
TRADEACCAccountNumber       STRING(30)                     !Account Number      
Hub                         STRING(30)                     !Hub                 
                         END
                     END                       

TRAEMAIL             FILE,DRIVER('Btrieve'),OEM,NAME('TRAEMAIL.DAT'),PRE(tre),CREATE,BINDABLE,THREAD !Trade Account Email Recipients
RecordNumberKey          KEY(tre:RecordNumber),NOCASE,PRIMARY !By Record Number    
RecipientKey             KEY(tre:RefNumber,tre:RecipientType),DUP,NOCASE !By Recipient Type   
ContactNameKey           KEY(tre:RefNumber,tre:ContactName),DUP,NOCASE !By Contact Name     
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Ref Number          
RecipientType               STRING(30)                     !Recipient Type      
ContactName                 STRING(60)                     !Contact Name        
EmailAddress                STRING(255)                    !Email Address       
SendStatusEmails            BYTE                           !Send Status Emails  
SendReportEmails            BYTE                           !Send Report Emails  
                         END
                     END                       

SUPVALB              FILE,DRIVER('Btrieve'),OEM,NAME('SUPVALB.DAT'),PRE(suvb),CREATE,BINDABLE,THREAD !Suppliers End Of Date Calcs B
RecordNumberKey          KEY(suvb:RecordNumber),NOCASE,PRIMARY !By Record Number    
RunDateKey               KEY(suvb:Supplier,suvb:RunDate),DUP,NOCASE !By Run Date         
LocationKey              KEY(suvb:Supplier,suvb:RunDate,suvb:Location),DUP,NOCASE !By Location         
DateOnly                 KEY(suvb:RunDate),DUP,NOCASE,OPT  !                    
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
Supplier                    STRING(30)                     !Supplier            
RunDate                     DATE                           !Date                
Location                    STRING(30)                     !Location            
BackOrderValue              REAL                           !Total Cost Of All Oustanding Orders
                         END
                     END                       

SUBBUSHR             FILE,DRIVER('Btrieve'),OEM,NAME('SUBBUSHR.DAT'),PRE(sbh),CREATE,BINDABLE,THREAD !Sub Account Hours Of Business Exceptions
RecordNumberKey          KEY(sbh:RecordNumber),NOCASE,PRIMARY !By Record Number    
TypeDateKey              KEY(sbh:RefNumber,sbh:TheDate),NOCASE !By Date             
EndTimeKey               KEY(sbh:RefNumber,sbh:TheDate,sbh:StartTime,sbh:EndTime),DUP,NOCASE !By End Time         
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Link To Parent      
TheDate                     DATE                           !Date                
ExceptionType               BYTE                           !Exception Type      
StartTime                   TIME                           !Start Time          
EndTime                     TIME                           !End Time            
                         END
                     END                       

JOBS                 FILE,DRIVER('Btrieve'),NAME('JOBS.DAT'),PRE(job),BINDABLE,CREATE,THREAD !Jobs                
Ref_Number_Key           KEY(job:Ref_Number),NOCASE,PRIMARY !By Job Number       
Model_Unit_Key           KEY(job:Model_Number,job:Unit_Type),DUP,NOCASE !By Job Number       
EngCompKey               KEY(job:Engineer,job:Completed,job:Ref_Number),DUP,NOCASE !By Job Number       
EngWorkKey               KEY(job:Engineer,job:Workshop,job:Ref_Number),DUP,NOCASE !By Job Number       
Surname_Key              KEY(job:Surname),DUP,NOCASE       !By Surname          
MobileNumberKey          KEY(job:Mobile_Number),DUP,NOCASE !By Mobile Number    
ESN_Key                  KEY(job:ESN),DUP,NOCASE           !By E.S.N. / I.M.E.I.
MSN_Key                  KEY(job:MSN),DUP,NOCASE           !By M.S.N.           
AccountNumberKey         KEY(job:Account_Number,job:Ref_Number),DUP,NOCASE !By Account Number   
AccOrdNoKey              KEY(job:Account_Number,job:Order_Number),DUP,NOCASE !By Order Number     
Model_Number_Key         KEY(job:Model_Number),DUP,NOCASE  !By Model Number     
Engineer_Key             KEY(job:Engineer,job:Model_Number),DUP,NOCASE !By Engineer         
Date_Booked_Key          KEY(job:date_booked),DUP,NOCASE   !By Date Booked      
DateCompletedKey         KEY(job:Date_Completed),DUP,NOCASE !By Date Completed   
ModelCompKey             KEY(job:Model_Number,job:Date_Completed),DUP,NOCASE !By Completed Date   
By_Status                KEY(job:Current_Status,job:Ref_Number),DUP,NOCASE !By Job Number       
StatusLocKey             KEY(job:Current_Status,job:Location,job:Ref_Number),DUP,NOCASE !By Job Number       
Location_Key             KEY(job:Location,job:Ref_Number),DUP,NOCASE !By Location         
Third_Party_Key          KEY(job:Third_Party_Site,job:Third_Party_Printed,job:Ref_Number),DUP,NOCASE !By Third Party      
ThirdEsnKey              KEY(job:Third_Party_Site,job:Third_Party_Printed,job:ESN),DUP,NOCASE !By ESN              
ThirdMsnKey              KEY(job:Third_Party_Site,job:Third_Party_Printed,job:MSN),DUP,NOCASE !By MSN              
PriorityTypeKey          KEY(job:Job_Priority,job:Ref_Number),DUP,NOCASE !By Priority         
Unit_Type_Key            KEY(job:Unit_Type),DUP,NOCASE     !By Unit Type        
EDI_Key                  KEY(job:Manufacturer,job:EDI,job:EDI_Batch_Number,job:Ref_Number),DUP,NOCASE !By Job Number       
InvoiceNumberKey         KEY(job:Invoice_Number),DUP,NOCASE !By Invoice_Number   
WarInvoiceNoKey          KEY(job:Invoice_Number_Warranty),DUP,NOCASE !By Invoice Number   
Batch_Number_Key         KEY(job:Batch_Number,job:Ref_Number),DUP,NOCASE !By Job Number       
Batch_Status_Key         KEY(job:Batch_Number,job:Current_Status,job:Ref_Number),DUP,NOCASE !By Ref Number       
BatchModelNoKey          KEY(job:Batch_Number,job:Model_Number,job:Ref_Number),DUP,NOCASE !By Job Number       
BatchInvoicedKey         KEY(job:Batch_Number,job:Invoice_Number,job:Ref_Number),DUP,NOCASE !By Ref Number       
BatchCompKey             KEY(job:Batch_Number,job:Completed,job:Ref_Number),DUP,NOCASE !By Job Number       
ChaInvoiceKey            KEY(job:Chargeable_Job,job:Account_Number,job:Invoice_Number,job:Ref_Number),DUP,NOCASE !                    
InvoiceExceptKey         KEY(job:Invoice_Exception,job:Ref_Number),DUP,NOCASE !By Job Number       
ConsignmentNoKey         KEY(job:Consignment_Number),DUP,NOCASE !By Cosignment Number
InConsignKey             KEY(job:Incoming_Consignment_Number),DUP,NOCASE !By Consignment Number
ReadyToDespKey           KEY(job:Despatched,job:Ref_Number),DUP,NOCASE !By Job Number       
ReadyToTradeKey          KEY(job:Despatched,job:Account_Number,job:Ref_Number),DUP,NOCASE !By Job Number       
ReadyToCouKey            KEY(job:Despatched,job:Current_Courier,job:Ref_Number),DUP,NOCASE !By Job Number       
ReadyToAllKey            KEY(job:Despatched,job:Account_Number,job:Current_Courier,job:Ref_Number),DUP,NOCASE !By Job Number       
DespJobNumberKey         KEY(job:Despatch_Number,job:Ref_Number),DUP,NOCASE !By Job Number       
DateDespatchKey          KEY(job:Courier,job:Date_Despatched,job:Ref_Number),DUP,NOCASE !By Job Number       
DateDespLoaKey           KEY(job:Loan_Courier,job:Loan_Despatched,job:Ref_Number),DUP,NOCASE !By Job Number       
DateDespExcKey           KEY(job:Exchange_Courier,job:Exchange_Despatched,job:Ref_Number),DUP,NOCASE !By Job Number       
ChaRepTypeKey            KEY(job:Repair_Type),DUP,NOCASE   !By Chargeable Repair Type
WarRepTypeKey            KEY(job:Repair_Type_Warranty),DUP,NOCASE !By Warranty Repair Type
ChaTypeKey               KEY(job:Charge_Type),DUP,NOCASE   !                    
WarChaTypeKey            KEY(job:Warranty_Charge_Type),DUP,NOCASE !                    
Bouncer_Key              KEY(job:Bouncer,job:Ref_Number),DUP,NOCASE !By Job Number       
EngDateCompKey           KEY(job:Engineer,job:Date_Completed),DUP,NOCASE !By Date Completed   
ExcStatusKey             KEY(job:Exchange_Status,job:Ref_Number),DUP,NOCASE !By Job Number       
LoanStatusKey            KEY(job:Loan_Status,job:Ref_Number),DUP,NOCASE !By Job Number       
ExchangeLocKey           KEY(job:Exchange_Status,job:Location,job:Ref_Number),DUP,NOCASE !By Job Number       
LoanLocKey               KEY(job:Loan_Status,job:Location,job:Ref_Number),DUP,NOCASE !By Job Number       
BatchJobKey              KEY(job:InvoiceAccount,job:InvoiceBatch,job:Ref_Number),DUP,NOCASE !By Job Number       
BatchStatusKey           KEY(job:InvoiceAccount,job:InvoiceBatch,job:InvoiceStatus,job:Ref_Number),DUP,NOCASE !By Job Number       
Record                   RECORD,PRE()
Ref_Number                  LONG                           !                    
Batch_Number                LONG                           !                    
Internal_Status             STRING(10)                     !                    
Auto_Search                 STRING(30)                     !                    
who_booked                  STRING(3)                      !                    
date_booked                 DATE                           !                    
time_booked                 TIME                           !                    
Cancelled                   STRING(3)                      !                    
Bouncer                     STRING(8)                      !                    
Bouncer_Type                STRING(3)                      !Type Of Bouncer  CHArgeable / WARranty / BOTh
Web_Type                    STRING(3)                      !Warranty/Insurance/Chargeable
Warranty_Job                STRING(3)                      !                    
Chargeable_Job              STRING(3)                      !                    
Model_Number                STRING(30)                     !                    
Manufacturer                STRING(30)                     !                    
ESN                         STRING(20)                     !                    
MSN                         STRING(20)                     !                    
ProductCode                 STRING(30)                     !                    
Unit_Type                   STRING(30)                     !                    
Colour                      STRING(30)                     !                    
Location_Type               STRING(10)                     !                    
Phone_Lock                  STRING(30)                     !                    
Workshop                    STRING(3)                      !                    
Location                    STRING(30)                     !                    
Authority_Number            STRING(30)                     !                    
Insurance_Reference_Number  STRING(30)                     !                    
DOP                         DATE                           !                    
Insurance                   STRING(3)                      !                    
Insurance_Type              STRING(30)                     !                    
Transit_Type                STRING(30)                     !                    
Physical_Damage             STRING(3)                      !                    
Intermittent_Fault          STRING(3)                      !                    
Loan_Status                 STRING(30)                     !                    
Exchange_Status             STRING(30)                     !                    
Job_Priority                STRING(30)                     !                    
Charge_Type                 STRING(30)                     !                    
Warranty_Charge_Type        STRING(30)                     !                    
Current_Status              STRING(30)                     !                    
Account_Number              STRING(15)                     !                    
Trade_Account_Name          STRING(30)                     !                    
Department_Name             STRING(30)                     !                    
Order_Number                STRING(30)                     !                    
POP                         STRING(3)                      !                    
In_Repair                   STRING(3)                      !                    
Date_In_Repair              DATE                           !                    
Time_In_Repair              TIME                           !                    
On_Test                     STRING(3)                      !                    
Date_On_Test                DATE                           !                    
Time_On_Test                TIME                           !                    
Estimate_Ready              STRING(3)                      !                    
QA_Passed                   STRING(3)                      !                    
Date_QA_Passed              DATE                           !                    
Time_QA_Passed              TIME                           !                    
QA_Rejected                 STRING(3)                      !                    
Date_QA_Rejected            DATE                           !                    
Time_QA_Rejected            TIME                           !                    
QA_Second_Passed            STRING(3)                      !                    
Date_QA_Second_Passed       DATE                           !                    
Time_QA_Second_Passed       TIME                           !                    
Title                       STRING(4)                      !                    
Initial                     STRING(1)                      !                    
Surname                     STRING(30)                     !                    
Postcode                    STRING(10)                     !                    
Company_Name                STRING(30)                     !                    
Address_Line1               STRING(30)                     !                    
Address_Line2               STRING(30)                     !                    
Address_Line3               STRING(30)                     !                    
Telephone_Number            STRING(15)                     !                    
Fax_Number                  STRING(15)                     !                    
Mobile_Number               STRING(15)                     !                    
Postcode_Collection         STRING(10)                     !                    
Company_Name_Collection     STRING(30)                     !                    
Address_Line1_Collection    STRING(30)                     !                    
Address_Line2_Collection    STRING(30)                     !                    
Address_Line3_Collection    STRING(30)                     !                    
Telephone_Collection        STRING(15)                     !                    
Postcode_Delivery           STRING(10)                     !                    
Address_Line1_Delivery      STRING(30)                     !                    
Company_Name_Delivery       STRING(30)                     !                    
Address_Line2_Delivery      STRING(30)                     !                    
Address_Line3_Delivery      STRING(30)                     !                    
Telephone_Delivery          STRING(15)                     !                    
Date_Completed              DATE                           !                    
Time_Completed              TIME                           !                    
Completed                   STRING(3)                      !                    
Paid                        STRING(3)                      !                    
Paid_Warranty               STRING(3)                      !                    
Date_Paid                   DATE                           !                    
Repair_Type                 STRING(30)                     !                    
Repair_Type_Warranty        STRING(30)                     !                    
Engineer                    STRING(3)                      !                    
Ignore_Chargeable_Charges   STRING(3)                      !                    
Ignore_Warranty_Charges     STRING(3)                      !                    
Ignore_Estimate_Charges     STRING(3)                      !                    
Courier_Cost                REAL                           !                    
Advance_Payment             REAL                           !                    
Labour_Cost                 REAL                           !                    
Parts_Cost                  REAL                           !                    
Sub_Total                   REAL                           !                    
Courier_Cost_Estimate       REAL                           !                    
Labour_Cost_Estimate        REAL                           !                    
Parts_Cost_Estimate         REAL                           !                    
Sub_Total_Estimate          REAL                           !                    
Courier_Cost_Warranty       REAL                           !                    
Labour_Cost_Warranty        REAL                           !                    
Parts_Cost_Warranty         REAL                           !                    
Sub_Total_Warranty          REAL                           !                    
Loan_Issued_Date            DATE                           !                    
Loan_Unit_Number            LONG                           !                    
Loan_accessory              STRING(3)                      !                    
Loan_User                   STRING(3)                      !                    
Loan_Courier                STRING(30)                     !                    
Loan_Consignment_Number     STRING(30)                     !                    
Loan_Despatched             DATE                           !                    
Loan_Despatched_User        STRING(3)                      !                    
Loan_Despatch_Number        LONG                           !                    
LoaService                  STRING(1)                      !Loan Service For City Link / LabelG
Exchange_Unit_Number        LONG                           !                    
Exchange_Authorised         STRING(3)                      !                    
Loan_Authorised             STRING(3)                      !                    
Exchange_Accessory          STRING(3)                      !                    
Exchange_Issued_Date        DATE                           !                    
Exchange_User               STRING(3)                      !                    
Exchange_Courier            STRING(30)                     !                    
Exchange_Consignment_Number STRING(30)                     !                    
Exchange_Despatched         DATE                           !                    
Exchange_Despatched_User    STRING(3)                      !                    
Exchange_Despatch_Number    LONG                           !                    
ExcService                  STRING(1)                      !Exchange Service For City Link / Label G
Date_Despatched             DATE                           !                    
Despatch_Number             LONG                           !                    
Despatch_User               STRING(3)                      !                    
Courier                     STRING(30)                     !                    
Consignment_Number          STRING(30)                     !                    
Incoming_Courier            STRING(30)                     !                    
Incoming_Consignment_Number STRING(30)                     !                    
Incoming_Date               DATE                           !                    
Despatched                  STRING(3)                      !                    
Despatch_Type               STRING(3)                      !                    
Current_Courier             STRING(30)                     !                    
JobService                  STRING(1)                      !Job Service For City Link / Label G
Last_Repair_Days            REAL                           !                    
Third_Party_Site            STRING(30)                     !                    
Estimate                    STRING(3)                      !                    
Estimate_If_Over            REAL                           !                    
Estimate_Accepted           STRING(3)                      !                    
Estimate_Rejected           STRING(3)                      !                    
Third_Party_Printed         STRING(3)                      !                    
ThirdPartyDateDesp          DATE                           !                    
Fault_Code1                 STRING(30)                     !                    
Fault_Code2                 STRING(30)                     !                    
Fault_Code3                 STRING(30)                     !                    
Fault_Code4                 STRING(30)                     !                    
Fault_Code5                 STRING(30)                     !                    
Fault_Code6                 STRING(30)                     !                    
Fault_Code7                 STRING(30)                     !                    
Fault_Code8                 STRING(30)                     !                    
Fault_Code9                 STRING(30)                     !                    
Fault_Code10                STRING(255)                    !                    
Fault_Code11                STRING(255)                    !                    
Fault_Code12                STRING(255)                    !                    
PreviousStatus              STRING(30)                     !                    
StatusUser                  STRING(3)                      !User who last changed the status
Status_End_Date             DATE                           !                    
Status_End_Time             TIME                           !                    
Turnaround_End_Date         DATE                           !                    
Turnaround_End_Time         TIME                           !                    
Turnaround_Time             STRING(30)                     !                    
Special_Instructions        STRING(30)                     !                    
InvoiceAccount              STRING(15)                     !Invoice Batch Account Number
InvoiceStatus               STRING(3)                      !Proforma Status (UNA - Unathorised / AUT - Authorised / QUE - Query)
InvoiceBatch                LONG                           !Proforma Batch Number
InvoiceQuery                STRING(255)                    !Query Reason        
EDI                         STRING(3)                      !                    
EDI_Batch_Number            REAL                           !                    
Invoice_Exception           STRING(3)                      !                    
Invoice_Failure_Reason      STRING(80)                     !                    
Invoice_Number              LONG                           !                    
Invoice_Date                DATE                           !                    
Invoice_Date_Warranty       DATE                           !                    
Invoice_Courier_Cost        REAL                           !                    
Invoice_Labour_Cost         REAL                           !                    
Invoice_Parts_Cost          REAL                           !                    
Invoice_Sub_Total           REAL                           !                    
Invoice_Number_Warranty     LONG                           !                    
WInvoice_Courier_Cost       REAL                           !                    
WInvoice_Labour_Cost        REAL                           !                    
WInvoice_Parts_Cost         REAL                           !                    
WInvoice_Sub_Total          REAL                           !                    
                         END
                     END                       

MODELNUM             FILE,DRIVER('Btrieve'),NAME('MODELNUM.DAT'),PRE(mod),CREATE,BINDABLE,THREAD !Model Numbers       
Model_Number_Key         KEY(mod:Model_Number),NOCASE,PRIMARY !By Model Number     
Manufacturer_Key         KEY(mod:Manufacturer,mod:Model_Number),DUP,NOCASE !By Manufacturer     
Manufacturer_Unit_Type_Key KEY(mod:Manufacturer,mod:Unit_Type),DUP,NOCASE !By Unit Type        
SalesModelKey            KEY(mod:Manufacturer,mod:SalesModel),DUP,NOCASE !By Sales Model      
Record                   RECORD,PRE()
Model_Number                STRING(30)                     !                    
Manufacturer                STRING(30)                     !                    
Specify_Unit_Type           STRING(3)                      !                    
Product_Type                STRING(30)                     !                    
Unit_Type                   STRING(30)                     !                    
Nokia_Unit_Type             STRING(30)                     !                    
ESN_Length_From             REAL                           !                    
ESN_Length_To               REAL                           !                    
MSN_Length_From             REAL                           !                    
MSN_Length_To               REAL                           !                    
ExchangeUnitMinLevel        LONG                           !Exchange Unit Min Level
ReplacementValue            REAL                           !Replacement Value   
WarrantyRepairLimit         REAL                           !Warranty Repair Limit
LoanReplacementValue        REAL                           !Loan Replacement Value
ExcludedRRCRepair           BYTE                           !Excluded From RRC Repair
AllowIMEICharacters         BYTE                           !Allow IMEI Characters
UseReplenishmentProcess     BYTE                           !Use Replenishment Process
SalesModel                  STRING(30)                     !Sales Model         
ExcludeAutomaticRebookingProcess BYTE                      !Exclude Automatic Rebooking Process
OneYearWarrOnly             STRING(1)                      !                    
Active                      STRING(1)                      !                    
ExchReplaceValue            REAL                           !Exchange Replacement Value
RRCOrderCap                 LONG                           !                    
                         END
                     END                       

TRABUSHR             FILE,DRIVER('Btrieve'),OEM,NAME('TRABUSHR.DAT'),PRE(tbh),CREATE,BINDABLE,THREAD !Main Account Hours Of Business Exceptions
RecordNumberKey          KEY(tbh:RecordNumber),NOCASE,PRIMARY !By Record Number    
TypeDateKey              KEY(tbh:RefNumber,tbh:TheDate),NOCASE !By Date             
EndTimeKey               KEY(tbh:RefNumber,tbh:TheDate,tbh:StartTime,tbh:EndTime),DUP,NOCASE !By End Time         
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Link To Parent      
TheDate                     DATE                           !Date                
ExceptionType               BYTE                           !Exception Type      
StartTime                   TIME                           !Start Time          
EndTime                     TIME                           !End Time            
                         END
                     END                       

SUBEMAIL             FILE,DRIVER('Btrieve'),OEM,NAME('SUBEMAIL.DAT'),PRE(sue),CREATE,BINDABLE,THREAD !Sub Account Email Recipients
RecordNumberKey          KEY(sue:RecordNumber),NOCASE,PRIMARY !By Record Number    
RecipientTypeKey         KEY(sue:RefNumber,sue:RecipientType),DUP,NOCASE !By Recipient Type   
ContactNameKey           KEY(sue:RefNumber,sue:ContactName),DUP,NOCASE !By Contact Name     
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Ref Number          
RecipientType               STRING(30)                     !Recipient Type      
ContactName                 STRING(60)                     !Contact Name        
EmailAddress                STRING(255)                    !Email Address       
SendStatusEmails            BYTE                           !Send Status Emails  
SendReportEmails            BYTE                           !Send Report Emails  
                         END
                     END                       

ORDERS               FILE,DRIVER('Btrieve'),NAME('ORDERS.DAT'),PRE(ord),CREATE,BINDABLE,THREAD !Orders              
Order_Number_Key         KEY(ord:Order_Number),NOCASE,PRIMARY !By Order_Number     
Printed_Key              KEY(ord:Printed,ord:Order_Number),DUP,NOCASE !By Order Number     
Supplier_Printed_Key     KEY(ord:Printed,ord:Supplier,ord:Order_Number),DUP,NOCASE !By Order Number     
Received_Key             KEY(ord:All_Received,ord:Order_Number),DUP,NOCASE !                    
Supplier_Key             KEY(ord:Supplier,ord:Order_Number),DUP,NOCASE !By Order Number     
Supplier_Received_Key    KEY(ord:Supplier,ord:All_Received,ord:Order_Number),DUP,NOCASE !By Order Number     
DateKey                  KEY(ord:Date),DUP,NOCASE          !By Date             
NotPrintedOrderKey       KEY(ord:BatchRunNotPrinted,ord:Order_Number),DUP,NOCASE !By Order Number     
Record                   RECORD,PRE()
Order_Number                LONG                           !Order Number        
Supplier                    STRING(30)                     !Supplier Name       
Date                        DATE                           !Date Ordered        
Printed                     STRING(3)                      !Order Printed       
All_Received                STRING(3)                      !All Parts On Order Received
User                        STRING(3)                      !Order Created By    
OrderedCurrency             STRING(30)                     !Currency On Ordering
OrderedDailyRate            REAL                           !Daily Rate On Ordering
OrderedDivideMultiply       STRING(1)                      !Ordered Divide Multiply
BatchRunNotPrinted          BYTE                           !Order Not Printed As Part Of Batch Run
                         END
                     END                       

UNITTYPE             FILE,DRIVER('Btrieve'),NAME('UNITTYPE.DAT'),PRE(uni),CREATE,BINDABLE,THREAD !Unit Types          
Unit_Type_Key            KEY(uni:Unit_Type),NOCASE,PRIMARY !By Unit Type        
ActiveKey                KEY(uni:Active,uni:Unit_Type),DUP,NOCASE !By Unit Type        
Record                   RECORD,PRE()
Unit_Type                   STRING(30)                     !                    
Active                      BYTE                           !Active              
                         END
                     END                       

REPAIRTY             FILE,DRIVER('Btrieve'),NAME('REPAIRTY.DAT'),PRE(rep),CREATE,BINDABLE,THREAD !NOT USED            
Repair_Type_Key          KEY(rep:Manufacturer,rep:Model_Number,rep:Repair_Type),NOCASE,PRIMARY !By Repair Type      
Manufacturer_Key         KEY(rep:Manufacturer,rep:Repair_Type),DUP,NOCASE !By Repair Type      
Model_Number_Key         KEY(rep:Model_Number,rep:Repair_Type),DUP,NOCASE !By Repair Type      
Repair_Type_Only_Key     KEY(rep:Repair_Type),DUP,NOCASE   !By Repair Type      
Model_Chargeable_Key     KEY(rep:Model_Number,rep:Chargeable,rep:Repair_Type),DUP,NOCASE !By Repair Type      
Model_Warranty_Key       KEY(rep:Model_Number,rep:Warranty,rep:Repair_Type),DUP,NOCASE !By Repair Type      
Record                   RECORD,PRE()
Manufacturer                STRING(30)                     !                    
Model_Number                STRING(30)                     !                    
Repair_Type                 STRING(30)                     !                    
Chargeable                  STRING(3)                      !                    
Warranty                    STRING(3)                      !                    
CompFaultCoding             BYTE                           !Compulsory Fault Coding
ExcludeFromEDI              BYTE                           !Exclude From EDI    
ExcludeFromInvoicing        BYTE                           !Exclude From Invoicing
                         END
                     END                       

TRDACC               FILE,DRIVER('Btrieve'),OEM,NAME('TRDACC.DAT'),PRE(trr),CREATE,BINDABLE,THREAD !Third Party Returned Accessories
RecordNumberKey          KEY(trr:RecordNumber),NOCASE,PRIMARY !By Record Number    
AccessoryKey             KEY(trr:RefNumber,trr:Accessory),DUP,NOCASE !By Accessory        
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !TRDBATCH Record Number
EntryDate                   DATE                           !Entry Date          
EntryTime                   TIME                           !Entry Time          
Accessory                   STRING(30)                     !Accessory           
                         END
                     END                       

INVOICE              FILE,DRIVER('Btrieve'),NAME('INVOICE.DAT'),PRE(inv),CREATE,BINDABLE,THREAD !Invoices            
Invoice_Number_Key       KEY(inv:Invoice_Number),NOCASE,PRIMARY !By Invoice Number   
Invoice_Type_Key         KEY(inv:Invoice_Type,inv:Invoice_Number),DUP,NOCASE !By Invoice_Number   
Account_Number_Type_Key  KEY(inv:Invoice_Type,inv:Account_Number,inv:Invoice_Number),DUP,NOCASE !By Invoice Number   
Account_Number_Key       KEY(inv:Account_Number,inv:Invoice_Number),DUP,NOCASE !By Invoice Number   
Date_Created_Key         KEY(inv:Date_Created),DUP,NOCASE  !By Invoice Date     
Batch_Number_Key         KEY(inv:Manufacturer,inv:Batch_Number),DUP,NOCASE !By Batch Number     
OracleDateKey            KEY(inv:ExportedOracleDate,inv:Invoice_Number),DUP,NOCASE !By Invoice Number   
OracleNumberKey          KEY(inv:OracleNumber,inv:Invoice_Number),DUP,NOCASE !By Oracle Number    
Account_Date_Key         KEY(inv:Account_Number,inv:Date_Created),DUP,NOCASE !By Date Created     
RRCInvoiceDateKey        KEY(inv:RRCInvoiceDate),DUP,NOCASE !By RRC Invoice Date Key
ARCInvoiceDateKey        KEY(inv:ARCInvoiceDate),DUP,NOCASE !By ARC Invoice Date 
ReconciledDateKey        KEY(inv:Reconciled_Date),DUP,NOCASE !By Reconciled Date  
AccountReconciledKey     KEY(inv:Account_Number,inv:Reconciled_Date),DUP,NOCASE !By Reconciled Date  
Record                   RECORD,PRE()
Invoice_Number              REAL                           !                    
Invoice_Type                STRING(3)                      !                    
Job_Number                  REAL                           !                    
Date_Created                DATE                           !                    
Account_Number              STRING(15)                     !                    
AccountType                 STRING(3)                      !Main Or Sub (MAI/SUB)
Total                       REAL                           !                    
Vat_Rate_Labour             REAL                           !                    
Vat_Rate_Parts              REAL                           !                    
Vat_Rate_Retail             REAL                           !                    
VAT_Number                  STRING(30)                     !                    
Invoice_VAT_Number          STRING(30)                     !                    
Currency                    STRING(30)                     !                    
Batch_Number                REAL                           !                    
Manufacturer                STRING(30)                     !                    
Claim_Reference             STRING(30)                     !                    
Total_Claimed               REAL                           !                    
Courier_Paid                REAL                           !                    
Labour_Paid                 REAL                           !                    
Parts_Paid                  REAL                           !                    
Reconciled_Date             DATE                           !                    
jobs_count                  REAL                           !                    
PrevInvoiceNo               LONG                           !Link To Previous Invoice Number for Credit Notes
InvoiceCredit               STRING(3)                      !Invoice Type: Invoice or Credit
UseAlternativeAddress       BYTE                           !Use Alternative Address
EuroExhangeRate             REAL                           !Euro Exchange Rate  
RRCVatRateLabour            REAL                           !RRC Vat Rate Labour 
RRCVatRateParts             REAL                           !RRC Vat Rate Parts  
RRCVatRateRetail            REAL                           !RRC Vat Rate Retail 
ExportedRRCOracle           BYTE                           !Exported RRC To Oracle
ExportedARCOracle           BYTE                           !Exported ARC To Oracle
ExportedOracleDate          DATE                           !Exported Date       
OracleNumber                LONG                           !Oracle Number       
RRCInvoiceDate              DATE                           !RRC Invoice Date    
ARCInvoiceDate              DATE                           !ARC Invoice Date    
                         END
                     END                       

TRDBATCH             FILE,DRIVER('Btrieve'),OEM,NAME('TRDBATCH.DAT'),PRE(trb),CREATE,BINDABLE,THREAD !Third Party Batch Number
RecordNumberKey          KEY(trb:RecordNumber),NOCASE,PRIMARY !By Record Number    
Batch_Number_Key         KEY(trb:Company_Name,trb:Batch_Number),DUP,NOCASE !By Job Number       
Batch_Number_Status_Key  KEY(trb:Status,trb:Batch_Number),DUP,NOCASE !By Batch Number     
BatchOnlyKey             KEY(trb:Batch_Number),DUP,NOCASE  !By Batch Number     
Batch_Company_Status_Key KEY(trb:Status,trb:Company_Name,trb:Batch_Number),DUP,NOCASE !By Batch Number     
ESN_Only_Key             KEY(trb:ESN),DUP,NOCASE           !By ESN              
ESN_Status_Key           KEY(trb:Status,trb:ESN),DUP,NOCASE !By ESN              
Company_Batch_ESN_Key    KEY(trb:Company_Name,trb:Batch_Number,trb:ESN),DUP,NOCASE !By ESN              
AuthorisationKey         KEY(trb:AuthorisationNo),DUP,NOCASE !By Authorisation Number
StatusAuthKey            KEY(trb:Status,trb:AuthorisationNo),DUP,NOCASE !By Status           
CompanyDateKey           KEY(trb:Company_Name,trb:Status,trb:DateReturn),DUP,NOCASE !By Date             
JobStatusKey             KEY(trb:Status,trb:Ref_Number),DUP,NOCASE !By Job Number       
JobNumberKey             KEY(trb:Ref_Number),DUP,NOCASE    !By Job Number       
CompanyDespatchedKey     KEY(trb:Company_Name,trb:DateDespatched),DUP,NOCASE !By Despatch Date    
ReturnDateKey            KEY(trb:DateReturn),DUP,NOCASE    !By Return Date      
ReturnCompanyKey         KEY(trb:DateReturn,trb:Company_Name),DUP,NOCASE !By Company Name     
NotPrintedManJobKey      KEY(trb:BatchRunNotPrinted,trb:Status,trb:Company_Name,trb:Ref_Number),DUP,NOCASE !By Job Number       
PurchaseOrderKey         KEY(trb:PurchaseOrderNumber,trb:Ref_Number),DUP,NOCASE !By Job Number       
KeyEVO_Status            KEY(trb:EVO_Status),DUP,NOCASE    !                    
Record                   RECORD,PRE()
RecordNumber                LONG                           !                    
Batch_Number                LONG                           !                    
Company_Name                STRING(30)                     !                    
Ref_Number                  LONG                           !Job Number          
ESN                         STRING(20)                     !                    
Status                      STRING(3)                      !Out or In           
Date                        DATE                           !                    
Time                        TIME                           !                    
AuthorisationNo             STRING(30)                     !Authorisation Number for the Despatched Batch
Exchanged                   STRING(3)                      !Has the Job got an Exchange Unit attached
DateReturn                  DATE                           !Date The Unit Was Returned
TimeReturn                  TIME                           !Time Unit Returned  
TurnTime                    LONG                           !Turnaround Time     
MSN                         STRING(30)                     !M.S.N.              
DateDespatched              DATE                           !Date Despatched     
TimeDespatched              TIME                           !Time Despatched     
ReturnUser                  STRING(3)                      !Return Batch User Code
ReturnWaybillNo             STRING(30)                     !Return Waybill Number
ReturnRepairType            STRING(30)                     !Return Batch Repair Type
ReturnRejectedAmount        REAL                           !Rejected Amount     
ReturnRejectedReason        STRING(255)                    !Rejected Reason     
ThirdPartyInvoiceNo         STRING(30)                     !3rd Party Invoice Number
ThirdPartyInvoiceDate       DATE                           !3rd Party Invoice Date
ThirdPartyInvoiceCharge     REAL                           !3rd Party Invoice Charge
ThirdPartyVAT               REAL                           !3rd Party V.A.T.    
ThirdPartyMarkup            REAL                           !3rd Party Markup    
Manufacturer                STRING(30)                     !Manufacturer        
ModelNumber                 STRING(30)                     !Model Number        
BatchRunNotPrinted          BYTE                           !Not Printed As Part Of Batch Run
PurchaseOrderNumber         LONG                           !Oracle Purchase Order Number
NewThirdPartySite           STRING(30)                     !New Third Party Site
NewThirdPartySiteID         STRING(30)                     !New Third Party Site ID
EVO_Status                  STRING(1)                      !                    
                         END
                     END                       

TRACHRGE             FILE,DRIVER('Btrieve'),NAME('TRACHRGE.DAT'),PRE(trc),CREATE,BINDABLE,THREAD !Trade Account Charges
Account_Charge_Key       KEY(trc:Account_Number,trc:Model_Number,trc:Charge_Type,trc:Unit_Type,trc:Repair_Type),NOCASE,PRIMARY !By Charge Type      
Model_Repair_Key         KEY(trc:Model_Number,trc:Repair_Type),DUP,NOCASE !                    
Charge_Type_Key          KEY(trc:Account_Number,trc:Model_Number,trc:Charge_Type),DUP,NOCASE !By Charge Type      
Unit_Type_Key            KEY(trc:Account_Number,trc:Model_Number,trc:Unit_Type),DUP,NOCASE !By Unit Type        
Repair_Type_Key          KEY(trc:Account_Number,trc:Model_Number,trc:Repair_Type),DUP,NOCASE !By Repair Type      
Cost_Key                 KEY(trc:Account_Number,trc:Model_Number,trc:Cost),DUP,NOCASE !By Cost             
Charge_Type_Only_Key     KEY(trc:Charge_Type),DUP,NOCASE   !                    
Repair_Type_Only_Key     KEY(trc:Repair_Type),DUP,NOCASE   !                    
Unit_Type_Only_Key       KEY(trc:Unit_Type),DUP,NOCASE     !                    
Record                   RECORD,PRE()
Account_Number              STRING(15)                     !                    
Charge_Type                 STRING(30)                     !                    
Unit_Type                   STRING(30)                     !                    
Model_Number                STRING(30)                     !                    
Repair_Type                 STRING(30)                     !                    
Cost                        REAL                           !                    
dummy                       BYTE                           !                    
WarrantyClaimRate           REAL                           !Warranty Claim Rate 
HandlingFee                 REAL                           !Handling Fee        
Exchange                    REAL                           !Exchange            
RRCRate                     REAL                           !R.R.C. Rate         
                         END
                     END                       

TRDMODEL             FILE,DRIVER('Btrieve'),NAME('TRDMODEL.DAT'),PRE(trm),CREATE,BINDABLE,THREAD !Third Party Models  
Model_Number_Key         KEY(trm:Company_Name,trm:Model_Number),NOCASE,PRIMARY !By Model Number     
Model_Number_Only_Key    KEY(trm:Model_Number),DUP,NOCASE  !By Model Number     
Record                   RECORD,PRE()
Company_Name                STRING(30)                     !                    
Model_Number                STRING(30)                     !                    
                         END
                     END                       

STOMODEL             FILE,DRIVER('Btrieve'),NAME('STOMODEL.DAT'),PRE(stm),CREATE,BINDABLE,THREAD !Stock Model Numbers 
RecordNumberKey          KEY(stm:RecordNumber),NOCASE,PRIMARY !By Record Number    
Model_Number_Key         KEY(stm:Ref_Number,stm:Manufacturer,stm:Model_Number),NOCASE !By Model Number     
Model_Part_Number_Key    KEY(stm:Accessory,stm:Model_Number,stm:Location,stm:Part_Number),DUP,NOCASE !By Model Number     
Description_Key          KEY(stm:Accessory,stm:Model_Number,stm:Location,stm:Description),DUP,NOCASE !By Description      
Manufacturer_Key         KEY(stm:Manufacturer,stm:Model_Number),DUP,NOCASE !By Manufacturer     
Ref_Part_Description     KEY(stm:Ref_Number,stm:Part_Number,stm:Location,stm:Description),DUP,NOCASE !Why?                
Location_Part_Number_Key KEY(stm:Model_Number,stm:Location,stm:Part_Number),DUP,NOCASE !                    
Location_Description_Key KEY(stm:Model_Number,stm:Location,stm:Description),DUP,NOCASE !                    
Mode_Number_Only_Key     KEY(stm:Ref_Number,stm:Model_Number),DUP,NOCASE !By Model Number     
Record                   RECORD,PRE()
Ref_Number                  LONG                           !                    
Manufacturer                STRING(30)                     !                    
Model_Number                STRING(30)                     !                    
Part_Number                 STRING(30)                     !                    
Description                 STRING(30)                     !                    
Location                    STRING(30)                     !                    
Accessory                   STRING(3)                      !                    
FaultCode1                  STRING(30)                     !Fault Code 1        
FaultCode2                  STRING(30)                     !Fault Code 2        
FaultCode3                  STRING(30)                     !Fault Code 3        
FaultCode4                  STRING(30)                     !Fault Code 4        
FaultCode5                  STRING(30)                     !Fault Code 5        
FaultCode6                  STRING(30)                     !Fault Code 6        
FaultCode7                  STRING(30)                     !Fault Code 7        
FaultCode8                  STRING(30)                     !Fault Code 8        
FaultCode9                  STRING(30)                     !Fault Code 9        
FaultCode10                 STRING(30)                     !Fault Code 10       
FaultCode11                 STRING(30)                     !Fault Code 11       
FaultCode12                 STRING(30)                     !Fault Code 12       
RecordNumber                LONG                           !Record Number       
                         END
                     END                       

TRADEACC             FILE,DRIVER('Btrieve'),NAME('TRADEACC.DAT'),PRE(tra),CREATE,BINDABLE,THREAD !Trade Accounts      
RecordNumberKey          KEY(tra:RecordNumber),NOCASE,PRIMARY !By Record Number    
Account_Number_Key       KEY(tra:Account_Number),NOCASE    !By Account Number   
Company_Name_Key         KEY(tra:Company_Name),DUP,NOCASE  !By Company Name     
ReplicateFromKey         KEY(tra:ReplicateAccount,tra:Account_Number),DUP,NOCASE !By Account Number   
StoresAccountKey         KEY(tra:StoresAccount),DUP,NOCASE !By Stores Account   
SiteLocationKey          KEY(tra:SiteLocation),DUP,NOCASE  !By Site Location    
RegionKey                KEY(tra:Region),DUP,NOCASE        !By Region           
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
Account_Number              STRING(15)                     !                    
Postcode                    STRING(15)                     !                    
Company_Name                STRING(30)                     !                    
Address_Line1               STRING(30)                     !                    
Address_Line2               STRING(30)                     !                    
Address_Line3               STRING(30)                     !                    
Telephone_Number            STRING(15)                     !                    
Fax_Number                  STRING(15)                     !                    
EmailAddress                STRING(255)                    !Email Address       
Contact_Name                STRING(30)                     !                    
Labour_Discount_Code        STRING(2)                      !                    
Retail_Discount_Code        STRING(2)                      !                    
Parts_Discount_Code         STRING(2)                      !                    
Labour_VAT_Code             STRING(2)                      !                    
Parts_VAT_Code              STRING(2)                      !                    
Retail_VAT_Code             STRING(2)                      !                    
Account_Type                STRING(6)                      !                    
Credit_Limit                REAL                           !                    
Account_Balance             REAL                           !                    
Use_Sub_Accounts            STRING(3)                      !                    
Invoice_Sub_Accounts        STRING(3)                      !                    
Stop_Account                STRING(3)                      !                    
Allow_Cash_Sales            STRING(3)                      !                    
Price_Despatch              STRING(3)                      !                    
Price_First_Copy_Only       BYTE                           !Display pricing details on first copy of despatch note
Num_Despatch_Note_Copies    LONG                           !Number of despatch notes to print
Price_Retail_Despatch       STRING(3)                      !                    
Print_Despatch_Complete     STRING(3)                      !Print Despatch Note At Completion
Print_Despatch_Despatch     STRING(3)                      !Print Despatch Note At Despatch
Standard_Repair_Type        STRING(15)                     !                    
Courier_Incoming            STRING(30)                     !                    
Courier_Outgoing            STRING(30)                     !                    
Use_Contact_Name            STRING(3)                      !                    
VAT_Number                  STRING(30)                     !                    
Use_Delivery_Address        STRING(3)                      !                    
Use_Collection_Address      STRING(3)                      !                    
UseCustDespAdd              STRING(3)                      !Use Customer Address As Despatch Address
Allow_Loan                  STRING(3)                      !                    
Allow_Exchange              STRING(3)                      !                    
Force_Fault_Codes           STRING(3)                      !                    
Despatch_Invoiced_Jobs      STRING(3)                      !                    
Despatch_Paid_Jobs          STRING(3)                      !                    
Print_Despatch_Notes        STRING(3)                      !                    
Print_Retail_Despatch_Note  STRING(3)                      !                    
Print_Retail_Picking_Note   STRING(3)                      !                    
Despatch_Note_Per_Item      STRING(3)                      !                    
Skip_Despatch               STRING(3)                      !Use Batch Despatch  
IgnoreDespatch              STRING(3)                      !Do not use Despatch Table
Summary_Despatch_Notes      STRING(3)                      !                    
Exchange_Stock_Type         STRING(30)                     !                    
Loan_Stock_Type             STRING(30)                     !                    
Courier_Cost                REAL                           !                    
Force_Estimate              STRING(3)                      !                    
Estimate_If_Over            REAL                           !                    
Turnaround_Time             STRING(30)                     !                    
Password                    STRING(20)                     !                    
Retail_Payment_Type         STRING(3)                      !                    
Retail_Price_Structure      STRING(3)                      !                    
Use_Customer_Address        STRING(3)                      !                    
Invoice_Customer_Address    STRING(3)                      !                    
ZeroChargeable              STRING(3)                      !Don't show Chargeable Costs
RefurbCharge                STRING(3)                      !                    
ChargeType                  STRING(30)                     !                    
WarChargeType               STRING(30)                     !                    
ExchangeAcc                 STRING(3)                      !Account used for repairing exchange units
MultiInvoice                STRING(3)                      !Invoice Type (SIN - Single / SIM - Single (Summary) / MUL - Multiple / BAT - Batch)
EDIInvoice                  STRING(3)                      !Will the Proforma Invoice be Exported?
ExportPath                  STRING(255)                    !For ProForma Export 
ImportPath                  STRING(255)                    !For ProForma Export 
BatchNumber                 LONG                           !ProForma Batch Number
ExcludeBouncer              BYTE                           !Exclude From Bouncer
RetailZeroParts             BYTE                           !Zero Value Parts    
HideDespAdd                 BYTE                           !Hide Address On Despatch Note
EuroApplies                 BYTE                           !Apply Euro          
ForceCommonFault            BYTE                           !Force Common Fault  
ForceOrderNumber            BYTE                           !Force Order Number  
ShowRepairTypes             BYTE                           !Repair Type Display 
WebMemo                     STRING(255)                    !Web Memo            
WebPassword1                STRING(30)                     !Web Password1       
WebPassword2                STRING(30)                     !WebPassword2        
InvoiceAtDespatch           BYTE                           !Print Invoice At Despatch
InvoiceType                 BYTE                           !Invoice Type        
IndividualSummary           BYTE                           !Print Individual Summary Report At Despatch
UseTradeContactNo           BYTE                           !Use Trade Contact No
StopThirdParty              BYTE                           !Stop Unit For Going To Third Party
E1                          BYTE                           !E1                  
E2                          BYTE                           !E2                  
E3                          BYTE                           !E3                  
UseDespatchDetails          BYTE                           !Use Alternative Contact Nos On Despatch Note
AltTelephoneNumber          STRING(30)                     !Telephone Number    
AltFaxNumber                STRING(30)                     !Fax Number          
AltEmailAddress             STRING(255)                    !Email Address       
AutoSendStatusEmails        BYTE                           !Auto Send Status Emails
EmailRecipientList          BYTE                           !Email Recipient List
EmailEndUser                BYTE                           !Email End User      
ForceEndUserName            BYTE                           !Force End User Name 
ChangeInvAddress            BYTE                           !Invoice Address     
ChangeCollAddress           BYTE                           !Collection Address  
ChangeDelAddress            BYTE                           !Delivery Address    
AllowMaximumDiscount        BYTE                           !Set Maximum Discount Level
MaximumDiscount             REAL                           !Maximum Discount    
SetInvoicedJobStatus        BYTE                           !Set Completed Status
SetDespatchJobStatus        BYTE                           !Set Completed Status
InvoicedJobStatus           STRING(30)                     !Completed Status    
DespatchedJobStatus         STRING(30)                     !Completed Status    
CCommissionInHouse          LONG                           !Chargeable          
WCommissionInHouse          LONG                           !Warranty            
CCommissionOutSource        LONG                           !Chargeable          
WCommissionOutSource        LONG                           !Warranty            
ReplicateAccount            STRING(30)                     !Account Replicated From
RemoteRepairCentre          BYTE                           !Remote Repair Centre
SiteLocation                STRING(30)                     !Site Location       
RRCFactor                   LONG                           !R.R.C. Chargeable (%)
ARCFactor                   LONG                           !A.R.C. Chargeable (%)
TransitType                 STRING(30)                     !Transit Type        
ForceMobileNumber           BYTE                           !Force Mobile Number 
BranchIdentification        STRING(2)                      !                    
AllocateQAEng               BYTE                           !                    
InvoiceAtCompletion         BYTE                           !Invoice At Completion
InvoiceTypeComplete         BYTE                           !Invoice Type        
UseTimingsFrom              STRING(30)                     !Use Timings From    
StartWorkHours              TIME                           !Start Work Hours    
EndWorkHours                TIME                           !End Work Hours      
IncludeSaturday             STRING(3)                      !Include Saturday    
IncludeSunday               STRING(3)                      !Include Sunday      
StoresAccount               STRING(30)                     !Stores Account      
RepairEngineerQA            BYTE                           !Repair Engineer To QA Job
SecondYearAccount           BYTE                           !Second Year Warranty Account (True/False)
Activate48Hour              BYTE                           !Activate 48 Hour Exchange Process
SatStartWorkHours           TIME                           !Start Time          
SatEndWorkHours             TIME                           !End Time            
SunStartWorkHours           TIME                           !Start Time          
SunEndWorkHours             TIME                           !End Time            
Line500AccountNumber        STRING(30)                     !Line 500 Account Number
CompanyOwned                BYTE                           !Company Ownder Franchise
OverrideMobileDefault       BYTE                           !Override Mandatory Mobile Number
Region                      STRING(30)                     !Region              
AllowCreditNotes            BYTE                           !Allow Credit Notes  
DoMSISDNCheck               BYTE                           !Do MSISDN Check     
Hub                         STRING(30)                     !Hub                 
UseSBOnline                 BYTE                           !Use SB Online       
IgnoreReplenishmentProcess  BYTE                           !Ignore Replenishment Process
VCPWaybillPrefix            STRING(30)                     !VCP Waybill Prefix  
RRCWaybillPrefix            STRING(30)                     !RRC Waybill Prefix  
OBFCompanyName              STRING(30)                     !                    
OBFAddress1                 STRING(30)                     !                    
OBFAddress2                 STRING(30)                     !                    
OBFSuburb                   STRING(30)                     !                    
OBFContactName              STRING(30)                     !                    
OBFContactNumber            STRING(15)                     !                    
OBFEmailAddress             STRING(255)                    !                    
SBOnlineJobProgress         BYTE                           !Job Progress        
coTradingName               STRING(40)                     !                    
coTradingName2              STRING(40)                     !                    
coLocation                  STRING(40)                     !                    
coRegistrationNo            STRING(30)                     !                    
coVATNumber                 STRING(30)                     !                    
coAddressLine1              STRING(40)                     !                    
coAddressLine2              STRING(40)                     !                    
coAddressLine3              STRING(40)                     !                    
coAddressLine4              STRING(40)                     !                    
coTelephoneNumber           STRING(30)                     !                    
coFaxNumber                 STRING(30)                     !                    
coEmailAddress              STRING(255)                    !                    
RtnExchangeAccount          STRING(30)                     !                    
                         END
                     END                       

SUBCHRGE             FILE,DRIVER('Btrieve'),NAME('SUBCHRGE.DAT'),PRE(suc),CREATE,BINDABLE,THREAD !Sub Trade Account Charges
Model_Repair_Type_Key    KEY(suc:Account_Number,suc:Model_Number,suc:Charge_Type,suc:Unit_Type,suc:Repair_Type),NOCASE,PRIMARY !By Repair Type      
Account_Charge_Key       KEY(suc:Account_Number,suc:Model_Number,suc:Charge_Type),DUP,NOCASE !By Charge Type      
Unit_Type_Key            KEY(suc:Account_Number,suc:Model_Number,suc:Unit_Type),DUP,NOCASE !By Unit Type        
Repair_Type_Key          KEY(suc:Account_Number,suc:Model_Number,suc:Repair_Type),DUP,NOCASE !By Repair Type      
Cost_Key                 KEY(suc:Account_Number,suc:Model_Number,suc:Cost),DUP,NOCASE !By Cost             
Charge_Type_Only_Key     KEY(suc:Charge_Type),DUP,NOCASE   !                    
Repair_Type_Only_Key     KEY(suc:Repair_Type),DUP,NOCASE   !                    
Unit_Type_Only_Key       KEY(suc:Unit_Type),DUP,NOCASE     !                    
Model_Repair_Key         KEY(suc:Model_Number,suc:Repair_Type),DUP,NOCASE !                    
Record                   RECORD,PRE()
Account_Number              STRING(15)                     !                    
Charge_Type                 STRING(30)                     !                    
Unit_Type                   STRING(30)                     !                    
Model_Number                STRING(30)                     !                    
Repair_Type                 STRING(30)                     !                    
Cost                        REAL                           !                    
dummy                       BYTE                           !                    
WarrantyClaimRate           REAL                           !Warranty Claim Rate 
HandlingFee                 REAL                           !Handling Fee        
Exchange                    REAL                           !Exchange            
RRCRate                     REAL                           !R.R.C. Rate         
                         END
                     END                       

DEFCHRGE             FILE,DRIVER('Btrieve'),NAME('DEFCHRGE.DAT'),PRE(dec),CREATE,BINDABLE,THREAD !Default Charges     
Charge_Type_Key          KEY(dec:Charge_Type),NOCASE,PRIMARY !By Charge Type      
Repair_Type_Only_Key     KEY(dec:Repair_Type),DUP,NOCASE   !                    
Unit_Type_Only_Key       KEY(dec:Unit_Type),DUP,NOCASE     !                    
Record                   RECORD,PRE()
Charge_Type                 STRING(30)                     !                    
Unit_Type                   STRING(30)                     !                    
Model_Number                STRING(30)                     !                    
Repair_Type                 STRING(30)                     !                    
Cost                        REAL                           !                    
                         END
                     END                       

SUBTRACC             FILE,DRIVER('Btrieve'),NAME('SUBTRACC.DAT'),PRE(sub),CREATE,BINDABLE,THREAD !Sub Trade Accounts  
RecordNumberKey          KEY(sub:RecordNumber),NOCASE,PRIMARY !By Record Number    
Main_Account_Key         KEY(sub:Main_Account_Number,sub:Account_Number),NOCASE !By Account Number   
Main_Name_Key            KEY(sub:Main_Account_Number,sub:Company_Name),DUP,NOCASE !By Company Name     
Account_Number_Key       KEY(sub:Account_Number),NOCASE    !By Account Number   
Branch_Key               KEY(sub:Branch),DUP,NOCASE        !By Branch           
Main_Branch_Key          KEY(sub:Main_Account_Number,sub:Branch),DUP,NOCASE !By Branch           
Company_Name_Key         KEY(sub:Company_Name),DUP,NOCASE  !By Company Name     
ReplicateFromKey         KEY(sub:ReplicateAccount,sub:Account_Number),DUP,NOCASE !By Account Number   
GenericAccountKey        KEY(sub:Generic_Account,sub:Account_Number),DUP,NOCASE !                    
GenericCompanyKey        KEY(sub:Generic_Account,sub:Company_Name),DUP,NOCASE !                    
GenericBranchKey         KEY(sub:Generic_Account,sub:Branch),DUP,NOCASE !                    
RegionKey                KEY(sub:Region),DUP,NOCASE        !By Region           
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
Main_Account_Number         STRING(15)                     !                    
Account_Number              STRING(15)                     !                    
Postcode                    STRING(15)                     !                    
Company_Name                STRING(30)                     !                    
Address_Line1               STRING(30)                     !                    
Address_Line2               STRING(30)                     !                    
Address_Line3               STRING(30)                     !                    
Telephone_Number            STRING(15)                     !                    
Fax_Number                  STRING(15)                     !                    
EmailAddress                STRING(255)                    !Email Address       
Branch                      STRING(30)                     !                    
Contact_Name                STRING(30)                     !                    
Enquiry_Source              STRING(30)                     !                    
Labour_Discount_Code        STRING(2)                      !                    
Retail_Discount_Code        STRING(2)                      !                    
Parts_Discount_Code         STRING(2)                      !                    
Labour_VAT_Code             STRING(2)                      !                    
Retail_VAT_Code             STRING(2)                      !                    
Parts_VAT_Code              STRING(2)                      !                    
Account_Type                STRING(6)                      !                    
Credit_Limit                REAL                           !                    
Account_Balance             REAL                           !                    
Stop_Account                STRING(3)                      !                    
Allow_Cash_Sales            STRING(3)                      !                    
Use_Delivery_Address        STRING(3)                      !                    
Use_Collection_Address      STRING(3)                      !                    
UseCustDespAdd              STRING(3)                      !Use Customer Address As Despatch Address
Courier_Incoming            STRING(30)                     !                    
Courier_Outgoing            STRING(30)                     !                    
VAT_Number                  STRING(30)                     !                    
Despatch_Invoiced_Jobs      STRING(3)                      !                    
Despatch_Paid_Jobs          STRING(3)                      !                    
Print_Despatch_Notes        STRING(3)                      !                    
PriceDespatchNotes          BYTE                           !Price Despatch Notes
Price_First_Copy_Only       BYTE                           !Display pricing details on first copy of despatch note
Num_Despatch_Note_Copies    LONG                           !Number of despatch notes to print
Print_Despatch_Complete     STRING(3)                      !Print Despatch Note At Completion
Print_Despatch_Despatch     STRING(3)                      !Print Despatch Note At Despatch
Print_Retail_Despatch_Note  STRING(3)                      !                    
Print_Retail_Picking_Note   STRING(3)                      !                    
Despatch_Note_Per_Item      STRING(3)                      !                    
Summary_Despatch_Notes      STRING(3)                      !                    
Courier_Cost                REAL                           !                    
Password                    STRING(20)                     !                    
Retail_Payment_Type         STRING(3)                      !                    
Retail_Price_Structure      STRING(3)                      !                    
Use_Customer_Address        STRING(3)                      !                    
Invoice_Customer_Address    STRING(3)                      !                    
ZeroChargeable              STRING(3)                      !Don't show Chargeable Costs
MultiInvoice                STRING(3)                      !Invoice Type (SIN - Single / SIM - Single (Summary) / MUL - Multiple / BAT - Batch)
EDIInvoice                  STRING(3)                      !Will the Proforma Invoice be Exported?
ExportPath                  STRING(255)                    !For ProForma Export 
ImportPath                  STRING(255)                    !For ProForma Export 
BatchNumber                 LONG                           !ProForma Batch Number
HideDespAdd                 BYTE                           !Hide Address On Despatch Note
EuroApplies                 BYTE                           !Apply Euro          
ForceCommonFault            BYTE                           !Force Common Fault  
ForceOrderNumber            BYTE                           !Force Order Number  
WebPassword1                STRING(30)                     !Web Password1       
WebPassword2                STRING(30)                     !WebPassword2        
InvoiceAtDespatch           BYTE                           !Print Invoice At Despatch
InvoiceType                 BYTE                           !Invoice Type        
IndividualSummary           BYTE                           !Print Individual Summary Report At Despatch
UseTradeContactNo           BYTE                           !Use Trade Contact No
E1                          BYTE                           !E1                  
E2                          BYTE                           !E2                  
E3                          BYTE                           !E3                  
UseDespatchDetails          BYTE                           !Use Alternative Contact Nos On Despatch Note
AltTelephoneNumber          STRING(30)                     !Telephone Number    
AltFaxNumber                STRING(30)                     !Fax Number          
AltEmailAddress             STRING(255)                    !Email Address       
UseAlternativeAdd           BYTE                           !Use Alternative Delivery Addresses
AutoSendStatusEmails        BYTE                           !Auto Send Status Emails
EmailRecipientList          BYTE                           !Email Recipient List
EmailEndUser                BYTE                           !Email End User      
ForceEndUserName            BYTE                           !Force End User Name 
ChangeInvAddress            BYTE                           !Invoice Address     
ChangeCollAddress           BYTE                           !Collection Address  
ChangeDelAddress            BYTE                           !Delivery Address    
AllowMaximumDiscount        BYTE                           !Set Maximum Discount Level
MaximumDiscount             REAL                           !Maximum Discount    
SetInvoicedJobStatus        BYTE                           !Set Completed Status
SetDespatchJobStatus        BYTE                           !Set Completed Status
InvoicedJobStatus           STRING(30)                     !Completed Status    
DespatchedJobStatus         STRING(30)                     !Completed Status    
ReplicateAccount            STRING(30)                     !Account Replicated From
FactorAccount               BYTE                           !Factor Account      
ForceEstimate               BYTE                           !Force Estimate      
EstimateIfOver              REAL                           !Estimate If Over    
InvoiceAtCompletion         BYTE                           !Invoice At Completion
InvoiceTypeComplete         BYTE                           !Invoice Type        
Generic_Account             BYTE                           !                    
StoresAccountNumber         STRING(30)                     !* NOT USED *        
Force_Customer_Name         BYTE                           !                    
FinanceContactName          STRING(30)                     !Contact Name        
FinanceTelephoneNo          STRING(30)                     !Contact Tel Number  
FinanceEmailAddress         STRING(255)                    !Email Address       
FinanceAddress1             STRING(30)                     !Address             
FinanceAddress2             STRING(30)                     !Address             
FinanceAddress3             STRING(30)                     !Finance             
FinancePostcode             STRING(30)                     !Postcode            
AccountLimit                REAL                           !Account Limit       
ExcludeBouncer              BYTE                           !Exclude From Bouncer
SatStartWorkHours           TIME                           !Start Time          
SatEndWorkHours             TIME                           !End Time            
SunStartWorkHours           TIME                           !Start Time          
SunEndWorkHours             TIME                           !End Time            
ExcludeFromTATReport        BYTE                           !Exclude From TAT Report
OverrideHeadVATNo           BYTE                           !Override Head Account V.A.T. No
Line500AccountNumber        STRING(30)                     !Line 500 Account Number
OverrideHeadMobile          BYTE                           !Override Head Account Mandatory Mobile Check
OverrideMobileDefault       BYTE                           !Override Mandatory Mobile Number
SIDJobBooking               BYTE                           !SID Job Booking     
SIDJobEnquiry               BYTE                           !SID Job Enquiry     
Region                      STRING(30)                     !Region              
AllowVCPLoanUnits           BYTE                           !Allow VCP Loan Units
Hub                         STRING(30)                     !Hub                 
RefurbishmentAccount        BYTE                           !Refurbishment Account
VCPWaybillPrefix            STRING(30)                     !VCP Waybill Prefix  
PrintOOWVCPFee              BYTE                           !Print OOW VCP Fee   
OOWVCPFeeLabel              STRING(30)                     !OOW VCP Fee Label   
OOWVCPFeeAmount             REAL                           !OOW VCP Fee Amount  
PrintWarrantyVCPFee         BYTE                           !Print Warranty VCP Fee
WarrantyVCPFeeAmount        REAL                           !Warranty VCP Fee Amount
DealerID                    STRING(30)                     !Dealer ID           
                         END
                     END                       

CHARTYPE             FILE,DRIVER('Btrieve'),NAME('CHARTYPE.DAT'),PRE(cha),CREATE,BINDABLE,THREAD !Charge Types        
Charge_Type_Key          KEY(cha:Charge_Type),NOCASE,PRIMARY !By Charge Type      
Ref_Number_Key           KEY(cha:Ref_Number,cha:Charge_Type),DUP,NOCASE !By Charge Type      
Physical_Damage_Key      KEY(cha:Allow_Physical_Damage,cha:Charge_Type),DUP,NOCASE !By Charge Type      
Warranty_Key             KEY(cha:Warranty,cha:Charge_Type),DUP,NOCASE !By Charge Type      
Warranty_Ref_Number_Key  KEY(cha:Warranty,cha:Ref_Number,cha:Charge_Type),DUP,NOCASE !By Charge Type      
Record                   RECORD,PRE()
Charge_Type                 STRING(30)                     !Charge Type         
Force_Warranty              STRING(3)                      !Force Fault Codes (YES/NO)
Allow_Physical_Damage       STRING(3)                      !Allow Physical Damage (YES/NO)
Allow_Estimate              STRING(3)                      !Allow Estimate (YES/NO)
Force_Estimate              STRING(3)                      !Force Estimate (YES/NO)
Invoice_Customer            STRING(3)                      !Invoice Customer (YES/NO)
Invoice_Trade_Customer      STRING(3)                      !Invoice Trade Customer (YES/NO)
Invoice_Manufacturer        STRING(3)                      !Invoice Manufacturer (YES/NO)
No_Charge                   STRING(3)                      !NOT USED            
Zero_Parts                  STRING(3)                      !Fixed Price (RRC) (YES/NO)
Warranty                    STRING(3)                      !Warranty Charge Type (YES/NO)
Ref_Number                  REAL                           !                    
Exclude_EDI                 STRING(3)                      !Exclude From EDI Process (YES/NO)
ExcludeInvoice              STRING(3)                      !Exclude From Invoicing (YES/NO)
ExcludeFromBouncer          BYTE                           !Exclude From Bouncer Table (True/False)
ForceAuthorisation          BYTE                           !Force Authorisation Code (True/False)
Zero_Parts_ARC              BYTE                           !Fixed Price ARC (True/False)
SecondYearWarranty          BYTE                           !Second Year Warranty (True/False)
                         END
                     END                       

WAYBAWT              FILE,DRIVER('Btrieve'),OEM,NAME('WAYBAWT.DAT'),PRE(wya),CREATE,BINDABLE,THREAD !Waybill Generation - Awaiting Processing
WAYBAWTIDKey             KEY(wya:WAYBAWTID),NOCASE,PRIMARY !                    
AccountJobNumberKey      KEY(wya:AccountNumber,wya:JobNumber),DUP,NOCASE !                    
JobNumberKey             KEY(wya:JobNumber),DUP,NOCASE     !                    
Record                   RECORD,PRE()
WAYBAWTID                   LONG                           !                    
JobNumber                   LONG                           !                    
AccountNumber               STRING(30)                     !Header Account Number
Manufacturer                STRING(30)                     !                    
ModelNumber                 STRING(30)                     !                    
IMEINumber                  STRING(20)                     !                    
                         END
                     END                       

MANUFACT             FILE,DRIVER('Btrieve'),NAME('MANUFACT.DAT'),PRE(man),CREATE,BINDABLE,THREAD !Manufacturers       
RecordNumberKey          KEY(man:RecordNumber),NOCASE,PRIMARY !By Record Number    
Manufacturer_Key         KEY(man:Manufacturer),NOCASE      !By Manufacturer     
EDIFileTypeKey           KEY(man:EDIFileType,man:Manufacturer),DUP,NOCASE !By Manufacturer     
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
Manufacturer                STRING(30)                     !Manufacturer        
Account_Number              STRING(30)                     !Account Number      
Postcode                    STRING(30)                     !Postcode            
Address_Line1               STRING(30)                     !Address Line 1      
Address_Line2               STRING(30)                     !Address Line 2      
Address_Line3               STRING(30)                     !Address Line 3      
Telephone_Number            STRING(30)                     !Telephone Number    
Fax_Number                  STRING(30)                     !Fax Number          
EmailAddress                STRING(30)                     !Email Address       
Use_MSN                     STRING(3)                      !Models Use MSN (YES/NO)
Contact_Name1               STRING(60)                     !Contact Name 1      
Contact_Name2               STRING(60)                     !Contact Name 2      
Head_Office_Telephone       STRING(15)                     !Head Office Telephone Number
Head_Office_Fax             STRING(15)                     !Head Office Fax Number
Technical_Support_Telephone STRING(15)                     !Technical Support Telephone Number
Technical_Support_Fax       STRING(15)                     !Technical Support Fax Number
Technical_Support_Hours     STRING(30)                     !Technical Support Hours
Batch_Number                LONG                           !Last EDI Batch Number
EDI_Account_Number          STRING(30)                     !EDI Account Number  
EDI_Path                    STRING(255)                    !EDI File Export Path
Trade_Account               STRING(30)                     !Warranty Account    
Supplier                    STRING(30)                     !Supplier            
Warranty_Period             REAL                           !Warranty Period From DOP
SamsungCount                LONG                           !Count Of Samsung Claims
IncludeAdjustment           STRING(3)                      !EDI Warranty Adjustments
AdjustPart                  BYTE                           !Assign Part Number To Warranty Adjustment
ForceParts                  BYTE                           !Force Warranty Adjustment If No Parts Used
NokiaType                   STRING(20)                     !Nokia Type (COMMUNICAID/NSH)
RemAccCosts                 BYTE                           !Remove Accessory Claims Costs
SiemensNewEDI               BYTE                           !Use New EDI Format  
DOPCompulsory               BYTE                           !D.O.P. Compulsory for Warranty
Notes                       STRING(255)                    !                    
UseQA                       BYTE                           !Use QA (True/False) 
UseElectronicQA             BYTE                           !Use Electronic QA (True/False)
QALoanExchange              BYTE                           !QA Loan/Exchange Units (True/False)
QAAtCompletion              BYTE                           !QA At Completion (True/False)
SiemensNumber               LONG                           !Siemens Number      
SiemensDate                 DATE                           !Date                
UseProductCode              BYTE                           !Use Product Code (True/False)
ApplyMSNFormat              BYTE                           !Apply M.S.N. Format (True/False)
MSNFormat                   STRING(30)                     !M.S.N. Format       
ValidateDateCode            BYTE                           !Validate Date Code (True/False)
ForceStatus                 BYTE                           !Force Status (True/False)
StatusRequired              STRING(30)                     !Status Required     
POPPeriod                   LONG                           !P.O.P. Period       
ClaimPeriod                 LONG                           !Claim Period        
QAParts                     BYTE                           !Validate Parts At QA (True/False)
QANetwork                   BYTE                           !Validate Network At QA (True/False)
POPRequired                 BYTE                           !POP Required For Claim (True/False)
UseInvTextForFaults         BYTE                           !Use Main Fault For Invoice Text (True/False)
ForceAccessoryCode          BYTE                           !Force Accessory Fault Codes Only (True/False)
UseInternetValidation       BYTE                           !Use Internet Validation (True/False)
AutoRepairType              BYTE                           !AutoRepairType (True/False)
BillingConfirmation         BYTE                           !Display Billing Confirmation Screen (True/False)
CreateEDIReport             BYTE                           !Create Service History Report (True/False)
EDIFileType                 STRING(30)                     !E.D.I. File Type    
CreateEDIFile               BYTE                           !Create EDI Export File
ExchangeFee                 REAL                           !Exchange Fee        
QALoan                      BYTE                           !QA Loans (True/False)
ForceCharFaultCodes         BYTE                           !Force Out Of Warranty Fault Codes (True/False)
IncludeCharJobs             BYTE                           !Include Out Of Warranty Jobs (True/False)
SecondYrExchangeFee         REAL                           !Second Year Exchange Fee
SecondYrTradeAccount        STRING(30)                     !Second Year Warranty Account
VATNumber                   STRING(30)                     !VAT Number          
UseProdCodesForEXC          BYTE                           !Use Handset Part Number Of Exchanges
UseFaultCodesForOBF         BYTE                           !Use Fault Code For OBF Jobs
KeyRepairRequired           BYTE                           !Force "Key Repair" Part
UseReplenishmentProcess     BYTE                           !Use Replenishment Process
UseResubmissionLimit        BYTE                           !Use Warranty Claim Resubmission Limit
ResubmissionLimit           LONG                           !Days To Resubmit By 
AutoTechnicalReports        BYTE                           !Auto-create Technical Reports
AlertEmailAddress           STRING(255)                    !Alert Email Address 
LimitResubmissions          BYTE                           !Limit Resubmissions 
TimesToResubmit             LONG                           !Times Allowed To Resubmit
CopyDOPFromBouncer          BYTE                           !Copy D.O.P. From Bouncer Job
ExcludeAutomaticRebookingProcess BYTE                      !Exclude Automatic Rebooking Process
OneYearWarrOnly             STRING(1)                      !                    
EDItransportFee             REAL                           !                    
SagemVersionNumber          STRING(30)                     !                    
UseBouncerRules             BYTE                           !                    
BouncerPeriod               LONG                           !                    
BouncerType                 BYTE                           !                    
BouncerInFault              BYTE                           !                    
BouncerOutFault             BYTE                           !                    
BouncerSpares               BYTE                           !                    
BouncerPeriodIMEI           LONG                           !                    
DoNotBounceWarranty         BYTE                           !                    
BounceRulesType             BYTE                           !Bounce Rules Type   
ThirdPartyHandlingFee       REAL                           !                    
ProductCodeCompulsory       STRING(3)                      !                    
Inactive                    BYTE                           !                    
                         END
                     END                       

USMASSIG             FILE,DRIVER('Btrieve'),NAME('USMASSIG.DAT'),PRE(usm),CREATE,BINDABLE,THREAD !Users Model Number Assignments
Model_Number_Key         KEY(usm:User_Code,usm:Model_Number),NOCASE,PRIMARY !By Model Number     
Record                   RECORD,PRE()
User_Code                   STRING(3)                      !                    
Model_Number                STRING(30)                     !                    
                         END
                     END                       

WEBJOB               FILE,DRIVER('Btrieve'),OEM,NAME('WEBJOB.DAT'),PRE(wob),CREATE,BINDABLE,THREAD !Web Job             
RecordNumberKey          KEY(wob:RecordNumber),NOCASE,PRIMARY !By Record Number    
RefNumberKey             KEY(wob:RefNumber),DUP,NOCASE     !By Ref Number       
HeadJobNumberKey         KEY(wob:HeadAccountNumber,wob:JobNumber),DUP,NOCASE !By Job Number       
HeadOrderNumberKey       KEY(wob:HeadAccountNumber,wob:OrderNumber,wob:RefNumber),DUP,NOCASE !By Order Number     
HeadMobileNumberKey      KEY(wob:HeadAccountNumber,wob:MobileNumber,wob:RefNumber),DUP,NOCASE !By Mobile Number    
HeadModelNumberKey       KEY(wob:HeadAccountNumber,wob:ModelNumber,wob:RefNumber),DUP,NOCASE !By Job Number       
HeadIMEINumberKey        KEY(wob:HeadAccountNumber,wob:IMEINumber,wob:RefNumber),DUP,NOCASE !By I.M.E.I. Number  
HeadMSNKey               KEY(wob:HeadAccountNumber,wob:MSN),DUP,NOCASE !By M.S.N.           
HeadEDIKey               KEY(wob:HeadAccountNumber,wob:EDI,wob:Manufacturer,wob:JobNumber),DUP,NOCASE !By Job Number       
ValidationIMEIKey        KEY(wob:HeadAccountNumber,wob:Validation,wob:IMEINumber),DUP,NOCASE !By I.M.E.I. Number  
HeadSubKey               KEY(wob:HeadAccountNumber,wob:SubAcountNumber,wob:RefNumber),DUP,NOCASE !                    
HeadRefNumberKey         KEY(wob:HeadAccountNumber,wob:RefNumber),DUP,NOCASE !                    
OracleNumberKey          KEY(wob:OracleExportNumber),DUP,NOCASE !By Oracle Number    
HeadCurrentStatusKey     KEY(wob:HeadAccountNumber,wob:Current_Status,wob:RefNumber),DUP,NOCASE !By Job Status       
HeadExchangeStatus       KEY(wob:HeadAccountNumber,wob:Exchange_Status,wob:RefNumber),DUP,NOCASE !By Exchange Status  
HeadLoanStatusKey        KEY(wob:HeadAccountNumber,wob:Loan_Status,wob:RefNumber),DUP,NOCASE !By Loan Status      
ExcWayBillNoKey          KEY(wob:ExcWayBillNumber),DUP,NOCASE !By Exchange Waybill Number
LoaWayBillNoKey          KEY(wob:LoaWayBillNumber),DUP,NOCASE !By Loan Waybill Number
JobWayBillNoKey          KEY(wob:JobWayBillNumber),DUP,NOCASE !By Job WayBill Number
RRCWInvoiceNumberKey     KEY(wob:RRCWInvoiceNumber,wob:RefNumber),DUP,NOCASE !By Job Number       
DateBookedKey            KEY(wob:HeadAccountNumber,wob:DateBooked),DUP,NOCASE !By Date Booked      
DateCompletedKey         KEY(wob:HeadAccountNumber,wob:DateCompleted),DUP,NOCASE !By Date Completed   
CompletedKey             KEY(wob:HeadAccountNumber,wob:Completed),DUP,NOCASE !By Completed        
DespatchKey              KEY(wob:HeadAccountNumber,wob:ReadyToDespatch,wob:RefNumber),DUP,NOCASE !By Job Number       
DespatchSubKey           KEY(wob:HeadAccountNumber,wob:ReadyToDespatch,wob:SubAcountNumber,wob:RefNumber),DUP,NOCASE !By Job Number       
DespatchCourierKey       KEY(wob:HeadAccountNumber,wob:ReadyToDespatch,wob:DespatchCourier,wob:RefNumber),DUP,NOCASE !By Job Number       
DespatchSubCourierKey    KEY(wob:HeadAccountNumber,wob:ReadyToDespatch,wob:SubAcountNumber,wob:DespatchCourier,wob:RefNumber),DUP,NOCASE !By Job Number       
EDIKey                   KEY(wob:HeadAccountNumber,wob:EDI,wob:Manufacturer,wob:RefNumber),DUP,NOCASE !By Job Number       
EDIRefNumberKey          KEY(wob:HeadAccountNumber,wob:EDI,wob:RefNumber),DUP,NOCASE !By Job Number       
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
JobNumber                   LONG                           !Job Number          
RefNumber                   LONG                           !Link to JOBS RefNumber
HeadAccountNumber           STRING(30)                     !Header Account Number
SubAcountNumber             STRING(30)                     !Sub Account Number  
OrderNumber                 STRING(30)                     !Order Number        
IMEINumber                  STRING(30)                     !I.M.E.I. Number     
MSN                         STRING(30)                     !M.S.N.              
Manufacturer                STRING(30)                     !Manufacturer        
ModelNumber                 STRING(30)                     !Model Number        
MobileNumber                STRING(30)                     !Mobile Number       
EDI                         STRING(3)                      !EDI                 
Validation                  STRING(3)                      !Validation          
OracleExportNumber          LONG                           !Oracle Export Number
Current_Status              STRING(30)                     !                    
Exchange_Status             STRING(30)                     !                    
Loan_Status                 STRING(30)                     !                    
Current_Status_Date         DATE                           !                    
Exchange_Status_Date        DATE                           !                    
Loan_Status_Date            DATE                           !                    
ExcWayBillNumber            LONG                           !Exchange Waybill Number
LoaWayBillNumber            LONG                           !Loan Waybill Number 
JobWayBillNumber            LONG                           !Job Waybill Number  
DateJobDespatched           DATE                           !Date Despatched     
RRCEngineer                 STRING(3)                      !RRC Engineer        
ReconciledMarker            BYTE                           !Marker              
RRCWInvoiceNumber           LONG                           !Warranty Invoice Number
DateBooked                  DATE                           !Copy Of JOBS Date Booked
TimeBooked                  TIME                           !Copy Of JOBS Time Booked
DateCompleted               DATE                           !Copy Of JOBS Date Completed
TimeCompleted               TIME                           !Copy Of JOBS Time Completed
Completed                   STRING(3)                      !Copy Of JOBS Completed
ReadyToDespatch             BYTE                           !Ready To Despatch   
DespatchCourier             STRING(30)                     !Courier To Despatch 
FaultCode13                 STRING(30)                     !Fault Code 13       
FaultCode14                 STRING(30)                     !Fault Code 14       
FaultCode15                 STRING(30)                     !Fault Code 15       
FaultCode16                 STRING(30)                     !Fault Code 16       
FaultCode17                 STRING(30)                     !Fault Code 17       
FaultCode18                 STRING(30)                     !Fault Code 18       
FaultCode19                 STRING(30)                     !Fault Code 19       
FaultCode20                 STRING(30)                     !Fault Code 20       
                         END
                     END                       

USUASSIG             FILE,DRIVER('Btrieve'),NAME('USUASSIG.DAT'),PRE(usu),CREATE,BINDABLE,THREAD !Users Unit Type Assignments
Unit_Type_Key            KEY(usu:User_Code,usu:Unit_Type),NOCASE,PRIMARY !By Unit Type        
Unit_Type_Only_Key       KEY(usu:Unit_Type),DUP,NOCASE     !By Unit Type        
Record                   RECORD,PRE()
User_Code                   STRING(3)                      !                    
Unit_Type                   STRING(30)                     !                    
                         END
                     END                       

JOBSLOCK             FILE,DRIVER('Btrieve'),NAME('JOBSLOCK.DAT'),PRE(lock),CREATE,BINDABLE,THREAD !Table To Hold Locked Jobs
RecordNumberKey          KEY(lock:RecordNumber),NOCASE,PRIMARY !By Record Number    
JobNumberKey             KEY(lock:JobNumber),NOCASE        !By Job Number       
Record                   RECORD,PRE()
RecordNumber                LONG                           !                    
JobNumber                   LONG                           !                    
DateLocked                  DATE                           !                    
TimeLocked                  TIME                           !                    
SessionValue                LONG                           !                    
                         END
                     END                       

TRAHUBAC             FILE,DRIVER('Btrieve'),NAME('TRAHUBAC.DAT'),PRE(TRA1),CREATE,BINDABLE,THREAD !Trade Hub Accounts  
RecordNoKey              KEY(TRA1:RecordNo),NOCASE,PRIMARY !                    
HeadAccSubAccKey         KEY(TRA1:HeadAcc,TRA1:SubAcc),DUP,NOCASE !                    
HeadAccSubAccNameKey     KEY(TRA1:HeadAcc,TRA1:SubAccName),DUP,NOCASE !                    
HeadAccSubAccBranchKey   KEY(TRA1:HeadAcc,TRA1:SubAccBranch),DUP,NOCASE !                    
Record                   RECORD,PRE()
RecordNo                    LONG                           !                    
HeadAcc                     STRING(15)                     !                    
SubAcc                      STRING(15)                     !                    
SubAccName                  STRING(30)                     !                    
SubAccBranch                STRING(30)                     !                    
                         END
                     END                       

STOHISTE             FILE,DRIVER('Btrieve'),NAME('STOHISTE.DAT'),PRE(stoe),CREATE,BINDABLE,THREAD !Stock History Extension
RecordNumberKey          KEY(stoe:RecordNumber),NOCASE,PRIMARY !By Record Number    
SHIRecordNumberKey       KEY(stoe:SHIRecordNumber),DUP,NOCASE !                    
KeyArcStatus             KEY(stoe:ARC_Status),DUP,NOCASE   !                    
Record                   RECORD,PRE()
RecordNumber                LONG                           !                    
SHIRecordNumber             LONG                           !                    
PreviousAveragePurchaseCost REAL                           !                    
PurchaseCost                REAL                           !                    
SaleCost                    REAL                           !                    
RetailCost                  REAL                           !                    
HistTime                    TIME                           !                    
ARC_Status                  STRING(1)                      !R = Requested S = SaleCreated
                         END
                     END                       

MODEXCHA             FILE,DRIVER('Btrieve'),NAME('MODEXCHA.DAT'),PRE(moa),CREATE,BINDABLE,THREAD !Exchange Order Model
RecordNumberKey          KEY(moa:RecordNumber),NOCASE,PRIMARY !                    
AccountNumberKey         KEY(moa:Manufacturer,moa:ModelNumber,moa:AccountNumber),NOCASE !By Account Number   
Record                   RECORD,PRE()
RecordNumber                LONG                           !                    
Manufacturer                STRING(30)                     !                    
ModelNumber                 STRING(30)                     !                    
AccountNumber               STRING(30)                     !                    
CompanyName                 STRING(30)                     !                    
                         END
                     END                       

TRDMAN               FILE,DRIVER('Btrieve'),NAME('TRDMAN.DAT'),PRE(tdm),CREATE,BINDABLE,THREAD !                    
RecordNumberKey          KEY(tdm:RecordNumber),NOCASE,PRIMARY !                    
ManufacturerKey          KEY(tdm:ThirdPartyCompanyName,tdm:Manufacturer),NOCASE !                    
Record                   RECORD,PRE()
RecordNumber                LONG                           !                    
ThirdPartyCompanyName       STRING(30)                     !                    
Manufacturer                STRING(30)                     !                    
                         END
                     END                       

AUDIT2               FILE,DRIVER('Btrieve'),NAME('AUDIT2.DAT'),PRE(aud2),CREATE,BINDABLE,THREAD !                    
RecordNumberKey          KEY(aud2:RecordNumber),NOCASE,PRIMARY !                    
AUDRecordNumberKey       KEY(aud2:AUDRecordNumber),DUP,NOCASE !                    
Record                   RECORD,PRE()
RecordNumber                LONG                           !                    
AUDRecordNumber             LONG                           !                    
Notes                       STRING(255)                    !                    
                         END
                     END                       

JOBSSL               FILE,DRIVER('Btrieve'),NAME('JOBSSL.DAT'),PRE(jsl),CREATE,BINDABLE,THREAD !Job Skyline Extension
RecordNumberKey          KEY(jsl:RecordNumber),NOCASE,PRIMARY !                    
RefNumberKey             KEY(jsl:RefNumber),DUP,NOCASE     !                    
SLNumberKey              KEY(jsl:SLNumber),DUP,NOCASE      !                    
Record                   RECORD,PRE()
RecordNumber                LONG                           !                    
RefNumber                   LONG                           !Linke To JOBS       
SLNumber                    LONG                           !Skyline Number      
                         END
                     END                       

JOBSOBF_ALIAS        FILE,DRIVER('Btrieve'),OEM,NAME('JOBSOBF.DAT'),PRE(jofali),CREATE,BINDABLE,THREAD !                    
RecordNumberKey          KEY(jofali:RecordNumber),NOCASE,PRIMARY !By Record Number    
RefNumberKey             KEY(jofali:RefNumber),DUP,NOCASE  !By Job Number       
StatusRefNumberKey       KEY(jofali:Status,jofali:RefNumber),DUP,NOCASE !By Job Number       
StatusIMEINumberKey      KEY(jofali:Status,jofali:IMEINumber),DUP,NOCASE !By I.M.E.I. Number  
HeadAccountCompletedKey  KEY(jofali:HeadAccountNumber,jofali:DateCompleted,jofali:RefNumber),DUP,NOCASE !By Date Completed   
HeadAccountProcessedKey  KEY(jofali:HeadAccountNumber,jofali:DateProcessed,jofali:RefNumber),DUP,NOCASE !By Date Processed   
Record                   RECORD,PRE()
RecordNumber                LONG                           !Record Number       
RefNumber                   LONG                           !Like to JOBS RefNumber
IMEINumber                  STRING(30)                     !I.M.E.I. Number     
Status                      BYTE                           !Processed Status    
Replacement                 BYTE                           !Replacement         
StoreReferenceNumber        STRING(30)                     !Stock Reference Number
RNumber                     STRING(30)                     !R Number            
RejectionReason             STRING(255)                    !Rejection Reason    
ReplacementIMEI             STRING(30)                     !Replacement I.M.E.I. Number
LAccountNumber              STRING(30)                     !L/Account Number    
UserCode                    STRING(3)                      !User Code           
HeadAccountNumber           STRING(30)                     !Head Account Number 
DateCompleted               DATE                           !Date Completed      
TimeCompleted               TIME                           !Time Completed      
DateProcessed               DATE                           !Date Process        
TimeProcessed               TIME                           !Time Processed      
                         END
                     END                       

JOBPAYMT_ALIAS       FILE,DRIVER('Btrieve'),NAME('JOBPAYMT.DAT'),PRE(jpt_ali),CREATE,BINDABLE,THREAD !                    
Record_Number_Key        KEY(jpt_ali:Record_Number),NOCASE,PRIMARY !By Record Number    
All_Date_Key             KEY(jpt_ali:Ref_Number,jpt_ali:Date),DUP,NOCASE !By Date             
Loan_Deposit_Key         KEY(jpt_ali:Ref_Number,jpt_ali:Loan_Deposit,jpt_ali:Date),DUP,NOCASE !                    
Record                   RECORD,PRE()
Record_Number               REAL                           !                    
Ref_Number                  REAL                           !                    
Date                        DATE                           !                    
Payment_Type                STRING(30)                     !                    
Credit_Card_Number          STRING(20)                     !                    
Expiry_Date                 STRING(5)                      !                    
Issue_Number                STRING(5)                      !                    
Amount                      REAL                           !                    
User_Code                   STRING(3)                      !                    
Loan_Deposit                BYTE                           !Loan Deposit        
                         END
                     END                       

JOBS_ALIAS           FILE,DRIVER('Btrieve'),NAME('JOBS.DAT'),PRE(job_ali),BINDABLE,CREATE,THREAD !                    
Ref_Number_Key           KEY(job_ali:Ref_Number),NOCASE,PRIMARY !By Job Number       
Model_Unit_Key           KEY(job_ali:Model_Number,job_ali:Unit_Type),DUP,NOCASE !By Job Number       
EngCompKey               KEY(job_ali:Engineer,job_ali:Completed,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
EngWorkKey               KEY(job_ali:Engineer,job_ali:Workshop,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
Surname_Key              KEY(job_ali:Surname),DUP,NOCASE   !By Surname          
MobileNumberKey          KEY(job_ali:Mobile_Number),DUP,NOCASE !By Mobile Number    
ESN_Key                  KEY(job_ali:ESN),DUP,NOCASE       !By E.S.N. / I.M.E.I.
MSN_Key                  KEY(job_ali:MSN),DUP,NOCASE       !By M.S.N.           
AccountNumberKey         KEY(job_ali:Account_Number,job_ali:Ref_Number),DUP,NOCASE !By Account Number   
AccOrdNoKey              KEY(job_ali:Account_Number,job_ali:Order_Number),DUP,NOCASE !By Order Number     
Model_Number_Key         KEY(job_ali:Model_Number),DUP,NOCASE !By Model Number     
Engineer_Key             KEY(job_ali:Engineer,job_ali:Model_Number),DUP,NOCASE !By Engineer         
Date_Booked_Key          KEY(job_ali:date_booked),DUP,NOCASE !By Date Booked      
DateCompletedKey         KEY(job_ali:Date_Completed),DUP,NOCASE !By Date Completed   
ModelCompKey             KEY(job_ali:Model_Number,job_ali:Date_Completed),DUP,NOCASE !By Completed Date   
By_Status                KEY(job_ali:Current_Status,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
StatusLocKey             KEY(job_ali:Current_Status,job_ali:Location,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
Location_Key             KEY(job_ali:Location,job_ali:Ref_Number),DUP,NOCASE !By Location         
Third_Party_Key          KEY(job_ali:Third_Party_Site,job_ali:Third_Party_Printed,job_ali:Ref_Number),DUP,NOCASE !By Third Party      
ThirdEsnKey              KEY(job_ali:Third_Party_Site,job_ali:Third_Party_Printed,job_ali:ESN),DUP,NOCASE !By ESN              
ThirdMsnKey              KEY(job_ali:Third_Party_Site,job_ali:Third_Party_Printed,job_ali:MSN),DUP,NOCASE !By MSN              
PriorityTypeKey          KEY(job_ali:Job_Priority,job_ali:Ref_Number),DUP,NOCASE !By Priority         
Unit_Type_Key            KEY(job_ali:Unit_Type),DUP,NOCASE !By Unit Type        
EDI_Key                  KEY(job_ali:Manufacturer,job_ali:EDI,job_ali:EDI_Batch_Number,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
InvoiceNumberKey         KEY(job_ali:Invoice_Number),DUP,NOCASE !By Invoice_Number   
WarInvoiceNoKey          KEY(job_ali:Invoice_Number_Warranty),DUP,NOCASE !By Invoice Number   
Batch_Number_Key         KEY(job_ali:Batch_Number,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
Batch_Status_Key         KEY(job_ali:Batch_Number,job_ali:Current_Status,job_ali:Ref_Number),DUP,NOCASE !By Ref Number       
BatchModelNoKey          KEY(job_ali:Batch_Number,job_ali:Model_Number,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
BatchInvoicedKey         KEY(job_ali:Batch_Number,job_ali:Invoice_Number,job_ali:Ref_Number),DUP,NOCASE !By Ref Number       
BatchCompKey             KEY(job_ali:Batch_Number,job_ali:Completed,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
ChaInvoiceKey            KEY(job_ali:Chargeable_Job,job_ali:Account_Number,job_ali:Invoice_Number,job_ali:Ref_Number),DUP,NOCASE !                    
InvoiceExceptKey         KEY(job_ali:Invoice_Exception,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
ConsignmentNoKey         KEY(job_ali:Consignment_Number),DUP,NOCASE !By Cosignment Number
InConsignKey             KEY(job_ali:Incoming_Consignment_Number),DUP,NOCASE !By Consignment Number
ReadyToDespKey           KEY(job_ali:Despatched,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
ReadyToTradeKey          KEY(job_ali:Despatched,job_ali:Account_Number,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
ReadyToCouKey            KEY(job_ali:Despatched,job_ali:Current_Courier,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
ReadyToAllKey            KEY(job_ali:Despatched,job_ali:Account_Number,job_ali:Current_Courier,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
DespJobNumberKey         KEY(job_ali:Despatch_Number,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
DateDespatchKey          KEY(job_ali:Courier,job_ali:Date_Despatched,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
DateDespLoaKey           KEY(job_ali:Loan_Courier,job_ali:Loan_Despatched,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
DateDespExcKey           KEY(job_ali:Exchange_Courier,job_ali:Exchange_Despatched,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
ChaRepTypeKey            KEY(job_ali:Repair_Type),DUP,NOCASE !By Chargeable Repair Type
WarRepTypeKey            KEY(job_ali:Repair_Type_Warranty),DUP,NOCASE !By Warranty Repair Type
ChaTypeKey               KEY(job_ali:Charge_Type),DUP,NOCASE !                    
WarChaTypeKey            KEY(job_ali:Warranty_Charge_Type),DUP,NOCASE !                    
Bouncer_Key              KEY(job_ali:Bouncer,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
EngDateCompKey           KEY(job_ali:Engineer,job_ali:Date_Completed),DUP,NOCASE !By Date Completed   
ExcStatusKey             KEY(job_ali:Exchange_Status,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
LoanStatusKey            KEY(job_ali:Loan_Status,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
ExchangeLocKey           KEY(job_ali:Exchange_Status,job_ali:Location,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
LoanLocKey               KEY(job_ali:Loan_Status,job_ali:Location,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
BatchJobKey              KEY(job_ali:InvoiceAccount,job_ali:InvoiceBatch,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
BatchStatusKey           KEY(job_ali:InvoiceAccount,job_ali:InvoiceBatch,job_ali:InvoiceStatus,job_ali:Ref_Number),DUP,NOCASE !By Job Number       
Record                   RECORD,PRE()
Ref_Number                  LONG                           !                    
Batch_Number                LONG                           !                    
Internal_Status             STRING(10)                     !                    
Auto_Search                 STRING(30)                     !                    
who_booked                  STRING(3)                      !                    
date_booked                 DATE                           !                    
time_booked                 TIME                           !                    
Cancelled                   STRING(3)                      !                    
Bouncer                     STRING(8)                      !                    
Bouncer_Type                STRING(3)                      !Type Of Bouncer  CHArgeable / WARranty / BOTh
Web_Type                    STRING(3)                      !Warranty/Insurance/Chargeable
Warranty_Job                STRING(3)                      !                    
Chargeable_Job              STRING(3)                      !                    
Model_Number                STRING(30)                     !                    
Manufacturer                STRING(30)                     !                    
ESN                         STRING(20)                     !                    
MSN                         STRING(20)                     !                    
ProductCode                 STRING(30)                     !                    
Unit_Type                   STRING(30)                     !                    
Colour                      STRING(30)                     !                    
Location_Type               STRING(10)                     !                    
Phone_Lock                  STRING(30)                     !                    
Workshop                    STRING(3)                      !                    
Location                    STRING(30)                     !                    
Authority_Number            STRING(30)                     !                    
Insurance_Reference_Number  STRING(30)                     !                    
DOP                         DATE                           !                    
Insurance                   STRING(3)                      !                    
Insurance_Type              STRING(30)                     !                    
Transit_Type                STRING(30)                     !                    
Physical_Damage             STRING(3)                      !                    
Intermittent_Fault          STRING(3)                      !                    
Loan_Status                 STRING(30)                     !                    
Exchange_Status             STRING(30)                     !                    
Job_Priority                STRING(30)                     !                    
Charge_Type                 STRING(30)                     !                    
Warranty_Charge_Type        STRING(30)                     !                    
Current_Status              STRING(30)                     !                    
Account_Number              STRING(15)                     !                    
Trade_Account_Name          STRING(30)                     !                    
Department_Name             STRING(30)                     !                    
Order_Number                STRING(30)                     !                    
POP                         STRING(3)                      !                    
In_Repair                   STRING(3)                      !                    
Date_In_Repair              DATE                           !                    
Time_In_Repair              TIME                           !                    
On_Test                     STRING(3)                      !                    
Date_On_Test                DATE                           !                    
Time_On_Test                TIME                           !                    
Estimate_Ready              STRING(3)                      !                    
QA_Passed                   STRING(3)                      !                    
Date_QA_Passed              DATE                           !                    
Time_QA_Passed              TIME                           !                    
QA_Rejected                 STRING(3)                      !                    
Date_QA_Rejected            DATE                           !                    
Time_QA_Rejected            TIME                           !                    
QA_Second_Passed            STRING(3)                      !                    
Date_QA_Second_Passed       DATE                           !                    
Time_QA_Second_Passed       TIME                           !                    
Title                       STRING(4)                      !                    
Initial                     STRING(1)                      !                    
Surname                     STRING(30)                     !                    
Postcode                    STRING(10)                     !                    
Company_Name                STRING(30)                     !                    
Address_Line1               STRING(30)                     !                    
Address_Line2               STRING(30)                     !                    
Address_Line3               STRING(30)                     !                    
Telephone_Number            STRING(15)                     !                    
Fax_Number                  STRING(15)                     !                    
Mobile_Number               STRING(15)                     !                    
Postcode_Collection         STRING(10)                     !                    
Company_Name_Collection     STRING(30)                     !                    
Address_Line1_Collection    STRING(30)                     !                    
Address_Line2_Collection    STRING(30)                     !                    
Address_Line3_Collection    STRING(30)                     !                    
Telephone_Collection        STRING(15)                     !                    
Postcode_Delivery           STRING(10)                     !                    
Address_Line1_Delivery      STRING(30)                     !                    
Company_Name_Delivery       STRING(30)                     !                    
Address_Line2_Delivery      STRING(30)                     !                    
Address_Line3_Delivery      STRING(30)                     !                    
Telephone_Delivery          STRING(15)                     !                    
Date_Completed              DATE                           !                    
Time_Completed              TIME                           !                    
Completed                   STRING(3)                      !                    
Paid                        STRING(3)                      !                    
Paid_Warranty               STRING(3)                      !                    
Date_Paid                   DATE                           !                    
Repair_Type                 STRING(30)                     !                    
Repair_Type_Warranty        STRING(30)                     !                    
Engineer                    STRING(3)                      !                    
Ignore_Chargeable_Charges   STRING(3)                      !                    
Ignore_Warranty_Charges     STRING(3)                      !                    
Ignore_Estimate_Charges     STRING(3)                      !                    
Courier_Cost                REAL                           !                    
Advance_Payment             REAL                           !                    
Labour_Cost                 REAL                           !                    
Parts_Cost                  REAL                           !                    
Sub_Total                   REAL                           !                    
Courier_Cost_Estimate       REAL                           !                    
Labour_Cost_Estimate        REAL                           !                    
Parts_Cost_Estimate         REAL                           !                    
Sub_Total_Estimate          REAL                           !                    
Courier_Cost_Warranty       REAL                           !                    
Labour_Cost_Warranty        REAL                           !                    
Parts_Cost_Warranty         REAL                           !                    
Sub_Total_Warranty          REAL                           !                    
Loan_Issued_Date            DATE                           !                    
Loan_Unit_Number            LONG                           !                    
Loan_accessory              STRING(3)                      !                    
Loan_User                   STRING(3)                      !                    
Loan_Courier                STRING(30)                     !                    
Loan_Consignment_Number     STRING(30)                     !                    
Loan_Despatched             DATE                           !                    
Loan_Despatched_User        STRING(3)                      !                    
Loan_Despatch_Number        LONG                           !                    
LoaService                  STRING(1)                      !Loan Service For City Link / LabelG
Exchange_Unit_Number        LONG                           !                    
Exchange_Authorised         STRING(3)                      !                    
Loan_Authorised             STRING(3)                      !                    
Exchange_Accessory          STRING(3)                      !                    
Exchange_Issued_Date        DATE                           !                    
Exchange_User               STRING(3)                      !                    
Exchange_Courier            STRING(30)                     !                    
Exchange_Consignment_Number STRING(30)                     !                    
Exchange_Despatched         DATE                           !                    
Exchange_Despatched_User    STRING(3)                      !                    
Exchange_Despatch_Number    LONG                           !                    
ExcService                  STRING(1)                      !Exchange Service For City Link / Label G
Date_Despatched             DATE                           !                    
Despatch_Number             LONG                           !                    
Despatch_User               STRING(3)                      !                    
Courier                     STRING(30)                     !                    
Consignment_Number          STRING(30)                     !                    
Incoming_Courier            STRING(30)                     !                    
Incoming_Consignment_Number STRING(30)                     !                    
Incoming_Date               DATE                           !                    
Despatched                  STRING(3)                      !                    
Despatch_Type               STRING(3)                      !                    
Current_Courier             STRING(30)                     !                    
JobService                  STRING(1)                      !Job Service For City Link / Label G
Last_Repair_Days            REAL                           !                    
Third_Party_Site            STRING(30)                     !                    
Estimate                    STRING(3)                      !                    
Estimate_If_Over            REAL                           !                    
Estimate_Accepted           STRING(3)                      !                    
Estimate_Rejected           STRING(3)                      !                    
Third_Party_Printed         STRING(3)                      !                    
ThirdPartyDateDesp          DATE                           !                    
Fault_Code1                 STRING(30)                     !                    
Fault_Code2                 STRING(30)                     !                    
Fault_Code3                 STRING(30)                     !                    
Fault_Code4                 STRING(30)                     !                    
Fault_Code5                 STRING(30)                     !                    
Fault_Code6                 STRING(30)                     !                    
Fault_Code7                 STRING(30)                     !                    
Fault_Code8                 STRING(30)                     !                    
Fault_Code9                 STRING(30)                     !                    
Fault_Code10                STRING(255)                    !                    
Fault_Code11                STRING(255)                    !                    
Fault_Code12                STRING(255)                    !                    
PreviousStatus              STRING(30)                     !                    
StatusUser                  STRING(3)                      !User who last changed the status
Status_End_Date             DATE                           !                    
Status_End_Time             TIME                           !                    
Turnaround_End_Date         DATE                           !                    
Turnaround_End_Time         TIME                           !                    
Turnaround_Time             STRING(30)                     !                    
Special_Instructions        STRING(30)                     !                    
InvoiceAccount              STRING(15)                     !Invoice Batch Account Number
InvoiceStatus               STRING(3)                      !Proforma Status (UNA - Unathorised / AUT - Authorised / QUE - Query)
InvoiceBatch                LONG                           !Proforma Batch Number
InvoiceQuery                STRING(255)                    !Query Reason        
EDI                         STRING(3)                      !                    
EDI_Batch_Number            REAL                           !                    
Invoice_Exception           STRING(3)                      !                    
Invoice_Failure_Reason      STRING(80)                     !                    
Invoice_Number              LONG                           !                    
Invoice_Date                DATE                           !                    
Invoice_Date_Warranty       DATE                           !                    
Invoice_Courier_Cost        REAL                           !                    
Invoice_Labour_Cost         REAL                           !                    
Invoice_Parts_Cost          REAL                           !                    
Invoice_Sub_Total           REAL                           !                    
Invoice_Number_Warranty     LONG                           !                    
WInvoice_Courier_Cost       REAL                           !                    
WInvoice_Labour_Cost        REAL                           !                    
WInvoice_Parts_Cost         REAL                           !                    
WInvoice_Sub_Total          REAL                           !                    
                         END
                     END                       

!endregion

Access:STDCHRGE      &FileManager,THREAD                   ! FileManager for STDCHRGE
Relate:STDCHRGE      &RelationManager,THREAD               ! RelationManager for STDCHRGE
Access:JOBOUTFL      &FileManager,THREAD                   ! FileManager for JOBOUTFL
Relate:JOBOUTFL      &RelationManager,THREAD               ! RelationManager for JOBOUTFL
Access:AUDITE        &FileManager,THREAD                   ! FileManager for AUDITE
Relate:AUDITE        &RelationManager,THREAD               ! RelationManager for AUDITE
Access:JOBSENG       &FileManager,THREAD                   ! FileManager for JOBSENG
Relate:JOBSENG       &RelationManager,THREAD               ! RelationManager for JOBSENG
Access:STMASAUD      &FileManager,THREAD                   ! FileManager for STMASAUD
Relate:STMASAUD      &RelationManager,THREAD               ! RelationManager for STMASAUD
Access:AUDSTATS      &FileManager,THREAD                   ! FileManager for AUDSTATS
Relate:AUDSTATS      &RelationManager,THREAD               ! RelationManager for AUDSTATS
Access:JOBTHIRD      &FileManager,THREAD                   ! FileManager for JOBTHIRD
Relate:JOBTHIRD      &RelationManager,THREAD               ! RelationManager for JOBTHIRD
Access:LOCATLOG      &FileManager,THREAD                   ! FileManager for LOCATLOG
Relate:LOCATLOG      &RelationManager,THREAD               ! RelationManager for LOCATLOG
Access:PRODCODE      &FileManager,THREAD                   ! FileManager for PRODCODE
Relate:PRODCODE      &RelationManager,THREAD               ! RelationManager for PRODCODE
Access:STOAUDIT      &FileManager,THREAD                   ! FileManager for STOAUDIT
Relate:STOAUDIT      &RelationManager,THREAD               ! RelationManager for STOAUDIT
Access:BOUNCER       &FileManager,THREAD                   ! FileManager for BOUNCER
Relate:BOUNCER       &RelationManager,THREAD               ! RelationManager for BOUNCER
Access:WIPAMF        &FileManager,THREAD                   ! FileManager for WIPAMF
Relate:WIPAMF        &RelationManager,THREAD               ! RelationManager for WIPAMF
Access:TEAMS         &FileManager,THREAD                   ! FileManager for TEAMS
Relate:TEAMS         &RelationManager,THREAD               ! RelationManager for TEAMS
Access:WIPAUI        &FileManager,THREAD                   ! FileManager for WIPAUI
Relate:WIPAUI        &RelationManager,THREAD               ! RelationManager for WIPAUI
Access:MODELCOL      &FileManager,THREAD                   ! FileManager for MODELCOL
Relate:MODELCOL      &RelationManager,THREAD               ! RelationManager for MODELCOL
Access:CONTHIST      &FileManager,THREAD                   ! FileManager for CONTHIST
Relate:CONTHIST      &RelationManager,THREAD               ! RelationManager for CONTHIST
Access:JOBSTAMP      &FileManager,THREAD                   ! FileManager for JOBSTAMP
Relate:JOBSTAMP      &RelationManager,THREAD               ! RelationManager for JOBSTAMP
Access:ACCESDEF      &FileManager,THREAD                   ! FileManager for ACCESDEF
Relate:ACCESDEF      &RelationManager,THREAD               ! RelationManager for ACCESDEF
Access:JOBACCNO      &FileManager,THREAD                   ! FileManager for JOBACCNO
Relate:JOBACCNO      &RelationManager,THREAD               ! RelationManager for JOBACCNO
Access:JOBRPNOT      &FileManager,THREAD                   ! FileManager for JOBRPNOT
Relate:JOBRPNOT      &RelationManager,THREAD               ! RelationManager for JOBRPNOT
Access:JOBSOBF       &FileManager,THREAD                   ! FileManager for JOBSOBF
Relate:JOBSOBF       &RelationManager,THREAD               ! RelationManager for JOBSOBF
Access:JOBSINV       &FileManager,THREAD                   ! FileManager for JOBSINV
Relate:JOBSINV       &RelationManager,THREAD               ! RelationManager for JOBSINV
Access:JOBSCONS      &FileManager,THREAD                   ! FileManager for JOBSCONS
Relate:JOBSCONS      &RelationManager,THREAD               ! RelationManager for JOBSCONS
Access:JOBSWARR      &FileManager,THREAD                   ! FileManager for JOBSWARR
Relate:JOBSWARR      &RelationManager,THREAD               ! RelationManager for JOBSWARR
Access:JOBSE2        &FileManager,THREAD                   ! FileManager for JOBSE2
Relate:JOBSE2        &RelationManager,THREAD               ! RelationManager for JOBSE2
Access:TRDSPEC       &FileManager,THREAD                   ! FileManager for TRDSPEC
Relate:TRDSPEC       &RelationManager,THREAD               ! RelationManager for TRDSPEC
Access:ORDPEND       &FileManager,THREAD                   ! FileManager for ORDPEND
Relate:ORDPEND       &RelationManager,THREAD               ! RelationManager for ORDPEND
Access:STOHIST       &FileManager,THREAD                   ! FileManager for STOHIST
Relate:STOHIST       &RelationManager,THREAD               ! RelationManager for STOHIST
Access:REPTYDEF      &FileManager,THREAD                   ! FileManager for REPTYDEF
Relate:REPTYDEF      &RelationManager,THREAD               ! RelationManager for REPTYDEF
Access:PRIORITY      &FileManager,THREAD                   ! FileManager for PRIORITY
Relate:PRIORITY      &RelationManager,THREAD               ! RelationManager for PRIORITY
Access:JOBSE         &FileManager,THREAD                   ! FileManager for JOBSE
Relate:JOBSE         &RelationManager,THREAD               ! RelationManager for JOBSE
Access:MANFAULT      &FileManager,THREAD                   ! FileManager for MANFAULT
Relate:MANFAULT      &RelationManager,THREAD               ! RelationManager for MANFAULT
Access:TRACHAR       &FileManager,THREAD                   ! FileManager for TRACHAR
Relate:TRACHAR       &RelationManager,THREAD               ! RelationManager for TRACHAR
Access:LOCVALUE      &FileManager,THREAD                   ! FileManager for LOCVALUE
Relate:LOCVALUE      &RelationManager,THREAD               ! RelationManager for LOCVALUE
Access:COURIER       &FileManager,THREAD                   ! FileManager for COURIER
Relate:COURIER       &RelationManager,THREAD               ! RelationManager for COURIER
Access:TRDPARTY      &FileManager,THREAD                   ! FileManager for TRDPARTY
Relate:TRDPARTY      &RelationManager,THREAD               ! RelationManager for TRDPARTY
Access:JOBNOTES      &FileManager,THREAD                   ! FileManager for JOBNOTES
Relate:JOBNOTES      &RelationManager,THREAD               ! RelationManager for JOBNOTES
Access:MANFAURL      &FileManager,THREAD                   ! FileManager for MANFAURL
Relate:MANFAURL      &RelationManager,THREAD               ! RelationManager for MANFAURL
Access:MODELCCT      &FileManager,THREAD                   ! FileManager for MODELCCT
Relate:MODELCCT      &RelationManager,THREAD               ! RelationManager for MODELCCT
Access:MANFAUEX      &FileManager,THREAD                   ! FileManager for MANFAUEX
Relate:MANFAUEX      &RelationManager,THREAD               ! RelationManager for MANFAUEX
Access:MANFPARL      &FileManager,THREAD                   ! FileManager for MANFPARL
Relate:MANFPARL      &RelationManager,THREAD               ! RelationManager for MANFPARL
Access:JOBPAYMT      &FileManager,THREAD                   ! FileManager for JOBPAYMT
Relate:JOBPAYMT      &RelationManager,THREAD               ! RelationManager for JOBPAYMT
Access:MANREJR       &FileManager,THREAD                   ! FileManager for MANREJR
Relate:MANREJR       &RelationManager,THREAD               ! RelationManager for MANREJR
Access:STOCK         &FileManager,THREAD                   ! FileManager for STOCK
Relate:STOCK         &RelationManager,THREAD               ! RelationManager for STOCK
Access:MANMARK       &FileManager,THREAD                   ! FileManager for MANMARK
Relate:MANMARK       &RelationManager,THREAD               ! RelationManager for MANMARK
Access:MODPROD       &FileManager,THREAD                   ! FileManager for MODPROD
Relate:MODPROD       &RelationManager,THREAD               ! RelationManager for MODPROD
Access:TRAFAULO      &FileManager,THREAD                   ! FileManager for TRAFAULO
Relate:TRAFAULO      &RelationManager,THREAD               ! RelationManager for TRAFAULO
Access:TRAFAULT      &FileManager,THREAD                   ! FileManager for TRAFAULT
Relate:TRAFAULT      &RelationManager,THREAD               ! RelationManager for TRAFAULT
Access:JOBLOHIS      &FileManager,THREAD                   ! FileManager for JOBLOHIS
Relate:JOBLOHIS      &RelationManager,THREAD               ! RelationManager for JOBLOHIS
Access:ACCESSOR      &FileManager,THREAD                   ! FileManager for ACCESSOR
Relate:ACCESSOR      &RelationManager,THREAD               ! RelationManager for ACCESSOR
Access:SUBACCAD      &FileManager,THREAD                   ! FileManager for SUBACCAD
Relate:SUBACCAD      &RelationManager,THREAD               ! RelationManager for SUBACCAD
Access:WARPARTS      &FileManager,THREAD                   ! FileManager for WARPARTS
Relate:WARPARTS      &RelationManager,THREAD               ! RelationManager for WARPARTS
Access:PARTS         &FileManager,THREAD                   ! FileManager for PARTS
Relate:PARTS         &RelationManager,THREAD               ! RelationManager for PARTS
Access:JOBACC        &FileManager,THREAD                   ! FileManager for JOBACC
Relate:JOBACC        &RelationManager,THREAD               ! RelationManager for JOBACC
Access:AUDIT         &FileManager,THREAD                   ! FileManager for AUDIT
Relate:AUDIT         &RelationManager,THREAD               ! RelationManager for AUDIT
Access:USERS         &FileManager,THREAD                   ! FileManager for USERS
Relate:USERS         &RelationManager,THREAD               ! RelationManager for USERS
Access:LOCSHELF      &FileManager,THREAD                   ! FileManager for LOCSHELF
Relate:LOCSHELF      &RelationManager,THREAD               ! RelationManager for LOCSHELF
Access:REPTYCAT      &FileManager,THREAD                   ! FileManager for REPTYCAT
Relate:REPTYCAT      &RelationManager,THREAD               ! RelationManager for REPTYCAT
Access:MANFPALO      &FileManager,THREAD                   ! FileManager for MANFPALO
Relate:MANFPALO      &RelationManager,THREAD               ! RelationManager for MANFPALO
Access:JOBSTAGE      &FileManager,THREAD                   ! FileManager for JOBSTAGE
Relate:JOBSTAGE      &RelationManager,THREAD               ! RelationManager for JOBSTAGE
Access:MANFAUPA      &FileManager,THREAD                   ! FileManager for MANFAUPA
Relate:MANFAUPA      &RelationManager,THREAD               ! RelationManager for MANFAUPA
Access:LOCINTER      &FileManager,THREAD                   ! FileManager for LOCINTER
Relate:LOCINTER      &RelationManager,THREAD               ! RelationManager for LOCINTER
Access:NOTESFAU      &FileManager,THREAD                   ! FileManager for NOTESFAU
Relate:NOTESFAU      &RelationManager,THREAD               ! RelationManager for NOTESFAU
Access:MANFAULO      &FileManager,THREAD                   ! FileManager for MANFAULO
Relate:MANFAULO      &RelationManager,THREAD               ! RelationManager for MANFAULO
Access:JOBEXHIS      &FileManager,THREAD                   ! FileManager for JOBEXHIS
Relate:JOBEXHIS      &RelationManager,THREAD               ! RelationManager for JOBEXHIS
Access:STOPARTS      &FileManager,THREAD                   ! FileManager for STOPARTS
Relate:STOPARTS      &RelationManager,THREAD               ! RelationManager for STOPARTS
Access:ORDPARTS      &FileManager,THREAD                   ! FileManager for ORDPARTS
Relate:ORDPARTS      &RelationManager,THREAD               ! RelationManager for ORDPARTS
Access:LOCATION      &FileManager,THREAD                   ! FileManager for LOCATION
Relate:LOCATION      &RelationManager,THREAD               ! RelationManager for LOCATION
Access:STOMPFAU      &FileManager,THREAD                   ! FileManager for STOMPFAU
Relate:STOMPFAU      &RelationManager,THREAD               ! RelationManager for STOMPFAU
Access:STOMJFAU      &FileManager,THREAD                   ! FileManager for STOMJFAU
Relate:STOMJFAU      &RelationManager,THREAD               ! RelationManager for STOMJFAU
Access:STOESN        &FileManager,THREAD                   ! FileManager for STOESN
Relate:STOESN        &RelationManager,THREAD               ! RelationManager for STOESN
Access:SUPPLIER      &FileManager,THREAD                   ! FileManager for SUPPLIER
Relate:SUPPLIER      &RelationManager,THREAD               ! RelationManager for SUPPLIER
Access:COUBUSHR      &FileManager,THREAD                   ! FileManager for COUBUSHR
Relate:COUBUSHR      &RelationManager,THREAD               ! RelationManager for COUBUSHR
Access:SUPVALA       &FileManager,THREAD                   ! FileManager for SUPVALA
Relate:SUPVALA       &RelationManager,THREAD               ! RelationManager for SUPVALA
Access:ESTPARTS      &FileManager,THREAD                   ! FileManager for ESTPARTS
Relate:ESTPARTS      &RelationManager,THREAD               ! RelationManager for ESTPARTS
Access:TRAHUBS       &FileManager,THREAD                   ! FileManager for TRAHUBS
Relate:TRAHUBS       &RelationManager,THREAD               ! RelationManager for TRAHUBS
Access:TRAEMAIL      &FileManager,THREAD                   ! FileManager for TRAEMAIL
Relate:TRAEMAIL      &RelationManager,THREAD               ! RelationManager for TRAEMAIL
Access:SUPVALB       &FileManager,THREAD                   ! FileManager for SUPVALB
Relate:SUPVALB       &RelationManager,THREAD               ! RelationManager for SUPVALB
Access:SUBBUSHR      &FileManager,THREAD                   ! FileManager for SUBBUSHR
Relate:SUBBUSHR      &RelationManager,THREAD               ! RelationManager for SUBBUSHR
Access:JOBS          &FileManager,THREAD                   ! FileManager for JOBS
Relate:JOBS          &RelationManager,THREAD               ! RelationManager for JOBS
Access:MODELNUM      &FileManager,THREAD                   ! FileManager for MODELNUM
Relate:MODELNUM      &RelationManager,THREAD               ! RelationManager for MODELNUM
Access:TRABUSHR      &FileManager,THREAD                   ! FileManager for TRABUSHR
Relate:TRABUSHR      &RelationManager,THREAD               ! RelationManager for TRABUSHR
Access:SUBEMAIL      &FileManager,THREAD                   ! FileManager for SUBEMAIL
Relate:SUBEMAIL      &RelationManager,THREAD               ! RelationManager for SUBEMAIL
Access:ORDERS        &FileManager,THREAD                   ! FileManager for ORDERS
Relate:ORDERS        &RelationManager,THREAD               ! RelationManager for ORDERS
Access:UNITTYPE      &FileManager,THREAD                   ! FileManager for UNITTYPE
Relate:UNITTYPE      &RelationManager,THREAD               ! RelationManager for UNITTYPE
Access:REPAIRTY      &FileManager,THREAD                   ! FileManager for REPAIRTY
Relate:REPAIRTY      &RelationManager,THREAD               ! RelationManager for REPAIRTY
Access:TRDACC        &FileManager,THREAD                   ! FileManager for TRDACC
Relate:TRDACC        &RelationManager,THREAD               ! RelationManager for TRDACC
Access:INVOICE       &FileManager,THREAD                   ! FileManager for INVOICE
Relate:INVOICE       &RelationManager,THREAD               ! RelationManager for INVOICE
Access:TRDBATCH      &FileManager,THREAD                   ! FileManager for TRDBATCH
Relate:TRDBATCH      &RelationManager,THREAD               ! RelationManager for TRDBATCH
Access:TRACHRGE      &FileManager,THREAD                   ! FileManager for TRACHRGE
Relate:TRACHRGE      &RelationManager,THREAD               ! RelationManager for TRACHRGE
Access:TRDMODEL      &FileManager,THREAD                   ! FileManager for TRDMODEL
Relate:TRDMODEL      &RelationManager,THREAD               ! RelationManager for TRDMODEL
Access:STOMODEL      &FileManager,THREAD                   ! FileManager for STOMODEL
Relate:STOMODEL      &RelationManager,THREAD               ! RelationManager for STOMODEL
Access:TRADEACC      &FileManager,THREAD                   ! FileManager for TRADEACC
Relate:TRADEACC      &RelationManager,THREAD               ! RelationManager for TRADEACC
Access:SUBCHRGE      &FileManager,THREAD                   ! FileManager for SUBCHRGE
Relate:SUBCHRGE      &RelationManager,THREAD               ! RelationManager for SUBCHRGE
Access:DEFCHRGE      &FileManager,THREAD                   ! FileManager for DEFCHRGE
Relate:DEFCHRGE      &RelationManager,THREAD               ! RelationManager for DEFCHRGE
Access:SUBTRACC      &FileManager,THREAD                   ! FileManager for SUBTRACC
Relate:SUBTRACC      &RelationManager,THREAD               ! RelationManager for SUBTRACC
Access:CHARTYPE      &FileManager,THREAD                   ! FileManager for CHARTYPE
Relate:CHARTYPE      &RelationManager,THREAD               ! RelationManager for CHARTYPE
Access:WAYBAWT       &FileManager,THREAD                   ! FileManager for WAYBAWT
Relate:WAYBAWT       &RelationManager,THREAD               ! RelationManager for WAYBAWT
Access:MANUFACT      &FileManager,THREAD                   ! FileManager for MANUFACT
Relate:MANUFACT      &RelationManager,THREAD               ! RelationManager for MANUFACT
Access:USMASSIG      &FileManager,THREAD                   ! FileManager for USMASSIG
Relate:USMASSIG      &RelationManager,THREAD               ! RelationManager for USMASSIG
Access:WEBJOB        &FileManager,THREAD                   ! FileManager for WEBJOB
Relate:WEBJOB        &RelationManager,THREAD               ! RelationManager for WEBJOB
Access:USUASSIG      &FileManager,THREAD                   ! FileManager for USUASSIG
Relate:USUASSIG      &RelationManager,THREAD               ! RelationManager for USUASSIG
Access:JOBSLOCK      &FileManager,THREAD                   ! FileManager for JOBSLOCK
Relate:JOBSLOCK      &RelationManager,THREAD               ! RelationManager for JOBSLOCK
Access:TRAHUBAC      &FileManager,THREAD                   ! FileManager for TRAHUBAC
Relate:TRAHUBAC      &RelationManager,THREAD               ! RelationManager for TRAHUBAC
Access:STOHISTE      &FileManager,THREAD                   ! FileManager for STOHISTE
Relate:STOHISTE      &RelationManager,THREAD               ! RelationManager for STOHISTE
Access:MODEXCHA      &FileManager,THREAD                   ! FileManager for MODEXCHA
Relate:MODEXCHA      &RelationManager,THREAD               ! RelationManager for MODEXCHA
Access:TRDMAN        &FileManager,THREAD                   ! FileManager for TRDMAN
Relate:TRDMAN        &RelationManager,THREAD               ! RelationManager for TRDMAN
Access:AUDIT2        &FileManager,THREAD                   ! FileManager for AUDIT2
Relate:AUDIT2        &RelationManager,THREAD               ! RelationManager for AUDIT2
Access:JOBSSL        &FileManager,THREAD                   ! FileManager for JOBSSL
Relate:JOBSSL        &RelationManager,THREAD               ! RelationManager for JOBSSL
Access:JOBSOBF_ALIAS &FileManager,THREAD                   ! FileManager for JOBSOBF_ALIAS
Relate:JOBSOBF_ALIAS &RelationManager,THREAD               ! RelationManager for JOBSOBF_ALIAS
Access:JOBPAYMT_ALIAS &FileManager,THREAD                  ! FileManager for JOBPAYMT_ALIAS
Relate:JOBPAYMT_ALIAS &RelationManager,THREAD              ! RelationManager for JOBPAYMT_ALIAS
Access:JOBS_ALIAS    &FileManager,THREAD                   ! FileManager for JOBS_ALIAS
Relate:JOBS_ALIAS    &RelationManager,THREAD               ! RelationManager for JOBS_ALIAS

FuzzyMatcher         FuzzyClass                            ! Global fuzzy matcher
GlobalErrorStatus    ErrorStatusClass,THREAD
GlobalErrors         ErrorClass                            ! Global error manager
INIMgr               INIClass                              ! Global non-volatile storage manager
GlobalRequest        BYTE(0),THREAD                        ! Set when a browse calls a form, to let it know action to perform
GlobalResponse       BYTE(0),THREAD                        ! Set to the response from the form
VCRRequest           LONG(0),THREAD                        ! Set to the request from the VCR buttons

Dictionary           CLASS,THREAD
Construct              PROCEDURE
Destruct               PROCEDURE
                     END


  CODE
  GlobalErrors.Init(GlobalErrorStatus)
  FuzzyMatcher.Init                                        ! Initilaize the browse 'fuzzy matcher'
  FuzzyMatcher.SetOption(MatchOption:NoCase, 1)            ! Configure case matching
  FuzzyMatcher.SetOption(MatchOption:WordOnly, 0)          ! Configure 'word only' matching
  INIMgr.Init('.\FixSBOnlineVettingJobs.INI', NVD_INI)     ! Configure INIManager to use INI file
  DctInit
  SYSTEM{PROP:Icon} = 'clarion.ico'
  Main
  INIMgr.Update
  INIMgr.Kill                                              ! Destroy INI manager
  FuzzyMatcher.Kill                                        ! Destroy fuzzy matcher
    
BHDebugMessage		Procedure(String fMessage)
 CODE
COMPILE('**DebugMessages**',_DEBUG_)
        Beep(Beep:SystemExclamation)  ;  Yield()
        Case Message('== DEBUG MESSAGE =='&|
             '|'&|
             '|' & Clip(fMessage) & ''&|
             '|'&|
             '|== END ==','Debug Message',|
             Icon:Exclamation,'&OK',1)
        Of 1 ! &OK Button
        End!Case Message
!**DebugMessages**
RETURN
BHAddToDebugLog		Procedure(String fMessage,<String fOverrideFilename>)
OutPutString    CSTRING(255)
 CODE
COMPILE('**DebugMessages**',_DEBUG_)
    OutputString = 'SBO_VettingFix: ' & CLIP(fMessage)
    BHOutputToDebugView(ADDRESS(OutputString))
!**DebugMessages**


Dictionary.Construct PROCEDURE

  CODE
  IF THREAD()<>1
     DctInit()
  END


Dictionary.Destruct PROCEDURE

  CODE
  DctKill()

