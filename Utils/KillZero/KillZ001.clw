

   MEMBER('KillZero.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('KILLZ001.INC'),ONCE        !Local module procedure declarations
                     END


Main                 PROCEDURE                        ! Declare Procedure
DeletingQ            QUEUE,PRE()
DeletingRecNo        LONG
                     END
rtnValue             BYTE
Count                LONG
  CODE
   Relate:JOBS.Open
   Relate:JOBSE.Open
   Relate:JOBSE2.Open
   Relate:WEBJOB.Open
   Relate:AUDIT.Open
   Relate:AUDSTATS.Open
   Relate:PARTS.Open
   Relate:WARPARTS.Open
   Relate:JOBNOTES.Open
   Relate:ESTPARTS.Open
   Relate:JOBSENG.Open
   Relate:JOBSLOCK.Open
!TB13274 - J - 16/04/14
!as requested a schedulable task to remove zero vales from jobs files

!13399 - J - 20/10/14 - they want to run this every three hours - even during working hours so I need a check on the in-use of the file
!for the non-jobs I can use a delay, but for jobs I will need to use a hold and release thingy - see jobinuse routine below
!the double loop slows things down but does check the ref_number is zero twice

!JOBS ==================================
    Loop
        Access:jobs.clearkey(job:Ref_Number_Key)
        job:Ref_Number = 0
        if access:jobs.fetch(job:Ref_Number_Key) = level:Benign then
            !message('Found a job')
            !is this one in use?
            Do JobInUse
            if rtnValue = 0 then
                !OK it can go
                relate:jobs.delete(0)
            END
        ELSE
            break !from loop to fetch all jobs
        END
    END !Loop
!JOBS ==================================


!JOBSE ==================================
    free(DeletingQ)
    clear(DeletingQ)

    !Primary loop to find zero ref_number records
    Access:jobse.clearkey(jobe:RefNumberKey)
    jobe:RefNumber = 0
    set(jobe:RefNumberKey,jobe:RefNumberKey)
    Loop
        if access:jobse.Next() then break.
        if jobe:RefNumber <> 0 then break.
        DeletingQ.DeletingRecNo = jobe:RecordNumber
        Add(DeletingQ)
    END !Primary loop to find refnumbers = 0

    !Secondary loop to check they are still zero and remove them
    Loop count = 1 to records(DeletingQ)
        get(DeletingQ,Count)
        Access:Jobse.clearkey(jobe:RecordNumberKey)
        jobe:RecordNumber = DeletingQ.DeletingRecNo
        if access:jobse.fetch(jobe:RecordNumberKey) = level:Benign
            if jobe:RefNumber = 0
                !Still zero
                relate:jobse.delete(0)
            END
        END !if jobse.fetch worked OK
    END !Loop
!JOBSE ==================================


!JOBSE2 ==================================
    free(DeletingQ)
    clear(DeletingQ)

    !Primary loop to find zero ref_number records
    Access:jobse2.clearkey(jobe2:RefNumberKey)
    jobe2:RefNumber = 0
    set(jobe2:RefNumberKey,jobe2:RefNumberKey)
    Loop
        if access:jobse2.next() then break.
        if jobe2:RefNumber <> 0 then break.
        DeletingQ.DeletingRecNo = jobe2:RecordNumber
        Add(DeletingQ)
    END !Primary Loop

    !Secondary loop
    Loop count = 1 to records(DeletingQ)
        get(DeletingQ,Count)
        Access:jobse2.clearkey(jobe2:RecordNumberKey)
        jobe2:RecordNumber = DeletingQ.DeletingRecNo
        if access:jobse2.fetch(jobe2:RecordNumberKey) = level:benign then
            if jobe2:RefNumber = 0 then
                Relate:jobse2.delete(0)
            END !if still zero
        END
    END !loop
!JOBSE2 ==================================


!WEBJOB ==================================
    free(DeletingQ)
    clear(DeletingQ)

    !Primary loop to find zero ref_number records
    Access:Webjob.clearkey(wob:RefNumberKey)
    wob:RefNumber = 0
    set(wob:RefNumberKey,wob:RefNumberKey)
    Loop
        if access:Webjob.next() then break.
        if wob:RefNumber <> 0 then break.
        DeletingQ.DeletingRecNo = wob:RecordNumber
        add(DeletingQ)
    END !Primary loop

    Loop count = 1 to records(DeletingQ)
        get(DeletingQ,Count)
        Access:Webjob.clearkey(wob:RecordNumberKey)
        wob:RecordNumber = DeletingQ.DeletingRecNo
        if access:Webjob.fetch(wob:RecordNumberKey) = level:Benign then
            !message('Found a webjob')
            if wob:RefNumber = 0
                Relate:Webjob.delete(0)
            END
        END
    END !Secondary Loop

!WEBJOB ==================================


!AUDIT ===================================
    free(DeletingQ)
    clear(DeletingQ)

    !Primary loop to find zero ref_number records
    access:Audit.clearkey(aud:Ref_Number_Key)
    aud:Ref_Number = 0
    set(aud:Ref_Number_Key,aud:Ref_Number_Key)
    Loop
        if access:Audit.next() then break.
        if aud:Ref_number <> 0 then break.
        DeletingQ.DeletingRecNo = aud:record_number
        add(DeletingQ)
    END !Primary loop

    Loop count = 1 to records(DeletingQ)
        Get(DeletingQ,Count)
        Access:Audit.clearkey(aud:Record_Number_Key)
        aud:record_number = DeletingQ.DeletingRecNo
        if access:Audit.fetch(aud:Record_Number_Key) = level:benign
            if Aud:Ref_number = 0 then
                Relate:Audit.delete(0)
            END !if 0
        END !if fetch
    END !loop through queue records
!AUDIT ===================================


!AUDSTATS ================================
    free(DeletingQ)
    clear(DeletingQ)

    !Primary loop to find zero ref_number records
    Access:Audstats.clearkey(aus:DateChangedKey)
    aus:RefNumber = 0
    set(aus:DateChangedKey,aus:DateChangedKey)
    loop
        if access:Audstats.next() then break.
        if aus:Refnumber <> 0 then break.
        DeletingQ.DeletingRecNo = aus:RecordNumber
        add(DeletingQ)
    END
    Loop count = 1 to records(DeletingQ)
        get(DeletingQ,count)
        Access:Audstats.clearkey(aus:RecordNumberKey)
        aus:RecordNumber = DeletingQ.DeletingRecNo
        if access:Audstats.fetch(aus:RecordNumberKey) = level:Benign
            if aus:RefNumber = 0
                Relate:Audstats.delete(0)
            END !if still zero
        END !if fetch worked
    END
!AUDSTATS ================================


!PARTS ===================================
    free(DeletingQ)
    clear(DeletingQ)

    !Primary loop to find zero ref_number records
    Access:parts.clearkey(par:RefPartRefNoKey)
    par:Ref_Number = 0
    set(par:RefPartRefNoKey,par:RefPartRefNoKey)
    loop
        if access:parts.next() then break.
        if par:Ref_Number <> 0 then break.
        DeletingQ.DeletingRecNo = par:Record_Number
        Add(DeletingQ)
    end !Primary loop

    Loop count = 1 to records(DeletingQ)
        Get(DeletingQ,count)
        Access:Parts.clearkey(par:recordnumberkey)
        par:Record_Number = DeletingQ.DeletingRecNo
        if access:parts.fetch(par:recordnumberkey)
            if par:Ref_Number = 0
                relate:parts.delete(0)
            END !if still 0
        END !if fetch
    END !second delayed loop

!PARTS ===================================


!WARPARTS ================================
    free(DeletingQ)
    clear(DeletingQ)

    !Primary loop to find zero ref_number records
    access:Warparts.clearkey(wpr:RefPartRefNoKey)
    wpr:Ref_Number = 0
    set(wpr:PartRefNoKey,wpr:PartRefNoKey)
    Loop
        if access:Warparts.next() then break.
        if wpr:Ref_Number <> 0 then break.
        DeletingQ.DeletingRecNo = wpr:Record_Number
        Add(DeletingQ)
    END !Primary loop

    Loop count = 1 to records(DeletingQ)
        Access:Warparts.clearkey(wpr:RecordNumberKey)
        wpr:Record_Number = DeletingQ.DeletingRecNo
        if access:Warparts.fetch(wpr:RecordNumberKey)
            if wpr:Ref_Number = 0
                Relate:Warparts.delete(0)
            END !if still 0
        END !if warparts fetched
    END !Seondary loop
!WARPARTS ================================


!ESTPARTS ================================
    free(DeletingQ)
    clear(DeletingQ)

    !Primary loop to find zero ref_number records
    Access:EstParts.clearkey(epr:Part_Ref_Number_Key)
    epr:Ref_Number = 0
    set(epr:Part_Ref_Number_Key,epr:Part_Ref_Number_Key)
    Loop
        if access:EstParts.next() then break.
        if epr:Ref_Number <> 0 then break.
        DeletingQ.DeletingRecNo = epr:Record_Number
        add(DeletingQ)
    END !primary loop

    Loop count = 1 to records(DeletingQ)
        get(DeletingQ,count)
        Access:EstPArts.clearkey(epr:record_number_key)
        epr:Record_Number = DeletingQ.DeletingRecNo
        if access:EstParts.fetch(epr:record_number_key)
            If epr:Ref_Number = 0
                Relate:EstParts.delete(0)
            END !if still zero
        END !if fetch
    END !Secondary loop
!ESTPARTS ================================


!JOBNOTES ================================
!NB no record number on jobnotes - just try a fetch
    Loop

        Access:jobnotes.clearkey(jbn:RefNumberKey)
        jbn:RefNumber = 0
        set(jbn:RefNumberKey,jbn:RefNumberKey)

        if access:jobnotes.next() then break.
        if jbn:RefNumber <>  0 then break.

        !message('Found a jobnote:'&jbn:Refnumber)
        relate:Jobnotes.delete(0)
        
    END
!JOBNOTES ================================


JobInUse    Routine 

    rtnValue = 0

    pointer# = Pointer(JOBS)
    Hold(JOBS,1)
    Get(JOBS,pointer#)
    if (errorcode()  = 43)
        rtnValue = 1
    END


!Jobs lock cannot be used for ref_Number = 0 ... there should be no entries for this anyway
!    else
!        
!        ! Account for jobs locked by SBOnline (DBH: 15/02/2010)
!        relate:JOBSLOCK.open()
!
!        ! First step. Remove any blanks from the file
!        Access:JOBSLOCK.ClearKey(lock:JobNumberKey)
!        lock:JobNumber = 0
!        SET(lock:JobNumberKey,lock:JobNumberKey)
!        LOOP
!            IF (Access:JOBSLOCK.Next())
!                BREAK
!            END
!            IF (lock:JobNumber <> 0)
!                BREAK
!            END
!            Access:JOBSLOCK.DeleteRecord(0)
!        END
!
!        access:JOBSLOCK.clearkey(lock:jobNumberKey)
!        lock:jobNUmber = job:ref_number
!        if (access:JOBSLOCK.tryfetch(lock:jobNumberKey) = level:benign)
!            if (TODAY() = lock:DateLocked and clock() < (lock:timelocked + 60000)) ! 10 Minutes Time Out
!            ! If it's the same user then there's no need to give the locked error?!
!                rtnValue = 1
!            else
!                ! Delete the record if accessed via Fat/Thin CLient (DBH: 15/02/2010)
!                access:JOBSLOCK.deleteRecord(0)
!            end
!        end
!        relate:JOBSLOCK.close()
!    end ! if (errorcode()  = 43)
   Relate:JOBS.Close
   Relate:JOBSE.Close
   Relate:JOBSE2.Close
   Relate:WEBJOB.Close
   Relate:AUDIT.Close
   Relate:AUDSTATS.Close
   Relate:PARTS.Close
   Relate:WARPARTS.Close
   Relate:JOBNOTES.Close
   Relate:ESTPARTS.Close
   Relate:JOBSENG.Close
   Relate:JOBSLOCK.Close
