

   MEMBER('AuditToAudit2.clw')                        ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('AUDIT001.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

locStartDate         DATE
locEndDate           DATE
window               WINDOW('Convert Audit To Audit2'),AT(,,228,108),FONT('Tahoma',8,,FONT:regular),COLOR(COLOR:White),CENTER,TILED
                       PROMPT('This routine will popuplate the AUDIT and AUDIT2 file from OAUDIT'),AT(4,4),USE(?Prompt1),TRN
                       PROMPT('Enter the date range of Audit Entries to copy'),AT(36,26),USE(?Prompt4),TRN
                       PROMPT('Start Date'),AT(40,44),USE(?locStartDate:Prompt),TRN
                       ENTRY(@d17),AT(96,44,60,10),USE(locStartDate),FONT('MS Sans Serif',,,)
                       BUTTON('...'),AT(160,44,12,10),USE(?PopCalendar)
                       PROMPT('End Date'),AT(40,62),USE(?locEndDate:Prompt),TRN
                       ENTRY(@d17),AT(96,62,60,10),USE(locEndDate),FONT('MS Sans Serif',,,)
                       BUTTON('...'),AT(160,62,12,10),USE(?PopCalendar:2)
                       BUTTON('Begin'),AT(4,90,47,14),USE(?btnBegin)
                       BUTTON('Cancel'),AT(172,90,47,14),USE(?btnCancel)
                       PROMPT(''),AT(60,92,100,10),USE(?promptStatus),CENTER,FONT(,,COLOR:Navy,,CHARSET:ANSI),COLOR(COLOR:White)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  locStartDate = DEFORMAT('01/01/2000',@d06)
  locEndDate = TODAY()
  OPEN(window)
  SELF.Opened=True
  ?locStartDate{Prop:Alrt,255} = MouseLeft2
  ?locEndDate{Prop:Alrt,255} = MouseLeft2
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          locStartDate = TINCALENDARStyle1(locStartDate)
          Display(?locStartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          locEndDate = TINCALENDARStyle1(locEndDate)
          Display(?locEndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?btnBegin
      ThisWindow.Update
      IF (locStartDate = '' OR locEndDate = '')
          CYCLE
      END ! IF
      IF (locStartDate > locEndDate)
          CYCLE
      END
      
      BEEP(BEEP:SystemQuestion)  ;  YIELD()
      CASE MESSAGE('Ready to begin?','ServiceBase',|
                     ICON:Question,'&Yes|&No',2) 
      OF 1 ! &Yes Button
      OF 2 ! &No Button
          CYCLE
      END!CASE MESSAGE
      
      count# = ConvertProcess(locStartdate,locEndDate)
      
      BEEP(BEEP:SystemAsterisk)  ;  YIELD()
      CASE MESSAGE('Completed.'&|
          '|'&|
          '|Records Updated: ' & CLIP(count#) & '','ServiceBase',|
                     ICON:Asterisk,'&OK',1) 
      OF 1 ! &OK Button
      END!CASE MESSAGE
    OF ?btnCancel
      ThisWindow.Update
      POST(EVENT:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?locStartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?locEndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

ConvertProcess PROCEDURE (DATE pStartDate,DATE pEndDate) !Generated from procedure template - Process

Progress:Thermometer BYTE
locStartDate         DATE
locEndDate           DATE
locRecordCount       LONG
Process:View         VIEW(OAUDIT)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),FONT('Tahoma',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisProcess          CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                    !Progress Manager
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(locRecordCount)


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ConvertProcess')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  locStartDate = pStartDate
  locEndDate = pEndDate
  Relate:AUDIT.Open
  Relate:OAUDIT.Open
  Access:AUDIT2.UseFile
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisProcess.Init(Process:View, Relate:OAUDIT, ?Progress:PctText, Progress:Thermometer, ProgressMgr, oaud:Date)
  ThisProcess.AddSortOrder(oaud:DateActionJobKey)
  ThisProcess.AddRange(oaud:Date,locStartDate,locEndDate)
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}='Converting...'
  SELF.AddItem(?Progress:Cancel, RequestCancelled)
  SEND(OAUDIT,'QUICKSCAN=on')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:OAUDIT.Close
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisProcess.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
      Access:AUDIT.ClearKey(aud:Record_Number_Key)
      aud:Record_Number = oaud:Record_Number
      IF (Access:AUDIT.TryFetch(aud:Record_Number_Key))
  
          IF (Access:AUDIT.PrimeRecord() = Level:Benign)
              aud:record_number = oaud:record_number
              aud:Ref_Number = oaud:Ref_Number
              aud:Date = oaud:Date
              aud:Time = oaud:Time
              aud:User = oaud:User
              aud:Action = oaud:Action
              aud:Type = oaud:Type
              IF (Access:AUDIT.TryInsert())
                  Access:AUDIT.CancelAutoInc()
              END ! IF
              IF (Access:AUDIT2.PrimeRecord() = Level:Benign)
                  aud2:AUDRecordNumber = aud:Record_Number
                  aud2:Notes = oaud:Notes
                  IF (Access:AUDIT2.TryInsert())
                      Access:AUDIT2.CancelAutoInc()
                  END ! !IF
              END ! IF
              locRecordCount += 1
              ?Progress:UserString{prop:Text} = 'Records Updated: ' & locRecordCount
          END ! IF
      END ! IF
  ReturnValue = PARENT.TakeRecord()
  RETURN ReturnValue

