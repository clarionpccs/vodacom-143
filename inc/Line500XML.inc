    SECTION('Data')
!region Data
seq                 LONG
RepositoryDir       CSTRING('C:\ServiceBaseMQ\MQ_Repository<0>{224}')
save_invoice_id     USHORT,AUTO
save_tradeacc_id    USHORT,AUTO
save_subtracc_id    USHORT,AUTO
tmp:STFMessage      STRING(30)
tmp:AOWMessage      STRING(30)
tmp:RIVMessage      STRING(30)
tmp:TestRecord      STRING(255),STATIC
TestRecord          File,Driver('ASCII'),Pre(test),Name(tmp:TestRecord),Create,Bindable,Thread
Record                  Record
TestLine                    String(1)
                        End
                    End
! ================================================================
! Xml Export FILE structure
fileXmlExport       FILE,DRIVER('ASCII'),CREATE,BINDABLE,THREAD
Record                  RECORD
recbuff                     STRING(256)
                        END
                    END
! Xml Export Class Instance
objXmlExport        XmlExport
! ================================================================
XmlData             group,type
ServiceCode             string(3)
REC                     group
SB_PO_NO                    string(10)
ORACLE_PO_NO                string(10)
URL                         string(256)
TYPE                        string(3)
                        end
AOW                     group,over(REC)
TRANS_DESC                  string(50)
BILL_AC_NO                  string(10)
SB_REPAIR_NO                string(20)
SB_INV_NO                   string(10)
AMT_EXCL_VAT                string(19)
VAT_RATE                    string(19)
COST                        string(19)
INV_DATE                    string(10)
INV_DUE_DATE                string(10)
DESPATCH_DATE               string(10)
DATE_REQUIRED               string(10)
SERVICE_CODE                string(20)
                        end
RIV                     group,over(REC)
TRANS_DESC                  string(50)
MAIN_RETAIL_AC              String(20)
SUB_RETAIL_AC               String(20)
FRANCHISE_BRANCH            String(20)
SB_INV_NO                   string(10)
AMT_EXCL_VAT                string(19)
VAT_RATE                    string(19)
COST                        string(19)
INV_DATE                    string(10)
INV_DUE_DATE                string(10)
DESPATCH_DATE               string(10)
DATE_REQUIRED               string(10)
SERVICE_CODE                string(20)
                        end
STF                     group,over(REC)
TRANS_DESC                  string(50)
BILL_AC_NO                  string(10)
CUST_ORD_NO                 string(20)
SB_INV_NO                   string(10)
AMT_EXCL_VAT                string(19)
VAT_RATE                    string(19)
COST                        string(19)
QTY                         string(10)
INV_DATE                    string(10)
INV_DUE_DATE                string(10)
DESPATCH_DATE               string(10)
DATE_REQUIRED               string(10)
SERVICE_CODE                string(20)
                        end
POR                     group,over(REC)
CURRENCY_CODE               string(3)
CURRENCY_UNIT_PRICE         string(19)
DELIV_TO_REQ                string(10)
DEST_ORG_ID                 string(20)
GL_DATE                     string(10)
INTERFACE_SRC               string(20)
ITEM_ID                     string(20)  ! Length ?????
ORG_ID                      string(2)
PREPARER_ID                 string(10)
QTY                         string(10)
SUPPLIER_NUMBER             string(15)
SB_PO_NUMBER                string(10)
TYPE                        string(3)
                        end
                    end

                    map
SendXML                 procedure(const *XmlData aXmlData, string aRepositoryDir)
CreateTestRecord        PROCEDURE(String f:Path),BYTE
                    end

XmlValues           GROUP(XmlData)
                    END
!endregion
    SECTION('Code')
!region Code
! ================================================================
! Initialise Xml Export Object
    objXmlExport.FInit(fileXmlExport)
! ================================================================
    ! Check default to see if should create messages - TrkBs: 5110 (DBH: 20-07-2005)
    If GETINI('XML','CreateLine500',,Clip(Path()) & '\SB2KDEF.INI') <> 1
        Return
    End ! If GETINI('XML','CreateLine500',,Clip(Path()) & '\SB2KDEF.INI') <> True

    ! Lookup General Default for XML repository folder - TrkBs: 5110 (DBH: 29-06-2005)
    RepositoryDir               = GETINI('XML', 'RepositoryFolder',, Clip(Path()) & '\SB2KDEF.INI')

    If CreateTestRecord(RepositoryDir) = False
        Return
    End ! If CreateTestRecord(RepositoryDir) = False

    ! Start - Prime the ServiceCode just incase the defaults haven't been set - TrkBs: 6178 (DBH: 01-08-2005)
    tmp:STFMessage = GETINI('XML','STFDescription',,Clip(Path()) & '\SB2KDEF.INI')
    If tmp:STFMessage = ''
        tmp:STFMessage = 'STF'
    End ! If tmp:STFMessage = ''

    tmp:AOWMessage = GETINI('XML','AOWDescription',,Clip(Path()) & '\SB2KDEF.INI')
    If tmp:AOWMessage = ''
        tmp:AOWMessage = 'AOW'
    End ! If tmp:STFMessage = ''

    tmp:RIVMessage = GETINI('XML','RIVDescription',,Clip(Path()) & '\SB2KDEF.INI')
    If tmp:RIVMessage = ''
        tmp:RIVMessage = 'RIV'
    End ! If tmp:RIVMessage = ''
    ! End   - Prime the ServiceCode just incase the defaults haven't been set - TrkBs: 6178 (DBH: 01-08-2005)

    ! Send XML Message

    ! Manually open Invoice, but assume all other files are open - TrkBs: 5110 (DBH: 29-06-2005)

    Relate:INVOICE.Open()
    save_INVOICE_id = Access:INVOICE.SaveFile()

    Case f:ID
    Of 'STF'
        Do CreateSTF

    Of 'AOW'
        Do CreateAOW

    Of 'RIV'
        Do CreateRIV
    End ! Case f:ID

    Access:INVOICE.RestoreFile(save_INVOICE_id)
    Relate:INVOICE.Close()
!endregion
!region Routines
CreateSTF           Routine !region
    Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
    inv:Invoice_Number  = ret:Invoice_Number
    If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
    ! Found

    Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
    ! Error
    End ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign

    clear(XmlValues)
    XmlValues.ServiceCode       = 'STF'
    XmlValues.STF.TRANS_DESC    = 'Retail Sale To Franchise'
    save_SUBTRACC_id = Access:SUBTRACC.SaveFile()
    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = ret:Account_Number
    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        ! Found
        XmlValues.STF.BILL_AC_NO    = Sub(sub:Line500AccountNumber, 1, 10) ! Retail Sale Account Number
    Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        ! Error
        XmlValues.STF.BILL_AC_NO    = ''
    End ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
    Access:SUBTRACC.RestoreFile(save_SUBTRACC_id)
    
    XmlValues.STF.CUST_ORD_NO   = Format(ret:Ref_Number, @n010) ! Retail Sale Number
    XmlValues.STF.SB_INV_NO     = 'Z' & Format(ret:Invoice_Number, @n09) ! Retail Sale Invoice Number
    XmlValues.STF.AMT_EXCL_VAT  = Round(ret:Invoice_Sub_Total, .01) ! Sale Amount (Excl Vat)
    XmlValues.STF.VAT_RATE      = Round(inv:Vat_Rate_Retail, .01)
    XmlValues.STF.COST          = Round(ret:Invoice_Sub_Total + (ret:Invoice_Sub_Total * inv:Vat_Rate_Retail / 100), .01)
    XmlValues.STF.QTY           = '1'   ! Always 1
    XmlValues.STF.INV_DATE      = Format(ret:Invoice_Date, @d06b)
    XmlValues.STF.INV_DUE_DATE  = Format(ret:Invoice_Date, @d06b)
    XmlValues.STF.DESPATCH_DATE = Format(ret:Invoice_Date, @d06b)
    XmlValues.STF.DATE_REQUIRED = Format(ret:Invoice_Date, @d06b)
    ! Use Service Code default - TrkBs: 6178 (DBH: 01-08-2005)
    XmlValues.STF.SERVICE_CODE  = Clip(tmp:STFMessage)! 'STF'   ! string(20)
    SendXml(XmlValues, RepositoryDir)
    !endregion
CreateAOW           Routine !region
    Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
    inv:Invoice_Number  = job:Invoice_Number
    If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
    ! Found

    Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
    ! Error
    End ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign

    ! Inserting (DBH 12/01/2006) #6997 - Reget the child files.. just incase
    Access:JOBSE.Clearkey(jobe:RefNumberKey)
    jobe:RefNumber  = job:Ref_Number
    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        ! Found

    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        ! Error
    End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

    Access:WEBJOB.Clearkey(wob:RefNumberKey)
    wob:RefNumber   = job:Ref_Number
    If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        ! Found

    Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        ! Error
    End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
    ! End (DBH 12/01/2006) #6997

    clear(XmlValues)
    XmlValues.ServiceCode    = 'AOW'
    XmlValues.AOW.TRANS_DESC = 'ARC Out Of Warranty Repair'

    save_TRADEACC_id = Access:TRADEACC.SaveFile()
    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number  = wob:HeadAccountNumber
    If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        ! Found
        ! Get the Trade Account details of the booking account - TrkBs: 5110 (DBH: 13-07-2005)
        If jobe:WebJob = 1 And ~glo:WebJob
            ! For a RRC job being invoiced at the ARC, use the RRC's account number - TrkBs: 5110 (DBH: 17-08-2005)
            XmlValues.AOW.BILL_AC_NO = Sub(tra:Line500AccountNumber, 1, 10) ! Booking Account Number
        End ! If jobe:WebJob = 1

        XmlValues.AOW.SB_REPAIR_NO  = Clip(job:Ref_Number) & '-' & Clip(tra:BranchIdentification) & Clip(wob:JobNumber)
        If glo:WebJob
            ! This is a "company owned" RRC, use it's Branch ID. - TrkBs: 6178 (DBH: 01-08-2005)
            XmlValues.AOW.SB_INV_NO     = 'Z' & Format(Clip(inv:Invoice_Number) & Clip(tra:BranchIdentification),@n09)
        End ! If glo:WebJob
    Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
    ! Error
    End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
    Access:TRADEACC.RestoreFile(save_TRADEACC_id)

    save_SUBTRACC_id = Access:SUBTRACC.SaveFile()
    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = job:Account_Number
    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        ! Found
        If ~jobe:WebJob Or glo:WebJob
            ! For ARC job, or RRC invoice, use the account number from the job account - TrkBs: 5110 (DBH: 17-08-2005)
            XmlValues.AOW.BILL_AC_NO = Sub(sub:Line500AccountNumber, 1, 10) ! Booking Account Number
        End ! If ~jobe:WebJob Or glo:WebJob
    Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        ! Error
    End ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
    Access:SUBTRACC.RestoreFile(save_SUBTRACC_id)

    If ~glo:WebJob
        ! This is NOT a "company owned" RRC. Use the ARC's Branch ID (DBH: 01-08-2005)
        save_TRADEACC_id = Access:TRADEACC.SaveFile()
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = GETINI('BOOKING', 'HeadAccount',, CLIP(PATH()) & '\SB2KDEF.INI')
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            ! Found
            ! Get the Trade Account details of the ARC - TrkBs: 5110 (DBH: 13-07-2005)
            XmlValues.AOW.SB_INV_NO     = 'Z' & Format(Clip(inv:Invoice_Number) & Clip(tra:BranchIdentification),@n09)
        Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        ! Error
        End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        Access:TRADEACC.RestoreFile(save_TRADEACC_id)
    End ! If ~glo:WebJob

    If glo:WebJob
        ! This is a "company owned" RRC. Use the RRC costs - TrkBs: 6178 (DBH: 01-08-2005)
        XmlValues.AOW.AMT_EXCL_VAT     = Round(job:Invoice_Courier_Cost + | ! RRC Invoice Sub Total
            jobe:InvRRCCLabourCost +   |
            jobe:InvRRCCPartsCost, .01)
        
        XmlValues.AOW.COST             = Round(XmlValues.AOW.AMT_EXCL_VAT + |
            (job:Invoice_Courier_Cost * (inv:Vat_Rate_Labour / 100) + | ! RRC Invoice VAT
            jobe:InvRRCCLabourCost * (inv:Vat_Rate_Parts / 100) +    |
            jobe:InvRRCCPartsCost * (inv:Vat_Rate_Labour / 100)), .01)
    Else ! If glo:WebJob
        ! This is NOT a "company owned" RRC. Use the ARC costs - TrkBs: 6178 (DBH: 01-08-2005)
        XmlValues.AOW.AMT_EXCL_VAT  = Round(job:Invoice_Courier_Cost + | ! Invoice Sub Total
            job:Invoice_Labour_Cost +  |
            job:Invoice_Parts_Cost, .01)
        XmlValues.AOW.COST          = Round(XmlValues.AOW.AMT_EXCL_VAT + |
            (job:Invoice_Courier_Cost * (inv:Vat_Rate_Labour / 100) + | ! Invoice VAT Rate
            job:Invoice_Parts_Cost * (inv:Vat_Rate_Parts / 100) +    |
            job:Invoice_Labour_Cost * (inv:Vat_Rate_Labour / 100)), .01)
    End ! If glo:WebJob
    ! As we can only use one Vat Rate, will use the Labour one - TrkBs: 5110 (DBH: 05-08-2005)
    XmlValues.AOW.VAT_RATE      = Round(inv:Vat_Rate_Labour, .01)
! Changing Code (DBH, 11/15/2005) - #6724 Not taking account of the RRC invoicing at a later date
!    XmlValues.AOW.INV_DATE      = Format(inv:Date_Created, @d06b)
!    XmlValues.AOW.INV_DUE_DATE  = Format(inv:Date_Created, @d06b)
!    XmlValues.AOW.DESPATCH_DATE = Format(inv:Date_Created, @d06b)
!    XmlValues.AOW.DATE_REQUIRED = Format(inv:Date_Created, @d06b)
!  to  (DBH, 11/15/2005) - #6724 :-
    If glo:WebJob
        ! If the RRC is creating the invoice, use the RRC invoice date (DBH: 15-11-2005)
        XmlValues.AOW.INV_DATE      = Format(inv:RRCInvoiceDate, @d06b)
        XmlValues.AOW.INV_DUE_DATE  = Format(inv:RRCInvoiceDate, @d06b)
        XmlValues.AOW.DESPATCH_DATE = Format(inv:RRCInvoiceDate, @d06b)
        XmlValues.AOW.DATE_REQUIRED = Format(inv:RRCInvoiceDate, @d06b)
    Else ! If glo:WebJob
        ! Use the ARC invoice date (DBH: 15-11-2005)
        XmlValues.AOW.INV_DATE      = Format(inv:ARCInvoiceDate, @d06b)
        XmlValues.AOW.INV_DUE_DATE  = Format(inv:ARCInvoiceDate, @d06b)
        XmlValues.AOW.DESPATCH_DATE = Format(inv:ARCInvoiceDate, @d06b)
        XmlValues.AOW.DATE_REQUIRED = Format(inv:ARCInvoiceDate, @d06b)
    End ! If glo:WebJob
! End (DBH, 11/15/2005) - #6724
    XmlValues.AOW.SERVICE_CODE  = Clip(tmp:AOWMessage)!'AOW'  ! string(20)
    SendXml(XmlValues, RepositoryDir)
    !endregion
CreateRIV           Routine !region
        Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
        inv:Invoice_Number  = job:Invoice_Number
        If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
    ! Found

        Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
    ! Error
        End ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign

    ! Inserting (DBH 12/01/2006) #6997 - Reget the child files.. just incase
        Access:JOBSE.Clearkey(jobe:RefNumberKey)
        jobe:RefNumber  = job:Ref_Number
        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        ! Found

        Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        ! Error
        End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

        Access:WEBJOB.Clearkey(wob:RefNumberKey)
        wob:RefNumber   = job:Ref_Number
        If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        ! Found

        Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        ! Error
        End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
    ! End (DBH 12/01/2006) #6997

        If glo:WebJob
        ! Changing (DBH 11/22/2005) #6774 - RIV is only produced for Generic Account, otherwise nothing (except for company owned RRCs)
        !         save_TRADEACC_id = Access:TRADEACC.SaveFile()
        !         Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        !         tra:Account_Number  = wob:HeadAccountNumber
        !         If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        !             ! Found
        !             ! Is this a company owned RRC?
        !             local:CompanyOwned = tra:CompanyOwned
        !         Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        !             ! Error
        !         End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        !         Access:TRADEACC.RestoreFile(save_TRADEACC_id)
        !
        !         If local:CompanyOwned = True
        !             save_SUBTRACC_id = Access:SUBTRACC.SaveFile()
        !             Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
        !             sub:Account_Number  = job:Account_Number
        !             If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !                 ! Found
        !                 If sub:Generic_Account <> True
        !                     ! This is a "company owned" RRC, repairing a non generic account. No RIV, just AOW - TrkBs: 6178 (DBH: 18-08-2005)
        !                     Do CreateAOW
        !                     Exit
        !                 End ! If sub:Generic_Account = True
        !             Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !                 ! Error
        !             End ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !             Access:SUBTRACC.RestoreFile(save_SUBTRACC_id)
        !        End ! If local:CompanyOwned = True
        ! to (DBH 11/22/2005) #6774
            save_SUBTRACC_id = Access:SUBTRACC.SaveFile()
            Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
            sub:Account_Number  = job:Account_Number
            If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
            ! Found
                If sub:Generic_Account <> True
                ! This is not a Generic Account so no message, except for "Company Owned" franchises which produce an AOW - TrkBs: 6774 (DBH: 22-11-2005)
                    save_TRADEACC_id = Access:TRADEACC.SaveFile()
                    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                    tra:Account_Number  = wob:HeadAccountNumber
                    If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    ! Found
                        IF tra:CompanyOwned = True
                            Do CreateAOW
                            Exit
                        End ! IF tra:CompanyOwned = True
                    Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    ! Error
                    End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    Access:TRADEACC.RestoreFile(save_TRADEACC_id)
                ! Not a "Company Owned" Franchise. Don't produce anything - TrkBs: 6774 (DBH: 22-11-2005)
                    Exit
                End ! If sub:Generic_Account <> True
            Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
            ! Error
            End ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
            Access:SUBTRACC.RestoreFile(save_SUBTRACC_id)
       ! End (DBH 11/22/2005) #6774
        End ! If glo:WebJob

        clear(XmlValues)
        XmlValues.ServiceCode    = 'RIV'
        XmlValues.RIV.TRANS_DESC = 'Retailer Invoice'

        save_SUBTRACC_id = Access:SUBTRACC.SaveFile()
        Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
        sub:Account_Number  = job:Account_Number
        If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        ! Found
            save_TRADEACC_id = Access:TRADEACC.SaveFile()
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number  = sub:Main_Account_Number
            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            ! Found
                XmlValues.RIV.MAIN_RETAIL_AC = tra:Line500AccountNumber
            Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            ! Error
            End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            Access:TRADEACC.RestoreFile(save_TRADEACC_id)
        Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        ! Error
        End ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        Access:SUBTRACC.RestoreFile(save_SUBTRACC_id)

        XmlValues.RIV.SUB_RETAIL_AC = job:Account_Number
        XmlValues.RIV.FRANCHISE_BRANCH = wob:HeadAccountNumber

        save_TRADEACC_id = Access:TRADEACC.SaveFile()
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = wob:HeadAccountNumber
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        ! Found
        ! Get the Trade Account details of the booking account - TrkBs: 5110 (DBH: 13-07-2005)
            XmlValues.RIV.SB_INV_NO     = 'Z' & Format(Clip(inv:Invoice_Number) & Clip(tra:BranchIdentification),@n09)
        Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
    ! Error
        End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        Access:TRADEACC.RestoreFile(save_TRADEACC_id)

        XmlValues.RIV.AMT_EXCL_VAT     = Round(job:Invoice_Courier_Cost + | ! RRC Invoice Sub Total
            jobe:InvRRCCLabourCost +   |
            jobe:InvRRCCPartsCost, .01)
    ! As we can only use one Vat Rate, will use the Labour one - TrkBs: 5110 (DBH: 05-08-2005)

        XmlValues.RIV.VAT_RATE         = Round(inv:Vat_Rate_Labour, .01)
        XmlValues.RIV.COST          = Round(XmlValues.RIV.AMT_EXCL_VAT + |
            (job:Invoice_Courier_Cost * (inv:Vat_Rate_Labour / 100) + | ! RRC Invoice VAT
            jobe:InvRRCCLabourCost * (inv:Vat_Rate_Parts / 100) +    |
            jobe:InvRRCCPartsCost * (inv:Vat_Rate_Labour / 100)), .01)
! Changing Code (DBH, 11/15/2005) - #6724 Not taking account of the RRC invoicing at a later date
!    XmlValues.RIV.INV_DATE      = Format(inv:Date_Created, @d06b)
!    XmlValues.RIV.INV_DUE_DATE  = Format(inv:Date_Created, @d06b)
!    XmlValues.RIV.DESPATCH_DATE = Format(inv:Date_Created, @d06b)
!    XmlValues.RIV.DATE_REQUIRED = Format(inv:Date_Created, @d06b)
!  to  (DBH, 11/15/2005) - #6724 :-
        XmlValues.RIV.INV_DATE      = Format(inv:RRCInvoiceDate, @d06b)
        XmlValues.RIV.INV_DUE_DATE  = Format(inv:RRCInvoiceDate, @d06b)
        XmlValues.RIV.DESPATCH_DATE = Format(inv:RRCInvoiceDate, @d06b)
        XmlValues.RIV.DATE_REQUIRED = Format(inv:RRCInvoiceDate, @d06b)
! End (DBH, 11/15/2005) - #6724
        XmlValues.RIV.SERVICE_CODE  = Clip(tmp:RIVMessage)!'RIV'  ! string(20)
        SendXml(XmlValues, RepositoryDir)
        !endregion
!endregion
!region Procedures  
SendXML             procedure(aXmlData, aRepositoryDir)
TheDate                 date
TheTime                 time
MsgId                   string(24)

    code

        TheDate = today()
        TheTime = clock()
        MsgId = 'MSG' & format(TheDate, @d012) & format(TheTime, @t05) & format(TheTime % 100, @n02) & format(seq, @n05)
        seq += 1
        if seq > 99999 then
            seq = 1
        end

        if objXmlExport.FOpen(clip(aRepositoryDir) & '\' & clip(aXmlData.ServiceCode) & '\' & MsgId & '.xml', true) = level:benign then
            objXmlExport.OpenTag('VODACOM_MESSAGE', 'version="1.0"')
            objXmlExport.OpenTag('MESSAGE_HEADER', 'direction="REQ"')
            objXmlExport.WriteTag('SRC_SYSTEM', '')
            objXmlExport.WriteTag('SRC_APPLICATION', '')
            objXmlExport.WriteTag('SERVICING_APPLICATION', '')
            objXmlExport.WriteTag('ACTION_CODE', 'UPD')
            objXmlExport.WriteTag('SERVICE_CODE', clip(aXmlData.ServiceCode))
            objXmlExport.WriteTag('MESSAGE_ID', '')
            objXmlExport.WriteTag('USER_NAME', '')
            objXmlExport.WriteTag('TOKEN', '')
            objXmlExport.CloseTag('MESSAGE_HEADER')
            objXmlExport.OpenTag('MESSAGE_BODY')

            case aXmlData.ServiceCode
            of 'AOW'
                objXmlExport.OpenTag('AOW_UPDATE_REQUEST', 'version="1.0"')
                objXmlExport.WriteTag('TRANS_DESC', clip(aXmlData.AOW.TRANS_DESC))
                objXmlExport.WriteTag('BILL_AC_NO', clip(aXmlData.AOW.BILL_AC_NO))
                objXmlExport.WriteTag('SB_REPAIR_NO', clip(aXmlData.AOW.SB_REPAIR_NO))
                objXmlExport.WriteTag('SB_INV_NO', clip(aXmlData.AOW.SB_INV_NO))
                objXmlExport.WriteTag('AMT_EXCL_VAT', clip(aXmlData.AOW.AMT_EXCL_VAT))
                objXmlExport.WriteTag('VAT_RATE', clip(aXmlData.AOW.VAT_RATE))
                objXmlExport.WriteTag('COST', clip(aXmlData.AOW.COST))
                objXmlExport.WriteTag('INV_DATE', clip(aXmlData.AOW.INV_DATE))
                objXmlExport.WriteTag('INV_DUE_DATE', clip(aXmlData.AOW.INV_DUE_DATE))
                objXmlExport.WriteTag('DESPATCH_DATE', clip(aXmlData.AOW.DESPATCH_DATE))
                objXmlExport.WriteTag('DATE_REQUIRED', clip(aXmlData.AOW.DATE_REQUIRED))
                objXmlExport.WriteTag('SERVICE_CODE', clip(aXmlData.AOW.SERVICE_CODE))
                objXmlExport.WriteTag('RESEND_YN', 'N')
                objXmlExport.CloseTag('AOW_UPDATE_REQUEST')

            of 'RIV'
                objXmlExport.OpenTag('RIV_UPDATE_REQUEST', 'version="1.0"')
                objXmlExport.WriteTag('TRANS_DESC', clip(aXmlData.RIV.TRANS_DESC))
                objXmlExport.WriteTag('MAIN_RETAIL_AC', clip(aXmlData.RIV.MAIN_RETAIL_AC))
                objXmlExport.WriteTag('SUB_RETAIL_AC', clip(aXmlData.RIV.SUB_RETAIL_AC))
                objXmlExport.WriteTag('FRANCHISE_BRANCH', clip(aXmlData.RIV.FRANCHISE_BRANCH))
                objXmlExport.WriteTag('SB_INV_NO', clip(aXmlData.RIV.SB_INV_NO))
                objXmlExport.WriteTag('AMT_EXCL_VAT', clip(aXmlData.RIV.AMT_EXCL_VAT))
                objXmlExport.WriteTag('VAT_RATE', clip(aXmlData.RIV.VAT_RATE))
                objXmlExport.WriteTag('COST', clip(aXmlData.RIV.COST))
                objXmlExport.WriteTag('INV_DATE', clip(aXmlData.RIV.INV_DATE))
                objXmlExport.WriteTag('INV_DUE_DATE', clip(aXmlData.RIV.INV_DUE_DATE))
                objXmlExport.WriteTag('DESPATCH_DATE', clip(aXmlData.RIV.DESPATCH_DATE))
                objXmlExport.WriteTag('DATE_REQUIRED', clip(aXmlData.RIV.DATE_REQUIRED))
                objXmlExport.WriteTag('SERVICE_CODE', clip(aXmlData.RIV.SERVICE_CODE))
                objXmlExport.WriteTag('RESEND_YN', 'N')
                objXmlExport.CloseTag('RIV_UPDATE_REQUEST')

            of 'STF'
                objXmlExport.OpenTag('STF_UPDATE_REQUEST', 'version="1.0"')
                objXmlExport.WriteTag('TRANS_DESC', clip(aXmlData.STF.TRANS_DESC))
                objXmlExport.WriteTag('BILL_AC_NO', clip(aXmlData.STF.BILL_AC_NO))
                objXmlExport.WriteTag('CUST_ORD_NO', clip(aXmlData.STF.CUST_ORD_NO))
                objXmlExport.WriteTag('SB_INV_NO', clip(aXmlData.STF.SB_INV_NO))
                objXmlExport.WriteTag('AMT_EXCL_VAT', clip(aXmlData.STF.AMT_EXCL_VAT))
                objXmlExport.WriteTag('VAT_RATE', clip(aXmlData.STF.VAT_RATE))
                objXmlExport.WriteTag('COST', clip(aXmlData.STF.COST))
                objXmlExport.WriteTag('QTY', clip(aXmlData.STF.QTY))
                objXmlExport.WriteTag('INV_DATE', clip(aXmlData.STF.INV_DATE))
                objXmlExport.WriteTag('INV_DUE_DATE', clip(aXmlData.STF.INV_DUE_DATE))
                objXmlExport.WriteTag('DESPATCH_DATE', clip(aXmlData.STF.DESPATCH_DATE))
                objXmlExport.WriteTag('DATE_REQUIRED', clip(aXmlData.STF.DATE_REQUIRED))
                objXmlExport.WriteTag('SERVICE_CODE', clip(aXmlData.STF.SERVICE_CODE))
                objXmlExport.WriteTag('RESEND_YN', 'N')
                objXmlExport.CloseTag('STF_UPDATE_REQUEST')

            of 'POR'
                objXmlExport.OpenTag('POR_UPDATE_REQUEST', 'version="1.0"')
                objXmlExport.WriteTag('CURRENCY_CODE', clip(aXmlData.POR.CURRENCY_CODE))
                objXmlExport.WriteTag('CURRENCY_UNIT_PRICE', clip(aXmlData.POR.CURRENCY_UNIT_PRICE))
                objXmlExport.WriteTag('DELIV_TO_REQ', clip(aXmlData.POR.DELIV_TO_REQ))
                objXmlExport.WriteTag('DEST_ORG_ID', clip(aXmlData.POR.DEST_ORG_ID))
                objXmlExport.WriteTag('GL_DATE', clip(aXmlData.POR.GL_DATE))
                objXmlExport.WriteTag('INTERFACE_SRC', clip(aXmlData.POR.INTERFACE_SRC))
                objXmlExport.WriteTag('ITEM_ID', clip(aXmlData.POR.ITEM_ID))
                objXmlExport.WriteTag('ORG_ID', clip(aXmlData.POR.ORG_ID))
                objXmlExport.WriteTag('PREPARER_ID', clip(aXmlData.POR.PREPARER_ID))
                objXmlExport.WriteTag('QTY', clip(aXmlData.POR.QTY))
                objXmlExport.WriteTag('SUPPLIER_NUMBER', clip(aXmlData.POR.SUPPLIER_NUMBER))
                objXmlExport.WriteTag('SB_PO_NUMBER', clip(aXmlData.POR.SB_PO_NUMBER))
                objXmlExport.WriteTag('TYPE', clip(aXmlData.POR.TYPE))
                objXmlExport.WriteTag('RESEND_YN', 'N')
                objXmlExport.CloseTag('POR_UPDATE_REQUEST')

            of 'REC'
                objXmlExport.OpenTag('REC_UPDATE_REQUEST', 'version="1.0"')
                objXmlExport.WriteTag('SB_PO_NO', clip(aXmlData.REC.SB_PO_NO))
                objXmlExport.WriteTag('ORACLE_PO_NO', clip(aXmlData.REC.ORACLE_PO_NO))
                objXmlExport.WriteTag('URL', clip(aXmlData.REC.URL))
                objXmlExport.WriteTag('TYPE', clip(aXmlData.REC.TYPE))
                objXmlExport.WriteTag('RESEND_YN', 'N')
                objXmlExport.CloseTag('REC_UPDATE_REQUEST')
            end

            objXmlExport.CloseTag('MESSAGE_BODY')
            objXmlExport.CloseTag('VODACOM_MESSAGE')
            objXmlExport.FClose();
        end
        
CreateTestRecord    PROCEDURE (String f:Path)       ! Declare Procedure

    CODE
    ! Start - Create a small test file in the repository folder - TrkBs: 5110 (DBH: 30-08-2005)
        tmp:TestRecord = Clip(f:Path)

        If Sub(Clip(tmp:TestRecord),-1,1) = '\'
            tmp:TestRecord = Clip(tmp:TestRecord) & Random(1,9999) & Clock() & '.tmp'
        Else ! If Sub(Clip(tmp:TestRecord),-1,1) = '\'
            tmp:TestRecord = Clip(tmp:TestRecord) & '\' & Random(1,9999) & Clock() & '.tmp'
        End ! If Sub(Clip(tmp:TestRecord),-1,1) = '\'

        Remove(TestRecord)
        Create(TestRecord)
        If Error()
            Return False
        End ! If Error()
        Open(TestRecord)
        If Error()
            Return False
        End ! If Error()
        test:TestLine = '1'
        Add(TestRecord)
        If Error()
            Return False
        End ! If Error()
        Close(TestRecord)
        Remove(TestRecord)
        If Error()
            Return False
        End ! If Error()
    ! End   - Create a small test file in the repository folder - TrkBs: 5110 (DBH: 30-08-2005
        Return True
 !endregion



