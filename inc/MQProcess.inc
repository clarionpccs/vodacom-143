    !-----------------------------
    ! Prototype:
    ! (String f:IMEINumber,*String f:POP,*Date f:DOP,*Byte f:Error,*String f:ErrorMessage,String f:OneYearWarranty),Byte
    ! Used in sba01app and Webserver

    Section('Local Data')
mqFileName          string(260), static

    ! Input / Output files - pipe delimited (|)
MQIInputFile        file, driver('BASIC','/COMMA=124 /ALWAYSQUOTE=OFF'), pre(MQIF), name(mqFileName), create
MQIFRec                 record
imei                        string(30)
                        end ! record
                    end ! file

MQIOutputFile       file, driver('BASIC','/COMMA=124 /ALWAYSQUOTE=OFF'), pre(MQOF), name(mqFileName)
MQOFRec                 record
activationDate              string(255)
activity                    string(255)
warranty                    string(255)
blackGrey                   string(255)
supplier                    string(255)
swap                        string(255)
usage                       string(255)
statusCode                  string(255)
statusDescription           string(255)
                        end ! record
                    end ! file

local:SBMQIFolder   Cstring(255)
inFileName          string(260)
outFileName         string(260)
local:ActivationDate        Date()
local:ActivationDateString  String(30)
local:OldDate       Date()
local:CurrentDate   Date()
local:YearDifference        Long()

    !-----------------------------
    Section('Processed Code')
    ! Clear variables
    job:DOP = f:DOP
    job:POP = f:POP
    f:Error = 0
    f:ErrorMessage = ''

    if (GETINI('MQ','IMEIValidation',,Clip(Path()) & '\SB2KDEF.INI') = 1)
        local:SBMQIFolder = GETINI('MQ','SBMQIFolder',Path(),Clip(Path()) & '\SB2KDEF.INI')

        if (local:SBMQIFolder = '')
            local:SBMQIFolder = Path()
        end ! if (local:SBMQIFolder = '')

        if (Sub(local:SBMQIFolder,-1,1) <> '\')
            local:SBMQIFolder = local:SBMQIFolder & '\'
        end ! if (Sub(local:SBMQIFolder,-1,1) <> '\')

        inFileName  = Clip(local:SBMQIFolder) & 'MQI' & clock() & '.CSV'   ! Unique Input file name
        outFileName = Clip(local:SBMQIFolder) & 'MQO' & clock() & '.CSV'  ! Unique Output file name

        mqFileName = Clip(inFileName)
        REMOVE(MQIInputFile)
        IF (EXISTS(inFileName))
            f:Error = 1
            f:ErrorMEssage = 'Error (4) occured during the automatic validation process.'
            RETURN TRUE
        END

        Create(MQIInputFile)

        IF (NOT EXISTS(inFileName))
            f:Error = 1
            f:ErrorMEssage = 'Error (5) occured during the automatic validation process.'
            RETURN TRUE
        END

        Open(MQIInputFile)
        mqif:IMEI = f:IMEINumber
        Add(MQIInputFile)
        Close(MQIInputFile)

        if (~exists(local:SBMQIFolder & 'SBMQI.EXE'))
            f:Error = 1
            ! EXE Doesn't Exists
            f:ErrorMessage = 'Error (1) occured during the automatic validation process.'
            return true
        end ! if (~exists(local:SBMQIFolder & 'SBMQI.EXE')

        Return# = RunDOS(local:SBMQIFolder & 'SBMQI.EXE ' & Clip(inFileName) & ' ' & Clip(outFileName),1)

        if (~Exists(outFileName))
            f:ErrorMessage = 'Error (2) occured during the automatic validation process.'
            f:Error = 1
            Return True
        end ! if (~Exists(outFileName))
        if  (Return# = False)

            f:ErrorMessage = 'Error (3) occured during the automatic validation process.'
            f:Error = 1
            Return True
        end ! if (~Exists(outFileName) Or Return# = False)

        mqFileName = Clip(outFileName)
        Open(MQIOutputFile)
        Set(MQIOutputFile,0)
        Next(MQIOutputFile)

        if (~Error())
            if  (Instring('THIS IMEI IS NOT IN THE 2ND YEAR VODACOM WARRANTY DATABASE',Clip(Upper(mqof:Supplier)),1,1) > 0) OR (Instring('DOES NOT QUALIFY FOR 2ND YEAR VODACOM WARRANTY',Clip(Upper(mqof:Supplier)),1,1) > 0) OR (Instring('DOES NOT QUALIFY FOR 2ND YEAR VODACOM WARRANTY',Clip(Upper(mqof:Swap)),1,1) > 0)
                f:Error = 1
                f:ErrorMessage = 'This IMEI is not on the Vodacom Service Provider Company Two Year Warranty Database.'
                f:POP = 'C'
            end ! if  (Instring('THIS IMEI IS NOT IN THE 2ND YEAR VODACOM WARRANTY DATABASE',Clip(Upper(mqof:Supplier)),1,1) > 0)

            if (Instring('CURRENTLY BLACKLISTED/GREYLISTED',Clip(Upper(mqof:BlackGrey)),1,1) > 0)
                if (f:ErrorMessage = '')
                    f:ErrorMessage = 'This IMEI has been Black Listed or Grey Listed.'
                else ! if (f:Error = 0)\
                    f:ErrorMessage = clip(f:ErrorMessage) & ' This IMEI has been Black Listed or Grey Listed.'
                end ! if (f:Error = 0)
                f:Error = 1
                f:POP = 'C'
            end ! if (Instring('CURRENTLY BLACKLISTED/GREYLISTED',Clip(Upper(mqof:BlackGrey)),1,1) > 0)

            local:ActivationDateString = Sub(Clip(mqof:Activity),-11,11)

            if (local:ActivationDateString <> '')
                local:ActivationDate = Deformat(local:ActivationDateString,@d08)
            end ! if (local:ActivationDateString <> '')

            if (local:ActivationDate <> '')
                f:DOP = local:ActivationDate

                local:OldDate    = local:ActivationDate
                local:CurrentDate = Today()
                loop while (local:CurrentDate > local:OldDate)
                    local:OldDate = Date(Month(local:OldDate), Day(local:OldDate), Year(local:OldDate) + 1)
                    local:YearDifference += 1
                end ! loop while (local:CurrentDate > local:OldDate)

                if (local:YearDifference > 0)
                    local:YearDifference -= 1
                end ! if (local:YearDifference > 0)

                if (local:YearDifference = 0 And f:Error = 0)
                    f:ErrorMessage = 'IMEI Validation Result:    This IMEI can be reparied under the First Year Manufacturer Warranty as per manufacturer criteria.'
                    f:POP = 'F'
                end ! if (local:YearDifference = 0 And f:Error = 0)

                if (local:YearDifference > 0 And local:YearDifference <= 1 And f:Error = 0)
                    if (Instring('WAS ONLY',Clip(Upper(mqof:Usage)),1,1) <> 0)
                        if (f:Error = 0)
                            f:ErrorMessage = 'This IMEI has not been active for 30 days in the last 90 days.'
                        else ! if (f:Error = 0)
                            f:ErrorMessage = clip(f:ErrorMessage) & 'This IMEI has not been active for 30 days in the last 90 days.'
                        end ! if (f:Error = 0)
                        f:Error = 1
                        f:POP = 'C'
                    else !if (Instring('WAS ONLY',Clip(Upper(mqof:Usage)),1,1) <> 0)
                        f:ErrorMessage = 'The IMEI can be repaired under the Second Year Warranty.'
                        f:POP = 'S'
                        IF (f:OneYearWarranty = 'Y')
                            ! #11344 If one year only, don't allow 2nd year (DBH: 07/06/2010)
                            f:Error = 1
                            f:ErrorMessage = 'This Phone must be repaired out of Warranty'
                            f:POP = 'C'
                        end
                    end ! if (Instring('WAS ONLY',Clip(Upper(mqof:Usage)),1,1) <> 0)
                end ! if (local:YearDifference > 0 And local:YearDifference <= 1 And f:Error = 0)

                if (f:Error = 0 And local:YearDifference > 1)
                    f:Error = 1
                    f:ErrorMessage = 'This Phone must be repaired out of Warranty'
                    f:POP = 'C'
                end ! if (f:Error = 0 And local:YearDifference > 1)
                ! #11344 Wrong code (DBH: 07/06/2010)
                !                !new code added by Paul 28/04/2010 - Log no 11344
                !                If f:OneYearWarranty = 'Y' then
                !                    !only a 1 year warranty - so check the year difference
                !                    If local:YearDifference > 1 then
                !                        !cannot be repaired under warranty
                !                        f:Error = 1
                !                        f:ErrorMessage = 'This Phone must be repaired out of Warranty'
                !                        f:POP = 'C'
                !                    End
                !                End
                !                !end new code
            else ! ! if (local:ActivationDate > 0)
                if (Instring('NEVER BEEN ACTIVE',Clip(Upper(mqof:Activity)),1,1) <> 0)
                    if (f:Error = 0)
                        f:ErrorMessage ='This IMEI has never been active on the Vodacom Network'
                    else ! if (f:Error = 0)
                        f:ErrorMessage = clip(f:ErrorMessage) & 'This IMEI has never been active on the Vodacom Network'
                    end ! if (f:Error = 0)
                    f:POP = 'C'
                    f:Error = 1
                ELSE
                    ! #13424 Result contains unexpected text and no activation date. (DBH: 20/11/2014)
                    f:Error = 1
                    f:ErrorMessage = 'This IMEI was NOT supplied by Vodacom and does not appear in the 2nd year Vodacom warranty database.'
                    Close(MQIOutputFile)
                    Remove(mqFileName)
                    RETURN TRUE
                    ! #13424 This is a bit of a bodge putting a return here.
                    ! Vodacom haven't specified that a charge type should be forced (they should have), 
                    ! so have to return with an error. I have to close the file to do this.
                    ! I don't want to re-write this code until I have to. (DBH: 20/11/2014)
                    
!                    f:POP = 'C'
                end ! if (Instring('NEVER BEEN ACTIVE',Clip(Upper(mqof:Activity)),1,1) <> 0)
            end ! if (local:ActivationDate > 0)
        end ! if (~Error())
        Close(MQIOutputFile)
        Remove(mqFileName)


    end ! if (GETINI('MQ','IMEIValidation',,Clip(Path()) & '\SB2KDEF.INI') <> 1)

    Return False