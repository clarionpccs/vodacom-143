    OMIT('_EndOfInclude_',_BHSimpleExportFile_)
_BHSimpleExportFile_   EQUATE(1)

SimpleExportFile    FILE,DRIVER('ASCII'),CREATE,BINDABLE
Record                  RECORD
ExportLine                  STRING(80000)
                        END
                    END

! ===========================================================================================
SimpleExportClass   CLASS,TYPE,MODULE('BHSimpleExportFile.clw'),LINK('BHSimpleExportFile.clw',1),DLL(0)
! ===========================================================================================
Delimeter               STRING(1),PRIVATE
LastError               STRING(255)
UseQuotes               BYTE(0),PRIVATE
FileOpen                BYTE(0),PRIVATE
Init                    PROCEDURE(STRING pUseFilename,<STRING pRecordDelimiter>,<BYTE pQuotesOnFields>),BYTE ! Create/Open Export File. Specify delimeter and quotes. Returns Error
AddField                PROCEDURE(<STRING pFieldText>,<BYTE pFirstFieldInRow>,<BYTE pLastFieldInRow>,<BYTE pMultipleRows>,<BYTE pIgnoreQuotes>) ! Add line to export file. Set First Field. Set Last Field (and ADD). Multiple Rows. Ignore Quotes to add a string of columns.
Kill                    PROCEDURE() ! Close Export File
                    END
    _EndOfInclude_
	
	