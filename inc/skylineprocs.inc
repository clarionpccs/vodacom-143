!region SkylineINIT
Skyline.Init        PROCEDURE(NetWebServerWorker p_web,STRING pHeaderAuthString,*QUEUE Q, LONG pNullEmptyFields=0)!,LONG
defaultUsername         STRING(255)
defaultPassword         STRING(255)
encryptedPassword       STRING(255)
authString              STRING(1000)
authLen                 LONG
passedAuthString        STRING(1000)
defaultAuthString       STRING(1000)
wc                      CLASS(NetWebClient)
                        END ! CLASS
    
                    MAP
GetValueFromWebStringVariable   PROCEDURE(STRING variableName),STRING		
PopulateQueue           PROCEDURE()
ReverseString           PROCEDURE(STRING pInputString),STRING
                    END!  MAP    
retValue                LONG()    
    CODE
        ! Return 1 = Error
		! Return 2 = Login Error
        p_web.SSV('SkylineResponseString','')
        
        IF (p_web.RequestData.DataString = '')
            RETURN 1
        END ! IF
        
        p_web.SSV('SkylineDataString',p_web.RequestData.DataString)
        
        LOOP 1 TIMES
            
            FREE(Q)
		
            PopulateQueue()
        
            IF (RECORDS(Q) = 0)
                retValue = 1
                BREAK
            END ! IF
            
            defaultUsername = GETINI('SKYLINE','Username',,PATH() & '\DEFAULTS.INI')
            defaultPassword = GETINI('SKYLINE','Password',,PATH() & '\DEFAULTS.INI')
            SELF.Scramble(defaultPassword)
			
            encryptedPassword = CLIP(defaultPassword) & CLIP(SkylineKey) & ReverseString(defaultPassword)
			
            rtn# = NetMD5(encryptedPassword,LEN(CLIP(encryptedPassword)),encryptedPassword)
		
            authString = CLIP(defaultUsername) & ':' & CLIP(encryptedPassword)
            authLen = LEN(CLIP(authString))
            
			! Define Default AuthString
            defaultAuthString = 'Basic ' & NetBase64Encode(CLIP(authString),authLen)
			
			! Save Auth String, passed for GetHeader
            passedAuthString = pHeaderAuthString
			
			! I don't know if GetHeaderField always passes a space at the start, so I'll remove it for now
            IF (SUB(passedAuthString,1,1) = ' ')
                passedAuthString = SUB(passedAuthString,2,LEN(CLIP(passedAuthString)))
            END
			
			! Compare Auth Strings
            IF (CLIP(passedAuthString) <> CLIP(defaultAuthString))
                retValue = 2
                BREAK
            END
            

        END ! IF
		
        RETURN retValue
		
PopulateQueue       PROCEDURE()
i                       LONG,AUTO
totalFields             LONG,AUTO
fieldName               STRING(60),DIM(200)
variableValue           ANY,DIM(200)
recordCount             LONG,AUTO
firstFieldName          STRING(60)
    CODE
        i = 1
        totalFields = 0
        LOOP
            IF (i > 200)
                HALT(,'Error. Cannot parse a string with over 200 fields.')
                BREAK
            END

            fieldName[i] = WHO(Q,i)            
            IF (fieldName[i] = '')
            ! Blank, so there can't be anymore fields
                totalFields = i - 1
                BREAK
            END

        ! Incase the Q has a prefix, remove it.
            col# = INSTRING(':',fieldName[i],1,1)
            IF (col# > 0)
                fieldName[i] = CLIP(SUB(fieldName[i],col# + 1,100))
            END

            i += 1
        END
    
    ! Assign variables to Q variables
        LOOP i = 1 TO totalFields
            variableValue[i] &= WHAT(Q,i)            
        END
    
        FREE(Q)
        CLEAR(Q)

        recordCount = 0
        IF (INSTRING('0%5B',p_web.GSV('SkylineDataString'),1,1) AND INSTRING('%5D=',p_web.GSV('SkylineDataString'),1,1))
        ! Multiple Data String because 0[ and ]= exist in the string
            LOOP
                firstFieldName = recordCount & '%5B' & CLIP(fieldName[1]) & '%5D='

                IF (INSTRING(CLIP(UPPER(firstFieldName)),UPPER(p_web.GSV('SkylineDataString')),1,1) = 0)
                ! The field doesn't exist so much be the end of the data
                    BREAK
                END
            
                LOOP i = 1 To totalFields
                
                    variableValue[i] = GetValueFromWebStringVariable(recordCount & '%5B' & CLIP(fieldName[i]) & '%5D')
                
                    BHAddToDebugLog('#' & recordCount & ' : ' & i & '  -  ' & recordCount & '%5B' & CLIP(fieldName[i]) & '%5D' & ' = ' & CLIP(variableValue[i]))
                END ! LOOP
            
                ADD(Q)
            
                recordCount += 1
            
            END
        
        ELSE
            LOOP i = 1 To totalFields
                variableValue[i] = GetValueFromWebStringVariable(CLIP(fieldName[i]))                
            END ! LOOP
            ADD(Q)
        END	
	
GetValueFromWebStringVariable       PROCEDURE(STRING variableName)!,STRING
wString                                 STRING(200000)
varValue                                STRING(1024)
varPos                                  LONG
ampPos                                  LONG
    CODE
        wString = p_web.GSV('SkylineDataString')
        varValue = ''

        varPos = INSTRING(CLIP(LOWER(variableName)) & '=',LOWER(wString),1,1)
    
        IF (varPos > 0)
        ! Get the next variable
            ampPos = INSTRING('&',wString,1,varPos)
        
            IF (ampPos = 0)
            ! No next variable. Get the end of line
                ampPos = INSTRING('<13>',wString,1,varPos)
            END ! IF
            IF (ampPos = 0)
            ! No next variable. Get the end of line
                ampPos = INSTRING('<10>',wString,1,varPos)
            END ! IF
            IF (ampPos = 0)
            ! No end of line. Get the end of the string
                ampPos = LEN(CLIP(wString)) + 1
            END ! IF
        
            varPos = varPos + LEN(CLIP(variableName)) + 1
        
            varValue = SUB(wString,varPos,ampPos - varPos)
            varValue = wc.DecodeWebString(varValue)
        ELSE
            IF (pNullEmptyFields = 1)
                varValue = 'NULL'
            END ! IF
        END !IF
    
        RETURN varValue	
        
ReverseString       PROCEDURE(STRING pInputString)!,STRING        
lenString               LONG,AUTO
stringIn                STRING(255),AUTO
stringOut               STRING(255),AUTO
i                       LONG,AUTO
j                       LONG,AUTO
    CODE
        IF (pInputString = '')
            RETURN ''
        END

        stringIn = pInputString
        stringOut = ''
        j = 1

        lenString = LEN(CLIP(stringIn))

        LOOP i = lenString to 1 BY -1
            stringOut[j] = stringIn[i]
            j += 1
        END

        RETURN stringOut
!endregion
!region Scramble
Skyline.Scramble    PROCEDURE(*STRING pString)
stringIn                STRING(255)
stringOut               STRING(255)
defaultNo               EQUATE(9)
i                       LONG()
    CODE
        stringIn = pString
        IF (StringIn = '')
            RETURN
        END
        
        LOOP i = 1 TO LEN(CLIP(StringIn))
            stringOut[i] = CHR(BXOR(VAL(stringIn[i]),(i + defaultNo)))
        END ! IF
        
        pString = stringOut
        
!endregion
!region CreateResponse
Skyline.CreateResponse      PROCEDURE(NetwebServerWorker p_web,STRING pCode,STRING pDescription,<STRING pDetails>)        
responseString                  EQUATE(SkylinePostString)
    CODE
        
        responseString = '<Response>'
        
        IF (pDetails <> '')
            responseString = CLIP(responseString) & CLIP(pDetails)
        END ! IF
        
        responseString = CLIP(responseString) & |
            '<ResponseCode>' & CLIP(pCode) & '</ResponseCode>' & |
            '<ResponseDescription>' & CLIP(pDescription) & '</ResponseDescription>' & |
            '</Response>'
        
        p_web.SSV('SkylineResponseString',responseString)
        
!endregion
!region PutNewJob
Skyline.PutNewJob   PROCEDURE(NetwebServerWorker p_web,qSkylinePutJobDetails Q,*STRING pErrorStr)!,LONG
NewJobNumber            LONG()

qAcc                    QUEUE(SkylinePJDAccessories),PRE(qAcc)
                        END
qDamage                 QUEUE(SkylinePJDDamageCheckListItems),PRE(qDamage)
                        END
i                       LONG()  
locAuditNotes           STRING(255)
json                    JSONParser  
    CODE
        GET(Q,1)
        
        Q = UPPER(Q)
        
       
        ! Read Arrays
        json.AddStringToQueue(qAcc,Q.Accessories)
        json.AddStringToQueue(qDamage,Q.DamageCheckListItems)
        
        qAcc = UPPER(qAcc)
        qDamage = UPPER(qDamage)
        
        !XML:DebugMyQueue('Accessories',qAcc)
        
        DO OpenFiles
        
        LOOP 1 TIMES
            Access:JOBSSL.ClearKey(jsl:RefNumberKey)
            jsl:SLNumber = Q.SLNumber
            IF (Access:JOBSSL.TryFetch(jsl:RefNumberKey) = Level:Benign)
                NewJobNumber = 0
                pErrorStr = 'SL Number Already Exists On SB Job No ' & jsl:RefNumber
                BREAK
            END ! IF
            
            ! Save user code for various routines
            p_web.SSV('BookingUserCode',Q.Username)
            
            ! RRC Booking
            IF (Access:JOBS.PrimeRecord() = Level:Benign)
                !Username
                job:Who_Booked = Q.Username
                !TransitType
                job:Transit_Type = Q.JobType
                ! IMEI Number
                job:ESN = Q.IMEINo
                ! MSN
                job:MSN = Q.SerialNo
                ! Manufacturer
                job:Manufacturer = Q.Manufacturer
                job:Model_Number = Q.ModelNo
                job:Unit_Type = Q.UnitType
                ! Mobile Number
                job:Mobile_Number = Q.CustomerMobileNo
                ! Product Code
                job:ProductCode = Q.ProductCode
                ! Colour
                job:Colour = Q.Colour
                ! DOP
                job:DOP = DEFORMAT(Q.DOP,@d06)
                ! POP
                job:POP = Q.ServiceType
                ! Authority Number
                job:Authority_Number = Q.AuthorisationNumber
                ! Trade Account
                job:Account_Number = Q.ClientAccountNo
                ! Order Number
                job:Order_Number = Q.AgentRefNo
                ! Courier
                job:Courier = Q.Courier
                ! Title
                job:Title = Q.CustomerTitle
                ! Initial
                job:Initial = Q.CustomerForename
                ! Surname
                job:Surname = Q.CustomerSurname
                ! End User Tel No
                job:Telephone_Number = Q.CustomerHomeTelNo
                ! Company Name
                job:Company_Name = Q.CustomerCompanyName
                ! Address
                job:Address_Line1 = Q.CustomerBuildingName
                job:Address_Line2 = Q.CustomerStreet
                job:Address_Line3 = Q.CustomerAddressArea
                job:Postcode = Q.CustomerPostcode
                ! Collection Address
                job:Company_Name_Collection = Q.ColAddCompanyName
                job:Address_Line1_Collection = Q.ColAddBuildingName
                job:Address_Line2_Collection = Q.ColAddStreet
                job:Address_Line3_Collection = Q.ColAddArea
                job:Postcode_Collection = Q.ColAddPostcode
                job:Telephone_Collection = Q.ColAddPhoneNo
                ! Delivery Address
                job:Company_Name_Delivery = Q.DelAddCompanyName
                job:Address_Line1_Delivery = Q.DelAddBuildingName
                job:Address_Line2_Delivery = Q.DelAddStreet
                job:Address_Line3_Delivery = Q.DelAddArea
                job:Postcode_Delivery = Q.DelAddPostcode
                job:Telephone_Delivery = Q.DelAddPhoneNo
                
                ! Service Type
                IF (Q.ServiceType = 'C')
                    job:Charge_Type = Q.ServiceTypeName
                    job:Chargeable_Job = 'YES'
                    ! Estimate
                    IF (Q.EstimateRequired = 'Y')
                        job:Estimate = 'YES'
                        
                    END ! IF
                    
                END ! IF
                IF (Q.ServiceType = 'W')
                    job:Warranty_Charge_Type = Q.ServiceTypeName
                    job:Warranty_Job = 'YES'
                END ! IF
                
                !Engineer
                job:Engineer = Q.EngineerCode

                IF (Access:JOBS.TryInsert())
                    Access:JOBS.CancelAutoInc()
                    BREAK
                END ! IF
                
                !Save the job number to return 
                NewJobNumber = job:Ref_Number
                
                Access:WEBJOBNO.ClearKey(wej:HeadAccountNumberKey)
                wej:HeadAccountNumber = Q.BookingAccountNumber
                If Access:WEBJOBNO.TryFetch(wej:HeadAccountNumberKey) = Level:Benign
              !Found
                    wej:LastWEBJOBNumber += 1
                    Access:WEBJOBNO.Update()
                    JobNumber# = wej:LastWEBJOBNumber
                Else ! If Access:WEBJOBNO.TryFetch(wej:HeadAccountNumberKy) = Level:Benign
              !Error
                    If Access:WEBJOBNO.PrimeRecord() = Level:Benign
                        wej:HeadAccountNumber = Q.BookingAccountNumber
                        wej:LastWEBJOBNumber = 1
                        JobNumber# = 1
                        If Access:WEBJOBNO.TryInsert() = Level:Benign
                      !Insert
                        Else ! If Access:WEBJOBNO.TryInsert() = Level:Benign
                            Access:WEBJOBNO.CancelAutoInc()
                        End ! If Access:WEBJOBNO.TryInsert() = Level:Benign
                    End ! If Access.WEBJOBNO.PrimeRecord() = Level:Benign
                End ! If Access:WEBJOBNO.TryFetch(wej:HeadAccountNumberKy) = Level:Benign
                
                
                IF (Access:WEBJOB.PrimeRecord() = Level:Benign)
                    wob:RefNumber = job:Ref_Number
                    wob:JobNumber = JobNumber#
                    wob:HeadAccountNumber = Q.BookingAccountNumber
                    wob:SubAcountNumber = job:Account_Number
                    wob:OrderNumber = job:Order_Number
                    wob:IMEINumber = job:ESN
                    wob:MSN = job:MSN
                    wob:Manufacturer = job:Manufacturer
                    wob:ModelNumber = job:Model_Number
                    wob:MobileNumber = job:Mobile_Number
                    wob:EDI = job:EDI
                    wob:Current_Status = job:Current_Status
                    wob:Exchange_Status = job:Exchange_Status
                    wob:Current_Status_Date = TODAY()
                    wob:Loan_Status = job:Loan_Status
                    wob:DateBooked = job:Date_Booked
                    wob:TimeBooked = job:Time_Booked
              
                    IF (Access:WEBJOB.TryInsert())
                        Access:WEBJOB.CancelAutoInc()
                    END ! IF
                END ! IF
                
                IF (Access:JOBSE.PrimeRecord() = Level:Benign)
                    jobe:WebJob = 1
                    ! Network (Maybe not the same field)
                    jobe:Network = Q.Network
                    !POP Type
                    ! Not sure how relevant this is?!
                    !jobe:POPType = Q.JobType
                    ! Vat Number
                    jobe:VatNumber = Q.VATNumber
                    ! Booking Option 
                    jobe:Booking48HourOption = 1 ! Not sure how to set this from Skyline
                    ! OBF
                    IF (Q.OBFIMEINumber <> '')
                        ! Going to assume that if it's OBF, it's validated
                        jobe:OBFValidated = 1
                        jobe:OBFValidateDate = job:Date_Booked
                        jobe:OBFValidateTime = job:Time_Booked
                        jobe:BoxESN = Q.OBFIMEINumber
                        jobe:ReturnDate = DEFORMAT(Q.OBFReturnDate,@d06)
                        jobe:TalkTime = Q.OBFTalkTime
                        jobe:BranchOFReturn = Q.OBFBranchOfReturn                     
                        jobe:OriginalDealer = Q.OBFOriginalDealer
                        jobe:OriginalBattery = CHOOSE(Q.OBFOriginalAccessories = 'Y',1,0)
                        jobe:OriginalPackaging = CHOOSE(Q.OBFOriginalPackaging = 'Y',1,0)
                        jobe:OriginalCharger = CHOOSE(Q.OBFOriginalCharger = 'Y',1,0)
                        jobe:OriginalAntenna = CHOOSE(Q.OBFOriginalAntenna = 'Y',1,0)
                        jobe:OriginalManuals = CHOOSE(Q.OBFOriginalManuals = 'Y',1,0)
                        jobe:PhysicalDamage = CHOOSE(Q.OBFPhysicalDamage = 'Y',1,0)
                        
                        IF (Access:JOBSOBF.PrimeRecord() = Level:Benign)
                            jof:RefNumber = job:Ref_Number
                            jof:IMEINumber = job:ESN
                            jof:Status = 1
                            jof:Replacement = CHOOSE(Q.OBFReplacement = 'Y',1,0)
                            jof:UserCode = job:Who_Booked
                            jof:HeadAccountNumber = wob:HeadAccountNumber
                            jof:StoreReferenceNumber = Q.OBFStoreReferenceNumber
                            jof:ReplacementIMEI = Q.OBFReplacementIMEINo
                            jof:LAccountNumber = Q.OBFLAccountNo
                            
                            IF (Access:JOBSOBF.TryInsert())
                                Access:JOBSOBF.CancelAutoInc()
                            END ! IF
                        END ! IF
                        If Access:JOBACC.PrimeRecord() = Level:Benign
                            jac:Ref_Number = job:Ref_Number
                            jac:Accessory = 'MANUALS'
                            jac:Damaged = 0
                            jac:Attached = False
                            If Access:JOBACC.TryInsert() = Level:Benign
                          !Insert
                            Else ! If Access:JOBACC.TryInsert() = Level:Benign
                                Access:JOBACC.CancelAutoInc()
                            End ! If Access:JOBACC.TryInsert() = Level:Benign
                        End ! If Access.JOBACC.PrimeRecord() = Level:Benign
  
                        If Access:JOBACC.PrimeRecord() = Level:Benign
                            jac:Ref_Number = job:Ref_Number
                            jac:Accessory = 'ORIGINAL PACKAGING'
                            jac:Damaged = 0
                            jac:Attached = False
                            If Access:JOBACC.TryInsert() = Level:Benign
                          !Insert
                            Else ! If Access:JOBACC.TryInsert() = Level:Benign
                                Access:JOBACC.CancelAutoInc()
                            End ! If Access:JOBACC.TryInsert() = Level:Benign
                        End ! If Access.JOBACC.PrimeRecord() = Level:Benign
                        
                        p_web.FileToSessionQueue(JOBS)
                        
                        AddToAudit(p_web,job:Ref_Number,'JOB',|
                            'O.B.F. VALIDATION ACCEPTED',|
                            'ORIGINAL DEALER <13,10>'& jobe:OriginalDealer & '<13,10>BRANCH OF RETURN<13,10>'&  jobe:BranchOfReturn)
                            
                    END ! IF
                    IF (Access:JOBSE.TryInsert())
                        Access:JOBSE.CancelAutoInc()
                    END ! IF
                END ! IF

                IF (Access:JOBSE2.PrimeRecord() = Level:Benign)
                    jobe2:RefNumber = job:Ref_Number
                    ! Warranty Ref now
                    jobe2:WarrantyRefNo = Q.NetWorkReferenceNo
                    ! Courier Waybill Number
                    jobe2:CourierWaybillNumber = Q.ConsignmentNo
                    ! ID Number
                    jobe2:IDNumber = Q.CustomerIDNumber
                    ! SMS Alert
                    jobe2:SMSNotification = CHOOSE(Q.SMSNotificationRequired = 'Y',1,0)
                    ! SMS Alert No
                    jobe2:SMSAlertNumber = Q.SMSAlertNo
                    ! EMail Alert
                    jobe2:EmailNotification = CHOOSE(Q.EmailNotificationRequired = 'Y',1,0)
                    ! Email Alert Address
                    jobe2:EmailAlertAddress = Q.EmailAlertAddress
                    ! Hubs
                    jobe2:HubCustomer = Q.CustomerCounty
                    jobe2:HubCollection = Q.ColAddCounty
                    jobe2:HubDelivery = Q.DelAddCounty
                    ! External Damage
                    IF (RECORDS(qDamage) = 0)
                        jobe2:XNone = 1
                    ELSE
                        LOOP i = 1 TO RECORDS(qDamage)
                            GET(qDamage,i)
                            IF (INSTRING('ANTENNA',qDamage.DamageCheckListItem,1,1))
                                jobe2:XAntenna = 1
                            END ! IF
                            IF (INSTRING('LENS',qDamage.DamageCheckListItem,1,1))
                                jobe2:XLens = 1
                            END ! IF
                            IF (INSTRING('FRONT COVER',qDamage.DamageCheckListItem,1,1))
                                jobe2:XFCover = 1
                            END ! IF
                            IF (INSTRING('BACK COVER',qDamage.DamageCheckListItem,1,1))
                                jobe2:XBCover = 1
                            END ! IF
                            IF (INSTRING('KEYPAD',qDamage.DamageCheckListItem,1,1))
                                jobe2:XKeyPad = 1
                            END ! IF
                            IF (INSTRING('BATTERY',qDamage.DamageCheckListItem,1,1))
                                jobe2:XBattery = 1
                            END ! IF
                            IF (INSTRING('CHARGER',qDamage.DamageCheckListItem,1,1))
                                jobe2:XCharger = 1
                            END ! IF
                            IF (INSTRING('LCD',qDamage.DamageCheckListItem,1,1))
                                jobe2:XLCD = 1
                            END ! IF
                            IF (INSTRING('SIM READER',qDamage.DamageCheckListItem,1,1))
                                jobe2:XSimReader = 1
                            END ! IF
                            IF (INSTRING('SYSTEM CONNECTOR',qDamage.DamageCheckListItem,1,1))
                                jobe2:XSystemConnector = 1
                            END ! IF
                            
                        END ! LOOP
                        jobe2:XNotes = Q.UnitCondition
                    END ! IF
                    
                    IF (Access:JOBSE2.TryInsert())
                        Access:JOBSE2.CancelAutoInc()
                    END ! IF
                END ! IF
                             
                IF (Access:JOBNOTES.PrimeRecord() = Level:Benign)
                    jbn:RefNumber = job:Ref_Number
                    ! Fault Description
                    jbn:Fault_Description = Q.ReportedFault
                    ! Engineer Notes
                    jbn:Engineers_Notes = Q.Notes
                    IF (Access:JOBNOTES.TryInsert())
                        Access:JOBNOTES.CancelAutoInc()
                    END ! IF
                END ! IF
                
                Access:TRANTYPE.ClearKey(trt:Transit_Type_Key)
                trt:Transit_Type = job:Transit_Type
                IF (Access:TRANTYPE.TryFetch(trt:Transit_Type_Key) = Level:Benign)
                    
                ELSE ! IF
                END ! IF
                
                VodacomClass.SetJobStatus(trt:Initial_Status[1:3],'JOB',job:Who_Booked)
                VodacomClass.SetJobStatus(trt:ExchangeStatus[1:3],'EXC',job:Who_Booked)
                VodacomClass.SetJobStatus(trt:LoanStatus[1:3],'LOA',job:Who_Booked)
            
                IF (job:Estimate = 'YES' AND job:Chargeable_Job = 'YES')
                    VodacomClass.SetJobStatus(505,'JOB',job:Who_Booked)
                END ! IF
                
                IF (jobe:Booking48HourOption = 4)
                    ! If Liquid Damage, then send to Hub
                    jobe:HubRepair = 1
                END ! IF
                IF (jobe:HubRepair = 1)
                    jobe:HubRepairDate = TODAY()
                    jobe:HubRepairDate = CLOCK()
                
                    ! Send to ARC
                    VodacomClass.SetJobStatus(GETINI('RRC','StatusSendToARC',,CLIP(PATH())&'\SB2KDEF.INI'),'JOB',job:Who_Booked)
                        
                    Access:REPTYDEF.ClearKey(rtd:ManRepairTypeKey)
                    rtd:Manufacturer = job:Manufacturer
                    SET(rtd:ManRepairTypeKey,rtd:ManRepairTypeKey)
                    LOOP UNTIL Access:REPTYDEF.Next() <> Level:Benign
                        IF (rtd:Manufacturer <> job:Manufacturer)
                            BREAK
                        END ! IF
                        IF (rtd:BER = 11) ! Handling Fee
                            IF (job:Chargeable_Job = 'YES' AND job:Repair_Type = '')
                                job:Repair_Type = rtd:Repair_Type
                            END ! IF
                            IF (job:Warranty_Job = 'YES' AND job:Repair_Type_Warranty = '')
                                job:Repair_Type_Warranty = rtd:Repair_Type
                            END ! IF
                        END ! IF
                    END ! LOOP    
                END ! IF
                
                Access:JOBS.TryUpdate()
                Access:JOBSE.TryUpdate()
                
                ! This will depend on where the location is being set?
                IF (Access:LOCATLOG.PrimeRecord() = Level:Benign)
                    lot:RefNumber = job:Ref_Number
                    lot:TheDate = TODAY()
                    lot:TheTime = CLOCK()
                    lot:UserCode = job:Who_Booked
                    lot:PreviousLocation = ''
                    lot:NewLocation = job:Location
                    IF (Access:LOCATLOG.TryInsert())
                        Access:LOCATLOG.CancelAutoInc()
                    END ! IF
                END ! IF

                ! Check SMS Alerts
                IF (jobe2:SMSNotification)
                    AddEmailSMS(job:Ref_Number,wob:HeadAccountNumber,'BOOK','SMS',jobe2:SMSAlertNumber,'',0,'')
                END!  IF
                IF (jobe2:EmailNotification)
                    AddEmailSMS(job:Ref_Number,wob:HeadAccountNumber,'BOOK','EMAIL','',jobe2:EmailAlertAddress,0,'')
                END ! IF
                
                locAuditNotes = ''
                locAuditNotes = 'UNIT DETAILS: ' & Clip(job:manufacturer) & ' ' & Clip(job:model_number) & ' - ' & Clip(job:unit_type) & |
                    '<13,10>I.M.E.I.: ' & Clip(job:esn) & |
                    '<13,10>M.S.N.: ' & Clip(job:msn)
                
                If job:Chargeable_job = 'YES'
                    locAuditNotes = Clip(locAuditNotes) & '<13,10>CHA. CHARGE TYPE: ' & Clip(job:Charge_Type)
                End !If job:Chargeable_job = 'YES'
                If job:Warranty_Job = 'YES'
                    locAuditNotes = Clip(locAuditNotes) & '<13,10>WAR. CHARGE TYPE: ' & Clip(job:Warranty_Charge_Type)
                End !If job:Warranty_Job = 'YES'
  
                locAuditNotes = Clip(locAuditNotes) & '<13,10>BOOKING OPTION: '
                Case jobe:Booking48HourOption
                Of 1
                    locAuditNotes = Clip(locAuditNotes) & ' 48 HOUR EXCHANGE'
                Of 2
                    locAuditNotes = Clip(locAuditNotes) & ' ARC REPAIR'
                Of 3
                    locAuditNotes = Clip(locAuditNotes) & ' 7 DAY TAT'
                End !Case tmp:Booking48HourOption
  
                If jobe2:Contract = 1
                    locAuditNotes = Clip(locAuditNotes) & '<13,10>CONTRACT'
                Else ! If jobe2:Contract = 1
                    If jobe2:Prepaid = 1
                        locAuditNotes = Clip(locAuditNotes) & '<13,10>PREPAID'
                    End ! If jobe2:Prepaid = 1
                End ! If jobe2:Contract = 1                
                
                If Clip(jobe2:IDNumber) <> ''
                    locAuditNotes = Clip(locAuditNotes) & '<13,10>ID NO: ' & Clip(jobe2:IDNumber)
                End !If Clip(jobe2:IDNumber) <> ''
                
                LOOP i = 1 TO RECORDS(qAcc)
                    GET(qAcc,i)
                    IF (Access:JOBACC.PrimeRecord() = Level:Benign)
                        jac:Ref_Number = job:Ref_Number
                        jac:Accessory = qAcc.Accessory
                        jac:Damaged = 0
                        jac:Attached = 0
                        IF (Access:JOBACC.TryInsert())
                            Access:JOBACC.CancelAutoInc()
                        END ! IF
                        locAuditNotes = '<13,10>ACC. ' & Clip(i) & ': ' & Clip(jac:accessory)
                    END ! IF
                END ! LOOP
                
                AddToAudit(p_web,job:Ref_Number,'JOB','SKYLINE NEW JOB BOOKING INTIAL ENTRY',Clip(locAuditNotes))
                
            END ! IF
            
            ! Check if we need to send any SMSs
            ! Load session Q as they are used by SMS procedures
            p_web.FileToSessionQueue(JOBS)
            p_web.FileToSessionQueue(JOBSE)
            p_web.FileToSessionQueue(JOBSE2)
            
            VodacomClass.SendSMS('J','Y','N',p_web)
            VodacomClass.SendSMS('E','Y','N',p_web)
            VodacomClass.SendSMS('L','Y','N',p_web)
            
        END ! LOOP
        
        DO CloseFiles
        
        RETURN NewJobNumber
        
OpenFiles           ROUTINE
    Relate:JOBSSL.Open()
    Relate:JOBSE.Open()
    Relate:JOBS.Open()
    Relate:JOBSE2.Open()
    Relate:JOBNOTES.Open()
    Relate:TRANTYPE.Open()
    Relate:REPTYDEF.Open()
    Relate:LOCATLOG.Open()     
    Relate:JOBACC.Open()
    Relate:JOBSOBF.Open()
    Relate:WEBJOB.Open()
    Relate:WEBJOBNO.Open()
    Relate:JOBACC.Open()
    
        
CloseFiles          ROUTINE
    Relate:JOBSSL.Close()
    Relate:JOBS.Close()
    Relate:JOBSE.Close()
    Relate:JOBSE2.Close()
    Relate:REPTYDEF.Close()
    Relate:TRANTYPE.Close()
    Relate:JOBNOTES.Close()
    Relate:LOCATLOG.Close()
    Relate:JOBACC.Close()        
    Relate:JOBSOBF.Close()
    Relate:WEBJOBNO.Close()
    Relate:WEBJOB.Close()
    Relate:JOBACC.Close()
!endregion
    