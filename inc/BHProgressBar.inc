    OMIT('_EndOfInclude_',_BHProgressBarIncluded_)
_BHProgressBarIncluded_     EQUATE(1)
! Subclass
!myProg  bhProgressBar

! Init Progress Bar
!myProg.Init(Number Of Records,<Show Percentage Figure>)

! Update Progress Window Inside Loop
!loop
!   if (myProg.UpdateProgressBar(Update Text))
!       break
!   end
!end ! Loop

! Close Progress Down
!myProg.Kill()

! ===========================================================================================
BHProgressBar       CLASS,TYPE,MODULE('BHProgressBar.clw'),LINK('BHProgressBar.clw',1),DLL(0)
! ===========================================================================================
ProgressThermometer     BYTE(),PRIVATE
RecordsToProcess        LONG(),PRIVATE
RecordsProcessed        LONG(),PRIVATE
PercentProgress         BYTE(),PRIVATE
UserString              STRING(100),PRIVATE
PercentString           STRING(60),PRIVATE
CancelButtonFEQ         LONG(),PRIVATE
SkipRecords             LONG(),PRIVATE
SkipCounter             LONG(),PRIVATE
Init                    PROCEDURE(LONG totalRecords,BYTE hidePercent=0,LONG skipRecords=0)
Kill                    PROCEDURE()
UpdateProgressBar       PROCEDURE(<STRING userString>),BYTE
ClearVariables          PROCEDURE(),PRIVATE
tProgressWindow         &Window
                    END

bhProgressWindow      WINDOW('Progress...'),AT(,,210,64),CENTER,FONT('Tahoma', 8,, FONT:regular, CHARSET:ANSI),GRAY,TIMER(1),DOUBLE
                        PROGRESS, AT(17,15,175,12), USE(?bhProgressThermometer), RANGE(0,100)
                        STRING(''), AT(0,3,211,10), USE(?bhUserString), CENTER
                        STRING(''), AT(0,30,208,10), USE(?bhPercentString), CENTER
                        BUTTON('Cancel'), AT(80,44,50,15), USE(?bhProgressCancel)
                        END

    _EndOfInclude_