!region Declarations
glo:LogFilePath     CSTRING(255),STATIC

LICENSE File,Driver('Btrieve'),OEM,Name('LICENSE.DAT'),pre(lic),create,bindable,thread,owner('pccspccs'),encrypt
RecordNumberKey key(lic:RecordNumber),Nocase,primary
Record          record,pre()
RecordNumber        long()
ExpiryDate          date()
                end
        end
        
DefModelQueue       Queue,Type
Manufacturer            String(30)
ModelNumber             String(30)
Quantity                Long
                    End

DefJobNumberQueue   Queue,Type
JobNumber               Long
IMEINumber              String(30)
ModelNumber             String(30)
Accessories             String(255)
                    End
qDefRec    QUEUE,TYPE
RecordNumber            LONG
                    END

gFaultCodes     GROUP,PRE(gfc),TYPE
Manufacturer            STRING(30)
FaultCode               STRING(30),DIM(20)
                END

NewJobBooking       Group,Type
JobNumber               Long()
AccountNumber           String(30)
IMEINumber              String(30)
MSN                     String(30)
ModelNumber             String(30)
ProductCode             String(30)
UnitType                String(30)
Colour                  String(30)
Network                 String(30)
Type                    String(3)
ActivationDate          DATE()
                    End ! Group

gExchangeOrders     GROUP,TYPE
OrderNumber                 LONG
InvoiceNumber               LONG
Price                       REAL
ReceivedDate                DATE
OrderedDate                 DATE
ReceivedBy                  STRING(3)
                    END

gGetOutFaults       GROUP(),PRE(gGetOutFaults),TYPE
cFaultCode              STRING(30)
cIndex                  LONG()
cFaultDescription       STRING(60)
cRepairType             STRING(30)
wFaultCode              STRING(30)
wFaultDescription       STRING(60)
wIndex                  LONG()
wRepairType             STRING(30)
keyRepair               LONG()
                    END ! GROUP

qStockAudit         QUEUE(),PRE(qStockAudit),STATIC
SessionID               LONG()
RecordNumber            LONG()
StoAuditRecordNumber    LONG()
StockRecordNumber       LONG()
TotalAudited            LONG()
TotalLinesInAudit       LONG()
StockQty                LONG()
LinesNotAudited         LONG()
PercentageAudited       REAL()
ShortagesExcess         STRING(1) ! S or E
                    END ! QUEUE

qStockCheck         QUEUE(),PRE(qStockCheck),STATIC
SessionID               LONG()
RefNumber               LONG()
Description             STRING(30)
ShelfLocation           STRING(30)
Usage                   LONG()
                    END ! 
qIncomeReport       QUEUE(),PRE(qIncomeReport),STATIC
SessionID               LONG()
RefNumber               LONG()
InvoiceNumber           STRING(30)
JobNumber               STRING(30)
InvoiceDate             STRING(30)
Repair                  STRING(3)
Exchanged               STRING(3)
Loan                    STRING(3)
AccountNumberGeneric    STRING(30)
AccountNameGeneric      STRING(30)
WaybillNumber           STRING(30)
HandlingFee             DECIMAL(10,2)
ARCCharge               DECIMAL(10,2)
LostLoanCharge          DECIMAL(10,2)
PartsCost               DECIMAL(10,2)
PartsSelling            DECIMAL(10,2)
Labour                  DECIMAL(10,2)
ARCMarkup               DECIMAL(10,2)
VAT                     DECIMAL(10,2)
Total                   DECIMAL(10,2)
CashAmount              DECIMAL(10,2)
CreditAmount            DECIMAL(10,2)
AmountPaid              DECIMAL(10,2)
Outstanding             DECIMAL(10,2)
CollectionBy            STRING(30)
                    END ! QUEUE
qWIncomeReport       QUEUE(),PRE(qWIncomeReport),STATIC
SessionID                   LONG()
RefNumber                   LONG()
BranchID                STRING(30)
JobNumber                   STRING(30)
FullJobNumber               STRING(30)
FranchiseAccountNumber      STRING(30)
FranchiseAccountName        STRING(30)
JobAccountNumber            STRING(30)
Repair                  STRING(3)
Exchanged               STRING(3)
HandlingFee             DECIMAL(10,2)
PartsCost               DECIMAL(10,2)
PartsSelling            DECIMAL(10,2)
Labour                      DECIMAL(10,2)
SubTotal DECIMAL(10,2)
VAT                     DECIMAL(10,2)
Total                       DECIMAL(10,2)
CompanyName                 STRING(30)
WarrantyStatus          STRING(30)
RejectionBy             STRING(30)
Engineer                    STRING(70)
Manufacturer                STRING(30)
ModelNumber                 STRING(30)
ChargeType                  STRING(30)
RepairType                  STRING(30)
CompletedDate               DATE()
RejectionReason1            STRING(255)
RejectionReason2            STRING(255)
RejectionReason3            STRING(255)
ClaimSubmitted              DATE()
                        END ! QUEUE

qHandlingFeeReport  QUEUE(),PRE(qHandlingFeeReport),STATIC
SessionID               LONG()
RefNumber               LONG()
Manufacturer            STRING(30)
ModelNumber             STRING(30)
RepairType              STRING(10)
ActionDate              DATE()
CompanyName             STRING(30)
AccountNumber           STRING(30)
ChargeType              STRING(60)
JobNumber               STRING(60)
RRCJobNumber            LONG()
HandlingFee             DECIMAL(10,2)
Repair                  STRING(3)
EngineerOption          STRING(3)
WaybillNo               STRING(30)
WaybillDate             DATE
ARCWaybillDate          DATE
BranchID                STRING(10)
OriginalIMEI            STRING(30)
JobBookedDate           DATE()
ExchangeIMEI            STRING(30)
ExchangeIMEIDate        DATE()
                    END ! QUEUE

qStockValueReport   QUEUE(),PRE(qStockValueReport),STATIC 
SessionID               LONG()
SiteLocation            STRING(30)
Manufacturer            STRING(30)
Quantity                LONG()
PurchaseValue           REAL!DECIMAL(10,2)
AveragePurchaseCost     REAL!DECIMAL(10,2)
SaleValue               REAL!DECIMAL(10,2)
PartNumber              STRING(30)
Description             STRING(30)
FirstModel              STRING(30)
MinimumLevel            LONG()

                    END ! QUEUE

qStatusReport       QUEUE(),PRE(qStatusReport),STATIC
SessionID               LONG()
JobNumber               LONG()
FullJobNumber           STRING(30)
OSDays                  LONG()
JDaysInStatus           LONG()
EDaysInStatus           LONG()
LDaysInStatus           LONG()
                    END ! QUEUE
qStatusReportAudit  QUEUE(),PRE(qStatusReportAudit),STATIC
SessionID               LONG()
RecordNumber                LONG()
                    END ! QUEUE
qStatusReportCParts QUEUE(),PRE(qStatusReportCParts),STATIC
SessionID               LONG()
RecordNumber            LONG()
                    END ! QUEUE
qStatusReportWParts QUEUE(),PRE(qStatusReportWParts),STATIC
SessionID               LONG()
RecordNumber            LONG()
                    END ! QUEUE
qStatusReportStatus QUEUE(),PRE(qStatusReportStatus),STATIC
SessionID               LONG()
StatusType              LONG() ! 0 JOB / 1 EXC / 2 LOA
Status                  STRING(30)
Number                  LONG()
RRCNumber               LONG()
                    END ! IF

! Q to save accessories on a job to determine if they have been changed
! when the job is saved
qSaveAccessories    QUEUE(),PRE(qSaveAccessories),STATIC
SessionID               LONG()
Accessory               STRING(30)
                    END ! QUEUE

qLinkedFaultCodes   QUEUE(),PRE(qLinkedFaultCodes),STATIC
SessionID               LONG()
RecordNumber            LONG()
FaultType               STRING(1)
                    END ! QUEUE

!endregion

!region Version Numbers
! Global Version Number
kVersionNumber              EQUATE('14.3')
! Webserver Versions
! Always include phase number in brackest, e.g. (Ph1)
! Revisit if the phase reaches double figures
kWebserverVersion_PH2       EQUATE('11 (Ph2)')
kWebserverVersion_PH3       EQUATE('12 (Ph3)')
kWebserverVersion_PH4       EQUATE('07 (Ph4)')
kWebserverVersion_PH5       EQUATE('08 (Ph5)')
kWebserverVersion_PH6       EQUATE('09 (Ph6)')
kWebserverVersion_PH7       EQUATE('10 (Ph7)')
kWebserverVersion_PH8       EQUATE('11 (Ph8)')
kWebserverVersion_PH9       EQUATE('49')
kWebserverCurrentVersion    EQUATE(kWebserverVersion_PH9)

! Report Versions
kVODR0083                   EQUATE('5012')
kVODR0100                   EQUATE('5005')

!endregion

!region Variables
kCommentPOPTypePassword     EQUATE('Password To Change POP Type') ! Webserver
kShelfLocation      EQUATE('SHELFLOCATION') ! Tag Equate
kSubAccount         EQUATE('SUB ACCOUNT') ! Tag Equate
kGenericAccount     EQUATE('HEAD ACCOUNT')
kStatusTypes        EQUATE('STATUS')
kInternalLocation EQUATE('LOCATION')
!endregion

