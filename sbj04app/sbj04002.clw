

   MEMBER('sbj04app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ04002.INC'),ONCE        !Local module procedure declarations
                     END


Job_Booking_Defaults_Window PROCEDURE                 !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Intial_Transit_Type STRING(30)
tmp:Trade_Account    STRING(15)
tmp:Force_Mobile_Number BYTE(0)
tmp:HeadAccount      STRING(20)
window               WINDOW('General Defaults'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Booking Defaults'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Booking Defaults'),USE(?Tab1)
                           PROMPT('Initial Transit Type'),AT(264,168),USE(?tmp:Intial_Transit_Type:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(264,178,124,10),USE(tmp:Intial_Transit_Type),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(392,176),USE(?Transit_Type),TRN,FLAT,LEFT,ICON('lookupp.jpg')
                           PROMPT('Account Number'),AT(264,206),USE(?tmp:Trade_Account:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(392,214),USE(?Account_No),TRN,FLAT,LEFT,ICON('lookupp.jpg')
                           ENTRY(@s15),AT(264,218,124,10),USE(tmp:Trade_Account),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           CHECK('Make Mobile Number Compulsory'),AT(264,242),USE(tmp:Force_Mobile_Number),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                         END
                       END
                       BUTTON,AT(308,260),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(372,260),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020508'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Job_Booking_Defaults_Window')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:SUBTRACC.Open
  Relate:TRANTYPE.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  If glo:WebJob
      tmp:HeadAccount = Clarionet:Global.Param2
  Else
      If GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI') <> ''
          tmp:HeadAccount = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
      End !If GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI') <> ''
  End
  
  tmp:Intial_Transit_Type = GETINI(tmp:HeadAccount,'InitialTransitType',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:Trade_Account = GETINI(tmp:HeadAccount,'TradeAccount',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:Force_Mobile_Number = GETINI(tmp:HeadAccount,'ForceMobileNo',,CLIP(PATH())&'\SB2KDEF.INI')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:SUBTRACC.Close
    Relate:TRANTYPE.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Account_No
      glo:Select1   = ''
      glo:Select13 = ''
      If glo:WebJob = 1
          glo:Select1   = Clarionet:Global.Param2
      Else !If glo:WebJob = 1
          If GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI') <> ''
              glo:Select1 = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
          End !If GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI') <> ''
      End !If glo:WebJob = 1
    OF ?OkButton
      PUTINIext(tmp:HeadAccount,'InitialTransitType',tmp:Intial_Transit_Type,CLIP(PATH())&'\SB2KDEF.INI')
      PUTINIext(tmp:HeadAccount,'TradeAccount',tmp:Trade_Account,CLIP(PATH())&'\SB2KDEF.INI')
      PUTINIext(tmp:HeadAccount,'ForceMobileNo',tmp:Force_Mobile_Number,CLIP(PATH())&'\SB2KDEF.INI')
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020508'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020508'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020508'&'0')
      ***
    OF ?Transit_Type
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTransitTypes
      ThisWindow.Reset
      if globalresponse = requestcompleted
          tmp:Intial_Transit_Type = trt:Transit_Type
      end
      
      display()
    OF ?Account_No
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickWebAccounts
      ThisWindow.Reset
      if (glO:Select13 <> '')
          !tmp:Trade_Account = sub:Account_Number
          tmp:Trade_Account = clip(glo:Select13) ! #11633 Allow for new "generic account" setting (DBH: 13/08/2010)
      end
      
      display()
    OF ?OkButton
      ThisWindow.Update
       POST(Event:CloseWindow)
    OF ?CancelButton
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
IMEIHistorySearch PROCEDURE (f:IMEI)                  !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::13:TAGDISPSTATUS   BYTE(0)
DASBRW::13:QUEUE          QUEUE
Job_Number_Pointer            LIKE(glo:Job_Number_Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
LocalRequest         LONG
tmp:GlobalIMEINumber STRING(30)
save_addsearch_id    USHORT,AUTO
FilesOpened          BYTE
Search_Field_Temp    STRING(30)
Search_By_Temp       BYTE
Search_Result_Temp   STRING(30)
Trade_Customer_Temp  STRING(46)
Customer_Name_Temp   STRING(70)
Days_Since_Booking_Temp LONG
total_temp           REAL
balance_temp         REAL
vat_total_warranty_temp REAL
total_warranty_temp  REAL
vat_total_estimate_temp REAL
total_estimate_temp  REAL
vat_total_temp       REAL
msn_temp             STRING(30)
tmp:JobNumber        STRING(20)
tmp:ExchangeIMEI     STRING(30)
tmp:CLabourCost      REAL
tmp:CPartsCost       REAL
tmp:CSubTotal        REAL
tmp:CVAT             REAL
tmp:CTotal           REAL
tmp:WLabourCost      REAL
tmp:WPartsCost       REAL
tmp:WSubTotal        REAL
tmp:WVAT             REAL
tmp:WTotal           REAL
tmp:ELabourCost      REAL
tmp:EPartsCost       REAL
tmp:ESubTotal        REAL
tmp:EVAT             REAL
tmp:ETotal           REAL
tmp:RepairTypes      STRING(65)
tmp:ColourFlag       STRING(1)
tmp:Exchanged        STRING(3)
Tagged               STRING(1)
tmp:Parts            STRING(30),DIM(12)
Save_par_ID          USHORT
Save_war_ID          USHORT
Save_job_ID          USHORT
BRW1::View:Browse    VIEW(ADDSEARCH)
                       PROJECT(addtmp:JobNumber)
                       PROJECT(addtmp:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
Tagged                 LIKE(Tagged)                   !List box control field - type derived from local data
Tagged_NormalFG        LONG                           !Normal forground color
Tagged_NormalBG        LONG                           !Normal background color
Tagged_SelectedFG      LONG                           !Selected forground color
Tagged_SelectedBG      LONG                           !Selected background color
Tagged_Icon            LONG                           !Entry's icon ID
tmp:JobNumber          LIKE(tmp:JobNumber)            !List box control field - type derived from local data
tmp:JobNumber_NormalFG LONG                           !Normal forground color
tmp:JobNumber_NormalBG LONG                           !Normal background color
tmp:JobNumber_SelectedFG LONG                         !Selected forground color
tmp:JobNumber_SelectedBG LONG                         !Selected background color
addtmp:JobNumber       LIKE(addtmp:JobNumber)         !List box control field - type derived from field
addtmp:JobNumber_NormalFG LONG                        !Normal forground color
addtmp:JobNumber_NormalBG LONG                        !Normal background color
addtmp:JobNumber_SelectedFG LONG                      !Selected forground color
addtmp:JobNumber_SelectedBG LONG                      !Selected background color
job_ali:Ref_Number     LIKE(job_ali:Ref_Number)       !List box control field - type derived from field
job_ali:Ref_Number_NormalFG LONG                      !Normal forground color
job_ali:Ref_Number_NormalBG LONG                      !Normal background color
job_ali:Ref_Number_SelectedFG LONG                    !Selected forground color
job_ali:Ref_Number_SelectedBG LONG                    !Selected background color
job_ali:Account_Number LIKE(job_ali:Account_Number)   !List box control field - type derived from field
job_ali:Account_Number_NormalFG LONG                  !Normal forground color
job_ali:Account_Number_NormalBG LONG                  !Normal background color
job_ali:Account_Number_SelectedFG LONG                !Selected forground color
job_ali:Account_Number_SelectedBG LONG                !Selected background color
job_ali:Surname        LIKE(job_ali:Surname)          !List box control field - type derived from field
job_ali:Surname_NormalFG LONG                         !Normal forground color
job_ali:Surname_NormalBG LONG                         !Normal background color
job_ali:Surname_SelectedFG LONG                       !Selected forground color
job_ali:Surname_SelectedBG LONG                       !Selected background color
job_ali:Mobile_Number  LIKE(job_ali:Mobile_Number)    !List box control field - type derived from field
job_ali:Mobile_Number_NormalFG LONG                   !Normal forground color
job_ali:Mobile_Number_NormalBG LONG                   !Normal background color
job_ali:Mobile_Number_SelectedFG LONG                 !Selected forground color
job_ali:Mobile_Number_SelectedBG LONG                 !Selected background color
job_ali:ESN            LIKE(job_ali:ESN)              !List box control field - type derived from field
job_ali:ESN_NormalFG   LONG                           !Normal forground color
job_ali:ESN_NormalBG   LONG                           !Normal background color
job_ali:ESN_SelectedFG LONG                           !Selected forground color
job_ali:ESN_SelectedBG LONG                           !Selected background color
job_ali:MSN            LIKE(job_ali:MSN)              !List box control field - type derived from field
job_ali:MSN_NormalFG   LONG                           !Normal forground color
job_ali:MSN_NormalBG   LONG                           !Normal background color
job_ali:MSN_SelectedFG LONG                           !Selected forground color
job_ali:MSN_SelectedBG LONG                           !Selected background color
tmp:Exchanged          LIKE(tmp:Exchanged)            !List box control field - type derived from local data
tmp:Exchanged_NormalFG LONG                           !Normal forground color
tmp:Exchanged_NormalBG LONG                           !Normal background color
tmp:Exchanged_SelectedFG LONG                         !Selected forground color
tmp:Exchanged_SelectedBG LONG                         !Selected background color
tmp:RepairTypes        LIKE(tmp:RepairTypes)          !List box control field - type derived from local data
tmp:RepairTypes_NormalFG LONG                         !Normal forground color
tmp:RepairTypes_NormalBG LONG                         !Normal background color
tmp:RepairTypes_SelectedFG LONG                       !Selected forground color
tmp:RepairTypes_SelectedBG LONG                       !Selected background color
job_ali:Charge_Type    LIKE(job_ali:Charge_Type)      !List box control field - type derived from field
job_ali:Charge_Type_NormalFG LONG                     !Normal forground color
job_ali:Charge_Type_NormalBG LONG                     !Normal background color
job_ali:Charge_Type_SelectedFG LONG                   !Selected forground color
job_ali:Charge_Type_SelectedBG LONG                   !Selected background color
job_ali:Warranty_Charge_Type LIKE(job_ali:Warranty_Charge_Type) !List box control field - type derived from field
job_ali:Warranty_Charge_Type_NormalFG LONG            !Normal forground color
job_ali:Warranty_Charge_Type_NormalBG LONG            !Normal background color
job_ali:Warranty_Charge_Type_SelectedFG LONG          !Selected forground color
job_ali:Warranty_Charge_Type_SelectedBG LONG          !Selected background color
job_ali:Date_Completed LIKE(job_ali:Date_Completed)   !List box control field - type derived from field
job_ali:Date_Completed_NormalFG LONG                  !Normal forground color
job_ali:Date_Completed_NormalBG LONG                  !Normal background color
job_ali:Date_Completed_SelectedFG LONG                !Selected forground color
job_ali:Date_Completed_SelectedBG LONG                !Selected background color
Days_Since_Booking_Temp LIKE(Days_Since_Booking_Temp) !List box control field - type derived from local data
Days_Since_Booking_Temp_NormalFG LONG                 !Normal forground color
Days_Since_Booking_Temp_NormalBG LONG                 !Normal background color
Days_Since_Booking_Temp_SelectedFG LONG               !Selected forground color
Days_Since_Booking_Temp_SelectedBG LONG               !Selected background color
job_ali:Exchange_Unit_Number LIKE(job_ali:Exchange_Unit_Number) !List box control field - type derived from field
job_ali:Exchange_Unit_Number_NormalFG LONG            !Normal forground color
job_ali:Exchange_Unit_Number_NormalBG LONG            !Normal background color
job_ali:Exchange_Unit_Number_SelectedFG LONG          !Selected forground color
job_ali:Exchange_Unit_Number_SelectedBG LONG          !Selected background color
job_ali:Warranty_Job   LIKE(job_ali:Warranty_Job)     !List box control field - type derived from field
job_ali:Warranty_Job_NormalFG LONG                    !Normal forground color
job_ali:Warranty_Job_NormalBG LONG                    !Normal background color
job_ali:Warranty_Job_SelectedFG LONG                  !Selected forground color
job_ali:Warranty_Job_SelectedBG LONG                  !Selected background color
job_ali:Chargeable_Job LIKE(job_ali:Chargeable_Job)   !List box control field - type derived from field
job_ali:Chargeable_Job_NormalFG LONG                  !Normal forground color
job_ali:Chargeable_Job_NormalBG LONG                  !Normal background color
job_ali:Chargeable_Job_SelectedFG LONG                !Selected forground color
job_ali:Chargeable_Job_SelectedBG LONG                !Selected background color
tmp:ColourFlag         LIKE(tmp:ColourFlag)           !List box control field - type derived from local data
tmp:ColourFlag_NormalFG LONG                          !Normal forground color
tmp:ColourFlag_NormalBG LONG                          !Normal background color
tmp:ColourFlag_SelectedFG LONG                        !Selected forground color
tmp:ColourFlag_SelectedBG LONG                        !Selected background color
job_ali:Model_Number   LIKE(job_ali:Model_Number)     !Browse hot field - type derived from field
job_ali:Manufacturer   LIKE(job_ali:Manufacturer)     !Browse hot field - type derived from field
job_ali:Unit_Type      LIKE(job_ali:Unit_Type)        !Browse hot field - type derived from field
job_ali:Loan_Status    LIKE(job_ali:Loan_Status)      !Browse hot field - type derived from field
job_ali:Exchange_Status LIKE(job_ali:Exchange_Status) !Browse hot field - type derived from field
msn_temp               LIKE(msn_temp)                 !Browse hot field - type derived from local data
job_ali:Postcode       LIKE(job_ali:Postcode)         !Browse hot field - type derived from field
job_ali:Company_Name   LIKE(job_ali:Company_Name)     !Browse hot field - type derived from field
job_ali:Telephone_Delivery LIKE(job_ali:Telephone_Delivery) !Browse hot field - type derived from field
job_ali:Address_Line3_Delivery LIKE(job_ali:Address_Line3_Delivery) !Browse hot field - type derived from field
job_ali:Address_Line2_Delivery LIKE(job_ali:Address_Line2_Delivery) !Browse hot field - type derived from field
job_ali:Company_Name_Delivery LIKE(job_ali:Company_Name_Delivery) !Browse hot field - type derived from field
job_ali:Address_Line1_Delivery LIKE(job_ali:Address_Line1_Delivery) !Browse hot field - type derived from field
job_ali:Postcode_Delivery LIKE(job_ali:Postcode_Delivery) !Browse hot field - type derived from field
job_ali:Telephone_Collection LIKE(job_ali:Telephone_Collection) !Browse hot field - type derived from field
job_ali:Address_Line3_Collection LIKE(job_ali:Address_Line3_Collection) !Browse hot field - type derived from field
job_ali:Address_Line2_Collection LIKE(job_ali:Address_Line2_Collection) !Browse hot field - type derived from field
job_ali:Address_Line1_Collection LIKE(job_ali:Address_Line1_Collection) !Browse hot field - type derived from field
job_ali:Company_Name_Collection LIKE(job_ali:Company_Name_Collection) !Browse hot field - type derived from field
job_ali:Postcode_Collection LIKE(job_ali:Postcode_Collection) !Browse hot field - type derived from field
job_ali:Fax_Number     LIKE(job_ali:Fax_Number)       !Browse hot field - type derived from field
job_ali:Address_Line3  LIKE(job_ali:Address_Line3)    !Browse hot field - type derived from field
job_ali:Telephone_Number LIKE(job_ali:Telephone_Number) !Browse hot field - type derived from field
job_ali:Address_Line2  LIKE(job_ali:Address_Line2)    !Browse hot field - type derived from field
job_ali:Address_Line1  LIKE(job_ali:Address_Line1)    !Browse hot field - type derived from field
job_ali:Courier_Cost   LIKE(job_ali:Courier_Cost)     !Browse hot field - type derived from field
job_ali:Advance_Payment LIKE(job_ali:Advance_Payment) !Browse hot field - type derived from field
job_ali:Labour_Cost    LIKE(job_ali:Labour_Cost)      !Browse hot field - type derived from field
job_ali:Parts_Cost     LIKE(job_ali:Parts_Cost)       !Browse hot field - type derived from field
job_ali:Sub_Total      LIKE(job_ali:Sub_Total)        !Browse hot field - type derived from field
job_ali:Courier_Cost_Estimate LIKE(job_ali:Courier_Cost_Estimate) !Browse hot field - type derived from field
job_ali:Labour_Cost_Estimate LIKE(job_ali:Labour_Cost_Estimate) !Browse hot field - type derived from field
job_ali:Parts_Cost_Estimate LIKE(job_ali:Parts_Cost_Estimate) !Browse hot field - type derived from field
job_ali:Sub_Total_Estimate LIKE(job_ali:Sub_Total_Estimate) !Browse hot field - type derived from field
job_ali:Courier_Cost_Warranty LIKE(job_ali:Courier_Cost_Warranty) !Browse hot field - type derived from field
job_ali:Labour_Cost_Warranty LIKE(job_ali:Labour_Cost_Warranty) !Browse hot field - type derived from field
job_ali:Parts_Cost_Warranty LIKE(job_ali:Parts_Cost_Warranty) !Browse hot field - type derived from field
job_ali:Sub_Total_Warranty LIKE(job_ali:Sub_Total_Warranty) !Browse hot field - type derived from field
total_temp             LIKE(total_temp)               !Browse hot field - type derived from local data
balance_temp           LIKE(balance_temp)             !Browse hot field - type derived from local data
vat_total_warranty_temp LIKE(vat_total_warranty_temp) !Browse hot field - type derived from local data
total_warranty_temp    LIKE(total_warranty_temp)      !Browse hot field - type derived from local data
vat_total_estimate_temp LIKE(vat_total_estimate_temp) !Browse hot field - type derived from local data
total_estimate_temp    LIKE(total_estimate_temp)      !Browse hot field - type derived from local data
vat_total_temp         LIKE(vat_total_temp)           !Browse hot field - type derived from local data
job_ali:Current_Status LIKE(job_ali:Current_Status)   !Browse hot field - type derived from field
addtmp:RecordNumber    LIKE(addtmp:RecordNumber)      !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW3::View:Browse    VIEW(PARTS)
                       PROJECT(par:Quantity)
                       PROJECT(par:Description)
                       PROJECT(par:Record_Number)
                       PROJECT(par:Ref_Number)
                       PROJECT(par:Part_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
par:Quantity           LIKE(par:Quantity)             !List box control field - type derived from field
par:Description        LIKE(par:Description)          !List box control field - type derived from field
par:Record_Number      LIKE(par:Record_Number)        !Primary key field - type derived from field
par:Ref_Number         LIKE(par:Ref_Number)           !Browse key field - type derived from field
par:Part_Number        LIKE(par:Part_Number)          !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW5::View:Browse    VIEW(WARPARTS)
                       PROJECT(wpr:Quantity)
                       PROJECT(wpr:Part_Number)
                       PROJECT(wpr:Record_Number)
                       PROJECT(wpr:Ref_Number)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:3
wpr:Quantity           LIKE(wpr:Quantity)             !List box control field - type derived from field
wpr:Part_Number        LIKE(wpr:Part_Number)          !List box control field - type derived from field
wpr:Record_Number      LIKE(wpr:Record_Number)        !Primary key field - type derived from field
wpr:Ref_Number         LIKE(wpr:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Auto Search Facility'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PROMPT('I.M.E.I. Number Search'),AT(8,10),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,8,640,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(588,10),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,384,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(4,28,672,234),USE(?Sheet1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('By I.M.E.I. Number'),USE(?ESN_Tab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING('Title'),AT(8,32),USE(?Match_Found_Text),TRN,FONT('Tahoma',10,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON('sho&W tags'),AT(387,192,1,1),USE(?DASSHOWTAG),HIDE
                           BUTTON('&Rev tags'),AT(387,194,1,1),USE(?DASREVTAG),HIDE
                         END
                       END
                       LIST,AT(8,48,664,208),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),VCR,FORMAT('10L(2)|M*I@s1@74L(2)|M*~Job Number~@s20@0R(2)|M*~Job Number~@s8@0R(2)|M*~Job No~' &|
   '@p<<<<<<<<<<<<#p@64L(2)|M*~Account Number~@s15@79L(2)|M*~Surname~@s30@63L(2)|M*~Mobile' &|
   ' Number~@s15@69L(2)|M*~I.M.E.I. Number~@s16@45L(2)|M*~MSN~@s10@40C(2)|M*~Exchang' &|
   'ed~@s3@100L(2)|M*~Repair Type~@s65@82L(2)|M*~Char Charge Type~@s30@82L(2)|M*~War' &|
   ' Charge Type~@s30@57R(2)|M*~Date Completed~L@D6b@24R(2)|M*~Days~@n_6@0R(2)|M*~Ex' &|
   'change Unit Number~@s8@0R(2)|M*~Warranty Job~@s3@0R(2)|M*~Warranty Job~@s3@0R(2)' &|
   '|M*~tmp : Colour Flag~@s1@'),FROM(Queue:Browse)
                       STRING('Unit Type'),AT(227,302),USE(?String14),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                       STRING(@s25),AT(295,302,124,12),USE(job_ali:Unit_Type),TRN,FONT(,,080FFFFH,FONT:bold)
                       STRING('Loan Status'),AT(227,322),USE(?String15),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                       STRING(@s30),AT(295,322,128,12),USE(job_ali:Loan_Status),TRN,FONT(,,080FFFFH,FONT:bold)
                       STRING('Exchange Status'),AT(227,332),USE(?String16),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                       STRING(@s30),AT(295,332,124,12),USE(job_ali:Exchange_Status),TRN,FONT(,,080FFFFH,FONT:bold)
                       STRING('Exchange IMEI'),AT(227,342),USE(?String16:2),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                       STRING(@s30),AT(295,342,124,12),USE(tmp:ExchangeIMEI),TRN,FONT(,,080FFFFH,FONT:bold)
                       BUTTON,AT(224,352),USE(?Button:SearchUsingExchangeIMEI),TRN,FLAT,ICON('serimeip.jpg')
                       BUTTON,AT(384,352),USE(?Button:ViewCosts),TRN,FLAT,ICON('costsp.jpg')
                       STRING('Job Status'),AT(227,312),USE(?String15:2),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                       STRING(@s30),AT(295,312,128,12),USE(job_ali:Current_Status),TRN,FONT(,,080FFFFH,FONT:bold)
                       SHEET,AT(460,264,212,116),USE(?Sheet5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('Chargeable Parts'),USE(?ChargeablePartsTab),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(464,280,204,96),USE(?List:2),IMM,HSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),VCR,FORMAT('21R(2)|M~Qty~@n4@120L(2)|M~Description~@s30@'),FROM(Queue:Browse:1)
                         END
                         TAB('Warranty Parts'),USE(?WarrantyPartsTab),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH,COLOR:White,09A6A7CH)
                           LIST,AT(464,280,204,96),USE(?List:3),IMM,HSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),VCR,FORMAT('24R(2)|M~Qty~@s8@120L(2)|M~Part Number~@s30@'),FROM(Queue:Browse:2)
                         END
                       END
                       BUTTON,AT(224,264),USE(?DASTAG),TRN,FLAT,ICON('tagitemp.jpg')
                       BUTTON,AT(302,264),USE(?DASTAGAll),TRN,FLAT,ICON('tagallp.jpg')
                       BUTTON,AT(380,264),USE(?DASUNTAGALL),TRN,FLAT,ICON('untagalp.jpg')
                       PANEL,AT(216,264,240,27),USE(?Panel4),FILL(09A6A7CH)
                       SHEET,AT(4,264,208,116),USE(?Sheet2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('Invoice'),USE(?Invoice_Tab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(8,280),USE(job_ali:Company_Name),FONT(,,,FONT:bold)
                           STRING(@s30),AT(8,293),USE(job_ali:Address_Line1),FONT(,,,FONT:bold)
                           STRING(@s30),AT(8,304),USE(job_ali:Address_Line2),FONT(,,,FONT:bold)
                           STRING(@s30),AT(8,317),USE(job_ali:Address_Line3),FONT(,,,FONT:bold)
                           STRING(@s10),AT(8,328),USE(job_ali:Postcode),FONT(,,,FONT:bold)
                           STRING(@s15),AT(8,341),USE(job_ali:Telephone_Number),FONT(,,,FONT:bold)
                           STRING(@s15),AT(8,352),USE(job_ali:Fax_Number),FONT(,,,FONT:bold)
                         END
                         TAB('Collection'),USE(?Collection_Tab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(8,280),USE(job_ali:Company_Name_Collection),FONT(,,,FONT:bold)
                           STRING(@s30),AT(8,293),USE(job_ali:Address_Line1_Collection),FONT(,,,FONT:bold)
                           STRING(@s30),AT(8,304),USE(job_ali:Address_Line2_Collection),FONT(,,,FONT:bold)
                           STRING(@s30),AT(8,317),USE(job_ali:Address_Line3_Collection),FONT(,,,FONT:bold)
                           STRING(@s10),AT(8,328),USE(job_ali:Postcode_Collection),FONT(,,,FONT:bold)
                           STRING(@s15),AT(8,341),USE(job_ali:Telephone_Collection),FONT(,,,FONT:bold)
                         END
                         TAB('Delivery'),USE(?delivery_tab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(8,280),USE(job_ali:Company_Name_Delivery),FONT(,,,FONT:bold)
                           STRING(@s30),AT(8,293),USE(job_ali:Address_Line1_Delivery),FONT(,,,FONT:bold)
                           STRING(@s30),AT(8,304),USE(job_ali:Address_Line2_Delivery),FONT(,,,FONT:bold)
                           STRING(@s30),AT(8,317),USE(job_ali:Address_Line3_Delivery),FONT(,,,FONT:bold)
                           STRING(@s10),AT(8,328),USE(job_ali:Postcode_Delivery),FONT(,,,FONT:bold)
                           STRING(@s15),AT(8,341),USE(job_ali:Telephone_Delivery),FONT(,,,FONT:bold)
                         END
                       END
                       PANEL,AT(216,294,240,86),USE(?Panel3),FILL(09A6A7CH)
                       BUTTON,AT(8,384),USE(?audit_Trail),TRN,FLAT,LEFT,ICON('auditp.jpg')
                       BUTTON,AT(212,384),USE(?Button6),FLAT,HIDE,LEFT,ICON('invtextp.jpg')
                       BUTTON,AT(608,384),USE(?CancelButton),SKIP,TRN,FLAT,LEFT,ICON('closep.jpg'),STD(STD:Close)
                       BUTTON,AT(540,384),USE(?Button10),TRN,FLAT,ICON('viewjobp.jpg')
                       BUTTON,AT(76,384),USE(?Button6:2),TRN,FLAT,LEFT,ICON('faudescp.jpg')
                       BUTTON,AT(356,384),USE(?Print_Job_Card:2),TRN,FLAT,LEFT,ICON('prnboup.jpg')
                       BUTTON,AT(468,384),USE(?Button12),TRN,FLAT,ICON('keyp.jpg')
                       BUTTON,AT(288,384),USE(?Print_Job_Card),TRN,FLAT,LEFT,ICON('prnjobp.jpg')
                       BUTTON,AT(144,384),USE(?Button7),TRN,FLAT,LEFT,ICON('engnotp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW3                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW3::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW5                 CLASS(BrowseClass)               !Browse using ?List:3
Q                      &Queue:Browse:2                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::13:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW1.UpdateBuffer
   glo:q_JobNumber.Job_Number_Pointer = addtmp:JobNumber
   GET(glo:q_JobNumber,glo:q_JobNumber.Job_Number_Pointer)
  IF ERRORCODE()
     glo:q_JobNumber.Job_Number_Pointer = addtmp:JobNumber
     ADD(glo:q_JobNumber,glo:q_JobNumber.Job_Number_Pointer)
    Tagged = '*'
  ELSE
    DELETE(glo:q_JobNumber)
    Tagged = ''
  END
    Queue:Browse.Tagged = Tagged
  Queue:Browse.Tagged_NormalFG = -1
  Queue:Browse.Tagged_NormalBG = -1
  Queue:Browse.Tagged_SelectedFG = -1
  Queue:Browse.Tagged_SelectedBG = -1
  IF (tagged = '*')
    Queue:Browse.Tagged_Icon = 2
  ELSE
    Queue:Browse.Tagged_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::13:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW1.Reset
  FREE(glo:q_JobNumber)
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:q_JobNumber.Job_Number_Pointer = addtmp:JobNumber
     ADD(glo:q_JobNumber,glo:q_JobNumber.Job_Number_Pointer)
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::13:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:q_JobNumber)
  BRW1.Reset
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::13:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::13:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:q_JobNumber)
    GET(glo:q_JobNumber,QR#)
    DASBRW::13:QUEUE = glo:q_JobNumber
    ADD(DASBRW::13:QUEUE)
  END
  FREE(glo:q_JobNumber)
  BRW1.Reset
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::13:QUEUE.Job_Number_Pointer = addtmp:JobNumber
     GET(DASBRW::13:QUEUE,DASBRW::13:QUEUE.Job_Number_Pointer)
    IF ERRORCODE()
       glo:q_JobNumber.Job_Number_Pointer = addtmp:JobNumber
       ADD(glo:q_JobNumber,glo:q_JobNumber.Job_Number_Pointer)
    END
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::13:DASSHOWTAG Routine
   CASE DASBRW::13:TAGDISPSTATUS
   OF 0
      DASBRW::13:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::13:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::13:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW1.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
RefreshDetails      Routine
    Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
    xch:Ref_Number = job_ali:Exchange_Unit_Number
    If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
        !Found
        ?Button:SearchUsingExchangeIMEI{prop:Hide} = False
        tmp:ExchangeIMEI = xch:ESN
    Else ! If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
        !Error
        ?Button:SearchUsingExchangeIMEI{prop:Hide} = True
        tmp:ExchangeIMEI = ''
    End ! If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign


    If job_ali:Chargeable_Job = 'YES'
        ?ChargeablePartsTab{prop:Hide} = False
    Else ! If job_ali:Chargeable_Job = 'YES'
        ?ChargeablePartsTab{prop:Hide} = True
    End ! If job_ali:Chargeable_Job = 'YES'

    If job_ali:Warranty_Job = 'YES'
        ?WarrantyPartsTab{prop:Hide} = False
    Else ! If job_ali:Warranty_Job = 'YES'
        ?WarrantyPartsTab{prop:Hide} = True
    End ! If job_ali:Warranty_Job = 'YES'
    Display()

DisplayIMEINumber         Routine
    ?Match_Found_Text{prop:Text} = 'I.M.E.I. Number: ' & tmp:GlobalIMEINumber
    Display()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020665'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('IMEIHistorySearch')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:ADDSEARCH.Open
  Relate:EXCHANGE.Open
  Relate:INVOICE.Open
  Relate:JOBNOTES_ALIAS.Open
  Relate:VATCODE.Open
  Relate:WEBJOB.Open
  Access:JOBS_ALIAS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:JOBPAYMT_ALIAS.UseFile
  Access:JOBSE.UseFile
  Access:REPTYDEF.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?List,Queue:Browse.ViewPosition,BRW1::View:Browse,Queue:Browse,Relate:ADDSEARCH,SELF)
  BRW3.Init(?List:2,Queue:Browse:1.ViewPosition,BRW3::View:Browse,Queue:Browse:1,Relate:PARTS,SELF)
  BRW5.Init(?List:3,Queue:Browse:2.ViewPosition,BRW5::View:Browse,Queue:Browse:2,Relate:WARPARTS,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  tmp:GlobalIMEINumber = f:IMEI
  Do DisplayIMEINumber
  Do RefreshDetails
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ?List:3{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse
  BRW1.AddSortOrder(,addtmp:JobNumberKey)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,addtmp:JobNumber,1,BRW1)
  BIND('Tagged',Tagged)
  BIND('tmp:JobNumber',tmp:JobNumber)
  BIND('tmp:Exchanged',tmp:Exchanged)
  BIND('tmp:RepairTypes',tmp:RepairTypes)
  BIND('Days_Since_Booking_Temp',Days_Since_Booking_Temp)
  BIND('tmp:ColourFlag',tmp:ColourFlag)
  BIND('msn_temp',msn_temp)
  BIND('total_temp',total_temp)
  BIND('balance_temp',balance_temp)
  BIND('vat_total_warranty_temp',vat_total_warranty_temp)
  BIND('total_warranty_temp',total_warranty_temp)
  BIND('vat_total_estimate_temp',vat_total_estimate_temp)
  BIND('total_estimate_temp',total_estimate_temp)
  BIND('vat_total_temp',vat_total_temp)
  BIND('GLO:Select2',GLO:Select2)
  BIND('GLO:Select11',GLO:Select11)
  ?List{PROP:IconList,1} = '~off.ico'
  ?List{PROP:IconList,2} = '~on.ico'
  BRW1.AddField(Tagged,BRW1.Q.Tagged)
  BRW1.AddField(tmp:JobNumber,BRW1.Q.tmp:JobNumber)
  BRW1.AddField(addtmp:JobNumber,BRW1.Q.addtmp:JobNumber)
  BRW1.AddField(job_ali:Ref_Number,BRW1.Q.job_ali:Ref_Number)
  BRW1.AddField(job_ali:Account_Number,BRW1.Q.job_ali:Account_Number)
  BRW1.AddField(job_ali:Surname,BRW1.Q.job_ali:Surname)
  BRW1.AddField(job_ali:Mobile_Number,BRW1.Q.job_ali:Mobile_Number)
  BRW1.AddField(job_ali:ESN,BRW1.Q.job_ali:ESN)
  BRW1.AddField(job_ali:MSN,BRW1.Q.job_ali:MSN)
  BRW1.AddField(tmp:Exchanged,BRW1.Q.tmp:Exchanged)
  BRW1.AddField(tmp:RepairTypes,BRW1.Q.tmp:RepairTypes)
  BRW1.AddField(job_ali:Charge_Type,BRW1.Q.job_ali:Charge_Type)
  BRW1.AddField(job_ali:Warranty_Charge_Type,BRW1.Q.job_ali:Warranty_Charge_Type)
  BRW1.AddField(job_ali:Date_Completed,BRW1.Q.job_ali:Date_Completed)
  BRW1.AddField(Days_Since_Booking_Temp,BRW1.Q.Days_Since_Booking_Temp)
  BRW1.AddField(job_ali:Exchange_Unit_Number,BRW1.Q.job_ali:Exchange_Unit_Number)
  BRW1.AddField(job_ali:Warranty_Job,BRW1.Q.job_ali:Warranty_Job)
  BRW1.AddField(job_ali:Chargeable_Job,BRW1.Q.job_ali:Chargeable_Job)
  BRW1.AddField(tmp:ColourFlag,BRW1.Q.tmp:ColourFlag)
  BRW1.AddField(job_ali:Model_Number,BRW1.Q.job_ali:Model_Number)
  BRW1.AddField(job_ali:Manufacturer,BRW1.Q.job_ali:Manufacturer)
  BRW1.AddField(job_ali:Unit_Type,BRW1.Q.job_ali:Unit_Type)
  BRW1.AddField(job_ali:Loan_Status,BRW1.Q.job_ali:Loan_Status)
  BRW1.AddField(job_ali:Exchange_Status,BRW1.Q.job_ali:Exchange_Status)
  BRW1.AddField(msn_temp,BRW1.Q.msn_temp)
  BRW1.AddField(job_ali:Postcode,BRW1.Q.job_ali:Postcode)
  BRW1.AddField(job_ali:Company_Name,BRW1.Q.job_ali:Company_Name)
  BRW1.AddField(job_ali:Telephone_Delivery,BRW1.Q.job_ali:Telephone_Delivery)
  BRW1.AddField(job_ali:Address_Line3_Delivery,BRW1.Q.job_ali:Address_Line3_Delivery)
  BRW1.AddField(job_ali:Address_Line2_Delivery,BRW1.Q.job_ali:Address_Line2_Delivery)
  BRW1.AddField(job_ali:Company_Name_Delivery,BRW1.Q.job_ali:Company_Name_Delivery)
  BRW1.AddField(job_ali:Address_Line1_Delivery,BRW1.Q.job_ali:Address_Line1_Delivery)
  BRW1.AddField(job_ali:Postcode_Delivery,BRW1.Q.job_ali:Postcode_Delivery)
  BRW1.AddField(job_ali:Telephone_Collection,BRW1.Q.job_ali:Telephone_Collection)
  BRW1.AddField(job_ali:Address_Line3_Collection,BRW1.Q.job_ali:Address_Line3_Collection)
  BRW1.AddField(job_ali:Address_Line2_Collection,BRW1.Q.job_ali:Address_Line2_Collection)
  BRW1.AddField(job_ali:Address_Line1_Collection,BRW1.Q.job_ali:Address_Line1_Collection)
  BRW1.AddField(job_ali:Company_Name_Collection,BRW1.Q.job_ali:Company_Name_Collection)
  BRW1.AddField(job_ali:Postcode_Collection,BRW1.Q.job_ali:Postcode_Collection)
  BRW1.AddField(job_ali:Fax_Number,BRW1.Q.job_ali:Fax_Number)
  BRW1.AddField(job_ali:Address_Line3,BRW1.Q.job_ali:Address_Line3)
  BRW1.AddField(job_ali:Telephone_Number,BRW1.Q.job_ali:Telephone_Number)
  BRW1.AddField(job_ali:Address_Line2,BRW1.Q.job_ali:Address_Line2)
  BRW1.AddField(job_ali:Address_Line1,BRW1.Q.job_ali:Address_Line1)
  BRW1.AddField(job_ali:Courier_Cost,BRW1.Q.job_ali:Courier_Cost)
  BRW1.AddField(job_ali:Advance_Payment,BRW1.Q.job_ali:Advance_Payment)
  BRW1.AddField(job_ali:Labour_Cost,BRW1.Q.job_ali:Labour_Cost)
  BRW1.AddField(job_ali:Parts_Cost,BRW1.Q.job_ali:Parts_Cost)
  BRW1.AddField(job_ali:Sub_Total,BRW1.Q.job_ali:Sub_Total)
  BRW1.AddField(job_ali:Courier_Cost_Estimate,BRW1.Q.job_ali:Courier_Cost_Estimate)
  BRW1.AddField(job_ali:Labour_Cost_Estimate,BRW1.Q.job_ali:Labour_Cost_Estimate)
  BRW1.AddField(job_ali:Parts_Cost_Estimate,BRW1.Q.job_ali:Parts_Cost_Estimate)
  BRW1.AddField(job_ali:Sub_Total_Estimate,BRW1.Q.job_ali:Sub_Total_Estimate)
  BRW1.AddField(job_ali:Courier_Cost_Warranty,BRW1.Q.job_ali:Courier_Cost_Warranty)
  BRW1.AddField(job_ali:Labour_Cost_Warranty,BRW1.Q.job_ali:Labour_Cost_Warranty)
  BRW1.AddField(job_ali:Parts_Cost_Warranty,BRW1.Q.job_ali:Parts_Cost_Warranty)
  BRW1.AddField(job_ali:Sub_Total_Warranty,BRW1.Q.job_ali:Sub_Total_Warranty)
  BRW1.AddField(total_temp,BRW1.Q.total_temp)
  BRW1.AddField(balance_temp,BRW1.Q.balance_temp)
  BRW1.AddField(vat_total_warranty_temp,BRW1.Q.vat_total_warranty_temp)
  BRW1.AddField(total_warranty_temp,BRW1.Q.total_warranty_temp)
  BRW1.AddField(vat_total_estimate_temp,BRW1.Q.vat_total_estimate_temp)
  BRW1.AddField(total_estimate_temp,BRW1.Q.total_estimate_temp)
  BRW1.AddField(vat_total_temp,BRW1.Q.vat_total_temp)
  BRW1.AddField(job_ali:Current_Status,BRW1.Q.job_ali:Current_Status)
  BRW1.AddField(addtmp:RecordNumber,BRW1.Q.addtmp:RecordNumber)
  BRW3.Q &= Queue:Browse:1
  BRW3.AddSortOrder(,par:Part_Number_Key)
  BRW3.AddRange(par:Ref_Number,job_ali:Ref_Number)
  BRW3.AddLocator(BRW3::Sort0:Locator)
  BRW3::Sort0:Locator.Init(,par:Part_Number,1,BRW3)
  BRW3.AddField(par:Quantity,BRW3.Q.par:Quantity)
  BRW3.AddField(par:Description,BRW3.Q.par:Description)
  BRW3.AddField(par:Record_Number,BRW3.Q.par:Record_Number)
  BRW3.AddField(par:Ref_Number,BRW3.Q.par:Ref_Number)
  BRW3.AddField(par:Part_Number,BRW3.Q.par:Part_Number)
  BRW5.Q &= Queue:Browse:2
  BRW5.AddSortOrder(,wpr:Part_Number_Key)
  BRW5.AddRange(wpr:Ref_Number,job_ali:Ref_Number)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,wpr:Part_Number,1,BRW5)
  BRW5.AddField(wpr:Quantity,BRW5.Q.wpr:Quantity)
  BRW5.AddField(wpr:Part_Number,BRW5.Q.wpr:Part_Number)
  BRW5.AddField(wpr:Record_Number,BRW5.Q.wpr:Record_Number)
  BRW5.AddField(wpr:Ref_Number,BRW5.Q.wpr:Ref_Number)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  BRW3.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW3.AskProcedure = 0
      CLEAR(BRW3.AskProcedure, 1)
    END
  END
  BRW5.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW5.AskProcedure = 0
      CLEAR(BRW5.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:q_JobNumber)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:q_JobNumber)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:ADDSEARCH.Close
    Relate:EXCHANGE.Close
    Relate:INVOICE.Close
    Relate:JOBNOTES_ALIAS.Close
    Relate:VATCODE.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?audit_Trail
      glo:select12 = JOB_ALI:Ref_Number
    OF ?Print_Job_Card:2
      if records(glo:q_JobNumber) = 0 then
          miss# = missive('Please tag the jobs you want to show on the report','ServiceBase 3g','mwarn.jpg','OK')
          cycle
      END
      
      !message('Got records = '&Records(glo:Q_JobNumber)&' sending '&clip(brw1.q.job_ali:ESN))
      !loop this# = 1 to records(glo:Q_jobNumber)
      !    get(glo:Q_jobNumber,this#)
      !    message('Sending jobnumber '&clip(GLO:Job_Number_Pointer))
      !END
      
      glo:Select2 = brw1.q.job_ali:ESN
      
      
      !CSV version not wanted after all
      !If missive('Do you want to print or export this report?','ServiceBase 3g','mquest.jpg','EXPORT|PRINT') = 2 then
          Bouncer_History
      !ELSE
      !    Export_Bouncer_Hist
      !END
      
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020665'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020665'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020665'&'0')
      ***
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Button:SearchUsingExchangeIMEI
      ThisWindow.Update
      ! Inserting (DBH 22/11/2006) # 8405 - Refresh the browse with the IMEI Number
      Save_ADDSEARCH_ID = Access:ADDSEARCH.SaveFile()
      Access:ADDSEARCH.Clearkey(addtmp:RecordNumberKey)
      addtmp:RecordNumber = 1
      Set(addtmp:RecordNumberKey,addtmp:RecordNumberKey)
      Loop ! Begin Loop
          If Access:ADDSEARCH.Next()
              Break
          End ! If Access:ADDSEARCH.Next()
          Relate:ADDSEARCH.Delete(0)
      End ! Loop
      Access:ADDSEARCH.RestoreFile(Save_ADDSEARCH_ID)
      
      BuildIMEISearch(tmp:ExchangeIMEI)
      Brw1.ResetSort(1)
      tmp:GlobalIMEINumber = tmp:ExchangeIMEI
      Do DisplayIMEINumber
      Do RefreshDetails
      ! End (DBH 22/11/2006) #8405
    OF ?Button:ViewCosts
      ThisWindow.Update
      glo:Preview = 'V'
      Access:JOBS.ClearKey(job:Ref_Number_Key)
      job:Ref_Number = brw1.q.job_ali:Ref_Number
      If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
          !Found
          ViewCosts
      Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
          !Error
      End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
      glo:Preview = ''
      Brw1.ResetSort(1)
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?audit_Trail
      ThisWindow.Update
      Browse_Audit
      ThisWindow.Reset
      glo:select12 = ''
    OF ?Button6
      ThisWindow.Update
      GlobalRequest = ChangeRecord
      ShowText(brw1.q.job_ali:ref_number,'INVOICE')
      ThisWindow.Reset
    OF ?Button10
      ThisWindow.Update
      if (SecurityCheck('JOBS - CHANGE'))  ! #11682 Check access. (Bryan: 07/09/2010)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You do not have access to this option.','ServiceBase',|
                         'mstop.jpg','/&OK')
          Of 1 ! &OK Button
          End!Case Message
          Cycle
      End
      Access:JOBS.ClearKey(job:Ref_Number_Key)
      job:Ref_Number = brw1.q.job_ali:Ref_Number
      If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
          !Found
          glo:Select20 = 'NOSBONLINE'  ! #11723 Nasty fudge to get around Vodacom's mistake. (Bryan: 07/10/2010)
          GlobalRequest# = GlobalRequest
          GlobalRequest = ChangeRecord
          Update_Jobs_Rapid
          GlobalRequest = GlobalRequest#
          Brw1.ResetSort(1)
          glo:Select20 = ''
      Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
          !Error
      End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
    OF ?Button6:2
      ThisWindow.Update
      ShowFaultDescription(brw1.q.job_ali:ref_number)
      ThisWindow.Reset
    OF ?Button12
      ThisWindow.Update
      IMEIHistoryKey
      ThisWindow.Reset
    OF ?Print_Job_Card
      ThisWindow.Update
      thiswindow.reset(1)
      glo:select1  = job_ali:ref_number
      Job_Card
      glo:select1  = 'ESN'
    OF ?Button7
      ThisWindow.Update
      GlobalRequest = ChangeRecord
      ShowEngineersNotes(brw1.q.job_ali:ref_number)
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List
    Do RefreshDetails
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      BRW1.ResetSort(1)
      BRW3.ResetSort(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:q_JobNumber.Job_Number_Pointer = addtmp:JobNumber
     GET(glo:q_JobNumber,glo:q_JobNumber.Job_Number_Pointer)
    IF ERRORCODE()
      Tagged = ''
    ELSE
      Tagged = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  Access:JOBS_ALIAS.ClearKey(job_ali:Ref_Number_Key)
  job_ali:Ref_Number = addtmp:JobNumber
  If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign
      !Found
  Else ! If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign
      !Error
  End ! If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign
  
  If job_ali:title = ''
      If job_ali:initial = ''
          customer_name_temp = Clip(job_ali:surname)
      Else
          customer_name_temp = Clip(job_ali:initial) & ' ' & Clip(job_ali:surname)
      End
  Else
      If job_ali:initial = ''
          customer_name_temp = Clip(job_ali:title) & ' ' & Clip(job_ali:surname)
      Else
          customer_name_temp = Clip(job_ali:title) & ' ' & Clip(job_ali:initial) & ' ' & Clip(job_ali:surname)
      End
  End
  
  Access:JOBSE.ClearKey(jobe:RefNumberKey)
  jobe:RefNumber = job_ali:Ref_Number
  If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
      !Found
  Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
  
  Access:WEBJOB.ClearKey(wob:RefNumberKey)
  wob:RefNumber = job_ali:Ref_Number
  If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      !Found
  Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
  
  Access:TRADEACC.ClearKey(tra:Account_Number_Key)
  tra:Account_Number = wob:HeadAccountNumber
  If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
      !Found
  Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
      !Error
  End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
  
  tmp:JobNumber = job_ali:Ref_Number & '-' & Clip(tra:BranchIdentification) & wob:JobNumber
  
  ! Inserting (DBH 31/01/2008) # 9716 - Color the browse depending on the repair type
  If job_ali:Chargeable_Job = 'YES'
      Access:REPTYDEF.Clearkey(rtd:ChaManRepairTypeKey)
      rtd:Manufacturer = job_ali:Manufacturer
      rtd:Chargeable = 'YES'
      rtd:Repair_Type = job_ali:Repair_Type
      If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
  
      End ! If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
  Else ! If job_ali:Chargeable_Job = 'YES'
      Access:REPTYDEF.Clearkey(rtd:WarManRepairTypeKey)
      rtd:Manufacturer = job_Ali:Manufacturer
      rtd:Warranty = 'YES'
      rtd:Repair_Type = job_ali:Repair_Type_Warranty
      If Access:REPTYDEF.TryFetch(rtd:WarManRepairTYpeKey) = Level:Benign
  
      End ! If Access:REPTYDEF.TryFetch(rtd:WarManRepairTYpeKey) = Level:Benign
  End ! If job_ali:Chargeable_Job = 'YES'
  Case rtd:BER
  Of 0 Orof 2 Orof 3
      tmp:ColourFlag = 0 ! Black
  Of 1 Orof 4
      tmp:ColourFlag = 1 ! Blue
  Of 5 Orof 6 Orof 8
      tmp:ColourFlag = 2 ! Purple
  Of 7
      tmp:ColourFlag = 3 ! Pink
  Else
      tmp:ColourFlag = 4 ! Gray
  End ! Case rtd:BER
  Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
  sub:Account_Number = job_ali:Account_Number
  If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
      If sub:RefurbishmentAccount = 1
          tmp:ColourFlag = 5
      End ! If sub:RefurbishmentAccount = 1
  End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
  ! End (DBH 31/01/2008) #9716
  
  
  Trade_Customer_Temp = CLIP(job_ali:Account_Number) & ' - ' & CLIP(job_ali:Trade_Account_Name)
  Days_Since_Booking_Temp = TODAY() - job_ali:date_booked
  IF (job_ali:MSN <> '')
    msn_temp = job_ali:MSN
  ELSE
    msn_temp = 'N/A'
  END
  IF (job_ali:Chargeable_Job = 'YES')
    tmp:RepairTypes = job_ali:Repair_Type
  ELSE
    tmp:RepairTypes = job_ali:Repair_Type_Warranty
  END
  IF (job_ali:Exchange_Unit_Number > 0)
    tmp:Exchanged = 'Yes'
  ELSE
    tmp:Exchanged = 'No'
  END
  PARENT.SetQueueRecord
  SELF.Q.Tagged_NormalFG = -1
  SELF.Q.Tagged_NormalBG = -1
  SELF.Q.Tagged_SelectedFG = -1
  SELF.Q.Tagged_SelectedBG = -1
  IF (tagged = '*')
    SELF.Q.Tagged_Icon = 2
  ELSE
    SELF.Q.Tagged_Icon = 1
  END
  SELF.Q.tmp:JobNumber_NormalFG = -1
  SELF.Q.tmp:JobNumber_NormalBG = -1
  SELF.Q.tmp:JobNumber_SelectedFG = -1
  SELF.Q.tmp:JobNumber_SelectedBG = -1
  SELF.Q.addtmp:JobNumber_NormalFG = -1
  SELF.Q.addtmp:JobNumber_NormalBG = -1
  SELF.Q.addtmp:JobNumber_SelectedFG = -1
  SELF.Q.addtmp:JobNumber_SelectedBG = -1
  SELF.Q.job_ali:Ref_Number_NormalFG = -1
  SELF.Q.job_ali:Ref_Number_NormalBG = -1
  SELF.Q.job_ali:Ref_Number_SelectedFG = -1
  SELF.Q.job_ali:Ref_Number_SelectedBG = -1
  SELF.Q.job_ali:Account_Number_NormalFG = -1
  SELF.Q.job_ali:Account_Number_NormalBG = -1
  SELF.Q.job_ali:Account_Number_SelectedFG = -1
  SELF.Q.job_ali:Account_Number_SelectedBG = -1
  SELF.Q.job_ali:Surname_NormalFG = -1
  SELF.Q.job_ali:Surname_NormalBG = -1
  SELF.Q.job_ali:Surname_SelectedFG = -1
  SELF.Q.job_ali:Surname_SelectedBG = -1
  SELF.Q.job_ali:Mobile_Number_NormalFG = -1
  SELF.Q.job_ali:Mobile_Number_NormalBG = -1
  SELF.Q.job_ali:Mobile_Number_SelectedFG = -1
  SELF.Q.job_ali:Mobile_Number_SelectedBG = -1
  SELF.Q.job_ali:ESN_NormalFG = -1
  SELF.Q.job_ali:ESN_NormalBG = -1
  SELF.Q.job_ali:ESN_SelectedFG = -1
  SELF.Q.job_ali:ESN_SelectedBG = -1
  SELF.Q.job_ali:MSN_NormalFG = -1
  SELF.Q.job_ali:MSN_NormalBG = -1
  SELF.Q.job_ali:MSN_SelectedFG = -1
  SELF.Q.job_ali:MSN_SelectedBG = -1
  SELF.Q.tmp:Exchanged_NormalFG = -1
  SELF.Q.tmp:Exchanged_NormalBG = -1
  SELF.Q.tmp:Exchanged_SelectedFG = -1
  SELF.Q.tmp:Exchanged_SelectedBG = -1
  SELF.Q.tmp:RepairTypes_NormalFG = -1
  SELF.Q.tmp:RepairTypes_NormalBG = -1
  SELF.Q.tmp:RepairTypes_SelectedFG = -1
  SELF.Q.tmp:RepairTypes_SelectedBG = -1
  SELF.Q.job_ali:Charge_Type_NormalFG = -1
  SELF.Q.job_ali:Charge_Type_NormalBG = -1
  SELF.Q.job_ali:Charge_Type_SelectedFG = -1
  SELF.Q.job_ali:Charge_Type_SelectedBG = -1
  SELF.Q.job_ali:Warranty_Charge_Type_NormalFG = -1
  SELF.Q.job_ali:Warranty_Charge_Type_NormalBG = -1
  SELF.Q.job_ali:Warranty_Charge_Type_SelectedFG = -1
  SELF.Q.job_ali:Warranty_Charge_Type_SelectedBG = -1
  SELF.Q.job_ali:Date_Completed_NormalFG = -1
  SELF.Q.job_ali:Date_Completed_NormalBG = -1
  SELF.Q.job_ali:Date_Completed_SelectedFG = -1
  SELF.Q.job_ali:Date_Completed_SelectedBG = -1
  SELF.Q.Days_Since_Booking_Temp_NormalFG = -1
  SELF.Q.Days_Since_Booking_Temp_NormalBG = -1
  SELF.Q.Days_Since_Booking_Temp_SelectedFG = -1
  SELF.Q.Days_Since_Booking_Temp_SelectedBG = -1
  SELF.Q.job_ali:Exchange_Unit_Number_NormalFG = -1
  SELF.Q.job_ali:Exchange_Unit_Number_NormalBG = -1
  SELF.Q.job_ali:Exchange_Unit_Number_SelectedFG = -1
  SELF.Q.job_ali:Exchange_Unit_Number_SelectedBG = -1
  SELF.Q.job_ali:Warranty_Job_NormalFG = -1
  SELF.Q.job_ali:Warranty_Job_NormalBG = -1
  SELF.Q.job_ali:Warranty_Job_SelectedFG = -1
  SELF.Q.job_ali:Warranty_Job_SelectedBG = -1
  SELF.Q.job_ali:Chargeable_Job_NormalFG = -1
  SELF.Q.job_ali:Chargeable_Job_NormalBG = -1
  SELF.Q.job_ali:Chargeable_Job_SelectedFG = -1
  SELF.Q.job_ali:Chargeable_Job_SelectedBG = -1
  SELF.Q.tmp:ColourFlag_NormalFG = -1
  SELF.Q.tmp:ColourFlag_NormalBG = -1
  SELF.Q.tmp:ColourFlag_SelectedFG = -1
  SELF.Q.tmp:ColourFlag_SelectedBG = -1
  SELF.Q.tmp:Exchanged = tmp:Exchanged                !Assign formula result to display queue
  SELF.Q.tmp:RepairTypes = tmp:RepairTypes            !Assign formula result to display queue
  SELF.Q.Days_Since_Booking_Temp = Days_Since_Booking_Temp !Assign formula result to display queue
  SELF.Q.msn_temp = msn_temp                          !Assign formula result to display queue
   
   
   IF (job_ali:Date_Completed = '')
     SELF.Q.Tagged_NormalFG = 32768
     SELF.Q.Tagged_NormalBG = 16777215
     SELF.Q.Tagged_SelectedFG = 16777215
     SELF.Q.Tagged_SelectedBG = 32768
   ELSIF(tmp:ColourFlag = 1)
     SELF.Q.Tagged_NormalFG = 8388608
     SELF.Q.Tagged_NormalBG = 16777215
     SELF.Q.Tagged_SelectedFG = 16777215
     SELF.Q.Tagged_SelectedBG = 8388608
   ELSIF(tmp:ColourFlag = 2)
     SELF.Q.Tagged_NormalFG = 8388736
     SELF.Q.Tagged_NormalBG = 16777215
     SELF.Q.Tagged_SelectedFG = 16777215
     SELF.Q.Tagged_SelectedBG = 8388736
   ELSIF(tmp:ColourFlag = 3)
     SELF.Q.Tagged_NormalFG = 16711935
     SELF.Q.Tagged_NormalBG = 16777215
     SELF.Q.Tagged_SelectedFG = 16777215
     SELF.Q.Tagged_SelectedBG = 16711935
   ELSIF(tmp:ColourFlag = 4)
     SELF.Q.Tagged_NormalFG = 8421504
     SELF.Q.Tagged_NormalBG = 16777215
     SELF.Q.Tagged_SelectedFG = 16777215
     SELF.Q.Tagged_SelectedBG = 8421504
   ELSIF(tmp:ColourFlag = 5)
     SELF.Q.Tagged_NormalFG = 255
     SELF.Q.Tagged_NormalBG = 16777215
     SELF.Q.Tagged_SelectedFG = 16777215
     SELF.Q.Tagged_SelectedBG = 255
   ELSE
     SELF.Q.Tagged_NormalFG = 0
     SELF.Q.Tagged_NormalBG = 16777215
     SELF.Q.Tagged_SelectedFG = 16777215
     SELF.Q.Tagged_SelectedBG = 0
   END
   IF (job_ali:Date_Completed = '')
     SELF.Q.tmp:JobNumber_NormalFG = 32768
     SELF.Q.tmp:JobNumber_NormalBG = 16777215
     SELF.Q.tmp:JobNumber_SelectedFG = 16777215
     SELF.Q.tmp:JobNumber_SelectedBG = 32768
   ELSIF(tmp:ColourFlag = 1)
     SELF.Q.tmp:JobNumber_NormalFG = 8388608
     SELF.Q.tmp:JobNumber_NormalBG = 16777215
     SELF.Q.tmp:JobNumber_SelectedFG = 16777215
     SELF.Q.tmp:JobNumber_SelectedBG = 8388608
   ELSIF(tmp:ColourFlag = 2)
     SELF.Q.tmp:JobNumber_NormalFG = 8388736
     SELF.Q.tmp:JobNumber_NormalBG = 16777215
     SELF.Q.tmp:JobNumber_SelectedFG = 16777215
     SELF.Q.tmp:JobNumber_SelectedBG = 8388736
   ELSIF(tmp:ColourFlag = 3)
     SELF.Q.tmp:JobNumber_NormalFG = 16711935
     SELF.Q.tmp:JobNumber_NormalBG = 16777215
     SELF.Q.tmp:JobNumber_SelectedFG = 16777215
     SELF.Q.tmp:JobNumber_SelectedBG = 16711935
   ELSIF(tmp:ColourFlag = 4)
     SELF.Q.tmp:JobNumber_NormalFG = 8421504
     SELF.Q.tmp:JobNumber_NormalBG = 16777215
     SELF.Q.tmp:JobNumber_SelectedFG = 16777215
     SELF.Q.tmp:JobNumber_SelectedBG = 8421504
   ELSIF(tmp:ColourFlag = 5)
     SELF.Q.tmp:JobNumber_NormalFG = 255
     SELF.Q.tmp:JobNumber_NormalBG = 16777215
     SELF.Q.tmp:JobNumber_SelectedFG = 16777215
     SELF.Q.tmp:JobNumber_SelectedBG = 255
   ELSE
     SELF.Q.tmp:JobNumber_NormalFG = 0
     SELF.Q.tmp:JobNumber_NormalBG = 16777215
     SELF.Q.tmp:JobNumber_SelectedFG = 16777215
     SELF.Q.tmp:JobNumber_SelectedBG = 0
   END
   IF (job_ali:Date_Completed = '')
     SELF.Q.addtmp:JobNumber_NormalFG = 32768
     SELF.Q.addtmp:JobNumber_NormalBG = 16777215
     SELF.Q.addtmp:JobNumber_SelectedFG = 16777215
     SELF.Q.addtmp:JobNumber_SelectedBG = 32768
   ELSIF(tmp:ColourFlag = 1)
     SELF.Q.addtmp:JobNumber_NormalFG = 8388608
     SELF.Q.addtmp:JobNumber_NormalBG = 16777215
     SELF.Q.addtmp:JobNumber_SelectedFG = 16777215
     SELF.Q.addtmp:JobNumber_SelectedBG = 8388608
   ELSIF(tmp:ColourFlag = 2)
     SELF.Q.addtmp:JobNumber_NormalFG = 8388736
     SELF.Q.addtmp:JobNumber_NormalBG = 16777215
     SELF.Q.addtmp:JobNumber_SelectedFG = 16777215
     SELF.Q.addtmp:JobNumber_SelectedBG = 8388736
   ELSIF(tmp:ColourFlag = 3)
     SELF.Q.addtmp:JobNumber_NormalFG = 16711935
     SELF.Q.addtmp:JobNumber_NormalBG = 16777215
     SELF.Q.addtmp:JobNumber_SelectedFG = 16777215
     SELF.Q.addtmp:JobNumber_SelectedBG = 16711935
   ELSIF(tmp:ColourFlag = 4)
     SELF.Q.addtmp:JobNumber_NormalFG = 8421504
     SELF.Q.addtmp:JobNumber_NormalBG = 16777215
     SELF.Q.addtmp:JobNumber_SelectedFG = 16777215
     SELF.Q.addtmp:JobNumber_SelectedBG = 8421504
   ELSIF(tmp:ColourFlag = 5)
     SELF.Q.addtmp:JobNumber_NormalFG = 255
     SELF.Q.addtmp:JobNumber_NormalBG = 16777215
     SELF.Q.addtmp:JobNumber_SelectedFG = 16777215
     SELF.Q.addtmp:JobNumber_SelectedBG = 255
   ELSE
     SELF.Q.addtmp:JobNumber_NormalFG = 0
     SELF.Q.addtmp:JobNumber_NormalBG = 16777215
     SELF.Q.addtmp:JobNumber_SelectedFG = 16777215
     SELF.Q.addtmp:JobNumber_SelectedBG = 0
   END
   IF (job_ali:Date_Completed = '')
     SELF.Q.job_ali:Ref_Number_NormalFG = 32768
     SELF.Q.job_ali:Ref_Number_NormalBG = 16777215
     SELF.Q.job_ali:Ref_Number_SelectedFG = 16777215
     SELF.Q.job_ali:Ref_Number_SelectedBG = 32768
   ELSIF(tmp:ColourFlag = 1)
     SELF.Q.job_ali:Ref_Number_NormalFG = 8388608
     SELF.Q.job_ali:Ref_Number_NormalBG = 16777215
     SELF.Q.job_ali:Ref_Number_SelectedFG = 16777215
     SELF.Q.job_ali:Ref_Number_SelectedBG = 8388608
   ELSIF(tmp:ColourFlag = 2)
     SELF.Q.job_ali:Ref_Number_NormalFG = 8388736
     SELF.Q.job_ali:Ref_Number_NormalBG = 16777215
     SELF.Q.job_ali:Ref_Number_SelectedFG = 16777215
     SELF.Q.job_ali:Ref_Number_SelectedBG = 8388736
   ELSIF(tmp:ColourFlag = 3)
     SELF.Q.job_ali:Ref_Number_NormalFG = 16711935
     SELF.Q.job_ali:Ref_Number_NormalBG = 16777215
     SELF.Q.job_ali:Ref_Number_SelectedFG = 16777215
     SELF.Q.job_ali:Ref_Number_SelectedBG = 16711935
   ELSIF(tmp:ColourFlag = 4)
     SELF.Q.job_ali:Ref_Number_NormalFG = 8421504
     SELF.Q.job_ali:Ref_Number_NormalBG = 16777215
     SELF.Q.job_ali:Ref_Number_SelectedFG = 16777215
     SELF.Q.job_ali:Ref_Number_SelectedBG = 8421504
   ELSIF(tmp:ColourFlag = 5)
     SELF.Q.job_ali:Ref_Number_NormalFG = 255
     SELF.Q.job_ali:Ref_Number_NormalBG = 16777215
     SELF.Q.job_ali:Ref_Number_SelectedFG = 16777215
     SELF.Q.job_ali:Ref_Number_SelectedBG = 255
   ELSE
     SELF.Q.job_ali:Ref_Number_NormalFG = 0
     SELF.Q.job_ali:Ref_Number_NormalBG = 16777215
     SELF.Q.job_ali:Ref_Number_SelectedFG = 16777215
     SELF.Q.job_ali:Ref_Number_SelectedBG = 0
   END
   IF (job_ali:Date_Completed = '')
     SELF.Q.job_ali:Account_Number_NormalFG = 32768
     SELF.Q.job_ali:Account_Number_NormalBG = 16777215
     SELF.Q.job_ali:Account_Number_SelectedFG = 16777215
     SELF.Q.job_ali:Account_Number_SelectedBG = 32768
   ELSIF(tmp:ColourFlag = 1)
     SELF.Q.job_ali:Account_Number_NormalFG = 8388608
     SELF.Q.job_ali:Account_Number_NormalBG = 16777215
     SELF.Q.job_ali:Account_Number_SelectedFG = 16777215
     SELF.Q.job_ali:Account_Number_SelectedBG = 8388608
   ELSIF(tmp:ColourFlag = 2)
     SELF.Q.job_ali:Account_Number_NormalFG = 8388736
     SELF.Q.job_ali:Account_Number_NormalBG = 16777215
     SELF.Q.job_ali:Account_Number_SelectedFG = 16777215
     SELF.Q.job_ali:Account_Number_SelectedBG = 8388736
   ELSIF(tmp:ColourFlag = 3)
     SELF.Q.job_ali:Account_Number_NormalFG = 16711935
     SELF.Q.job_ali:Account_Number_NormalBG = 16777215
     SELF.Q.job_ali:Account_Number_SelectedFG = 16777215
     SELF.Q.job_ali:Account_Number_SelectedBG = 16711935
   ELSIF(tmp:ColourFlag = 4)
     SELF.Q.job_ali:Account_Number_NormalFG = 8421504
     SELF.Q.job_ali:Account_Number_NormalBG = 16777215
     SELF.Q.job_ali:Account_Number_SelectedFG = 16777215
     SELF.Q.job_ali:Account_Number_SelectedBG = 8421504
   ELSIF(tmp:ColourFlag = 5)
     SELF.Q.job_ali:Account_Number_NormalFG = 255
     SELF.Q.job_ali:Account_Number_NormalBG = 16777215
     SELF.Q.job_ali:Account_Number_SelectedFG = 16777215
     SELF.Q.job_ali:Account_Number_SelectedBG = 255
   ELSE
     SELF.Q.job_ali:Account_Number_NormalFG = 0
     SELF.Q.job_ali:Account_Number_NormalBG = 16777215
     SELF.Q.job_ali:Account_Number_SelectedFG = 16777215
     SELF.Q.job_ali:Account_Number_SelectedBG = 0
   END
   IF (job_ali:Date_Completed = '')
     SELF.Q.job_ali:Surname_NormalFG = 32768
     SELF.Q.job_ali:Surname_NormalBG = 16777215
     SELF.Q.job_ali:Surname_SelectedFG = 16777215
     SELF.Q.job_ali:Surname_SelectedBG = 32768
   ELSIF(tmp:ColourFlag = 1)
     SELF.Q.job_ali:Surname_NormalFG = 8388608
     SELF.Q.job_ali:Surname_NormalBG = 16777215
     SELF.Q.job_ali:Surname_SelectedFG = 16777215
     SELF.Q.job_ali:Surname_SelectedBG = 8388608
   ELSIF(tmp:ColourFlag = 2)
     SELF.Q.job_ali:Surname_NormalFG = 8388736
     SELF.Q.job_ali:Surname_NormalBG = 16777215
     SELF.Q.job_ali:Surname_SelectedFG = 16777215
     SELF.Q.job_ali:Surname_SelectedBG = 8388736
   ELSIF(tmp:ColourFlag = 3)
     SELF.Q.job_ali:Surname_NormalFG = 16711935
     SELF.Q.job_ali:Surname_NormalBG = 16777215
     SELF.Q.job_ali:Surname_SelectedFG = 16777215
     SELF.Q.job_ali:Surname_SelectedBG = 16711935
   ELSIF(tmp:ColourFlag = 4)
     SELF.Q.job_ali:Surname_NormalFG = 8421504
     SELF.Q.job_ali:Surname_NormalBG = 16777215
     SELF.Q.job_ali:Surname_SelectedFG = 16777215
     SELF.Q.job_ali:Surname_SelectedBG = 8421504
   ELSIF(tmp:ColourFlag = 5)
     SELF.Q.job_ali:Surname_NormalFG = 255
     SELF.Q.job_ali:Surname_NormalBG = 16777215
     SELF.Q.job_ali:Surname_SelectedFG = 16777215
     SELF.Q.job_ali:Surname_SelectedBG = 255
   ELSE
     SELF.Q.job_ali:Surname_NormalFG = 0
     SELF.Q.job_ali:Surname_NormalBG = 16777215
     SELF.Q.job_ali:Surname_SelectedFG = 16777215
     SELF.Q.job_ali:Surname_SelectedBG = 0
   END
   IF (job_ali:Date_Completed = '')
     SELF.Q.job_ali:Mobile_Number_NormalFG = 32768
     SELF.Q.job_ali:Mobile_Number_NormalBG = 16777215
     SELF.Q.job_ali:Mobile_Number_SelectedFG = 16777215
     SELF.Q.job_ali:Mobile_Number_SelectedBG = 32768
   ELSIF(tmp:ColourFlag = 1)
     SELF.Q.job_ali:Mobile_Number_NormalFG = 8388608
     SELF.Q.job_ali:Mobile_Number_NormalBG = 16777215
     SELF.Q.job_ali:Mobile_Number_SelectedFG = 16777215
     SELF.Q.job_ali:Mobile_Number_SelectedBG = 8388608
   ELSIF(tmp:ColourFlag = 2)
     SELF.Q.job_ali:Mobile_Number_NormalFG = 8388736
     SELF.Q.job_ali:Mobile_Number_NormalBG = 16777215
     SELF.Q.job_ali:Mobile_Number_SelectedFG = 16777215
     SELF.Q.job_ali:Mobile_Number_SelectedBG = 8388736
   ELSIF(tmp:ColourFlag = 3)
     SELF.Q.job_ali:Mobile_Number_NormalFG = 16711935
     SELF.Q.job_ali:Mobile_Number_NormalBG = 16777215
     SELF.Q.job_ali:Mobile_Number_SelectedFG = 16777215
     SELF.Q.job_ali:Mobile_Number_SelectedBG = 16711935
   ELSIF(tmp:ColourFlag = 4)
     SELF.Q.job_ali:Mobile_Number_NormalFG = 8421504
     SELF.Q.job_ali:Mobile_Number_NormalBG = 16777215
     SELF.Q.job_ali:Mobile_Number_SelectedFG = 16777215
     SELF.Q.job_ali:Mobile_Number_SelectedBG = 8421504
   ELSIF(tmp:ColourFlag = 5)
     SELF.Q.job_ali:Mobile_Number_NormalFG = 255
     SELF.Q.job_ali:Mobile_Number_NormalBG = 16777215
     SELF.Q.job_ali:Mobile_Number_SelectedFG = 16777215
     SELF.Q.job_ali:Mobile_Number_SelectedBG = 255
   ELSE
     SELF.Q.job_ali:Mobile_Number_NormalFG = 0
     SELF.Q.job_ali:Mobile_Number_NormalBG = 16777215
     SELF.Q.job_ali:Mobile_Number_SelectedFG = 16777215
     SELF.Q.job_ali:Mobile_Number_SelectedBG = 0
   END
   IF (job_ali:Date_Completed = '')
     SELF.Q.job_ali:ESN_NormalFG = 32768
     SELF.Q.job_ali:ESN_NormalBG = 16777215
     SELF.Q.job_ali:ESN_SelectedFG = 16777215
     SELF.Q.job_ali:ESN_SelectedBG = 32768
   ELSIF(tmp:ColourFlag = 1)
     SELF.Q.job_ali:ESN_NormalFG = 8388608
     SELF.Q.job_ali:ESN_NormalBG = 16777215
     SELF.Q.job_ali:ESN_SelectedFG = 16777215
     SELF.Q.job_ali:ESN_SelectedBG = 8388608
   ELSIF(tmp:ColourFlag = 2)
     SELF.Q.job_ali:ESN_NormalFG = 8388736
     SELF.Q.job_ali:ESN_NormalBG = 16777215
     SELF.Q.job_ali:ESN_SelectedFG = 16777215
     SELF.Q.job_ali:ESN_SelectedBG = 8388736
   ELSIF(tmp:ColourFlag = 3)
     SELF.Q.job_ali:ESN_NormalFG = 16711935
     SELF.Q.job_ali:ESN_NormalBG = 16777215
     SELF.Q.job_ali:ESN_SelectedFG = 16777215
     SELF.Q.job_ali:ESN_SelectedBG = 16711935
   ELSIF(tmp:ColourFlag = 4)
     SELF.Q.job_ali:ESN_NormalFG = 8421504
     SELF.Q.job_ali:ESN_NormalBG = 16777215
     SELF.Q.job_ali:ESN_SelectedFG = 16777215
     SELF.Q.job_ali:ESN_SelectedBG = 8421504
   ELSIF(tmp:ColourFlag = 5)
     SELF.Q.job_ali:ESN_NormalFG = 255
     SELF.Q.job_ali:ESN_NormalBG = 16777215
     SELF.Q.job_ali:ESN_SelectedFG = 16777215
     SELF.Q.job_ali:ESN_SelectedBG = 255
   ELSE
     SELF.Q.job_ali:ESN_NormalFG = 0
     SELF.Q.job_ali:ESN_NormalBG = 16777215
     SELF.Q.job_ali:ESN_SelectedFG = 16777215
     SELF.Q.job_ali:ESN_SelectedBG = 0
   END
   IF (job_ali:Date_Completed = '')
     SELF.Q.job_ali:MSN_NormalFG = 32768
     SELF.Q.job_ali:MSN_NormalBG = 16777215
     SELF.Q.job_ali:MSN_SelectedFG = 16777215
     SELF.Q.job_ali:MSN_SelectedBG = 32768
   ELSIF(tmp:ColourFlag = 1)
     SELF.Q.job_ali:MSN_NormalFG = 8388608
     SELF.Q.job_ali:MSN_NormalBG = 16777215
     SELF.Q.job_ali:MSN_SelectedFG = 16777215
     SELF.Q.job_ali:MSN_SelectedBG = 8388608
   ELSIF(tmp:ColourFlag = 2)
     SELF.Q.job_ali:MSN_NormalFG = 8388736
     SELF.Q.job_ali:MSN_NormalBG = 16777215
     SELF.Q.job_ali:MSN_SelectedFG = 16777215
     SELF.Q.job_ali:MSN_SelectedBG = 8388736
   ELSIF(tmp:ColourFlag = 3)
     SELF.Q.job_ali:MSN_NormalFG = 16711935
     SELF.Q.job_ali:MSN_NormalBG = 16777215
     SELF.Q.job_ali:MSN_SelectedFG = 16777215
     SELF.Q.job_ali:MSN_SelectedBG = 16711935
   ELSIF(tmp:ColourFlag = 4)
     SELF.Q.job_ali:MSN_NormalFG = 8421504
     SELF.Q.job_ali:MSN_NormalBG = 16777215
     SELF.Q.job_ali:MSN_SelectedFG = 16777215
     SELF.Q.job_ali:MSN_SelectedBG = 8421504
   ELSIF(tmp:ColourFlag = 5)
     SELF.Q.job_ali:MSN_NormalFG = 255
     SELF.Q.job_ali:MSN_NormalBG = 16777215
     SELF.Q.job_ali:MSN_SelectedFG = 16777215
     SELF.Q.job_ali:MSN_SelectedBG = 255
   ELSE
     SELF.Q.job_ali:MSN_NormalFG = 0
     SELF.Q.job_ali:MSN_NormalBG = 16777215
     SELF.Q.job_ali:MSN_SelectedFG = 16777215
     SELF.Q.job_ali:MSN_SelectedBG = 0
   END
   IF (job_ali:Date_Completed = '')
     SELF.Q.tmp:Exchanged_NormalFG = 32768
     SELF.Q.tmp:Exchanged_NormalBG = 16777215
     SELF.Q.tmp:Exchanged_SelectedFG = 16777215
     SELF.Q.tmp:Exchanged_SelectedBG = 32768
   ELSIF(tmp:ColourFlag = 1)
     SELF.Q.tmp:Exchanged_NormalFG = 8388608
     SELF.Q.tmp:Exchanged_NormalBG = 16777215
     SELF.Q.tmp:Exchanged_SelectedFG = 16777215
     SELF.Q.tmp:Exchanged_SelectedBG = 8388608
   ELSIF(tmp:ColourFlag = 2)
     SELF.Q.tmp:Exchanged_NormalFG = 8388736
     SELF.Q.tmp:Exchanged_NormalBG = 16777215
     SELF.Q.tmp:Exchanged_SelectedFG = 16777215
     SELF.Q.tmp:Exchanged_SelectedBG = 8388736
   ELSIF(tmp:ColourFlag = 3)
     SELF.Q.tmp:Exchanged_NormalFG = 16711935
     SELF.Q.tmp:Exchanged_NormalBG = 16777215
     SELF.Q.tmp:Exchanged_SelectedFG = 16777215
     SELF.Q.tmp:Exchanged_SelectedBG = 16711935
   ELSIF(tmp:ColourFlag = 4)
     SELF.Q.tmp:Exchanged_NormalFG = 8421504
     SELF.Q.tmp:Exchanged_NormalBG = 16777215
     SELF.Q.tmp:Exchanged_SelectedFG = 16777215
     SELF.Q.tmp:Exchanged_SelectedBG = 8421504
   ELSIF(tmp:ColourFlag = 5)
     SELF.Q.tmp:Exchanged_NormalFG = 255
     SELF.Q.tmp:Exchanged_NormalBG = 16777215
     SELF.Q.tmp:Exchanged_SelectedFG = 16777215
     SELF.Q.tmp:Exchanged_SelectedBG = 255
   ELSE
     SELF.Q.tmp:Exchanged_NormalFG = 0
     SELF.Q.tmp:Exchanged_NormalBG = 16777215
     SELF.Q.tmp:Exchanged_SelectedFG = 16777215
     SELF.Q.tmp:Exchanged_SelectedBG = 0
   END
   IF (job_ali:Date_Completed = '')
     SELF.Q.tmp:RepairTypes_NormalFG = 32768
     SELF.Q.tmp:RepairTypes_NormalBG = 16777215
     SELF.Q.tmp:RepairTypes_SelectedFG = 16777215
     SELF.Q.tmp:RepairTypes_SelectedBG = 32768
   ELSIF(tmp:ColourFlag = 1)
     SELF.Q.tmp:RepairTypes_NormalFG = 8388608
     SELF.Q.tmp:RepairTypes_NormalBG = 16777215
     SELF.Q.tmp:RepairTypes_SelectedFG = 16777215
     SELF.Q.tmp:RepairTypes_SelectedBG = 8388608
   ELSIF(tmp:ColourFlag = 2)
     SELF.Q.tmp:RepairTypes_NormalFG = 8388736
     SELF.Q.tmp:RepairTypes_NormalBG = 16777215
     SELF.Q.tmp:RepairTypes_SelectedFG = 16777215
     SELF.Q.tmp:RepairTypes_SelectedBG = 8388736
   ELSIF(tmp:ColourFlag = 3)
     SELF.Q.tmp:RepairTypes_NormalFG = 16711935
     SELF.Q.tmp:RepairTypes_NormalBG = 16777215
     SELF.Q.tmp:RepairTypes_SelectedFG = 16777215
     SELF.Q.tmp:RepairTypes_SelectedBG = 16711935
   ELSIF(tmp:ColourFlag = 4)
     SELF.Q.tmp:RepairTypes_NormalFG = 8421504
     SELF.Q.tmp:RepairTypes_NormalBG = 16777215
     SELF.Q.tmp:RepairTypes_SelectedFG = 16777215
     SELF.Q.tmp:RepairTypes_SelectedBG = 8421504
   ELSIF(tmp:ColourFlag = 5)
     SELF.Q.tmp:RepairTypes_NormalFG = 255
     SELF.Q.tmp:RepairTypes_NormalBG = 16777215
     SELF.Q.tmp:RepairTypes_SelectedFG = 16777215
     SELF.Q.tmp:RepairTypes_SelectedBG = 255
   ELSE
     SELF.Q.tmp:RepairTypes_NormalFG = 0
     SELF.Q.tmp:RepairTypes_NormalBG = 16777215
     SELF.Q.tmp:RepairTypes_SelectedFG = 16777215
     SELF.Q.tmp:RepairTypes_SelectedBG = 0
   END
   IF (job_ali:Date_Completed = '')
     SELF.Q.job_ali:Charge_Type_NormalFG = 32768
     SELF.Q.job_ali:Charge_Type_NormalBG = 16777215
     SELF.Q.job_ali:Charge_Type_SelectedFG = 16777215
     SELF.Q.job_ali:Charge_Type_SelectedBG = 32768
   ELSIF(tmp:ColourFlag = 1)
     SELF.Q.job_ali:Charge_Type_NormalFG = 8388608
     SELF.Q.job_ali:Charge_Type_NormalBG = 16777215
     SELF.Q.job_ali:Charge_Type_SelectedFG = 16777215
     SELF.Q.job_ali:Charge_Type_SelectedBG = 8388608
   ELSIF(tmp:ColourFlag = 2)
     SELF.Q.job_ali:Charge_Type_NormalFG = 8388736
     SELF.Q.job_ali:Charge_Type_NormalBG = 16777215
     SELF.Q.job_ali:Charge_Type_SelectedFG = 16777215
     SELF.Q.job_ali:Charge_Type_SelectedBG = 8388736
   ELSIF(tmp:ColourFlag = 3)
     SELF.Q.job_ali:Charge_Type_NormalFG = 16711935
     SELF.Q.job_ali:Charge_Type_NormalBG = 16777215
     SELF.Q.job_ali:Charge_Type_SelectedFG = 16777215
     SELF.Q.job_ali:Charge_Type_SelectedBG = 16711935
   ELSIF(tmp:ColourFlag = 4)
     SELF.Q.job_ali:Charge_Type_NormalFG = 8421504
     SELF.Q.job_ali:Charge_Type_NormalBG = 16777215
     SELF.Q.job_ali:Charge_Type_SelectedFG = 16777215
     SELF.Q.job_ali:Charge_Type_SelectedBG = 8421504
   ELSIF(tmp:ColourFlag = 5)
     SELF.Q.job_ali:Charge_Type_NormalFG = 255
     SELF.Q.job_ali:Charge_Type_NormalBG = 16777215
     SELF.Q.job_ali:Charge_Type_SelectedFG = 16777215
     SELF.Q.job_ali:Charge_Type_SelectedBG = 255
   ELSE
     SELF.Q.job_ali:Charge_Type_NormalFG = 0
     SELF.Q.job_ali:Charge_Type_NormalBG = 16777215
     SELF.Q.job_ali:Charge_Type_SelectedFG = 16777215
     SELF.Q.job_ali:Charge_Type_SelectedBG = 0
   END
   IF (job_ali:Date_Completed = '')
     SELF.Q.job_ali:Warranty_Charge_Type_NormalFG = 32768
     SELF.Q.job_ali:Warranty_Charge_Type_NormalBG = 16777215
     SELF.Q.job_ali:Warranty_Charge_Type_SelectedFG = 16777215
     SELF.Q.job_ali:Warranty_Charge_Type_SelectedBG = 32768
   ELSIF(tmp:ColourFlag = 1)
     SELF.Q.job_ali:Warranty_Charge_Type_NormalFG = 8388608
     SELF.Q.job_ali:Warranty_Charge_Type_NormalBG = 16777215
     SELF.Q.job_ali:Warranty_Charge_Type_SelectedFG = 16777215
     SELF.Q.job_ali:Warranty_Charge_Type_SelectedBG = 8388608
   ELSIF(tmp:ColourFlag = 2)
     SELF.Q.job_ali:Warranty_Charge_Type_NormalFG = 8388736
     SELF.Q.job_ali:Warranty_Charge_Type_NormalBG = 16777215
     SELF.Q.job_ali:Warranty_Charge_Type_SelectedFG = 16777215
     SELF.Q.job_ali:Warranty_Charge_Type_SelectedBG = 8388736
   ELSIF(tmp:ColourFlag = 3)
     SELF.Q.job_ali:Warranty_Charge_Type_NormalFG = 16711935
     SELF.Q.job_ali:Warranty_Charge_Type_NormalBG = 16777215
     SELF.Q.job_ali:Warranty_Charge_Type_SelectedFG = 16777215
     SELF.Q.job_ali:Warranty_Charge_Type_SelectedBG = 16711935
   ELSIF(tmp:ColourFlag = 4)
     SELF.Q.job_ali:Warranty_Charge_Type_NormalFG = 8421504
     SELF.Q.job_ali:Warranty_Charge_Type_NormalBG = 16777215
     SELF.Q.job_ali:Warranty_Charge_Type_SelectedFG = 16777215
     SELF.Q.job_ali:Warranty_Charge_Type_SelectedBG = 8421504
   ELSIF(tmp:ColourFlag = 5)
     SELF.Q.job_ali:Warranty_Charge_Type_NormalFG = 255
     SELF.Q.job_ali:Warranty_Charge_Type_NormalBG = 16777215
     SELF.Q.job_ali:Warranty_Charge_Type_SelectedFG = 16777215
     SELF.Q.job_ali:Warranty_Charge_Type_SelectedBG = 255
   ELSE
     SELF.Q.job_ali:Warranty_Charge_Type_NormalFG = 0
     SELF.Q.job_ali:Warranty_Charge_Type_NormalBG = 16777215
     SELF.Q.job_ali:Warranty_Charge_Type_SelectedFG = 16777215
     SELF.Q.job_ali:Warranty_Charge_Type_SelectedBG = 0
   END
   IF (job_ali:Date_Completed = '')
     SELF.Q.job_ali:Date_Completed_NormalFG = 32768
     SELF.Q.job_ali:Date_Completed_NormalBG = 16777215
     SELF.Q.job_ali:Date_Completed_SelectedFG = 16777215
     SELF.Q.job_ali:Date_Completed_SelectedBG = 32768
   ELSIF(tmp:ColourFlag = 1)
     SELF.Q.job_ali:Date_Completed_NormalFG = 8388608
     SELF.Q.job_ali:Date_Completed_NormalBG = 16777215
     SELF.Q.job_ali:Date_Completed_SelectedFG = 16777215
     SELF.Q.job_ali:Date_Completed_SelectedBG = 8388608
   ELSIF(tmp:ColourFlag = 2)
     SELF.Q.job_ali:Date_Completed_NormalFG = 8388736
     SELF.Q.job_ali:Date_Completed_NormalBG = 16777215
     SELF.Q.job_ali:Date_Completed_SelectedFG = 16777215
     SELF.Q.job_ali:Date_Completed_SelectedBG = 8388736
   ELSIF(tmp:ColourFlag = 3)
     SELF.Q.job_ali:Date_Completed_NormalFG = 16711935
     SELF.Q.job_ali:Date_Completed_NormalBG = 16777215
     SELF.Q.job_ali:Date_Completed_SelectedFG = 16777215
     SELF.Q.job_ali:Date_Completed_SelectedBG = 16711935
   ELSIF(tmp:ColourFlag = 4)
     SELF.Q.job_ali:Date_Completed_NormalFG = 8421504
     SELF.Q.job_ali:Date_Completed_NormalBG = 16777215
     SELF.Q.job_ali:Date_Completed_SelectedFG = 16777215
     SELF.Q.job_ali:Date_Completed_SelectedBG = 8421504
   ELSIF(tmp:ColourFlag = 5)
     SELF.Q.job_ali:Date_Completed_NormalFG = 255
     SELF.Q.job_ali:Date_Completed_NormalBG = 16777215
     SELF.Q.job_ali:Date_Completed_SelectedFG = 16777215
     SELF.Q.job_ali:Date_Completed_SelectedBG = 255
   ELSE
     SELF.Q.job_ali:Date_Completed_NormalFG = 0
     SELF.Q.job_ali:Date_Completed_NormalBG = 16777215
     SELF.Q.job_ali:Date_Completed_SelectedFG = 16777215
     SELF.Q.job_ali:Date_Completed_SelectedBG = 0
   END
   IF (job_ali:Date_Completed = '')
     SELF.Q.Days_Since_Booking_Temp_NormalFG = 32768
     SELF.Q.Days_Since_Booking_Temp_NormalBG = 16777215
     SELF.Q.Days_Since_Booking_Temp_SelectedFG = 16777215
     SELF.Q.Days_Since_Booking_Temp_SelectedBG = 32768
   ELSIF(tmp:ColourFlag = 1)
     SELF.Q.Days_Since_Booking_Temp_NormalFG = 8388608
     SELF.Q.Days_Since_Booking_Temp_NormalBG = 16777215
     SELF.Q.Days_Since_Booking_Temp_SelectedFG = 16777215
     SELF.Q.Days_Since_Booking_Temp_SelectedBG = 8388608
   ELSIF(tmp:ColourFlag = 2)
     SELF.Q.Days_Since_Booking_Temp_NormalFG = 8388736
     SELF.Q.Days_Since_Booking_Temp_NormalBG = 16777215
     SELF.Q.Days_Since_Booking_Temp_SelectedFG = 16777215
     SELF.Q.Days_Since_Booking_Temp_SelectedBG = 8388736
   ELSIF(tmp:ColourFlag = 3)
     SELF.Q.Days_Since_Booking_Temp_NormalFG = 16711935
     SELF.Q.Days_Since_Booking_Temp_NormalBG = 16777215
     SELF.Q.Days_Since_Booking_Temp_SelectedFG = 16777215
     SELF.Q.Days_Since_Booking_Temp_SelectedBG = 16711935
   ELSIF(tmp:ColourFlag = 4)
     SELF.Q.Days_Since_Booking_Temp_NormalFG = 8421504
     SELF.Q.Days_Since_Booking_Temp_NormalBG = 16777215
     SELF.Q.Days_Since_Booking_Temp_SelectedFG = 16777215
     SELF.Q.Days_Since_Booking_Temp_SelectedBG = 8421504
   ELSIF(tmp:ColourFlag = 5)
     SELF.Q.Days_Since_Booking_Temp_NormalFG = 255
     SELF.Q.Days_Since_Booking_Temp_NormalBG = 16777215
     SELF.Q.Days_Since_Booking_Temp_SelectedFG = 16777215
     SELF.Q.Days_Since_Booking_Temp_SelectedBG = 255
   ELSE
     SELF.Q.Days_Since_Booking_Temp_NormalFG = 0
     SELF.Q.Days_Since_Booking_Temp_NormalBG = 16777215
     SELF.Q.Days_Since_Booking_Temp_SelectedFG = 16777215
     SELF.Q.Days_Since_Booking_Temp_SelectedBG = 0
   END
   IF (job_ali:Date_Completed = '')
     SELF.Q.job_ali:Exchange_Unit_Number_NormalFG = 32768
     SELF.Q.job_ali:Exchange_Unit_Number_NormalBG = 16777215
     SELF.Q.job_ali:Exchange_Unit_Number_SelectedFG = 16777215
     SELF.Q.job_ali:Exchange_Unit_Number_SelectedBG = 32768
   ELSIF(tmp:ColourFlag = 1)
     SELF.Q.job_ali:Exchange_Unit_Number_NormalFG = 8388608
     SELF.Q.job_ali:Exchange_Unit_Number_NormalBG = 16777215
     SELF.Q.job_ali:Exchange_Unit_Number_SelectedFG = 16777215
     SELF.Q.job_ali:Exchange_Unit_Number_SelectedBG = 8388608
   ELSIF(tmp:ColourFlag = 2)
     SELF.Q.job_ali:Exchange_Unit_Number_NormalFG = 8388736
     SELF.Q.job_ali:Exchange_Unit_Number_NormalBG = 16777215
     SELF.Q.job_ali:Exchange_Unit_Number_SelectedFG = 16777215
     SELF.Q.job_ali:Exchange_Unit_Number_SelectedBG = 8388736
   ELSIF(tmp:ColourFlag = 3)
     SELF.Q.job_ali:Exchange_Unit_Number_NormalFG = 16711935
     SELF.Q.job_ali:Exchange_Unit_Number_NormalBG = 16777215
     SELF.Q.job_ali:Exchange_Unit_Number_SelectedFG = 16777215
     SELF.Q.job_ali:Exchange_Unit_Number_SelectedBG = 16711935
   ELSIF(tmp:ColourFlag = 4)
     SELF.Q.job_ali:Exchange_Unit_Number_NormalFG = 8421504
     SELF.Q.job_ali:Exchange_Unit_Number_NormalBG = 16777215
     SELF.Q.job_ali:Exchange_Unit_Number_SelectedFG = 16777215
     SELF.Q.job_ali:Exchange_Unit_Number_SelectedBG = 8421504
   ELSIF(tmp:ColourFlag = 5)
     SELF.Q.job_ali:Exchange_Unit_Number_NormalFG = 255
     SELF.Q.job_ali:Exchange_Unit_Number_NormalBG = 16777215
     SELF.Q.job_ali:Exchange_Unit_Number_SelectedFG = 16777215
     SELF.Q.job_ali:Exchange_Unit_Number_SelectedBG = 255
   ELSE
     SELF.Q.job_ali:Exchange_Unit_Number_NormalFG = 0
     SELF.Q.job_ali:Exchange_Unit_Number_NormalBG = 16777215
     SELF.Q.job_ali:Exchange_Unit_Number_SelectedFG = 16777215
     SELF.Q.job_ali:Exchange_Unit_Number_SelectedBG = 0
   END
   IF (job_ali:Date_Completed = '')
     SELF.Q.job_ali:Warranty_Job_NormalFG = 32768
     SELF.Q.job_ali:Warranty_Job_NormalBG = 16777215
     SELF.Q.job_ali:Warranty_Job_SelectedFG = 16777215
     SELF.Q.job_ali:Warranty_Job_SelectedBG = 32768
   ELSIF(tmp:ColourFlag = 1)
     SELF.Q.job_ali:Warranty_Job_NormalFG = 8388608
     SELF.Q.job_ali:Warranty_Job_NormalBG = 16777215
     SELF.Q.job_ali:Warranty_Job_SelectedFG = 16777215
     SELF.Q.job_ali:Warranty_Job_SelectedBG = 8388608
   ELSIF(tmp:ColourFlag = 2)
     SELF.Q.job_ali:Warranty_Job_NormalFG = 8388736
     SELF.Q.job_ali:Warranty_Job_NormalBG = 16777215
     SELF.Q.job_ali:Warranty_Job_SelectedFG = 16777215
     SELF.Q.job_ali:Warranty_Job_SelectedBG = 8388736
   ELSIF(tmp:ColourFlag = 3)
     SELF.Q.job_ali:Warranty_Job_NormalFG = 16711935
     SELF.Q.job_ali:Warranty_Job_NormalBG = 16777215
     SELF.Q.job_ali:Warranty_Job_SelectedFG = 16777215
     SELF.Q.job_ali:Warranty_Job_SelectedBG = 16711935
   ELSIF(tmp:ColourFlag = 4)
     SELF.Q.job_ali:Warranty_Job_NormalFG = 8421504
     SELF.Q.job_ali:Warranty_Job_NormalBG = 16777215
     SELF.Q.job_ali:Warranty_Job_SelectedFG = 16777215
     SELF.Q.job_ali:Warranty_Job_SelectedBG = 8421504
   ELSIF(tmp:ColourFlag = 5)
     SELF.Q.job_ali:Warranty_Job_NormalFG = 255
     SELF.Q.job_ali:Warranty_Job_NormalBG = 16777215
     SELF.Q.job_ali:Warranty_Job_SelectedFG = 16777215
     SELF.Q.job_ali:Warranty_Job_SelectedBG = 255
   ELSE
     SELF.Q.job_ali:Warranty_Job_NormalFG = 0
     SELF.Q.job_ali:Warranty_Job_NormalBG = 16777215
     SELF.Q.job_ali:Warranty_Job_SelectedFG = 16777215
     SELF.Q.job_ali:Warranty_Job_SelectedBG = 0
   END
   IF (job_ali:Date_Completed = '')
     SELF.Q.job_ali:Chargeable_Job_NormalFG = 32768
     SELF.Q.job_ali:Chargeable_Job_NormalBG = 16777215
     SELF.Q.job_ali:Chargeable_Job_SelectedFG = 16777215
     SELF.Q.job_ali:Chargeable_Job_SelectedBG = 32768
   ELSIF(tmp:ColourFlag = 1)
     SELF.Q.job_ali:Chargeable_Job_NormalFG = 8388608
     SELF.Q.job_ali:Chargeable_Job_NormalBG = 16777215
     SELF.Q.job_ali:Chargeable_Job_SelectedFG = 16777215
     SELF.Q.job_ali:Chargeable_Job_SelectedBG = 8388608
   ELSIF(tmp:ColourFlag = 2)
     SELF.Q.job_ali:Chargeable_Job_NormalFG = 8388736
     SELF.Q.job_ali:Chargeable_Job_NormalBG = 16777215
     SELF.Q.job_ali:Chargeable_Job_SelectedFG = 16777215
     SELF.Q.job_ali:Chargeable_Job_SelectedBG = 8388736
   ELSIF(tmp:ColourFlag = 3)
     SELF.Q.job_ali:Chargeable_Job_NormalFG = 16711935
     SELF.Q.job_ali:Chargeable_Job_NormalBG = 16777215
     SELF.Q.job_ali:Chargeable_Job_SelectedFG = 16777215
     SELF.Q.job_ali:Chargeable_Job_SelectedBG = 16711935
   ELSIF(tmp:ColourFlag = 4)
     SELF.Q.job_ali:Chargeable_Job_NormalFG = 8421504
     SELF.Q.job_ali:Chargeable_Job_NormalBG = 16777215
     SELF.Q.job_ali:Chargeable_Job_SelectedFG = 16777215
     SELF.Q.job_ali:Chargeable_Job_SelectedBG = 8421504
   ELSIF(tmp:ColourFlag = 5)
     SELF.Q.job_ali:Chargeable_Job_NormalFG = 255
     SELF.Q.job_ali:Chargeable_Job_NormalBG = 16777215
     SELF.Q.job_ali:Chargeable_Job_SelectedFG = 16777215
     SELF.Q.job_ali:Chargeable_Job_SelectedBG = 255
   ELSE
     SELF.Q.job_ali:Chargeable_Job_NormalFG = 0
     SELF.Q.job_ali:Chargeable_Job_NormalBG = 16777215
     SELF.Q.job_ali:Chargeable_Job_SelectedFG = 16777215
     SELF.Q.job_ali:Chargeable_Job_SelectedBG = 0
   END
   IF (job_ali:Date_Completed = '')
     SELF.Q.tmp:ColourFlag_NormalFG = 32768
     SELF.Q.tmp:ColourFlag_NormalBG = 16777215
     SELF.Q.tmp:ColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:ColourFlag_SelectedBG = 32768
   ELSIF(tmp:ColourFlag = 1)
     SELF.Q.tmp:ColourFlag_NormalFG = 8388608
     SELF.Q.tmp:ColourFlag_NormalBG = 16777215
     SELF.Q.tmp:ColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:ColourFlag_SelectedBG = 8388608
   ELSIF(tmp:ColourFlag = 2)
     SELF.Q.tmp:ColourFlag_NormalFG = 8388736
     SELF.Q.tmp:ColourFlag_NormalBG = 16777215
     SELF.Q.tmp:ColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:ColourFlag_SelectedBG = 8388736
   ELSIF(tmp:ColourFlag = 3)
     SELF.Q.tmp:ColourFlag_NormalFG = 16711935
     SELF.Q.tmp:ColourFlag_NormalBG = 16777215
     SELF.Q.tmp:ColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:ColourFlag_SelectedBG = 16711935
   ELSIF(tmp:ColourFlag = 4)
     SELF.Q.tmp:ColourFlag_NormalFG = 8421504
     SELF.Q.tmp:ColourFlag_NormalBG = 16777215
     SELF.Q.tmp:ColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:ColourFlag_SelectedBG = 8421504
   ELSIF(tmp:ColourFlag = 5)
     SELF.Q.tmp:ColourFlag_NormalFG = 255
     SELF.Q.tmp:ColourFlag_NormalBG = 16777215
     SELF.Q.tmp:ColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:ColourFlag_SelectedBG = 255
   ELSE
     SELF.Q.tmp:ColourFlag_NormalFG = 0
     SELF.Q.tmp:ColourFlag_NormalBG = 16777215
     SELF.Q.tmp:ColourFlag_SelectedFG = 16777215
     SELF.Q.tmp:ColourFlag_SelectedBG = 0
   END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  IF BRW1::RecordStatus NOT=Record:OK THEN RETURN BRW1::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:q_JobNumber.Job_Number_Pointer = addtmp:JobNumber
     GET(glo:q_JobNumber,glo:q_JobNumber.Job_Number_Pointer)
    EXECUTE DASBRW::13:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW1::RecordStatus
  RETURN ReturnValue


BRW3.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

OfflineIMEIValidation PROCEDURE  (manufacturer, imeiNumber) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
strResult string(30)
  CODE
    ! OfflineIMEIValidation(string manufacturer, string imeiNumber), string strResult
    !
    ! Validate the IMEI number against a local file for a particular manufacturer

    case manufacturer
        of 'NOKIA'

            Relate:IMEISHIP.Open()

            Access:IMEISHIP.ClearKey(IMEI:IMEIKey)
            IMEI:IMEINumber = clip(imeiNumber)
            if Access:IMEISHIP.Fetch(IMEI:IMEIKey)
                strResult = 'FAIL' ! Failure to validate IMEI
            else
                strResult = 'SUCCESS'
                GLO:Select3 = IMEI:ShipDate ! DOP
            end

            Relate:IMEISHIP.Close()
    end

    return strResult
PreviousAddressWindow PROCEDURE                       !Generated from procedure template - Window

ReturnFlag           BYTE(0)
locOutFault          STRING(255)
locOutFaultDescription STRING(255)
locOutFaultDisplay   STRING(255)
window               WINDOW('Previous Customer Details'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Previous Customer Details'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Previous Customer'),USE(?Tab1)
                           STRING('Previous Job Details'),AT(299,122),USE(?String1),FONT('Tahoma',8,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING('Company Name'),AT(212,162),USE(?String2),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(292,162),USE(job_ali:Company_Name),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(292,138),USE(job_ali:Ref_Number),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING('Out Fault'),AT(212,148),USE(?String2:3),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s255),AT(292,148,216,10),USE(locOutFaultDisplay),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING('Job Number'),AT(212,138),USE(?String2:2),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(292,174),USE(job_ali:Address_Line1),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING('Address'),AT(212,174),USE(?String3),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(292,182),USE(job_ali:Address_Line2),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING('Contact Numbers'),AT(212,202),USE(?String4),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING('Telephone Number'),AT(212,214),USE(?String10),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING('Fax Number'),AT(212,222),USE(?String11),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s15),AT(292,222),USE(job_ali:Fax_Number),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING('Email Address'),AT(212,230),USE(?String12),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s40),AT(292,230,176,12),USE(jobe:EndUserEmailAddress),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING('Do you wish to use the same customer details on this job?'),AT(219,268),USE(?String15),FONT('Tahoma',9,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s15),AT(292,214),USE(job_ali:Telephone_Number),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(292,190),USE(job_ali:Address_Line3),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(380,332),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(448,332),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(ReturnFlag)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020512'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('PreviousAddressWindow')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:JOBSE.Open
  Access:JOBS_ALIAS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      GetTheMainOutFault(job_ali:Ref_Number,locOutFault,locOutFaultDescription)
      if (locOutFault <> '')
          locOutFaultDisplay = clip(locOutFault) & ': ' & clip(locOUtFaultDescription)
      else
          locOutFaultDisplay = ''
      end
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBSE.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
      ReturnFlag = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020512'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020512'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020512'&'0')
      ***
    OF ?OkButton
      ThisWindow.Update
       POST(Event:CloseWindow)
    OF ?CancelButton
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Multiple_Job_Booking PROCEDURE                        !Generated from procedure template - Window

Multiple_Setup       GROUP,PRE()
Model_No             BYTE
Colour1              BYTE
Order_No             BYTE
Fault_Code           BYTE
Engineers_Notes      BYTE
Contact_History      BYTE
Replicate_Job_No     LONG
                     END
window               WINDOW('Multiple Job Booking'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Multiple Job Booking'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Multiple Job Booking'),USE(?Tab1)
                           PROMPT('Select the fields that will change from one job to another.'),AT(248,166,184,20),USE(?Txt),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('Model Number'),AT(304,184),USE(Model_No),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                           CHECK('Colour'),AT(304,196),USE(Colour1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                           CHECK('Order Number'),AT(304,208),USE(Order_No),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                           CHECK('Fault Code'),AT(304,220),USE(Fault_Code),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                           CHECK('Engineers Notes'),AT(304,232),USE(Engineers_Notes),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                           CHECK('Contact History'),AT(304,244),USE(Contact_History),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                         END
                       END
                       BUTTON,AT(304,260),USE(?Ok),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(368,260),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020509'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Multiple_Job_Booking')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:JOBS.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Ok
          ! Multiple Job Booking
          0{PROP:HIDE} = True
          Display()
      
          FirstLoop# = True
          loop ! Loop until user cancel
              GlobalRequest = InsertRecord
              glo:insert_global = 'YES'
              glo:Select24 = ''
              Update_Jobs(Address(Multiple_Setup)) ! Pass group with multiple job setup parameters
              If glo:Select24 = 'CONTINUE BOOKING'
                  ! Inserting (DBH 18/09/2006) # 8278 - If the first job fails, then you can't continue the booking
                  If FirstLoop# = True
                      Break
                  Else ! If FirstLoop# = True
                      Cycle
                  End ! If FirstLoop# = True
                  ! End (DBH 18/09/2006) #8278
              End !If glo:Select24 = 'CONTINUE BOOKING'
              FirstLoop# = False
              if GlobalResponse <> RequestCompleted then break.
              ! Inserting (DBH 14/07/2006) # 7975 - What reports are required to be printed?
              If GlobalResponse = 1
                  ! Job has booked correctly (DBH: 14/07/2006)
                  Loop x# = 1 To Records(glo:Queue)
                      Get(glo:Queue,x#)
                      Case glo:Pointer
                      Of 'THERMAL LABEL'
                          glo:Select1 = job:Ref_Number
                          If job:Bouncer = 'B'
                              Thermal_Labels('SE')
                          Else ! If job:Bouncer = 'B'
                              If ExchangeAccount(job:Account_Number)
                                  Thermal_Labels('ER')
                              Else ! If ExchangeAccount(job:Account_Number)
                                  Thermal_Labels('')
                              End ! If ExchangeAccount(job:Account_Number)
                          End ! If job:Bouncer = 'B'
                          glo:Select1 = ''
                      Of 'JOB CARD'
                          glo:Select1 = job:Ref_Number
                          Job_Card
                          glo:Select1 = ''
                      Of 'RETAINED ACCESSORIES LABEL'
                          glo:Select1 = job:Ref_Number
                          Job_Retained_Accessories_Label
                          glo:Select1 = ''
                      Of 'RECEIPT'
                          glo:Select1 = job:Ref_Number
                          Job_Receipt
                          glo:Select1 = ''
                      End ! Case glo:Pointer
                  End ! Loop x# = 1 To Records(glo:Queue)
      
              End ! If GlobalReponse = 1
              ! Inserting (DBH 14/07/2006) # 7975 - What reports are required to be printed?
      
              Case Missive('Do you want to book another job?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                  Of 1 ! No Button
                      break
              End ! Case Missive
      
          end
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020509'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020509'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020509'&'0')
      ***
    OF ?Ok
      ThisWindow.Update
       POST(Event:CloseWindow)
    OF ?Close
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
ChoosePrintRoutine PROCEDURE                          !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
OptionChoice         STRING(30)
window               WINDOW('Choose Print Routine'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Choose Print Routine'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,82,352,248),USE(?Panel1),FILL(09A6A7CH)
                       OPTION,AT(260,130,160,152),USE(OptionChoice),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         RADIO('  Customer Receipt'),AT(284,142),USE(?OptionChoice:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('CUSTOMER')
                         RADIO('  Job Card'),AT(284,156),USE(?OptionChoice:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('JOBCARD')
                         RADIO('  Estimate'),AT(284,172),USE(?OptionChoice:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('ESTIMATE')
                         RADIO('  Invoice'),AT(284,186),USE(?OptionChoice:Radio4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('INVOICE')
                         RADIO('  Credit Note'),AT(340,186),USE(?OptionChoice:Radio5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('CREDIT')
                         RADIO('  Despatch Note'),AT(284,202),USE(?OptionChoice:Radio6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('DESPATCH')
                         RADIO('  Waybill'),AT(284,218),USE(?OptionChoice:Radio7),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('WAYBILL')
                         RADIO('  Bastion Validation Print'),AT(284,234),USE(?OptionChoice:Radio8),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('BASTION')
                         RADIO('  Loan Collection Note'),AT(284,250),USE(?OptionChoice:Radio9),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('LOANCOLL')
                         RADIO('  OBF Validation Report'),AT(284,266),USE(?OptionChoice:Radio10),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('OBFVAL')
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020505'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ChoosePrintRoutine')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  set(DEFAULTS,0)
  if not Access:DEFAULTS.Next()
      if Instring('VODAC',def:User_Name,1,1)
          ?OptionChoice:Radio8{prop:Hide} = False
      end
  end
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OK
      glo:select1 = OptionChoice
    OF ?Cancel
      glo:select1 = 'CANCEL'
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020505'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020505'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020505'&'0')
      ***
    OF ?OK
      ThisWindow.Update
       POST(Event:CloseWindow)
    OF ?Cancel
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Xfiles PROCEDURE                                      !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Caption'),AT(,,260,100),GRAY,DOUBLE
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Xfiles')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = 1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTV.Open
  Relate:EXPGEN.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTV.Close
    Relate:EXPGEN.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
ShowKey PROCEDURE                                     !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Colour Code Key'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,82,352,248),USE(?Panel1),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Line Colour Code Key'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PROMPT('Line Colour:'),AT(244,150),USE(?Prompt3),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('Reason:'),AT(332,150),USE(?Prompt4),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       STRING('Job Not At RRC'),AT(332,186),USE(?String4),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PANEL,AT(244,206,73,9),USE(?Panel6:3),FILL(COLOR:Blue)
                       PANEL,AT(244,186,73,9),USE(?Panel6:2),FILL(COLOR:Red)
                       STRING('Job Despatched To Customer'),AT(332,206),USE(?String4:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PANEL,AT(244,226,73,9),USE(?Panel6:4),FILL(COLOR:Green)
                       STRING('Job At RRC'),AT(332,166),USE(?String5),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PANEL,AT(244,166,73,9),USE(?Panel6),FILL(COLOR:Black)
                       STRING('Job Awaiting Transit To ARC'),AT(332,226),USE(?String6),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PANEL,AT(244,246,73,9),USE(?Panel6:5),FILL(COLOR:Fuschia)
                       PANEL,AT(244,268,73,9),USE(?Panel6:6),FILL(COLOR:Purple)
                       STRING('Jobs In transit To RRC'),AT(332,246),USE(?StrTransitRRC),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       STRING('Liquid Damage'),AT(332,266),USE(?StrTransitRRC:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       BUTTON,AT(448,332),USE(?Button1),TRN,FLAT,LEFT,ICON('okp.jpg'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020517'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ShowKey')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Panel1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020517'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020517'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020517'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
ReprintWayBillThinClient PROCEDURE (WaybillNumber)    !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
LocalWaybillNumber   LONG
LocalOverWrite       STRING(1)
LocalCourier         STRING(30)
BRW8::View:Browse    VIEW(JOBS_ALIAS)
                       PROJECT(job_ali:Ref_Number)
                       PROJECT(job_ali:ESN)
                       PROJECT(job_ali:Model_Number)
                       PROJECT(job_ali:Consignment_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
job_ali:Ref_Number     LIKE(job_ali:Ref_Number)       !List box control field - type derived from field
job_ali:ESN            LIKE(job_ali:ESN)              !List box control field - type derived from field
job_ali:Model_Number   LIKE(job_ali:Model_Number)     !List box control field - type derived from field
job_ali:Consignment_Number LIKE(job_ali:Consignment_Number) !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Reprint Waybills'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Reprint Waybill'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       STRING(@s8),AT(232,100),USE(LocalWaybillNumber),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       LIST,AT(168,112,344,214),USE(?List),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('48L(2)|FM~Job Number~@s8@80L(2)|F~I.M.E.I. Number~@s20@120L(2)|F~Model Number~@s' &|
   '30@'),FROM(Queue:Browse)
                       STRING('Waybill Number:'),AT(168,100),USE(?StrWaybillNumber),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Jobs On Waybill'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(380,332),USE(?Close),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW8                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW8::Sort0:Locator  StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020516'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ReprintWayBillThinClient')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:JOBS_ALIAS.Open
  SELF.FilesOpened = True
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:JOBS_ALIAS,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  LocalWaybillNumber = WayBillNumber
  Access:JOBS_ALIAS.CLEARKEY(job_ali:ConsignmentNoKey)
  job_ali:Consignment_Number = LocalWayBillNumber
  SET(job_ali:ConsignmentNoKey,job_ali:ConsignmentNoKey)
  IF Access:JOBS_ALIAS.NEXT() THEN
     flag# = 0
  ELSE
      IF job_ali:Consignment_Number <> LocalWayBillNumber THEN
         flag# = 0
      ELSE
          flag# = 1
      END !IF
  END !IF
  IF flag# = 0 THEN
     DISABLE(?Close)
  END !IF
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW8.Q &= Queue:Browse
  BRW8.AddSortOrder(,job_ali:ConsignmentNoKey)
  BRW8.AddRange(job_ali:Consignment_Number,LocalWaybillNumber)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(,job_ali:Consignment_Number,1,BRW8)
  BRW8.AddField(job_ali:Ref_Number,BRW8.Q.job_ali:Ref_Number)
  BRW8.AddField(job_ali:ESN,BRW8.Q.job_ali:ESN)
  BRW8.AddField(job_ali:Model_Number,BRW8.Q.job_ali:Model_Number)
  BRW8.AddField(job_ali:Consignment_Number,BRW8.Q.job_ali:Consignment_Number)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW8.AskProcedure = 0
      CLEAR(BRW8.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS_ALIAS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Close
      Access:TRADEACC.Clearkey(tra:Account_Number_key)
      tra:Account_Number  = Clarionet:Global.Param2
      Access:TRADEACC.Fetch(tra:Account_Number_key)
      
      free(glo:q_jobnumber)
      clear(glo:q_jobnumber)
      
      Access:JOBS.ClearKey(job:ConsignmentNoKey)
      job:Consignment_Number = LocalWayBillNumber
      set(job:ConsignmentNoKey, job:ConsignmentNoKey)
      loop until Access:JOBS.Next()
        if job:Consignment_Number <> LocalWayBillNumber then break.
        glo:q_JobNumber.GLO:Job_Number_Pointer  = job:Ref_Number
        Add(glo:q_JobNumber)
      end
      
      !make sure we get the overriden courier if it is set
      LocalOverWrite = GETINI('DESPATCH','OVERWRITECOURIER','N',CLIP(PATH()) & '\SB2KDEF.INI')
      LocalCourier = GETINI('DESPATCH','OUTGOINGCOURIER','',CLIP(PATH()) & '\SB2KDEF.INI')
      if Records(glo:q_JobNumber)
          If LocalOverWrite <> 'Y'
             WayBillDespatch(Clip(Clarionet:Global.Param2),'TRA',|
                             Clip(GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')),'TRA',|
                             LocalWayBillNumber,tra:Courier_Outgoing)
          Else
              WayBillDespatch(Clip(Clarionet:Global.Param2),'TRA',|
                             Clip(GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')),'TRA',|
                             LocalWayBillNumber,LocalCourier)
          End !If LocalOverWrite <> 'Y'
          free(glo:q_jobnumber)
      end
      
      
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020516'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020516'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020516'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

ReprintExchangeLoanWaybill PROCEDURE  (func:WayBillNumber) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Type             STRING(3)
tmp:Courier          STRING(30)
  CODE
   Relate:WAYBILLS.Open
   Relate:TRADEACC.Open
   Relate:SUBTRACC.Open
   Relate:WAYITEMS.Open
    Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
    way:WayBillNumber = func:WayBillNumber
    if not Access:WAYBILLS.Fetch(way:WayBillNumberKey)
        case way:WaybillID
            of 100  ! Exchange Waybill
                do Waybill:Exchange
            of 101  ! Loan Waybill
                do Waybill:Loan
        end
    end
   Relate:WAYBILLS.Close
   Relate:TRADEACC.Close
   Relate:SUBTRACC.Close
   Relate:WAYITEMS.Close
Waybill:Exchange routine

    ! Inserting (DBH 20/06/2006) #7853 - If this waybill is going back to the National Stores
    If way:ToAccount = GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI')
        WayBillExchange(way:FromAccount,'TRA',|
                        way:FromAccount,'DEF',|
                        way:WaybillNumber,GETINI('DESPATCH','RETCourier',,Clip(Path()) & '\SB2KDEF.INI'),1)
    Else ! If way:ToAccount = GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI')
    ! End (DBH 20/06/2006) #7853

        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = way:ToAccount
        if not Access:TRADEACC.Fetch(tra:Account_Number_Key)
            tmp:Type = 'TRA'
        else
            Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
            sub:Account_Number = way:ToAccount
            Access:SUBTRACC.Fetch(sub:Account_Number_Key)
            tmp:Type = 'SUB'
        end

        tmp:courier = getini('DESPATCH','RETcourier','',clip(path())&'\SB2KDEF.INI')

        !In case the tmp:courier was not set up
        if tmp:courier = ''
            Case tmp:Type
                Of 'TRA'
                    tmp:Courier = tra:Courier_Incoming
                Of 'SUB'
                    tmp:Courier = sub:Courier_Incoming
            End !Case func:Type
        END

        ARCAccount" = Clip(GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI'))


! Changing (DBH 12/01/2007) # 8666 - Check if this is a 48 hour waybill, (therefore show job number on reprint)
!        WayBillExchange(ARCAccount",'TRA',|
!                        way:ToAccount,tmp:Type,|
!                        way:WayBillNumber,tmp:Courier,0)
! to (DBH 12/01/2007) # 8666
        Is48Hour# = 0

        Access:WAYITEMS.Clearkey(wai:WaybillNumberKey)
        wai:WayBillNumber = way:WayBillNumber
        Set(wai:WaybillNumberKey,wai:WaybillNumberKey)
        Loop ! Begin Loop
            If Access:WAYITEMS.Next()
                Break
            End ! If Access:WAYITEMS.Next()
            If wai:WaybillNumber <> way:WayBillNumber
                Break
            End ! If wai:WaybillNumber <> way:WayBillNumber
            If wai:JobNumber48Hour > 0
                Is48Hour# = 1
                Break
            End ! If wai:JobNumber48Hour > 0
        End ! Loop

        WayBillExchange(ARCAccount",'TRA',|
                        way:ToAccount,tmp:Type,|
                        way:WayBillNumber,tmp:Courier,Is48Hour#)

! End (DBH 12/01/2007) #8666
    End ! If way:ToAccount = GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI')

Waybill:Loan routine

    Access:TRADEACC.ClearKey(tra:Account_Number_Key)
    tra:Account_Number = way:ToAccount
    if not Access:TRADEACC.Fetch(tra:Account_Number_Key)
        tmp:Type = 'TRA'
    else
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = way:ToAccount
        Access:SUBTRACC.Fetch(sub:Account_Number_Key)
        tmp:Type = 'SUB'
    end

    Case tmp:Type
        Of 'TRA'
            tmp:Courier = tra:Courier_Incoming
        Of 'SUB'
            tmp:Courier = sub:Courier_Incoming
    End !Case func:Type

    ARCAccount" = Clip(GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI'))

    WayBillLoan(ARCAccount",'TRA',|
                way:ToAccount,tmp:Type,|
                way:WayBillNumber,tmp:Courier)
