

   MEMBER('sbj04app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ04001.INC'),ONCE        !Local module procedure declarations
                     END


WobLabel             PROCEDURE                        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
!And this is where it starts
    !Find the relevant details

    Access:jobs.clearkey(job:Ref_Number_Key)
    job:Ref_Number = Glo:Select1
    access:jobs.fetch(job:Ref_Number_Key)

    Access:jobNotes.clearkey(jbn:RefNumberKey)
    jbn:RefNumber = Glo:Select1
    access:jobnotes.fetch(jbn:RefNumberKey)

    ClarioNET:CallClientProcedure('LABEL',GLO:Select2,Job:Date_booked,Job:Time_Booked,|
                    clip(job:title)&Clip(Job:Surname),Job:COmpany_Name,Job:LOcation,|
                    jbn:fault_description,job:Model_Number,job:Manufacturer,|
                    job:ESN,job:Warranty_Job,job:Chargeable_Job)

    Case Missive('A label for job number ' & Clip(glo:Select1) & ' will print when you exit this program.','ServiceBase 3g',|
                   'midea.jpg','/OK')
        Of 1 ! OK Button
    End ! Case Missive
    !Labels will now be printed on exiting this program


CancelWobReason PROCEDURE                             !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Close            BYTE(0)
tmp:CancelReason     STRING(255)
window               WINDOW('Cancel Reason'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Cancellation Reason'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Please enter your reason for cancelling this job'),AT(236,158),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(236,172,204,74),USE(tmp:CancelReason),VSCROLL,LEFT,FONT(,,,FONT:bold),COLOR(COLOR:White),MSG('Cancel Reason'),TIP('Cancel Reason'),REQ,UPR
                         END
                       END
                       BUTTON,AT(448,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:CancelReason)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020503'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('CancelWobReason')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020503'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020503'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020503'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      If tmp:CancelReason = ''
          Select(?tmp:CancelReason)
      Else !tmp:CancelReason = ''
          tmp:Close = 1
          Post(Event:CloseWindow)
      End !tmp:CancelReason = ''
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
      If ~tmp:Close
          Cycle
      End !tmp:Cancel
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
CancelWob            PROCEDURE  (SentNo)              ! Declare Procedure
JobNumber            LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
   Relate:JOBS.Open
   Relate:AUDIT.Open
   Relate:USERS.Open
   Relate:WEBJOB.Open
!Cancel job

!get the sent number
    JobNumber = SentNo

!Stolen from Bryans cancel job
    check_access('JOBS - CANCELLING',x")
    if x" = false
        Case Missive('You do not have access to this option.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
    Else!if x" = false
        Case Missive('You are about to CANCEL the selected job.'&|
          '<13,10>If you continue an entry will be made in the audit trail. You WILL be able to despatch the job back to the customer.'&|
          '<13,10>You will NOT be able to reverse this process. Are you sure?','ServiceBase 3g',|
                       'mquest.jpg','\No|/Yes')
            Of 2 ! Yes Button

                error# = 0
                access:jobs.clearkey(job:ref_number_key)
                job:ref_number  = jobnumber
                If access:jobs.tryfetch(job:ref_number_key) = Level:benign

                    If job:cancelled       = 'YES'
                        Case Missive('Error! This job has already been cancelled.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                        error# = 1
                    End!If job:consignment_number <> ''

                    If job:consignment_number <> ''
                        Case Missive('Error! This job has already been despatched.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                        error# = 1
                    End!If job:consignment_number <> ''
                    If job:exchange_unit_number <> ''
                        Case Missive('Error! Cannot cancel job as it has an exchange unit attached.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                        error# = 1
                    End!If job:exchange_unit_number <> ''
                    If error# = 0
                        If job:loan_unit_number <> ''
                            Case Missive('Error! Cannot cancel job as it has an loan unit attached.','ServiceBase 3g',|
                                           'mstop.jpg','/OK')
                                Of 1 ! OK Button
                            End ! Case Missive
                            error# = 1
                        End!If job:loan_unit_number <> ''
                    End!If error# = 0
                End!If access:jobs.tryfetch(job:ref_number_key) = Level:benign
                If error# = 0
                    !Complete Job
                    If job:date_completed = ''
                        job:date_completed  = Today()
                        job:time_completed  = Clock()
                        job:completed       = 'YES'
                        job:edi             = 'XXX'
                        !Update WEBJOBS with Completed Info -  (DBH: 14-10-2003)
                        Access:WEBJOB.Clearkey(wob:RefNumberKey)
                        wob:RefNumber = job:Ref_Number
                        If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                            !Found
                            wob:DateCompleted = job:Date_Completed
                            wob:TimeCompleted   = job:Time_Completed
                            wob:Completed   = 'YES'
                            Access:WEBJOB.Update()
                        Else !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                            !Error
                        End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign

                    End!If job:date_completed = ''
!Warning
!Remmed out
                    !If joEnd!nsignment_number = ''
                    !    Include('companc.inc')
                    !End!If job:consignment_number = ''
                    GetStatus(799,1,'JOB')
                    job:cancelled       = 'YES'
                    job:Bouncer         = ''
                    access:jobs.update()

                    IF (AddToAudit(job:ref_number,'JOB','JOB CANCELLED',CancelWobReason()))
                    END ! IF

                End!If error# = 0
            Of 1 ! No Button
        End ! Case Missive
    End!if x" = false
   Relate:JOBS.Close
   Relate:AUDIT.Close
   Relate:USERS.Close
   Relate:WEBJOB.Close
GetWEDefaults PROCEDURE                               !Generated from procedure template - Window

NokiaUserName        STRING(30)
NokiaPassword        STRING(30)
MotorolaUsername     STRING(30)
MotorolaPassword     STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Setting Web Enquiry Details'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Set-up Web Enquiry Defaults'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,82,352,248),USE(?Panel3),FILL(09A6A7CH)
                       STRING('Error !'),AT(329,116),USE(?TitleString),TRN,FONT('Tahoma',12,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('The details needed for the web enquiry system to work have not been fully set up' &|
   '. So that this system can work properly please complete the following fields and' &|
   ' click on OK.'),AT(242,144,196,36),USE(?Prompt3),CENTER,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       GROUP('Nokia Details'),AT(224,186,232,52),USE(?NokiaGroup),BOXED,TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         STRING('User Name'),AT(236,198),USE(?String1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@s30),AT(316,198,124,10),USE(NokiaUserName),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                         STRING('Password'),AT(236,218),USE(?String2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@s30),AT(316,218,124,10),USE(NokiaPassword),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                       END
                       GROUP('Motorola Details'),AT(224,244,232,52),USE(?MotorolaGroup),BOXED,TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         STRING('User Name'),AT(236,257),USE(?String3),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@s30),AT(316,257,124,10),USE(MotorolaUsername),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                         STRING('Password'),AT(236,278),USE(?String4),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@s30),AT(316,278,124,10),USE(MotorolaPassword),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                       END
                       BUTTON,AT(380,332),USE(?OKButton),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020506'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('GetWEDefaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  !read details from the file
  NokiaUserName  = getini('WESettings','Username')
  NokiaPassword  = getini('WESettings','Password')
  MotorolaUsername  = getini('MWESettings','Username')
  MotorolaPassword  = getini('MWESettings','Password')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020506'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020506'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020506'&'0')
      ***
    OF ?OKButton
      ThisWindow.Update
      !Return the details to the ini file
      putiniext('WESettings','Username',NokiaUserName,'')
      putiniext('WESettings','Password',NokiaPassword,'')
      putiniext('MWESettings','Username',MotorolaUsername,'')
      putiniext('MWESettings','Password',MotorolaPassword,'')
       POST(Event:CloseWindow)
    OF ?CancelButton
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
getDetails           PROCEDURE  (Manufacturer,SentImei) ! Declare Procedure
RowNo                BYTE
PhoneDetail          STRING(20),DIM(2,20)
WindowDirectoryName  CSTRING(260)
ReadLine             STRING(255)
SplitNo              BYTE
Filename             STRING(255)
TempString           STRING(255)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
NokiaSA              BYTE(0)
tmp:WebUserName      STRING(30)
tmp:WebPassword      STRING(30)
INFILE FILE,DRIVER('ASCII'),PRE(INF),NAME(glo:file_name5),CREATE
INFILERec RECORD
TextLine  STRING(255)
    end !Record  (Part of infile)
    end !infile


waitwindow WINDOW('Internet Connection'),AT(0,0,260,100),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER, |
         GRAY,DOUBLE
       PANEL,AT(4,4,249,90),USE(?Panel11),FILL(COLOR:Silver)
       PANEL,AT(13,13,231,73),USE(?Panel12),FILL(080FFFFH),BEVEL(-4)
       PANEL,AT(89,20,83,17),USE(?Panel13),FILL(COLOR:Silver)
       STRING('Please Wait'),AT(103,24),USE(?String31),TRN,FONT('Tahoma',10,,FONT:bold,CHARSET:ANSI)
       STRING('Attempting to contact manufacturer''s web site'),AT(32,44),USE(?String32),TRN,FONT('Tahoma',10,,FONT:bold,CHARSET:ANSI)
       STRING('This may take some time and cannot be cancelled'),AT(24,56),USE(?String33),TRN,FONT('Tahoma',10,,FONT:bold,CHARSET:ANSI)
     END !Pushwindow

window WINDOW,AT(,,160,24),FONT('Tahoma',8,,),CENTER,IMM,GRAY,DOUBLE,AUTO
       PANEL,AT(0,0,160,24),USE(?Panel1),FILL(COLOR:Silver)
       STRING('Accessing Internet'),AT(8,4,148,20),USE(?String1),TRN,FONT(,18,COLOR:Red,FONT:bold)
     END
  CODE
  !go go gary

  !glo:file_name5 needs to be a global variable
  clear(PhoneDetail)
  !check for passwords
  error# = 0
  if getini('WESettings','Username','')   ='' then error#=1.
  if getini('WESettings','Password','')   ='' then error#=1.
  if getini('MWESettings','Username','')  ='' then error#=1.
  if getini('MWESettings','Password','')  ='' then error#=1.

  NokiaSA = False

  Relate:DEFAULTS.Open()

  set(DEFAULTS, 0)
  Access:DEFAULTS.Next()

  if instring('VODAC', def:User_Name, 1, 1)
    NokiaSA = true
  end

  tmp:WebUserName = getini(Manufacturer,'WebUserName','',path()&'\WEBCHECK.INI')
  tmp:WebPassword = getini(Manufacturer,'WebPassword','',path()&'\WEBCHECK.INI')

  Relate:DEFAULTS.Close()

!  if error# = 1 then
!     !will use GetWEDefaults for the moment
!
!     If NokiaSA
!        putini('WESettings','Username','heidih')            ! Nokia South Africa
!        putini('WESettings','Password','12345')
!     Else
!        putini('WESettings','Username','gb5mhamill')        ! Nokia Europe
!        putini('WESettings','Password','gb5marham')
!     End
!
!     putini('MWESettings','Username','P105706')             ! Motorola
!     putini('MWESettings','Password','ymbGfp39')
!  END

  !Lookup the windows directory name
  !Need an entry inthe global map
  If getwindowsdirectory(WindowDirectoryName,260)

  End !If getwindowsdirectory(WindowDirectoryName,260)

  !SET UP A UNIQUE FILENAME
  loop
      tempstring = clip(glo:account_Number_web)&random(10,99)
      filename=sub(clip(tempstring), len(clip(tempstring))-6, len(clip(tempstring)))&'Z.CSV'
      if ~exists(filename) then break.
  END !Loop to find a filename


  !Set up the reply file
  glo:file_name5=sub(clip(tempstring), len(clip(tempstring))-6, len(clip(tempstring)))&'R.CSV'
  if exists(clip(WindowDirectoryName)&'\'&glo:file_name5) then remove(clip(WindowDirectoryName)&'\'&glo:file_name5).

  !copy imei to the send file
  lineprint('"'&clip(SentImei)&'"',clip(WindowDirectoryName)&'\'&clip(filename))

  !show the wait for it window
  IF Glo:WebJob
    ClarioNET:openpushwindow(waitwindow)
  ELSE
    OPEN(Window)
    DISPLAY()
  END
  !Run gary's program
  case Manufacturer
    of 'NOKIA'
        putini('WESettings','Username',tmp:WebUserName)
        putini('WESettings','Password',tmp:WebPassword)
        If NokiaSA
            run('NOKWEB.EXE '&clip(filename)&' '&clip(glo:file_name5),1)
        Else
            run('WEBC.EXE '&clip(filename)&' '&clip(glo:file_name5),1)
        End
    of 'MOTOROLA'
        putini('MWESettings','Username',tmp:WebUserName)
        putini('MWESettings','Password',tmp:WebPassword)
        run('MOTOWEB.EXE '&clip(filename)&' '&clip(glo:file_name5),1)
  END!Case
  !close the wait for it window
  IF Glo:WebJob
    ClarioNET:closepushwindow(waitwindow)
  ELSE
    Close(Window)
  END

  !check the reply file
  if exists(clip(WindowDirectoryName)&'\'&glo:file_name5) then
      !sucesss
      glo:file_name5 = clip(WindowDirectoryName)&'\'&clip(glo:file_name5)
      open(infile)
      set(infile)

      next(infile)
      readline = inf:textline
      rowno = 1
      do splitrow

      next(infile)
      readline = inf:textline
      rowno = 2
      do splitrow

      !Return the variables
      !IMEI
      If NokiaSA
        GLO:Select1 = PhoneDetail[1,2]
      Else
        GLO:Select1 = PhoneDetail[2,1]
      End

      !Reset if not matching
      if GLO:Select1 <> SentIMEI then
        !error
        Glo:Select1 = 'FAIL'
      ELSE
          case Manufacturer
            of 'NOKIA'
                If NokiaSA
                    ! Returns single line - see example below
                    ! "IMEI Number : ","350613109137337 ","Warranty Owner : ","Vodacom ","Warranty Start Date : ","20 November 2001 ","Warranty Expiry Date : ","20 May 2002   ","BER : ","  ","Stolen : ","No "
                    !MSN
                    !GLO:Select2 = PhoneDetail[2,3]
                    !Warranty Start Date
                    GLO:Select3 = PhoneDetail[1,6]
                    do NokiaSA:Format_Date
                    !WAranty END date
                    GLO:Select4 = PhoneDetail[1,8]
                    !Warranty 'YES/NO'
                    GLO:Select5 = 'YES'
                Else
                    !MSN
                    !GLO:Select2 = PhoneDetail[2,3]
                    !Warranty Start Date
                    GLO:Select3 = PhoneDetail[2,4]
                    !WAranty END date
                    GLO:Select4 = PhoneDetail[2,5]
                    !Warranty 'YES/NO'
                    GLO:Select5 = PhoneDetail[2,6]
                End

            of 'MOTOROLA'
                !MSN
                GLO:Select2 =  PhoneDetail[2,2]
                !Nothing else can be found
                !Warranty Start Date
                GLO:Select3 = ''
                !WAranty END date
                GLO:Select4 = ''
                !Warranty 'YES/NO'
                GLO:Select5 = ''

            END !Case
      END!if glo:Select1 was not the sent imei


  ELSE
      !error
        Case Missive('Unable to connect to website.'&|
          '<13,10>Please book job in manually.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
      GLO:Select1 = 'TIME'
      GLO:Select2 = ''

  END

  !clear up existing files
  if exists(clip(WindowDirectoryName)&'\'&clip(filename)) then remove(clip(WindowDirectoryName)&'\'&clip(filename)).
  if exists(clip(WindowDirectoryName)&'\'&clip(glo:file_name5)) then remove(clip(WindowDirectoryName)&'\'&clip(glo:file_name5)).
  if exists(clip(glo:file_name5)) then remove(clip(glo:file_name5)).




SplitRow     Routine
    SplitNo = 1
    loop
      x# = instring(',',readline)
      if x#=0 then
          PhoneDetail[RowNo,SplitNo] = readline[2:len(clip(readline)-1)]
          break
      END !If x#=0
      PhoneDetail[RowNo,SplitNo] = readline[2:x#-2]
      readline = readline[x#+1:len(clip(readline))]
      SplitNo += 1
      if SplitNo >20 then break.
    END !Loop

NokiaSA:Format_Date Routine
    data
strFormatDate string(30)
strDay        string(2)
strMonth      string(30)
strYear       string(4)
count         long
spaces        long,dim(2)
element       long
    code
    strFormatDate = clip(GLO:Select3)
    element = 1

    !"20 November 2001"
    loop i# = 1 to len(clip(strFormatDate))     ! Find spaces
        if strFormatDate[i#] = ' '
            if element > 2 then break.
            spaces[element] = i#
            element += 1
        end
    end

    strDay   = strFormatDate[1:spaces[1]-1]
    strMonth = strFormatDate[spaces[1]+1:spaces[2]-1]
    strYear  = strFormatDate[spaces[2]+1:len(clip(strFormatDate))]

    GLO:Select3 = clip(strMonth) & ' ' & clip(strDay) & ', ' & clip(strYear) ! Date Format (@D4)
    GLO:Select3 = deformat(GLO:Select3, @d4)
OutOfBoxFailure PROCEDURE
  CODE
  GlobalErrors.ThrowMessage(Msg:ProcedureToDo,'OutOfBoxFailure')
  SETKEYCODE(0)
  GlobalResponse = RequestCancelled
Pop_Validate PROCEDURE                                !Generated from procedure template - Window

localText            STRING(20)
LocalDate            DATE
localMemo            STRING(255)
LocalImei            STRING(16)
LocalType            STRING(1)
LocalSubType         STRING(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('This will change'),AT(0,0,232,212),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,GRAY,DOUBLE
                       PANEL,AT(4,4,224,176),USE(?Panel2),FILL(0D6E7EFH)
                       STRING(''),AT(73,13),USE(?String2)
                       STRING('This is the warranty string, it will be changed'),AT(4,16,224,20),USE(?WarrantyString),TRN,CENTER,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                       GROUP,AT(8,80,204,60),USE(?InsGroup),TRN,HIDE
                       END
                       ENTRY(@d17),AT(133,148,,10),USE(LocalDate),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(080FFFFH),REQ
                       PROMPT('Date of Purchase'),AT(8,148),USE(?Prompt2),TRN
                       GROUP,AT(8,36,216,40),USE(?OutsideGroup),BOXED,TRN,HIDE
                         OPTION('Option 1'),AT(10,44,200,32),USE(LocalType)
                           RADIO('  Proof of Purchase Supplied'),AT(14,48),USE(?Option1:Radio1),TRN,FONT('Tahoma',8,,),VALUE('P')
                           RADIO('  Insurance Claim'),AT(126,48),USE(?LocalType:Radio2),TRN,FONT('Tahoma',8,,),VALUE('I')
                           RADIO('  Chargeable Repair'),AT(14,60),USE(?LocalType:Radio3),TRN,FONT('Tahoma',8,,),VALUE('C')
                         END
                       END
                       GROUP,AT(8,76,216,68),USE(?POPGroup),BOXED,TRN,HIDE
                         OPTION,AT(14,84,196,52),USE(LocalSubType)
                           RADIO('  Original Receipt'),AT(126,88,68,12),USE(?Option2:Radio1),TRN,FONT('Tahoma',8,,),VALUE('O')
                           RADIO('  Card Statement'),AT(18,88,68,12),USE(?LocalSubType:Radio2),TRN,FONT('Tahoma',8,,),VALUE('C')
                           RADIO('  Company Record'),AT(126,104,76,12),USE(?LocalSubType:Radio3),TRN,FONT('Tahoma',8,,),VALUE('R')
                           RADIO('  Other'),AT(18,104,44,12),USE(?LocalSubType:Radio4),TRN,FONT('Tahoma',8,,),VALUE('X')
                           RADIO('  None'),AT(18,120,40,12),USE(?LocalSubType:Radio5),TRN,FONT('Tahoma',8,,),VALUE('N')
                         END
                       END
                       ENTRY(@s20),AT(133,164,,10),USE(localText),HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(080FFFFH),REQ,UPR
                       PROMPT('thiswill change - the LocalTextPromt'),AT(8,164,120,12),USE(?LocalTextPrompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                       PANEL,AT(4,184,224,24),USE(?Panel1),FILL(0D6E7EFH)
                       BUTTON('OK'),AT(108,188,56,16),USE(?OK),FLAT,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(168,188,56,16),USE(?CancelButton),FLAT,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),ICON('cancel.gif'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Pop_Validate')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Panel2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?OK,RequestCancelled)
  Relate:IMEISHIP.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  !Set up the defaults
  
   thiswindow{PROP:TEXT}='Proof of Purchase Proceedure'
  
   !out of warranty
   ?WarrantyString{PROP:TEXT}='UNIT OUTSIDE WARRANTY PERIOD'
   localMemo = ''
   hide(?InsGroup)
   Hide(?popGroup)
   Unhide(?OutsideGroup)
   LocalType = ''
   LocalSubType=''
  
  thiswindow.update
  display()
  
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:IMEISHIP.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?LocalType
      !Change the display
      Case LocalType
        of 'P'   !Proof of purchase coming
          HIDE(?localtext)
          HIDE(?localtextprompt)
          !?localTextPrompt{Prop:Text}='Date of Purchase'
          unhide(?popGroup)
          Hide(?insGroup)
          LocalSubType=''
      
        of 'I'   !Insurance job
          unHIDE(?localtext)
          ?localTextPrompt{Prop:Text}='Insurance Reference Number'
          unHIDE(?localtextprompt)
          hide(?insgroup)
          hide(?popGroup)
          LocalSubType='I'
      
        of 'C'   !Chargeable job
          !LocalMemo = WebMemo   !Looked up at log in, specific to this trader
          unhide(?insgroup)
          HIDE(?localtext)
          HIDE(?localtextprompt)
          hide(?popGroup)
          LocalSubType='C'
      
      End !Case local type
      
      
      thiswindow.update
      display()
    OF ?LocalSubType
      !Change the display
      case LocalSubType
      
        of 'X'
      
          ?localTextPrompt{PROP:TEXT} = 'Please specify type of proof'
          unHIDE(?localtext)
          unHIDE(?localtextprompt)
      
        of 'N'
      
          ?localTextPrompt{PROP:TEXT} = 'Authority Number'
          unHIDE(?localtext)
          unHIDE(?localtextprompt)
      
      ENd!Case
      
      Hide(?insGroup)
      thiswindow.update
      display()
    OF ?OK
      !return the variables collected
      
      GLO:Select1 = LocalType & LocalSubType
      GLO:Select2 = CLip(localText)
      GLO:Select3 = localDate
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LocalType:Radio2
      unhide(?insGroup)
      ?LocalTextPrompt{Prop:TEXT}='Insurance Reference Number'
      localtext = ''
      UNHIDE(?localtext)
      UNHIDE(?localtextprompt)
      hide(?popGroup)
      thiswindow.update
      display()
    OF ?LocalType:Radio3
      unHide(?insGroup)
      localMemo = 'The unit will be repaired for a fixed charge of 39.99.||Or the unit can be upgraded for a charge of 49.00'
      HIDE(?localtext)
      HIDE(?localtextprompt)
      hide(?popGroup)
      thiswindow.update
      display()
    OF ?OK
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
POP_Validate2 PROCEDURE (f:Disable)                   !Generated from procedure template - Window

Tmp:YearCode         STRING(1)
tmp:MonthCode        STRING(1)
Tmp:Month            STRING(2)
Tmp:Year             STRING(4)
Tmp:MonthWrite       STRING(3)
tmp:Close            BYTE(0)
tmp:alreadyset       BYTE
tmp:OKPressed        BYTE(0)
SaveGroup            GROUP,PRE(save)
POP                  STRING(30)
                     END
window               WINDOW('Proof Of Purchase Procedure'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Proof Of Purchase'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Proof Of Purchase Procedure'),USE(?Tab1)
                           STRING(@s3),AT(391,158),USE(Tmp:MonthWrite),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           STRING(@s4),AT(411,158),USE(Tmp:Year),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Activation/D.O.P.'),AT(247,158),USE(?glo:Select2:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(323,158,,10),USE(GLO:Select2),LEFT(2),FONT(,,,FONT:bold),COLOR(COLOR:White)
                           OPTION('Job Type'),AT(247,170,184,76),USE(GLO:Select1),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO(' First Year Warranty'),AT(295,180),USE(?Option1:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('F')
                             RADIO(' Second Year Warranty'),AT(295,196),USE(?Option1:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('S')
                             RADIO(' Out Of Box Failure'),AT(295,212),USE(?GLO:Select1:Radio4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('O')
                             RADIO(' Chargeable'),AT(295,228),USE(?Option1:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('C')
                           END
                           PROMPT('POP Type'),AT(247,250),USE(?Prompt2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(323,250,56,10),USE(GLO:Select3),LEFT(2),FONT(,,010101H,FONT:bold),COLOR(COLOR:White),DROP(10),FROM('POP|BASTION|NONE')
                           PROMPT('Warranty Ref No'),AT(247,270),USE(?jobe2:WarrantyRefNo:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(323,270,124,10),USE(jobe2:WarrantyRefNo),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Warranty Ref No'),TIP('Warranty Ref No'),UPR
                         END
                       END
                       BUTTON,AT(380,332),USE(?OkButton),TRN,FLAT,LEFT,ICON('valpopp.jpg'),DEFAULT
                       BUTTON,AT(444,332),USE(?Button:Cancel),TRN,FLAT,HIDE,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:OKPressed)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020511'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('POP_Validate2')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:ACCAREAS.Open
  Relate:JOBS.Open
  Relate:MANUDATE.Open
  Access:MANUFACT.UseFile
  Access:JOBSE2.UseFile
  Access:MODELNUM.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  ! Inserting (DBH 26/04/2006) #7149 - Show Warranty Ref No
  Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
  jobe2:RefNumber = job:Ref_Number
  If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
      ! Found
  
  Else ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
      ! Error
  End ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
  ! End (DBH 26/04/2006) #7149
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      tmp:alreadyset = FALSE
      Access:MANUFACT.Clearkey(man:Manufacturer_Key)
      man:Manufacturer    = job:manufacturer
      If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
      ! Found
  
      Else! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
          ! Error
          ! Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
      IF job:manufacturer = 'MOTOROLA'
          tmp:YearCode  = Sub(job:MSN, 5, 1)
          tmp:MonthCode = Sub(job:MSN, 6, 1)
  
          Access:MANUDATE.Clearkey(mad:DateCodeKey)
          mad:Manufacturer = 'MOTOROLA'
          mad:DateCode     = Clip(tmp:YearCode) & Clip(tmp:MonthCode)
          If Access:MANUDATE.Tryfetch(mad:DateCodeKey) = Level:Benign
              ! Found
              tmp:Year  = mad:TheYear
              tmp:Month = mad:TheMonth
          End ! If
          CASE Tmp:Month
          OF 1
              Tmp:MonthWrite = 'JAN'
          OF 2
              Tmp:MonthWrite = 'FEB'
          OF 3
              Tmp:MonthWrite = 'MAR'
          OF 4
              Tmp:MonthWrite = 'APR'
          OF 5
              Tmp:MonthWrite = 'MAY'
          OF 6
              Tmp:MonthWrite = 'JUN'
          OF 7
              Tmp:MonthWrite = 'JUL'
          OF 8
              Tmp:MonthWrite = 'AUG'
          OF 9
              Tmp:MonthWrite = 'SEP'
          OF 10
              Tmp:MonthWrite = 'OCT'
          OF 11
              Tmp:MonthWrite = 'NOV'
          OF 12
              Tmp:MonthWrite = 'DEC'
          END ! CASE
          DISPLAY()
      END ! IF
      IF GLO:Select1 <> ''
          POST(Event:Accepted, ?Glo:Select1)
      END ! IF
      IF GLO:Select2 <> ''
          POST(Event:Accepted, ?Glo:Select2)
      END ! IF
      UPDATE()
      IF glo:select1 = 'F'
          tmp:alreadyset = TRUE
      END ! IF
  
      ! Inserting (DBH 02/10/2006) # 8275 - Only show the cancel button if DOP is already set, or it's a Chargeable Job
      If job:DOP <> '' Or glo:Select1 = 'C'
          ?Button:Cancel{prop:Hide} = False
      End ! If job:DOP <> ''
      ! End (DBH 02/10/2006) #8275
  
      ! Insert --- Don't allow to change the DOP if it has been disabled on the job screen (DBH: 26/02/2009) #10591
      if (f:Disable)
          ?glo:Select2{prop:Disable} = 1
      end ! if (f:Disable)
      ! end --- (DBH: 26/02/2009) #10591
  
  
      ! #11912 Save the POP to see if it's changed. (Bryan: 01/02/2011)
      save:POP = glo:Select3
  ?GLO:Select3{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?GLO:Select3{prop:vcr} = False
  ?GLO:Select3{prop:vcr} = False
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS.Close
    Relate:JOBS.Close
    Relate:MANUDATE.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?GLO:Select2
      If glo:Select2 > Today()
          Case Missive('Invalid Date.', 'ServiceBase 3g', |
                       'mstop.jpg', '/OK')
          Of 1 ! OK Button
          End ! Case Missive
          glo:Select2 = ''
          Select(?glo:Select2)
      
      Else ! glo:Select2 > Today()
          IF Glo:Select1 = '' AND  tmp:alreadyset = FALSE
              ! Work out if 2nd Year!
              ! IF Glo:Select1 <> 'C'  AND man:ValidateDateCode = TRUE
              IF GLO:Select2 < (job:date_booked - man:warranty_period)
                  ! Out of First Year!
                  IF GLO:Select2 < (job:date_booked - 730)
                      ! Out of 2nd!
                      Glo:Select1 = 'C'
                  ELSE
                      IF Glo:Select1 <> 'O'
                          GLO:Select1 = 'S'
                      END ! IF
                  END ! IF
              ELSE
                  Glo:Select1 = 'F'
              END ! IF
              ! END
              UPDATE()
              DISPLAY()
          END ! IF
      End ! glo:Select2 > Today()
      
      
      !Added by Paul 30/04/2010 - Log no 11344
      !check the manufacturer and handset for single year warranty
      If GLO:Select1 = 'S' then
          !check manufacturer
          ! DBH Consolidate into single routine
          If (IsOneYearWarranty(job:Manufacturer,job:Model_Number))
              glo:Select1 = 'C'
          end
      !    Access:Manufact.Clearkey(man:Manufacturer_Key)
      !    man:Manufacturer = job:Manufacturer
      !    If Access:Manufact.fetch(man:Manufacturer_Key) = level:benign then
      !        If man:OneYearWarrOnly = 'Y' then
      !            GLO:Select1 = 'C'
      !        End
      !    End
      !
      !    !check Handset
      !    Access:ModelNum.Clearkey(mod:Manufacturer_Key)
      !    mod:Manufacturer    = job:Manufacturer
      !    mod:Model_Number    = job:Model_Number
      !    If Access:ModelNum.Fetch(mod:Manufacturer_Key) = level:benign then
      !        If mod:OneYearWarrOnly = 'Y' then
      !            GLO:Select1 = 'C'
      !        End
      !    End
      
          update()
          Display()
      End
      
      
      
      
    OF ?OkButton
      IF GLO:Select1 = 'F'
        IF GLO:Select2 = '' AND tmp:alreadyset = FALSE
          SELECT(?GLO:Select2)
          CYCLE
        END
      END
      IF GLO:Select1 = 'S'
        IF GLO:Select2 = ''
          SELECT(?GLO:Select2)
          CYCLE
        END
      END
      IF GLO:Select1 = 'O'
        IF GLO:Select2 = ''
          SELECT(?GLO:Select2)
          CYCLE
        END
      END
      !Work out if 2nd Year!
      IF Glo:Select1 <> 'C'  AND man:ValidateDateCode = TRUE
        IF GLO:Select2 < (job:date_booked - man:warranty_period)
          !Out of First Year!
          IF GLO:Select2 < (job:date_booked - 730)
            !Out of 2nd!
          ELSE
            IF Glo:Select1 <> 'O'
              GLO:Select1 = 'S'
            END
          END
        END
      END
      
      If glo:Select1 = ''
          Case Missive('You have not selected an option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          CYCLE
      End !glo:Select1 = ''
      
      !IF TODAY()-job:dop
      
      ! Inserting (DBH 26/04/2006) #7149 - Save any changes
      Access:JOBSE2.Update()
      ! End (DBH 26/04/2006) #7149
      ! Inserting (DBH 02/10/2006) # 8275 - Pass back that OK was pressed instead of cancel
      tmp:OKPressed = 1
      ! End (DBH 02/10/2006) #8275
      tmp:Close = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020511'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020511'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020511'&'0')
      ***
    OF ?GLO:Select1
      If glo:Select1 = 'C'
          ?glo:Select2{prop:Req} = 0
      Else
          ?glo:Select2{prop:Req} = 1
      End !glo:select1 <> '' And glo:Select1 <> 'C'
      Bryan.CompFieldColour()
    OF ?GLO:Select3
      IF (glo:Select3 <> save:POP)
          ! #11912 Has the user got access to change the pop type (Bryan: 01/02/2011)
          Error# = 0
          Access:USERS.Clearkey(use:User_Code_Key)
          use:User_Code = InsertEngineerPassword()
          IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
              Access:ACCAREAS.Clearkey(acc:Access_Level_Key)
              acc:User_Level = use:User_Level
              acc:Access_Area = 'AMEND POP TYPE'
              If (Access:ACCAREAS.TryFetch(acc:Access_Level_Key) = Level:Benign)
      
              ELSE
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to amend this value.','ServiceBase Distribution',|
                                 'mstop.jpg','/&OK')
                  Of 1 ! &OK Button
                  End!Case Message
                  Error# = 1
              END
      
          ELSE
              If (use:User_Code <> '')
                  Case Missive('Incorrect password.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
              END
              Error# = 1
          END
          IF (Error# = 1)
              glo:Select3 = save:POP
          ELSE
              save:POP = glo:Select3
          END
          DISPLAY()
      END ! IF (glo:Select3 <> save:POP)
    OF ?OkButton
      ThisWindow.Update
       POST(Event:CloseWindow)
    OF ?Button:Cancel
      ThisWindow.Update
      ! Inserting (DBH 02/10/2006) # 8275 - Pass back that OK wasn't pressed
      tmp:OKPressed = 0
      tmp:Close = 1
      Post(Event:CloseWindow)
      ! End (DBH 02/10/2006) #8275
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
      If ~tmp:Close
          Cycle
      End !tmp:Close
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Update_Jobs PROCEDURE (pMultiple_Setup,f:NewJobBookingGroup) !Generated from procedure template - Window

CurrentTab           STRING(80)
tmp:HeadAccountNumber STRING(30)
save_jobsobf_id      USHORT,AUTO
save_manufact_id     USHORT,AUTO
save_accareas_id     USHORT,AUTO
save_users_id        USHORT,AUTO
save_job_id          USHORT,AUTO
save_jobs_id         USHORT,AUTO
save_exchange_id     USHORT,AUTO
save_jobse_alias_id  USHORT,AUTO
save_jobs_alias_id   USHORT,AUTO
SaveTransitType      STRING(30)
SaveEngineer         STRING(3)
save_moc_id          USHORT,AUTO
force_fault_codes_temp STRING(3)
tmp:printreceipt     BYTE(0)
tmp:OBF_Flag         BYTE(0)
tmp:retain           BYTE(0)
tmp:IMEIValid        STRING(3)
save_xch_id          USHORT,AUTO
sav:path             STRING(255)
save_cha_id          USHORT,AUTO
save_maf_id          USHORT,AUTO
Transit_Type_Temp    STRING(30)
print_label_temp     STRING('NO {1}')
label_type_temp      STRING(30)
print_job_card_temp  STRING('NO {1}')
sav:ref_number       LONG
sav:esn              STRING(20)
save_jac_id          USHORT,AUTO
location_temp        STRING(30)
save_ccp_id          USHORT,AUTO
save_cwp_id          USHORT,AUTO
Account_Number_Temp  STRING(15)
save_job_ali_id      USHORT
model_number_temp    STRING(30)
ESN_temp             STRING(16)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
left_title_temp      STRING(80)
right_title_temp     STRING(80)
accessories_temp     STRING(30)
save_taf_id          USHORT,AUTO
ESN_Entry_Temp       STRING(18)
tmp:SkillLevel       LONG
tmp:Print_JobCard    BYTE(0)
tmp:HUBRepair        BYTE(0)
tmp:Network          STRING(30)
WebCheckString       STRING(3)
VBAF::ViewMode       BYTE
tmp:OutOfBoxFailure  BYTE
tmp:Intial_Transit_Type STRING(30)
tmp:Trade_Account    STRING(15)
tmp:Force_Mobile_Number BYTE(0)
tmp:LastDate         DATE
tmp:Original_EmailAddress STRING(255)
tmp:DisableLocation  BYTE(0)
tmp:DisplayPOP       BYTE(0)
tmp:EndUserTelNo     STRING(15)
tmp:POPType          STRING('NONE {26}')
tmp:SaveFaultCode3   STRING(30)
tmp:SaveFaultCode7   STRING(30)
tmp:Booking48HourOption BYTE(0)
tmp:VSACustomer      BYTE(0)
tmp:OKPressed        BYTE(0)
ActivationDate       DATE
Notes                STRING(255)
msgActivationDate    STRING(255)
msgActivity          STRING(255)
msgWarranty          STRING(255)
msgBlackGrey         STRING(255)
msgSupplier          STRING(255)
msgSwap              STRING(255)
msgUsage             STRING(255)
msgStatusCode        STRING(255)
msgStatusDesc        STRING(255)
tmp:wobJobNumber     LONG
tmp:wobRefNumber     LONG
tmp:wobFaultCode13   STRING(30)
tmp:wobFaultCode14   STRING(30)
tmp:wobFaultCode15   STRING(30)
tmp:wobFaultCode16   STRING(30)
tmp:wobFaultCode17   STRING(30)
tmp:wobFaultCode18   STRING(30)
tmp:wobFaultCode19   STRING(30)
tmp:wobFaultCode20   STRING(30)
tmp:IMEIValidationStatus BYTE(0)
tmp:Contract         BYTE(0)
tmp:Prepaid          BYTE(0)
tmp:AuditNotes       STRING(255)
tmp:OverrideMSISDNCheck BYTE(0)
tmp:MobileValidated  BYTE(0)
tmp:MobileActivationDate DATE
tmp:MobilePhoneName  STRING(255)
tmp:MobileLoyaltyStatus STRING(255)
tmp:MobileUpgradeDate DATE
tmp:MobileAverageSpend STRING(20)
tmp:MobileLifetimeValue STRING(30)
tmp:MobileIDNumber   STRING(30)
tmp:48HourRefurb     BYTE(0)
OneYearWarrantyOnly  STRING(1)
locPOPConfirmed      BYTE
History::job:Record  LIKE(job:RECORD),STATIC
!! ** Bryan Harrison (c)1998 **
temp_string String(255)
QuickWindow          WINDOW('Update the JOBS File'),AT(0,0,680,429),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       MENUBAR
                         MENU('Options'),USE(?Options)
                           ITEM('Allocate Job'),USE(?OptionsAllocateJob)
                           ITEM('Estimate'),USE(?OptionsEstimate),DISABLE
                           ITEM,SEPARATOR
                           ITEM('Allocate/Return Loan'),USE(?OptionsAllocateLoan),DISABLE
                           ITEM('Allocate/Return Exchange'),USE(?OptionsAllocateExchange),DISABLE
                         END
                         MENU('Browse'),USE(?Browse)
                           ITEM('Audit Trail'),USE(?BrowseAuditTrail)
                           ITEM('Status Change'),USE(?BrowseStatusChange)
                           ITEM,SEPARATOR
                           ITEM('Contact History'),USE(?BrowseContactHistory)
                           ITEM('Engineer History'),USE(?BrowseEngineerHistory)
                           ITEM('Location History'),USE(?BrowseLocationHistory)
                         END
                         MENU('Defaults'),USE(?Defaults)
                           ITEM('General Defaults'),USE(?DefaultsGeneralDefaults)
                         END
                       END
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert Job'),AT(72,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,55,236,308),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           STRING(@s80),AT(68,84,208,12),USE(left_title_temp),LEFT,FONT(,10,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Transit Type'),AT(68,100),USE(?JOB:Transit_Type:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(140,100,124,10),USE(job:Transit_Type),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(DownKey),UPR,READONLY
                           BUTTON,AT(268,96),USE(?Lookup_Transit_Type),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('I.M.E.I. Number'),AT(68,114),USE(?JOB:ESN:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s20),AT(140,114,124,10),USE(job:ESN),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('M.S.N.'),AT(68,124),USE(?JOB:MSN:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s16),AT(140,124,124,10),USE(job:MSN),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Product Code'),AT(68,156),USE(?job:ProductCode:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(140,156,124,10),USE(job:ProductCode),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),ALRT(DownKey),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),UPR,READONLY
                           BUTTON,AT(268,151),USE(?LookupProductCode),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Model Number'),AT(68,138),USE(?JOB:Model_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(140,138,124,10),USE(job:Model_Number),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(DownKey),ALRT(MouseRight),REQ,UPR,READONLY
                           BUTTON,AT(268,132),USE(?Lookup_Model_Number),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Unit Type '),AT(68,175),USE(?JOB:Unit_Type:Prompt:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(140,175,124,10),USE(job:Unit_Type),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(DownKey),ALRT(MouseRight),UPR,READONLY
                           BUTTON,AT(268,172),USE(?Lookup_Unit_Type),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           ENTRY(@s30),AT(140,191,124,10),USE(job:Colour),HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(DownKey),ALRT(MouseRight),UPR,READONLY
                           BUTTON,AT(268,189),USE(?Lookup_Colour),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Network'),AT(68,211),USE(?tmp:Network:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(140,211,124,10),USE(tmp:Network),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Network'),TIP('Network'),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(DownKey),UPR,READONLY
                           BUTTON,AT(268,207),USE(?LookupNetwork),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           CHECK('Bypass MSISDN Check'),AT(140,227),USE(tmp:OverrideMSISDNCheck),DISABLE,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Override MSISDN Check'),TIP('Override MSISDN Check'),VALUE('1','0')
                           PROMPT('Mobile Number'),AT(68,242),USE(?JOB:Mobile_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s15),AT(140,242,124,10),USE(job:Mobile_Number),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           CHECK('Chargeable Job'),AT(68,258),USE(job:Chargeable_Job),RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           BUTTON,AT(268,252),USE(?lookup_Charge_Type),TRN,FLAT,ICON('lookupp.jpg')
                           ENTRY(@s30),AT(140,258,124,10),USE(job:Charge_Type),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           CHECK('Warranty Job'),AT(68,276),USE(job:Warranty_Job),RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           BUTTON,AT(268,273),USE(?Lookup_Warranty_Charge_Type),TRN,FLAT,ICON('lookupp.jpg')
                           ENTRY(@s30),AT(140,276,124,10),USE(job:Warranty_Charge_Type),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           CHECK('Intermittent Fault '),AT(140,291),USE(job:Intermittent_Fault),HIDE,LEFT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           ENTRY(@d6),AT(140,305,56,10),USE(job:DOP),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Skill Level'),AT(208,305),USE(?tmp:SkillLevel:Prompt),TRN,HIDE,FONT(,8,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           SPIN(@n8),AT(256,305,24,10),USE(tmp:SkillLevel),HIDE,RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Job Skill Level'),TIP('Job Skill Level'),UPR,RANGE(0,5),STEP(1)
                           PROMPT('Activation/D.O.P.'),AT(68,305),USE(?JOB:DOP:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(''),AT(164,318,132,16),USE(?bouncer_text),TRN,HIDE,FONT(,14,COLOR:Red,FONT:bold)
                           PROMPT('This job has a LOAN unit attached!'),AT(68,349),USE(?StrLoanWarning),TRN,HIDE,FONT(,,080FFFFH,,CHARSET:ANSI)
                           CHECK('POP Confirmed'),AT(68,320),USE(locPOPConfirmed),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1','0')
                           CHECK('Print Duplicate Job Card'),AT(308,242),USE(tmp:Print_JobCard),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Print Duplicate Job Card'),TIP('Print Duplicate Job Card'),VALUE('1','0')
                           PROMPT('Colour'),AT(68,191),USE(?job:Colour:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       SHEET,AT(304,55,312,200),USE(?Sheet3),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 3'),USE(?Tab3)
                           STRING(@s80),AT(308,84,224,12),USE(right_title_temp),RIGHT,FONT(,10,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Account Number'),AT(308,100),USE(?JOB:Account_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s15),AT(384,100,124,10),USE(job:Account_Number),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(080FFFFH),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(DownKey),ALRT(MouseRight),REQ,UPR,READONLY
                           BUTTON,AT(512,97),USE(?Lookup_Account_Number),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Order Number'),AT(308,116),USE(?JOB:Order_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(384,116,124,10),USE(job:Order_Number),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('End User Name'),AT(308,139),USE(?Customer_Name),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Title'),AT(384,131),USE(?job:Title:Prompt),TRN,HIDE,FONT(,7,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Init'),AT(412,131),USE(?job:Initial:Prompt),TRN,HIDE,FONT(,7,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           ENTRY(@s4),AT(384,139,24,10),USE(job:Title),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s1),AT(412,139,12,10),USE(job:Initial),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Surname'),AT(428,131),USE(?job:Surname:Prompt),TRN,HIDE,FONT(,7,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(428,139,80,10),USE(job:Surname),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(548,110),USE(?Button:ExternalDamageChecklist),TRN,FLAT,ICON('xdamagep.jpg')
                           PROMPT('End User Tel No'),AT(308,153),USE(?jobe:EndUserTelNo:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s15),AT(384,153,124,10),USE(tmp:EndUserTelNo),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Location'),AT(308,167),USE(?JOB:Location:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(384,167,124,10),USE(job:Location),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(DownKey),ALRT(MouseRight),UPR
                           PROMPT('Authority Number'),AT(308,181),USE(?JOB:Authority_Number:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(384,181,124,10),USE(job:Authority_Number),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           OPTION('Booking Option'),AT(310,196,226,34),USE(tmp:Booking48HourOption),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('48Hour Exchange'),AT(316,206),USE(?tmp:Booking48HourOption:Radio1),DISABLE,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('ARC Repair'),AT(408,206),USE(?tmp:Booking48HourOption:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                             RADIO('7 Day TAT'),AT(480,206),USE(?tmp:Booking48HourOption:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('3')
                             RADIO('Liquid Damage'),AT(316,218),USE(?tmp:Booking48HourOption:Radio4),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('4')
                           END
                           CHECK('VSA Customer'),AT(308,232),USE(tmp:VSACustomer),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('VSA Customer'),TIP('VSA Customer'),VALUE('1','0')
                           CHECK('HUB Repair'),AT(432,232),USE(tmp:HUBRepair),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('HUB Repair'),TIP('HUB Repair'),VALUE('1','0')
                           CHECK('Contract'),AT(431,242),USE(tmp:Contract),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Contract'),TIP('Contract'),VALUE('1','0')
                           CHECK('Prepaid'),AT(515,242),USE(tmp:Prepaid),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Prepaid'),TIP('Prepaid'),VALUE('1','0')
                         END
                       END
                       SHEET,AT(304,257,312,106),USE(?Sheet2),BELOW,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('Address Details'),USE(?InvoiceAddress),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Customer Address'),AT(308,261),USE(?Invoice_Address),FONT(,10,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Company Name'),AT(308,279),USE(?CompanyName:Prompt),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(548,277),USE(?Amend),IMM,TRN,FLAT,LEFT,ICON('editaddp.jpg')
                           PROMPT('Address'),AT(308,287),USE(?Address:prompt),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(384,279,100,12),USE(job:Company_Name),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(384,287,100,10),USE(job:Address_Line1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Postcode'),AT(308,311),USE(?Postcode:Prompt),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(384,295,100,10),USE(job:Address_Line2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(384,303,100,10),USE(job:Address_Line3),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s10),AT(384,311,100,10),USE(job:Postcode),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                         TAB('Collection Address'),USE(?CollectionAddress),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Collection Address'),AT(308,261),USE(?Invoice_Address:2),FONT(,10,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Company Name'),AT(308,279),USE(?CompanyName:Prompt:2),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(396,279,100,12),USE(job:Company_Name_Collection),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(548,277),USE(?Amend:2),TRN,FLAT,LEFT,ICON('editaddp.jpg')
                           PROMPT('Address'),AT(308,287),USE(?Address:prompt:2),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(396,287,100,10),USE(job:Address_Line1_Collection),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(396,295,100,10),USE(job:Address_Line2_Collection),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(396,303,100,10),USE(job:Address_Line3_Collection),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Postcode'),AT(308,311),USE(?Postcode:Prompt:2),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s10),AT(396,311,100,10),USE(job:Postcode_Collection),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                         TAB('Delivery Address'),USE(?DeliveryAddress),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Delivery Address'),AT(308,261),USE(?Invoice_Address:3),FONT(,10,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Company Name'),AT(308,279),USE(?CompanyName:Prompt:3),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(396,279,100,12),USE(job:Company_Name_Delivery),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(548,277),USE(?Amend:3),TRN,FLAT,LEFT,ICON('editaddp.jpg')
                           PROMPT('Address'),AT(308,287),USE(?Address:prompt:3),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(396,287,100,10),USE(job:Address_Line1_Delivery),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(396,295,100,10),USE(job:Address_Line2_Delivery),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(396,303,100,10),USE(job:Address_Line3_Delivery),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Postcode'),AT(308,311),USE(?Postcode:Prompt:3),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s10),AT(396,311,100,10),USE(job:Postcode_Delivery),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(548,83),USE(?Lookup_Accessory),TRN,FLAT,ICON('editaccp.jpg')
                       BUTTON,AT(548,141),USE(?Fault_Codes_Lookup),TRN,FLAT,ICON('amdfaup.jpg')
                       BUTTON,AT(548,171),USE(?Fault_Description),TRN,FLAT,ICON('faudescp.jpg')
                       BUTTON,AT(548,205),USE(?engineers_notes),TRN,FLAT,ICON('engnotp.jpg')
                       BUTTON,AT(68,366),USE(?Repair_History),FLAT,HIDE,LEFT,ICON('prevhisp.jpg')
                       BUTTON,AT(144,366),USE(?Validate_POP),TRN,FLAT,HIDE,LEFT,ICON('valpopp.jpg')
                       BUTTON,AT(480,366),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(548,366),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       TEXT,AT(244,370,192,56),USE(Notes),SKIP,HIDE,VSCROLL,CENTER,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),READONLY
                       STRING(''),AT(392,261,219,11),USE(?Completed),RIGHT,FONT('Tahoma',10,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
Multiple_Setup group,type
MS_Model            byte
MS_Colour           byte
MS_Order_No         byte
MS_Fault_Code       byte
MS_Engineers_Notes  byte
MS_Contact_History  byte
MS_First_Job_No     long
               end

grpResult group
grp_TimeStamp               string(255)
grp_IMEI_Number             string(255)
grp_T1_Supplier_MSG         string(255)
grp_T1_Pass                 byte
grp_T2_Activity_MSG         string(255)
grp_T2_Pass                 byte
grp_T3_Black_Greylist_MSG   string(255)
grp_T3_Pass                 byte
grp_T4_Usage_MSG            string(255)
grp_T4_Pass                 byte
grp_T5_Swapout_MSG          string(255)
grp_T5_Pass                 byte
grp_Final_MSG               string(255)
grp_Final_Pass              byte
grp_Act_Date                string(255)
          end

MultiJobBkGrp &Multiple_Setup   ! Reference to the above group
                                ! This way we can assign a group only if required.

ContHistQ queue
ContHistBuffer like(cht:Record) ! Record buffer for contact history
          end

JobAccQ queue
JobAccBuffer like(jac:Record)   ! Record buffer for job accessories
        end


! Insert --- Book new job with details of 48 Hour Exchange (DBH: 11/03/2009) #10473
NewJobBookingGroup    &NewJobBooking
! end --- (DBH: 11/03/2009) #10473


local         Class
ValidateIMEINumber    Procedure(Long  func:Bouncers),Byte
CopyDOPFromBouncer    Procedure(Date  f:PreviousDOP)
              End
TempFilePath         CSTRING(255)
!MQ Setup
mqFileName  string(260), static

! Input / Output files - pipe delimited (|)
MQIInputFile file, driver('BASIC','/COMMA=124 /ALWAYSQUOTE=OFF'), pre(MQIF), name(mqFileName), create
MQIFRec         record
imei                string(30)
                end ! record
            end ! file

MQIOutputFile file, driver('BASIC','/COMMA=124 /ALWAYSQUOTE=OFF'), pre(MQOF), name(mqFileName)
MQOFRec         record
activationDate      string(255)
activity            string(255)
warranty            string(255)
blackGrey           string(255)
supplier            string(255)
swap                string(255)
usage               string(255)
statusCode          string(255)
statusDescription   string(255)
                end ! record
            end ! file



!Save Entry Fields Incase Of Lookup
look:job:Transit_Type                Like(job:Transit_Type)
look:job:ProductCode                Like(job:ProductCode)
look:job:Model_Number                Like(job:Model_Number)
look:job:Unit_Type                Like(job:Unit_Type)
look:job:Colour                Like(job:Colour)
look:tmp:Network                Like(tmp:Network)
look:job:Account_Number                Like(job:Account_Number)
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

!A
AddLoanUnit     Routine
Data
local:LoanUnitNumber        Long()
Code
    !Despatch info is helding WEBJOBS, so create a record, just incase - 3871 (DBH: 12-02-2004)

    If ThisWindow.Request = InsertRecord
        local:LoanUnitNumber = job:Loan_Unit_Number
    End !ThisWindow.Request = InsertRecord

    Do PutWebJob

    ViewLoanUnit

    Do GetWebJob
    !update the jobs file in case any change made
    if ?OK{prop:hide}=true then
        access:jobs.update()
    END

    If ThisWindow.Request = InsertRecord
        If local:LoanUnitNumber <> job:Loan_Unit_Number
            !Something changed -  (DBH: 11-02-2004)
            If job:Loan_Unit_Number = 0
                !Do RemoveWebJob
            Else !If job:Loan_Unit_Number = 0
                ?Cancel{prop:Disable} = 1
            End !If job:Loan_Unit_Number = 0
        Else !If local:LoanUnitNumber <> job:Loan_Unit_Number
            !Nothing changed. Remove the webjob - 3871 (DBH: 12-02-2004)
            !Do RemoveWebJob
        End !If local:LoanUnitNumber <> job:Loan_Unit_Number
    End !ThisWinow.Request = InsertRecord
Allow48Hour     Routine
    If ?tmp:Booking48HourOption{prop:Hide} = 0
        If Allow48Hour(job:ESN,job:Model_Number,wob:HeadAccountNumber) = True
            ?tmp:Booking48HourOption:Radio1{prop:Disable} = 0
        Else !If Allow48Hour(job:ESN,job:Model_Number) = True
            ?tmp:Booking48HourOption:Radio1{prop:Disable} = 1
            !Reset the Booking Option incase it has already
            !been selected -  (DBH: 15-10-2003)
            If ThisWindow.Request = InsertRecord
                If tmp:Booking48HourOption = 1
                    tmp:Booking48HourOption = 0
                End !If tmp:Booking48HourOption = 1
            End !If ThisWindow.Request = InsertRecord
        End !If Allow48Hour(job:ESN,job:Model_Number) = True
    End !If ?tmp:Booking48HourOption{prop:Hide} = 0
AllocationsReport      routine
 if SaveEngineer<>Job:engineer then
    !engineer was changed - print the allocations report
    Print# = 0
    If GETINI('RAPIDSTATUS','AutoPrint',,CLIP(PATH())&'\SB2KDEF.INI') = 1
        Print# = 1
    Else !If GETINI('RAPIDSTOCK','AutoPrint',,CLIP(PATH())&'\SB2KDEF.INI') = 1
        Case Missive('Do you wish to print an allocation report of the job?','ServiceBase 3g',|
                       'mquest.jpg','\No|/Yes')
            Of 2 ! Yes Button
                Print# = 1
            Of 1 ! No Button
        End ! Case Missive

    End !If GETINI('RAPIDSTOCK','AutoPrint',,CLIP(PATH())&'\SB2KDEF.INI') = 1

    If Print# = 1
        Free(glo:Queue)
        Clear(glo:Queue)
        glo:pointer  = job:Ref_Number
        Add(glo:Queue)

        !Save the job information, otherwise it'll get lost
        !while printing the report - 4485 (DBH: 19-07-2004)
        save_JOBS_id = Access:JOBS.SaveFile()

        If GETINI('RAPIDSTATUS','ReportType',,CLIP(PATH())&'\SB2KDEF.INI') = 1
            RapidStatusAllocationsReport(job:engineer,GETINI('RAPIDSTATUS','Copies',,CLIP(PATH())&'\SB2KDEF.INI'),'RS')
        Else !If GETINI('RAPIDSTOCK','ReportType',,CLIP(PATH())&'\SB2KDEF.INI') = 1
            Rapid_Status_Update_Report(GETINI('RAPIDSTATUS','Copies',,CLIP(PATH())&'\SB2KDEF.INI'))
        End !If GETINI('RAPIDSTOCK','ReportType',,CLIP(PATH())&'\SB2KDEF.INI') = 1
        Free(glo:Queue)

        Access:JOBS.RestoreFile(save_JOBS_id)

    End !If Print# = 1
 END
!B
Bastion:AuditEntry routine
    ! Add entry to audit trail
    If AddToAudit(job:Ref_Number,'JOB','BASTION WARRANTY CHECK',clip(grpResult.grp_T1_Supplier_MSG) & '<13,10>' & |
                                                               clip(grpResult.grp_T2_Activity_MSG) & '<13,10>' & |
                                                               clip(grpResult.grp_T3_Black_Greylist_MSG) & '<13,10>' & |
                                                               clip(grpResult.grp_T4_Usage_MSG) & '<13,10>' & |
                                                               clip(grpResult.grp_T5_Swapout_MSG) & '<13,10>' & |
                                                               clip(grpResult.grp_Final_MSG))
    End ! If AddToAudit(job:Ref_Number,'JOB','BASTION WARRANTY CHECK',clip(grpResult.grp_T1_Supplier_MSG) & '<13,10>' & |
BastionCheck routine
    data
tmp:UseBastionWarrantyChecking byte
    code
    tmp:UseBastionWarrantyChecking = GETINI('BASTION','EnableChecking',,CLIP(PATH())&'\SB2KDEF.INI')
    if tmp:UseBastionWarrantyChecking = 0 then exit.

    if GETINI('SoapClient','WSDL') = '' then exit.          ! URL
    if GETINI('SoapClient','UserID') = '' then exit.        ! User
    if GETINI('SoapClient','Password') = '' then exit.      ! Password

    clear(grpResult)
    if not BastionInterface(job:ESN, job:Ref_Number, address(grpResult))
        ! success
        !grpResult.grp_TimeStamp
        !grpResult.grp_IMEI_Number

        do Bastion:AuditEntry   ! Add audit trail entry

        if grpResult.grp_T3_Pass = true     ! Is this IMEI black/greylisted
            Case Missive('This unit was found to be blacklisted / greylisted.'&|
              '<13,10>'&|
              '<13,10>This unit cannot be repaired under warranty.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            job:Warranty_Job = 'NO'
            Post(Event:Accepted,?job:Warranty_Job)
            job:Chargeable_Job = 'YES'
            Post(Event:Accepted,?job:Chargeable_Job)
            job:Warranty_Charge_Type = ''
            DISABLE(?Job:Warranty_Job)
            ENABLE(?Job:Chargeable_Job)
            !Post(Event:Accepted,?job:Warranty_Charge_Type)
            !message('PaulMessage 1')
            job:Charge_Type = 'NON-WARRANTY'
            !Post(Event:Accepted,?job:Charge_Type)
            UPDATE()
            DISPLAY()
            exit
        end

        if grpResult.grp_T1_Pass = false    ! Supplier is not VSPC, call Proof Of Purchase
            if tmp:DisplayPOP = true
                do Validate_Motorola        ! Call POP routine
                tmp:DisplayPOP = false
                exit
            else
                tmp:DisplayPOP = true       ! Set variable to display POP on exit (prevents the POP routine from
                exit                        !                                      being called twice)
            end
        end

        ! Activity
        if grpResult.grp_T2_Pass = true     ! Has the handset been active?
            Access:MANUFACT.Clearkey(man:Manufacturer_Key)
            man:Manufacturer = job:manufacturer
            if not Access:MANUFACT.Fetch(man:Manufacturer_Key)
                if grpResult.grp_Act_Date < (job:date_booked - man:warranty_period) ! Check warranty period
                    ! Out of First Year
                else    ! Within warranty period
                    job:Warranty_Job = 'YES'
                    Post(Event:Accepted,?job:Warranty_Job)
                    job:Warranty_Charge_Type = 'WARRANTY (MFTR)'
                    !Post(Event:Accepted,?job:Warranty_Charge_Type)
                    job:Chargeable_Job = 'NO'
                    Post(Event:Accepted,?job:Chargeable_Job)
                    job:Charge_Type = ''
                    ENABLE(?Job:Chargeable_Job)
                    ENABLE(?Job:Warranty_Job)
                    Job:DOP = grpResult.grp_Act_Date
                    Post(Event:Accepted,?Job:DOP)
                    UPDATE()
                    DISPLAY()
                    exit
                end
            end
        end

        ! Usage (2nd year warranty check)
        if grpResult.grp_T4_Pass = true ! Is Usage more than 30 days in the last 90 ?
            job:Warranty_Job = 'YES'
            Post(Event:Accepted,?job:Warranty_Job)
            job:Warranty_Charge_Type = 'WARRANTY (2ND YR)'
            !Post(Event:Accepted,?job:Warranty_Charge_Type)
            job:Chargeable_Job = 'NO'
            Post(Event:Accepted,?job:Chargeable_Job)
            job:Charge_Type = ''
            ENABLE(?Job:Chargeable_Job)
            ENABLE(?Job:Warranty_Job)
            Job:DOP = Glo:Select2
            Post(Event:Accepted,?Job:DOP)
            UPDATE()
            DISPLAY()
        else    ! Chargeable job
            Case Missive('Bastion Warranty Checking:'&|
              '<13,10>This unit cannot be repaired under warranty.'&|
              '<13,10>Usage is less than 30 of the last 90 days.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            job:Warranty_Job = 'NO'
            Post(Event:Accepted,?job:Warranty_Job)
            job:Chargeable_Job = 'YES'
            Post(Event:Accepted,?job:Chargeable_Job)
            job:Warranty_Charge_Type = ''
            DISABLE(?Job:Warranty_Job)
            ENABLE(?Job:Chargeable_Job)
            !Post(Event:Accepted,?job:Warranty_Charge_Type)
            !message('PaulMessage 2')
            job:Charge_Type = 'NON-WARRANTY'
            !Post(Event:Accepted,?job:Charge_Type)
            UPDATE()
            DISPLAY()
        end

        ! Swapout - No need to check this, if the unit is a swap then the details from the original IMEI are returned
        !           from the Web Service instead.(i.e grpResult.grp_T2_Pass ~ Activity  - will reflect original handset)
        !if grpResult.grp_T5_Pass = true ! Is the handset a swap?
            !grpResult.grp_T5_Swapout_MSG
        !end

    end
BookFrom48HourExchange      Routine

    if (f:NewJobBookingGroup = 0)
        exit
    end ! if (f:NewJobBookingGroup = 0)

    NewJobBookingGroup &= (f:NewJobBookingGroup)

    job:ESN = NewJobBookingGroup.IMEINumber
    job:MSN = NewJobBookingGroup.MSN
    job:Model_Number = NewJobBookingGroup.ModelNumber
    job:ProductCode = NewJobBookingGroup.ProductCode
    job:Unit_Type = NewJobBookingGroup.UnitType
    job:Colour = NewJobBookingGroup.Colour
    tmp:Network = NewJobBookingGroup.Network
    ! This should override the default (DBH: 11/03/2009) #10473
    tmp:Trade_Account = NewJobBookingGroup.AccountNumber

    IF (NewJobBookingGroup.Type = 'RTN')
        ! #12151 Booking from a Returned Exchange Unit. (Bryan: 26/09/2011)
        IF (AddToAudit(job:Ref_Number,'JOB','EXCHANGE RETURN REJECTION BOOKING',''))
        END ! if (AddToAudit(job:Ref_Number,'JOB','UNIT REBOOKED AS REFURB','')
        job:DOP = NewJobBookingGroup.ActivationDate
    ELSE
        ! Keep track that this job will be returned to Exchange Stock (DBH: 11/03/2009) #10473
        if (NewJobBookingGroup.Type = '48H')
            tmp:48HourRefurb = 1 !Return to 48 Stock When Completed

            job:Company_Name = '48HR REFURB'
            job:Company_Name_Delivery = '48HR REFURB'
            ?cancel{prop:Disable} = 1
        elsif (NewJobBookingGroup.Type = '3RD')
            tmp:48HourRefurb = 2 ! ??? Third Party Repair
        end ! if (NewJobBookingGroup.Type = '48HR')


        tmp:Booking48HourOption = 2

        if (AddToAudit(job:Ref_Number,'JOB','UNIT REBOOKED AS REFURB','ORIGINAL JOB NUMBER: ' & NewJobBookingGroup.JobNumber))
        end ! if (AddToAudit(job:Ref_Number,'JOB','UNIT REBOOKED AS REFURB','')
    END
    Post(Event:Accepted,?job:ESN)
Bouncer_Text        Routine
    If job:bouncer = 'B'
        Unhide(?bouncer_text)
        !?bouncer_text{prop:text} = 'BOUNCER'
        !Unhide(?bouncerPanel)
        UnHide(?repair_history)
    Else!If job:bouncer = 'B'
        Hide(?bouncer_text)
        !hide(?BouncerPanel)
        Hide(?repair_history)
    End!If job:bouncer = 'B'

 if glo:webjob then
    thiswindow.update()
    display()
 END

!C
check_force_fault_codes    Routine
    force_fault_codes_temp = 'NO'
    If job:chargeable_job = 'YES'
        required# = 0
        access:chartype.clearkey(cha:charge_type_key)
        cha:charge_type = job:charge_type
        If access:chartype.fetch(cha:charge_type_key) = Level:Benign
            If cha:force_warranty = 'YES'
                force_fault_codes_temp = 'YES'
            End
        end !if
    End!If job:chargeable = 'YES'
    If job:warranty_job = 'YES' And force_fault_codes_temp <> 'YES'
        access:chartype.clearkey(cha:charge_type_key)
        cha:charge_type = job:warranty_charge_type
        If access:chartype.fetch(cha:charge_type_key) = Level:Benign
            If cha:force_warranty = 'YES'
                force_fault_codes_temp = 'YES'
            End
        end !if
    End!If job:warranty_job = 'YES'
Check_Msn       Routine   !Does Manufacturer Use MSN Number

    If MSNRequired(job:Manufacturer) = Level:Benign
        ?job:MSN{prop:Hide} = 0
        ?job:MSN:Prompt{prop:Hide} = 0
        IF job:MSN = ''
          SELECT(?job:MSN)
        END
    Else !If MSNRequired(job:Manufacturer) = Level:Benign
        ?job:MSN{prop:Hide} = 1
        ?job:MSN:Prompt{prop:Hide} = 1
    End !If MSNRequired(job:Manufacturer) = Level:Benign

    Case ProductCodeRequired(job:Manufacturer)
        of Level:Benign
            ?job:ProductCode{prop:Hide} = 0
            ?job:ProductCode:Prompt{prop:Hide} = 0
            ?LookupProductCode{prop:Hide} = 0
            ?job:ProductCode{prop:req} = 0
            ?job:productCode{prop:Background} = color:white
        of Level:notify
            ?job:ProductCode{prop:Hide} = 0
            ?job:ProductCode:Prompt{prop:Hide} = 0
            ?LookupProductCode{prop:Hide} = 0
            ?job:ProductCode{prop:req} = 1
            ?job:productCode{prop:Background} = 080FFFFH
        ELSE
            ?job:ProductCode{prop:Hide} = 1
            ?job:ProductCode:Prompt{prop:Hide} = 1
            ?LookupProductCode{prop:Hide} = 1
            ?job:ProductCode{prop:req} = 0
            ?job:productCode{prop:Background} = color:white
    End !If ProductCodeRequired(job:Manufacturer) = Level:Benign
CheckRequiredFields     Routine
    If ThisWindow.Request <> ViewRecord
        Set(DEFAULTS)
        Access:DEFAULTS.Next()

        If ForceTransitType('B')
            ?job:Transit_Type{prop:Req} = 1
            ?job:Transit_Type{prop:BackGround} = 080FFFFH
        Else !If ForceTransitType('B')
            ?job:Transit_Type{prop:Req} = 0
            ?job:Transit_Type{prop:background} = color:White
        End !If ForceTransitType('B')

        If ForceIMEI('B')
            ?job:ESN{prop:Req} = 1
            ?job:ESN{prop:BackGround} = 080FFFFH
        Else !If ForceIMEI('B')
            ?job:ESN{prop:Req} = 0
            ?job:ESN{prop:background} = color:White
        End !If ForceIMEI('B')

        If ForceMSN(job:Manufacturer,'B')
            ?job:MSN{prop:Req} = 1
            ?job:MSN{prop:BackGround} = 080FFFFH
        Else !If ForceMSN('B')
            ?job:MSN{prop:Req} = 0
            ?job:MSN{prop:background} = color:White
        End !If ForceMSN('B')

        If ForceModelNumber('B')
            ?job:Model_Number{prop:Req} = 1
            ?job:Model_Number{prop:BackGround} = 080FFFFH
        Else !If ForceModelNumber('B')
            ?job:Model_Number{prop:Req} = 0
            ?job:Model_Number{prop:background} = color:White
        End !If ForceModelNumber('B')

        If ForceUnitType('B')
            ?job:Unit_Type{prop:Req} = 1
            ?job:Unit_Type{prop:BackGround} = 080FFFFH
        Else !If ForceUnitType{'B')
            ?job:Unit_Type{prop:Req} = 0
            ?job:Unit_Type{prop:background} = color:White
        End !If ForceUnitType{'B')

        If ForceColour('B')
            ?job:Colour{prop:Req} = 1
            ?job:Colour{prop:BackGround} = 080FFFFH
        Else !If ForceColour('B')
            ?job:Colour{prop:Req} = 0
            ?job:Colour{prop:background} = color:White
        End !If ForceColour('B')

        If ForceNetwork('B')
            ?tmp:Network{prop:Req} = 1
            ?tmp:Network{prop:BackGround} = 080FFFFH
        Else !If ForceColour('B')
            ?tmp:Network{prop:Req} = 0
            ?tmp:Network{prop:background} = color:White
        End !If ForceColour('B')

        If ForceDOP(job:Transit_Type,job:Manufacturer,job:Warranty_Job,'B')
            ?job:DOP{prop:Req} = 1
            ?job:DOP{prop:BackGround} = 080FFFFH
        Else !If ForceDOP(job:Transit_Type,job:Manufacturer,job:Warranty_Job,'B')
            if job:pop = 'F' or job:pop = 'S' or job:pop = 'O' then
                ?job:DOP{prop:Req} = 1
                ?job:DOP{prop:BackGround} = 080FFFFH
            ELSE
                ?job:DOP{prop:Req} = 0
                ?job:DOP{prop:background} = color:White

                !if job:pop = 'C' or ''
            END
        End !If ForceDOP(job:Transit_Type,job:Manufacturer,job:Warranty_Job,'B')

        ! Am using an existing default, but the wrong way round. True=False and vice versa - TrkBs: 6141 (DBH: 23-08-2005)

        tmp:Force_Mobile_Number = True
        If ForceMobileNumber('B') Or GETINI(tmp:HeadAccountNumber,'ForceMobileNo',,CLIP(PATH())&'\SB2KDEF.INI') = 1
            tmp:Force_Mobile_Number = False
            Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
            sub:Account_Number  = job:Account_Number
            If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                !Found
                Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                tra:Account_Number  = sub:Main_Account_Number
                If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    !Found
                    ! Does the account NOT need to have the Mobile No Forced? - TrkBs: 6141 (DBH: 23-08-2005)
                    If sub:OverrideHeadMobile
                        tmp:Force_Mobile_Number = sub:OverrideMobileDefault
                    Else ! If sub:OverrideHeadMobile
                        tmp:Force_Mobile_Number = tra:OverrideMobileDefault
                    End ! If sub:OverrideHeadMobile
                    ! End   - Does the account NOT need to have the Mobile No Forced? - TrkBs: 6141 (DBH: 23-08-2005)
                Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    !Error
                End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                !Error
            End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        End ! If ForceMobileNumber('B')

        If tmp:Force_Mobile_Number = False
            ?job:Mobile_Number{prop:Req} = 1
            ?job:Mobile_Number{prop:BackGround} = 080FFFFH
        Else ! If tmp:ForceMobileNumber = False
            ?job:Mobile_Number{prop:Req} = 0
            ?job:Mobile_Number{prop:background} = color:White
        End ! If tmp:ForceMobileNumber = False

        If ForceLocation(job:Transit_Type,job:Workshop,'B')
            ?job:Location{prop:Req} = 1
            ?job:Location{prop:BackGround} = 080FFFFH
        Else !If ForceLocation(job:Transit_Type,job:Workshop,'B')
            ?job:Location{prop:Req} = 0
            ?job:Location{prop:background} = color:White
        End !If ForceLocation(job:Transit_Type,job:Workshop,'B')

        If ForceAuthorityNumber('B')
            ?job:Authority_Number{prop:Req} = 1
            ?job:Authority_Number{prop:BackGround} = 080FFFFH
        Else !If ForceAuthorityNumber('B')
            ?job:Authority_Number{prop:Req} = 0
            ?job:Authority_Number{prop:background} = color:White
        End !If ForceAuthorityNumber('B')

        If ForceOrderNumber(job:Account_Number,'B')
            ?job:Order_Number{prop:Req} = 1
            ?job:Order_Number{prop:BackGround} = 080FFFFH
        Else !If ForceOrderNumber(job:Account_Number,'B')
            ?job:Order_Number{prop:Req} = 0
            ?job:Order_Number{prop:background} = color:White
        End !If ForceOrderNumber(job:Account_Number,'B')

        !TB13307 - J - 01/07/14 - now have three cases for show, force and hide
        Case ForceCustomerName(job:Account_Number,'B')
        of 1
            !force
            Do ForceCustomerFields
        of 2
            !show
            Do ShowCustomerFields
        of 0
            !hide
            Do HideCustomerFields
        END

!        If ForceCustomerName(job:Account_Number,'B')
!            ?job:Surname{prop:Req} = 1
!            ?job:Surname{prop:BackGround} = 080FFFFH
!        Else !If ForceCustomerName(job:Account_Number,'B')
!            ?job:Surname{prop:Req} = 0
!            ?job:Surname{prop:background} = color:White
!        End !If ForceCustomerName(job:Account_Number,'B')

        !Incoming Courier is not used by Vodacom -  (DBH: 14-10-2003)
!        If ForceIncomingCourier('B')
!            ?job:Incoming_Courier{prop:Req} = 1
!            ?job:Incoming_Consignment_Number{prop:Req} = 1
!            ?job:Incoming_Date{prop:Req} = 1
!        Else !If ForceIncomingCourier('B')
!            ?job:Incoming_Courier{prop:Req} = 0
!            ?job:Incoming_Consignment_Number{prop:Req} = 0
!            ?job:Incoming_Date{prop:Req} = 0
!        End !If ForceIncomingCourier('B')

        Required# = 0
        If job:Chargeable_Job = 'YES'
            Access:CHARTYPE.ClearKey(cha:Warranty_Key)
            cha:Warranty    = 'NO'
            cha:Charge_Type = job:Charge_Type
            If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
                !Found
                If cha:ForceAuthorisation
                    Required# = 1
                End !If cha:ForceAuthorisation
            Else!If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
        End !If job:Chargeable_Job = 'YES'

        If job:Warranty_Job = 'YES' And Required# = 0
            Access:CHARTYPE.ClearKey(cha:Warranty_Key)
            cha:Warranty    = 'YES'
            cha:Charge_Type = job:Warranty_Charge_Type
            If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
                !Found
                If cha:ForceAuthorisation
                    Required# = 1
                End !If cha:ForceAuthorisation
            Else!If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
        End !If job:Warranty_Job = 'YES' And Required# = 0

        If Required#
            ?job:Authority_Number{prop:Hide} = 0
            ?job:Authority_Number:Prompt{prop:Hide} = 0
            ?job:Authority_Number{prop:Req} = 1
            ?job:Authority_Number{prop:BackGround} = 080FFFFH
        Else !If Required#
            If def:hide_authority_number = 'YES'
                ?job:Authority_Number{prop:Hide} = 1
                ?job:Authority_Number:Prompt{prop:Hide} = 1
            End !If def:hide_authority_number = 'YES'
            ?job:Authority_Number{prop:Req} = 0
            ?job:Authority_Number{prop:background} = color:White
        End !If Required#

        !Bryan.CompFieldColour()

    End !If ThisWindow.Request <> ViewRecord

    ! --- Only show options when required details are set ----
    ! Inserting:  DBH 27/01/2009 #10659
    If job:Model_Number <> '' And job:Account_Number <> ''
        ?OptionsAllocateLoan{prop:Disable} = 0
        ?OptionsAllocateExchange{prop:Disable} = 0

    End ! If job:Model_Number <> ''
    ! End: DBH 27/01/2009 #10659
    ! -----------------------------------------

Clear_Job_Fields routine
    ! After a job has been replicated from a previous job record, reset fields for
    ! a new job booking
    !message('In clear_job_fields')

    job:Batch_Number = 0
    job:Internal_Status = ''
    job:Auto_Search = ''
    job:date_booked = today()
    job:time_booked = clock()

    Access:USERS.ClearKey(use:password_key)
    use:password = glo:password
    Access:USERS.Fetch(use:password_key)
    job:who_booked = use:user_code

    job:Transit_Type = ''
    job:Location = GETINI('RRC','RRCLocation',,CLIP(PATH())&'\SB2KDEF.INI')
    job:Loan_Status = '101 NOT ISSUED'
    job:Exchange_Status = '101 NOT ISSUED'
    GetStatus(sub(GETINI('RRC','StatusSendToARC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3),0,'JOB')
    job:In_Repair = 'NO'
    job:Date_In_Repair = ''
    job:Time_In_Repair = ''
    job:On_Test = 'NO'
    job:Date_On_Test = ''
    job:Time_On_Test = ''
    job:QA_Passed = 'NO'
    job:Date_QA_Passed = ''
    job:Time_QA_Passed = ''
    job:QA_Rejected = 'NO'
    job:Date_QA_Rejected = ''
    job:Time_QA_Rejected = ''
    job:QA_Second_Passed = 'NO'
    job:Date_QA_Second_Passed = ''
    job:Time_QA_Second_Passed = ''
    job:Completed = 'NO'
    job:Date_Completed = ''
    job:Time_Completed = ''
    job:Paid = 'NO'
    job:Paid_Warranty = 'NO'
    job:Date_Paid = ''
    job:Courier_Cost = 0
    job:Advance_Payment = 0
    job:Labour_Cost = 0
    job:Parts_Cost = 0
    job:Sub_Total = 0
    job:Courier_Cost_Estimate = 0
    job:Labour_Cost_Estimate = 0
    job:Parts_Cost_Estimate = 0
    job:Sub_Total_Estimate = 0
    job:Courier_Cost_Warranty = 0
    job:Labour_Cost_Warranty = 0
    job:Parts_Cost_Warranty = 0
    job:Sub_Total_Warranty = 0
    job:Loan_Issued_Date = ''
    job:Loan_Unit_Number = 0
    job:Loan_accessory = ''
    job:Loan_User = ''
    job:Loan_Courier = ''
    job:Loan_Consignment_Number = ''
    job:Loan_Despatched = ''
    job:Loan_Despatched_User = ''
    job:Loan_Despatch_Number = 0
    job:Exchange_Unit_Number = 0
    job:Exchange_Authorised = 'NO'
    job:Loan_Authorised = 'NO'
    job:Exchange_Accessory = ''
    job:Exchange_Issued_Date = ''
    job:Exchange_User = ''
    job:Exchange_Courier = ''
    job:Exchange_Consignment_Number = ''
    job:Exchange_Despatched = ''
    job:Exchange_Despatched_User = ''
    job:Exchange_Despatch_Number = 0
    job:Date_Despatched = ''
    job:Despatch_Number = ''
    job:Despatch_User = ''
    job:Consignment_Number = ''
    job:Incoming_Courier = ''
    job:Incoming_Consignment_Number = ''
    job:Estimate = 'NO'
    job:Estimate_Accepted = 'NO'
    job:Estimate_Rejected = 'NO'
    job:ThirdPartyDateDesp = ''
    job:Third_Party_Printed = 'NO'
    job:PreviousStatus = ''
    job:StatusUser = ''
    job:Status_End_Date = ''
    job:Status_End_Time = ''
    job:Turnaround_End_Date = ''
    job:Turnaround_End_Time = ''
    job:Turnaround_Time = ''
    job:EDI = 'XXX'
    job:EDI_Batch_Number = 0
    job:Invoice_Exception = ''
    job:Invoice_Failure_Reason = ''
    job:Invoice_Number = 0
    job:Invoice_Date = ''
    job:Invoice_Date_Warranty = ''
    job:Invoice_Courier_Cost = 0
    job:Invoice_Labour_Cost = 0
    job:Invoice_Parts_Cost = 0
    job:Invoice_Sub_Total = 0
    job:Invoice_Number_Warranty = 0
    job:WInvoice_Courier_Cost = 0
    job:WInvoice_Labour_Cost = 0
    job:WInvoice_Parts_Cost = 0
    job:WInvoice_Sub_Total = 0
    job:Despatched = 'NO'
    job:Cancelled = 'NO'
    job:Incoming_Date = today()
Clear_JobSE_Fields routine
    ! After a job has been replicated from a previous job record, reset fields for
    ! a new job booking
    !message('In the clearing routine')

    jobe:JobMark = 0
    jobe:JobReceived = 0
    jobe:FailedDelivery = 0
    jobe:CConfirmSecondEntry = ''
    jobe:WConfirmSecondEntry = ''
    jobe:HubRepair = 1
    jobe:POPConfirmed = 0
    jobe:HubRepairDate = ''
    jobe:HubRepairTime = ''
    jobe:ClaimValue = 0
    jobe:HandlingFee = 0
    jobe:ExchangeRate = 0
    jobe:InvoiceClaimValue = 0
    jobe:InvoiceHandlingFee = 0
    jobe:InvoiceExchangeRate = 0
    jobe:LabourAdjustment = 0
    jobe:PartsAdjustment = 0
    jobe:SubTotalAdjustment = 0
    jobe:IgnoreClaimCosts = 0
    jobe:RRCCLabourCost = 0
    jobe:RRCCPartsCost = 0
    jobe:RRCCPartsSale = 0
    jobe:RRCCSubTotal = 0
    jobe:InvRRCCLabourCost = 0
    jobe:InvRRCCPartsCost = 0
    jobe:InvRRCCPartsSale = 0
    jobe:InvRRCCSubTotal = 0
    jobe:RRCWLabourCost = 0
    jobe:RRCWPartsCost = 0
    jobe:RRCWPartsSale = 0
    jobe:RRCWSubTotal = 0
    jobe:InvRRCWLabourCost = 0
    jobe:InvRRCWPartsCost = 0
    jobe:InvRRCWPartsSale = 0
    jobe:InvRRCWSubTotal = 0
    jobe:ARC3rdPartyCost = 0
    jobe:InvARC3rdPartCost = 0
    jobe:WebJob = glo:webJob
    jobe:RRCELabourCost = 0
    jobe:RRCEPartsCost = 0
    jobe:RRCESubTotal = 0
    jobe:IgnoreRRCChaCosts = 0
    jobe:IgnoreRRCWarCosts = 0
    jobe:IgnoreRRCEstCosts = 0
    jobe:OBFvalidated = 0
    jobe:OBFvalidateDate = ''
    jobe:OBFvalidateTime = ''
    jobe:Despatched = 'NO'
    jobe:WarrantyClaimStatus = ''
    jobe:WarrantyStatusDate = ''
    jobe:InSecurityPackNo = ''
    jobe:JobSecurityPackNo = ''
    jobe:ExcSecurityPackNo = ''
    jobe:LoaSecurityPackNo = ''
    jobe:ExceedWarrantyRepairLimit = 0
    jobe:ConfirmClaimAdjustment = 0
    jobe:ARC3rdPartyVAT = 0
    jobe:ARC3rdPartyInvoiceNumber = 0
    jobe:ExchangeAdjustment = 0
    jobe:ExchangedATRRC = 0
    jobe:ClaimColour = 0
CreateWebJob        Routine
!   !Now to create an entry in WebJob
!
!   !First find the last Job number used
!   Access:Webjob.clearkey(wob:HeadJobNumberKey)
!   wob:JobNumber         = 999999999
!   wob:HeadAccountNumber = tmp:HeadAccountNumber
!   set(wob:HeadJobNumberKey,wob:HeadJobNumberKey)
!   if access:WebJob.previous()then
!        !Couldn't find one - set up the first
!        x# = 1
!   Else
!!        If glo:webJOb
!        If wob:HeadAccountNumber = tmp:HeadAccountNumber
!            x# = wob:JobNumber + 1
!        Else !If wob:HeadAccountNumber = Clarionet:Global.Param2
!            x# = 1
!        End !If wob:HeadAccountNumber = Clarionet:Global.Param2
!   END
!
!   Access:WEBJOB.ClearKey(wob:RefNumberKey)
!   wob:RefNumber = job:Ref_Number
!   if Access:WEBJOB.Fetch(wob:RefNumberKey)
!     !Now create the entry
!      if ~access:Webjob.primerecord()
!        !error
!        wob:JobNumber        = x#
!        wob:RefNumber        = job:Ref_Number
!        wob:HeadAccountNumber= tmp:HeadAccountNumber   !set up above
!        wob:SubAcountNumber  = job:account_number
!        wob:OrderNumber      = job:Order_Number
!        wob:IMEINumber       = job:ESN
!        wob:MSN              = job:MSN
!        wob:Manufacturer     = job:Manufacturer
!        wob:ModelNumber      = job:Model_Number
!        wob:MobileNumber     = job:Mobile_Number
!        wob:EDI              = job:EDI
!        wob:Validation       = tmp:IMEIValid
!        !Wob status are not set up the first time -need to be copied here
!        wob:Current_Status       = job:Current_Status
!        wob:Exchange_Status      = job:Exchange_Status
!        wob:Loan_Status          = job:Loan_Status
!        wob:Current_Status_Date  = today()
!        wob:Exchange_Status_Date = today()
!        wob:Loan_Status_Date     = today()
!        access:webjob.insert()
!        !WebJob is now complete
!        !   !message('Job number '&wob:jobNumber &', '&wob:refNUmber)
!      end
!   Else
!    !message('fail on prime record')


!        wob:RefNumber        = job:Ref_Number
        Do PutWebJob
        wob:HeadAccountNumber= tmp:HeadAccountNumber   !set up above
        wob:SubAcountNumber  = job:account_number
        wob:OrderNumber      = job:Order_Number
        wob:IMEINumber       = job:ESN
        wob:MSN              = job:MSN
        wob:Manufacturer     = job:Manufacturer
        wob:ModelNumber      = job:Model_Number
        wob:MobileNumber     = job:Mobile_Number
        wob:EDI              = job:EDI
        wob:Validation       = tmp:IMEIValid
        !Wob status are not set up the first time -need to be copied here
        wob:Current_Status       = job:Current_Status
        wob:Exchange_Status      = job:Exchange_Status
        wob:Loan_Status          = job:Loan_Status
        wob:Current_Status_Date  = today()
        wob:Exchange_Status_Date = today()
        wob:Loan_Status_Date     = today()
        access:webjob.update()
!   END
!Customer fields - show, hide and force routines

ShowCustomerFields      Routine
    !show them
    Unhide(?job:surname)
    Unhide(?job:title)
    Unhide(?job:title:prompt)
    Unhide(?job:surname:prompt)
    Unhide(?customer_name)
    unhide(?job:initial:prompt)
    unhide(?job:initial)
    unhide(?jobe:EndUserTelNo:Prompt)
    unhide(?tmp:EndUserTelNo)
    !make sure they are not forced
    ?job:Surname{prop:Req} = 0
    ?job:Surname{prop:background} = color:White


    exit


HideCustomerFields      Routine
    !hide them
    Hide(?job:surname)
    Hide(?job:title)
    Hide(?job:title:prompt)
    Hide(?job:surname:prompt)
    Hide(?customer_name)
    Hide(?job:initial:prompt)
    Hide(?job:initial)
    Hide(?jobe:EndUserTelNo:Prompt)
    Hide(?tmp:EndUserTelNo)
    !make sure they are not forced
    ?job:Surname{prop:Req} = 0
    ?job:Surname{prop:background} = color:White

    exit


ForceCustomerFields     Routine
    !show them
    Unhide(?job:surname)
    Unhide(?job:title)
    Unhide(?job:title:prompt)
    Unhide(?job:surname:prompt)
    Unhide(?customer_name)
    unhide(?job:initial:prompt)
    unhide(?job:initial)
    unhide(?jobe:EndUserTelNo:Prompt)
    unhide(?tmp:EndUserTelNo)
    !make sure they ARE forced
    ?job:Surname{prop:Req} = 1
    ?job:Surname{prop:BackGround} = 080FFFFH

    exit
!D
Display_Menu_Option Routine
    if job:Chargeable_Job = 'YES'
        Enable(?OptionsEstimate)
    else
        Disable(?OptionsEstimate)
    end
!E
ESN_Model_Check     Routine
    Esn_Model_Routine(job:esn,job:model_number,model",pass")
    If pass" = 1
        job:model_number = model"
        access:modelnum.clearkey(mod:model_number_key)
        mod:model_number = job:model_number
        if access:modelnum.fetch(mod:model_number_key) = Level:Benign
            job:manufacturer = mod:manufacturer
            If mod:specify_unit_type = 'YES'
                job:unit_type = mod:unit_type
            End!If mod:specify_unit_type = 'YES'
            If mod:product_Type <> ''
                job:fault_code1 = mod:product_type
            End!If mod:product_Type <> ''
        end
        model_number_temp = job:model_number
        Do check_msn
    End!If fill_in# = 1
    Display()
Estimate_Check      Routine
        !Check to see if the selected charge type
        !requires an Estimate to be forced.
        access:chartype.clearkey(cha:warranty_key)
        cha:charge_type = job:charge_type
        cha:warranty    = 'NO'
        if access:chartype.fetch(cha:warranty_key) = Level:Benign
            If cha:Force_Estimate = 'YES'
                job:Estimate = 'YES'
            Else ! !If cha:Force_Estimate = 'YES'
                !If you assign a Charge Type that doesn't
                !have "Force Estimate", just that the Trade Account
                !doesn't have "Force Estimate" setup.
                !Because if it doesn't you can turn estimate off.
                Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                sub:Account_Number  = job:Account_Number
                If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                    !Found
                    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                    tra:Account_Number  = sub:Main_Account_Number
                    If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                        !Found
                        If tra:force_estimate <> 'YES'
                            job:Estimate = 'NO'
                        End !If tra:force_estimate <> 'YES'
                    Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                    End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

                Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign

            End ! !If cha:Force_Estimate = 'YES'
        End !if access:chartype.fetch(cha:charge_type_key) = Level:Benign
!G
GETJOBSE                   Routine
   Access:JOBSE.Clearkey(jobe:RefNumberKey)
   jobe:RefNumber  = job:Ref_Number
   If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
       ! Found
       tmp:SkillLevel          = jobe:SkillLevel
       tmp:HubRepair           = jobe:HubRepair
       tmp:Network             = jobe:Network
       tmp:POPType             = jobe:POPType
       tmp:EndUserTelNo        = jobe:EndUserTelNo
       tmp:Booking48HourOption = jobe:Booking48HourOption
       tmp:VSACustomer         = jobe:VSACustomer
       ! Insert --- Use unused field to signify 48 Hour Exchange Repair (DBH: 11/03/2009) #10473
       tmp:48HourRefurb        = jobe:JobReceived
       ! end --- (DBH: 11/03/2009) #10473

   Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
   ! Error
   End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign


SETJOBSE                   Routine
   Access:JOBSE.Clearkey(jobe:RefNumberKey)
   jobe:RefNumber  = job:Ref_Number
   If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
       ! Found
       jobe:SkillLevel          = tmp:SkillLevel
       jobe:HUBRepair           = tmp:HUBRepair
       jobe:Network             = tmp:Network
       jobe:POPType             = tmp:POPType
       jobe:EndUserTelNo        = tmp:EndUserTelNo
       jobe:Booking48HourOption = tmp:Booking48HourOption
       jobe:VSACustomer         = tmp:VSACustomer
       ! Insert --- Use unused field to signify 48 Hour Exchange Repair (DBH: 11/03/2009) #10473
       jobe:JobReceived         = tmp:48HourRefurb
       ! end --- (DBH: 11/03/2009) #10473

       Access:JOBSE.TryUpdate()
   Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
   ! Error
   End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
GetWebJob       Routine
    Access:WEBJOB.Clearkey(wob:RefNumberKey)
    wob:RefNumber   = job:Ref_Number
    If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        ! Found
        tmp:wobJobNumber   = wob:JobNumber
        tmp:wobRefNumber   = wob:RefNumber
        tmp:wobFaultCode13 = wob:FaultCode13
        tmp:wobFaultCode14 = wob:FaultCode14
        tmp:wobFaultCode15 = wob:FaultCode15
        tmp:wobFaultCode16 = wob:FaultCode16
        tmp:wobFaultCode17 = wob:FaultCode17
        tmp:wobFaultCode18 = wob:FaultCode18
        tmp:wobFaultCode19 = wob:FaultCode19
        tmp:wobFaultCode20 = wob:FaultCode20
    Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        ! Error
    End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign

PutWebJob       Routine
    Access:WEBJOB.Clearkey(wob:RefNumberKey)
    wob:RefNumber   = job:Ref_Number
    If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        ! Found
        wob:JobNumber   = tmp:wobJobNumber
        wob:RefNumber   = tmp:wobRefNumber
        wob:FaultCode13 = tmp:wobFaultCode13
        wob:FaultCode14 = tmp:WobFaultCode14
        wob:FaultCode15 = tmp:wobFaultCode15
        wob:FaultCode16 = tmp:wobFaultCode16
        wob:FaultCode17 = tmp:wobFaultCode17
        wob:FaultCode18 = tmp:wobFaultCode18
        wob:FaultCode19 = tmp:wobFaultCode19
        wob:FaultCode20 = tmp:wobFaultCode20
        Access:WEBJOB.Update()
    Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        ! Error
    End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!L
Location_Bit     Routine
        job:location = loi:location
        If location_temp <> ''
    !Add To Old Location
            access:locinter.clearkey(loi:location_key)
            loi:location = location_temp
            If access:locinter.fetch(loi:location_key) = Level:Benign
                If loi:allocate_spaces = 'YES'
                    loi:current_spaces+= 1
                    loi:location_available = 'YES'
                    access:locinter.update()
                End
            end !if
        End!If location_temp <> ''
    !Take From New Location
        access:locinter.clearkey(loi:location_key)
        loi:location = job:location
        If access:locinter.fetch(loi:location_key) = Level:Benign
            If loi:allocate_spaces = 'YES'
                loi:current_spaces -= 1
                If loi:current_spaces< 1
                    loi:current_spaces = 0
                    loi:location_available = 'NO'
                End
                access:locinter.update()
            End
        end !if
        location_temp  = job:location

        Display()
!M
MSISDNRoutine        Routine
Data
local:Folder            String(255)
local:ININame           String(255)
local:INIFileName       String(255)
Code
    tmp:MobileActivationDate = ''
    tmp:MobilePhoneName      = ''
    tmp:MobileLoyaltyStatus  = ''
    tmp:MobileValidated      = ''
    tmp:MobileUpgradeDate    = ''
    tmp:MobileAverageSpend   = 0
    tmp:MobileLifetimeValue  = ''
    tmp:MobileIDNumber       = ''

    If tmp:OverrideMSISDNCheck
        Exit
    End ! If tmp:OverrideMSISDNCheck

 !Test Code
!    tmp:MobileActivationDate = Deformat('01/01/2010',@d06)
!    tmp:MobilePhoneName = 'Phone Name'
!    tmp:MobileLoyaltyStatus = 'Super Duper Customer'
!    tmp:MobileUpgradeDate    = Deformat('02/02/2010',@d6)
!    tmp:MobileAverageSpend   = '345.90'
!    tmp:MobileLifetimeValue  = 'Best Customer In The World'
!    tmp:MobileIDNumber       = '12345678901234567890'
!
!    tmp:MobileValidated = 1
!    ! DBH #11388 - Show new lookup screen
!    MobileNumberStatus(tmp:MobileLifetimeValue,                |
!                    tmp:MobileAverageSpend,                    |
!                    tmp:MobileLoyaltyStatus,                   |
!                    tmp:MobileUpgradeDate,                     |
!                    tmp:MobileIDNumber)
!    exit

    Access:TRADEACC.CLearkey(tra:Account_Number_Key)
    tra:Account_Number = wob:HeadAccountNumber
    If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        If ~tra:DoMSISDNCheck
            Exit
        End ! If ~tra:DoMSISDNCheck
    End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
    If job:Mobile_Number = ''
        Exit
    End ! If job:Mobile_Number = ''
    local:Folder =  GETINI('MQ','MQRequestFolder',,Clip(Path()) & '\SB2KDEF.INI')
    If local:Folder = ''
        Exit
    End ! If GETINI('MQ','MQRequestFolder',,Clip(Path()) & '\SB2KDEF.INI') = ''

    If Sub(Clip(local:Folder),-1,1) <> '\'
        local:Folder = Clip(local:Folder) & '\'
    End ! If Sub(Clip(local:Folder),-1,1) <> '\'

    local:ININame = Format(job:Ref_Number,@n09) & '_' & Format(Clock(),@t05) & '.ini'

    local:INIFIleName = Clip(local:Folder) & Clip(local:ININame)

    Remove(local:INIFileName)
    Remove(Clip(local:INIFileName) & '.old')

    PUTINI('REQUEST','TYPE','QRY_SUB',local:INIFileName)
    PUTINI('REQUEST','MSISDN',Clip(job:Mobile_Number),local:INIFileName)

    Return# = RunDOS(Clip(Path()) & '\MQREMOTE.EXE ' & Clip(local:ININame),1)

    Error# = 0
    If Return# = False
        Error# = 1
!        Case Missive('An error has occured during the MSISDN Customer Details Lookup Process.','ServiceBase 3g',|
!                       'mstop.jpg','/OK')
!            Of 1 ! OK Button
!        End ! Case Missive
        ! DBH #11388 - Show new lookup screen
        MobileNumberStatus('',0,'',0,'')

    Else ! If Return# = False
        If GETINI('RESPONSE','REQUEST_STATUS',,local:INIFileName) <> 'SUCCESS'
            Error# = 1
            If GETINI('RESPONSE','REQUEST_STATUS_CODE',,local:INIFileName) = '006'
                Case Missive('This is a "prepaid" customer.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
            Else ! If GETINI('RESPONSE','REQUEST_STATUS_CODE',,local:INIFileName) = '006'
!                Case Missive('An error has occured during the MSISDN Customer Details Lookup Process.','ServiceBase 3g',|
!                               'mstop.jpg','/OK')
!                    Of 1 ! OK Button
!                End ! Case Missive
                ! DBH #11388 - Show new lookup screen
                MobileNumberStatus('',0,'',0,'')
            End ! If GETINI('RESPONSE','REQUEST_STATUS_CODE',,local:INIFileName) = '006'
        End ! If GETINI('RESPONSE','REQUEST_STATUS',,local:INIFileName) <> 'SUCCESS'
    End ! If Return# = False

    If Error# = 1
        If GETINI('MQ','MSISDNKeepFiles',,Clip(Path()) & '\SB2KDEF.INI') = 1
            ! Keep a history of processed files (DBH: 07/11/2007)
            Rename(local:INIFilename,Clip(local:INIFileName) & '.old')
        Else ! If GETINI('MQ','MSISDNKeepFiles',,Clip(Path()) & '\SB2KDEF.INI') = 1
            Remove(local:INIFilename)
        End ! If GETINI('MQ','MSISDNKeepFiles',,Clip(Path()) & '\SB2KDEF.INI') = 1

        Exit
    End ! If Error# = 1

    tmp:MobileActivationDate = Deformat(GETINI('RESPONSE','ACTIVATION_DATE',,local:INIFileName),@d6)
    tmp:MobilePhoneName = GETINI('RESPONSE','PHONE_NAME',,local:INIFileName)
    tmp:MobileLoyaltyStatus = GETINI('RESPONSE','LOYALTY_STATUS',,local:INIFileName)
    tmp:MobileUpgradeDate    = Deformat(GETINI('RESPONSE','NEXT_UPGRADE_DATE',,local:INIFileName),@d6)
    tmp:MobileAverageSpend   = GETINI('RESPONSE','AVERAGE_SPEND',,local:INIFilename)
    tmp:MobileIDNumber       = GETINI('RESPONSE','ID_NUMBER',,local:INIFilename)

    ! Remove/Rename First File
    If GETINI('MQ','MSISDNKeepFiles',,Clip(Path()) & '\SB2KDEF.INI') = 1
        ! Keep a history of processed files (DBH: 07/11/2007)
        Rename(local:INIFilename,Clip(local:INIFileName) & '.old')
    Else ! If GETINI('MQ','MSISDNKeepFiles',,Clip(Path()) & '\SB2KDEF.INI') = 1
        Remove(local:INIFilename)
    End ! If GETINI('MQ','MSISDNKeepFiles',,Clip(Path()) & '\SB2KDEF.INI') = 1

    ! DBH #11388 - Call for second INI File.

    local:ININame = Format(job:Ref_Number,@n09) & '_2_' & Format(Clock(),@t05) & '.ini'

    local:INIFIleName = Clip(local:Folder) & Clip(local:ININame)

    Remove(local:INIFileName)
    Remove(Clip(local:INIFileName) & '.old')

    PUTINI('REQUEST','TYPE','QRY_VDTATT',local:INIFileName)
    PUTINI('REQUEST','MSISDN',Clip(job:Mobile_Number),local:INIFileName)

    Return# = RunDOS(Clip(Path()) & '\MQREMOTE.EXE ' & Clip(local:ININame),1)

    tmp:MobileLifetimeValue  = GETINI('RESPONSE','SUBSCRIBER_ATTRIBUTE_1_SAC_DESCRIPTION',,local:INIFilename)

    tmp:MobileValidated = 1
    ! DBH #11388 - Show new lookup screen
    MobileNumberStatus(tmp:MobileLifetimeValue,                |
                    tmp:MobileAverageSpend,                    |
                    tmp:MobileLoyaltyStatus,                   |
                    tmp:MobileUpgradeDate,                     |
                    tmp:MobileIDNumber)


    If GETINI('MQ','MSISDNKeepFiles',,Clip(Path()) & '\SB2KDEF.INI') = 1
        ! Keep a history of processed files (DBH: 07/11/2007)
        Rename(local:INIFilename,Clip(local:INIFileName) & '.old')
    Else ! If GETINI('MQ','MSISDNKeepFiles',,Clip(Path()) & '\SB2KDEF.INI') = 1
        Remove(local:INIFilename)
    End ! If GETINI('MQ','MSISDNKeepFiles',,Clip(Path()) & '\SB2KDEF.INI') = 1

    Display()
MSN_Embeds          ROUTINE

    data
tmp:DateCodeChecked byte
    code


    IF LEN(CLIP(job:MSN)) = 11 AND job:Manufacturer = 'ERICSSON'
      !Ericsson MSN!
      Job:MSN = SUB(job:MSN,2,10)
      UPDATE()
      Display()
    END

    !End!
    Error# = 0
    If CheckLength('MSN',job:model_number,job:msn)
        Select(?job:msn)
        Error# = 1
    Else!If error# = 1
        Error# = 0
        Access:MANUFACT.ClearKey(man:Manufacturer_Key)
        man:Manufacturer = job:Manufacturer
        If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
            !Found
            If man:ApplyMSNFormat
                If CheckFaultFormat(job:Msn,man:MSNFormat)
                    Case Missive('M.S.N. failed format validation.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                        Of 1 ! OK Button
                    End ! Case Missive
                    Select(?job:msn)
                    Error# = 1
                End !If CheckFaultFormat(job:Msn,man:MSNFormat)
            End !If man:ApplyMSNFormat
        Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
    End!If error# = 1

    !If it's an OBF, then don't do any validation
    If job:Warranty_job = 'YES' and job:Warranty_Charge_type = 'WARRANTY (OBF)'
        EXIT
    End !If job:Warranty_job = 'YES' and job:Warranty_Charge_type = 'WARRANTY (OBF)'

    job:Warranty_Job = ''
    job:Warranty_Charge_type = ''
    !these then have to be changed

    !Added By Neil!
    If job:Manufacturer = 'MOTOROLA' AND Error# = 0

        tmp:DateCodeChecked = true

        If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
          tmp:DisplayPOP = true
          do BastionCheck
          if tmp:DisplayPOP = true
            do Validate_Motorola
          end

        Else !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
          job:Warranty_Job = 'YES'
          Post(Event:Accepted,?job:Warranty_Job)
          job:Warranty_Charge_Type = 'WARRANTY (MFTR)'
          IF job:pop = ''
            job:pop = 'F'
          END
        End !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)

        IF job:pop <> ''
          UNHIDE(?Validate_POP)
        ELSE
          HIDE(?Validate_POP)
        END

    End !If job:Manufacturer = 'MOTOROLA'

    If job:Manufacturer = 'SAMSUNG' AND Error# = 0

        tmp:DateCodeChecked = true

        If DateCodeValidation('SAMSUNG',job:MSN,job:Date_Booked)
          tmp:DisplayPOP = true
          do BastionCheck
          if tmp:DisplayPOP = true
            do Validate_Motorola
          end

        Else !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
          job:Warranty_Job = 'YES'
          Post(Event:Accepted,?job:Warranty_Job)

          job:Warranty_Charge_Type = 'WARRANTY (MFTR)'
          IF job:pop = ''
            job:pop = 'F'
          END
        End !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
        IF job:pop <> ''
          UNHIDE(?Validate_POP)
        ELSE
          HIDE(?Validate_POP)
        END
    End !If job:Manufacturer = 'MOTOROLA'

    If job:Manufacturer = 'PHILIPS' AND Error# = 0

        tmp:DateCodeChecked = true

        If DateCodeValidation('PHILIPS',job:MSN,job:Date_Booked)
          tmp:DisplayPOP = true
          do BastionCheck
          if tmp:DisplayPOP = true
            do Validate_Motorola
          end

        Else !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
          job:Warranty_Job = 'YES'
          Post(Event:Accepted,?job:Warranty_Job)

          job:Warranty_Charge_Type = 'WARRANTY (MFTR)'
          IF job:pop = ''
            job:pop = 'F'
          END
        End !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)

        IF job:pop <> ''
          UNHIDE(?Validate_POP)
        ELSE
          HIDE(?Validate_POP)
        END
    End !If job:Manufacturer = 'MOTOROLA'

    if tmp:DateCodeChecked = false And Error# = 0
        tmp:DisplayPOP = true
        do BastionCheck
        if tmp:DisplayPOP = true
            do Validate_Motorola
        end
    end

    if job:Warranty_job = 'YES' then
        enable(?Lookup_Warranty_Charge_Type)
    END



    display()
MQ:Process                 routine
Data
local:SystemError   Byte(0)
local:Error  Byte(0)
local:ErrorMessage  String(255)
Code
    if (job:ESN = '') ! #11344 Don't bother if no IMEI (DBH: 04/06/2010)
        exit
    end

    ! Set Status To Zero - TrkBs: 6690 (DBH: 11-11-2005)
    tmp:IMEIValidationStatus = 0
    ?Notes{prop:Hide} = 1

    ! Inserting Code (DBH, 11/16/2005) - #6729 Clear any existing out fault entries before validating the IMEI
    Access:JOBOUTFL.ClearKey(joo:JobNumberKey)
    joo:JobNumber = job:Ref_Number
    Set(joo:JobNumberKey,joo:JobNumberKey)
    Loop
        If Access:JOBOUTFL.NEXT()
           Break
        End !If
        If joo:JobNumber <> job:Ref_Number      |
            Then Break.  ! End If
        Relate:JOBOUTFL.Delete(0)
    End !Loop
    ! End (DBH, 11/16/2005) - #6729

    !Added by Paul 28/04/2010 Log no 11344

    !First we need to check if the manufacturer has a 1 year warranty limit
    ! DBH - Call a routine to keep sbonline in sync
    If (IsOneYearWarranty(job:Manufacturer,job:Model_Number))
        OneYearWarrantyOnly = 'Y'
    else
        OneYearWarrantyOnly = 'N'
    end
!    Access:Manufact.clearkey(man:Manufacturer_Key)
!    man:Manufacturer = job:Manufacturer
!    If Access:Manufact.Fetch(man:Manufacturer_Key) = level:benign then
!        !found the manufacturer
!        If man:OneYearWarrOnly = 'Y' then
!            OneYearWarrantyOnly = 'Y'
!        Else
!            OneYearWarrantyOnly = 'N'
!        End
!    Else
!        OneYearWarrantyOnly = 'N'
!    End
!
!    If OneYearWarrantyOnly = 'N' then
!        !check to see if the handset has a 1 year warranty limit
!        Access:ModelNum.Clearkey(mod:Manufacturer_Key)
!        mod:Manufacturer    = job:Manufacturer
!        mod:Model_Number    = job:Model_Number
!        If Access:ModelNum.Fetch(mod:Manufacturer_Key) = Level:Benign then
!            IF mod:OneYearWarrOnly = 'Y' then
!                OneYearWarrantyOnly = 'Y'
!            Else
!                OneYearWarrantyOnly = 'N'
!            End
!        Else
!            OneYearWarrantyOnly = 'N'
!        End
!    End


    local:SystemError = MQProcess(job:ESN,job:POP,job:DOP,local:Error,local:ErrorMessage,OneYearWarrantyOnly)
                                                                         !(notes)

    !added by Paul 30/04/2010 - Log No 11344
    If job:POP = 'S' then
        !double check the 2nd year warranty
        If OneYearWarrantyOnly = 'Y' then
            !change the warranty to chargeable
            job:POP = 'C'
        End
    End

    !End addition by Paul

    if (local:SystemError)
        Case Missive('An error has occurred during the automatic validation process.'&|
          '|Please validate this I.M.E.I. number manually.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
        ! Error occured - TrkBs: 6690 (DBH: 11-11-2005)
        tmp:IMEIValidationStatus = 1
        ! Show a failed return in the outfaults - TrkBs: 6690 (DBH: 11-11-2005)
        If Access:JOBOUTFL.PrimeRecord() = Level:Benign
            joo:JobNumber = job:Ref_Number
            joo:FaultCode = 0
            ! Add text "IMEI VALIDATION" so SB knows not to delete this entry - TrkBs: 6690 (DBH: 11-11-2005)
            joo:Description = 'IMEI VALIDATION: ' & clip(upper(local:ErrorMessage)) & ' PLEASE VALIDATE THIS I.M.E.I. MANUALLY'
            joo:Level = 0
            If Access:JOBOUTFL.TryInsert() = Level:Benign
                ! Insert Successful

            Else ! If Access:JOBOUTFL.TryInsert() = Level:Benign
                ! Insert Failed
                Access:JOBOUTFL.CancelAutoInc()
            End ! If Access:JOBOUTFL.TryInsert() = Level:Benign
        End !If Access:JOBOUTFL.PrimeRecord() = Level:Benign
    else
        case job:POP
        of 'C'  ! Chargeable
            job:Chargeable_Job = 'YES'
            !message('PaulMessage 3')
            job:Charge_Type = 'NON-WARRANTY'
            job:Warranty_Job = 'NO'
            job:Warranty_Charge_Type = ''
            Post(Event:Accepted,?job:Chargeable_Job)
            Post(Event:Accepted,?job:Warranty_Job)

            if (Upper(local:ErrorMessage) <> 'THIS PHONE MUST BE REPAIRED OUT OF WARRANTY.')
                Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
                jbn:RefNumber = job:Ref_Number
                If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
                    !Found

                Else !If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
                    !Error
                    If Access:JOBNOTES.PrimeRecord() = Level:Benign
                        jbn:RefNumber = job:Ref_Number
                        if clip(PRE:FaultText) <> '' then
                            jbn:Fault_Description = PRE:FaultText
                        END
                        If Access:JOBNOTES.TryInsert() = Level:Benign
                            ! Insert Successful

                        Else ! If Access:JOBNOTES.TryInsert() = Level:Benign
                            ! Insert Failed
                            Access:JOBNOTES.CancelAutoInc()
                        End ! If Access:JOBNOTES.TryInsert() = Level:Benign  
                    End !If Access:JOBNOTES.PrimeRecord() = Level:Benign
                End !If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign

                If Clip(jbn:Fault_Description) <> ''
                    jbn:Fault_Description = Clip(jbn:Fault_Description) & '<13,10>' & Clip(Upper(local:ErrorMessage))
                Else ! If Clip(jbn:Fault_Description) <> ''
                    jbn:Fault_Description = CLip(Upper(local:ErrorMessage))
                End ! If Clip(jbn:Fault_Description) <> ''
                Access:JOBNOTES.Update()
            end ! if (Upper(f:ErrorMessage)     <> 'THIS PHONE MUST BE REPAIRED OUT OF WARRANTY.')

        of 'F' ! First Year Warranty
            job:Warranty_Job = 'YES'
            job:Warranty_Charge_Type = 'WARRANTY (MFTR)'
            Post(Event:Accepted,?job:Warranty_Job)
        of 'S' ! Second Year Warranty
            job:Warranty_Job = 'YES'
            job:Warranty_Charge_Type = 'WARRANTY (2ND YR)'
            Post(Event:Accepted,?job:Warranty_Job)
        end ! case f:POP

        if (local:ErrorMessage <> '')

            Notes = local:ErrorMessage

            if (local:Error)
                Notes = 'This IMEI must be repaired Out of Warranty, due to the following reason(s):<13,10>' & Clip(Notes)
            end ! if (f:Error)

            ?Notes{prop:Hide} = 0

            ! Start - Add Warranty Text to OutFault - TrkBs: 3287 (DBH: 27-09-2005)
            If Access:JOBOUTFL.PrimeRecord() = Level:Benign
                joo:JobNumber = job:Ref_Number
                joo:FaultCode = 0
                ! Add text "IMEI VALIDATION" so SB knows not to delete this entry - TrkBs: 6690 (DBH: 11-11-2005)
                joo:Description = 'IMEI VALIDATION: ' & Upper(Clip(Notes)) & '<13,10>ACTIVATION DATE: ' & Format(job:DOP,@d6)
                joo:Level = 0
                If Access:JOBOUTFL.TryInsert() = Level:Benign
                    ! Insert Successful

                Else ! If Access:JOBOUTFL.TryInsert() = Level:Benign
                    ! Insert Failed
                    Access:JOBOUTFL.CancelAutoInc()
                End ! If Access:JOBOUTFL.TryInsert() = Level:Benign
            End !If Access:JOBOUTFL.PrimeRecord() = Level:Benign
            ! End   - Add Warranty Text to OutFault - TrkBs: 3287 (DBH: 27-09-2005)
        end ! if (f:ErrorMessage <> '')

    end ! if (local:Error)

    ! DBH #10544 - Check if previous job was liquid damaged (don't check if sent to ARC)
    If (IsUnitLiquidDamaged(job:Ref_Number,job:ESN,TRUE))
        job:Warranty_Job = 'NO'
        job:Chargeable_Job = 'YES'
        ?job:Warranty_Job{prop:Disable} = 1
        ?tmp:Booking48HourOption:Radio4{prop:Hide} = 0
        post(event:accepted,?job:Chargeable_Job)
        post(event:accepted,?job:Warranty_Job)
!        if (local:Error and ~local:SystemError)
!            ! Append to failure notes
!            Notes = Clip(Notes) & '<13,10>Previous Repair, handset deemed Liquid Damaged'
!        else
            ! Add notes before passed notes
        Notes = 'The IMEI must be repaired out of warranty due to the following reason: ' & |
                 'Previous Repair, handset deemed Liquid Damaged.'
!        end
        ?Notes{prop:Hide} = 0
    else
        ?job:Warranty_Job{prop:Disable} = 0
        ?tmp:Booking48HourOption:Radio4{prop:Hide} = 1
    end !If (IsUnitLiquidDamaged(job:Ref_Number,job:ESN))


!    Data
!inFileName                 string(260)
!outFileName                string(260)
!
!vErrorMsg                  string(1000)
!vError                     byte
!vWarrantyMessage           string(1000)
!vActivationDate            string(30)
!vYearDiff                  long
!oldDate                    date
!currentDate                date
!local:UserName             String(30)
!local:Password             String(30)
!local:SBMQIFolder           CString(255)
!    code
!    ! Set Status To Zero - TrkBs: 6690 (DBH: 11-11-2005)
!    tmp:IMEIValidationStatus = 0
!
!    ?Notes{prop:Hide} = 1
!
!    If GETINI('MQ','IMEIValidation',,Clip(Path()) & '\SB2KDEF.INI') <> 1
!        Exit
!    End ! If GETINI('MQ','IMEIValidation',,Clip(Path()) & '\SB2KDEF.INI') <> 1
!
!    ! Inserting Code (DBH, 11/16/2005) - #6729 Clear any existing out fault entries before validating the IMEI
!    Access:JOBOUTFL.ClearKey(joo:JobNumberKey)
!    joo:JobNumber = job:Ref_Number
!    Set(joo:JobNumberKey,joo:JobNumberKey)
!    Loop
!        If Access:JOBOUTFL.NEXT()
!           Break
!        End !If
!        If joo:JobNumber <> job:Ref_Number      |
!            Then Break.  ! End If
!        Relate:JOBOUTFL.Delete(0)
!    End !Loop
!    ! End (DBH, 11/16/2005) - #6729
!
!    ! Get location of SBMQI folder. Use default location if not filled in - TrkBs: 6613 (DBH: 04-11-2005)
!    local:SBMQIFolder = GETINI('MQ','SBMQIFolder',,Clip(Path()) & '\SB2KDEF.INI')
!    If local:SBMQIFolder = ''
!        local:SBMQIFolder = Clip(Path())
!    End ! If local:SBMQIFolder = ''
!
!    ! Make sure field has trailing backslash
!    If Sub(Clip(local:SBMQIFolder),-1,1) <> '\'
!        local:SBMQIFolder = Clip(local:SBMQIFolder) & '\'
!    End ! If Sub(Clip(local:SBMQIFolder),-1,1) <> '\'
!
!    inFileName  = Clip(local:SBMQIFolder) & 'MQI' & clock() & '.CSV'   ! Unique Input file name
!    outFileName = Clip(local:SBMQIFolder) & 'MQO' & clock() & '.CSV'  ! Unique Output file name
!    !outFileName = 'MQ.CSV'
!
!     ! Create the input file - just send the IMEI number
!     ! -----------------------------------------------------------------------
!    mqFileName = clip(inFileName)
!
!    create(MQIInputFile)
!    open(MQIInputFile)
!
!    MQIF:imei = job:ESN
!    add(MQIInputFile)
!    close(MQIInputFile)
!    ! -----------------------------------------------------------------------
!
!!    !Message('Input File: ' & Clip(inFileName) & |
!!            '|Output File: ' & Clip(outFileName) & |
!!            '|Command Line: ' & Clip(Clip(local:SBMQIFolder) & 'SBMQI.EXE ' & clip(inFileName) & ' ' & clip(outFileName)))
!
!    ! Usage :- SBMQI.EXE <InputFile> <OutputFile>
!    !run(Clip(local:SBMQIFolder) & 'SBMQI.EXE ' & clip(inFileName) & ' ' & clip(outFileName), 1)
!
!    ! Call the new routine (no DOS box should show). True should be returned if run successfully - TrkBs: 6631 (DBH: 11-11-2005)
!    Return# = RunDOS(Clip(local:SBMQIFolder) & 'SBMQI.EXE ' & clip(inFileName) & ' ' & clip(outFileName),1)
!
!     ! Handle the output file
!     ! -----------------------------------------------------------------------
!    If ~Exists(outFileName) Or Return# = False
!        Case Missive('An error has occurred during the automatic validation process.'&|
!          '|Please validate this I.M.E.I. number manually.','ServiceBase 3g',|
!                       'mstop.jpg','/OK')
!            Of 1 ! OK Button
!        End ! Case Missive
!        ! Error occured - TrkBs: 6690 (DBH: 11-11-2005)
!        tmp:IMEIValidationStatus = 1
!        ! Show a failed return in the outfaults - TrkBs: 6690 (DBH: 11-11-2005)
!        If Access:JOBOUTFL.PrimeRecord() = Level:Benign
!            joo:JobNumber = job:Ref_Number
!            joo:FaultCode = 0
!            ! Add text "IMEI VALIDATION" so SB knows not to delete this entry - TrkBs: 6690 (DBH: 11-11-2005)
!            joo:Description = 'IMEI VALIDATION: AN ERROR OCCURED DURING THE AUTOMATIC VALIDATION PROCESS. PLEASE VALIDATE THIS I.M.E.I. MANUALLY'
!            joo:Level = 0
!            If Access:JOBOUTFL.TryInsert() = Level:Benign
!                ! Insert Successful
!
!            Else ! If Access:JOBOUTFL.TryInsert() = Level:Benign
!                ! Insert Failed
!                Access:JOBOUTFL.CancelAutoInc()
!            End ! If Access:JOBOUTFL.TryInsert() = Level:Benign
!        End !If Access:JOBOUTFL.PrimeRecord() = Level:Benign
!    Else ! If ~Exists(outFileName) Or Return# = False
!        mqFileName = outFileName
!
!        open(MQIOutputFile)
!
!        set(MQIOutputFile, 0)
!        next(MQIOutputFile)
!        if not error()
!
!            ! Output the results from the MQmessage
!            msgActivationDate = 'Activation Date: ' & clip(MQOF:activationDate)
!            msgActivity       = 'Activity: ' & clip(MQOF:activity)
!            msgWarranty       = 'Warranty: ' & clip(MQOF:warranty)
!            msgBlackGrey      = 'Black / Grey Listing: ' & clip(MQOF:blackGrey)
!            msgSupplier       = 'Supplier: ' & clip(MQOF:supplier)
!            msgSwap           = 'Swap: ' & clip(MQOF:swap)
!            msgUsage          = 'Usage: ' & clip(MQOF:usage)
!            msgStatusCode     = 'Status Code: ' & clip(MQOF:statusCode)
!            msgStatusDesc     = 'Status Description: ' & clip(MQOF:statusDescription)
!
!             ! The following adapted from the java code in the spec :-
!             ! Just parsing through strings ....
!            vErrorMsg        = ''
!            vError           = false
!            vWarrantyMessage = ''
!            vActivationDate  = ''
!            vYearDiff        = 0
!
!             ! Confused by code. Shouldn't it be Instring? (DBH: 27-09-2005)
!!            if clip(msgSupplier) = 'This IMEI is not in the 2nd year Vodacom warranty database'
!!                vErrorMsg = 'This IMEI is not on the Vodacom Service Provider Company Two Year Warranty database.'
!!                vError    = true
!!            end ! if
!            ! End   - Confused by code. Shouldn't it be Instring? (DBH: 27-09-2005)
!            if Instring('This IMEI is not in the 2nd year Vodacom warranty database',clip(msgSupplier),1,1) > 0
!                vErrorMsg = 'This IMEI is not on the Vodacom Service Provider Company Two Year Warranty database.'
!                vError    = true
!
!                Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
!                jbn:RefNumber = job:Ref_Number
!                If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
!                    !Found
!
!                Else !If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
!                    !Error
!                    If Access:JOBNOTES.PrimeRecord() = Level:Benign
!                        jbn:RefNumber = job:Ref_Number
!                        If Access:JOBNOTES.TryInsert() = Level:Benign
!                            ! Insert Successful
!
!                        Else ! If Access:JOBNOTES.TryInsert() = Level:Benign
!                            ! Insert Failed
!                            Access:JOBNOTES.CancelAutoInc()
!                        End ! If Access:JOBNOTES.TryInsert() = Level:Benign
!                    End !If Access:JOBNOTES.PrimeRecord() = Level:Benign
!                End !If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
!
!                If Clip(jbn:Fault_Description) <> ''
!                    jbn:Fault_Description = Clip(jbn:Fault_Description) & '<13,10>' & Clip(Upper(vErrorMsg))
!                Else ! If Clip(jbn:Fault_Description) <> ''
!                    jbn:Fault_Description = CLip(Upper(vErrorMsg))
!                End ! If Clip(jbn:Fault_Description) <> ''
!                Access:JOBNOTES.Update()
!
!                job:Chargeable_Job = 'YES'
!                job:Charge_Type = 'NON-WARRANTY'
!                job:Warranty_Job = 'NO'
!                job:Warranty_Charge_Type = ''
!                Post(Event:Accepted,?job:Chargeable_Job)
!                Post(Event:Accepted,?job:Warranty_Job)
!            end ! if
!
!            ! ASSUMPTION : If we find the text "Currently Blacklisted/Greylisted" in the string
!            if instring('Currently Blacklisted/Greylisted', clip(msgBlackGrey), 1, 1) <> 0
!                if vErrorMsg = ''
!                    vErrorMsg = 'This IMEI has been Black Listed or Grey Listed.'
!                else
!                    vErrorMsg = clip(vErrorMsg) & '<13,10>' & 'This IMEI has been Black Listed or Grey Listed.'
!                end ! if
!                vError = true
!
!                Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
!                jbn:RefNumber = job:Ref_Number
!                If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
!                    !Found
!
!                Else !If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
!                    !Error
!                    If Access:JOBNOTES.PrimeRecord() = Level:Benign
!                        jbn:RefNumber = job:Ref_Number
!                        If Access:JOBNOTES.TryInsert() = Level:Benign
!                            ! Insert Successful
!
!                        Else ! If Access:JOBNOTES.TryInsert() = Level:Benign
!                            ! Insert Failed
!                            Access:JOBNOTES.CancelAutoInc()
!                        End ! If Access:JOBNOTES.TryInsert() = Level:Benign
!                    End !If Access:JOBNOTES.PrimeRecord() = Level:Benign
!                End !If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
!
!                If Clip(jbn:Fault_Description) <> ''
!                    jbn:Fault_Description = Clip(jbn:Fault_Description) & '<13,10>' & Clip(Upper(vErrorMsg))
!                Else ! If Clip(jbn:Fault_Description) <> ''
!                    jbn:Fault_Description = CLip(Upper(vErrorMsg))
!                End ! If Clip(jbn:Fault_Description) <> ''
!                Access:JOBNOTES.Update()
!
!                job:Chargeable_Job = 'YES'
!                job:Charge_Type = 'NON-WARRANTY'
!                job:Warranty_Job = 'NO'
!                job:Warranty_Charge_Type = ''
!                Post(Event:Accepted,?job:Chargeable_Job)
!                Post(Event:Accepted,?job:Warranty_Job)
!            end ! if
!
!            ! ASSUMPTION : The date will always be at the end of the string and will have the format "dd MMM yyyy"
!            vActivationDate = sub(clip(msgActivity), -11, 11)
!            ! If you deformat an invalid date string the activation date gets set to 31/12/2000 ???
!            if vActivationDate <> ''
!                activationDate = deformat(vActivationDate, @D08)
!            end ! if
!
!            if activationDate <> ''
!                job:DOP     = ActivationDate
!                oldDate     = activationDate
!                currentDate = today()
!
!                loop while currentDate > oldDate
!                    oldDate    = date(month(oldDate), day(oldDate), year(oldDate) + 1)
!                    vYearDiff += 1
!                end ! loop
!
!                if vYearDiff > 0
!                    vYearDiff = vYearDiff - 1
!                end ! if
!
!                if vError = false and vYearDiff = 0
!                    vWarrantyMessage = 'The IMEI can be repaired under the First Year Manufacturer Warranty as per manufacturer criteria.'
!                    job:Warranty_Job = 'YES'
!                    job:Warranty_Charge_Type = 'WARRANTY (MFTR)'
!                    Post(Event:Accepted,?job:Warranty_Job)
!                end ! if
!
!                if vError = false and vYearDiff > 0 and vYearDiff <= 1
!! Changing (DBH 12/01/2007) # 8593 - Drop the "Vodacom" reference
!!                    vWarrantyMessage = 'The IMEI can be repaired under the Second Year Vodacom Warranty.'
!! to (DBH 12/01/2007) # 8593
!                    vWarrantyMessage = 'The IMEI can be repaired under the Second Year Warranty.'
!! End (DBH 12/01/2007) #8593
!                     ! only when we are in second year warranty, check if active for more than 30 days
!                     ! ASSUMPTION: if we find the test "was only" in the string, we are working with activity less than 30 days
!                    if instring('was only', clip(msgUsage), 1, 1) <> 0
!                        if vErrorMsg = ''
!                            vErrorMsg = 'This IMEI has not been active for 30 days in the last 90 days.'
!                        else
!                            vErrorMsg = clip(vErrorMsg) & '<13,10>' & 'This IMEI has not been active for 30 days in the last 90 days.'
!                        end ! if
!                        vError = true
!                        Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
!                        jbn:RefNumber = job:Ref_Number
!                        If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
!                            !Found
!
!                        Else !If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
!                            !Error
!                            If Access:JOBNOTES.PrimeRecord() = Level:Benign
!                                jbn:RefNumber = job:Ref_Number
!                                If Access:JOBNOTES.TryInsert() = Level:Benign
!                                    ! Insert Successful
!
!                                Else ! If Access:JOBNOTES.TryInsert() = Level:Benign
!                                    ! Insert Failed
!                                    Access:JOBNOTES.CancelAutoInc()
!                                End ! If Access:JOBNOTES.TryInsert() = Level:Benign
!                            End !If Access:JOBNOTES.PrimeRecord() = Level:Benign
!                        End !If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
!
!                        If Clip(jbn:Fault_Description) <> ''
!                            jbn:Fault_Description = Clip(jbn:Fault_Description) & '<13,10>' & Clip(Upper(vErrorMsg))
!                        Else ! If Clip(jbn:Fault_Description) <> ''
!                            jbn:Fault_Description = CLip(Upper(vErrorMsg))
!                        End ! If Clip(jbn:Fault_Description) <> ''
!                        Access:JOBNOTES.Update()
!
!                        job:Chargeable_Job = 'YES'
!                        job:Charge_Type = 'NON-WARRANTY'
!                        job:Warranty_Job = 'NO'
!                        job:Warranty_Charge_Type = ''
!                        Post(Event:Accepted,?job:Chargeable_Job)
!                        Post(Event:Accepted,?job:Warranty_Job)
!                    else
!                        job:Warranty_Job = 'YES'
!                        job:Warranty_Charge_Type = 'WARRANTY (2ND YR)'
!                        Post(Event:Accepted,?job:Warranty_Job)
!                    end ! if
!                end ! if
!
!                if vError = false and vYearDiff > 1
!                    if vWarrantyMessage = ''
!                        vWarrantyMessage = 'This Phone must be repaired out of Warranty.'
!                    else
!                        vWarrantyMessage = clip(vErrorMsg) & '<13,10>' & 'This Phone must be repaired out of Warranty.'
!                    end ! if
!                    job:Chargeable_Job = 'YES'
!                    job:Charge_Type = 'NON-WARRANTY'
!                    job:Warranty_Job = 'NO'
!                    job:Warranty_Charge_Type = ''
!                    Post(Event:Accepted,?job:Chargeable_Job)
!                    Post(Event:Accepted,?job:Warranty_Job)
!                end ! if
!            else
!                 ! if we get here, it means that we dont have a parseable date in the string...
!                 ! ASSUMPTION: if we find the text "never been active" in the string, the imei was not active
!                if instring('never been active', clip(msgActivity), 1, 1) <> 0
!                    if vErrorMsg = ''
!                        vErrorMsg = 'This IMEI has never been active on the Vodacom Network.'
!                    else
!                        vErrorMsg = clip(vErrorMsg) & '<13,10>' & 'This IMEI has never been active on the Vodacom Network.'
!                    end ! if
!                    vError = true
!
!                    job:Chargeable_Job = 'YES'
!                    job:Charge_Type = 'NON-WARRANTY'
!                    job:Warranty_Job = 'NO'
!                    job:Warranty_Charge_Type = ''
!                    Post(Event:Accepted,?job:Chargeable_Job)
!                    Post(Event:Accepted,?job:Warranty_Job)
!                end ! if
!            end ! if
!
!            if (vError = true)
!                notes = 'This IMEI must be repaired Out of Warranty, due to the following reason(s):<13,10>' & clip(vErrorMsg)
!                job:Chargeable_Job = 'YES'
!                job:Charge_Type = 'NON-WARRANTY'
!                job:Warranty_Job = 'NO'
!                job:Warranty_Charge_Type = ''
!                Post(Event:Accepted,?job:Chargeable_Job)
!                Post(Event:Accepted,?job:Warranty_Job)
!            else
!                notes = clip(vWarrantyMessage)
!            end ! if
!            ?Notes{prop:Hide} = 0
!
!            ! Start - Add Warranty Text to OutFault - TrkBs: 3287 (DBH: 27-09-2005)
!            If Access:JOBOUTFL.PrimeRecord() = Level:Benign
!                joo:JobNumber = job:Ref_Number
!                joo:FaultCode = 0
!                ! Add text "IMEI VALIDATION" so SB knows not to delete this entry - TrkBs: 6690 (DBH: 11-11-2005)
!                joo:Description = 'IMEI VALIDATION: ' & Upper(Clip(Notes)) & '<13,10>ACTIVATION DATE: ' & Format(ActivationDate,@d6)
!                joo:Level = 0
!                If Access:JOBOUTFL.TryInsert() = Level:Benign
!                    ! Insert Successful
!
!                Else ! If Access:JOBOUTFL.TryInsert() = Level:Benign
!                    ! Insert Failed
!                    Access:JOBOUTFL.CancelAutoInc()
!                End ! If Access:JOBOUTFL.TryInsert() = Level:Benign
!            End !If Access:JOBOUTFL.PrimeRecord() = Level:Benign
!            ! End   - Add Warranty Text to OutFault - TrkBs: 3287 (DBH: 27-09-2005)
!
!        end ! if
!
!        close(MQIOutputFile)
!        ! Remove output file, the input file will be removed by the SBMQI.EXE
!        remove(mqFileName)
!
!        display()
!    End ! if
!
Multiple_Job_Booking_Setup Routine
    !Setup the update form for multiple job booking
    DATA
Original_Job_No         long
Original_Eng_Notes      string(255)
Count                   long(1)
Original_Record_Number  long
Original_EmailAddress   string(255)
Original_Network        string(30)
Original_EndUserTel     String(30)
    CODE

    if pMultiple_Setup = 0 then exit.                       ! Pointer = NULL ?
    MultiJobBkGrp &= (pMultiple_Setup)                      ! Assign pointer reference

    if MultiJobBkGrp.MS_First_Job_No = 0                    ! Is this the first job to be booked ?
        MultiJobBkGrp.MS_First_Job_No = job:Ref_Number
        exit
    end

    Original_Job_No = job:Ref_Number

    Access:Jobs_Alias.ClearKey(job_ali:Ref_Number_Key)
    job_ali:Ref_Number = MultiJobBkGrp.MS_First_Job_No  ! Fetch first job number
    if Access:Jobs_Alias.Fetch(job_ali:Ref_Number_Key) then exit.

    job:record :=: job_ali:record                       ! Copy record buffer
    job:Ref_Number = Original_Job_No                    ! Reset job number

    !message('In multiple job booking')
    ! Inserting (DBH 02/03/2007) # 8816 - Make sure each new job has it's own booking date/time
    job:Date_Booked = Today()
    job:Time_Booked = Clock()
    ! End (DBH 02/03/2007) #8816

    ! Clear fields
    job:ESN = ''
    job:MSN = ''
    job:Mobile_Number = ''
    job:Warranty_Job = 'NO'
    job:Chargeable_Job = 'NO'
    job:Charge_Type = ''
    job:Warranty_Charge_Type = ''
    job:DOP = ''
    ! Accessories (linked record)
    ! Fault description (linked record)

    ! Clear fields from options selected on multiple job booking form
    if MultiJobBkGrp.MS_Model then job:Model_Number = ''.
    if MultiJobBkGrp.MS_Colour then job:Colour = ''.
    if MultiJobBkGrp.MS_Order_No then job:Order_Number = ''.

    if MultiJobBkGrp.MS_Fault_Code
        job:Fault_Code1 = '';   job:Fault_Code2 = '';   job:Fault_Code3 = '';   job:Fault_Code4 = ''
        job:Fault_Code5 = '';   job:Fault_Code6 = '';   job:Fault_Code7 = '';   job:Fault_Code8 = ''
        job:Fault_Code9 = '';   job:Fault_Code10 = '';  job:Fault_Code11 = '';  job:Fault_Code12 = ''
    else ! If we are not blanking fault codes, check if we need to replicate to fault description
        Access:JobNotes.Clearkey(jbn:RefNumberKey)
        jbn:RefNumber   = job:Ref_Number
        if Access:JobNotes.TryFetch(jbn:RefNumberKey)   ! Fetch JobNotes record for fault description
            if not Access:JobNotes.PrimeRecord()
                jbn:RefNumber   = job:ref_number
                Access:JobNotes.TryInsert()
            end
        end

        Access:ManFault.ClearKey(maf:Field_Number_Key)  ! Lookup fault code descriptions to write into
        maf:Manufacturer = job:Manufacturer             ! the fault description field.
        set(maf:Field_Number_Key,maf:Field_Number_Key)
        loop until Access:ManFault.Next()
            if maf:Manufacturer <> job:Manufacturer then break.
            if upper(maf:ReplicateFault) <> 'YES' then cycle.
            Access:ManFaulO.ClearKey(mfo:Field_Key)
            mfo:Manufacturer = job:Manufacturer
            mfo:Field_Number = maf:Field_Number

            execute maf:Field_Number
                mfo:Field = job:Fault_Code1
                mfo:Field = job:Fault_Code2
                mfo:Field = job:Fault_Code3
                mfo:Field = job:Fault_Code4
                mfo:Field = job:Fault_Code5
                mfo:Field = job:Fault_Code6
                mfo:Field = job:Fault_Code7
                mfo:Field = job:Fault_Code8
                mfo:Field = job:Fault_Code9
                mfo:Field = job:Fault_Code10
                mfo:Field = job:Fault_Code11
                mfo:Field = job:Fault_Code12
            end

            if not Access:ManFaulO.Fetch(mfo:Field_Key)
                jbn:Fault_Description = clip(jbn:Fault_Description) & mfo:Description & '<13,10>'
            end
        end
        Access:JobNotes.TryUpdate()
    end

    ! //-----------------------------------------------------------------------------
    if not MultiJobBkGrp.MS_Engineers_Notes ! Replicate job notes

        Access:JobNotes.Clearkey(jbn:RefNumberKey)
        jbn:RefNumber   = MultiJobBkGrp.MS_First_Job_No
        if not Access:JobNotes.TryFetch(jbn:RefNumberKey)
            Original_Eng_Notes = jbn:Engineers_Notes

            Access:JobNotes.Clearkey(jbn:RefNumberKey)
            jbn:RefNumber   = job:Ref_Number
            if Access:JobNotes.TryFetch(jbn:RefNumberKey)
                if not Access:JobNotes.PrimeRecord()
                    jbn:RefNumber   = job:ref_number
                    jbn:Engineers_Notes = Original_Eng_Notes
                    Access:JobNotes.TryInsert()
                end
            else ! Notes already exist ?
                jbn:Engineers_Notes = Original_Eng_Notes
                Access:JobNotes.TryUpdate()
            end

        end

    end

    ! //-----------------------------------------------------------------------------
    if not MultiJobBkGrp.MS_Contact_History ! Replicate contact history

        Access:ContHist.ClearKey(cht:Ref_Number_Key)
        cht:Ref_Number = MultiJobBkGrp.MS_First_Job_No
        set(cht:Ref_Number_Key,cht:Ref_Number_Key)
        loop until Access:ContHist.Next()
            if cht:Ref_Number <> MultiJobBkGrp.MS_First_Job_No then break.
            ContHistQ.ContHistBuffer :=: cht:Record
            add(ContHistQ)
        end

        loop
            get(ContHistQ,Count)
            if error() then break.
            if not Access:ContHist.PrimeRecord()
                Original_Record_Number = cht:Record_Number

                cht:Record :=: ContHistQ.ContHistBuffer       ! Copy record buffer
                cht:Record_Number = Original_Record_Number    ! Reset autoincrementing value
                cht:Ref_Number = job:Ref_Number               ! Update job number
                cht:Date = today()
                cht:Time = clock()
                Access:ContHist.TryUpdate()
            end
            Count += 1
        end

    end

    ! //-----------------------------------------------------------------------------
    ! Need to replicate some fields held in the Job extension file   

    Access:JobSE.ClearKey(jobe:refnumberkey)
    jobe:refnumber = MultiJobBkGrp.MS_First_Job_No
    if not Access:JobSE.Fetch(jobe:refnumberkey)            ! Fetch first job booked
        Original_EmailAddress = jobe:EndUserEmailAddress    ! Save fields to copy
        Original_Network = jobe:Network
        Original_EndUserTel = jobe:EndUserTelNo

        Access:JobSE.ClearKey(jobe:refnumberkey)
        jobe:refnumber = job:Ref_Number
        if not Access:JobSE.Fetch(jobe:refnumberkey)        ! Update current job
            jobe:EndUserEmailAddress = Original_EmailAddress
            jobe:Network = Original_Network
            jobe:EndUserTelNo = Original_EndUserTel
            Access:JobSE.TryUpdate()

            tmp:Network = jobe:Network                      ! Form uses temp. variable
            tmp:EndUserTelNo = jobe:EndUserTelNo
        end

    end
    If job:Account_Number <> ''
        !Should only do this for the second time through the screen
        Post(Event:Accepted,?job:Account_Number)
    End !If job:Account_Number <> ''


!O
OBFCheck            Routine
Data
local:OBF           Byte(0)
local:ValidationString  String(255)
Code
    If tmp:OBF_Flag = 1
        Do SetJOBSE
! Change --- Pass if the DOP is disabled (DBH: 06/03/2009) #10614
!Local:OBF = OutOfBoxFailure_New(local:ValidationString)
! To --- (DBH: 06/03/2009) #10614
        local:OBF = OutOfBoxFailure_New(local:ValidationString,?job:DOP{prop:Disable})
! end --- (DBH: 06/03/2009) #10614
        Do GetJOBSE

        ! Count the bouncers (DBH: 09/03/2007)
        Bouncers# = 0
        Bouncers# = CountHistory(job:ESN,job:Ref_Number,job:Date_Booked)

        If local.ValidateIMEINumber(Bouncers#)
            glo:Select24 = 'CONTINUE BOOKING'
            Post(Event:CloseWindow)
            job:ESN = ''
            job:Unit_Type = ''
            job:Manufacturer = ''
            job:Model_Number = ''
            Select(?job:ESN)
            Hide(?Bouncer_Text)
        Else ! If local.ValidateIMEINumber(Bouncers#)
            If local:OBF = 1
                ! OBF is true (DBH: 09/03/2007)
                job:POP = 'O'
                jobe:OBFValidated = 1
                jobe:OBFValidateDate = Today()
                jobe:OBFValidateTime = Clock()
                ! Set engineer option to "ARC Repair" (DBH: 09/03/2007)
                jobe:Engineer48HourOption = 2
                Access:JOBSE.update()

                Do GetJOBSE

                tmp:HubRepair = 1
                ?tmp:HubRepair{prop:Disable} = 1
                job:Incoming_Consignment_Number = ''

                ! Set to warranty job (DBH: 09/03/2007)
                job:Warranty_Job = 'YES'
                job:Warranty_Charge_Type = 'WARRANTY (OBF)'
                ?Lookup_Warranty_Charge_Type{prop:Disable} = 1
                ?job:Warranty_Job{prop:Disable} = 1
                ?job:Chargeable_Job{prop:Disable} = 1

                If Access:JOBACC.PrimeRecord() = Level:Benign
                    jac:Ref_Number = job:Ref_Number
                    jac:Accessory = 'MANUALS'
                    jac:Damaged = 0
                    If glo:WebJob
                        jac:Attached = False
                    Else ! If glo:WebJob
                        jac:Attached = True
                    End ! If glo:WebJob
                    If Access:JOBACC.TryInsert() = Level:Benign
                        !Insert
                    Else ! If Access:JOBACC.TryInsert() = Level:Benign
                        Access:JOBACC.CancelAutoInc()
                    End ! If Access:JOBACC.TryInsert() = Level:Benign
                End ! If Access.JOBACC.PrimeRecord() = Level:Benign

                If Access:JOBACC.PrimeRecord() = Level:Benign
                    jac:Ref_Number = job:Ref_Number
                    jac:Accessory = 'ORIGINAL PACKAGING'
                    jac:Damaged = 0
                    If glo:WebJob
                        jac:Attached = False
                    Else ! If glo:WebJob
                        jac:Attached = True
                    End ! If glo:WebJob
                    If Access:JOBACC.TryInsert() = Level:Benign
                        !Insert
                    Else ! If Access:JOBACC.TryInsert() = Level:Benign
                        Access:JOBACC.CancelAutoInc()
                    End ! If Access:JOBACC.TryInsert() = Level:Benign
                End ! If Access.JOBACC.PrimeRecord() = Level:Benign

                ! Cannot auto add accessories anymore (DBH: 09/03/2007)
                !Put something in the audit trail
                If AddToAudit(job:Ref_Number,'JOB','O.B.F. VALIDATION ACCEPTED','ORIGINAL DEALER <13,10>'& jobe:OriginalDealer & '<13,10>BRANCH OF RETURN<13,10>'&  jobe:BranchOfReturn)

                End ! If AddToAudit(job:Ref_Number,'JOB','O.B.F. VALIDATION ACCEPTED','ORIGINAL DEALER <13,10>'& jobe:OriginalDealer & '<13,10>BRANCH OF RETURN<13,10>'&  jobe:BranchOfReturn)
            Else ! If OBF# = 1
                Case Missive('The OBF Check has Failed.'&|
                  '|Please now select a new Transit Type for this job.','ServiceBase 3g',|
                               'mexclam.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                Loop
                    If ThisWindow.Run(1,SelectRecord) = RequestCompleted
                        If Access:TRANTYPE.TryFetch(trt:Transit_Type_Key) = Level:Benign
                            !Found
                        Else ! If Access:TRANTYPE.TryFetch(trt:Transit_Type_Key) = Level:Benign
                            !Error
                        End ! If Access:TRANTYPE.TryFetch(trt:Transit_Type_Key) = Level:Benign

                        If trt:OBF = 1
                            Case Missive('The OBF has failed. You must select a Non-OBF Transit Type.','ServiceBase 3g',|
                                           'mstop.jpg','/OK')
                                Of 1 ! OK Button
                            End ! Case Missive
                        Else ! If trt:OBF = 1
                            job:Transit_Type = trt:Transit_Type
                            Break
                        End ! If trt:OBF = 1
                    Else ! If Self.Run(1,SelectRecod) = RequestCompleted
                        Case Missive('The OBF has failed. You must select another Transit Type for this job.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                    End ! If Self.Run(1,SelectRecod) = RequestCompleted
                End !Loop
                Post(Event:Accepted,?job:Transit_Type)

                tmp:OBF_Flag = 0
                If glo:WebJob
                    tmp:HubRepair = False
                End ! If glo:WebJob
                ?tmp:HubRepair{prop:Disable} = False
                !Put something in the audit trail
                If AddToAudit(job:Ref_Number,'JOB','O.B.F. VALIDATION FAILED',Clip(local:ValidationString))

                End ! If AddToAudit(job:Ref_Number,'JOB','O.B.F. VALIDATION FAILED',glo:Select3)
                If job:DOP <> ''
                    ! If the DOP has been entered during the obf process, autofill in the necessary (DBH: 12/03/2007)
                    Post(Event:Accepted,?job:DOP)
                End ! If job:DOP <> ''
            End ! If OBF# = 1
        End ! If local.ValidateIMEINumber(Bouncers#)
    Else ! If tmp:OBF_Flag = 1
        ?tmp:HubRepair{prop:Disable} = 0
    End ! If tmp:OBF_Flag = 1
!P
Pricing     Routine
    !Add new prototype - L945 (DBH: 04-09-2003)
    JobPricingRoutine(0)
    Display()
!R
RemoveWebJob        Routine
    ! Webjob should hopefully be automatically deleted by child relationship - TrkBs: 5904 (DBH: 22-09-2005)

    Access:WEBJOB.Clearkey(wob:RefNumberKey)
    wob:RefNumber   = job:Ref_Number
    If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        !Found
        Delete(WEBJOB)
    Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        !Error
    End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
Replicate_Job routine
    ! To use the job replication routine you need :- GLO:Select30 = 'REPLICATE'
    !                                                GLO:Select31 = Job number to replicate

    data
jobNumToReplicate   long
jobNumCurrent       long
jobNotesBuffer      like(jbn:record)
jobSEBuffer         like(jobe:record)
jobSE2Buffer        like(jobe2:record)
WebJobBuffer        like(wob:record)
jobSERecordID       long
JobSE2RecordID      long
webjobRecordID      long
count               long
    code

    !message('Just before the replicate jobs stuff. Select30 = '&clip(glo:Select30)&'|Select32 = '&clip(glo:Select32))

    if clip(GLO:Select30) <> 'REPLICATE' then exit.

    If Glo:Select32 <> 'COPY' then
        Clear(wob:Record)
    End !If Glo:Select32 <> 'COPY' then

    jobNumToReplicate = GLO:Select31
    jobNumCurrent = job:Ref_Number

    ! Fetch job record to replicate
    Access:Jobs_Alias.ClearKey(job_ali:Ref_Number_Key)
    job_ali:Ref_Number = jobNumToReplicate
    if Access:Jobs_Alias.Fetch(job_ali:Ref_Number_Key) then exit.

    job:record :=: job_ali:record                       ! Copy record buffer
    GLO:Select31 = jobNumCurrent
    job:Ref_Number = jobNumCurrent                      ! Reset job number

    !Added By Paul 18/09/09 Log No 11079
    !dont clear the fields if this is being copied for a QA Fail
    If GLO:Select32 <> 'COPY' then
        do Clear_Job_Fields
    End !If GLO:Select32 <> 'COPY' then

    ! Current JOBSE record should already be fetched, see self.Init()
    jobSERecordID = jobe:RecordNumber
    Access:JOBSE.TryUpdate()

    Access:JOBSE.ClearKey(jobe:RefNumberKey)
    jobe:RefNumber = jobNumToReplicate
    if not Access:JOBSE.Fetch(jobe:RefNumberKey)
        jobSEBuffer :=: jobe:record
    end !if not Access:JOBSE.Fetch(jobe:RefNumberKey)

    Access:JOBSE.ClearKey(jobe:RefNumberKey)
    jobe:RefNumber = job:Ref_Number
    if not Access:JOBSE.Fetch(jobe:RefNumberKey)
        jobe:record :=: jobSEBuffer                       ! Copy record buffer
        jobe:RecordNumber = jobSERecordID
        jobe:RefNumber = job:Ref_Number
        !message('Have glo:Select32 as '&clip(Glo:Select32))
        If GLO:Select32 <> 'COPY' then
            do Clear_JobSE_Fields
        End !If GLO:Select32 <> 'COPY' then

        Access:JOBSE.TryUpdate()
    end

    !add the jobse2 record as well
    jobSE2RecordID = jobe2:RecordNumber
    Access:JOBSE2.TryUpdate()

    Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
    jobe2:RefNumber = jobNumToReplicate
    if not Access:JOBSE2.Fetch(jobe2:RefNumberKey)
        jobSE2Buffer :=: jobe2:record
    end

    Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
    jobe2:RefNumber = job:Ref_Number
    if not Access:JOBSE2.Fetch(jobe2:RefNumberKey)
        jobe2:record :=: jobSE2Buffer                       ! Copy record buffer
        jobe2:RecordNumber = jobSE2RecordID
        jobe2:RefNumber = job:Ref_Number


        Access:JOBSE2.TryUpdate()
    end

    !copy the webjobs if its a copy
    If clip(GLO:Select32) = 'COPY' then
        !message('about to copy webjob')
        webjobRecordID = wob:RecordNumber
        Access:Webjob.TryUpdate()

        Access:Webjob.ClearKey(wob:RefNumberKey)
        wob:RefNumber = jobNumToReplicate
        if not Access:Webjob.Fetch(wob:RefNumberKey)
            WebJobBuffer :=: wob:record
        end !if not Access:Webjob.Fetch(wob:RefNumberKey)

        Access:Webjob.ClearKey(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        if not Access:Webjob.Fetch(wob:RefNumberKey)
            wob:record :=: WebJobBuffer                       ! Copy record buffer
            wob:RecordNumber = webjobRecordID
            wob:RefNumber = job:Ref_Number
            ! #12131 QA ARC Fail is replicating the webjob record including the fact is has been completed.
            !Which is corrupting the TAT report, and any other report which uses the webjob completed key. (Bryan: 03/06/2011)
            wob:DateCompleted = ''
            wob:TimeCompleted = ''
            wob:Completed = 'NO'


            Access:Webjob.TryUpdate()
        end !if not Access:Webjob.Fetch(wob:RefNumberKey)
        !now repopulate the temp fields with the current data
        Do GetWebJob
    End !If GLO:Select32 = 'COPY' then



    ! Copy job notes
    Access:JobNotes.ClearKey(jbn:RefNumberKey)
    jbn:RefNumber = jobNumToReplicate
    if not Access:JobNotes.Fetch(jbn:RefNumberKey)
        jobNotesBuffer :=: jbn:record
    end

    Access:JobNotes.Clearkey(jbn:RefNumberKey)
    jbn:RefNumber = job:Ref_Number
    if Access:JobNotes.Fetch(jbn:RefNumberKey)
        if not Access:JobNotes.PrimeRecord()
            jbn:record :=: jobNotesBuffer
            jbn:RefNumber = job:ref_number

            Access:JobNotes.TryInsert()
        end
    else ! Notes already exist
        jbn:record :=: jobNotesBuffer
        jbn:RefNumber = job:ref_number
        Access:JobNotes.TryUpdate()
    end

    ! Copy accessories
    free(JobAccQ)

    Access:JobAcc.ClearKey(jac:ref_number_key)
    jac:ref_number = jobNumToReplicate
    set(jac:ref_number_key, jac:ref_number_key)
    loop until Access:JobAcc.Next()
        if jac:ref_number <> jobNumToReplicate then break.
        JobAccQ.JobAccBuffer :=: jac:record
        add(JobAccQ)
    end



    count = 1


    loop x# = 1 to records(JobAccQ)
        get(JobAccQ,x#)
        if error() then
            break
        End
        jac:record :=: JobAccQ.JobAccBuffer       ! Copy record buffer
        jac:Ref_Number = job:ref_number

        Access:JOBACC.TryInsert()
    End



!    loop
!        get(JobAccQ, count)
!        if error() then break.
!        if not Access:JOBACC.PrimeRecord()
!            jac:record :=: JobAccQ.JobAccBuffer       ! Copy record buffer
!            jac:Ref_Number = job:ref_number
!
!            Access:JOBACC.TryUpdate()
!            !message('accessory added = ' & clip(jac:Accessory))
!        end
!        count += 1
!    end

    free(JobAccQ)

    tmp:Network = jobe:Network
Replicate_Job:Process_Fields routine
    ! Post accepted events to certain fields
    if clip(GLO:Select30) <> 'REPLICATE' then exit.

    if job:Warranty_Job = 'YES' then enable(?Lookup_Warranty_Charge_Type).

    !Check for Bouncers

    job:bouncer = ''
    hide(?repair_history)
    hide(?Bouncer_Text)

    Bouncers# = 0
!    Bouncers# = CountBouncer(job:Ref_Number,job:Date_Booked,job:ESN,|
!                             job:Chargeable_Job,job:Charge_Type,job:Repair_Type,|
!                             job:Warranty_Job,job:Warranty_Charge_Type,job:Repair_Type_Warranty)

    Bouncers# = CountHistory(job:ESN,job:Ref_number,job:Date_Booked)
    if Bouncers# <> 0
        job:bouncer = 'B'
        ?bouncer_text{prop:text} = Clip(Bouncers#) & ' PREV. JOB(S)'
        !?Repair_History{prop:Text} = 'Prev. Job(s): ' & Bouncers#
    end

    do bouncer_text

    if job:bouncer = 'B'
        do set_Bouncer_Charge_type
    end

    Thiswindow.update()
    display()
!S
Save_Fields                Routine
    esn_temp            = job:esn
    location_temp       = job:location
    account_number_temp = job:account_number
    transit_Type_temp   = job:transit_type

set_Bouncer_Charge_type       Routine
! Deleting (DBH 23/10/2006) # 8389 - Why is this being set at booking?
!    if job:chargeable_job='YES' then
!        job:charge_type = GETINI('BOUNCER','ChargeableChargeType',,CLIP(PATH())&'\SB2KDEF.INI')
!    END !If chargeable
!    if job:warranty_job = 'YES'
!        job:Warranty_charge_type = GETINI('BOUNCER','WarrantyChargeType',,CLIP(PATH())&'\SB2KDEF.INI')
!    END !if warranty job
!    !Select from defaults?
! End (DBH 23/10/2006) #8389
ShowCompletedText       Routine
    If job:Date_Completed <> ''
        ?Completed{prop:hide} = 0
        ?Completed{prop:Text} = 'JOB COMPLETED'
        ?Completed{prop:FontColor} = color:Red
        ?Completed{prop:FontSize} =15
    Else !If job:Date_Completed <> ''
        !Estimate Details
        If job:Estimate = 'YES' and job:Chargeable_Job = 'YES'
            ?Completed{prop:Text} = 'Estimate If Over: ' & Clip(left(format(job:Estimate_If_Over,@N10.2))) & '  Status: '
            ?Completed{prop:Hide} = 0
            ?Completed{prop:FontColor} = color:Red !color:purple
            ?Completed{prop:FontSize} = 10 !10
            If job:Estimate_Accepted = 'YES'
                ?Completed{prop:Text} = Clip(?Completed{prop:Text}) & ' Accepted'
            Else !If job:EstimateAccepted = 'YES'
                If job:Estimate_Rejected = 'YES'
                    ?Completed{prop:Text} = Clip(?Completed{prop:Text}) & ' Rejected'
                Else !If job:Estimate_Rejected = 'YES'
                    If job:Estimate_Ready = 'YES'
                        ?Completed{prop:Text} = Clip(?Completed{prop:Text}) & ' Ready'
                    Else !If job:Estimate_Ready = 'YES'
                        ?Completed{prop:Text} = Clip(?Completed{prop:Text}) & ' In Progress'
                    End !If job:Estimate_Ready = 'YES'

                End !If job:Estimate_Rejected = 'YES'
            End !If job:EstimateAccepted = 'YES'
        Else
            ?Completed{prop:Hide} = 1
        End !If job:Estimate = 'YES'
    End !If job:Date_Completed <> ''
ShowTitle       Routine
    If glo:WebJob
        If tmp:wobJobNumber > 0
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number  = ClarioNET:Global.Param2
            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                ! Found
                Left_Title_Temp = 'Repair Details. Job No: ' & tmp:wobRefNumber & '-' & Clip(tra:BranchIdentification) & tmp:wobJobNumber
            Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                ! Error
                Left_Title_Temp = 'Repair Details. Job No: ' & Clip(job:Ref_Number)
            End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        Else ! If tmp:wobJobNumber > 0
            Left_Title_Temp = 'Repair Details. Job No: ' & Clip(job:Ref_Number)
        End ! If tmp:wobJobNumber > 0
    Else ! If glo:WebJob
        Left_Title_Temp = 'Repair Details. Job No: ' & Clip(job:Ref_Number)
    End ! If glo:WebJob

    Right_Title_Temp = 'Date Booked: ' & Format(job:Date_Booked,@d6b) & ' ' & Format(job:Time_Booked,@t1) & ' - ' & Clip(job:Who_Booked)
    Display()
!T
Time_Remaining          Routine      !Turnaround Time Nightmare
Trade_Account_Lookup_Bit        Routine
!U
Update_accessories      Routine
    count# = 0
    setcursor(cursor:wait)
    save_jac_id = access:jobacc.savefile()
    access:jobacc.clearkey(jac:ref_number_key)
    jac:ref_number = job:ref_number
    set(jac:ref_number_key,jac:ref_number_key)
    loop
        if access:jobacc.next()
           break
        end !if
        if jac:ref_number <> job:ref_number      |
            then break.  ! end if
        count# += 1
    end !loop
    access:jobacc.restorefile(save_jac_id)
    setcursor()

    If count# = 1
        access:jobacc.clearkey(jac:ref_number_key)
        jac:ref_number = job:ref_number
        Set(jac:ref_number_key,jac:ref_number_key)
        access:jobacc.next()
        Accessories_Temp = jac:accessory
    Else
        accessories_temp = Clip(count#)
    End
    Display()
!V
Validate_Motorola   ROUTINE
    !Do not validate anything if the job is warranty obf - L942 (DBH: 01-09-2003)
    If job:Warranty_Job = 'YES' and job:Warranty_Charge_Type = 'WARRANTY (OBF)'
        Exit
    End !If job:Warranty_Job = 'YES' and job:Warranty_Charge_Type = 'WARRANTY (OBF')
    ! DBH #10544 - Do nothing if it's a Liquid Damage Booking
    If (?tmp:Booking48HourOption:Radio4{prop:Hide} = 0)
        Exit
    end


    IF job:POP = ''
      Glo:Select1 = ''
    ELSE
      Glo:Select1 = job:pop
    END

    IF job:dop = ''
      Glo:Select2 = ''
    ELSE
      Glo:Select2 = job:dop
    END

    !Glo:Select2 = ''
!    glo:Select3 = 'NONE' ! #11912 Allow for the POP Type to be saved (Bryan: 01/02/2011)
    glo:Select3 = tmp:POPType


   ! Inserting (DBH 02/10/2006) # 8275 - Only validate if cancel isn't pressed
    ! Change --- Pass if the DOP field has been disabled (DBH: 26/02/2009) #10591
    !If POP_Validate2()
    ! To --- (DBH: 26/02/2009) #10591
    If POP_Validate2(?job:DOP{prop:Disable})
    ! end --- (DBH: 26/02/2009) #10591
   ! End (DBH 02/10/2006) #8275
       tmp:POPType = glo:Select3

       IF Glo:Select1 = ''
         !Nothing done!
         job:Warranty_Job = 'NO'
         job:Warranty_Charge_type = ''
       ELSE
         job:POP = Glo:Select1
         IF Glo:Select1 = 'F'
           job:Warranty_Job = 'YES'
           Post(Event:Accepted,?job:Warranty_Job)
           job:Warranty_Charge_Type = 'WARRANTY (MFTR)'
           !Post(Event:Accepted,?job:Warranty_Charge_Type)
           job:Chargeable_Job = 'NO'
           Post(Event:Accepted,?job:Chargeable_Job)
           job:Charge_Type = ''
           ENABLE(?Job:Chargeable_Job)
           ENABLE(?Job:Warranty_Job)
           Job:DOP = Glo:Select2
           Post(Event:Accepted,?Job:DOP)
           UPDATE()
           DISPLAY()
         END

         IF Glo:Select1 = 'S'
           job:Warranty_Job = 'YES'
           Post(Event:Accepted,?job:Warranty_Job)
           job:Warranty_Charge_Type = 'WARRANTY (2ND YR)'
           !Post(Event:Accepted,?job:Warranty_Charge_Type)
           job:Chargeable_Job = 'NO'
           Post(Event:Accepted,?job:Chargeable_Job)
           job:Charge_Type = ''
           ENABLE(?Job:Chargeable_Job)
           ENABLE(?Job:Warranty_Job)
           Job:DOP = Glo:Select2
           Post(Event:Accepted,?Job:DOP)
           UPDATE()
           DISPLAY()
         END

         IF Glo:Select1 = 'O'
           job:Warranty_Job = 'YES'
           Post(Event:Accepted,?job:Warranty_Job)
           job:Warranty_Charge_Type = 'WARRANTY (OBF)'
           !Post(Event:Accepted,?job:Warranty_Charge_Type)
           job:Chargeable_Job = 'NO'
           Post(Event:Accepted,?job:Chargeable_Job)
           job:Charge_Type = ''
           ENABLE(?Job:Chargeable_Job)
           ENABLE(?Job:Warranty_Job)
           Job:DOP = Glo:Select2
           Post(Event:Accepted,?Job:DOP)
           UPDATE()
           DISPLAY()
         END

         IF Glo:Select1 = 'C'
           job:Warranty_Job = 'NO'
           Post(Event:Accepted,?job:Warranty_Job)
           job:Chargeable_Job = 'YES'
           Post(Event:Accepted,?job:Chargeable_Job)
           job:Warranty_Charge_Type = ''
           DISABLE(?Job:Warranty_Job)
           ENABLE(?Job:Chargeable_Job)
           !Post(Event:Accepted,?job:Warranty_Charge_Type)
           !message('PaulMessage 4')
           job:Charge_Type = 'NON-WARRANTY'
           !Post(Event:Accepted,?job:Charge_Type)
           ! Inserting (DBH 03/10/2006) # 8275 - Record any change of DOP
           Job:DOP = Glo:Select2
           ! End (DBH 03/10/2006) #8275
           UPDATE()
           DISPLAY()
         END

       END
   End ! If POP_Validate2()

   Glo:Select1 = ''
   Glo:Select2 = ''
!W
Web_Check_Imei   routine
    GLO:Select1 = '' !Might get changed if the lookup on the internet was valid
    glo:Select2 = ''
    tmp:IMEIValid = 'NO'

    !CHECK FOR THIS MANUFACTURER FIRST
    webcheckstring = getini('GLOBAL',job:Manufacturer,'NO',path()&'\WEBCHECk.INI')
    if webcheckstring = 'YES'
        !NOW CHECK FOR GENERAL USE
        webcheckstring =getini('GLOBAL','WEBCHECK','MIS',path()&'\WEBCHECK.INI')
        Case WebcheckString
            of 'YES'
                GetDetails(job:manufacturer,job:ESN)
            of 'MIS'
                putiniext('GLOBAL','WEBCHECK','YES',path()&'\WEBCHECK.INI')
                !Do nothing for anything else
                GetDetails(job:manufacturer,job:ESN)
        END  !Case

        !Now to set up the checking details
        CASE GLO:Select1
            of 'FAIL'
            !Failure to connect
                Case Missive('Error. Warranty validation failed.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive

                !job:Current_Status = '140 AWAITING WARRANTY CHECK'
                GetStatus(140,0,'JOB')
                tmp:IMEIValid = 'NO'
            of 'TIME'
            !TIMEOUT
                Case Missive('Error. Warranty validation timed out.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive

                !job:Current_Status = '141 FAILED WARRANTY CHECK'
                GetStatus(141,0,'JOB')
                tmp:IMEIValid = 'TIM'
            of ''
               !didn't get called?
               tmp:IMEIValid = 'NO'
               !job:Current_status =  '140 AWAITING WARRANTY CHECK'
               GetStatus(140,0,'JOB')
            ELSE
                !Anything else must be the IMEI
                !Sucess!
                tmp:IMEIValid = 'YES'
                job:Warranty_Job = 'YES'
                GetStatus(Sub(trt:Initial_Status,1,3),0,'JOB')
                if GLO:Select2 <> '' then JOB:MSN = GLO:Select2.
                if GLO:Select5 = 'YES' !this is a nokia within Warranty
                    job:Warranty_Job = 'YES'
                    job:DOP = GLO:Select3    !Warranty start Date
                END
                IF GLO:Select2 <> ''
                  POST(Event:Accepted,?Job:MSN)
                END
                UPDATE()
                DISPLAY()
        END !Case
    end

    ! Web validation not enabled
    if getini('WARVALIDATIONFILE', job:manufacturer, '0', path() & '\SB2KDEF.INI') = '1'
        if tmp:IMEIValid <> 'YES' ! Skip local file validation if online check was successful
            ! Local file validation enabled
            case OfflineIMEIValidation(job:manufacturer, job:ESN)
                of 'FAIL'
                    ! Failure to validate IMEI
                    GetStatus(140, 0, 'JOB') ! Awaiting warranty check
                    tmp:IMEIValid = 'NO'
                of 'SUCCESS'
                    ! Success
                    tmp:IMEIValid = 'YES'
                    job:Warranty_Job = 'YES'
                    GetStatus(Sub(trt:Initial_Status,1,3),0,'JOB')
                    job:DOP = GLO:Select3 ! Warranty start Date
                    update()
                    display()
                else
                    ! Does not apply to the selected manufacturer
            end
        end
    end

    !Check the DOP - L949 (DBH: 15-09-2003)
    Post(Event:Accepted,?job:DOP)
Waybill:Check Routine
    ! Check if this job requires inserting into the 'waybill generation - awaiting processing' table

    ! DBH #10544 - Auto add to waybill process if 'Liquid Damage' is selected.
    if (tmp:Booking48HourOption = 4)
        jobe:HubRepair = 1
        jobe:HubRepairDate = Today()
        jobe:HubRepairTime = Clock()
        stanum#           = Sub(GETINI('RRC','StatusSendToARC',,clip(path()) & '\SB2KDEF.INI'), 1, 3)
        GetStatus(stanum#, 0, 'JOB')
        access:JOBSE.tryupdate()
    end

    if jobe:HubRepair

        if job:Incoming_Consignment_Number <> ''                    ! Job must have an empty consignment number
            do Waybill:CheckRemoveFromAwaitProcess
            exit
        end

        if job:Location <> GETINI('RRC','RRCLocation',,CLIP(PATH())&'\SB2KDEF.INI')
            do Waybill:CheckRemoveFromAwaitProcess
            exit
        end


        ! Shouldn't need to reget the web job. Should be open - TrkBs: 5904 (DBH: 22-09-2005)
!        Access:WEBJOB.ClearKey(wob:RefNumberKey)                    ! Fetch web job
!        wob:RefNumber = job:Ref_Number
!        if Access:WEBJOB.TryFetch(wob:RefNumberKey) then exit.
        ! End   - Shouldn't need to reget the web job. Should be open - TrkBs: 5904 (DBH: 22-09-2005)

        Access:WAYBAWT.ClearKey(wya:AccountJobNumberKey)            ! Does this record already exist ?
        wya:AccountNumber = tmp:HeadAccountNumber
        wya:JobNumber     = job:Ref_Number
        if Access:WAYBAWT.Fetch(wya:AccountJobNumberKey)
            if not Access:WAYBAWT.PrimeRecord()                     ! Insert new waybill 'awaiting processing' record
                wya:JobNumber     = job:Ref_Number
                wya:AccountNumber = tmp:HeadAccountNumber
                wya:Manufacturer  = job:Manufacturer
                wya:ModelNumber   = job:Model_Number
                wya:IMEINumber    = job:ESN
                if Access:WAYBAWT.Update()
                    Access:WAYBAWT.CancelAutoInc()
                end
            end
        end

    else
        do Waybill:CheckRemoveFromAwaitProcess
    end
Waybill:CheckRemoveFromAwaitProcess Routine
    ! Remove waybill 'awaiting processing' record (not actual waybill record(!))

        ! Shouldn't need to reget the web job. Should be open - TrkBs: 5904 (DBH: 22-09-2005)
!        Access:WEBJOB.ClearKey(wob:RefNumberKey)                    ! Fetch web job
!        wob:RefNumber = job:Ref_Number
!        if Access:WEBJOB.TryFetch(wob:RefNumberKey) then exit.
        ! End   - Shouldn't need to reget the web job. Should be open - TrkBs: 5904 (DBH: 22-09-2005)

    Access:WAYBAWT.ClearKey(wya:AccountJobNumberKey)            ! Does this record already exist ?
    wya:AccountNumber = tmp:HeadAccountNumber
    wya:JobNumber = job:Ref_Number
    if not Access:WAYBAWT.Fetch(wya:AccountJobNumberKey)
        Relate:WAYBAWT.Delete(0)                                ! Delete record if it exists
    end

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Job'
  OF ChangeRecord
    ActionMessage = 'Changing A Job'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020500'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  if clip(GLO:Select30) = 'REPLICATE'
      !message('Glo:Select30 was replicate???')
      glo:insert_global = 'YES'
  end
  If glo:insert_global    = 'YES'  or glo:insert_global = 'PRE'
      Globalrequest   = Insertrecord
  End!If insert_global    = 'YES'
  
  
  GlobalErrors.SetProcedureName('Update_Jobs')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(job:Record,History::job:Record)
  SELF.AddHistoryField(?job:Transit_Type,30)
  SELF.AddHistoryField(?job:ESN,16)
  SELF.AddHistoryField(?job:MSN,17)
  SELF.AddHistoryField(?job:ProductCode,18)
  SELF.AddHistoryField(?job:Model_Number,14)
  SELF.AddHistoryField(?job:Unit_Type,19)
  SELF.AddHistoryField(?job:Colour,20)
  SELF.AddHistoryField(?job:Mobile_Number,70)
  SELF.AddHistoryField(?job:Chargeable_Job,13)
  SELF.AddHistoryField(?job:Charge_Type,36)
  SELF.AddHistoryField(?job:Warranty_Job,12)
  SELF.AddHistoryField(?job:Warranty_Charge_Type,37)
  SELF.AddHistoryField(?job:Intermittent_Fault,32)
  SELF.AddHistoryField(?job:DOP,27)
  SELF.AddHistoryField(?job:Account_Number,39)
  SELF.AddHistoryField(?job:Order_Number,42)
  SELF.AddHistoryField(?job:Title,60)
  SELF.AddHistoryField(?job:Initial,61)
  SELF.AddHistoryField(?job:Surname,62)
  SELF.AddHistoryField(?job:Location,24)
  SELF.AddHistoryField(?job:Authority_Number,25)
  SELF.AddHistoryField(?job:Company_Name,64)
  SELF.AddHistoryField(?job:Address_Line1,65)
  SELF.AddHistoryField(?job:Address_Line2,66)
  SELF.AddHistoryField(?job:Address_Line3,67)
  SELF.AddHistoryField(?job:Postcode,63)
  SELF.AddHistoryField(?job:Company_Name_Collection,72)
  SELF.AddHistoryField(?job:Address_Line1_Collection,73)
  SELF.AddHistoryField(?job:Address_Line2_Collection,74)
  SELF.AddHistoryField(?job:Address_Line3_Collection,75)
  SELF.AddHistoryField(?job:Postcode_Collection,71)
  SELF.AddHistoryField(?job:Company_Name_Delivery,79)
  SELF.AddHistoryField(?job:Address_Line1_Delivery,78)
  SELF.AddHistoryField(?job:Address_Line2_Delivery,80)
  SELF.AddHistoryField(?job:Address_Line3_Delivery,81)
  SELF.AddHistoryField(?job:Postcode_Delivery,77)
  SELF.AddUpdateFile(Access:JOBS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:ACCESSOR.Open
  Relate:COLOUR.Open
  Relate:COMMONCP.Open
  Relate:DEFAULTS.Open
  Relate:DEFRAPID.Open
  Relate:ESNMODEL.Open
  Relate:EXCHANGE.Open
  Relate:JOBS2_ALIAS.Open
  Relate:JOBSE2_ALIAS.Open
  Relate:JOBSE_ALIAS.Open
  Relate:MANUDATE.Open
  Relate:NETWORKS.Open
  Relate:PREJOB.Open
  Relate:STAHEAD.Open
  Relate:SUBURB.Open
  Relate:TRADEACC_ALIAS.Open
  Relate:TRANTYPE.Open
  Relate:TURNARND.Open
  Relate:USELEVEL.Open
  Relate:USERS_ALIAS.Open
  Relate:WEBJOB.Open
  Relate:WEBJOBNO.Open
  Access:JOBSTAGE.UseFile
  Access:AUDIT.UseFile
  Access:USERS.UseFile
  Access:MODELNUM.UseFile
  Access:JOBS_ALIAS.UseFile
  Access:MANUFACT.UseFile
  Access:STATUS.UseFile
  Access:COMMONWP.UseFile
  Access:COMMONFA.UseFile
  Access:MODELCOL.UseFile
  Access:MANFAULT.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:COURIER.UseFile
  Access:UNITTYPE.UseFile
  Access:LOCINTER.UseFile
  Access:JOBSE.UseFile
  Access:TRAFAULO.UseFile
  Access:TRAFAULT.UseFile
  Access:CHARTYPE.UseFile
  Access:JOBNOTES.UseFile
  Access:CONTHIST.UseFile
  Access:MANFAULO.UseFile
  Access:JOBACC.UseFile
  Access:WAYBAWT.UseFile
  Access:LOCATLOG.UseFile
  Access:REPTYDEF.UseFile
  Access:MODPROD.UseFile
  Access:JOBSE2.UseFile
  Access:TRAHUBS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:JOBS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  ! this code was commented, but ...
  ! J - I think we may need this to lock down the job for single jobs
  ! may be able to use the variable
  ! if clip(GLO:Select30) = 'REPLICATE' then set to insertrecord
  
  !unknown author for this comment
  ! I think this code is not necessary, as it is causing the primed information to be lost.
      If ThisWindow.Request = InsertRecord and clip(GLO:Select30) <> 'REPLICATE'
          Pointer# = Pointer(JOBS)
          Hold(JOBS, 1)
          Get(JOBS, Pointer#)
          If Errorcode() = 43  !someone else is using this record
              !TB13290 - J - 08/07/14
              !there was nothing between the error code = 43 and END so nothing was done if the record was in use.
              miss# = missive('There has been a system error on creating a new job, please try again',|
                      'ServiceBase 3g','mexclam.jpg','OK')
              
              Post(event:closewindow)
              !End of additin TB13290
          End ! If Errorcode() = 43
      End ! If ThisWindow.Request = InsertRecord
  
  !TB13028 reset the primed fields - only two of them
  access:users.clearkey(use:password_key)
  use:password    = glo:password
  access:users.fetch(use:password_key)
  job:who_booked  = use:user_code
  !message('reSet who_booked to '&clip(job:who_booked))
  job:incoming_date   = Today()
  self.InsertAction = Insert:Caller
  !if self.Request <> ViewRecord               ! Code to prompt user if they want to insert another job
  !    if pMultiple_Setup = 0                  ! Don't display in multiple job booking mode (it won't replicate details)
  !        self.InsertAction = Insert:Query
  !    end
  !end
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
    quickwindow{prop:buffer} = 1
    ?Sheet2{prop:wizard}     = True

    Access:JOBSE.ClearKey(jobe:refnumberkey)
    jobe:refnumber = job:ref_number
    IF Access:JOBSE.Fetch(jobe:refnumberkey)
        If Access:JOBSE.PrimeRecord() = Level:Benign
            jobe:RefNumber  = job:Ref_Number
            jobe:POPType = 'NONE'
            If Access:JOBSE.TryInsert() = Level:Benign
            ! Insert Successful
            Else ! If Access:JOBSE.TryInsert() = Level:Benign
                ! Insert Failed
                tmp:Network = jobe:Network
            End! If Access:JOBSE.TryInsert() = Level:Benign
        End ! If Access:JOBSE.PrimeRecord() = Level:Benign
    Else
        tmp:Network = jobe:Network
    END! if access jobse fetch worked

    ! Need to make sure the JOBSE fields are filled in
    ! especially when view a job that has already been booked - 3802 (DBH: 14-01-2004)
    Do GetJOBSE

  ! Inserting (DBH 07/04/2006) #7305 - Make sure a jobse2 record exists#
    
    Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
    jobe2:RefNumber   = job:Ref_Number
    If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
        ! Found
        If ThisWindow.Request = InsertRecord
    
            jobe2:SMSNotification = 1
            if glo:insert_global = 'PRE'
                jobe2:IDNumber = PRE:ID_number
                jobe2:SMSAlertNumber = PRE:Mobile_number
    
            END ! if prejob booking

           Access:JOBSE2.TryUPdate()
        End ! If ThisWindow.Request = InsertRecord
        tmp:Contract = jobe2:Contract
        tmp:Prepaid  = jobe2:Prepaid
    Else ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
        ! Error
        If Access:JOBSE2.PrimeRecord() = Level:Benign
            jobe2:RefNumber = job:Ref_Number
            ! Inserting (DBH 07/12/2007) # 9491 - Default the SMS notification to on
            jobe2:SMSNotification = 1
            ! End (DBH 07/12/2007) #9491
            if glo:insert_global = 'PRE'
                jobe2:IDNumber = PRE:ID_number
                jobe2:SMSAlertNumber = PRE:Mobile_number
            END ! if prejob booking

            If Access:JOBSE2.TryInsert() = Level:Benign
            ! Insert Successful

            Else ! If Access:JOBSE2.TryInsert() = Level:Benign
                ! Insert Failed
                Access:JOBSE2.CancelAutoInc()
            End ! If Access:JOBSE2.TryInsert() = Level:Benign
        End ! If Access:JOBSE2.PrimeRecord() = Level:Benign
    End ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
    ! End (DBH 07/04/2006) #7305

    ! _____________________________________________________________________

    Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
    jbn:RefNumber   = job:Ref_Number
    If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
        ! Found

    Else ! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
        ! Error
        If Access:JOBNOTES.PrimeRecord() = Level:Benign
            jbn:RefNumber   = job:Ref_Number
            if PRE:FaultText <> ''
                jbn:Fault_Description = PRE:FaultText
            END
            If Access:JOBNOTES.TryInsert() = Level:Benign
            ! Insert Successful
            Else ! If Access:JOBNOTES.TryInsert() = Level:Benign
                ! Insert Failed
                Access:JOBNOTES.CancelAutoInc()
            End ! If Access:JOBNOTES.TryInsert() = Level:Benign
        End ! If Access:JOBNOTES.PrimeRecord() = Level:Benign
    End ! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign

    ! _____________________________________________________________________

    ! Set up WOB record

    If glo:WebJob
        ! Head account comes from booking account (DBH: 12-10-2005)
        tmp:HeadAccountNumber = Clarionet:Global.Param2
    Else ! If glo:WebJob
        tmp:HeadAccountNumber = GETINI('BOOKING', 'HeadAccount',, Clip(Path()) & '\SB2KDEF.INI')
    End ! If glo:WebJob

    If ThisWindow.Request = InsertRecord
        ! First find the last Job number used
! Changing (DBH 07/08/2006) # 8080 - Use "control file" to get next job number
!        Access:Webjob.clearkey(wob:HeadJobNumberKey)
!        wob:JobNumber         = 999999999
!        wob:HeadAccountNumber = tmp:HeadAccountNumber
!        set(wob:HeadJobNumberKey, wob:HeadJobNumberKey)
!        if access:WebJob.previous()then
!            ! Couldn't find one - set up the first
!            x# = 1
!        Else
!            !        If glo:webJOb
!            If wob:HeadAccountNumber = tmp:HeadAccountNumber
!                x# = wob:JobNumber + 1
!            Else ! If wob:HeadAccountNumber = Clarionet:Global.Param2
!                x# = 1
!            End ! If wob:HeadAccountNumber = Clarionet:Global.Param2
!        end ! if
! to (DBH 07/08/2006) # 8080
        Access:WEBJOBNO.ClearKey(wej:HeadAccountNumberKey)
        wej:HeadAccountNumber = tmp:HeadAccountNumber
        If Access:WEBJOBNO.TryFetch(wej:HeadAccountNumberKey) = Level:Benign
            !Found
            ! Found this account. Add one to last number and use it (DBH: 07/08/2006)
            wej:LastWEBJOBNumber += 1
            Access:WEBJOBNo.Update()
            x# = wej:LastWEBJOBNumber
        Else ! If Access:WEBJOBNO.TryFetch(wej:HeadJobNumberKey) = Level:Benign
            !Error
            ! No entry for this account. Start from 1 (DBH: 07/08/2006)
            If Access:WEBJOBNO.PrimeRecord() = Level:Benign
                wej:HeadAccountNumber = tmp:HEadAccountNumber
                wej:LastWEBJOBNUmber = 1
                x# = 1
                If Access:WEBJOBNO.TryInsert() = Level:Benign
                    !Insert
                Else ! If Access:WEBJOBNO.TryInsert() = Level:Benign
                    Access:WEBJOBNO.CancelAutoInc()
                End ! If Access:WEBJOBNO.TryInsert() = Level:Benign
            End ! If Access.WEBJOBNO.PrimeRecord() = Level:Benign
        End ! If Access:WEBJOBNO.TryFetch(wej:HeadJobNumberKey) = Level:Benign
! End (DBH 07/08/2006) #8080

        Access:WEBJOB.Clearkey(wob:RefNumberKey)
        wob:RefNumber   = job:Ref_Number
        If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        ! Found
        Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
            ! Error
            If Access:WEBJOB.PrimeRecord() = Level:Benign
                wob:JobNumber         = x#
                wob:RefNumber         = job:Ref_Number
                wob:HeadAccountNumber = tmp:HeadAccountNumber
                wob:IMEINumber        = '* IN PROGRESS *'
                If Access:WEBJOB.TryInsert() = Level:Benign
                    ! Insert Successful
                    Access:WEBJOB.Update()
                Else ! If Access:WEBJOB.TryInsert() = Level:Benign
                    ! Insert Failed
                    Access:WEBJOB.CancelAutoInc()
                End ! If Access:WEBJOB.TryInsert() = Level:Benign
            Else ! If Access:WEBJOB.PrimeRecord() = Level:Benign
            End ! If Access:WEBJOB.PrimeRecord() = Level:Benign
        End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
    Else ! If ThisWindow.Request = InsertRecord
        ! #11912 Don't think anything can be changed, but disable it anyway. (Bryan: 01/02/2011)
        ?locPOPConfirmed{prop:Disable} = 1
    End ! If ThisWindow.Request = InsertRecord

    Do GetWebJob


    do Multiple_Job_Booking_Setup


    tmp:Intial_Transit_Type = GETINI(tmp:HeadAccountNumber, 'InitialTransitType',, CLIP(PATH()) & '\SB2KDEF.INI')
    tmp:Trade_Account       = GETINI(tmp:HeadAccountNumber, 'TradeAccount',, CLIP(PATH()) & '\SB2KDEF.INI')

    do BookFrom48HourExchange
    !message('Open window call to replicate_job')
    do Replicate_Job

    if self.Request = InsertRecord
        if tmp:Intial_Transit_Type <> ''
            job:Transit_Type = tmp:Intial_Transit_Type
        end ! if
        if job:Transit_Type <> ''
            post(event:Accepted, ?job:Transit_Type)
        end ! if
        if tmp:Trade_Account <> ''
            job:Account_Number = tmp:Trade_Account
            ! Insert --- This is so the account details are update correctly (DBH: 17/03/2009) #10473
            post(event:Accepted,?job:Account_Number)
            ! end --- (DBH: 17/03/2009) #10473
        end ! if
    end ! if

    ! Check to see if Booking Account has 48 Hour
    ! option activated -  (DBH: 14-10-2003)
    If AccountActivate48Hour(tmp:HeadAccountNumber) = True
        ?tmp:Booking48HourOption:Radio1{prop:Hide} = False
    End ! AccountActivate48Hour(tmp:HeadAccount) = True

    Do ShowTitle
    !Show Fields
    Set(defaults)
    access:defaults.next()
    If def:show_mobile_number = 'YES'
        Hide(?job:mobile_number)
        Hide(?job:mobile_number:prompt)
    Else
        Unhide(?job:mobile_number)
        Unhide(?job:mobile_number:prompt)
    End
    IF job:pop <> ''
      UNHIDE(?Validate_POP)
    ELSE
      HIDE(?Validate_POP)
    END
    If def:HideColour   = 'YES'
        Hide(?job:Colour)
        Hide(?lookup_colour)
        Hide(?job:colour:prompt)
    Else!If def:HideColour   = 'YES'
        UnHide(?job:Colour)
        UnHide(?lookup_colour)
        UnHide(?job:colour:prompt)
    End!If def:HideColour   = 'YES'
    !Vodacom do not use Incoming Courier -  (DBH: 14-10-2003)
  !  If DEF:HideInCourier = 'YES'
  !      Hide(?job:incoming_courier:prompt)
  !      Hide(?job:incoming_courier)
  !      Hide(?lookup_Courier)
  !      Hide(?job:incoming_consignment_number)
  !      Hide(?job:incoming_consignment_number:prompt)
  !      Hide(?job:incoming_date)
  !      HIde(?job:incoming_date:prompt)
  !  Else!If DEF:HideInCourier = 'YES'
  !      UnHide(?job:incoming_courier:prompt)
  !      UnHide(?job:incoming_courier)
  !      Unhide(?lookup_courier)
  !      UnHide(?job:incoming_consignment_number)
  !      UnHide(?job:incoming_consignment_number:prompt)
  !      UnHide(?job:incoming_date)
  !      UnHIde(?job:incoming_date:prompt)
  !
  !  End!If DEF:HideInCourier = 'YES'
    If def:hideproduct = 'YES'
        Hide(?job:productcode)
        Hide(?job:productcode:prompt)
        !Hide(?LookupProductCode)
    End!If def:hideproduct = 'YES'

    If DEF:HideIntFault = 'YES'
        Hide(?job:intermittent_fault)
    Else!If DEF:HideIntFault = 'YES'
        UnHide(?job:intermittent_fault)
    End!If DEF:HideIntFault = 'YES'

    If def:hide_authority_number = 'YES'
        Hide(?Job:authority_number)
        Hide(?job:authority_number:prompt)
    Else
        UnHide(?Job:authority_number)
        UnHide(?job:authority_number:prompt)
    End!If def:hide_authority_number = 'YES'
    If def:HideLocation
        tmp:DisableLocation = GETINI('DISABLE','InternalLocation',,CLIP(PATH())&'\SB2KDEF.INI')
        if tmp:DisableLocation
            ?job:Location{prop:Disable} = 1
        else
            ?job:Location{prop:hide} = 1
            ?job:Location:Prompt{prop:hide} = 1
        end
    End !def:HideLocation

    Do check_Msn

    If GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI') = 1
        ?job:Colour:Prompt{prop:Text} = Clip(GETINI('RENAME','ColourName',,CLIP(PATH())&'\SB2KDEF.INI'))
    End !GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI') = 1

    If GETINI('HIDE','SkillLevel',,CLIP(PATH())&'\SB2KDEF.INI') = 1
        ?tmp:SkillLevel{prop:Hide} = 1
        ?tmp:SkillLevel:prompt{prop:Hide} = 1
    Else !GETINI('HIDE','SkillLevel',,CLIP(PATH())&'\SB2KDEF.INI') = 1
        ?tmp:SkillLevel{prop:Hide} = 0
        ?tmp:SkillLevel:prompt{prop:Hide} = 0
    End !GETINI('HIDE','SkillLevel',,CLIP(PATH())&'\SB2KDEF.INI') = 1

    If GETINI('HIDE','HideNetwork',,CLIP(PATH())&'\SB2KDEF.INI') = 1
        ?tmp:Network{prop:Hide} = 1
        ?tmp:Network:Prompt{prop:Hide} = 1
        ?LookupNetwork{prop:Hide} = 1
    Else !GETINI('HIDE','HideNetwork',,CLIP(PATH())&'\SB2KDEF.INI') = 1
        ?tmp:Network{prop:Hide} = 0
        ?tmp:Network:Prompt{prop:Hide} = 0
        ?LookupNetwork{prop:Hide} = 0
    End !GETINI('HIDE','HideNetwork',,CLIP(PATH())&'\SB2KDEF.INI') = 1

    If GETINI('HIDE','HubRepair',,CLIP(PATH())&'\SB2KDEF.INI') = 1
        ?tmp:HubRepair{prop:Hide} = 1
    Else !GETINI('HIDE','HubRepair',,CLIP(PATH())&'\SB2KDEF.INI') = 1
        ?tmp:HubRepair{prop:Hide} = 0
    End !GETINI('HIDE','HubRepair',,CLIP(PATH())&'\SB2KDEF.INI') = 1

! Inserting (DBH 07/11/2007) # 9200 - Do we do the mobile check?
    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number = wob:HeadAccountNumber
    If Access:TRADEACC.tryFetch(tra:Account_Number_Key) = Level:Benign
        If tra:DoMSISDNCheck
            ?tmp:OverrideMSISDNCheck{prop:Disable} = 0
        End ! If tra:DoMSISDNCheck
    End ! If Access:TRADEACC.tryFetch(tra:Account_Number_Key) = Level:Benign
! End (DBH 07/11/2007) #9200
  !Which Address To Show
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  Case def:Browse_Option
      Of 'INV'
          Select(?Sheet2,1)
      Of 'COL'
          Select(?Sheet2,2)
      Of 'DEL'
          Select(?Sheet2,3)
  End!Case def:Browse_Option
  Post(event:accepted,?job:chargeable_job)
  Post(event:accepted,?job:warranty_job)
  
  Do bouncer_text
  do ShowCompletedText
  !Check Compulsory Fields
  Set(defaults)
  access:defaults.next()
  Do CheckRequiredFields
  
  glo:select3=''
  If Records(TranType) = 1
      Set(TranType,0)
      If Access:TranType.Next() = Level:Benign
          job:Transit_Type = trt:Transit_Type
          Update()
          Display()
          Post(Event:Accepted,?job:Transit_Type)
          ?job:Transit_Type{prop:Skip} = True
      End ! If Access:TranType.Next() = Level:Benign
  End ! If Records(TranType) = 1
  tmp:Print_JobCard = False
  
  ! View Only
  If ThisWindow.Request = ViewRecord
      Do ShowTitle
      SELECT(?Cancel)
      glo:Preview = 'V'
  ELSE
      !TB12676 � Reset to not "ReadOnly" when a new job is being booked � JC 09/07/2012
      Glo:Preview = ''
  End ! ThisWindow.Request = ViewRecord
  
  saveengineer = job:engineer
  
  If thiswindow.request = Insertrecord
      job:pop = ''
  End ! If
  
  If job:Loan_Unit_Number <> 0 THEN
      UNHIDE(?StrLoanWarning)
  End ! IF
  
  ! Can't see any reason to show this field
  ! at booking, for an ARC job -  (DBH: 28-11-2003)
  If glo:WebJob <> 1
      ?tmp:HubRepair{prop:Disable} = 1
  End ! If glo:WebJob
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?job:Transit_Type{Prop:Tip} AND ~?Lookup_Transit_Type{Prop:Tip}
     ?Lookup_Transit_Type{Prop:Tip} = 'Select ' & ?job:Transit_Type{Prop:Tip}
  END
  IF ?job:Transit_Type{Prop:Msg} AND ~?Lookup_Transit_Type{Prop:Msg}
     ?Lookup_Transit_Type{Prop:Msg} = 'Select ' & ?job:Transit_Type{Prop:Msg}
  END
  IF ?job:Model_Number{Prop:Tip} AND ~?Lookup_Model_Number{Prop:Tip}
     ?Lookup_Model_Number{Prop:Tip} = 'Select ' & ?job:Model_Number{Prop:Tip}
  END
  IF ?job:Model_Number{Prop:Msg} AND ~?Lookup_Model_Number{Prop:Msg}
     ?Lookup_Model_Number{Prop:Msg} = 'Select ' & ?job:Model_Number{Prop:Msg}
  END
  IF ?job:Unit_Type{Prop:Tip} AND ~?Lookup_Unit_Type{Prop:Tip}
     ?Lookup_Unit_Type{Prop:Tip} = 'Select ' & ?job:Unit_Type{Prop:Tip}
  END
  IF ?job:Unit_Type{Prop:Msg} AND ~?Lookup_Unit_Type{Prop:Msg}
     ?Lookup_Unit_Type{Prop:Msg} = 'Select ' & ?job:Unit_Type{Prop:Msg}
  END
  IF ?job:Colour{Prop:Tip} AND ~?Lookup_Colour{Prop:Tip}
     ?Lookup_Colour{Prop:Tip} = 'Select ' & ?job:Colour{Prop:Tip}
  END
  IF ?job:Colour{Prop:Msg} AND ~?Lookup_Colour{Prop:Msg}
     ?Lookup_Colour{Prop:Msg} = 'Select ' & ?job:Colour{Prop:Msg}
  END
  IF ?job:Account_Number{Prop:Tip} AND ~?Lookup_Account_Number{Prop:Tip}
     ?Lookup_Account_Number{Prop:Tip} = 'Select ' & ?job:Account_Number{Prop:Tip}
  END
  IF ?job:Account_Number{Prop:Msg} AND ~?Lookup_Account_Number{Prop:Msg}
     ?Lookup_Account_Number{Prop:Msg} = 'Select ' & ?job:Account_Number{Prop:Msg}
  END
  IF ?tmp:Network{Prop:Tip} AND ~?LookupNetwork{Prop:Tip}
     ?LookupNetwork{Prop:Tip} = 'Select ' & ?tmp:Network{Prop:Tip}
  END
  IF ?tmp:Network{Prop:Msg} AND ~?LookupNetwork{Prop:Msg}
     ?LookupNetwork{Prop:Msg} = 'Select ' & ?tmp:Network{Prop:Msg}
  END
  IF ?job:ProductCode{Prop:Tip} AND ~?LookupProductCode{Prop:Tip}
     ?LookupProductCode{Prop:Tip} = 'Select ' & ?job:ProductCode{Prop:Tip}
  END
  IF ?job:ProductCode{Prop:Msg} AND ~?LookupProductCode{Prop:Msg}
     ?LookupProductCode{Prop:Msg} = 'Select ' & ?job:ProductCode{Prop:Msg}
  END
  IF SELF.Request = ViewRecord
    DISABLE(?ButtonHelp)
    ?job:Transit_Type{PROP:ReadOnly} = True
    DISABLE(?Lookup_Transit_Type)
    ?job:ESN{PROP:ReadOnly} = True
    ?job:MSN{PROP:ReadOnly} = True
    DISABLE(?job:ProductCode)
    DISABLE(?LookupProductCode)
    ?job:Model_Number{PROP:ReadOnly} = True
    DISABLE(?Lookup_Model_Number)
    ?job:Unit_Type{PROP:ReadOnly} = True
    DISABLE(?Lookup_Unit_Type)
    ?job:Colour{PROP:ReadOnly} = True
    DISABLE(?Lookup_Colour)
    ?tmp:Network{PROP:ReadOnly} = True
    DISABLE(?LookupNetwork)
    ?job:Mobile_Number{PROP:ReadOnly} = True
    DISABLE(?job:Chargeable_Job)
    DISABLE(?lookup_Charge_Type)
    ?job:Charge_Type{PROP:ReadOnly} = True
    DISABLE(?job:Warranty_Job)
    DISABLE(?Lookup_Warranty_Charge_Type)
    ?job:Warranty_Charge_Type{PROP:ReadOnly} = True
    DISABLE(?job:Intermittent_Fault)
    ?job:DOP{PROP:ReadOnly} = True
    HIDE(?tmp:SkillLevel:Prompt)
    HIDE(?tmp:SkillLevel)
    ?job:Account_Number{PROP:ReadOnly} = True
    DISABLE(?Lookup_Account_Number)
    ?job:Order_Number{PROP:ReadOnly} = True
    ?job:Title{PROP:ReadOnly} = True
    ?job:Initial{PROP:ReadOnly} = True
    ?job:Surname{PROP:ReadOnly} = True
    DISABLE(?Button:ExternalDamageChecklist)
    ?tmp:EndUserTelNo{PROP:ReadOnly} = True
    ?job:Location{PROP:ReadOnly} = True
    ?job:Authority_Number{PROP:ReadOnly} = True
    DISABLE(?tmp:HUBRepair)
    DISABLE(?Amend)
    DISABLE(?Amend:2)
    DISABLE(?Amend:3)
    DISABLE(?Repair_History)
    HIDE(?OK)
  END
  IF ?job:Chargeable_Job{Prop:Checked} = True
    ENABLE(?lookup_Charge_Type)
    ENABLE(?JOB:Charge_Type)
  END
  IF ?job:Chargeable_Job{Prop:Checked} = False
    job:Charge_Type = ''
    DISABLE(?lookup_Charge_Type)
    DISABLE(?JOB:Charge_Type)
  END
  IF ?job:Warranty_Job{Prop:Checked} = True
    ENABLE(?Lookup_Warranty_Charge_Type)
    ENABLE(?JOB:Warranty_Charge_Type)
  END
  IF ?job:Warranty_Job{Prop:Checked} = False
    job:Warranty_Charge_Type = ''
    DISABLE(?Lookup_Warranty_Charge_Type)
    DISABLE(?JOB:Warranty_Charge_Type)
  END
  IF ?tmp:Contract{Prop:Checked} = True
    tmp:Prepaid = 0
  END
  IF ?tmp:Prepaid{Prop:Checked} = True
    tmp:Contract = 0
  END
  IF ?job:Chargeable_Job{Prop:Disable} = True
      ?Lookup_Charge_Type{prop:Disable} = True
      ?job:Charge_Type{Prop:Disable} =  True
  END
  IF ?job:Warranty_Job{Prop:Disable} = True
      ?Lookup_Warranty_Charge_Type{Prop:Disable} = True
      ?job:Warranty_Charge_Type{prop:Disable} = True
  END
  
  do Display_Menu_Option
  SELF.SetAlerts()
  do Replicate_Job:Process_Fields
  !added by Paul 03/09/2009 Log No 10785
  if clip(GLO:Select30) = 'REPLICATE'
      !message('changing some fields')
      !check the final changes that need to be made to the record
      !set the status on the job to '450 SEND TO ARC
      GetStatus(450,0,'JOB')
      !set the new job as a chargeable job
      job:Warranty_Job         = 'NO'
      job:Chargeable_Job       = 'YES'
      job:Charge_Type          = 'NON-WARRANTY (RE-REPAIR)'
      job:Warranty_Charge_Type = ''
  
      !set the job location to 'AT FRANCISE'
      job:Location = 'AT FRANCHISE'
  
      tmp:Booking48HourOption = jobe:Booking48HourOption
  
      !enter this job in the waybil generation table
      jobe:HubRepair          = 1
      jobe:HubRepairDate      = today()
      jobe:HubRepairTime      = clock()
  
      access:Jobse.update()
  
      !make sure the exchange pone details are the current job details if its an exchange job
      If GLO:Select33 = 'EXCH' then
          !copy from exchange phone details
          Access:Exchange.clearkey(xch:Ref_Number_Key)
          xch:Ref_Number = job:Exchange_Unit_Number
          If access:Exchange.Fetch(xch:Ref_Number_Key) = level:benign then
              !found the exchange record
              job:Model_Number    = xch:Model_Number
              job:Manufacturer    = xch:Manufacturer
              job:ESN             = xch:ESN
              job:MSN             = xch:MSN
              job:Colour          = xch:Colour
              !as this is an exchange we need to blank the product code
              clear(job:ProductCode)
          End
      End
  
      Access:WEBJOB.ClearKey(wob:RefNumberKey)                    ! Fetch web job
      wob:RefNumber = job:Ref_Number
      if Access:WEBJOB.TryFetch(wob:RefNumberKey) = level:benign then
          Access:WAYBAWT.ClearKey(wya:AccountJobNumberKey)            ! Does this record already exist ?
          wya:AccountNumber = wob:HeadAccountNumber
          wya:JobNumber   = job:Ref_Number
          if Access:WAYBAWT.Fetch(wya:AccountJobNumberKey)
              wya:JobNumber     = job:Ref_Number
              wya:AccountNumber = wob:HeadAccountNumber
              wya:Manufacturer  = job:Manufacturer
              wya:ModelNumber   = job:Model_Number
              wya:IMEINumber    = job:ESN
  
              access:WAYBAWT.insert()
  !            if Access:WAYBAWT.Update()
  !                !message('oh no it didnt')
  !                Access:WAYBAWT.CancelAutoInc()
  !            end
          else
  
          end
      End
  
      !now we need to blank some fields so the job can be processed like a new job
      clear(job:In_Repair)
      clear(job:Date_In_Repair)
      clear(job:Time_In_Repair)
      clear(job:On_Test)
      clear(job:Date_On_Test)
      clear(job:Time_On_Test)
      clear(job:Estimate_Ready)
      clear(job:QA_Passed)
      clear(job:Date_QA_Passed)
      clear(job:Time_QA_Passed)
      clear(job:QA_Rejected)
      clear(job:Date_QA_Rejected)
      clear(job:Time_QA_Rejected)
      clear(job:QA_Second_Passed)
      clear(job:Date_QA_Second_Passed)
      clear(job:Time_QA_Second_Passed)
      clear(job:Completed)
      clear(job:Date_Completed)
      clear(job:Time_Completed)
      clear(job:Engineer)
      clear(SaveEngineer)
      clear(job:Paid)
      clear(job:Paid_Warranty)
      clear(job:Date_Paid)
      clear(job:Repair_Type)
      clear(job:Repair_Type_Warranty)
      !clear(job:Engineer)
      clear(job:Ignore_Chargeable_Charges)
      clear(job:Ignore_Warranty_Charges)
      clear(job:Ignore_Estimate_Charges)
      clear(job:Courier_Cost)
      clear(job:Advance_Payment)
      clear(job:Labour_Cost)
      clear(job:Parts_Cost)
      clear(job:Sub_Total)
      clear(job:Courier_Cost_Estimate)
      clear(job:Labour_Cost_Estimate)
      clear(job:Parts_Cost_Estimate)
      clear(job:Sub_Total_Estimate)
      clear(job:Courier_Cost_Warranty)
      clear(job:Labour_Cost_Warranty)
      clear(job:Parts_Cost_Warranty)
      clear(job:Sub_Total_Warranty)
      clear(job:Loan_Issued_Date)
      clear(job:Loan_Unit_Number)
      clear(job:Loan_User)
      clear(job:Loan_Courier)
      clear(job:Loan_Consignment_Number)
      clear(job:Loan_Despatched)
      clear(job:Loan_Despatched_User)
      clear(job:Loan_Despatch_Number)
      clear(job:LoaService)
      clear(job:Exchange_Unit_Number)
      clear(job:Exchange_Authorised)
      clear(job:Loan_Authorised)
      clear(job:Exchange_Accessory)
      clear(job:Exchange_Issued_Date)
      clear(job:Exchange_Courier)
      clear(job:Exchange_User)
      clear(job:Exchange_Consignment_Number)
      clear(job:Exchange_Despatched)
      clear(job:Exchange_Despatched_User)
      clear(job:Exchange_Despatch_Number)
      clear(job:ExcService)
      clear(job:Date_Despatched)
      clear(job:Despatch_Number)
      clear(job:Despatch_User)
      !clear(job:Courier)
      clear(job:Consignment_Number)
      !clear(job:Incoming_Courier)
      !clear(job:Incoming_Consignment_Number)
      !clear(job:Incoming_Date)
      clear(job:Despatched)
      !clear(job:Current_Courier)
      clear(job:Estimate)
      clear(job:Estimate_If_Over)
      clear(job:Estimate_Accepted)
      clear(job:Third_Party_Printed)
      clear(job:ThirdPartyDateDesp)
      clear(job:InvoiceAccount)
      clear(job:InvoiceStatus)
      clear(job:InvoiceBatch)
      clear(job:InvoiceQuery)
      clear(job:Invoice_Exception)
      clear(job:Invoice_Failure_Reason)
      clear(job:Invoice_Number)
      clear(job:Invoice_Date)
      clear(job:Invoice_Date_Warranty)
      clear(job:Invoice_Courier_Cost)
      clear(job:Invoice_Labour_Cost)
      clear(job:Invoice_Parts_Cost)
      clear(job:Invoice_Sub_Total)
      clear(job:Invoice_Number_Warranty)
      clear(job:WInvoice_Courier_Cost)
      clear(job:WInvoice_Labour_Cost)
      clear(job:WInvoice_Parts_Cost)
      clear(job:WInvoice_Sub_Total)
  
      !set the loan and exchange status depending on the transit type
      Access:TranType.clearkey(trt:Transit_Type_Key)
      trt:Transit_Type = clip(job:Transit_Type)
      If Access:TranType.fetch(trt:Transit_Type_Key) = level:benign then
          !set the load and exchange status from this record
          job:Loan_Status     = trt:LoanStatus
          job:Exchange_Status = trt:ExchangeStatus
      Else
          !leave tham as was
      End
  
      !added by Paul 05/10/2009 - log no 10785
      !if the booking option is blank - then default it to 7 day TAT - mainly for VCP jobs
      If jobe:Booking48HourOption = '' then
          !empty
          jobe:Booking48HourOption = 3
          !also set the tmp field for display on screen
          tmp:Booking48HourOption = 3
          access:jobse.update()
      End
  
      do ShowCompletedText
  
      DO GETJOBSE
  
      access:jobs.update()
      !disbale the page so no changes can be made - and only allow the OK button to be pressed
      Disable(?Sheet1)
      Disable(?Sheet2)
      Disable(?Sheet3)
      Disable(?Cancel)
  
      display()
      thiswindow.Update()
  end
  if glo:insert_global = 'PRE'
      !details are held in the open prejob record
      job:Title = PRE:Title
      job:Initial = PRE:Initial
      job:Surname = PRE:Surname
      job:Company_Name = clip(PRE:Title)&' '&PRE:Initial&' '&clip(PRE:Surname)
      job:Address_Line1 = PRE:Address_Line1
      job:Address_Line2 = PRE:Address_Line2
      job:Address_Line3 = PRE:Suburb
      job:Telephone_Number = PRE:Telephone_number
      job:Mobile_Number = PRE:Mobile_number
      ! = PRE:ID_number
      job:ESN = PRE:ESN
      job:Manufacturer = PRE:Manufacturer
      job:Model_Number = PRE:Model_number
      job:DOP = PRE:DOP
      Access:modelnum.clearkey(mod:Model_Number_Key)
      mod:Model_Number = PRE:Model_number
      if access:Modelnum.fetch(mod:Model_Number_Key) = level:benign
        job:Unit_Type = mod:Unit_Type
      END
  
      !What sort of charge type is called for?
      Access:Chartype.clearkey(cha:Charge_Type_Key)
      cha:Charge_Type = PRE:ChargeType
      if access:Chartype.fetch(cha:Charge_Type_Key) = level:benign
        if cha:Warranty = 'YES' then
            job:Chargeable_Job = 'NO'
            Job:Warranty_job = 'YES'
            job:Warranty_Charge_Type  = PRE:ChargeType
        ELSE
            job:Chargeable_Job = 'YES'
            Job:Warranty_job = 'NO'
            job:Charge_Type  = Pre:ChargeType
        END
  
      END !if charge type found
  
      Access:jobs.update()
  
      !TB13014 - check if we need to show the MSN etc
      do Check_MSN
      do CheckRequiredFields
  END
  
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  If tmp:OKPressed = 0 And ThisWindow.Request = InsertRecord
      !Job has been cancelled. Delete the webjob entry
      Do RemoveWebJob
  End ! If GlobalReponse = 2
  glo:insert_Global   = ''
  Release(JOBS)
  
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:ACCESSOR.Close
    Relate:COLOUR.Close
    Relate:COMMONCP.Close
    Relate:DEFAULTS.Close
    Relate:DEFRAPID.Close
    Relate:ESNMODEL.Close
    Relate:EXCHANGE.Close
    Relate:JOBS2_ALIAS.Close
    Relate:JOBSE2_ALIAS.Close
    Relate:JOBSE_ALIAS.Close
    Relate:MANUDATE.Close
    Relate:NETWORKS.Close
    Relate:PREJOB.Close
    Relate:STAHEAD.Close
    Relate:SUBURB.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:TRANTYPE.Close
    Relate:TURNARND.Close
    Relate:USELEVEL.Close
    Relate:USERS_ALIAS.Close
    Relate:WEBJOB.Close
    Relate:WEBJOBNO.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    job:EDI = 'XXX'
    job:Loan_Status = '101 NOT ISSUED'
    job:Exchange_Status = '101 NOT ISSUED'
    job:Third_Party_Printed = 'NO'
    job:Completed = 'NO'
    job:Despatched = 'NO'
    job:Incoming_Date = Today()
  PARENT.PrimeFields
  If glo:insert_global    = 'YES'  or glo:insert_global = 'PRE'
      access:jobs.primerecord()
  End!If insert_global    = 'YES'
  access:users.clearkey(use:password_key)
  use:password    = glo:password
  access:users.fetch(use:password_key)
  job:who_booked  = use:user_code
  get(jobstage,0)
  if access:jobstage.primerecord() = level:benign
  
      jst:ref_number = job:ref_number
      jst:job_stage  = '000 NEW JOB'
      jst:date       = Today()
      jst:time       = Clock()
      access:users.clearkey(use:password_key)
      use:password =glo:password
      access:users.fetch(use:password_key)
      jst:user = use:user_code
      if access:jobstage.tryinsert()
          access:jobstage.cancelautoinc()
      end
  
  end!if access:jobstage.primerecord() = level:benign
  
  GetStatus(0,ThisWindow.Request,'JOB')
  
  job:incoming_date   = Today()
  tmp:SkillLevel = 0
  !Save Fields
  Do save_Fields
  Do ShowTitle
  ?job:dop{prop:req} = 1
  ?job:dop{prop:Background} = 080FFFFH
  !Bryan.CompFieldColour()
  


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  GlobalRequest = ViewRecord
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      PickTransitTypes
      PickNewModelProductCodes
      Browse_Model_Numbers
      Browse_Unit_Types
      Pick_Model_Colour
      PickNetworks
      PickWebAccounts
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OptionsAllocateJob
      !Save the job, before changing/adding an engineer - 4485 (DBH: 19-07-2004)
      Access:JOBS.TryUpdate()
    OF ?BrowseAuditTrail
      glo:select12 = job:ref_number
    OF ?BrowseContactHistory
      glo:select12 = job:ref_number
    OF ?job:ProductCode
      GLO:Select50 = job:Model_Number
    OF ?job:Mobile_Number
      
          temp_string = Clip(left(job:Mobile_Number))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          job:Mobile_Number    = temp_string
          Display(?job:Mobile_Number)
    OF ?tmp:HUBRepair
      If ~0{prop:acceptall}
          case tmp:HUBRepair
              of 0
                  Case Missive('De-selecting hub repair will credit this job to a RRC.'&|
                    '<13,10>'&|
                    '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                                 'mquest.jpg','\No|/Yes')
                      Of 2 ! Yes Button
                      Of 1 ! No Button
                          tmp:HUBRepair = 1
      
                          display()
                          cycle
                  End ! Case Missive
              of 1
                  Case Missive('Selecting hub repair will credit this job to the ARC.'&|
                    '<13,10>'&|
                    '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                                 'mquest.jpg','\No|/Yes')
                      Of 2 ! Yes Button
                      Of 1 ! No Button
                          tmp:HUBRepair = 0
                          display()
                          cycle
                  End ! Case Missive
          end
      end
    OF ?Amend
      Do SetJOBSE
    OF ?Amend:2
      Do SetJOBSE
    OF ?Amend:3
      Do SetJOBSE
    OF ?Fault_Description
      access:jobnotes.clearkey(jbn:RefNumberKey)
      jbn:RefNumber   = job:Ref_Number
      If access:jobnotes.tryfetch(jbn:RefNumberKey)
          If access:jobnotes.primerecord() = Level:Benign
              jbn:RefNumber   = job:ref_number
                if PRE:FaultText <> ''
                    jbn:Fault_Description = PRE:FaultText
                END
      
              access:jobnotes.tryinsert()
          End!If access:jobnotes.primerecord() = Level:Benign
      End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
    OF ?engineers_notes
      access:jobnotes.clearkey(jbn:RefNumberKey)
      jbn:RefNumber   = job:Ref_Number
      If access:jobnotes.tryfetch(jbn:RefNumberKey)
          If access:jobnotes.primerecord() = Level:Benign
              jbn:RefNumber   = job:ref_number
              if PRE:FaultText <> ''
                  jbn:Fault_Description = PRE:FaultText
              END
      
              access:jobnotes.tryinsert()
          End!If access:jobnotes.primerecord() = Level:Benign
      End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
    OF ?Validate_POP
      If SecurityCheck('JOBS - AMEND POP DETAILS')
          Case Missive('You do not have access to amend the POP Details.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else!If SecurityCheck('COMMON FAULTS - APPLY TO JOBS')
          !Validate Proof OF Purchase
          Glo:Select1 = job:pop
          Glo:Select2 = job:dop
      
          Error# = 0
          ! Check access level to determine if password screen should appear - TrkBs: 6685 (DBH: 10-11-2005)
          If SecurityCheck('VALIDATE POP WITHOUT PASSWORD')
              Access:USERS.Clearkey(use:Password_Key)
              use:Password    = glo:Password
              If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                  ! Found
                  UserCode" = use:User_Code
                  If InsertEngineerPassword() <> UserCode"
                      Case Missive('Incorrect password.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      Error# = 1
                  End ! If InsertEngineerPassword() <> UserCode"
              Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                  ! Error
              End ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          End ! If SecurityCheck('VALIDATE POP WITHOUT PASSWORD')
      
          If Error# = 0
              !Incase the ****wits put the wrong date in and try and validate it again
              !change the status to the initial transit type default
              If Sub(job:Current_Status,1,3) = '130'
                  Access:TRANTYPE.ClearKey(trt:Transit_Type_Key)
                  trt:Transit_Type = job:Transit_Type
                  If Access:TRANTYPE.TryFetch(trt:Transit_Type_Key) = Level:Benign
                      !Found
                      GetStatus(Sub(trt:Initial_Status,1,3),0,'JOB')
                  Else!If Access:TRANTYPE.TryFetch(trt:Transit_Type_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End!If Access:TRANTYPE.TryFetch(trt:Transit_Type_Key) = Level:Benign
              End !If Sub(job:Current_Status,1,3) = '130'
              DO Validate_Motorola
          End ! If Error# = 0
      
          Glo:Select1 = ''
          Glo:Select2 = ''
          glo:Select3 = ''
      END
    OF ?Cancel
      Case Missive('Are you sure you want to cancel?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
          Of 1 ! No Button
              Cycle
      End ! Case Missive
      if thiswindow.request <> Insertrecord then do allocationsReport.
      
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OptionsAllocateJob
      ThisWindow.Update
      AllocateEngineerToJob
      ThisWindow.Reset
    OF ?OptionsEstimate
      ThisWindow.Update
      EstimateDetails
      ThisWindow.Reset
    OF ?OptionsAllocateLoan
      ThisWindow.Update
      Do AddLoanUnit
    OF ?OptionsAllocateExchange
      ThisWindow.Update
      ViewExchangeUnit
      ThisWindow.Reset
      !update the jobs file in case any change made
      if ?OK{prop:hide}=true then
          access:jobs.update()
      END
    OF ?BrowseAuditTrail
      ThisWindow.Update
      Browse_Audit
      ThisWindow.Reset
      glo:select12 = ''
    OF ?BrowseStatusChange
      ThisWindow.Update
      BrowseStatusChanges(job:Ref_Number)
      ThisWindow.Reset
    OF ?BrowseContactHistory
      ThisWindow.Update
      Browse_Contact_History
      ThisWindow.Reset
      glo:select12 = ''
    OF ?BrowseEngineerHistory
      ThisWindow.Update
      BrowseEngineersOnJob(job:Ref_Number)
      ThisWindow.Reset
    OF ?BrowseLocationHistory
      ThisWindow.Update
      BrowseLocationChanges(job:ref_number)
      ThisWindow.Reset
    OF ?DefaultsGeneralDefaults
      ThisWindow.Update
      Job_Booking_Defaults_Window
      ThisWindow.Reset
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020500'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020500'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020500'&'0')
      ***
    OF ?job:Transit_Type
      If ~0{prop:acceptall}
      IF job:Transit_Type OR ?job:Transit_Type{Prop:Req}
        trt:Transit_Type = job:Transit_Type
        !Save Lookup Field Incase Of error
        look:job:Transit_Type        = job:Transit_Type
        IF Access:TRANTYPE.TryFetch(trt:Transit_Type_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            job:Transit_Type = trt:Transit_Type
          ELSE
            !Restore Lookup On Error
            job:Transit_Type = look:job:Transit_Type
            SELECT(?job:Transit_Type)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! Changing (DBH 09/03/2007) # 8718 - Call the "OBF" check AFTER the IMEI Number has been entered.
      !If tmp:OBF_flag = 1 then
      !    Case Missive('You cannot change the transit type once the OBF check has taken place.','ServiceBase 3g',|
      !               'mstop.jpg','/OK')
      !        Of 1 ! OK Button
      !    End ! Case Missive
      !    job:transit_type = savetransittype
      !    Cycle
      !End
      !
      !SaveTransitType = job:transit_type
      !
      !Set(defaults)
      !access:defaults.next()
      !
      !Access:trantype.clearkey(trt:transit_type_key)
      !trt:transit_type = job:transit_type
      !if access:trantype.fetch(trt:transit_type_key) = Level:Benign
      !    hide# = 0
      !    If trt:collection_address <> 'YES'
      !        JOB:Postcode_Collection=''
      !        JOB:Company_Name_Collection=''
      !        JOB:Address_Line1_Collection=''
      !        JOB:Address_Line2_Collection=''
      !        JOB:Address_Line3_Collection=''
      !        JOB:Telephone_Collection=''
      !    End
      !    hide# = 0
      !    If trt:delivery_address <> 'YES'
      !        JOB:Postcode_Delivery=''
      !        JOB:Address_Line1_Delivery=''
      !        JOB:Company_Name_Delivery=''
      !        JOB:Address_Line2_Delivery=''
      !        JOB:Address_Line3_Delivery=''
      !        JOB:Telephone_Delivery=''
      !    End!If trt:delivery_address <> 'YES'
      !    If trt:force_dop = 'YES' and job:warranty_job = 'YES'
      !        ?job:dop{prop:req} = 1
      !        Bryan.CompFieldColour()
      !    Else!If trt:force_dop = 'YES'
      !        ?job:dop{prop:req} = 0
      !        Bryan.CompFieldColour()
      !    End!If trt:force_dop = 'YES'
      !
      !    If trt:exchange_unit = 'YES'
      !        tmp:retain = 1
      !    Else
      !        tmp:retain = 0
      !    End!If trt:exchange_unit = 'YES'
      !    If trt:InternalLocation <> ''
      !        job:Location    = trt:InternalLocation
      !    End !If trt:InternalLocation <> ''
      !    If trt:InWorkshop = 'YES'
      !        job:workshop = 'YES'
      !    Else!If trt:InWorkshop = 'YES'
      !        job:workshop = 'NO'
      !    End!If trt:InWorkshop = 'YES'
      !    If trt:location <> 'YES'
      !        Hide(?job:location)
      !        Hide(?job:location:prompt)
      !        ?job:location{prop:req} = 0
      !        Bryan.CompFieldColour()
      !    Else
      !        If glo:WebJob then
      !            !Location will be filled in automatically
      !            !But must not be seen or altered
      !            Hide(?job:location)
      !            Hide(?job:location:prompt)
      !        ELSE
      !            Unhide(?job:location)
      !            unhide(?job:location:prompt)
      !            If trt:force_location = 'YES' and job:workshop = 'YES'
      !                ?job:location{prop:req} = 1
      !                Bryan.CompFieldColour()
      !            Else
      !                ?job:location{prop:req} = 0
      !                Bryan.CompFieldColour()
      !            End!If trt:forcelocation = 'YES' and job:workshop = 'YES'
      !        END !IF glo:webjob
      !    End
      !
      !    If ThisWindow.Request = Insertrecord
      !        GetStatus(Sub(trt:initial_status,1,3),1,'JOB')
      !
      !        GetStatus(Sub(TRT:ExchangeStatus,1,3),1,'EXC')
      !
      !        GetStatus(Sub(trt:loanStatus,1,3),1,'LOA')
      !
      !        Do time_remaining
      !
      !        If trt:workshop_label = 'YES'
      !            print_label_temp = 'YES'
      !        Else
      !            print_label_temp = 'NO'
      !        End
      !        If trt:job_card = 'YES'
      !            print_job_card_temp = 'YES'
      !            ?tmp:Print_JobCard{prop:Hide} = 0
      !            tmp:Print_JobCard = 0
      !        Else!If trt:job_card = 'YES'
      !            print_job_card_temp = 'NO'
      !            ?tmp:Print_JobCard{prop:Hide} = 1
      !        End!If trt:job_card = 'YES'
      !        If trt:jobreceipt = 'YES'
      !            tmp:printreceipt = 1
      !        Else!If trt:jobreceipt = 'YES'
      !            tmp:printreceipt = 0
      !        End!If trt:jobreceipt = 'YES'
      !        job:turnaround_time = trt:initial_priority
      !        access:turnarnd.clearkey(tur:turnaround_time_key)
      !        tur:turnaround_time = job:turnaround_time
      !        if access:turnarnd.fetch(tur:turnaround_time_key) = Level:Benign
      !            Turnaround_Routine(tur:days,tur:hours,end_date",end_time")
      !            job:turnaround_end_date = Clip(end_date")
      !            job:turnaround_end_time = Clip(end_time")
      !            Do Time_Remaining
      !        end!if access:turnarnd.fetch(tur:turnaround_time_key) = Level:Benign
      !
      !        If ?tmp:HubRepair{prop:Hide} = 0
      !            tmp:HubRepair   = trt:HubRepair
      !        Else
      !            tmp:HubRepair   = 0
      !        End !If ?tmp:HubRepair{prop:Hide} = 0
      !    End !If thiswindow is insert record
      !
      !End!if access:trantype.fetch(trt:transit_type_key) = Level:Benign
      !
      !Thiswindow.update()
      !display()
      ! to (DBH 09/03/2007) # 8718
      If tmp:OBF_Flag = 1
          Case Missive('You cannot change the Transit Type once you have selected an "OBF" Transit Type.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          job:Transit_Type = SaveTransitType
          display(?job:Transit_type)
          Cycle
      End ! If tmp:OBF_Flag = 1
      
      SaveTransitType = job:Transit_Type
      
      Set(DEFAULTS)
      Access:DEFAULTS.Next()
      
      Access:TRANTYPE.ClearKey(trt:Transit_Type_Key)
      trt:Transit_Type = job:Transit_Type
      If Access:TRANTYPE.TryFetch(trt:Transit_Type_Key) = Level:Benign
          !Found
          If trt:Collection_Address <> 'YES'
              ! Collection address not needed (DBH: 09/03/2007)
              job:Postcode_Collection = ''
              job:Company_Name_Collection = ''
              job:Address_Line1_collection = ''
              job:Address_Line2_Collection = ''
              job:Address_Line3_Collection = ''
              job:Telephone_Collection = ''
          End ! If trt:Collection_Address <> 'YES'
          If trt:Delivery_Address <> 'YES'
              ! Delivery Address not needed (DBH: 09/03/2007)
              job:Postcode_Delivery = ''
              job:Company_Name_Delivery = ''
              job:Address_Line1_Delivery = ''
              job:Address_Line2_Delivery = ''
              job:Address_Line3_Delivery = ''
              job:Telephone_Delivery = ''
          End ! If trt:Delivery_Address <> 'YES'
      
          If trt:Force_DOP = 'YES' And job:Warranty_Job = 'YES'
              ?job:DOP{prop:Req} = 1
          Else ! If trt:Force_DOP = 'YES' And job:Warranty_Job = 'YES'
              ?job:DOP{prop:Req} = 0
          End ! If trt:Force_DOP = 'YES' And job:Warranty_Job = 'YES'
      
          If trt:Exchange_Unit = 'YES'
              tmp:Retain = 1
          Else ! If trt:Exchage_Unit = 'YES'
              tmp:Retain = 0
          End ! If trt:Exchage_Unit = 'YES'
      
          If trt:InternalLocation <> ''
              job:Location = trt:InternalLocation
          End ! If trt:Internal_Location <> ''
      
          If trt:InWorkshop = 'YES'
              job:Workshop = 'YES'
          Else ! If trt:InWorkshop = 'YES'
              job:Workshop = 'NO'
          End ! If trt:InWorkshop = 'YES'
      
          If trt:Location <> 'YES'
              ?job:Location{prop:Hide} = 1
              ?job:Location:Prompt{prop:Hide} = 1
              ?job:Location{prop:Req} = 0
              ?job:Location{prop:Background} = color:white
      
          Else ! If trt:Location <> 'YES'
              If ~glo:WebJob
                  ?job:Location{prop:Hide} = 0
                  ?job:Location:Prompt{prop:Hide} = 0
                  If trt:Force_Location = 'YES' And job:Workshop = 'YES'
                      ?job:Location{prop:Req} = 1
                      ?job:Location{prop:Background} = 080FFFFH
                  Else ! If trt:Force_Location = 'YES' And job:Workshop = 'YES'
                      ?job:Location{prop:Req} = 0
                      ?job:Location{prop:Background}= color:white
                  End ! If trt:Force_Location = 'YES' And job:Workshop = 'YES'
              End ! If ~glo:WebJob
          End ! If trt:Location <> 'YES'
      
          If ThisWindow.Request = InsertRecord
              !changed by Paul 29/09/2009 - log no 10785
              !do not auto change the status for qa failure copy jobs
              If Glo:Select32 <> 'COPY' then
                GetStatus(Sub(trt:Initial_Status,1,3),1,'JOB')
              End
              !End Change
              GetStatus(Sub(trt:ExchangeStatus,1,3),1,'EXC')
              GetStatus(Sub(trt:LoanStatus,1,3),1,'LOA')
      
              Do Time_Remaining
      
              If trt:Workshop_Label = 'YES'
                  Print_Label_Temp = 'YES'
              Else ! If trt:Workshop_Label = 'YES'
                  Print_Label_Temp = 'NO'
              End ! If trt:Workshop_Label = 'YES'
      
              If trt:Job_Card = 'YES'
                  Print_Job_Card_Temp = 'YES'
                  ?tmp:PRint_JobCard{prop:Hide} = 0
                  tmp:Print_JobCard = 0
              Else ! If trt:Job_Card = 'YES'
                  Print_Job_Card_Temp = 'NO'
                  ?tmp:Print_JobCard{Prop:Hide} = 1
              End ! If trt:Job_Card = 'YES'
      
              If trt:JobReceipt = 'YES'
                  tmp:PrintReceipt = 1
              Else ! If trt:JobReceipt = 'YES'
                  tmp:PrintReceipt = 0
              End ! If trt:JobReceipt = 'YES'
      
              job:Turnaround_Time = trt:Initial_Priority
      
              Access:TURNARND.ClearKey(tur:Turnaround_Time_Key)
              tur:Turnaround_Time = job:Turnaround_Time
              If Access:TURNARND.TryFetch(tur:Turnaround_Time_Key) = Level:Benign
                  !Found
                  Turnaround_Routine(tur:Days,tur:Hours,End_Date",End_Time")
                  job:Turnaround_End_Date = Clip(End_Date")
                  job:Turnaround_End_Time = Clip(End_Time")
                  Do Time_Remaining
              Else ! If Access:TURNARND.TryFetch(tur:Turnaround_Time_Key) = Level:Benign
                  !Error
              End ! If Access:TURNARND.TryFetch(tur:Turnaround_Time_Key) = Level:Benign
      
              !Change added by Paul - 02/10/2009 - log no 10785
              !do not change this status if the record is being copied
              If Glo:Select32 <> 'COPY' then
                  If ?tmp:HubRepair{prop:Hide} = 0
                      tmp:HubRepair = trt:HubRepair
                  Else ! If ?tmp:HubRepair{prop:Hide} = 0
                      tmp:HubRepair = 0
                  End ! If ?tmp:HubRepair{prop:Hide} = 0
              End
              !End Change
      
          End ! If ThisWindow.Request = InsertRecord
      
          ! Check for OBF (DBH: 09/03/2007)
          If trt:OBF
              tmp:OBF_Flag = 1
          Else
              tmp:OBF_Flag = 0
          End ! If trt:OBF
      Else ! If Access:TRANTYPE.TryFetch(trt:Transit_Type_Key) = Level:Benign
          !Error
      End ! If Access:TRANTYPE.TryFetch(trt:Transit_Type_Key) = Level:Benign
      !Bryan.CompFieldColour()
      Display()
      ! End (DBH 09/03/2007) #8718
      End !If ~0{prop:acceptall}
      
      Do CheckRequiredFields
      do Bouncer_Text
      
    OF ?Lookup_Transit_Type
      ThisWindow.Update
      trt:Transit_Type = job:Transit_Type
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          job:Transit_Type = trt:Transit_Type
          Select(?+1)
      ELSE
          Select(?job:Transit_Type)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Transit_Type)
    OF ?job:ESN
      
       temp_string = Clip(left(job:ESN))
       string_length# = Len(temp_string)
       string_position# = 1
      
       temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
       Loop string_position# = 1 To string_length#
       If sub(temp_string,string_position#,1) = ' '
       temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
       End
       End
      
       job:ESN = temp_string
       Display(?job:ESN)
       If ~0{prop:acceptall}           
          ! Inserting (DBH 09/03/2007) # 8718 - Make sure a transit type is entered before the IMEI number.
          If Clip(job:Transit_Type) = ''
              Case Missive('Please select a Transit Type before you enter the I.M.E.I. Number.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              job:ESN = ''
              Select(?job:Transit_Type)
              Display()
              Cycle
          End ! If Clip(job:Transit_Type) = ''
          ! End (DBH 09/03/2007) #8718
      
          !Default to continue booking
          glo:Select24 = ''
          Set(DEFAULTS)
          Access:DEFAULTS.Next()
      
          !Added by Neil - Code to strip 18 char IMEI's and 15 char MSN's
          If Len(Clip(job:ESN)) = 18
              job:ESN = Sub(job:ESN,4,15)
              Display(?job:ESN)
          End !If Len(Clip(job:ESN)) = 18
      
          ! Insert --- Check the unit isn't available somewhere else (DBH: 24/02/2009) #10545
          if (IsIMEIExchangeLoan(job:ESN,1))
              job:ESN = ''
              Display()
              select(?job:ESN)
              Cycle
          end !if (IsIMEIExchangeLoan(job:ESN,1))
          ! end --- (DBH: 24/02/2009) #10545
      
          Error# = 0
          job:Model_Number = IMEIModelRoutine(job:ESN,job:Model_Number)
      
          If job:Model_Number = ''
              !No Model Number Returned
              If SecurityCheck('IMEI TO MODEL - INSERT')
                  Error# = 1
                  job:ESN = ''
              End !If SecurityCheck('IMEI TO MODEL - INSERT')
          Else !If job:Model_Number = ''
              Do Allow48Hour
          End !If job:Model_Number = ''
      
          ! Inserting (DBH 0/06/2006) #6972 - Check if the imei number is allow "characters"
          If IsIMEIValid(job:ESN,job:Model_Number,1) = False
              job:ESN = ''
              Display()
              Cycle
          End ! If IsIMEIValid(job:ESN,job:Model_Number,1) = False
          ! End (DBH 01/06/2006) #6972
      
          !Check for Bouncers
          If job:ESN <> 'N/A' And job:ESN <> ''
              job:bouncer = ''
              previousDOP# = 0
              Hide(?repair_history)
              Hide(?Bouncer_Text)
              !?BouncerPanel{prop:Hide} = 1
      
              Bouncers# = 0
              Bouncers# = CountHistory(job:ESN,job:Ref_Number,job:Date_Booked)
      
      ! Deleting (DBH 14/02/2008) # 9718 - Don't need this now
      !        ! Inserting (DBH 21/11/2006) # 8405 - Display a message if the IMEI has been used, but has not been picked up by the bouncer check
      !        If Bouncer# = 0
      !            If PreviousIMEI(job:Ref_Number,job:ESN)
      !                Case Missive('This I.M.E.I. Number has been entered onto the system before.'&|
      !                  '|The previous history can be viewed using the "By IMEI No" functionality on the Job Program Screen.','ServiceBase 3g',|
      !                               'mexclam.jpg','/OK')
      !                    Of 1 ! OK Button
      !                End ! Case Missive
      !            End ! If PreviousIMEI(job:Ref_Number,job:ESN)
      !        End ! If Bouncer# = 0
      !        ! End (DBH 21/11/2006) #8405
      ! End (DBH 14/02/2008) #9718
      
              !But the bouncer error messages into a routine
              !so it can be called from more than one place - 3266 (DBH: 26-09-2003)
              If Local.ValidateIMEINumber(Bouncers#)
                  glo:Select24 = 'CONTINUE BOOKING'
                  Post(Event:CloseWindow)
                  job:esn = ''
                  job:Unit_Type = ''
                  job:Manufacturer = ''
                  job:Model_Number = ''
                  SELECT(?job:ESN)
                  error# = 1
                  hide(?bouncer_text)
              End !If Local.ValidateIMEINumber()
      
              If def:Allow_Bouncer = 'YES'
                  If Error# = 0
                      If Bouncers#
                          Case Missive('This I.M.E.I. number has been entered onto the system before.'&|
                            '<13,10>Please select "Prev Job" to view it''s history.','ServiceBase 3g',|
                                         'mexclam.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
      
                          job:bouncer = 'B'
                          Unhide(?repair_history)
                          Unhide(?bouncer_text)
                          !?BouncerPanel{prop:Hide} = 0
                          ?bouncer_text{prop:text} = Clip(Bouncers#) & ' PREV. JOB(S)'
                          !?Repair_History{prop:Text} = 'Prev. Job(s): ' & Bouncers#
      
                          !Get the a bouncer and copy the MSN
                          !Shouldn't matter which bouncer, as all the MSN's should be the same
                          tmp:LastDate = ''
                          JobRef#  = 0
      
                          Save_job_ali_ID = Access:JOBS_ALIAS.SaveFile()
                          Access:JOBS_ALIAS.ClearKey(job_ali:ESN_Key)
                          job_ali:ESN = job:ESN
                          Set(job_ali:ESN_Key,job_ali:ESN_Key)
                          Loop
                              If Access:JOBS_ALIAS.NEXT()
                                 Break
                              End !If
                              If job_ali:ESN <> job:ESN      |
                                  Then Break.  ! End If
                              If job_ali:Ref_Number <> job:Ref_Number
                                  ! Insert --- Get the DOP of the previous job (DBH: 26/02/2009) #10591
                                  previousDOP# =  job_ali:DOP
                                  ! end --- (DBH: 26/02/2009) #10591
                                  job:MSN = job_ali:MSN
      
                                  ! #11602 Simplify code to find the lastest bouncer (DBH: 28/07/2010)
                                  if (job_ali:Ref_Number > jobRef#)
                                      jobRef# = job_ali:Ref_Number
                                  end! if (job_ali:Ref_Number > jobRef#)
      
      !                            if tmp:LastDate = ''
      !                                tmp:LastDate = job_ali:date_booked
      !                                JobRef#  = job_ali:Ref_Number
      !                            else
      !                                if job_ali:date_booked > tmp:LastDate
      !                                    JobRef#  = job_ali:Ref_Number
      !                                    tmp:LastDate = job_ali:date_booked
      !                                end
      !                            end
                                  !Break
                              End !If job_ali:Ref_Number <> job:Ref_Number
                          End !Loop
      
                          !------------------------------------------------------------------------------------------
                          ! Now fetch last job
                          Do SETJOBSE
      
                          Access:JOBS_ALIAS.ClearKey(job_ali:Ref_Number_Key)
                          job_ali:Ref_Number = JobRef#
                          Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key)
      
                          Access:JobSE.TryUpdate()                                ! Save current record
      
                          Access:JobSE.ClearKey(jobe:refnumberkey)
                          jobe:refnumber = job_ali:Ref_Number
                          Access:JobSE.TryFetch(jobe:refnumberkey)               ! Fetch JobSE record for alias record (holds email)
      
                          tmp:Original_EmailAddress = ''
      
                          if PreviousAddressWindow()
      ! Changing (DBH 29/09/2008) # 10380 - Don't auto populate
      !                        job:Company_Name     = job_ali:Company_Name
      !                        job:Address_Line1    = job_ali:Address_Line1
      !                        job:Address_Line2    = job_ali:Address_Line2
      !                        job:Address_Line3    = job_ali:Address_Line3
      !                        job:Telephone_Number = job_ali:Telephone_Number
      !                        job:Fax_Number       = job_ali:Fax_Number
      !                        job:Mobile_Number    = job_ali:Mobile_Number
      !                        tmp:Original_EmailAddress = jobe:EndUserEmailAddress    ! Save fields to copy
      !                        ! Inserting (DBH 02/10/2006) # 8290 - Copy the postcode, and try and look it up from the suburb file
      !                        job:Postcode         = job_ali:Postcode
      !                        Access:SUBURB.ClearKey(sur:SuburbKey)
      !                        sur:Suburb           = job:Address_Line3
      !                        If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
      !                            !Found
      !                            job:Postcode = sur:Postcode
      !                        Else ! If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
      !                          !Error
      !                        End ! If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
      !                        ! End (DBH 02/10/2006) #8290
      !                        Access:JOBSE2_ALIAS.Clearkey(jobe2_ali:RefNumberKey)
      !                        jobe2_ali:RefNumber = job_ali:Ref_Number
      !                        If Access:JOBSE2_ALIAS.TryFetch(jobe2_ali:RefNumberKey) = Level:Benign
      !                            jobe2:HubCustomer = jobe2_Ali:HubCustomer
      !                            Access:JOBSE2.TryUpdate()
      !                        End ! If Access:JOBSE2_ALIAS.TryFetch(jobe2_ali:RefNumberKey) = Level:Benign
      ! to (DBH 29/09/2008) # 10380
                              Show_Addresses(1)
      ! End (DBH 29/09/2008) #10380
                              display()
                          end
      
                          Access:JobSE.ClearKey(jobe:refnumberkey)
                          jobe:refnumber = job:Ref_Number
                          if not Access:JobSE.Fetch(jobe:refnumberkey)                ! Fetch JobSE record for this job
                              if tmp:Original_EmailAddress <> ''
                                  jobe:EndUserEmailAddress = tmp:Original_EmailAddress
                                  Access:JobSE.TryUpdate()
                              end
                          end
                          !------------------------------------------------------------------------------------------
                          Access:JOBS_ALIAS.RestoreFile(Save_job_ali_ID)
      
                          Do GETJOBSE
      
      
                      End !If Bouncers#
                  End !If Clip(GETINI('BOUNCER','StopLive',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
      
              End !If def:Allow_Bouncer <> 'YES'
          End !If job:ESN <> 'N/A' And job:ESN <> ''
          If Error# = 0
              Access:MODELNUM.Clearkey(mod:Model_Number_Key)
              mod:Model_Number    = job:Model_Number
              If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                  !Found
                  job:Manufacturer    = mod:Manufacturer
                  If mod:Specify_Unit_Type = 'YES'
                      job:Unit_Type = mod:Unit_Type
                  End !If mod:Specify_Unit_Type = 'YES'
                  Model_Number_Temp   = job:Model_Number
                  Post(Event:Accepted,?job:Model_Number)
      
                  Do Check_MSN
      
              Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
      
          End !If Error# = 0
      
      ! Delete --- Replaced by a simple "is unit available" check (DBH: 24/02/2009) #10454
      !    !Check to see if the IMEI matches the account number, if there is one.
      !    If Error# = 0 And job:Account_Number <> ''
      !        If ExchangeAccount(job:Account_Number)
      !            Access:EXCHANGE.ClearKey(xch:ESN_Only_Key)
      !            xch:ESN = job:ESN
      !            If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
      !                !Found
      !                !Write the relevant fields to say this unit is in repair and take completed.
      !                !Otherwise the use could click cancel and screw everything up.
      !            Else!If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
      !                !Error
      !                Case Missive('The selected trade account is set-up to repair exchange units, but the entered I.M.E.I. number cannot be found in exchange stock.','ServiceBase 3g',|
      !                               'mstop.jpg','/OK')
      !                    Of 1 ! OK Button
      !                End ! Case Missive
      !                Error# = 1
      !            End!If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
      !        Else !ExchangeAccount(job:Account_Number)
      !            Access:EXCHANGE.Clearkey(xch:ESN_Only_Key)
      !            xch:ESN = job:ESN
      !            If Access:EXCHANGE.Tryfetch(xch:ESN_Only_Key) = Level:Benign
      !                !Found
      !                If xch:Available <> 'DES'
      !                    Case Missive('The entered I.M.E.I. matches an entry in Exchange Stock. This unit has not marked "Despatched" so may still exist in stock.'&|
      !                      '|If you continue this job will be a normal repair and not an "In House Exchange Repair".'&|
      !                      '|Are you sure you want to continue?','ServiceBase 3g',|
      !                                   'mquest.jpg','\No|/Yes')
      !                        Of 2 ! Yes Button
      !                        Of 1 ! No Button
      !                            Error# = 1
      !                    End ! Case Missive
      !                End!Case !MessageEx
      !
      !            Else! If Access:EXCHANGE.Tryfetch(xch:ESN_Only_Key) = Level:Benign
      !                !Error
      !                !Assert(0,'<13,10>Fetch Error<13,10>')
      !            End! If Access:EXCHANGE.Tryfetch(xch:ESN_Only_Key) = Level:Benign
      !        End !ExchangeAccount(job:Account_Number)
      !    End !Error# = 0
      ! End --- (DBH: 24/02/2009) #10454
      
      
          Display()
          Do check_msn
          Do bouncer_text
          Do check_msn
          do Web_Check_Imei
      
          if ?job:MSN{prop:Hide} = true
              tmp:DisplayPOP = true
              do BastionCheck
          end
      
          ! Start - Validate the IMEI via the MQ Process - TrkBs: 3287 (DBH: 27-09-2005)
          Do MQ:Process
          ! End   - Validate the IMEI via the MQ Process - TrkBs: 3287 (DBH: 27-09-2005)
      
          local.CopyDOPFromBouncer(previousDOP#)
      
          ! Inserting (DBH 23/10/2006) # 8389 - Code moved inside "IF" so that it isn't called when the OK button is pressed
          if job:bouncer = 'B' then
              !add code to run MSN accepted embeds
              do MSN_Embeds
              !and code to set the charge type
              do set_Bouncer_Charge_type
          END!if job:bouncer
      
          if job:Warranty_Job = 'YES' then enable(?Lookup_Warranty_Charge_Type).
      
          Thiswindow.update()
          display()
          ! End (DBH 23/10/2006) #8389
      
      End!If ~0{prop:acceptall}
      Do CheckRequiredFields
      Do Display_Menu_Option
      
      ! Inserting (DBH 09/03/2007) # 8718 - Once the IMEI has been entered, do not allow to change the transit type
      If Clip(job:ESN) <> '' And Clip(job:Model_Number) = ''
          Do OBFCheck
          ?job:Transit_Type{prop:Disable} = 1
          ?Lookup_Transit_Type{prop:Hide} = 1
          !Bryan.CompFieldColour()
      End ! If Clip(job:ESN) <> ''
      ! End (DBH 09/03/2007) #8718
    OF ?job:MSN
      if ~0{prop:acceptall}
          do MSN_Embeds
      End!If ~0{prop:acceptall}
      Do CheckRequiredFields
      do Display_Menu_Option
    OF ?job:ProductCode
      Mop:ModelNumber = Job:Model_number
      IF job:ProductCode OR ?job:ProductCode{Prop:Req}
        mop:ProductCode = job:ProductCode
        !Save Lookup Field Incase Of error
        look:job:ProductCode        = job:ProductCode
        IF Access:MODPROD.TryFetch(mop:ProductCodeKey)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            job:ProductCode = mop:ProductCode
          ELSE
            !Restore Lookup On Error
            job:ProductCode = look:job:ProductCode
            SELECT(?job:ProductCode)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupProductCode
      ThisWindow.Update
      GLO:Select50 = job:Model_Number
      Mop:ModelNumber = Job:Model_number
      mop:ProductCode = job:ProductCode
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          job:ProductCode = mop:ProductCode
          Select(?+1)
      ELSE
          Select(?job:ProductCode)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:ProductCode)
    OF ?job:Model_Number
      If ~0{prop:acceptall}
      IF job:Model_Number OR ?job:Model_Number{Prop:Req}
        mod:Model_Number = job:Model_Number
        !Save Lookup Field Incase Of error
        look:job:Model_Number        = job:Model_Number
        IF Access:MODELNUM.TryFetch(mod:Model_Number_Key)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            job:Model_Number = mod:Model_Number
          ELSE
            !Restore Lookup On Error
            job:Model_Number = look:job:Model_Number
            SELECT(?job:Model_Number)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      !Added by Neil!
      !Changed by Paul - 15/06/2010 - log no 11466
      if clip(job:ESN) <> ''
          job:Model_Number = IMEIModelRoutine(job:ESN,job:Model_Number)
          If clip(job:Model_Number) = 'ERROR' then
              job:Model_Number = ''
              job:ESN = ''
              select(?job:ESN)
          End
      END !if job:ESN exists
      
      access:modelnum.clearkey(mod:model_number_key)
      mod:model_number = job:model_number
      If access:modelnum.fetch(mod:model_number_key) = Level:Benign
          job:manufacturer = mod:manufacturer
          !Only autofil the Product Code Fault Code if it's Nokia and they
          !are not using the other Product Code field.
          !Only autofil the Product Code Fault Code if it's Nokia and they
          !are not using the other Product Code field.
          If ?job:ProductCode{prop:Hide} = 1
              If job:manufacturer = 'SIEMENS' Or job:manufacturer = 'NOKIA'  Or job:manufacturer = 'MOTOROLA'
                  job:fault_code1 = mod:product_type
              End!If job:manufacturer = 'SIEMENS' Or job:manufacturer = 'NOKIA' Or job:manufacturer = 'MOTOROLA'
          Else !If ?job:ProductCode{prop:Hide} = 1
              If job:manufacturer = 'SIEMENS' Or job:manufacturer = 'NOKIA'  Or job:manufacturer = 'MOTOROLA'
                  job:fault_code1 = job:ProductCode
              End!If job:manufacturer = 'SIEMENS' Or job:manufacturer = 'NOKIA' Or job:manufacturer = 'MOTOROLA'
          End !If ?job:ProductCode{prop:Hide} = 1
          model_number_temp   = job:model_number
      End!If access:modelnum.fetch(mod:model_number_key)
      
      count# = 0
      colour" = ''
      save_moc_id = access:modelcol.savefile()
      access:modelcol.clearkey(moc:colour_key)
      moc:model_number = job:model_number
      set(moc:colour_key,moc:colour_key)
      loop
          if access:modelcol.next()
             break
          end !if
          if moc:model_number <> job:model_number      |
              then break.  ! end if
          count# += 1
          colour" = moc:colour
          If count# > 1
              Break
          End!If count# > 1
      end !loop
      access:modelcol.restorefile(save_moc_id)
      If count# = 1
          job:colour  = colour"
      End!If count# = 1
      
      !!Added by Neil!
      !job:Model_Number = IMEIModelRoutine(job:ESN,job:Model_Number)
      !If clip(job:Model_Number) = 'ERROR' then
      !    job:Model_Number = ''
      !    job:ESN = ''
      !End
      
      !DO ESN_Model_Check
      
      !do check_msn
      
      If mod:specify_unit_type = 'YES'
          job:unit_type   = mod:unit_type
      End!If mod:specify_unit_type = 'YES'
      
      Do Check_MSN
      
      If CheckLength('IMEI',job:Model_Number,job:ESN)
          job:ESN = ''
          Select(?job:ESN)
          Error# = 1
      End !job:ESN <> '*INVALID IMEI*'
      
      Do Allow48Hour
      End !If ~0{prop:acceptall}
      
      Do CheckRequiredFields
      ! Inserting (DBH 09/03/2007) # 8718 - Once the IMEI has been entered, do not allow to change the transit type
      ! Put the code here as well as on ESN to make sure the Model Number validation is done before calling the obf (DBH: 12/03/2007)
      If Clip(job:ESN) <> '' And Clip(job:Model_Number) <> '' And ?job:Transit_Type{prop:Disable} = 0
          Do OBFCheck
          ?job:Transit_Type{prop:Disable} = 1
          ?Lookup_Transit_Type{prop:Hide} = 1
          !Bryan.CompFieldColour()
      End ! If Clip(job:ESN) <> ''
      ! End (DBH 09/03/2007) #8718
    OF ?Lookup_Model_Number
      ThisWindow.Update
      mod:Model_Number = job:Model_Number
      
      IF SELF.RUN(3,Selectrecord)  = RequestCompleted
          job:Model_Number = mod:Model_Number
          Select(?+1)
      ELSE
          Select(?job:Model_Number)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Model_Number)
    OF ?job:Unit_Type
      IF job:Unit_Type OR ?job:Unit_Type{Prop:Req}
        uni:Unit_Type = job:Unit_Type
        !Save Lookup Field Incase Of error
        look:job:Unit_Type        = job:Unit_Type
        IF Access:UNITTYPE.TryFetch(uni:Unit_Type_Key)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            job:Unit_Type = uni:Unit_Type
          ELSE
            !Restore Lookup On Error
            job:Unit_Type = look:job:Unit_Type
            SELECT(?job:Unit_Type)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?Lookup_Unit_Type
      ThisWindow.Update
      uni:Unit_Type = job:Unit_Type
      
      IF SELF.RUN(4,Selectrecord)  = RequestCompleted
          job:Unit_Type = uni:Unit_Type
          Select(?+1)
      ELSE
          Select(?job:Unit_Type)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Unit_Type)
    OF ?job:Colour
      IF job:Colour OR ?job:Colour{Prop:Req}
        moc:Colour = job:Colour
        moc:Model_Number = JOB:Model_Number
        !Save Lookup Field Incase Of error
        look:job:Colour        = job:Colour
        IF Access:MODELCOL.TryFetch(moc:Colour_Key)
          IF SELF.Run(5,SelectRecord) = RequestCompleted
            job:Colour = moc:Colour
          ELSE
            CLEAR(moc:Model_Number)
            !Restore Lookup On Error
            job:Colour = look:job:Colour
            SELECT(?job:Colour)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?Lookup_Colour
      ThisWindow.Update
      moc:Colour = job:Colour
      moc:Model_Number = JOB:Model_Number
      
      IF SELF.RUN(5,Selectrecord)  = RequestCompleted
          job:Colour = moc:Colour
          Select(?+1)
      ELSE
          Select(?job:Colour)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Colour)
    OF ?tmp:Network
      IF tmp:Network OR ?tmp:Network{Prop:Req}
        net:Network = tmp:Network
        !Save Lookup Field Incase Of error
        look:tmp:Network        = tmp:Network
        IF Access:NETWORKS.TryFetch(net:NetworkKey)
          IF SELF.Run(6,SelectRecord) = RequestCompleted
            tmp:Network = net:Network
          ELSE
            !Restore Lookup On Error
            tmp:Network = look:tmp:Network
            SELECT(?tmp:Network)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupNetwork
      ThisWindow.Update
      net:Network = tmp:Network
      
      IF SELF.RUN(6,Selectrecord)  = RequestCompleted
          tmp:Network = net:Network
          Select(?+1)
      ELSE
          Select(?tmp:Network)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Network)
    OF ?job:Mobile_Number
      ! Deleted (DBH 22/06/2006) #7597 - Incorporate the mobile number length checking into the one routine
      ! ! Check the default length of the mobile no, IF it's required - TrkBs: 6141 (DBH: 24-08-2005)
      ! If ?job:Mobile_Number{prop:req} = True
      !     If CheckLength('MOBILE', '', job:Mobile_Number)
      !         job:Mobile_Number = ''
      !         Select(?job:Mobile_Number)
      !         Cycle
      !     End ! job:ESN <> '*INVALID IMEI*'
      ! End ! If ?job:Mobile_Number{prop:req} = True
      ! ! End   - Check the default length of the mobile no, IF it's required - TrkBs: 6141 (DBH: 24-08-2005)
      ! End (DBH 22/06/2006) #7597
      
      ! Inserting (DBH 03/05/2006) #7149 - Check the format of the Mobile Number
      If PassMobileNumberFormat(job:Mobile_Number,1) = False
          job:Mobile_Number = ''
          Select(?job:Mobile_Number)
          Cycle
      End ! If PassMobileNumberLengh(job:Mobile_Number,1) = False
      ! End (DBH 03/05/2006) #7149
      
      If ~0{prop:AcceptAll}
          Do MSISDNRoutine
          ! Inserting (DBH 07/12/2007) # 9491 - If SMS Notification, replicate the mobile number to the SMS Alert Number
          If job:Mobile_Number <> ''
              Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
              jobe2:RefNumber = job:Ref_Number
              If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
                  If jobe2:SMSNotification And jobe2:SMSAlertNumber = ''
                      jobe2:SMSAlertNumber = job:Mobile_Number
                      Access:JOBSE2.TryUpdate()
                  End ! If jobe2:SMSNotification And jobe2:SMSAlertNumber = ''
              End ! If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
          End ! If job:Mobile_Number <> ''
          ! End (DBH 07/12/2007) #9491
      
      End ! If ~0{prop:AcceptAll}
      
      
    OF ?job:Chargeable_Job
      IF ?job:Chargeable_Job{Prop:Checked} = True
        ENABLE(?lookup_Charge_Type)
        ENABLE(?JOB:Charge_Type)
      END
      IF ?job:Chargeable_Job{Prop:Checked} = False
        job:Charge_Type = ''
        DISABLE(?lookup_Charge_Type)
        DISABLE(?JOB:Charge_Type)
      END
      ThisWindow.Reset
      if job:chargeable_job{prop:disable}=1 then disable(?lookup_Charge_type).
      If ~0{prop:acceptall}
          If job:chargeable_job = 'YES'
              Set(DEFAULTS)
              Access:DEFAULTS.Next()
              If def:ChaChargeType <> ''
                    !message('PaulMessage 5')
                  !changed by Paul 22/09/09 - log no 10785
                  !only apply the default charge type if no charge type has been selected
                  If clip(job:Charge_Type) = '' then
                      job:Charge_Type = def:ChaChargeType
                  End!If clip(job:Charge_Type) = '' then
                  !end change
              Else!If def:ChaChargeType <> ''
                  charge_type"    = ''
                  found# = 0
                  save_cha_id = access:chartype.savefile()                                            !It's too late, but the warranty tick is not
                  access:chartype.clearkey(cha:warranty_key)                                          !Defaulting to no
                  cha:warranty    = 'NO'
                  set(cha:warranty_key,cha:warranty_key)
                  loop
                      if access:chartype.next()
                         break
                      end !if
                      If cha:warranty <> 'NO' Then Break.
                      found# += 1
                      charge_type"    = cha:charge_type
                      If found# > 1
                          Break
                      End!If found# > 1
      
                  end !loop
                  access:chartype.restorefile(save_cha_id)
                  If found# = 1
                        !message('PaulMessage 6')
                      job:charge_type = charge_type"
                      Do Estimate_Check
                      !Select(?job:warranty_job)
                  Else
                      !Select(?lookup_charge_type)
                  End!If found# = 1
              End!If def:ChaChargeType <> ''
              Display(?job:charge_type)
      
          End!If job:warranty_job = 'YES'
      End!If ~0{prop:acceptall}
      Do CheckRequiredFields
      do Display_Menu_Option
    OF ?lookup_Charge_Type
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickChargeableChargeTypes
      ThisWindow.Reset
      case globalresponse
          of requestcompleted
                !message('PaulMessage 7')
              job:charge_type = cha:charge_type
              Do Estimate_Check
              select(?+2)
          of requestcancelled
      !        job:charge_type = ''
              select(?-1)
      end!case globalreponse
      display(?job:charge_type)
      
      Do CheckRequiredFields
      do Display_Menu_Option
    OF ?job:Warranty_Job
      IF ?job:Warranty_Job{Prop:Checked} = True
        ENABLE(?Lookup_Warranty_Charge_Type)
        ENABLE(?JOB:Warranty_Charge_Type)
      END
      IF ?job:Warranty_Job{Prop:Checked} = False
        job:Warranty_Charge_Type = ''
        DISABLE(?Lookup_Warranty_Charge_Type)
        DISABLE(?JOB:Warranty_Charge_Type)
      END
      ThisWindow.Reset
      If ~0{prop:acceptall}
          ! Inserting (DBH 23/10/2006) # 8389 - Only overwrite the default charge type if it isn't already filled in
          If job:warranty_job = 'YES' And job:Warranty_Charge_Type = ''
          ! End (DBH 23/10/2006) #8389
              Set(DEFAULTS)
              Access:DEFAULTS.Next()
              If def:WarChargeType <> ''
                  if Job:Warranty_Charge_type <> 'WARRANTY (OBF)' then
                    job:Warranty_Charge_Type = def:WarChargeType
                  END !stop them changing OBF
              Else!If def:WarChargeType <> ''
                  charge_type"    = ''
                  found# = 0
                  save_cha_id = access:chartype.savefile()
                  access:chartype.clearkey(cha:warranty_key)
                  cha:warranty    = 'YES'
                  set(cha:warranty_key,cha:warranty_key)
                  loop
                      if access:chartype.next()
                         break
                      end !if
                      if cha:warranty    <> 'YES'      |
                          then break.  ! end if
                      found# += 1
                      charge_type"    = cha:charge_type
                      If found# > 1
                          Break
                      End!If found# > 1
      
                  end !loop
                  access:chartype.restorefile(save_cha_id)
                  If found# = 1
                    if Job:Warranty_Charge_type <>  'WARRANTY (OBF)' then
                      job:warranty_Charge_type = charge_type"
                      !Select(?JOB:Intermittent_Fault)
                    END !stop changing OBF
                  Else
                      !Select(?lookup_warranty_charge_type)
                  End!If found# = 1
              End!If def:WarChargeType <> ''
              Display(?job:warranty_charge_type)
      
          End!If job:warranty_job = 'YES'
      End!If ~0{prop:acceptall}
      If job:warranty_job = 'NO'
          job:edi = 'XXX'
          job:warranty_charge_type = ''
      End!If job:warranty_job = 'NO'
      Display()
      Do CheckRequiredFields
    OF ?Lookup_Warranty_Charge_Type
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickWarrantyChargeTypes
      ThisWindow.Reset
      case globalresponse
          of requestcompleted
              job:warranty_charge_type = cha:charge_type
              select(?+2)
          of requestcancelled
      !        job:charge_type = ''
              select(?-1)
      end!case globalreponse
      display(?job:warranty_charge_type)
      
      Do CheckRequiredFields
      do Display_Menu_Option
    OF ?job:Intermittent_Fault
      If ~0{prop:acceptall}
          If job:intermittent_fault = 'YES'
              access:jobnotes.clearkey(jbn:refnumberkey)
              jbn:refnumber = job:ref_number
              If access:jobnotes.fetch(jbn:refnumberkey) = Level:Benign
                  if PRE:FaultText <> ''
                      jbn:Fault_Description = PRE:FaultText
                  END
                  If jbn:fault_description = ''
                      jbn:fault_description = 'INTERMITTENT FAULT'
                  Else!If job:fault_description = ''
                      jbn:fault_description = Clip(jbn:fault_description) & '<13,10>INTERMITTENT FAULT'
                  End!If job:fault_description = ''
              End!If access:jobnotes.fetch(jbn:ref_number_key) = Level:Benign
          End!If job:intermittent_fault = 'YES'
      End!If ~0{prop:acceptall}
    OF ?job:DOP
      If ~0{prop:acceptall}
          error# = 0
          If job:dop > Today()
              Case Missive('You have entered an invalid date.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Select(?job:dop)
              error# = 1
          End!If job:dop > Today()
          If error# = 0
              If job:warranty_job = 'YES'
                  access:manufact.clearkey(man:manufacturer_key)
                  man:manufacturer = job:manufacturer
                  if access:manufact.fetch(man:manufacturer_key) = Level:Benign
                      !*WORKAROUND*
                      IF job:Warranty_Charge_Type = 'WARRANTY (2ND YR)'
                        man:warranty_period = 730
                      END
                      If job:dop < (job:date_booked - man:warranty_period) and man:warranty_period <> 0 and job:dop <> ''
                          Case Missive('This unit is outside the manufacturer''s warranty period .','ServiceBase 3g',|
                                         'mstop.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                          GetStatus(130,ThisWindow.Request,'JOB') !DOP Query
      
                          !If job is OBF do not make Chargeable, otherwise do - L949 (DBH: 15-09-2003)
                          If job:Warranty_Charge_Type <> 'WARRANTY (OBF)'
                              job:Warranty_Job = 'NO'
                              job:Warranty_Charge_Type = ''
                              job:Chargeable_Job = 'YES'
                              !message('PaulMessage 8')
                              job:Charge_Type = 'NON-WARRANTY'
                              ?job:Warranty_Job{prop:Disable} = 1
                              ?job:Chargeable_Job{prop:Disable} = 0
                              Post(Event:Accepted,?job:Warranty_Job)
                              Post(Event:Accepted,?job:Chargeable_Job)
                          End !If job:Warranty_Charge_Type <> 'WARRANTY (OBF)'
      
                          DO time_remaining
                          If AddToAudit(job:Ref_Number,'JOB','D.O.P. OVER WARRANTY PERIOD BY ' & (job:date_booked - (job:dop + man:warranty_period)) & 'DAY(S)','')
      
                          End ! If AddToAudit(job:Ref_Number,'JOB','D.O.P. OVER WARRANTY PERIOD BY ' & (job:date_booked - (job:dop + man:warranty_period)) & 'DAY(S)','')
                      Else
                          !Incase the fuckwits put the wrong date in and try and validate it again
                          !change the status to the initial transit type default
                          If Sub(job:Current_Status,1,3) = '130'
                              Access:TRANTYPE.ClearKey(trt:Transit_Type_Key)
                              trt:Transit_Type = job:Transit_Type
                              If Access:TRANTYPE.TryFetch(trt:Transit_Type_Key) = Level:Benign
                                  !Found
                                  GetStatus(Sub(trt:Initial_Status,1,3),0,'JOB')
                              Else!If Access:TRANTYPE.TryFetch(trt:Transit_Type_Key) = Level:Benign
                                  !Error
                                  !Assert(0,'<13,10>Fetch Error<13,10>')
                              End!If Access:TRANTYPE.TryFetch(trt:Transit_Type_Key) = Level:Benign
                          End !If Sub(job:Current_Status,1,3) = '130'
                      End!If job:dop > man:warranty_period + Today()
                  end!if access:manufact.fetch(man:manufacturer_key) = Level:Benign
              End!If job:warranty_job = 'YES'
          End!If error# = 0
      End!If ~0{prop:acceptall}
      
    OF ?locPOPConfirmed
      ! #11912 Call the POP Validation screen if this is ticked. (Bryan: 01/02/2011)
      IF (NOT 0{PROP:AcceptAll})
          IF (locPOPConfirmed = 1)
              tmp:POPType = 'POP'
              Post(Event:Accepted,?Validate_POP)
          END
      END
      
    OF ?job:Account_Number
      glo:Select1   = ''
      If glo:WebJob = 1
          glo:Select1   = Clarionet:Global.Param2
      Else !If glo:WebJob = 1
          If GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI') <> ''
              glo:Select1 = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
          End !If GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI') <> ''
      End !If glo:WebJob = 1
      IF job:Account_Number OR ?job:Account_Number{Prop:Req}
        sub:Account_Number = job:Account_Number
        !Save Lookup Field Incase Of error
        look:job:Account_Number        = job:Account_Number
        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
          IF SELF.Run(7,SelectRecord) = RequestCompleted
            job:Account_Number = sub:Account_Number
          ELSE
            !Restore Lookup On Error
            job:Account_Number = look:job:Account_Number
            SELECT(?job:Account_Number)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      glo:Select1 = ''
      !sssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
      DO SETJOBSE
      If ~0{prop:acceptall}
          error# = 0
          stop# = 0
      
          Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
          sub:Account_Number = job:Account_Number
          If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
              Access:TRADEACC.Clearkey(tra:Account_Number_Key)
              tra:Account_Number = sub:Main_Account_Number
              If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                  If tra:Invoice_Sub_Accounts = 'YES' And tra:Use_Sub_Accounts = 'YES'
                      If sub:ForceOrderNumber
                          ?job:Order_Number{prop:req} = 1
                      Else !If tra:ForceOrderNumber
                          ?job:Order_Number{prop:req} = 0
                      End !If tra:ForceOrderNumber
      
                      If sub:Stop_Account = 'YES'
                          stop# = 1
                      End !If tra:Stop_Account = 'YES'
                  Else !If tra:Invoice_Sub_Accounts = 'YES' And tra:Use_Sub_Accounts = 'YES'
                      If tra:ForceOrderNumber
                          ?job:Order_Number{prop:req} = 1
                      Else !If tra:ForceOrderNumber
                          ?job:Order_Number{prop:req} = 0
                      End !If tra:ForceOrderNumber
      
                      If tra:Stop_Account = 'YES'
                          stop# = 1
                      End !If tra:Stop_Account = 'YES'
                  End !If tra:Invoice_Sub_Accounts = 'YES' And tra:Use_Sub_Accounts = 'YES'
              End ! If ACcess:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
          End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
      
      ! Inserting (DBH 04/02/2008) # 9613 - Check that the account picked is in the hub?
          If Stop# = 0
              If tra:Invoice_Sub_Accounts <> 'YES'
                  If tra:Use_Customer_Address <> 'YES'
                      If HubOutOfRegion(wob:HeadAccountNumber,tra:Hub) = 1
                          Beep(Beep:SystemExclamation);  Yield()
                          Case Missive('Warning! The selected Account is outside your Hub.'&|
                              '|If you continue you may not be able to despatch this job.'&|
                              '|'&|
                              '|Are you sure you want to continue?','ServiceBase 3g',|
                                         'mexclam.jpg','/No|\Yes')
                              Of 2 ! Yes Button
                              Of 1 ! No Button
                                  job:Account_Number = ''
                                  Select(?job:Account_Number)
                                  Display()
                                  Cycle
                          End ! Case Missive
                      End ! If HubOutOfRegion(wob:HeadAccountNumber,tra:Hub) = 1
                  End ! If tra:Use_Customer_Address <> 'YES'
              Else ! If tra:Invoice_Sub_Accounts <> 'YES'
                  If sub:Use_Customer_Address <> 'YES'
                      If HubOutOfRegion(wob:HeadAccountNumber,sub:Hub) = 1
                          Beep(Beep:SystemExclamation);  Yield()
                          Case Missive('Warning! The selected Account is outside your Hub.'&|
                              '|If you continue you may not be able to despatch this job.'&|
                              '|'&|
                              '|Are you sure you want to continue?','ServiceBase 3g',|
                                         'mexclam.jpg','/No|\Yes')
                              Of 2 ! Yes Button
                              Of 1 ! No Button
                                  job:Account_Number = ''
                                  Select(?job:Account_Number)
                                  Display()
                                  Cycle
                          End ! Case Missive
                      End ! If HubOutOfRegion(wob:HeadAccountNumber,tra:Hub) = 1
                  End ! If sub:Use_Customer_Address <> 'YES'
              End ! If tra:Invoice_Sub_Accounts <> 'YES'
          End ! If Stop# = 0
      ! End (DBH 04/02/2008) #9613
      
          If stop# = 0
              error# = 0
              access:trantype.clearkey(trt:transit_type_key)
              trt:transit_type = job:transit_type
              if access:trantype.fetch(trt:transit_type_key) = Level:Benign
                  If trt:loan_unit = 'YES' And tra:allow_loan <> 'YES'
                      Case Missive('Loan Units are not authorised for this trade customer. Amend the trade customer''s details or select another Initial Transit Type.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      error# = 1
                  End !If trt:loan_unit = 'YES' And tra:allow_loan <> 'YES'
                  If trt:exchange_unit = 'YES' And tra:allow_exchange <> 'YES' And error# = 0
                      Case Missive('Exchange Units are not authorised for this trade customer. Amend the trade customer''s details or select another Initial Transit Type.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      error# = 1
                  End !If trt:exchange_unit = 'YES' And tra:allow_loan <> 'YES'
              end
      
              !Check to see if the IMEI matches the account number, if there is one.
              If Error# = 0 And job:ESN <> ''
      ! Delete --- Not needed. Should of been replaced earlier (DBH: 11/03/2009) #10473
      !            If ExchangeAccount(job:Account_Number)
      !                Access:EXCHANGE.ClearKey(xch:ESN_Only_Key)
      !                xch:ESN = job:ESN
      !                If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
      !                    !Found
      !                    !Write the relevant fields to say this unit is in repair and take completed.
      !                    !Otherwise the use could click cancel and screw everything up.
      !                Else!If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
      !                    !Error
      !                    Case Missive('The selected Trade Account is set-up to repair exchange units but the entered I.M.E.I. number cannot be found in exchange stock.','ServiceBase 3g',|
      !                                   'mstop.jpg','/OK')
      !                        Of 1 ! OK Button
      !                    End ! Case Missive
      !                    Error# = 1
      !                End!If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
      !            Else
      !                Access:EXCHANGE.Clearkey(xch:ESN_Only_Key)
      !                xch:ESN = job:ESN
      !                If Access:EXCHANGE.Tryfetch(xch:ESN_Only_Key) = Level:Benign
      !                    !Found
      !                    If xch:Available <> 'DES'
      !                        Case Missive('The entered I.M.E.I. matches an entry in Exchange Stock. This unit has not marked "Despatched" so may still exist in stock.'&|
      !                          '|If you continue this job will be a normal repair and not an "In House Exchange Repair".'&|
      !                          '|Are you sure you want to continue?','ServiceBase 3g',|
      !                                       'mquest.jpg','\No|/Yes')
      !                            Of 2 ! Yes Button
      !                            Of 1 ! No Button
      !                                Error# = 1
      !                        End ! Case Missive
      !                    End!Case !MessageEx
      !
      !                Else! If Access:EXCHANGE.Tryfetch(xch:ESN_Only_Key) = Level:Benign
      !                    !Error
      !                    !Assert(0,'<13,10>Fetch Error<13,10>')
      !                End! If Access:EXCHANGE.Tryfetch(xch:ESN_Only_Key) = Level:Benign
      !
      !            End !ExchangeAccount(job:Account_Number)
      ! end --- (DBH: 11/03/2009) #10473
      
              End !Error# = 0
      
              If error# = 0
                  access:trantype.clearkey(trt:transit_type_key)
                  trt:transit_type = job:transit_type
                  if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
                      If tra:refurbcharge = 'YES' And trt:exchange_unit = 'YES'
                          job:chargeable_job  = 'YES'
                          job:warranty_job    = 'YES'
                          !message('PaulMessage 9')
                          job:charge_type = tra:chargetype
                          job:warranty_charge_type    = tra:warchargetype
                      End!If tra:refurbcharge = 'YES'
                  end!if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
                  If tra:turnaround_time <> ''
                      job:turnaround_time = tra:turnaround_time
                  End!If tra:turnaround_time <> ''

                  !TB13307 - now have three levels of requirement Show, force or hide
                  Case CustomerNameRequired(job:Account_Number)
                  of 1
                    !force
                    Do ForceCustomerFields
                  of 2
                    !show
                    Do ShowCustomerFields
                  of 0
                    !hide
                    Do HideCustomerFields
                  END

!                  If CustomerNameRequired(job:Account_Number) = Level:Benign
!                      Unhide(?job:surname)
!                      Unhide(?job:title)
!                      Unhide(?job:title:prompt)
!                      Unhide(?job:surname:prompt)
!                      Unhide(?customer_name)
!                      unhide(?job:initial:prompt)
!                      unhide(?job:initial)
!                      unhide(?jobe:EndUserTelNo:Prompt)
!                      unhide(?tmp:EndUserTelNo)
!                  Else
!                      Hide(?job:surname)
!                      Hide(?job:title)
!                      Hide(?job:title:prompt)
!                      Hide(?job:surname:prompt)
!                      Hide(?customer_name)
!                      Hide(?job:initial:prompt)
!                      Hide(?job:initial)
!                      Hide(?jobe:EndUserTelNo:Prompt)
!                      Hide(?tmp:EndUserTelNo)
!                  End
      
      !Estimate
                  If tra:Use_Sub_Accounts = 'YES' and tra:Invoice_Sub_Accounts = 'YES'
                      If sub:ForceEstimate
                          job:estimate_if_over    = sub:EstimateIfOver
                          job:estimate        = 'YES'
                      End!If tra:force_estimate = 'YES'
                  Else !If tra:Use_Sub_Accounts = 'YES' and tra:Invoice_Sub_Accounts = 'YES
      
                      If tra:force_estimate = 'YES'
                          job:estimate_if_over    = tra:estimate_if_over
                          job:estimate        = 'YES'
                      End!If tra:force_estimate = 'YES'
      
                  End !If tra:Use_Sub_Accounts = 'YES' and tra:Invoice_Sub_Accounts = 'YES
      
                  ! Inserting (DBH 04/02/2008) # 9613 - If "Cash" sale, default the hub to the RRC's. Although this will change
                  If Instring('CASH',Upper(job:Account_Number),1,1)
                      Access:TRADEACC_ALIAS.Clearkey(tra_ali:Account_Number_Key)
                      tra_ali:Account_number = wob:HeadAccountNumber
                      If Access:TRADEACC_ALIAS.TryFetch(tra_Ali:Account_Number_Key) = Level:Benign
                          jobe2:HubCustomer = tra:Hub
                          jobe2:HubCollection = tra:Hub
                          jobe2:HubDelivery = tra:Hub
                      End ! If Access:TRADEACC_ALIAS.TryFetch(tra_Ali:Account_Number_Key) = Level:Benign
                  End ! If Instring('CASH',Upper(job:Account_Number),1,1)
                  ! End (DBH 04/02/2008) #9613
      
                  If tra:invoice_sub_accounts  <> 'YES'
                      If tra:use_customer_address <> 'YES'
                          JOB:Postcode            = tra:Postcode
                          ! Insert --- Don't fill the companyname in, if it's a refurb rebooking (DBH: 24/03/2009) #10473
                          if (f:NewJobBookingGroup = 0)
                              JOB:Company_Name        = tra:Company_Name
                          end ! if (NewJobBookingGroup.Type <> '48HR')
                          ! end --- (DBH: 24/03/2009) #10473
                          JOB:Address_Line1       = tra:Address_Line1
                          JOB:Address_Line2       = tra:Address_Line2
                          JOB:Address_Line3       = tra:Address_Line3
                          JOB:Telephone_Number    = tra:Telephone_Number
                          JOB:Fax_Number          = tra:Fax_Number
                          jobe:EndUserEmailAddress = tra:EmailAddress
                          jobe2:HubCustomer = tra:Hub
                      End!If tra:use_customer_address <> 'YES'
                  Else!If tra:invoice_sub_accounts  <> 'YES'
                      If sub:use_customer_address <> 'YES'
                          JOB:Postcode            = sub:Postcode
                          ! Insert --- Don't fill the companyname in, if it's a refurb rebooking (DBH: 24/03/2009) #10473
                          if (f:NewJobBookingGroup = 0)
                              JOB:Company_Name        = sub:Company_Name
                          end ! if (NewJobBookingGroup.Type <> '48HR')
                          ! end --- (DBH: 24/03/2009) #10473
                          JOB:Address_Line1       = sub:Address_Line1
                          JOB:Address_Line2       = sub:Address_Line2
                          JOB:Address_Line3       = sub:Address_Line3
                          JOB:Telephone_Number    = sub:Telephone_Number
                          JOB:Fax_Number          = sub:Fax_Number
                          jobe:EndUserEmailAddress = sub:EmailAddress
                          jobe2:HUbCustomer = sub:Hub
                      End!If sub:use_customer_address <> 'YES'
                  End!If tra:invoice_sub_accounts  <> 'YES'
      
                  !save the e-mail address before jobe is reset
                  Access:JOBSE.TryUpdate()
      
                  If tra:use_sub_accounts <> 'YES'
                      If tra:use_delivery_address = 'YES'
                          job:postcode_delivery   = tra:postcode
                          ! Insert --- Don't fill the companyname in, if it's a refurb rebooking (DBH: 24/03/2009) #10473
                          if (f:NewJobBookingGroup = 0)
                              job:company_name_delivery   = tra:company_name
                          end ! if (NewJobBookingGroup.Type <> '48HR')
                          ! end --- (DBH: 24/03/2009) #10473
      
                          job:address_line1_delivery  = tra:address_line1
                          job:address_line2_delivery  = tra:address_line2
                          job:address_line3_delivery  = tra:address_line3
                          job:telephone_delivery      = tra:telephone_number
                          jobe2:HubDelivery = tra:Hub
                      End !If tra:use_delivery_address = 'YES'
                      If tra:use_collection_address = 'YES'
                          job:postcode_collection   = tra:postcode
                          job:company_name_collection   = tra:company_name
                          job:address_line1_collection  = tra:address_line1
                          job:address_line2_collection  = tra:address_line2
                          job:address_line3_collection  = tra:address_line3
                          job:telephone_collection      = tra:telephone_number
                          jobe2:HubCollection = tra:Hub
                      End !If tra:use_delivery_address = 'YES'
      
                      job:courier = tra:courier_outgoing
                      job:incoming_courier = tra:courier_incoming
                      job:exchange_courier = job:courier
                      job:loan_courier = job:courier
                  Else
                      If sub:use_delivery_address = 'YES'
                          job:postcode_delivery      = sub:postcode
                          ! Insert --- Don't fill the companyname in, if it's a refurb rebooking (DBH: 24/03/2009) #10473
                          if (f:NewJobBookingGroup = 0)
                              job:company_name_delivery   = sub:company_name
                          end ! if (NewJobBookingGroup.Type <> '48HR')
                          ! end --- (DBH: 24/03/2009) #10473
                          job:company_name_delivery  = sub:company_name
                          job:address_line1_delivery = sub:address_line1
                          job:address_line2_delivery = sub:address_line2
                          job:address_line3_delivery = sub:address_line3
                          job:telephone_delivery     = sub:telephone_number
                          jobe2:HubDelivery = sub:Hub
                      End
                      If sub:use_collection_address = 'YES'
                          job:postcode_collection      = sub:postcode
                          job:company_name_collection  = sub:company_name
                          job:address_line1_collection = sub:address_line1
                          job:address_line2_collection = sub:address_line2
                          job:address_line3_collection = sub:address_line3
                          job:telephone_collection     = sub:telephone_number
                          jobe2:HubCollection          = sub:Hub
                      End
      
                      job:courier = sub:courier_outgoing
                      job:incoming_courier = sub:courier_incoming
                      job:exchange_courier = job:courier
                      job:loan_courier = job:courier
                  End
                  access:courier.clearkey(cou:courier_key)
                  cou:courier = job:courier
                  access:courier.tryfetch(cou:courier_key)
                  IF cou:courier_type = 'CITY LINK' Or cou:courier_Type = 'LABEL G'
                      job:loaservice  = cou:service
                      job:excservice  = cou:service
                      job:jobservice  = cou:service
                  Else!IF cou:courier_type = 'CITY LINK' Or cou:courier_Type = 'LABEL G'
                      job:loaservice  = ''
                      job:excservice  = ''
                      job:jobservice  = ''
                  End!IF cou:courier_type = 'CITY LINK' Or cou:courier_Type = 'LABEL G'
                  ! Inserting (DBH 04/02/2008) # 9612 - Add the Hub information
                  Access:JOBSE2.TryUpdate()
                  ! End (DBH 04/02/2008) #9612
              Else!If error# = 0
                  job:account_number  = ''
                  Select(?job:account_number)
              End!If error# = 0
          Else!If stop# = 0
              Case Missive('The selected trade account is on stop.'&|
                '<13,10>'&|
                '<13,10>Please select another account.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              job:account_number = ''
              Display()
          End!If stop# = 0
          Display()
      
      
      End!If ~0{prop:acceptall}
      Display()
      DO GETJOBSE
      
      Do CheckRequiredFields
    OF ?Lookup_Account_Number
      ThisWindow.Update
      glo:Select1   = ''
      If glo:WebJob = 1
          glo:Select1   = Clarionet:Global.Param2
      Else !If glo:WebJob = 1
          If GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI') <> ''
              glo:Select1 = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
          End !If GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI') <> ''
      End !If glo:WebJob = 1
      
      sub:Account_Number = job:Account_Number
      
      IF SELF.RUN(7,Selectrecord)  = RequestCompleted
          job:Account_Number = sub:Account_Number
          Select(?+1)
      ELSE
          Select(?job:Account_Number)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Account_Number)
      glo:Select1 = ''
      If clip(GLO:Select2) <> ''  then
          job:Account_Number = clip(GLO:Select2)
      End
      
      Display()
    OF ?Button:ExternalDamageChecklist
      ThisWindow.Update
      ExternalDamageChecklist
      ThisWindow.Reset
    OF ?job:Location
!      ! Amend Location Levels
!      If job:location <> location_temp
!          job:location = loi:location
!          If location_temp <> ''
!      !Add To Old Location
!              access:locinter.clearkey(loi:location_key)
!              loi:location = location_temp
!              If access:locinter.fetch(loi:location_key) = Level:Benign
!                  If loi:allocate_spaces = 'YES'
!                      loi:current_spaces+= 1
!                      loi:location_available = 'YES'
!                      access:locinter.update()
!                  End
!              end !if
!          End!If location_temp <> ''
!      !Take From New Location
!          access:locinter.clearkey(loi:location_key)
!          loi:location = job:location
!          If access:locinter.fetch(loi:location_key) = Level:Benign
!              If loi:allocate_spaces = 'YES'
!                  loi:current_spaces -= 1
!                  If loi:current_spaces< 1
!                      loi:current_spaces = 0
!                      loi:location_available = 'NO'
!                  End
!                  access:locinter.update()
!              End
!          end !if
!          location_temp  = job:location
!
!          Display()
!
!
!
!      End!If job:location <> location_temp
!
!
    OF ?tmp:Contract
      IF ?tmp:Contract{Prop:Checked} = True
        tmp:Prepaid = 0
      END
      ThisWindow.Reset
    OF ?tmp:Prepaid
      IF ?tmp:Prepaid{Prop:Checked} = True
        tmp:Contract = 0
      END
      ThisWindow.Reset
    OF ?Amend
      ThisWindow.Update
      Show_Addresses(1)
      ThisWindow.Reset
      Do GetJOBSE
    OF ?Amend:2
      ThisWindow.Update
      Show_Addresses(1)
      ThisWindow.Reset
      Do GetJOBSE
    OF ?Amend:3
      ThisWindow.Update
      Show_Addresses(1)
      ThisWindow.Reset
      Do GetJOBSE
    OF ?Lookup_Accessory
      ThisWindow.Update
      Do SetJOBSE
      If job:Model_Number = ''
          Case Missive('You must enter a model number.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else
          glo:Select1  = job:Model_Number
          glo:Select2  = job:Ref_Number
          If SecurityCheck('JOBS - AMEND ACCESSORIES')
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
          Else!If SecurityCheck('JOBS - AMEND ACCESSORIES')
              Browse_job_accessories
          End!If count# = 0
      
          glo:Select1  = ''
          glo:Select2  = ''
      
          Do Update_Accessories
      End
      
      
      Do GetJOBSE
    OF ?Fault_Codes_Lookup
      ThisWindow.Update
      Do SETJOBSE
      If job:model_Number = ''
          Case Missive('You must select a Model Number.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else!If job:model_Number = ''
          Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
          jbn:RefNumber   = job:Ref_number
          If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
              !Found
      
          Else! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
              access:jobnotes.clearkey(jbn:RefNumberKey)
              jbn:RefNumber   = job:Ref_Number
              If access:jobnotes.tryfetch(jbn:RefNumberKey)
                  If access:jobnotes.primerecord() = Level:Benign
                      jbn:RefNumber   = job:ref_number
                      if PRE:FaultText <> ''
                          jbn:Fault_Description = PRE:FaultText
                      END
      
                      access:jobnotes.tryinsert()
                  End!If access:jobnotes.primerecord() = Level:Benign
              End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      
          End! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
      
          tmp:Original_EmailAddress = jobe:EndUserEmailAddress
          tmp:EndUserTelNo = jobe:EndUserTelNo
      
          Access:JOBSE.Clearkey(jobe:RefNumberKey)
          jobe:RefNumber  = job:Ref_Number
          If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
              !Found
          Else! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
              If Access:JOBSE.Primerecord() = Level:Benign
                  jobe:RefNumber  = job:Ref_Number
                  If Access:JOBSE.Tryinsert()
                      Access:JOBSE.Cancelautoinc()
                  End!If Access:JOBSE.Tryinsert()
              End!If Access:JOBSE.Primerecord() = Level:Benign
          End! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      
          tmp:SaveFaultCode3 = job:Fault_Code3
          tmp:SaveFaultCode7 = job:Fault_Code7
          ! Changing (DBH 23/05/2006) #6733 - Allow for forced chargeable fault codes to show
          ! If job:Chargeable_Job = 'YES'
          !         JobType"    = 'C'
          !         ChargeType"   = job:Charge_Type
          !         RepairType"   = job:Repair_Type
          !     End !If job:Chargeable_Job = 'YES'
          !     If job:Warranty_Job = 'YES'
          !         JobType"    = 'W'
          !         ChargeType"   = job:Warranty_Charge_Type
          !         RepairType"   = job:Repair_Type_Warranty
          !     End !If job:Warranty_Job = 'YES'
          ! to (DBH 23/05/2006) #6733
          ! Repair Type is not set at booking - TrkBs: 6733 (DBH: 23-05-2006)
          If job:Chargeable_Job = 'YES' And job:Warranty_Job <> 'YES'
              JobType" = 'C'
              ChargeType" = job:Charge_Type
              RepairType" = ''
          End ! If job:Chargeable_Job = 'YES' And job:Warranty_Job <> 'YES'
          If job:Chargeable_Job <> 'YES' And job:Warranty_Job = 'YES'
              JobType" = 'W'
              ChargeType" = job:Warranty_Charge_Type
              RepairType" = ''
          End ! If job:Chargeable_Job <> 'YES' And job:Warranty_Job = 'YES'
          If job:Chargeable_Job = 'YES' And job:Warranty_Job = 'YES'
              ! For split jobs use the repair type field for the extra charge type - TrkBs: 6733 (DBH: 23-05-2006)
              JobType" = 'S'
              ChargeType" = job:Charge_Type
              RepairType" = job:Warranty_Charge_Type
          End ! If job:Charegeable_Job = 'YES' And job:Warranty_Job = 'YES'
          ! End (DBH 23/05/2006) #6733
      
          Clear(glo:FaultCodeGroup)
          glo:FaultCode1 = job:Fault_Code1
          glo:FaultCode2 = job:Fault_Code2
          glo:FaultCode3 = job:Fault_Code3
          glo:FaultCode4 = job:Fault_Code4
          glo:FaultCode5 = job:Fault_Code5
          glo:FaultCode6 = job:Fault_Code6
          glo:FaultCode7 = job:Fault_Code7
          glo:FaultCode8 = job:Fault_Code8
          glo:FaultCode9 = job:Fault_Code9
          glo:FaultCode10 = job:Fault_Code10
          glo:FaultCode11 = job:Fault_Code11
          glo:FaultCode12 = job:Fault_Code12
          glo:FaultCode13 = tmp:wobFaultCode13
          glo:FaultCode14 = tmp:wobFaultCode14
          glo:FaultCode15 = tmp:wobFaultCode15
          glo:FaultCode16 = tmp:wobFaultCode16
          glo:FaultCode17 = tmp:wobFaultCode17
          glo:FaultCode18 = tmp:wobFaultCode18
          glo:FaultCode19 = tmp:wobFaultCode19
          glo:FaultCode20 = tmp:wobFaultCode20
          GenericFaultCodes(job:Ref_Number,JobType",ThisWindow.Request,job:account_Number,job:Manufacturer,|
                              ChargeType",RepairType",job:Date_Completed)
          job:Fault_Code1   = glo:FaultCode1
          job:Fault_Code2   = glo:FaultCode2
          job:Fault_Code3   = glo:FaultCode3
          job:Fault_Code4   = glo:FaultCode4
          job:Fault_Code5   = glo:FaultCode5
          job:Fault_Code6   = glo:FaultCode6
          job:Fault_Code7   = glo:FaultCode7
          job:Fault_Code8   = glo:FaultCode8
          job:Fault_Code9   = glo:FaultCode9
          job:Fault_Code10  = glo:FaultCode10
          job:Fault_Code11  = glo:FaultCode11
          job:Fault_Code12  = glo:FaultCode12
          tmp:wobFaultCode13   = glo:FaultCode13
          tmp:wobFaultCode14   = glo:FaultCode14
          tmp:wobFaultCode15   = glo:FaultCode15
          tmp:wobFaultCode16   = glo:FaultCode16
          tmp:wobFaultCode17   = glo:FaultCode17
          tmp:wobFaultCode18   = glo:FaultCode18
          tmp:wobFaultCode19   = glo:FaultCode19
          tmp:wobFaultCode20   = glo:FaultCode20
          Clear(glo:FaultCodeGroup)
      
          jobe:EndUserEmailAddress = tmp:Original_EmailAddress
          jobe:EndUserTelNo = tmp:EndUserTelNo
      
          Access:JOBSE.TryUpdate()
      
          !Do not change anything on an OBF job - L942 (DBH: 15-09-2003)
          If job:Warranty_Job = 'YES' And job:Warranty_Charge_Type = 'WARRANTY (OBF)'
              !Do nothing else -  (DBH: 15-09-2003)
          Else !If job:Warranty_Job = 'YES' And job:Warranty_Charge_Type = 'WARRANTY (OBF)'
              !Warranty Check!
              If job:Manufacturer = 'ALCATEL' And (tmp:SaveFaultCode3 <> job:Fault_Code3)
                IF job:Fault_Code3 <> ''
                  If DateCodeValidation('ALCATEL',job:Fault_Code3,job:Date_Booked)
                    DO Validate_Motorola
      
                  Else !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
                    job:Warranty_Job = 'YES'
                    Post(Event:Accepted,?job:Warranty_Job)
      
                    job:Warranty_Charge_Type = 'WARRANTY (MFTR)'
                    IF job:pop = ''
                      job:pop = 'F'
                    END
                  End !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
                  IF job:pop <> ''
                    UNHIDE(?Validate_POP)
                  ELSE
                    HIDE(?Validate_POP)
                  END
                END
              End !If job:Manufacturer = 'MOTOROLA'
      
              If job:Manufacturer = 'ERICSSON' AND (tmp:SaveFaultCode7 <> job:Fault_Code7)
                IF job:fault_code7 <> ''
                  If DateCodeValidation('ERICSSON',CLIP(CLIP(job:Fault_Code7)&CLIP(job:Fault_Code8)),job:Date_Booked)
                    DO Validate_Motorola
      
                  Else !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
      
                    job:Warranty_Job = 'YES'
                    Post(Event:Accepted,?job:Warranty_Job)
      
                    job:Warranty_Charge_Type = 'WARRANTY (MFTR)'
                    IF job:pop = ''
                      job:pop = 'F'
                    END
                  End !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
                  IF job:pop <> ''
                    UNHIDE(?Validate_POP)
                  ELSE
                    HIDE(?Validate_POP)
                  END
                END
              End !If job:Manufacturer = 'MOTOROLA'
      
              If job:Manufacturer = 'BOSCH' AND (tmp:SaveFaultCode7 <> job:Fault_Code7)
                IF job:fault_code7 <> ''
                  If DateCodeValidation('BOSCH',job:Fault_Code7,job:Date_Booked)
                    DO Validate_Motorola
      
                  Else !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
                    job:Warranty_Job = 'YES'
                    Post(Event:Accepted,?job:Warranty_Job)
      
                    job:Warranty_Charge_Type = 'WARRANTY (MFTR)'
                    IF job:pop = ''
                      job:pop = 'F'
                    END
                  End !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
                  IF job:pop <> ''
                    UNHIDE(?Validate_POP)
                  ELSE
                    HIDE(?Validate_POP)
                  END
                END
              End !If job:Manufacturer = 'MOTOROLA'
      
      
              If job:Manufacturer = 'SIEMENS' AND (tmp:SaveFaultCode3 <> job:Fault_Code3)
                IF job:Fault_Code3 <> ''
                  If DateCodeValidation('SIEMENS',job:Fault_Code3,job:Date_Booked)
                    DO Validate_Motorola
                  Else !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
                    job:Warranty_Job = 'YES'
                    Post(Event:Accepted,?job:Warranty_Job)
      
                    job:Warranty_Charge_Type = 'WARRANTY (MFTR)'
                    IF job:pop = ''
                      job:pop = 'F'
                    END
                  End !If DateCodeValidation('MOTOROLA',job:MSN,job:Date_Booked)
                  IF job:pop <> ''
                    UNHIDE(?Validate_POP)
                  ELSE
                    HIDE(?Validate_POP)
                  END
                End
              End !If job:Manufacturer = 'MOTOROLA'
          End !If job:Warranty_Job = 'YES' And job:Warranty_Charge_Type = 'WARRANTY (OBF)'
      
      End!If job:model_Number = ''
      DO GETJOBSE
    OF ?Fault_Description
      ThisWindow.Update
      GlobalRequest = ChangeRecord
      Rapid_Fault_Description
      ThisWindow.Reset
    OF ?engineers_notes
      ThisWindow.Update
      GlobalRequest = ChangeRecord
      Engineers_Notes
      ThisWindow.Reset
    OF ?Repair_History
      ThisWindow.Update
      If GetTempPathA(255,TempFilePath)
          If Sub(TempFilePath,-1,1) = '\'
              glo:FileName = Clip(TempFilePath) & 'JOBBOUNCER' & Clock() & '.TMP'
          Else !If Sub(TempFilePath,-1,1) = '\'
              glo:FileName = Clip(TempFilePath) & '\JOBBOUNCER' & Clock() & '.TMP'
          End !If Sub(TempFilePath,-1,1) = '\'
      End
      job:Account_Number = BrowseJobBouncers('')
      If job:Account_Number <> ''
          Post(Event:Accepted,?job:Account_Number)
      End !job:Account_Number <> ''
      Remove(glo:FileName)
    OF ?Validate_POP
      ThisWindow.Update
      Do CheckRequiredFields
      do Display_Menu_Option
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Error Checking
  error# = 0
  If ?job:msn{prop:hide} = 0
      access:modelnum.clearkey(mod:model_number_key)
      mod:model_number = job:model_number
      if access:modelnum.fetch(mod:model_number_key) = Level:benign
          If job:esn <> ''
              If Len(Clip(job:esn)) < mod:esn_length_from Or Len(Clip(job:esn)) > mod:esn_length_to
                  Case Missive('The I.M.E.I. number entered should be between ' & Clip(mod:ESN_Length_From) & ' and ' & Clip(mod:ESN_Length_To) & ' characters in length.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  error# = 1
                  Select(?job:ESN)
              End !If Len(job:esn) < mod:esn_length_from Or Len(job:esn) > mod:esn_length_to
              If man:use_msn = 'YES' and job:msn <> '' and job:msn <> 'N/A'
                  If len(Clip(job:msn)) < mod:msn_length_from Or len(Clip(job:msn)) > mod:msn_length_to
                      Case Missive('The M.S.N. entered should be between ' & Clip(mod:MSN_Length_From) & ' and ' & Clip(mod:MSN_Length_To) & ' characters in length.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      error# = 1
                      Select(?job:msn)
                  End!If len(job:msn) < mod:msn_length_from Or len(job:msn) > mod:msn_length_to
              End!If man:use_msn = 'YES'
          End !If job:esn <> ''
      End!if access:modelnum.fetch(mod:model_number_key) = Level:benign
  End!If ?job:msn{prop:hide} = 0
  If error# = 1
      Cycle
  End!If error# = 1

  !Check 48 Booking Option -  (DBH: 14-10-2003)
  If ?tmp:Booking48HourOption{prop:Hide} = 0
      If tmp:Booking48HourOption = 0
          Case Missive('You must select a "Booking Option".','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Cycle
      End !If tmp:Booking48HourOption = 0
  End !?tmp:Booking48HourOption{prop:Hide} = 0
!  ! Inserting (DBH 01/02/2008) # 9612 - Force the courier waybill number
!  If job:Transit_Type = 'ARRIVED FRANCHISE'
!      Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
!      jobe2:RefNumber = job:Ref_Number
!      If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
!          If jobe2:CourierWaybillNumber = ''
!              Case Missive('You must enter a Courier Waybill Number.','ServiceBase 3g',|
!                             'mstop.jpg','/OK')
!                  Of 1 ! OK Button
!              End ! Case Missive
!              Cycle
!          End ! If jobe2:CourierWaybillNumber = ''
!      End ! If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
!  End ! If job:Transit_Type = 'ARRIVED FRANCHISE'
!  ! End (DBH 01/02/2008) #9612
    !Message('Before if request = insert')
    If thiswindow.request = Insertrecord
      !Save Fields
        !message('This is still insertrecord')
        Do SETJOBSE

        !Prime the Second Exchange Status -  (DBH: 05-01-2004)
        jobe:SecondExchangeNumber = 0
        jobe:SecondExchangeStatus = '101 NOT ISSUED'

        !use default courier cost for the subtrader
        access:Subtracc.clearkey(sub:Account_Number_Key)
        sub:Account_Number = job:account_number
        if access:Subtracc.fetch(sub:Account_Number_Key)
            !error - leave this blank
        ELSE
            job:Courier_cost = sub:courier_cost
        END

        If glo:WebJob
            jobe:WebJob = 1
            !message('Should be setting jobe:webjob to 1')
            If jobe:HubRepair
                jobe:HubRepairDate = Today()
                jobe:HubRepairTime  = Clock()
                stanum# = Sub(GETINI('RRC','StatusSendToARC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3)
                GetStatus(stanum#,0,'JOB')
                !change 28/11 28/11 ref:G152/L321 to add update of repair type
                !if there is a Handling Fee repair type use it
                !Do not overwrite it repair type is already filled in.
                access:reptydef.clearkey(rtd:ManRepairTypeKey)
                rtd:Manufacturer = job:manufacturer
                set(rtd:ManRepairTypeKey,rtd:ManRepairTypeKey)
                loop
                   if access:reptydef.next() then break.
                   if rtd:Manufacturer <> job:manufacturer then break.
                   if rtd:BER = 11 then
                       !we have an exchange fee
                       if (job:Chargeable_Job = 'YES' and job:Repair_Type = '') then job:Repair_Type = rtd:Repair_Type.
                       if (job:Warranty_Job = 'YES' And job:Repair_Type_Warranty = '') |
                            then job:Repair_Type_Warranty = rtd:Repair_Type.
                       break
                   end !if ber = 10
                End!loop
            End !If jobe:HubRepair
        Else
            If jobe:HubRepair
                jobe:HubRepairDate = Today()
                jobe:HubRepairTime  = Clock()
            End !If jobe:HubRepair
            ! Inserting (DBH 12/03/2007) # 8718 - Job is already at hub, so put ito obf validation browse
            If tmp:OBF_Flag = 1
                Relate:JOBSOBF.Open()
                Access:JOBSOBF.ClearKey(jof:RefNumberKey)
                jof:RefNumber = job:Ref_Number
                If Access:JOBSOBF.TryFetch(jof:RefNumberKey) = Level:Benign
                    !Found
                    jof:HeadAccountNumber = wob:HeadAccountNumber
                    jof:Status = 1
                    Access:JOBSOBF.Update()
                Else ! If Access:JOBSOBF.TryFetch(jof:RefNumberKey) = Level:Benign
                    !Error
                    If Access:JOBSOBF.PrimeRecord() = Level:Benign
                        jof:RefNumber = job:Ref_Number
                        jof:IMEINumber = job:ESN
                        jof:HeadAccountNumber = wob:HeadAccountNumber
                        jof:HeadAccountNumber = wob:HeadAccountNumber
                        jof:Status = 1
                        If Access:JOBSOBF.TryInsert() = Level:Benign
                            !Insert
                        Else ! If Access:JOBSOBF.TryInsert() = Level:Benign
                            Access:JOBSOBF.CancelAutoInc()
                        End ! If Access:JOBSOBF.TryInsert() = Level:Benign
                    End ! If Access.JOBSOBF.PrimeRecord() = Level:Benign
                End ! If Access:JOBSOBF.TryFetch(jof:RefNumberKey) = Level:Benign
                Relate:JOBSOBF.Close
            End ! If tmp:OBF_Flag = 1
            ! End (DBH 12/03/2007) #8718
        End !If glo:WebJob
        !The ***** don't understand that a blank location change browse
        !means that the unit hasn't moved, so I have to write an initial entry
        !into it.

        LocationChange(job:Location)

        access:JOBSE.TryUpdate()

        jbn:Fault_Description   = Upper(jbn:Fault_Description)
        jbn:Engineers_Notes     = Upper(jbn:Engineers_Notes)
        jbn:Invoice_Text        = Upper(jbn:Invoice_Text)
        jbn:Collection_Text     = Upper(jbn:Collection_Text)
        jbn:Delivery_Text       = Upper(jbn:Delivery_Text)
        jbn:ColContatName       = Upper(jbn:ColContatName)
        jbn:ColDepartment       = Upper(jbn:ColDepartment)
        jbn:DelContactName      = Upper(jbn:DelContactName)
        jbn:DelDepartment       = Upper(jbn:DelDepartment)

        Access:JOBNOTES.Update()  !this doesn't appear to be being saved anywhere else
        cont# = 1

        If NoAccessories(job:Ref_Number)
            Case Missive('You have not entered any accessories for this job.'&|
              '<13,10>'&|
              '<13,10>Do you wish to enter them before you complete booking?','ServiceBase 3g',|
                           'mquest.jpg','\No|/Yes')
                Of 2 ! Yes Button
                    cont# = 0
                    Post(Event:Accepted,?Lookup_Accessory)
                    Cycle
                Of 1 ! No Button
                    tmp:retain = 0
            End ! Case Missive
        End


        If cont# = 1
            ! Inserting (DBH 19/04/2006) #7551 - Make sure the fault codes are written
            Do PutWebJob
            ! End (DBH 19/04/2006) #7551
            error# = 0
            CompulsoryFieldCheck('B')
            If glo:ErrorText <> ''
                glo:errortext = 'You cannot complete booking due to the following error(s): <13,10>' & Clip(glo:errortext)
                Error_Text
                glo:errortext = ''
                Cycle
            End!If glo:ErrorText <> ''

            !End Of Error Checking

            Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
            jobe2:RefNumber = job:Ref_Number
            If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
                ! Found
                jobe2:Contract = tmp:Contract
                jobe2:Prepaid   = tmp:Prepaid
                jobe2:POPConfirmed = locPOPConfirmed  ! #11912 New field (Bryan: 01/02/2011)
                if glo:insert_global = 'PRE'
                    jobe2:IDNumber = PRE:ID_number
                    jobe2:SMSAlertNumber = PRE:Mobile_number
                END ! if prejob booking

                Access:JOBSE2.Update()
            Else ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
                ! Error
            End ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign

            ! Inserting (DBH 11/05/2006) #7597 - Send Email/SMS Alert
            If jobe2:SMSNotification
                !AddEmailSMS(job:Ref_Number,wob:HeadAccountNumber,'BOOK','SMS',jobe2:SMSAlertNumber,'',0,'')
                SendSMSText('J','Y','N')
                !Debug erroring turned off for distruibution
!                if glo:ErrorText[1:5] = 'ERROR' then
!                    miss# = missive('Unable to send SMS as '&glo:ErrorText[ 6 : len(clip(Glo:ErrorText)) ],'Service Base 3g','mstop.jpg','OK' )
!                ELSE
!                    miss# = missive('An SMS has been sent to '&clip(job:Initial)&' '&clip(job:Surname),'Service Base 3g','mwarn.jpg','OK' )
!                END
                glo:ErrorText = ''

            End ! If jobe2:SMSNotification

            If jobe2:EmailNotification
                AddEmailSMS(job:Ref_Number,wob:HeadAccountNumber,'BOOK','EMAIL','',jobe2:EmailAlertAddress,0,'')
            End ! If jobe2:EmailNotification
            ! End (DBH 11/05/2006) #7597

            tmp:AuditNotes = ''
            tmp:AuditNotes         = 'UNIT DETAILS: ' & Clip(job:manufacturer) & ' ' & Clip(job:model_number) & ' - ' & Clip(job:unit_type) & |
                                    '<13,10>I.M.E.I.: ' & Clip(job:esn) & |
                                    '<13,10>M.S.N.: ' & Clip(job:msn)
            If job:Chargeable_job = 'YES'
                tmp:AuditNotes = Clip(tmp:AuditNotes) & '<13,10>CHA. CHARGE TYPE: ' & Clip(job:Charge_Type)
            End !If job:Chargeable_job = 'YES'
            If job:Warranty_Job = 'YES'
                tmp:AuditNotes = Clip(tmp:AuditNotes) & '<13,10>WAR. CHARGE TYPE: ' & Clip(job:Warranty_Charge_Type)
            End !If job:Warranty_Job = 'YES'

            tmp:AuditNotes = Clip(tmp:AuditNotes) & '<13,10>BOOKING OPTION: '
            Case tmp:Booking48HourOption
                Of 1
                    tmp:AuditNotes = Clip(tmp:AuditNotes) & ' 48 HOUR EXCHANGE'
                Of 2
                    tmp:AuditNotes = Clip(tmp:AuditNotes) & ' ARC REPAIR'
                Of 3
                    tmp:AuditNotes = Clip(tmp:AuditNotes) & ' 7 DAY TAT'
            End !Case tmp:Booking48HourOption

            ! Inserting (DBH 25/04/2006) #7419 - Insert Contract Type in audit trail
            If jobe2:Contract = 1
                tmp:AuditNotes = Clip(tmp:AuditNotes) & '<13,10>CONTRACT'
            Else ! If jobe2:Contract = 1
                If jobe2:Prepaid = 1
                    tmp:AuditNotes = Clip(tmp:AuditNotes) & '<13,10>PREPAID'
                End ! If jobe2:Prepaid = 1
            End ! If jobe2:Contract = 1
            ! End (DBH 25/04/2006) #7419

            ! Inserting (DBH 10/04/2006) #7305 - Display the ID Number in the Audit Trail
            If Clip(jobe2:IDNumber) <> ''
                tmp:AuditNotes = Clip(tmp:AuditNotes) & '<13,10>ID NO: ' & Clip(jobe2:IDNumber)
            End !If Clip(jobe2:IDNumber) <> ''
            ! End (DBH 10/04/2006) #7305


            count# = 0
            save_jac_id = access:jobacc.savefile()
            access:jobacc.clearkey(jac:ref_number_key)
            jac:ref_number = job:ref_number
            set(jac:ref_number_key,jac:ref_number_key)
            loop
                if access:jobacc.next()
                   break
                end !if
                if jac:ref_number <> job:ref_number      |
                    then break.  ! end if
                count# += 1
                tmp:AuditNotes = Clip(tmp:AuditNotes) & '<13,10>ACC. ' & Clip(count#) & ': ' & Clip(jac:accessory)
            end !loop
            access:jobacc.restorefile(save_jac_id)

            If AddToAudit(job:Ref_Number,'JOB','RAPID NEW JOB BOOKING INITIAL ENTRY',Clip(tmp:AuditNotes))

            End ! If AddToAudit(job:Ref_Number,'JOB','RAPID NEW JOB BOOKING INITIAL ENTRY',Clip(tmp:AuditNotes))

            If (jobe2:POPConfirmed) ! #11912 New field (Bryan: 01/02/2011)
                IF (AddToAudit(job:Ref_Number,'JOB','POP CONFIRMED',''))
                END
            END

            If jobe:VSACustomer
                CID_XML(job:Mobile_Number,tmp:HeadAccountNumber,1)
            End ! If jobe:VSACustomer

            If tmp:IMEIValidationStatus = True
                ! Add an audit entry if the imei failed the validation - TrkBs: 6690 (DBH: 11-11-2005)
                If AddToAudit(job:Ref_Number,'JOB','IMEI VALIDATION FAILED','AN ERROR OCCURED DURING THE AUTOMATIC VALIDATION PROCESS. PLEASE VALIDATE THIS I.M.E.I. NUMBER MANUALLY')

                End ! If AddToAudit(job:Ref_Number,'JOB','IMEI VALIDATION FAILED','AN ERROR OCCURED DURING THE AUTOMATIC VALIDATION PROCESS. PLEASE VALIDATE THIS I.M.E.I. NUMBER MANUALLY')
            End ! If tmp:IMEIValidationStatus = True

            ! Inserting (DBH 07/11/2007) # 9200 - Add audit entry for MSISDN check
            If tmp:MobileValidated
                If AddToAudit(job:Ref_Number,'JOB','MSISDN VALIDATION','ACTIVATION DATE: ' & Format(tmp:MobileActivationDate,@d6) & |
                            '<13,10>PHONE NAME: ' & Clip(tmp:MobilePhoneName) & |
                            '<13,10>LOYALTY STATUS: ' & Clip(tmp:MobileLoyaltyStatus) & |
                            '<13,10>LIFETIME VALUE: ' & Clip(tmp:MobileLifeTimeValue) & |
                            '<13,10>AVE SPEND: ' & tmp:MobileAverageSpend & |
                            '<13,10>UPGRADE DATE: ' & Format(tmp:MobileUpgradeDate,@d06b) & |
                            '<13,10>ID NO: ' & Clip(tmp:MobileIDNumber))

                End ! If Add To Audit
            End ! If tmp:MobileValidated
            ! End (DBH 07/11/2007) #9200



            if job:warranty_job = 'YES'
                job:edi = Set_Edi(job:manufacturer)
            End!if job:warranty_job = 'YES'

            If job:chargeable_job = 'YES'
                labour" = 0
                parts"  = 0
                pass"   = False
                Pricing_Routine('C',labour",parts",pass",discount",handling",Exchange",RRCRate",RRCParts")
                If pass" = true
                    job:labour_cost = labour"
                    job:parts_cost  = parts"
                    Access:JOBSE.Clearkey(jobe:RefNumberKey)
                    jobe:RefNumber  = job:Ref_Number
                    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                        !Found
                        If ~jobe:IgnoreRRCChaCosts
                            jobe:RRCCLabourCost = RRCRate"
                            jobe:RRCCPartsCost  = RRCParts"
                            jobe:RRCCSubTotal   = jobe:RRCCLabourCost + jobe:RRCCPartsCost + job:Courier_Cost
                        End !If ~jobe:IgnoreRRCChaCosts
                        jobe:HandlingFee = handling"
                    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                        !Error
                    End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                Else!If pass" = true
                    job:labour_cost = 0
                    job:parts_cost  = 0
                End!If pass" = true

                !Is this job an estimate, if so change the status
                ! Bit of a bodge, but don't change status at booking
                ! if it's a replicate job from QA Fail (DBH: 01/02/2010) #11255
                if (glo:select30 = 'REPLICATE' and glo:select32 = 'COPY')
                else ! if (glo:select30 = 'REPLICATE' and glo:select32 = 'COPY')
                    If job:Estimate = 'YES'
                        GetStatus(505,1,'JOB')
                    End !If job:Estimate = 'YES'
                end !if (glo:select30 = 'REPLICATE' and glo:select32 = 'COPY')
            End!If job:chargeable_job = 'YES'

            If job:warranty_job = 'YES'
                labour" = 0
                parts"  = 0
                pass"   = False
                Pricing_Routine('W',labour",parts",pass",claim",handling",Exchange",RRCRate",RRCParts")
                Access:JOBSE.Clearkey(jobe:RefNumberKey)
                jobe:RefNumber  = job:Ref_Number
                If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                    !Found
                    !Just get it again, to make sure
                Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                    !Error
                End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                If pass" = true
                    job:labour_cost_warranty    = labour"
                    job:parts_cost_warranty     = parts"
                    jobe:HandlingFee            = handling"
                    If job:Exchange_Unit_Number <> 0 and ~jobe:ExchangedAtRRC
                        jobe:ExchangeRate   = Exchange"
                    Else !If job:Exchange_Unit_Number <> 0 and ~jobe:ExchangedAtRRC
                        jobe:ExchangeRate   = 0
                    End !If job:Exchange_Unit_Number <> 0 and ~jobe:ExchangedAtRRC
                    jobe:ClaimValue = claim"
                    Access:JOBSE.Clearkey(jobe:RefNumberKey)
                    jobe:RefNumber  = job:Ref_Number
                    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                        !Found
                        If ~jobe:IgnoreRRCWarCosts
                            jobe:RRCWLabourCost = RRCRate"
                            jobe:RRCWPartsCost  = RRCParts"
                            jobe:RRCWSubTotal   = jobe:RRCWLabourCost + jobe:RRCWPartsCost + job:Courier_Cost_Warranty
                        End !If ~jobe:IgnoreRRCChaCosts
                    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                        !Error
                    End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                Else!If pass" = true
                    job:labour_cost_warranty    = 0
                    job:parts_cost_warranty     = 0
                    jobe:ClaimValue = 0
                    jobe:HandlingFee = 0
                End!If pass" = true
                Access:JOBSE.TryUpdate()
            End!If job:warranty_job = 'YES'


            set(Defrapid)
            access:defrapid.next()

            If print_label_temp = 'YES'
                Set(defaults)
                access:defaults.next()
                Case def:label_printer_type
                    Of 'TEC B-440 / B-442'
                        Label_Type_temp = 1
                    Of 'TEC B-452'
                        label_type_temp = 2
                End!Case def:themal_printer_type
            End!If print_label_temp = 'YES'

            sav:ref_number   = job:ref_number
            sav:esn          = job:esn
        End!If cont# = 1
    End!If thiswindow.request = Insertrecord
  !POP Status
  If job:POP = 'REJ'
      Access:MANUFACT.Clearkey(man:Manufacturer_Key)
      man:Manufacturer    = job:Manufacturer
      If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
          !Found
          If man:ForceStatus
              GetStatus(Sub(man:StatusRequired,1,3),1,'JOB')
          End !If man:ForceStatus
      Else! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
  
  End !job:POP = 'REJ'
  ?tmp:Print_JobCard{prop:Hide} = 1
  Do CreateWebJob
  do AllocationsReport
  !In case they set up the engineer on the job booking
  !changed By Paul 29/09/2009 - Log no 10785
  If Glo:Select32 <> 'COPY' then
      do Waybill:Check
  End
  !end change
  ! Inserting (DBH 14/07/2006) # 7975 - Determine what reports are going to print on close
  Free(glo:Queue)
  Clear(glo:Queue)
  If ThisWindow.Request = InsertRecord
      Set(DEFAULTS)
      Access:DEFAULTS.Next()
      If Print_Label_Temp = 'YES' And def:Use_Job_Label = 'YES'
          glo:Pointer = 'THERMAL LABEL'
          Add(glo:Queue)
      End ! If Print_Label_Temp = 'YES' And def:Use_Job_Label = 'YES'
  
      If Print_Job_Card_Temp = 'YES'
          glo:Pointer = 'JOB CARD'
          Add(glo:Queue)
      End ! If Print_Job_Card_Temp = 'YES'
  
      If tmp:Print_JobCard = True And ?tmp:Print_JobCard{prop:Hide} = 0
          glo:Pointer = 'JOB CARD'
          Add(glo:Queue)
      End ! If tmp:Print_JobCard = True And ?tmp:Print_JobCard{prop:Hide} = 0
  
      If tmp:Retain = 1 And def:Use_Job_Label = 'YES'
          glo:Pointer = 'RETAINED ACCESSORIES LABEL'
          Add(glo:Queue)
      End ! If tmp:Retail = 1 And def:Use_Job_Label = 'YES'
  
      If tmp:PrintReceipt = True
          glo:Pointer = 'RECEIPT'
          Add(glo:Queue)
      End ! If tmp:PrintReceipt = True
  End ! If tmp:OKPressed And ThisWindow.Request = InsertRecord
  ! End (DBH 14/07/2006) #7975
  ReturnValue = PARENT.TakeCompleted()
  ! Do not delete WEBJOBS as the window closes - TrkBs: 5904 (DBH: 26-09-2005)
  tmp:OKPressed = 1
  
  !update the prejob ref if this was a pre
  if glo:insert_global = 'PRE'
      PRE:JobRef_number = job:Ref_Number
      Access:PreJob.update()
  END !glo:insert_global = 'PRE'
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?OptionsEstimate
    GLO:Select1 = 'INSERT'
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?job:Transit_Type
      Do showtitle
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber  = job:Ref_Number
      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          !Found
      Else! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
          If Access:JOBSE.Primerecord() = Level:Benign
              jobe:RefNumber  = job:Ref_Number
              If Access:JOBSE.Tryinsert()
                  Access:JOBSE.Cancelautoinc()
              End!If Access:.Tryinsert()
          End!If Access:JOBSE.Primerecord() = Level:Benign
      End! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
    OF ?job:ESN
      Do showtitle
      Display()
    END
  ReturnValue = PARENT.TakeSelected()
    CASE FIELD()
    OF ?Amend
      IF job:ESN <> ''
        POST(Event:Accepted,?Amend)
      END
    OF ?Amend:2
            IF job:ESN <> ''
        POST(Event:Accepted,?Amend:2)
      END
    OF ?Amend:3
            IF job:ESN <> ''
        POST(Event:Accepted,?Amend:3)
      END
    OF ?Lookup_Accessory
      Do SetJOBSE
      !CHECK FOR NO ACCESSORIES
      !This envelope for webjob avoids an early popup
      !It now willnot check if the job IMEI is blank
      
      IF GLO:WEBJOB  THEN         !was if glo:webjob and job@mwn=''
          !IGNORE THIS CALL
      else
          If NoAccessories(job:Ref_Number)
              If ForceAccessories('B')
                  If SecurityCheck('JOBS - AMEND ACCESSORIES')
                      Case Missive('You do not have access to this option.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
      
                  Else !If SecurityCheck('JOBS - AMEND ACCESSORIES')
                      glo:Select1  = job:Model_Number
                      glo:Select2  = job:Ref_Number
                      Browse_job_accessories
      
                      glo:Select1  = ''
                      glo:Select2  = ''
                  End !If SecurityCheck('JOBS - AMEND ACCESSORIES')
                  Do Update_Accessories
              End !If ForceAccessories('B')
      
          End !End !If ForceAccessories('B')
      
          If ForceFaultCodes(job:Chargeable_Job,job:Warranty_job,|
                              job:Charge_Type,job:Warranty_Charge_Type,|
                              job:Repair_Type,job:Repair_Type_Warranty,'X')
              If NoFaultCodes('B')
                  Post(Event:Accepted,?Fault_Codes_Lookup)
              End !If NoFaultCodes('B')
          End !If ForceFaultCodes(job:Chargeable_Job,job:Warranty_job,|
      
          If ForceFaultDescription('B')
              If jbn:Fault_Description = ''
                  !message('at posting ')
                  Post(Event:Accepted,?Fault_Description)
              End !If jobe:Fault_Description = ''
          End !If ForceFaultDescription('B')
      
      END !IF GLO:WEBJOB AND JOB:esn = ''
      Do SetJOBSE
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      Case Keycode()
          Of F3Key
              If ?Repair_History{prop:Hide} = 0
                  Post(Event:Accepted,?Repair_History)
              End !If ?Repair_History{prop:Hide} = 0
          Of F4Key
              Post(Event:Accepted,?Lookup_Accessory)
          Of F5Key
              Post(Event:Accepted,?Fault_Codes_Lookup)
          Of F6Key
              Post(Event:Accepted,?Fault_Description)
          Of F7Key
              Post(Event:Accepted,?Engineers_Notes)
          Of F9Key
              Case Choice(?Sheet2)
                  Of 1
                      Post(Event:Accepted,?Amend)
                  Of 2
                      Post(Event:Accepted,?Amend:2)
                  Of 3
                      Post(Event:Accepted,?Amend:3)
              End !Case Choice(?Sheet2)
          Of F10Key
              Post(Event:Accepted,?OK)
      End !Keycode()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

local.CopyDOPFromBouncer        Procedure(Date f:PreviousDOP)
local:Found     Byte()
local:NewDOP        Date()
local:ChangeReason  String(255)
Code
    CopyDOPFromBouncer(job:Manufacturer,f:PreviousDOP,job:DOP,job:ESN,job:Bouncer,local:Found)

    if (local:Found)
        ?job:DOP{prop:Disable} = 1
        Beep(Beep:SystemExclamation)  ;  Yield()
        Case Missive('This I.M.E.I. has been entered onto the system before. '&|
            '|'&|
            '|The Date Of Purchase will be filled automatically from the previous job. To enter a new date selected Override.','ServiceBase',|
                       'mexclam.jpg','\&Override|/&Accept')
        Of 2 ! &Accept Button
            ! Don't allow to change the DOP. Should be retrieved from the previous job (DBH: 25/02/2009) #10591
        Of 1 ! &Override Button
            save_USERS_ID = Access:USERS.SaveFile()
            Access:USERS.Clearkey(use:User_Code_Key)
            use:User_Code    = InsertEngineerPassword()
            if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
                ! Found
                save_ACCAREAS_ID = Access:ACCAREAS.SaveFile()
                Access:ACCAREAS.Clearkey(acc:Access_Level_Key)
                acc:User_Level    = use:User_Level
                acc:Access_Area    = 'JOB - DOP OVERRIDE'
                if (Access:ACCAREAS.TryFetch(acc:Access_Level_Key) = Level:Benign)
                    ! Found

                    ChangeDOP(job:DOP,local:NewDOP,local:ChangeReason)

                    if (clip(local:ChangeReason) <> '' and local:NewDOP <> 0)
                        x# = AddToAudit(job:Ref_Number,'JOB','DOP OVERRIDE','PREV DOP: ' & format(job:DOP,@d06) & |
                                                                            '<13,10>NEW DOP: ' & format(local:NewDOP,@d06) & |
                                                                            '<13,10>REASON: ' & clip(local:ChangeReason))
                        job:DOP = local:NewDOP
                    end ! if (clip(local:ChangeReason) <> '' and local:NewDOP <> 0)
                else ! if (Access:ACCAREAS.TryFetch(acc:Access_Level_Key) = Level:Benign)
                    ! Error
                    Case Missive('You do not have sufficient access to amend this option.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                        Of 1 ! OK Button
                    End ! Case Missive
                    ! Don't allow to change the DOP. Should be retrieved from the previous job (DBH: 25/02/2009) #10591
                    !?job:DOP{prop:Disable} = 1
                end ! if (Access:ACCAREAS.TryFetch(acc:Access_Level_Key) = Level:Benign)
                Access:ACCAREAS.RestoreFile(save_ACCAREAS_ID)
            else ! if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
                ! Error
            end ! if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
        End!Case missiuve
    end ! if (local:Found)
Local.ValidateIMEINumber  Procedure(Long func:Bouncers)
Code
    If def:Allow_Bouncer <> 'YES'
        If func:Bouncers
            Case Missive('The selected I.M.E.I. number has been entered previously within the default period.'&|
              '<13,10>'&|
              '<13,10>Please re-book this job and select another I.M.E.I. number.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            Return 1
        End !If Bouncers#
    Else !If def:Allow_Bouncer <> 'YES'
        If Clip(GETINI('BOUNCER','StopLive',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
            If LiveBouncers(job:Ref_Number,job:Date_Booked,job:ESN)
                Case Missive('The selected I.M.E.I. already exists on a live job.'&|
                  '<13,10>Please check you have entered the correct I.M.E.I. number of this new booking.'&|
                  '<13,10>'&|
                  '<13,10>If this I.M.E.I. number is correct then the previous job should be completed.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                Return 1
            End !If LiveBouncer(job:Ref_Number,job:Date_Booked,job:ESN)
        End !If Clip(GETINI('BOUNCER','StopLive',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
    End !If def:Allow_Bouncer <> 'YES'
ExternalDamageChecklist PROCEDURE                     !Generated from procedure template - Window

window               WINDOW('External Damage Checklist'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       GROUP,AT(212,132,85,145),USE(?Group:Boxes)
                         CHECK('Antenna'),AT(212,132),USE(jobe2:XAntenna),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Antenna'),TIP('Antenna'),VALUE('1','0')
                         CHECK('Lens'),AT(212,148),USE(jobe2:XLens),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Lens'),TIP('Lens'),VALUE('1','0')
                         CHECK('F/Cover'),AT(212,162),USE(jobe2:XFCover),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('F/Cover'),TIP('F/Cover'),VALUE('1','0')
                         CHECK('B/Cover'),AT(212,177),USE(jobe2:XBCover),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('B/Cover'),TIP('B/Cover'),VALUE('1','0')
                         CHECK('Keypad'),AT(212,193),USE(jobe2:XKeypad),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Keypad'),TIP('Keypad'),VALUE('1','0')
                         CHECK('Battery'),AT(212,207),USE(jobe2:XBattery),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Battery'),TIP('Battery'),VALUE('1','0')
                         CHECK('Charger'),AT(212,223),USE(jobe2:XCharger),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Charger'),TIP('Charger'),VALUE('1','0')
                         CHECK('LCD'),AT(212,238),USE(jobe2:XLCD),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('LCD'),TIP('LCD'),VALUE('1','0')
                         CHECK('Sim Reader'),AT(212,252),USE(jobe2:XSimReader),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Sim Reader'),TIP('Sim Reader'),VALUE('1','0')
                         CHECK('System Connector'),AT(212,268),USE(jobe2:XSystemConnector),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('System Connector'),TIP('System Connector'),VALUE('1','0')
                       END
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(164,82,352,248),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('Please select the areas with signs of external damage:'),AT(236,118),USE(?Prompt3),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       CHECK('None'),AT(212,284),USE(jobe2:XNone),FONT('Tahoma',8,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('None'),TIP('None'),VALUE('1','0')
                       PROMPT('Notes'),AT(324,176),USE(?jobe2:XNotes:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       TEXT,AT(376,176,125,49),USE(jobe2:XNotes),VSCROLL,LEFT,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),MSG('Notes'),TIP('Notes'),UPR
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('External Damage Checklist'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(372,332),USE(?Close),TRN,FLAT,ICON('okp.jpg')
                       BUTTON,AT(444,332),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020644'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ExternalDamageChecklist')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?jobe2:XAntenna
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:JOBSE2.Open
  SELF.FilesOpened = True
  Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
  jobe2:RefNumber = job:Ref_Number
  If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
      ! Found
  
  Else ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
      ! Error
  End ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  !Post(Event:Accepted,?jobe2:XNone)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?jobe2:XNone{Prop:Checked} = True
    DISABLE(?Group:Boxes)
  END
  IF ?jobe2:XNone{Prop:Checked} = False
    ENABLE(?Group:Boxes)
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBSE2.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Close
      Access:JOBSE2.Update()
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?jobe2:XNone
      IF ?jobe2:XNone{Prop:Checked} = True
        DISABLE(?Group:Boxes)
      END
      IF ?jobe2:XNone{Prop:Checked} = False
        ENABLE(?Group:Boxes)
      END
      ThisWindow.Reset
      If ~0{prop:AcceptAll}
          If jobe2:XNone = 1
              jobe2:XAntenna      = 0
              jobe2:XLens         = 0
              jobe2:XFCover       = 0
              jobe2:XBCover       = 0
              jobe2:XKeypad       = 0
              jobe2:XBattery      = 0
              jobe2:XCharger      = 0
              jobe2:XLCD          = 0
              jobe2:XSimReader    = 0
              jobe2:XSystemConnector  = 0
              Display()
          End ! If jobe2:XNone = 1
      End ! If ~0{prop:AcceptAll}e
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020644'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020644'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020644'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
