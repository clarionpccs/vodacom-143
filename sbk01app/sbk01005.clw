

   MEMBER('sbk01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABEIP.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABQuery.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Xplore.INC'),ONCE

                     MAP
                       INCLUDE('SBK01005.INC'),ONCE        !Local module procedure declarations
                     END


IMEISearch PROCEDURE (func:IMEI)                      !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(ADDSEARCH)
                       PROJECT(addtmp:JobNumber)
                       PROJECT(addtmp:Surname)
                       PROJECT(addtmp:AddressLine1)
                       PROJECT(addtmp:AddressLine2)
                       PROJECT(addtmp:Postcode)
                       PROJECT(addtmp:RecordNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
addtmp:JobNumber       LIKE(addtmp:JobNumber)         !List box control field - type derived from field
addtmp:Surname         LIKE(addtmp:Surname)           !List box control field - type derived from field
addtmp:AddressLine1    LIKE(addtmp:AddressLine1)      !List box control field - type derived from field
addtmp:AddressLine2    LIKE(addtmp:AddressLine2)      !List box control field - type derived from field
addtmp:Postcode        LIKE(addtmp:Postcode)          !List box control field - type derived from field
addtmp:RecordNumber    LIKE(addtmp:RecordNumber)      !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('I.M.E.I. Number Search'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('I.M.E.I. Number Search'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(64,54,552,20),USE(?Panel1),FILL(09A6A7CH)
                       PROMPT('I.M.E.I. Number'),AT(68,58),USE(?Prompt1),FONT(,12,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                       STRING(@s30),AT(160,58),USE(func:IMEI),FONT(,12,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                       LIST,AT(108,108,432,246),USE(?Browse:1),IMM,HVSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(EnterKey),ALRT(MouseLeft2),FORMAT('37L(2)|M~Job No~@s8@86L(2)|M~Surname~@s30@120L(2)|M~Address Line 1~@s30@103L(2)|' &|
   'M~Address Line 2~@s30@120L(2)|M~Postcode~@s30@'),FROM(Queue:Browse:1)
                       BUTTON,AT(548,238),USE(?Edit),TRN,FLAT,LEFT,ICON('editp.jpg')
                       SHEET,AT(64,78,552,282),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Job Number'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(108,94,64,10),USE(addtmp:JobNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Address Line 1'),TIP('Address Line 1'),UPR
                         END
                       END
                       BUTTON,AT(548,366),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepLongClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020532'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('IMEISearch')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ADDSEARCH.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:ADDSEARCH,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,addtmp:JobNumberKey)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?addtmp:JobNumber,addtmp:JobNumber,1,BRW1)
  BRW1.AddField(addtmp:JobNumber,BRW1.Q.addtmp:JobNumber)
  BRW1.AddField(addtmp:Surname,BRW1.Q.addtmp:Surname)
  BRW1.AddField(addtmp:AddressLine1,BRW1.Q.addtmp:AddressLine1)
  BRW1.AddField(addtmp:AddressLine2,BRW1.Q.addtmp:AddressLine2)
  BRW1.AddField(addtmp:Postcode,BRW1.Q.addtmp:Postcode)
  BRW1.AddField(addtmp:RecordNumber,BRW1.Q.addtmp:RecordNumber)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ADDSEARCH.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020532'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020532'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020532'&'0')
      ***
    OF ?Edit
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Edit, Accepted)
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number  = brw1.q.addtmp:JobNumber
      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Found
          GlobalRequest = ChangeRecord
          Update_Jobs_Rapid
      Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Edit, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
      Case KeyCode()
          Of MouseLeft2 Orof EnterKey
              Post(Event:Accepted,?Edit)
      End !KeyCode()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?addtmp:JobNumber
      Select(?Browse:1)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

OrderSearch PROCEDURE (func:IMEI)                     !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(ADDSEARCH)
                       PROJECT(addtmp:JobNumber)
                       PROJECT(addtmp:Surname)
                       PROJECT(addtmp:AddressLine1)
                       PROJECT(addtmp:AddressLine2)
                       PROJECT(addtmp:Postcode)
                       PROJECT(addtmp:RecordNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
addtmp:JobNumber       LIKE(addtmp:JobNumber)         !List box control field - type derived from field
addtmp:Surname         LIKE(addtmp:Surname)           !List box control field - type derived from field
addtmp:AddressLine1    LIKE(addtmp:AddressLine1)      !List box control field - type derived from field
addtmp:AddressLine2    LIKE(addtmp:AddressLine2)      !List box control field - type derived from field
addtmp:Postcode        LIKE(addtmp:Postcode)          !List box control field - type derived from field
addtmp:RecordNumber    LIKE(addtmp:RecordNumber)      !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Order Number Search'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Order Number Search'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(64,54,552,20),USE(?Panel1),FILL(09A6A7CH)
                       PROMPT('Order Number'),AT(68,58),USE(?Prompt1),FONT(,12,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                       STRING(@s30),AT(152,58),USE(func:IMEI),FONT(,12,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                       LIST,AT(104,108,432,252),USE(?Browse:1),IMM,HVSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(EnterKey),ALRT(MouseLeft2),FORMAT('37L(2)|M~Job No~@s8@86L(2)|M~Surname~@s30@120L(2)|M~Address Line 1~@s30@103L(2)|' &|
   'M~Address Line 2~@s30@120L(2)|M~Postcode~@s30@'),FROM(Queue:Browse:1)
                       BUTTON,AT(548,240),USE(?Edit),TRN,FLAT,LEFT,ICON('editp.jpg')
                       SHEET,AT(64,78,552,286),USE(?CurrentTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('By Job Number'),USE(?Tab:2)
                           ENTRY(@s8),AT(104,94,64,10),USE(addtmp:JobNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Address Line 1'),TIP('Address Line 1'),UPR
                         END
                       END
                       BUTTON,AT(548,366),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepLongClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020533'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('OrderSearch')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ADDSEARCH.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:ADDSEARCH,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,addtmp:JobNumberKey)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?addtmp:JobNumber,addtmp:JobNumber,1,BRW1)
  BRW1.AddField(addtmp:JobNumber,BRW1.Q.addtmp:JobNumber)
  BRW1.AddField(addtmp:Surname,BRW1.Q.addtmp:Surname)
  BRW1.AddField(addtmp:AddressLine1,BRW1.Q.addtmp:AddressLine1)
  BRW1.AddField(addtmp:AddressLine2,BRW1.Q.addtmp:AddressLine2)
  BRW1.AddField(addtmp:Postcode,BRW1.Q.addtmp:Postcode)
  BRW1.AddField(addtmp:RecordNumber,BRW1.Q.addtmp:RecordNumber)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ADDSEARCH.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020533'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020533'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020533'&'0')
      ***
    OF ?Edit
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Edit, Accepted)
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number  = brw1.q.addtmp:JobNumber
      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Found
          GlobalRequest = ChangeRecord
          UpdateJOBS
      Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Edit, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
      Case KeyCode()
          Of MouseLeft2 Orof EnterKey
              Post(Event:Accepted,?Edit)
      End !KeyCode()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?addtmp:JobNumber
      Select(?Browse:1)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

CancelReason PROCEDURE                                !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Close            BYTE(0)
tmp:CancelReason     STRING(255)
window               WINDOW('Cancel Reason'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Cancellation Reason'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,84,352,244),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Please enter your reason for cancelling this job'),AT(240,159),USE(?Prompt1),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(240,173,200,72),USE(tmp:CancelReason),VSCROLL,LEFT,FONT(,,,FONT:bold),COLOR(COLOR:White),MSG('Cancel Reason'),TIP('Cancel Reason'),REQ,UPR
                         END
                       END
                       BUTTON,AT(448,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:CancelReason)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020556'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('CancelReason')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020556'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020556'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020556'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      If tmp:CancelReason = ''
          Select(?tmp:CancelReason)
      Else !tmp:CancelReason = ''
          tmp:Close = 1
          Post(Event:CloseWindow)
      End !tmp:CancelReason = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
      If ~tmp:Close
          Cycle
      End !tmp:Cancel
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Strip                PROCEDURE  (in_string)           ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    in_string = CLIP(LEFT(in_string))

    STR_LEN#  = LEN(in_string)
    STR_POS#  = 1

    in_string = UPPER(SUB(in_string,STR_POS#,1)) & SUB(in_string,STR_POS#+1,STR_LEN#-1)

    LOOP STR_POS# = 1 TO STR_LEN#

     IF SUB(in_string,STR_POS#,1) = ','
        in_string = SUB(in_string,1,STR_POS#-1) & ' ' & SUB(in_string,STR_POS#+1,STR_LEN#-1)
     .
     IF VAL(SUB(in_string,STR_POS#,1)) < 32 Or VAL(SUB(in_string,STR_POS#,1)) > 126
        in_string = SUB(in_string,1,STR_POS#-1) & ' ' & SUB(in_string,STR_POS#+1,STR_LEN#-1)
     .
    .

    RETURN(in_string)
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
AmendClaimValue PROCEDURE (func:JobNumber)            !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
save_wpr_id          USHORT,AUTO
tmp:Claimed          REAL
tmp:ClaimLabour      REAL
tmp:ClaimParts       REAL
tmp:ClaimSubTotal    REAL
tmp:ClaimVAT         REAL
tmp:ClaimTotal       REAL
tmp:AdjLabour        REAL
tmp:AdjParts         REAL
tmp:AdjSubTotal      REAL
tmp:AdjVAT           REAL
tmp:AdjTotal         REAL
tmp:JobNumber        LONG
tmp:ConfirmClaimAdjustment BYTE(0)
tmp:ClaimExchange    REAL
tmp:AdjExchange      REAL
BRW5::View:Browse    VIEW(WARPARTS)
                       PROJECT(wpr:Part_Number)
                       PROJECT(wpr:Description)
                       PROJECT(wpr:Quantity)
                       PROJECT(wpr:CostAdjustment)
                       PROJECT(wpr:Record_Number)
                       PROJECT(wpr:Ref_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
wpr:Part_Number        LIKE(wpr:Part_Number)          !List box control field - type derived from field
wpr:Description        LIKE(wpr:Description)          !List box control field - type derived from field
wpr:Quantity           LIKE(wpr:Quantity)             !List box control field - type derived from field
tmp:Claimed            LIKE(tmp:Claimed)              !List box control field - type derived from local data
wpr:CostAdjustment     LIKE(wpr:CostAdjustment)       !List box control field - type derived from field
wpr:Record_Number      LIKE(wpr:Record_Number)        !Primary key field - type derived from field
wpr:Ref_Number         LIKE(wpr:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Amend Job Claim Value'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(60,38,560,360),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Amend Job Claim Value'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,56,552,194),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Warranty Parts'),USE(?WarrantyPartsTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(144,76,392,168),USE(?List),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('120L(2)|M~Part Number~@s30@120L(2)|M~Description~@s30@32D(2)|M~Quantity~L@n8@56R' &|
   '(2)|M~Claimed~L@n14.2@56R(2)|M~Paid~L@n14.2@'),FROM(Queue:Browse)
                           BUTTON('&Change'),AT(244,168,42,12),USE(?Change),HIDE
                         END
                       END
                       SHEET,AT(64,254,552,108),USE(?Sheet2),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Claim Details'),USE(?ClaimDetailsTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Claim Made'),AT(306,258),USE(?Prompt6),FONT(,10,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Labour'),AT(240,270),USE(?Prompt1),FONT(,8,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           STRING(@n14.2),AT(304,270),USE(tmp:ClaimLabour),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(412,270,48,10),USE(tmp:AdjLabour),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Adjustment Labour'),TIP('Adjustment Labour'),UPR
                           GROUP,AT(240,282,184,16),USE(?ExchangeRateGroup),HIDE
                             PROMPT('Exchange Rate'),AT(240,286),USE(?ExchangeRate),FONT(,8,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                             STRING(@n14.2),AT(304,286),USE(tmp:ClaimExchange),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@n14.2),AT(412,286,48,10),USE(tmp:AdjExchange),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Adjust Exchange Rate'),TIP('Adjust Exchange Rate'),UPR
                           END
                           STRING(@n14.2),AT(304,302),USE(tmp:ClaimParts),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@n14.2),AT(384,302),USE(tmp:AdjParts),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Claim Adjustment'),AT(385,258),USE(?Prompt6:2),FONT(,10,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Parts'),AT(240,302),USE(?Prompt1:2),FONT(,8,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Sub Total'),AT(240,314),USE(?Prompt1:3),FONT(,8,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           STRING(@n14.2),AT(304,330),USE(tmp:ClaimVAT),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@n14.2),AT(384,346),USE(tmp:AdjTotal),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('Confirm Claim Adjustment'),AT(496,346),USE(tmp:ConfirmClaimAdjustment),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                           STRING(@n14.2),AT(304,314),USE(tmp:ClaimSubTotal),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@n14.2),AT(384,314),USE(tmp:AdjSubTotal),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LINE,AT(308,326,151,0),USE(?Line1),COLOR(COLOR:Black)
                           PROMPT('V.A.T.'),AT(240,330),USE(?Prompt1:4),FONT(,8,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           STRING(@n14.2),AT(304,346),USE(tmp:ClaimTotal),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@n14.2),AT(384,330),USE(tmp:AdjVAT),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Claim Total'),AT(240,346),USE(?Prompt1:5),FONT(,8,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(548,366),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW5                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW5::EIPManager     BrowseEIPManager                 !Browse EIP Manager for Browse using ?List
EditInPlace::wpr:CostAdjustment EditEntryClass        !Edit-in-place class for field wpr:CostAdjustment
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
Pricing     Routine
    Parts$ = 0
    Adjustment$ = 0
    Save_wpr_ID = Access:WARPARTS.SaveFile()
    Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
    wpr:Ref_Number  = tmp:JobNumber
    Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
    Loop
        If Access:WARPARTS.NEXT()
           Break
        End !If
        If wpr:Ref_Number  <> tmp:JobNumber      |
            Then Break.  ! End If
        Parts$ += wpr:Quantity * wpr:Purchase_Cost
        Adjustment$ += wpr:CostAdjustment
    End !Loop
    Access:WARPARTS.RestoreFile(Save_wpr_ID)

    tmp:ClaimParts  = Parts$
    tmp:AdjParts    = Adjustment$

    Access:MANUFACT.Clearkey(man:Manufacturer_Key)
    man:Manufacturer    = job:Manufacturer
    If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        !Found

    Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        !Error
    End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign

    Pricing_Routine('W',labour",parts",pass",claim",handling",exchange",RRCRate",RRCParts")

    If job:Ignore_Warranty_Charges = 'YES'
        tmp:ClaimLabour = job:Labour_cost_Warranty
    Else !If job:Ignore_Warranty_Charge = 'YES'
        If GETINI('STOCK','UseVirtualPrice',,CLIP(PATH())&'\SB2KDEF.INI') = 1
            If ~jobe:ExchangedATRRC and job:Exchange_Unit_Number <> 0
                 job:Courier_Cost_Warranty    = man:ExchangeFee
            Else !If ~jobe:ExchangeATRRC and job:Exchange_Unit_Number <> 0
                 job:Courier_Cost_Warranty    = 0
            End !If ~jobe:ExchangeATRRC and job:Exchange_Unit_Number <> 0

            tmp:ClaimExchange = job:Courier_Cost_Warranty

            tmp:ClaimLabour = Claim"
        Else !If GETINI('STOCK','UseVirtualPrice',,CLIP(PATH())&'\SB2KDEF.INI') = 1
            tmp:ClaimLabour = Labour"
        End !If GETINI('STOCK','UseVirtualPrice',,CLIP(PATH())&'\SB2KDEF.INI') = 1
    End !If job:Ignore_Warranty_Charge = 'YES'
    !tmp:ClaimLabour     = tmp:ClaimLabour + job:Courier_Cost_Warranty

    tmp:ClaimSubTotal   = tmp:ClaimLabour + tmp:ClaimParts + tmp:ClaimExchange

    tmp:ClaimVAT        = (tmp:ClaimLabour * (VatRate(job:Account_Number,'L')/100)) + |
                            (tmp:ClaimExchange * (VatRate(job:Account_Number,'L')/100)) + |
                            (tmp:ClaimParts * (VatRate(job:Account_Number,'P')/100))

    tmp:ClaimTotal      = tmp:ClaimSubTotal + tmp:ClaimVAT


    tmp:AdjSubTotal     = tmp:AdjLabour + tmp:AdjParts + tmp:AdjExchange

    tmp:AdjVAT          = (tmp:AdjLabour * (VatRate(job:Account_Number,'L')/100)) + |
                            (tmp:AdjExchange * (VatRate(job:Account_Number,'L')/100)) + |
                            (tmp:AdjParts * (VatRate(job:Account_Number,'P')/100))
    tmp:AdjTotal        = tmp:AdjSubTotal + tmp:AdjVAT

    Display()
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020540'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('AmendClaimValue')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?OK,RequestCancelled)
  Relate:JOBS.Open
  Access:JOBS_ALIAS.UseFile
  Access:JOBSE.UseFile
  Access:MANUFACT.UseFile
  SELF.FilesOpened = True
  tmp:JobNumber   = func:JobNumber
  Access:JOBSE.Clearkey(jobe:RefNumberKey)
  jobe:RefNumber  = tmp:JobNumber
  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !Found
      tmp:AdjLabour              = jobe:LabourAdjustment
      tmp:ConfirmClaimAdjustment = jobe:ConfirmClaimAdjustment
      tmp:AdjExchange             = jobe:ExchangeAdjustment
  Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !Error
  End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  
  
  BRW5.Init(?List,Queue:Browse.ViewPosition,BRW5::View:Browse,Queue:Browse,Relate:WARPARTS,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Do Pricing
  
  Access:JOBS.Clearkey(job:Ref_Number_Key)
  job:Ref_Number  = tmp:JobNumber
  If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      !Found
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber    = job:Ref_Number
      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        !Found
  
      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        !Error
      End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      If job:Exchange_Unit_Number <> '' and ~jobe:ExchangedATRRC
          ?ExchangeRateGroup{prop:Hide} = 0
      Else !job:Exchange_Unit_Number <> ''
          ?ExchangeRateGroup{prop:Hide} = 1
      End !job:Exchange_Unit_Number <> ''
  Else ! If Access:JOBS.Tryfetch(job:RefNumberKey) = Level:Benign
      !Error
  End !If Access:JOBS.Tryfetch(job:RefNumberKey) = Level:Benign
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW5.Q &= Queue:Browse
  BRW5.RetainRow = 0
  BRW5.AddSortOrder(,wpr:Part_Number_Key)
  BRW5.AddRange(wpr:Ref_Number,tmp:JobNumber)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,wpr:Part_Number,1,BRW5)
  BIND('tmp:Claimed',tmp:Claimed)
  BRW5.AddField(wpr:Part_Number,BRW5.Q.wpr:Part_Number)
  BRW5.AddField(wpr:Description,BRW5.Q.wpr:Description)
  BRW5.AddField(wpr:Quantity,BRW5.Q.wpr:Quantity)
  BRW5.AddField(tmp:Claimed,BRW5.Q.tmp:Claimed)
  BRW5.AddField(wpr:CostAdjustment,BRW5.Q.wpr:CostAdjustment)
  BRW5.AddField(wpr:Record_Number,BRW5.Q.wpr:Record_Number)
  BRW5.AddField(wpr:Ref_Number,BRW5.Q.wpr:Ref_Number)
  BRW5.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW5.AskProcedure = 0
      CLEAR(BRW5.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Update_Warranty_Part
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020540'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020540'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020540'&'0')
      ***
    OF ?tmp:AdjLabour
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:AdjLabour, Accepted)
      Do Pricing
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:AdjLabour, Accepted)
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber  = tmp:JobNumber
      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          !Found
          jobe:LabourAdjustment       = tmp:AdjLabour
          jobe:PartsAdjustment        = tmp:AdjParts
          jobe:SubTotalAdjustment     = jobe:LabourAdjustment + jobe:PartsAdjustment
          jobe:ConfirmClaimAdjustment = tmp:ConfirmClaimAdjustment
          jobe:ExchangeAdjustment     = tmp:AdjExchange
          Access:JOBSE.TryUpdate()
      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          !Error
      End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW5.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.EIP &= BRW5::EIPManager
  SELF.AddEditControl(EditInPlace::wpr:CostAdjustment,5)
  SELF.AddEditControl(,4)
  SELF.ArrowAction = EIPAction:Default+EIPAction:Remain+EIPAction:RetainColumn
  IF WM.Request <> ViewRecord
    SELF.ChangeControl=?Change
  END


BRW5.SetQueueRecord PROCEDURE

  CODE
  tmp:Claimed = wpr:Quantity * wpr:Purchase_Cost
  PARENT.SetQueueRecord
  SELF.Q.tmp:Claimed = tmp:Claimed                    !Assign formula result to display queue


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW5::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(5, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  Do Pricing
  BRW5::RecordStatus=ReturnValue
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(5, ValidateRecord, (),BYTE)
  RETURN ReturnValue

Get_EDI_Reason PROCEDURE                              !Generated from procedure template - Window

tmp:Close            BYTE(0)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:UserName         STRING(70)
window               WINDOW('Please Provide the Reason for Your Action'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Please Provide A Reason For Your Action'),AT(262,170),USE(?WindowTitle:2),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       TEXT,AT(262,194,156,106),USE(glo:EDI_Reason),FONT(,,010101H,,CHARSET:ANSI),COLOR(080FFFFH),REQ,UPR
                       PROMPT('Please Provide A Reason For Your Action'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,84,352,244),USE(?Panel1),FILL(09A6A7CH)
                       PROMPT('Current User:'),AT(262,136),USE(?Prompt4),TRN,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                       STRING(@s70),AT(233,148),USE(tmp:UserName),TRN,CENTER,FONT(,12,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020546'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Get_EDI_Reason')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:USERS.Open
  SELF.FilesOpened = True
  Access:USERS.Clearkey(use:Password_Key)
  use:Password    = glo:Password
  If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      ! Found
      tmp:UserName = Clip(use:Forename) & ' ' & Clip(use:Surname)
  Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      ! Error
  End ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:USERS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020546'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020546'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020546'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      If glo:EDI_Reason <> ''
          tmp:Close  = 1
          Post(Event:CloseWindow)
      End !glo:EDI_Reason <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    OF ?Cancel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      glo:EDI_Reason = ''
      tmp:Close = 1
      Post(Event:CloseWindow)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
      If tmp:Close = 0
          Cycle
      End !tmp:Close = 0
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
RemoteDespatch PROCEDURE (func:JobNumber,func:DespatchType) !Generated from procedure template - Window

save_jpt_id          USHORT,AUTO
save_webjob_id       USHORT,AUTO
Local                CLASS
ValidateAccessories  Procedure(),Byte
                     END
tmp:UserName         STRING(60)
save_jac_id          USHORT,AUTO
save_lac_id          USHORT,AUTO
tmp:UserCode         STRING(3)
tmp:DespatchDate     DATE
tmp:ConsignmentNo    STRING(30)
tmp:DespatchType     STRING(3)
Courier_temp         STRING(30)
tmp:DespatchCode     STRING(3)
tmp:createInvoice    BYTE
tmp:CourierCost      REAL
tmp:LabourCosts      REAL
tmp:PartsCost        REAL
tmp:SubTotal         REAL
tmp:VAT              REAL
tmp:Total            REAL
InvoiceText          STRING(20)
tmp:RenameCourierCost BYTE(0)
tmp:CourierCostName  STRING(30)
tmp:LoanText         STRING(255)
tmp:Paid             REAL
tmp:ARCLocation      STRING(30)
locAuditNotes        STRING(255)
window               WINDOW('Despatch Information'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Despatch Information'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,84,352,246),USE(?Panel1),FILL(09A6A7CH)
                       ENTRY(@s30),AT(244,160,124,10),USE(job:Company_Name_Delivery),SKIP,TRN,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH),READONLY
                       STRING('This warranty string will be changed'),AT(168,116,212,16),USE(?WarrantyString),TRN,FONT('Tahoma',14,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       STRING(@s20),AT(384,116,128,16),USE(InvoiceText),TRN,FONT('Tahoma',14,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       STRING(@s255),AT(168,132),USE(tmp:LoanText),FONT(,10,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('Despatched to:'),AT(168,160),USE(?Prompt1),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s30),AT(244,172,124,10),USE(job:Address_Line1_Delivery),SKIP,TRN,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH),READONLY
                       GROUP('Chargeable Costs'),AT(404,132,108,120),USE(?ChargeableGroup),BOXED,TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         PROMPT('Parts'),AT(412,162),USE(?PartPrompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         STRING('V.A.T.'),AT(412,204),USE(?String6),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         LINE,AT(416,218,86,0),USE(?Line2),COLOR(COLOR:Black)
                         STRING('Total'),AT(412,226),USE(?String8),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         PROMPT('Paid'),AT(412,236),USE(?Prompt11),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         LINE,AT(414,188,88,0),USE(?Line1),COLOR(COLOR:Black)
                         ENTRY(@n10.2),AT(452,162,52,10),USE(tmp:PartsCost),SKIP,TRN,RIGHT,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH),READONLY
                         PROMPT('Courier'),AT(412,172),USE(?CourierCostPrompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@n10.2),AT(452,172,52,10),USE(tmp:CourierCost),SKIP,TRN,RIGHT,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH),READONLY
                         ENTRY(@n10.2),AT(452,194,52,10),USE(tmp:SubTotal),SKIP,TRN,RIGHT,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH),READONLY
                         ENTRY(@n10.2),AT(452,204,52,10),USE(tmp:VAT),SKIP,TRN,RIGHT,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH),READONLY
                         ENTRY(@n14.2),AT(452,226,52,10),USE(tmp:Total),SKIP,TRN,RIGHT,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH),READONLY
                         ENTRY(@n14.2),AT(452,236,52,10),USE(tmp:Paid),SKIP,TRN,RIGHT,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH),READONLY
                         PROMPT('SubTotal'),AT(412,194),USE(?SubTotalPrompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         PROMPT('Labour'),AT(412,148),USE(?LabourPrompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@n10.2),AT(452,148,52,10),USE(tmp:LabourCosts),SKIP,TRN,RIGHT,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH),READONLY
                       END
                       PROMPT('Job Number:'),AT(168,148),USE(?Prompt10),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s8),AT(244,148,124,10),USE(job:Ref_Number),SKIP,TRN,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH),READONLY
                       ENTRY(@s30),AT(244,184,124,10),USE(job:Address_Line2_Delivery),SKIP,TRN,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH),READONLY
                       ENTRY(@s30),AT(244,196,124,10),USE(job:Address_Line3_Delivery),SKIP,TRN,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH),READONLY
                       PROMPT('Despatched by:'),AT(168,208),USE(?Prompt2),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s60),AT(244,208,124,10),USE(tmp:UserName),SKIP,TRN,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH),READONLY
                       ENTRY(@d17),AT(244,230,124,10),USE(tmp:DespatchDate),FONT(,,,FONT:bold),COLOR(COLOR:White)
                       PROMPT('Courier'),AT(168,246),USE(?Prompt4),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s30),AT(244,246,124,10),USE(Courier_temp),LEFT,FONT(,,,FONT:bold),COLOR(COLOR:White)
                       BUTTON,AT(372,242),USE(?LookupCourier),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                       PROMPT('Consignment No'),AT(168,262),USE(?tmp:ConsignmentNo:prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s30),AT(244,262,124,10),USE(tmp:ConsignmentNo),FONT(,,,FONT:bold),COLOR(COLOR:White)
                       BUTTON,AT(168,280),USE(?InvoiceButton),TRN,FLAT,LEFT,ICON('invoicep.jpg')
                       PROMPT('Despatch Date'),AT(168,230),USE(?Prompt3),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       BUTTON,AT(448,280),USE(?OkButton),TRN,FLAT,LEFT,ICON('despp.jpg'),DEFAULT
                       BUTTON,AT(240,280),USE(?PaymentButton),TRN,FLAT,LEFT,ICON('paydetp.jpg')
                       BUTTON,AT(448,332),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:Courier_temp                Like(Courier_temp)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
CheckPayment        Routine
    !How much has been paid
    tmp:Paid = 0
    Save_jpt_ID = Access:JOBPAYMT.SaveFile()
    Access:JOBPAYMT.ClearKey(jpt:All_Date_Key)
    jpt:Ref_Number = job:Ref_Number
    Set(jpt:All_Date_Key,jpt:All_Date_Key)
    Loop
        If Access:JOBPAYMT.NEXT()
           Break
        End !If
        If jpt:Ref_Number <> job:Ref_Number      |
            Then Break.  ! End If
        Access:USERS.Clearkey(use:User_Code_Key)
        use:User_Code   = jpt:User_code
        If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
            !Found
            If use:Location <> tmp:ARCLocation
                tmp:Paid += jpt:Amount
            End !If use:Location = tmp:ARCLocation
        Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
            !Error
        End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
    End !Loop
    Access:JOBPAYMT.RestoreFile(Save_jpt_ID)
CalculateVAT     routine

    if job:Invoice_Number>0 !this has been invoiced

    else

    end !if job:invoice number >0CalculateVAT     routine
!and this is really where it starts!
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020539'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('RemoteDespatch')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Relate:JOBPAYMT.Open
  Relate:VATCODE.Open
  Relate:WAYBILLS.Open
  Relate:WEBJOB.Open
  Access:USERS.UseFile
  Access:JOBS.UseFile
  Access:JOBSE.UseFile
  Access:COURIER.UseFile
  Access:LOAN.UseFile
  Access:TRADEACC.UseFile
  Access:JOBACC.UseFile
  Access:INVOICE.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  !set up local variables
  tmp:DespatchType = func:DespatchType
  tmp:despatchCode  = tmp:despatchtype[1:3]
  
  !JOB
  access:jobs.clearkey(job:Ref_Number_Key)
  job:ref_Number = func:JobNumber
  Access:jobs.fetch(job:Ref_Number_Key)
  
  !Jobe - chargeable details
  Access:JOBSE.Clearkey(jobe:RefNumberKey)
  jobe:RefNumber  = job:Ref_Number
  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !Found
  
  Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !Error
  End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  
  tmp:RenameCourierCost = Clip(GETINI('RENAME','RenameCourierCost',,CLIP(PATH())&'\SB2KDEF.INI'))
  
  if tmp:RenameCourierCost
      tmp:CourierCostName = Clip(GETINI('RENAME','CourierCostName',,CLIP(PATH())&'\SB2KDEF.INI'))
  end
  
  if tmp:CourierCostName <> ''
      ?CourierCostPrompt{prop:Text} = clip(tmp:CourierCostName)                       ! Rename courier cost prompt
  end
  
  If glo:WebJob
      Case jobe:DespatchType
          Of 'JOB'
              Courier_temp    = job:Courier
          Of 'EXC'
              Courier_temp    = job:Exchange_Courier
          Of 'LOA'
              Courier_Temp    = job:Loan_Courier
      End !Case job:Despatch_Type
  End !glo:WebJob
  
  Access:COURIER.Clearkey(cou:Courier_Key)
  cou:Courier = Courier_Temp
  If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
      !Found
      If cou:AutoConsignmentNo
          ?tmp:ConsignmentNo{prop:Hide} = 1
          ?tmp:ConsignmentNo:Prompt{prop:Hide} = 1
      End !If cou:AutoConsignmentNo
  Else ! If Access:COURIER.Tryfetch(cou:Courier_Temp) = Level:Benign
      !Error
  End !If Access:COURIER.Tryfetch(cou:Courier_Temp) = Level:Benign
  
  DISPLAY()
  !check for ready to despatch
  set(defaults)
  access:defaults.next()
  
  error"=''
  
  !Checks have already been done. the job is in the despatch table. If it's there. it needs to be despatched
  !no need for error checks
  
  
  !CASE tmp:DespatchType
  !    OF 'LOAN'
  !        IF  job:loan_status <> 'AWAITING DESPATCH' THEN ERROR" = 'LOAN UNIT NOT READY FOR DESPATCH'.
  !        IF  job:loan_despatched <> '' THEN ERROR" = 'LOAN UNIT ALREADY DESPATCHED'.
  !        IF  job:loan_unit_number = '' THEN ERROR" = 'LOAN UNIT NOT ALLOCATED'.
  !    OF 'EXCHANGE'
  !        IF  job:exchange_status <> 'AWAITING DESPATCH' THEN ERROR" = 'EXCHANGE UNIT NOT READY FOR DESPATCH'.
  !        IF  job:exchange_despatched <> '' THEN ERROR" = 'EXCHANGE UNIT ALREADY DESPATCHED'.
  !        IF  job:exchange_unit_number = '' THEN ERROR" = 'EXCHANGE UNIT NOT ALLOCATED'.
  !    OF 'JOB'
  !        IF  job:date_completed = '' THEN ERROR" = 'JOB NOT COMPLETED'.
  !        IF  job:despatched = 'YES'THEN ERROR" = 'JOB ALREADY DESPATCHED'.
  !        IF  DEF:QA_REQUIRED = 'YES'
  !            IF  job:qa_passed <> 'YES' AND job:qa_second_passed <> 'YES' THEN ERROR" = 'JOB HAS NOT PASSED QA'.
  !        END
  !
  !END !CASE
  
  !set up the screen defaults
  
  !Cost string
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  tra:Account_Number  = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
  If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Found
      tmp:ARCLocation = tra:SiteLocation
  Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Error
  End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
  if job:Chargeable_Job = 'YES' then
  
  
      Do CheckPayment
  
      if job:Warranty_Job = 'YES' then
          ?WarrantyString{prop:text}='Split Warranty/Chargeable'
      ELSE
          ?WarrantyString{prop:text}='Chargeable Repair - ' & CLip(jobe:DespatchType)
      END !if warranty job
  
      !show charegeable costs
      tmp:CourierCost   = job:Courier_cost
      tmp:LabourCosts   = jobe:RRCCLabourCost
      tmp:PartsCost     = jobe:RRCCPartsCost
      tmp:SubTotal      = jobe:RRCCSubTotal
      Total_Price('C',vat",total",balance")
  
      if job:Invoice_Number>0 !this has been invoiced
          Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
          inv:Invoice_Number  = job:Invoice_Number
          If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
              !Found
              If inv:RRCInvoiceDate <> ''
                  InvoiceText = 'Invoice No. ' & CLIP(job:Invoice_Number) & '-' & Clip(tra:BranchIdentification)
                  tmp:CourierCost   = job:Invoice_Courier_cost
                  tmp:LabourCosts   = jobe:InvRRCCLabourCost
                  tmp:PartsCost     = jobe:InvRRCCPartsCost
                  tmp:SubTotal      = jobe:InvRRCCSubTotal
                  Total_Price('I',vat",total",balance")
  
              End !If inv:RRCInvoiceDate <> ''
          Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
              !Error
          End !If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
      end !if job:invoice number >0
      tmp:vat   = vat"
      tmp:Total = tmp:SubTotal + tmp:vat
  
  
  ELSE
      HIDE(?InvoiceButton)
      HIDE(?PaymentButton)
  
      ?WarrantyString{prop:text}='Warranty Repair - ' & CLip(jobe:DespatchType)
      !hide chargeable costs
      ?ChargeableGroup{prop:hide}=true
  END !IF chargeable job
  
  If job:Loan_Unit_Number <> 0 and jobe:DespatchType = 'JOB'
      tmp:LoanText = 'Loan Unit Outstanding'
  Else
      tmp:LoanText = ''
  End !job:Loan_Unit_Number <> 0
  
  !USER
  Access:users.clearkey(use:password_key)
  use:Password = glo:password
  access:users.fetch(use:password_key)
  tmp:UserName = clip(use:Forename)&' '&clip(use:Surname)
  tmp:UserCode = use:User_Code
  !Date
  tmp:DespatchDate = today()
  DISPLAY()
  
  
  
  
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?Courier_temp{Prop:Tip} AND ~?LookupCourier{Prop:Tip}
     ?LookupCourier{Prop:Tip} = 'Select ' & ?Courier_temp{Prop:Tip}
  END
  IF ?Courier_temp{Prop:Msg} AND ~?LookupCourier{Prop:Msg}
     ?LookupCourier{Prop:Msg} = 'Select ' & ?Courier_temp{Prop:Msg}
  END
  SELF.SetAlerts()
     If error" <> ''
          Case Missive('The selected job cannot be despatched.'&|
            '<13,10>'&|
            '<13,10>Error: ' & Clip(Error") & '.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
          Post(Event:CloseWindow)
      End!Case MessageEx
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:JOBPAYMT.Close
    Relate:VATCODE.Close
    Relate:WAYBILLS.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    PickCouriers
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?InvoiceButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?InvoiceButton, Accepted)
      Case Missive('Are you sure you want to create an invoice?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes') 
          Of 2 ! Yes Button
          
      
              Access:JOBS.Clearkey(job:Ref_Number_Key)
              job:Ref_Number  = func:JobNumber
              If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                  !Found
                  Access:JOBSE.Clearkey(jobe:RefNumberKey)
                  jobe:RefNumber  = job:Ref_Number
                  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                      !Found
      
                  Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                      !Error
                  End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                  Access:WEBJOB.Clearkey(wob:RefNumberKey)
                  wob:RefNumber = job:Ref_Number
                  If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                      !Found
      
                  Else !If Access:WEBJOB.Tryfetch(jobe:RefNumberKey) = Level:Benign
                      !Error
                  End !If Access:WEBJOB.Tryfetch(jobe:RefNumberKey) = Level:Benign
      
                  !Add new prototype - L945 (DBH: 04-09-2003)
                  JobPricingRoutine(0)
      
                  Access:JOBS.Update()
                  Access:JOBSE.Update()
      
                  Set(defaults)
                  access:defaults.next()
                  tmp:CreateInvoice = 0
      
                  IF CanJobInvoiceBePrinted(1) = Level:Benign
                      !tmp:CreateInvoice   = 1
                      If CreateInvoice() = Level:Benign
                          glo:Select1  = job:Invoice_Number
      !! Changing (DBH 25/05/2007) # 9024 - Allow to set invoice type
      !!                    IF glo:webjob
      !!                      VOdacom_Delivery_Note_Web
      !!                    ELSE
      !!                      Single_Invoice
      !!                    END
      !! to (DBH 25/05/2007) # 9024
      !                    If GETINI('PRINTING','PrintA5Invoice',,Clip(Path()) & '\SB2KDEF.INI') = 1
      !                        If glo:WebJob
      !                            Vodacom_Delivery_Note_Web('','')
      !                        Else ! If glo:WebJob
      !                            Vodacom_Delivery_Note
      !                        End ! If glo:WebJob
      !                        
      !                    Else ! If GETINI('PRINTING','PrintA5Invoice',,Clip(Path()) & '\SB2KDEF.INI') = 1
      !                        Single_Invoice('','')
      !                    End ! If GETINI('PRINTING','PrintA5Invoice',,Clip(Path()) & '\SB2KDEF.INI') = 1
      !! End (DBH 25/05/2007) #9024
                          VodacomSingleInvoice(job:Invoice_Number,job:Ref_Number,'','') ! #11881 New Single Invoice (Bryan: 16/03/2011)
      
                          InvoiceText = 'Invoice No. ' & CLIP(job:Invoice_Number)
                          ThisWindow.UPDATE()
                          DISPLAY()
                          glo:Select1  = job:Ref_Number
                      End
                  End!IF error# = 0
              Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                  !Error
              End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      
          Of 1 ! No Button
      End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?InvoiceButton, Accepted)
    OF ?OkButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      ! #12302 Rewrite code to deal with Vodacom crashes (DBH: 17/01/2012)
      !!Despatch job
      !    Set(DEFAULTS)
      !    Access:DEFAULTS.Next()
      !    Access:JOBS.Clearkey(job:Ref_Number_Key)
      !    job:Ref_Number  = func:JobNumber
      !    If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      !        !Found
      !
      !    Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      !        !Error
      !    End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      !
      !    !
      !    IF (JobInUse(job:Ref_Number,1)) ! #12302 Check again that the job is not in use (Bryan: 10/10/2011)
      !        CYCLE
      !    END
      !
      !    If courier_temp = ''
      !        Case Missive('You have not selected courier.','ServiceBase 3g',|
      !                       'mstop.jpg','/OK') 
      !            Of 1 ! OK Button
      !        End ! Case Missive
      !    Else!If courier_temp = ''
      !        Error# = 0
      !        Access:COURIER.Clearkey(cou:Courier_Key)
      !        cou:Courier = Courier_Temp
      !        If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
      !            !Found
      !            
      !        Else ! If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
      !            !Error
      !            Error# = 1
      !            Case Missive('Error! Cannot find the selected courier''s details.','ServiceBase 3g',|
      !                           'mstop.jpg','/OK') 
      !                Of 1 ! OK Button
      !            End ! Case Missive
      !        End !If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
      !
      !        Access:JOBSE.Clearkey(jobe:RefNumberKey)
      !        jobe:RefNumber  = job:Ref_Number
      !        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !            !Found
      !        Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !            !Error
      !        End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !
      !        ! Inserting (DBH 24/01/2007) # 8678 - Check if the job has been paid/invoiced and if it is therefore allowed to be despatched
      !        Save_WEBJOB_ID = Access:WEBJOB.SaveFile()
      !        Access:WEBJOB.ClearKey(wob:RefNumberKey)
      !        wob:RefNumber = job:Ref_Number
      !        If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      !            !Found
      !            If CanJobBeDespatched() = False
      !                Error# = 1
      !            End ! If CanJobBeDespatched() = False
      !        Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      !            !Error
      !        End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      !        Access:WEBJOB.RestoreFile(Save_WEBJOB_ID)
      !        ! End (DBH 24/01/2007) #8678
      !
      !        If def:Force_Accessory_Check = 'YES' And Error# = 0
      !            If Local.ValidateAccessories()
      !                Access:JOBS.Update()
      !                !Failed Check
      !                Error# = 1
      !            End !If LocalValidateAccessories(job:Despatch_Type,job:Ref_Number)
      !        End !If def:Force_Accessory_Check = 'YES'
      !
      !        If Error# = 0
      !            !Blank the global so that it should ask for the Security Pack Number
      !            glo:Select21 = ''
      !            !Get the courier again#
      !            Access:COURIER.Clearkey(cou:Courier_Key)
      !            cou:Courier = Courier_Temp
      !            If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
      !                !Found
      !                
      !            Else ! If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
      !                !Error
      !                Error# = 1
      !                Case Missive('Error! Cannot find the selected courier''s details.','ServiceBase 3g',|
      !                               'mstop.jpg','/OK') 
      !                    Of 1 ! OK Button
      !                End ! Case Missive
      !            End !If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
      !
      !            If Error# = 0
      !                !Use the new procedure to get a waybillnumber,
      !                !and do not continue if an error occurs. - 3432 (DBH: 27-10-2003)
      !                If cou:PrintWaybill
      !                    tmp:ConsignmentNo = NextWayBillNumber()
      !                    If tmp:ConsignmentNo = 0
      !                        Error# = 0
      !                    End !If tmp:ConsignmentNo = 0
      !
      !                    If Error# = 0
      !                        Access:WAYBILLS.Clearkey(way:WaybillNumberKey)
      !                        way:WaybillNumber   = tmp:ConsignmentNo
      !                        If Access:WAYBILLS.Tryfetch(way:WaybillNumberKey) = Level:Benign
      !                            !Found
      !                            way:AccountNumber   = Clarionet:Global.Param2
      !                            Case jobe:DespatchType
      !                                Of 'JOB'
      !                                    way:WaybillID       = 2
      !                                Of 'EXC'
      !                                    way:WaybillID       = 3
      !                                Of 'LOA'
      !                                    way:WaybillID       = 4
      !                            End !Case jobe:DespatchType
      !                            ! Inserting (DBH 07/07/2006) # 7149 - Specify this as a RRC to PUP Waybill
      !                            If job:Who_Booked = 'WEB'
      !                                way:WayBillType = 9
      !                                way:WayBillID   = 21
      !                            Else ! If job:Who_Booked = 'WEB'
      !                            ! End (DBH 07/07/2006) #7149
      !                                way:WayBillType     = 2 !Do not show in another confirmation browse
      !                            End ! If job:Who_Booked = 'WEB'
      !
      !                            way:FromAccount     = Clarionet:Global.Param2
      !                            way:ToAccount       = job:Account_Number
      !                            If Access:WAYBILLS.Update()= Level:Benign
      !                                !Add the waybill jobs list
      !                                If Access:WAYBILLJ.PrimeRecord() = Level:Benign
      !                                    waj:WayBillNumber      = way:WayBillNumber
      !                                    waj:JobNumber          = job:Ref_Number
      !                                    Case jobe:DespatchType
      !                                        Of 'JOB'
      !                                            waj:IMEINumber         = job:ESN
      !                                        Of 'EXC'
      !                                            Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
      !                                            xch:Ref_Number  = job:Exchange_Unit_Number
      !                                            If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
      !                                                !Found
      !                                                waj:IMEINumber  = xch:ESN
      !                                            Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
      !                                                !Error
      !                                            End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
      !                                        Of 'LOA'
      !                                            Access:LOAN.Clearkey(loa:Ref_Number_Key)
      !                                            loa:Ref_Number  = job:Loan_Unit_Number
      !                                            If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
      !                                                !Found
      !                                                waj:IMEINumber  = loa:ESN
      !                                            Else ! If Access:LOANS.Tryfetch(loa:Ref_Number_Key) = Level:Benign
      !                                                !Error
      !                                            End !If Access:LOANS.Tryfetch(loa:Ref_Number_Key) = Level:Benign
      !                                    End !Case jobe:DespatchType
      !                                    
      !                                    waj:OrderNumber        = job:Order_Number
      !                                    !waj:SecurityPackNumber = wyp:SecurityPackNumber
      !                                    !Will be filled in by DespatchWebJob Routine
      !                                    waj:JobType            = jobe:DespatchType
      !                                    If Access:WAYBILLJ.TryInsert() = Level:Benign
      !                                        !Insert Successful
      !
      !                                    Else !If Access:WAYBILLJ.TryInsert() = Level:Benign
      !                                        !Insert Failed
      !                                        Access:WAYBILLJ.CancelAutoInc()
      !                                    End !If Access:WAYBILLJ.TryInsert() = Level:Benign
      !                                End !If Access:WAYBILLJ.PrimeRecord() = Level:Benign
      !
      !                                !Insert Successful
      !                                DespatchWebJob(jobe:DespatchType,Courier_Temp,tmp:ConsignmentNo,0)
      !                            End !If Access:WAYBILLS.Update()= Level:Benign
      !                        Else ! If Access:WAYBILLS.Tryfetch(way:WaybillNumberKey) = Level:Benign
      !                            !Error
      !                            Error# = 1
      !                        End !If Access:WAYBILLS.Tryfetch(way:WaybillNumberKey) = Level:Benign
      !                    End !If Error# = 0
      !                Else !If cou:PrintWaybill
      !                   !print waybill
      !                    If cou:AutoConsignmentNo 
      !                        cou:LastConsignmentNo += 1
      !                        tmp:ConsignmentNo = cou:LastConsignmentNo
      !                        If Access:COURIER.Update()
      !                            Case Missive('An error occured updating the courier details, please try again.','ServiceBase 3g',|
      !                                           'mstop.jpg','/OK') 
      !                                Of 1 ! OK Button
      !                            End ! Case Missive
      !                            Error# = 1
      !                        End !If Access:COURIER.Update()
      !                    End!Else !If cou:AutoConsignmentNo
      !                    If Error# = 0
      !                        DespatchWebJob(jobe:DespatchType,Courier_Temp,tmp:ConsignmentNo,0)
      !                    End !If Error# = 0
      !                End  !If cou:PrintWaybill
      !
      !                !Despatch Web Job has changed job file - save now before printWaybill refetches it
      !                if access:jobs.update()
      !                    !error
      !                END
      !
      !                If cou:PrintWayBill And Error# = 0
      !                    Clear(glo:Q_JobNumber)
      !                    Free(glo:Q_JobNumber)
      !                    glo:Job_Number_Pointer = job:Ref_Number
      !                    Add(glo:q_JobNumber)
      !
      !                    If jobe:Sub_Sub_Account <> ''
      !                        WayBillDespatch(Clip(Clarionet:Global.Param2),'TRA',|
      !                                        jobe:Sub_Sub_Account,'SUB',|
      !                                        tmp:ConsignmentNo,Courier_Temp)
      !                    Else !If jobe:Sub_Sub_Account <> ''
      !                        WayBillDespatch(Clip(Clarionet:Global.Param2),'TRA',|
      !                                        job:Account_Number,'CUS',|
      !                                        tmp:ConsignmentNo,Courier_Temp)
      !                    End !If jobe:Sub_Sub_Account <> ''
      !
      !    !                Else
      !    !                    glo:select1 = job:ref_number
      !    !                    despatch_note
      !    !                    glo:select1 = ''
      !                End !If cou:PrintWayBill
      !
      !                !I don't need to update courier again.. do I?
      !!                access:courier.update()
      !
      !                !update jobse
      !                jobe:despatchType = ''
      !                jobe:despatched = ''
      !                access:jobse.update()
      !                save_WEBJOB_id = Access:WEBJOB.SaveFile()
      !                Access:WEBJOB.Clearkey(wob:RefNumberKey)
      !                wob:RefNumber = job:Ref_Number
      !                If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      !                  !Found
      !                  wob:ReadyToDespatch = 0
      !                  Access:WEBJOB.TryUpdate()
      !                Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      !                  !Error
      !                End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      !                Access:WEBJOB.RestoreFile(save_WEBJOB_id)
      !                !update Job
      !            End !If Error# = 0
      !        End !If Error# = 0
      !
      !    End!If courier_temp = ''
      !
      ! Despatch Job
          SET(DEFAULTS)
          Access:DEFAULTS.Next()
      
          ! Get Job
          Access:JOBS.Clearkey(job:Ref_Number_Key)
          job:Ref_Number = func:JobNumber
          IF (Access:JOBS.Tryfetch(job:Ref_Number_Key))
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('Unable to find the selected job.','ServiceBase',|
                  'mstop.jpg','/&OK') 
              Of 1 ! &OK Button
              End!Case Message
              CYCLE
          END
      
          IF (JobInUse(job:Ref_Number,1))
              CYCLE
          END
      
          IF (Courier_Temp = '')
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('You have not selected a courier.','ServiceBase',|
                  'mstop.jpg','/&OK') 
              Of 1 ! &OK Button
              End!Case Message
              CYCLE
          END
      
          Access:COURIER.Clearkey(cou:Courier_Key)
          cou:Courier = Courier_Temp
          IF (Access:COURIER.Tryfetch(cou:Courier_Key))
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('Unable to find the selected courier.','ServiceBase',|
                  'mstop.jpg','/&OK') 
              Of 1 ! &OK Button
              End!Case Message
              CYCLE
          END
      
          Access:WEBJOB.Clearkey(wob:RefNumberKey)
          wob:RefNumber = job:Ref_Number
          IF (Access:WEBJOB.Tryfetch(wob:RefNumberKey))
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('An error occurred looking up the job details. Please try again.','ServiceBase',|
                  'mstop.jpg','/&OK') 
              Of 1 ! &OK Button
              End!Case Message
              CYCLE
          END
      
          Access:JOBSE.Clearkey(jobe:RefNumberKey)
          jobe:RefNumber = job:Ref_Number
          IF (Access:JOBSE.Tryfetch(jobe:RefNumberKey))
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('An error occurred looking up the job details. Please try again.','ServiceBase',|
                  'mstop.jpg','/&OK') 
              Of 1 ! &OK Button
              End!Case Message
              CYCLE
          END
      
          IF (CanTheJobBeDespatched() = FALSE)
              CYCLE
          END
      
          IF (def:Force_Accessory_Check = 'YES')
              IF (local.ValidateAccessories())
                  CYCLE
              END
          END
      
          IF (cou:PrintWaybill)
              tmp:ConsignmentNo = NextWaybillNumber()
              IF (tmp:ConsignmentNo = 0)
                  CYCLE
              END
      
              IF (DespatchTheJob(tmp:ConsignmentNo))
                  ! An error occurred, remove the waybill
                  Access:WAYBILLS.Clearkey(way:WaybillNumberKey)
                  way:WaybillNumber = tmp:ConsignmentNo
                  IF (Access:WAYBILLS.Tryfetch(way:WaybillNumberKey) = Level:Benign)
                      Relate:WAYBILLS.Delete(0)
                  END
              END
          ELSE
              IF (cou:AutoConsignmentNo)
                  cou:LastConsignmentNo += 1
                  tmp:ConsignmentNo = cou:LastConsignmentNo
                  IF (Access:COURIER.TryUpdate())
                      Case Missive('An error occured updating the courier details, please try again.','ServiceBase 3g',|
                          'mstop.jpg','/OK') 
                      Of 1 ! OK Button
                      End ! Case Missive
                      CYCLE
                  END
              END
              IF (DespatchTheJob(tmp:ConsignmentNo))
                  ! An error occurred
              END
          END ! IF (cou:PrintWaybill)
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?PaymentButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PaymentButton, Accepted)
      If SecurityCheck('PAYMENT DETAILS')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
      Else!If SecurityCheck('PAYMENT DETAILS')
          Thiswindow.reset(1)
      
          Access:JOBS.Clearkey(job:Ref_Number_Key)
          job:Ref_Number  = func:JobNumber
          If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
              !Found
      
              continue# = 1
              if job:Loan_Unit_Number = 0
                  If job:Warranty_Job = 'YES' and job:Chargeable_Job <> 'YES'
                      continue# = 0
                      Case Missive('This job is marked as a warranty job only.'&|
                        '<13,10>'&|
                        '<13,10>It can only be marked as paid by reconciling the claim.','ServiceBase 3g',|
                                     'mstop.jpg','/OK') 
                          Of 1 ! OK Button
                      End ! Case Missive
                  End!If job_ali:Warranty_Job = 'YES' and job_ali:Chargeable_Job <> 'YES'
              end
      
              If job:Date_Completed = '' And continue# = 1
                  Case Missive('This job has not been completed.'&|
                    '<13,10>'&|
                    '<13,10>Are you sure you want to insert/amend payments?','ServiceBase 3g',|
                                 'mquest.jpg','\No|/Yes') 
                      Of 2 ! Yes Button
                      Of 1 ! No Button
                          continue# = 0
                  End ! Case Missive
              End!If job_ali:Date_Completed = '' And continue# = 1
      
              If continue# = 1
                  glo:Select1 = func:JobNumber
      
                  Browse_Payments
      
                  Access:JOBS.Clearkey(job:Ref_Number_Key)
                  job:Ref_Number  = func:JobNumber
                  If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Found
                      ChkPaid()
                  Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Error
                  End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      
              End!If continue# = 1
              Do CheckPayment
          Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
              !Error
          End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      
      End!If SecurityCheck('PAYMENT DETAILS')
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PaymentButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020539'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020539'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020539'&'0')
      ***
    OF ?Courier_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Courier_temp, Accepted)
      !Check for Consignment number
      Access:COURIER.Clearkey(cou:Courier_Key)
      cou:Courier  = Courier_Temp
      If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
          !Found
          if cou:AutoConsignmentNo then
              ?tmp:ConsignmentNo{prop:Hide} = 1
              ?tmp:ConsignmentNo:Prompt{prop:Hide} = 1
          ELSE
              ?tmp:ConsignmentNo{prop:Hide} = 0
              ?tmp:ConsignmentNo:Prompt{prop:Hide} = 0
          END !if autoconsignment number in use
      
      Else ! If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
          !Error
      End !If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
      
      !thismakeover.refresh()
      thiswindow.update()
      display()
      IF Courier_temp OR ?Courier_temp{Prop:Req}
        cou:Courier = Courier_temp
        !Save Lookup Field Incase Of error
        look:Courier_temp        = Courier_temp
        IF Access:COURIER.TryFetch(cou:Courier_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            Courier_temp = cou:Courier
          ELSE
            !Restore Lookup On Error
            Courier_temp = look:Courier_temp
            SELECT(?Courier_temp)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Courier_temp, Accepted)
    OF ?LookupCourier
      ThisWindow.Update
      cou:Courier = Courier_temp
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          Courier_temp = cou:Courier
          Select(?+1)
      ELSE
          Select(?Courier_temp)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?Courier_temp)
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
Local.ValidateAccessories    Procedure()
local:ErrorCode             Byte
local:Despatch_Type         like(job:Despatch_Type)
    Code

    if glo:WebJob
        local:Despatch_Type = jobe:DespatchType 
    else
        local:Despatch_Type = job:Despatch_Type
    end

    local:ErrorCode  = 0
    Case local:Despatch_Type
        Of 'JOB'
            If job:Exchange_Unit_Number <> ''
                !If an exchange unit is attached, do not
                !do the accessory check
                Return Level:Benign
            Else !If job:Exchange_Unit_Number <> ''
                If AccessoryCheck('JOB')
                    If AccessoryMismatch(job:Ref_Number,'JOB')
                        local:Errorcode = 1
                    End !If AccessoryMismatch(job:Ref_Number,'JOB')
                End !If AccessoryCheck('JOB')
            End !If job:Exchange_Unit_Number <> ''
        Of 'LOA'
            If AccessoryCheck('LOAN')
                If AccessoryMismatch(job:Ref_Number,'LOA')
                    local:Errorcode = 1
                End !If AccessoryMismatch(job:Ref_Number,'LOA')
            End !If AccessoryCheck('LOAN')
        Of 'EXC'
            If AccessoryCheck('EXC')
                If AccessoryMismatch(job:Ref_Number,'EXC')
                    local:Errorcode = 1
                End !If AccessoryMismatch(job:Ref_Number,'EXC')
            End !If AccesoryCheck('EXC')
    End !Case job:Despatch_Type
    If local:Errorcode = 1
        GetStatus(850,1,local:Despatch_Type)
        IF (Access:JOBS.TryUpdate() = Level:Benign)

            locAuditNotes         = 'ACCESSORY MISMATCH<13,10,13,10>ACCESSORIES BOOKED IN:'
            If local:Despatch_Type <> 'LOAN'
                Save_jac_ID = Access:JOBACC.SaveFile()
                Access:JOBACC.ClearKey(jac:Ref_Number_Key)
                jac:Ref_Number = job:Ref_Number
                Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
                Loop
                    If Access:JOBACC.NEXT()
                       Break
                    End !If
                    If jac:Ref_Number <> job:Ref_Number      |
                        Then Break.  ! End If
                    locAuditNotes = Clip(locAuditNotes) & '<13,10>' & Clip(jac:Accessory)
                End !Loop
                Access:JOBACC.RestoreFile(Save_jac_ID)
            Else !If job:Despatch_Type <> 'LOAN'
                Save_lac_ID = Access:LOANACC.SaveFile()
                Access:LOANACC.ClearKey(lac:Ref_Number_Key)
                lac:Ref_Number = job:Ref_Number
                Set(lac:Ref_Number_Key,lac:Ref_Number_Key)
                Loop
                    If Access:LOANACC.NEXT()
                       Break
                    End !If
                    If lac:Ref_Number <> job:Ref_Number      |
                        Then Break.  ! End If
                    locAuditNotes = Clip(locAuditNotes) & '<13,10>' & Clip(lac:Accessory)
                End !Loop
                Access:LOANACC.RestoreFile(Save_lac_ID)
            End !If job:Despatch_Type <> 'LOAN'
            locAuditNotes         = Clip(locAuditNotes) & '<13,10,13,10>ACCESSORIES BOOKED OUT: '
            Clear(glo:Queue)
            Loop x# = 1 To Records(glo:Queue)
                Get(glo:Queue,x#)
                locAuditNotes = Clip(locAuditNotes) & '<13,10>' & Clip(glo:Pointer)
            End !Loop x# = 1 To Records(glo:Queue).

            IF (AddToAudit(job:Ref_Number,local:Despatch_Type,'FAILED DESPATCH VALIDATION',locAuditNotes))
            END ! IF

        End!If Access:AUDIT.PrimeRecord() = Level:Benign
        Return Level:Fatal
        
    End !If Return Level:Fatal

    Return Level:Benign
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
FinalRejectionDate PROCEDURE (func:Date,func:RejectionReason) !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Date             DATE
tmp:RejectionReason  STRING('EXPIRED DATE RANGE {237}')
tmp:Pass             BYTE(0)
window               WINDOW('Reject Claims'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(160,64,360,300),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(164,84,352,44),USE(?PanelTip),FILL(0D6EAEFH)
                       GROUP('Top Tip'),AT(168,88,344,36),USE(?GroupTip),BOXED,TRN
                       END
                       BUTTON,AT(476,92,36,32),USE(?ButtonHelp),TRN,FLAT,ICON('F1Help.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Reject Claims'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,132,352,198),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Only jobs with a completed date up to, and including, the selected date with be ' &|
   'Finally Rejected'),AT(238,154,204,20),USE(?Prompt1),CENTER,FONT(,,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Date'),AT(240,190),USE(?tmp:Date:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(316,190,64,10),USE(tmp:Date),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Date'),TIP('Date'),REQ,UPR
                           BUTTON,AT(384,186),USE(?LookupDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Rejection Reason'),AT(240,212),USE(?tmp:RejectionReason:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(316,212,124,72),USE(tmp:RejectionReason),VSCROLL,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Rejection Reason'),TIP('Rejection Reason'),REQ,UPR
                         END
                       END
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:Pass)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020545'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('FinalRejectionDate')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  tmp:Date = Today()
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
  ?tmp:Date{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020545'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020545'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020545'&'0')
      ***
    OF ?LookupDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:Date = TINCALENDARStyle1(tmp:Date)
          Display(?tmp:Date)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Cancel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      tmp:Pass = 0
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
          ! Changing (DBH 11/01/2006) #6996 - Force the rejection reason
          ! If tmp:Date <> 0 And tmp:RejectionReason <> ''
          ! to (DBH 11/01/2006) #6996
          If tmp:Date <> 0 And Clip(tmp:RejectionReason) <> ''
          ! End (DBH 11/01/2006) #6996
              func:Date = tmp:Date
          ! Changing (DBH 01/02/2007) # 8719 - Surely this is wrong
          !    func:RejectionReason = glo:EDI_Reason
          ! to (DBH 01/02/2007) # N/A
              func:RejectionReason = tmp:RejectionReason
          ! End (DBH 01/02/2007) #N/A
              tmp:Pass = 1
              Post(Event:CloseWindow)
          End !tmp:Date <> 0 And tmp:Reason <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:Date
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
SelectNewBatchNumber PROCEDURE (func:Manufacturer,func:BatchNumber) !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:DestinationBatch LONG
tmp:Manufacturer     STRING(30)
tmp:Close            BYTE(0)
FDB6::View:FileDrop  VIEW(EDIBATCH)
                       PROJECT(ebt:Batch_Number)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?tmp:DestinationBatch
ebt:Batch_Number       LIKE(ebt:Batch_Number)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Move Claims'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(240,146,200,144),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,164,192,44),USE(?PanelTip),FILL(0D6EAEFH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(396,172,36,32),USE(?ButtonHelp),TRN,FLAT,ICON('F1Help.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Move Claims'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       GROUP('Top Tip'),AT(248,168,184,36),USE(?GroupTip),BOXED,TRN
                         SHEET,AT(244,212,192,42),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                           TAB('Destination Batch Number'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             LIST,AT(308,230,64,10),USE(tmp:DestinationBatch),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('24L(2)|M@p<<<<<<<<<<#pB@'),DROP(5),FROM(Queue:FileDrop)
                           END
                         END
                         BUTTON,AT(300,258),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                         BUTTON,AT(368,258),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       END
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDB6                 CLASS(FileDropClass)             !File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:Close)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020550'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('SelectNewBatchNumber')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:EDIBATCH.Open
  SELF.FilesOpened = True
  tmp:Manufacturer = func:Manufacturer
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?tmp:DestinationBatch{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?tmp:DestinationBatch{prop:vcr} = False
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  FDB6.Init(?tmp:DestinationBatch,Queue:FileDrop.ViewPosition,FDB6::View:FileDrop,Queue:FileDrop,Relate:EDIBATCH,ThisWindow)
  FDB6.Q &= Queue:FileDrop
  FDB6.AddSortOrder(ebt:Batch_Number_Key)
  FDB6.AddRange(ebt:Manufacturer,tmp:Manufacturer)
  FDB6.AddField(ebt:Batch_Number,FDB6.Q.ebt:Batch_Number)
  ThisWindow.AddItem(FDB6.WindowComponent)
  FDB6.DefaultFill = 0
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EDIBATCH.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020550'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020550'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020550'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      If tmp:DestinationBatch <> 0
          func:BatchNumber  = tmp:DestinationBatch
          tmp:Close = 1
          Post(event:CloseWindow)
      End !tmp:DestinationBatch <> 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    OF ?Cancel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      tmp:Close = 0
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

FDB6.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %FileDropMethodCodeSection) DESC(FileDrop Method Executable Code Section) ARG(6, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  If ebt:ApprovedDate <> 0
      Return Record:Filtered
  End !ebt:ApprovedDate <> 0
  ! After Embed Point: %FileDropMethodCodeSection) DESC(FileDrop Method Executable Code Section) ARG(6, ValidateRecord, (),BYTE)
  RETURN ReturnValue




ARCClaims PROCEDURE                                   !Generated from procedure template - Window

CurrentTab           STRING(80)
Local                CLASS
FinalRejection       Procedure(Byte func:Type)
                     END
tmp:Tag              STRING(1)
BouncerApproved      STRING(30)
tmp:POPRequired      BYTE(0)
save_wob_id          USHORT,AUTO
save_job_id          USHORT,AUTO
save_aud_id          USHORT,AUTO
save_job_ali_id      USHORT,AUTO
FilesOpened          BYTE
Manufacturer_Temp    STRING(30)
Batch_Number_Temp    LONG
jobs_tag             STRING(1)
jobs_tag_temp        STRING(1)
Charge_Repair_Type_Temp STRING(60)
BrFilter1            STRING(300)
BrLocator1           STRING(50)
BrFilter2            STRING(300)
BrLocator2           STRING(50)
tmp:Claim            REAL
tmp:NO               STRING('NO')
tmp:JobNumber        STRING(20)
tmp:Parts_Cost_Warranty REAL
tmp:EDI              STRING(3)
cost:RRCExchange     REAL
cost:RRCHandling     REAL
cost:RRCLabour       REAL
cost:RRCParts        REAL
cost:RRCTotal        REAL
cost:ARCExchange     REAL
cost:ARCLabour       REAL
cost:ARCParts        REAL
cost:ARCTotal        REAL
cost:ClaimExchange   REAL
cost:ClaimLabour     REAL
cost:ClaimParts      REAL
cost:ClaimTotal      REAL
tmp:ProcessBeforeDate DATE
tmp:ExchangedAt      STRING(3)
tmp:RepairedAt       STRING(3)
tmp:BranchID         STRING(2)
tmp:SelectID         STRING(30)
tmp:Filter           BYTE(0)
tmp:RRCClaimStatus   STRING(30)
tmp:Blank            STRING(30)
tmp:BouncerClaim     BYTE(0)
tmp:Date             DATE
tmp:ResubmitFlag     BYTE(0)
pos                  STRING(255)
tmp:SiteFilter       BYTE(0)
tmp:SiteFilterType   STRING('ARC')
tmp:JobType          STRING('NO {1}')
tmp:ARCLocation      STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:SelectManufacturer BYTE(0)
BRW1::View:Browse    VIEW(WEBJOB)
                       PROJECT(wob:RefNumber)
                       PROJECT(wob:JobNumber)
                       PROJECT(wob:HeadAccountNumber)
                       PROJECT(wob:RecordNumber)
                       PROJECT(wob:EDI)
                       PROJECT(wob:Manufacturer)
                       JOIN(job:Ref_Number_Key,wob:RefNumber)
                         PROJECT(job:Ref_Number)
                         PROJECT(job:Account_Number)
                         PROJECT(job:ESN)
                         PROJECT(job:Date_Completed)
                         PROJECT(job:Model_Number)
                         PROJECT(job:Warranty_Charge_Type)
                         PROJECT(job:Repair_Type_Warranty)
                         PROJECT(job:MSN)
                         PROJECT(job:Order_Number)
                         PROJECT(job:Fault_Code1)
                         PROJECT(job:Fault_Code2)
                         PROJECT(job:Fault_Code3)
                         PROJECT(job:Fault_Code4)
                         PROJECT(job:Fault_Code5)
                         PROJECT(job:Fault_Code6)
                         PROJECT(job:Fault_Code7)
                         PROJECT(job:Fault_Code8)
                         PROJECT(job:Fault_Code9)
                         PROJECT(job:Fault_Code10)
                         PROJECT(job:Fault_Code11)
                         PROJECT(job:Fault_Code12)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
tmp:Tag                LIKE(tmp:Tag)                  !List box control field - type derived from local data
tmp:Tag_NormalFG       LONG                           !Normal forground color
tmp:Tag_NormalBG       LONG                           !Normal background color
tmp:Tag_SelectedFG     LONG                           !Selected forground color
tmp:Tag_SelectedBG     LONG                           !Selected background color
tmp:Tag_Icon           LONG                           !Entry's icon ID
wob:RefNumber          LIKE(wob:RefNumber)            !List box control field - type derived from field
wob:RefNumber_NormalFG LONG                           !Normal forground color
wob:RefNumber_NormalBG LONG                           !Normal background color
wob:RefNumber_SelectedFG LONG                         !Selected forground color
wob:RefNumber_SelectedBG LONG                         !Selected background color
wob:JobNumber          LIKE(wob:JobNumber)            !List box control field - type derived from field
wob:JobNumber_NormalFG LONG                           !Normal forground color
wob:JobNumber_NormalBG LONG                           !Normal background color
wob:JobNumber_SelectedFG LONG                         !Selected forground color
wob:JobNumber_SelectedBG LONG                         !Selected background color
wob:HeadAccountNumber  LIKE(wob:HeadAccountNumber)    !List box control field - type derived from field
wob:HeadAccountNumber_NormalFG LONG                   !Normal forground color
wob:HeadAccountNumber_NormalBG LONG                   !Normal background color
wob:HeadAccountNumber_SelectedFG LONG                 !Selected forground color
wob:HeadAccountNumber_SelectedBG LONG                 !Selected background color
job:Ref_Number         LIKE(job:Ref_Number)           !List box control field - type derived from field
job:Ref_Number_NormalFG LONG                          !Normal forground color
job:Ref_Number_NormalBG LONG                          !Normal background color
job:Ref_Number_SelectedFG LONG                        !Selected forground color
job:Ref_Number_SelectedBG LONG                        !Selected background color
tmp:BranchID           LIKE(tmp:BranchID)             !List box control field - type derived from local data
tmp:BranchID_NormalFG  LONG                           !Normal forground color
tmp:BranchID_NormalBG  LONG                           !Normal background color
tmp:BranchID_SelectedFG LONG                          !Selected forground color
tmp:BranchID_SelectedBG LONG                          !Selected background color
tmp:JobNumber          LIKE(tmp:JobNumber)            !List box control field - type derived from local data
tmp:JobNumber_NormalFG LONG                           !Normal forground color
tmp:JobNumber_NormalBG LONG                           !Normal background color
tmp:JobNumber_SelectedFG LONG                         !Selected forground color
tmp:JobNumber_SelectedBG LONG                         !Selected background color
tmp:RepairedAt         LIKE(tmp:RepairedAt)           !List box control field - type derived from local data
tmp:RepairedAt_NormalFG LONG                          !Normal forground color
tmp:RepairedAt_NormalBG LONG                          !Normal background color
tmp:RepairedAt_SelectedFG LONG                        !Selected forground color
tmp:RepairedAt_SelectedBG LONG                        !Selected background color
job:Account_Number     LIKE(job:Account_Number)       !List box control field - type derived from field
job:Account_Number_NormalFG LONG                      !Normal forground color
job:Account_Number_NormalBG LONG                      !Normal background color
job:Account_Number_SelectedFG LONG                    !Selected forground color
job:Account_Number_SelectedBG LONG                    !Selected background color
job:ESN                LIKE(job:ESN)                  !List box control field - type derived from field
job:ESN_NormalFG       LONG                           !Normal forground color
job:ESN_NormalBG       LONG                           !Normal background color
job:ESN_SelectedFG     LONG                           !Selected forground color
job:ESN_SelectedBG     LONG                           !Selected background color
job:Date_Completed     LIKE(job:Date_Completed)       !List box control field - type derived from field
job:Date_Completed_NormalFG LONG                      !Normal forground color
job:Date_Completed_NormalBG LONG                      !Normal background color
job:Date_Completed_SelectedFG LONG                    !Selected forground color
job:Date_Completed_SelectedBG LONG                    !Selected background color
job:Model_Number       LIKE(job:Model_Number)         !List box control field - type derived from field
job:Model_Number_NormalFG LONG                        !Normal forground color
job:Model_Number_NormalBG LONG                        !Normal background color
job:Model_Number_SelectedFG LONG                      !Selected forground color
job:Model_Number_SelectedBG LONG                      !Selected background color
job:Warranty_Charge_Type LIKE(job:Warranty_Charge_Type) !List box control field - type derived from field
job:Warranty_Charge_Type_NormalFG LONG                !Normal forground color
job:Warranty_Charge_Type_NormalBG LONG                !Normal background color
job:Warranty_Charge_Type_SelectedFG LONG              !Selected forground color
job:Warranty_Charge_Type_SelectedBG LONG              !Selected background color
job:Repair_Type_Warranty LIKE(job:Repair_Type_Warranty) !List box control field - type derived from field
job:Repair_Type_Warranty_NormalFG LONG                !Normal forground color
job:Repair_Type_Warranty_NormalBG LONG                !Normal background color
job:Repair_Type_Warranty_SelectedFG LONG              !Selected forground color
job:Repair_Type_Warranty_SelectedBG LONG              !Selected background color
tmp:ExchangedAt        LIKE(tmp:ExchangedAt)          !List box control field - type derived from local data
tmp:ExchangedAt_NormalFG LONG                         !Normal forground color
tmp:ExchangedAt_NormalBG LONG                         !Normal background color
tmp:ExchangedAt_SelectedFG LONG                       !Selected forground color
tmp:ExchangedAt_SelectedBG LONG                       !Selected background color
job:MSN                LIKE(job:MSN)                  !List box control field - type derived from field
job:MSN_NormalFG       LONG                           !Normal forground color
job:MSN_NormalBG       LONG                           !Normal background color
job:MSN_SelectedFG     LONG                           !Selected forground color
job:MSN_SelectedBG     LONG                           !Selected background color
job:Order_Number       LIKE(job:Order_Number)         !List box control field - type derived from field
job:Order_Number_NormalFG LONG                        !Normal forground color
job:Order_Number_NormalBG LONG                        !Normal background color
job:Order_Number_SelectedFG LONG                      !Selected forground color
job:Order_Number_SelectedBG LONG                      !Selected background color
job:Fault_Code1        LIKE(job:Fault_Code1)          !List box control field - type derived from field
job:Fault_Code1_NormalFG LONG                         !Normal forground color
job:Fault_Code1_NormalBG LONG                         !Normal background color
job:Fault_Code1_SelectedFG LONG                       !Selected forground color
job:Fault_Code1_SelectedBG LONG                       !Selected background color
job:Fault_Code2        LIKE(job:Fault_Code2)          !List box control field - type derived from field
job:Fault_Code2_NormalFG LONG                         !Normal forground color
job:Fault_Code2_NormalBG LONG                         !Normal background color
job:Fault_Code2_SelectedFG LONG                       !Selected forground color
job:Fault_Code2_SelectedBG LONG                       !Selected background color
job:Fault_Code3        LIKE(job:Fault_Code3)          !List box control field - type derived from field
job:Fault_Code3_NormalFG LONG                         !Normal forground color
job:Fault_Code3_NormalBG LONG                         !Normal background color
job:Fault_Code3_SelectedFG LONG                       !Selected forground color
job:Fault_Code3_SelectedBG LONG                       !Selected background color
job:Fault_Code4        LIKE(job:Fault_Code4)          !List box control field - type derived from field
job:Fault_Code4_NormalFG LONG                         !Normal forground color
job:Fault_Code4_NormalBG LONG                         !Normal background color
job:Fault_Code4_SelectedFG LONG                       !Selected forground color
job:Fault_Code4_SelectedBG LONG                       !Selected background color
job:Fault_Code5        LIKE(job:Fault_Code5)          !List box control field - type derived from field
job:Fault_Code5_NormalFG LONG                         !Normal forground color
job:Fault_Code5_NormalBG LONG                         !Normal background color
job:Fault_Code5_SelectedFG LONG                       !Selected forground color
job:Fault_Code5_SelectedBG LONG                       !Selected background color
job:Fault_Code6        LIKE(job:Fault_Code6)          !List box control field - type derived from field
job:Fault_Code6_NormalFG LONG                         !Normal forground color
job:Fault_Code6_NormalBG LONG                         !Normal background color
job:Fault_Code6_SelectedFG LONG                       !Selected forground color
job:Fault_Code6_SelectedBG LONG                       !Selected background color
job:Fault_Code7        LIKE(job:Fault_Code7)          !List box control field - type derived from field
job:Fault_Code7_NormalFG LONG                         !Normal forground color
job:Fault_Code7_NormalBG LONG                         !Normal background color
job:Fault_Code7_SelectedFG LONG                       !Selected forground color
job:Fault_Code7_SelectedBG LONG                       !Selected background color
job:Fault_Code8        LIKE(job:Fault_Code8)          !List box control field - type derived from field
job:Fault_Code8_NormalFG LONG                         !Normal forground color
job:Fault_Code8_NormalBG LONG                         !Normal background color
job:Fault_Code8_SelectedFG LONG                       !Selected forground color
job:Fault_Code8_SelectedBG LONG                       !Selected background color
job:Fault_Code9        LIKE(job:Fault_Code9)          !List box control field - type derived from field
job:Fault_Code9_NormalFG LONG                         !Normal forground color
job:Fault_Code9_NormalBG LONG                         !Normal background color
job:Fault_Code9_SelectedFG LONG                       !Selected forground color
job:Fault_Code9_SelectedBG LONG                       !Selected background color
job:Fault_Code10       LIKE(job:Fault_Code10)         !List box control field - type derived from field
job:Fault_Code10_NormalFG LONG                        !Normal forground color
job:Fault_Code10_NormalBG LONG                        !Normal background color
job:Fault_Code10_SelectedFG LONG                      !Selected forground color
job:Fault_Code10_SelectedBG LONG                      !Selected background color
job:Fault_Code11       LIKE(job:Fault_Code11)         !List box control field - type derived from field
job:Fault_Code11_NormalFG LONG                        !Normal forground color
job:Fault_Code11_NormalBG LONG                        !Normal background color
job:Fault_Code11_SelectedFG LONG                      !Selected forground color
job:Fault_Code11_SelectedBG LONG                      !Selected background color
job:Fault_Code12       LIKE(job:Fault_Code12)         !List box control field - type derived from field
job:Fault_Code12_NormalFG LONG                        !Normal forground color
job:Fault_Code12_NormalBG LONG                        !Normal background color
job:Fault_Code12_SelectedFG LONG                      !Selected forground color
job:Fault_Code12_SelectedBG LONG                      !Selected background color
jobe:POPConfirmed      LIKE(jobe:POPConfirmed)        !List box control field - type derived from field
jobe:POPConfirmed_NormalFG LONG                       !Normal forground color
jobe:POPConfirmed_NormalBG LONG                       !Normal background color
jobe:POPConfirmed_SelectedFG LONG                     !Selected forground color
jobe:POPConfirmed_SelectedBG LONG                     !Selected background color
jobe:PendingClaimColour LIKE(jobe:PendingClaimColour) !List box control field - type derived from field
jobe:PendingClaimColour_NormalFG LONG                 !Normal forground color
jobe:PendingClaimColour_NormalBG LONG                 !Normal background color
jobe:PendingClaimColour_SelectedFG LONG               !Selected forground color
jobe:PendingClaimColour_SelectedBG LONG               !Selected background color
tmp:ResubmitFlag       LIKE(tmp:ResubmitFlag)         !List box control field - type derived from local data
tmp:ResubmitFlag_NormalFG LONG                        !Normal forground color
tmp:ResubmitFlag_NormalBG LONG                        !Normal background color
tmp:ResubmitFlag_SelectedFG LONG                      !Selected forground color
tmp:ResubmitFlag_SelectedBG LONG                      !Selected background color
tmp:BouncerClaim       LIKE(tmp:BouncerClaim)         !List box control field - type derived from local data
tmp:BouncerClaim_NormalFG LONG                        !Normal forground color
tmp:BouncerClaim_NormalBG LONG                        !Normal background color
tmp:BouncerClaim_SelectedFG LONG                      !Selected forground color
tmp:BouncerClaim_SelectedBG LONG                      !Selected background color
tmp:BouncerClaim_Icon  LONG                           !Entry's icon ID
tmp:Blank              LIKE(tmp:Blank)                !List box control field - type derived from local data
tmp:Blank_NormalFG     LONG                           !Normal forground color
tmp:Blank_NormalBG     LONG                           !Normal background color
tmp:Blank_SelectedFG   LONG                           !Selected forground color
tmp:Blank_SelectedBG   LONG                           !Selected background color
tmp:RRCClaimStatus     LIKE(tmp:RRCClaimStatus)       !Browse hot field - type derived from local data
wob:RecordNumber       LIKE(wob:RecordNumber)         !Primary key field - type derived from field
wob:EDI                LIKE(wob:EDI)                  !Browse key field - type derived from field
wob:Manufacturer       LIKE(wob:Manufacturer)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK15::wob:EDI             LIKE(wob:EDI)
HK15::wob:HeadAccountNumber LIKE(wob:HeadAccountNumber)
HK15::wob:Manufacturer    LIKE(wob:Manufacturer)
! ---------------------------------------- Higher Keys --------------------------------------- !
!Extension Templete Code:XploreOOPBrowse,DataSectionBeforeWindow
!CW Version         = 5507                            !Xplore
!CW TemplateVersion = v5.5                            !Xplore
XploreMask1          DECIMAL(10,0,0)                  !Xplore
XploreMask11          DECIMAL(10,0,48)                !Xplore
XploreTitle1         STRING(' ')                      !Xplore
xpInitialTab1        SHORT                            !Xplore
QuickWindow          WINDOW('Browse ARC Warranty Jobs'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PROMPT('Browse The ARC Warranty Job File'),AT(8,12),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,10,640,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(592,12),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,384,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(608,154),USE(?ResubmitAccept),TRN,FLAT,LEFT,ICON('resubclp.jpg')
                       LIST,AT(8,88,572,286),USE(?Browse:1),IMM,HVSCROLL,COLOR(COLOR:White),MSG('Browsing Records'),ALRT(EnterKey),ALRT(MouseLeft2),FORMAT('0L(2)F*I@s1@37R(2)|FM*~Job No~@n_8@0L(2)F*~Job Number~@s8@0L(2)F*~Header Account' &|
   ' Number~@s30@0R(2)|FM*~Job No~@s8@0C(2)|FM*~ID~@s2@62R(2)|FM*~RRC Job No~@s20@0L' &|
   '(2)|M*~Repaired~@s3@64L(2)|M*~Account Number~@s15@69L(2)|M*~I.M.E.I. Number~@s16' &|
   '@38R(2)|M*~Completed~@D6b@58L(2)|M*~Model Number~@s30@64L(2)|M*~Charge Type~@s30' &|
   '@64L(2)|M*~Repair Type~@s30@43L(2)|M*~Exchanged~@s3@61L(2)|M*~MSN~@s14@120L(2)|M' &|
   '*~Order Number~@s30@120L(2)|M*~Fault Code 1~@s30@120L(2)|M*~Fault Code 2~@s30@12' &|
   '0L(2)|M*~Fault Code 3~@s30@120L(2)|M*~Fault Code 4~@s30@120L(2)|M*~Fault Code 5~' &|
   '@s30@120L(2)|M*~Fault Code 6~@s30@120L(2)|M*~Fault Code 7~@s30@120L(2)|M*~Fault ' &|
   'Code 8~@s30@120L(2)|M*~Fault Code 9~@s30@120L(2)|M*~Fault Code 10~@s255@120L(2)|' &|
   'M*~Fault Code 11~@s255@120L(2)|M*~Fault Code 12~@s255@66L(2)|M*~POP Confirmed~@n' &|
   '1@0L(2)|M*~Pending Claim Colour~@n1@0L(2)|M*~Resubmit Flag~@n1@11L(2)|*I~B~@n1@1' &|
   '20L(2)|*@s30@'),FROM(Queue:Browse:1)
                       GROUP('Mark Job'),AT(588,194,80,36),USE(?Group1),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         BUTTON,AT(616,207),USE(?Blue),TRN,FLAT,ICON('markblup.jpg')
                         BUTTON,AT(640,207),USE(?Red),TRN,FLAT,ICON('markredp.jpg')
                         BUTTON,AT(592,207),USE(?Green),TRN,FLAT,ICON('markgrnp.jpg')
                       END
                       BUTTON,AT(608,254),USE(?BrowseAudit),TRN,FLAT,LEFT,ICON('auditp.jpg')
                       BUTTON,AT(608,288),USE(?View),TRN,FLAT,LEFT,ICON('viewjobp.jpg')
                       BUTTON,AT(608,120),USE(?Valudate_Claims),TRN,FLAT,LEFT,ICON('valclamp.jpg')
                       ENTRY(@s30),AT(116,54,128,10),USE(Manufacturer_Temp),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                       BUTTON,AT(248,50),USE(?LookupManufacturer),TRN,FLAT,ICON('lookupp.jpg')
                       SHEET,AT(4,34,672,350),USE(?CurrentTab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Job Ready For Warranty Process'),USE(?Tab:2)
                           OPTION('Job Type'),AT(376,38,204,48),USE(tmp:JobType),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Pending Claims'),AT(384,49),USE(?tmp:JobType:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('NO')
                             RADIO('Approved Claims'),AT(496,49),USE(?tmp:JobType:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('APP')
                             RADIO('In Process Claims'),AT(384,60),USE(?tmp:JobType:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES')
                             RADIO('Rejected Claims'),AT(496,60),USE(?tmp:JobType:Radio4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('EXC')
                             RADIO('Accepted Rejected Claims'),AT(384,72),USE(?tmp:JobType:Radio5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('AAJ')
                           END
                           CHECK('Select By Manufacturer'),AT(8,55),USE(tmp:SelectManufacturer),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Select By Manufacturer'),TIP('Select By Manufacturer'),VALUE('1','0')
                           PROMPT('Job Ready For Warranty Process'),AT(8,39),USE(?Prompt2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(608,384),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
ViewOrder              BYTE(0)                        !Xplore
FileSeqOn              BYTE(0)                        !Xplore
SequenceNbr            BYTE(0)                        !Xplore
SortOrderNbr           BYTE(0)                        !Xplore
FileOrderNbr           BYTE(0)                        !Xplore
LocatorField           STRING(30)                     !Xplore
SavedPosition          USHORT                         !Xplore
SavePosition           PROCEDURE()                    !Xplore
RestorePosition        PROCEDURE()                    !Xplore
RecordsInKey           PROCEDURE(),LONG,PROC          !Xplore
ResetPairsQ            PROCEDURE()                    !Xplore
OrderInit              PROCEDURE(<Key K>)            !Xplore
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
GetFreeElementName     PROCEDURE(),STRING,DERIVED
GetFreeElementPosition PROCEDURE(),BYTE,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeEvent              PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort1:Locator  StepLocatorClass                 !Conditional Locator - Upper(tmp:SelectManufacturer) = 0
BRW1::Sort2:Locator  StepLocatorClass                 !Conditional Locator - Upper(tmp:SelectManufacturer) = 1
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:Manufacturer_Temp                Like(Manufacturer_Temp)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
!Extension Templete Code:XploreOOPBrowse,LocalDataAfterClasses
Xplore1              CLASS(XploreClass)               !Xplore
RestoreHeader          BYTE(0)
LastEvent              LONG
AddColumnSortOrder     PROCEDURE(LONG SortQRecord),VIRTUAL
ListBoxData            PROCEDURE(),PRIVATE,VIRTUAL
GetStringForGraph      PROCEDURE(SHORT QFieldNumber,*CSTRING Pic),VIRTUAL
SetNewOrderFields      PROCEDURE(),VIRTUAL
SetFilters             PROCEDURE(),VIRTUAL
GetRecordsInKey        PROCEDURE(),LONG,PROC,VIRTUAL
MyPreviewer            PROCEDURE(PreviewQueue PQ,REPORT CPCS),BYTE,VIRTUAL
                     END                              !Xplore
Xplore1Step1         StepCustomClass !                !Xplore: Column displaying tmp:Tag
Xplore1Locator1      IncrementalLocatorClass          !Xplore: Column displaying tmp:Tag
Xplore1Step2         StepLongClass   !LONG            !Xplore: Column displaying wob:RefNumber
Xplore1Locator2      IncrementalLocatorClass          !Xplore: Column displaying wob:RefNumber
Xplore1Step3         StepLongClass   !LONG            !Xplore: Column displaying wob:JobNumber
Xplore1Locator3      IncrementalLocatorClass          !Xplore: Column displaying wob:JobNumber
Xplore1Step4         StepStringClass !STRING          !Xplore: Column displaying wob:HeadAccountNumber
Xplore1Locator4      IncrementalLocatorClass          !Xplore: Column displaying wob:HeadAccountNumber
Xplore1Step5         StepLongClass   !LONG            !Xplore: Column displaying job:Ref_Number
Xplore1Locator5      IncrementalLocatorClass          !Xplore: Column displaying job:Ref_Number
Xplore1Step6         StepCustomClass !                !Xplore: Column displaying tmp:BranchID
Xplore1Step8         StepCustomClass !                !Xplore: Column displaying tmp:RepairedAt
Xplore1Locator8      IncrementalLocatorClass          !Xplore: Column displaying tmp:RepairedAt
Xplore1Step9         StepStringClass !STRING          !Xplore: Column displaying job:Account_Number
Xplore1Locator9      IncrementalLocatorClass          !Xplore: Column displaying job:Account_Number
Xplore1Step10        StepStringClass !STRING          !Xplore: Column displaying job:ESN
Xplore1Locator10     IncrementalLocatorClass          !Xplore: Column displaying job:ESN
Xplore1Step11        StepCustomClass !DATE            !Xplore: Column displaying job:Date_Completed
Xplore1Locator11     IncrementalLocatorClass          !Xplore: Column displaying job:Date_Completed
Xplore1Step12        StepStringClass !STRING          !Xplore: Column displaying job:Model_Number
Xplore1Locator12     IncrementalLocatorClass          !Xplore: Column displaying job:Model_Number
Xplore1Step13        StepStringClass !STRING          !Xplore: Column displaying job:Warranty_Charge_Type
Xplore1Locator13     IncrementalLocatorClass          !Xplore: Column displaying job:Warranty_Charge_Type
Xplore1Step14        StepStringClass !STRING          !Xplore: Column displaying job:Repair_Type_Warranty
Xplore1Locator14     IncrementalLocatorClass          !Xplore: Column displaying job:Repair_Type_Warranty
Xplore1Step15        StepCustomClass !                !Xplore: Column displaying tmp:ExchangedAt
Xplore1Locator15     IncrementalLocatorClass          !Xplore: Column displaying tmp:ExchangedAt
Xplore1Step16        StepStringClass !STRING          !Xplore: Column displaying job:MSN
Xplore1Locator16     IncrementalLocatorClass          !Xplore: Column displaying job:MSN
Xplore1Step17        StepStringClass !STRING          !Xplore: Column displaying job:Order_Number
Xplore1Locator17     IncrementalLocatorClass          !Xplore: Column displaying job:Order_Number
Xplore1Step18        StepStringClass !STRING          !Xplore: Column displaying job:Fault_Code1
Xplore1Locator18     IncrementalLocatorClass          !Xplore: Column displaying job:Fault_Code1
Xplore1Step19        StepStringClass !STRING          !Xplore: Column displaying job:Fault_Code2
Xplore1Locator19     IncrementalLocatorClass          !Xplore: Column displaying job:Fault_Code2
Xplore1Step20        StepStringClass !STRING          !Xplore: Column displaying job:Fault_Code3
Xplore1Locator20     IncrementalLocatorClass          !Xplore: Column displaying job:Fault_Code3
Xplore1Step21        StepStringClass !STRING          !Xplore: Column displaying job:Fault_Code4
Xplore1Locator21     IncrementalLocatorClass          !Xplore: Column displaying job:Fault_Code4
Xplore1Step22        StepStringClass !STRING          !Xplore: Column displaying job:Fault_Code5
Xplore1Locator22     IncrementalLocatorClass          !Xplore: Column displaying job:Fault_Code5
Xplore1Step23        StepStringClass !STRING          !Xplore: Column displaying job:Fault_Code6
Xplore1Locator23     IncrementalLocatorClass          !Xplore: Column displaying job:Fault_Code6
Xplore1Step24        StepStringClass !STRING          !Xplore: Column displaying job:Fault_Code7
Xplore1Locator24     IncrementalLocatorClass          !Xplore: Column displaying job:Fault_Code7
Xplore1Step25        StepStringClass !STRING          !Xplore: Column displaying job:Fault_Code8
Xplore1Locator25     IncrementalLocatorClass          !Xplore: Column displaying job:Fault_Code8
Xplore1Step26        StepStringClass !STRING          !Xplore: Column displaying job:Fault_Code9
Xplore1Locator26     IncrementalLocatorClass          !Xplore: Column displaying job:Fault_Code9
Xplore1Step27        StepStringClass !STRING          !Xplore: Column displaying job:Fault_Code10
Xplore1Locator27     IncrementalLocatorClass          !Xplore: Column displaying job:Fault_Code10
Xplore1Step28        StepStringClass !STRING          !Xplore: Column displaying job:Fault_Code11
Xplore1Locator28     IncrementalLocatorClass          !Xplore: Column displaying job:Fault_Code11
Xplore1Step29        StepStringClass !STRING          !Xplore: Column displaying job:Fault_Code12
Xplore1Locator29     IncrementalLocatorClass          !Xplore: Column displaying job:Fault_Code12
Xplore1Step30        StepCustomClass !BYTE            !Xplore: Column displaying jobe:POPConfirmed
Xplore1Locator30     IncrementalLocatorClass          !Xplore: Column displaying jobe:POPConfirmed
Xplore1Step31        StepCustomClass !BYTE            !Xplore: Column displaying jobe:PendingClaimColour
Xplore1Locator31     IncrementalLocatorClass          !Xplore: Column displaying jobe:PendingClaimColour
Xplore1Step32        StepCustomClass !                !Xplore: Column displaying tmp:ResubmitFlag
Xplore1Locator32     IncrementalLocatorClass          !Xplore: Column displaying tmp:ResubmitFlag
Xplore1Step33        StepCustomClass !                !Xplore: Column displaying tmp:BouncerClaim
Xplore1Locator33     IncrementalLocatorClass          !Xplore: Column displaying tmp:BouncerClaim
Xplore1Step34        StepCustomClass !                !Xplore: Column displaying tmp:Blank
Xplore1Locator34     IncrementalLocatorClass          !Xplore: Column displaying tmp:Blank
Xplore1Step35        StepCustomClass !                !Xplore: Column displaying tmp:RRCClaimStatus
Xplore1Locator35     IncrementalLocatorClass          !Xplore: Column displaying tmp:RRCClaimStatus
Xplore1Step36        StepLongClass   !LONG            !Xplore: Column displaying wob:RecordNumber
Xplore1Locator36     IncrementalLocatorClass          !Xplore: Column displaying wob:RecordNumber
Xplore1Step37        StepStringClass !STRING          !Xplore: Column displaying wob:EDI
Xplore1Locator37     IncrementalLocatorClass          !Xplore: Column displaying wob:EDI
Xplore1Step38        StepStringClass !STRING          !Xplore: Column displaying wob:Manufacturer
Xplore1Locator38     IncrementalLocatorClass          !Xplore: Column displaying wob:Manufacturer

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
!TagGreen    Routine
!  ?Browse:1{PROPLIST:MouseDownField} = 2
!  SETCURSOR(CURSOR:Wait)
!  BRW1.Reset
!  FREE(glo:Queue)
!  LOOP
!    NEXT(BRW1::View:Browse)
!    IF ERRORCODE()
!      BREAK
!    END
!     If jobe:PendingClaimColour = 1
!         glo:Queue.Pointer = job:Ref_Number
!         ADD(glo:Queue,glo:Queue.Pointer)
!     End !If jobe:PendingClaimColour = 1
!  END
!  SETCURSOR
!  BRW1.ResetSort(1)
!  SELECT(?Browse:1,CHOICE(?Browse:1))
!
!TagBlue    Routine
!  ?Browse:1{PROPLIST:MouseDownField} = 2
!  SETCURSOR(CURSOR:Wait)
!  BRW1.Reset
!  FREE(glo:Queue)
!  LOOP
!    NEXT(BRW1::View:Browse)
!    IF ERRORCODE()
!      BREAK
!    END
!     If jobe:PendingClaimColour = 2
!         glo:Queue.Pointer = job:Ref_Number
!         ADD(glo:Queue,glo:Queue.Pointer)
!     End !If jobe:PendingClaimColour = 1
!  END
!  SETCURSOR
!  BRW1.ResetSort(1)
!  SELECT(?Browse:1,CHOICE(?Browse:1))
!
!TagRed    Routine
!  ?Browse:1{PROPLIST:MouseDownField} = 2
!  SETCURSOR(CURSOR:Wait)
!  BRW1.Reset
!  FREE(glo:Queue)
!  LOOP
!    NEXT(BRW1::View:Browse)
!    IF ERRORCODE()
!      BREAK
!    END
!     If jobe:PendingClaimColour = 3
!         glo:Queue.Pointer = job:Ref_Number
!         ADD(glo:Queue,glo:Queue.Pointer)
!     End !If jobe:PendingClaimColour = 1
!  END
!  SETCURSOR
!  BRW1.ResetSort(1)
!  SELECT(?Browse:1,CHOICE(?Browse:1))
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeAccept
  Xplore1.GetBbSize('ARCClaims','?Browse:1')          !Xplore
  BRW1.SequenceNbr = 0                                !Xplore
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020541'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('ARCClaims')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:DISCOUNT.Open
  Relate:EDIBATCH.Open
  Relate:EXCHANGE.Open
  Relate:VATCODE.Open
  Relate:WEBJOB.Open
  Access:WARPARTS.UseFile
  Access:CHARTYPE.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:TRACHAR.UseFile
  Access:SUBCHRGE.UseFile
  Access:STDCHRGE.UseFile
  Access:MANUFACT.UseFile
  Access:LOCATLOG.UseFile
  Access:JOBNOTES.UseFile
  Access:INVOICE.UseFile
  Access:USERS.UseFile
  Access:JOBS_ALIAS.UseFile
  Access:JOBS.UseFile
  Access:JOBSE.UseFile
  Access:JOBSE2.UseFile
  SELF.FilesOpened = True
  tmp:ARCLocation = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:WEBJOB,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  !Extension Templete Code:XploreOOPBrowse,AfterWindowOpening
  !Extension Templete Code:XploreOOPBrowse,ProcedureSetup
  Xplore1.Init(ThisWindow,BRW1,Queue:Browse:1,QuickWindow,?Browse:1,'sbk01app.INI','>Header',0,BRW1.ViewOrder,Xplore1.RestoreHeader,BRW1.SequenceNbr,XploreMask1,XploreMask11,XploreTitle1,BRW1.FileSeqOn)
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?Manufacturer_Temp{Prop:Tip} AND ~?LookupManufacturer{Prop:Tip}
     ?LookupManufacturer{Prop:Tip} = 'Select ' & ?Manufacturer_Temp{Prop:Tip}
  END
  IF ?Manufacturer_Temp{Prop:Msg} AND ~?LookupManufacturer{Prop:Msg}
     ?LookupManufacturer{Prop:Msg} = 'Select ' & ?Manufacturer_Temp{Prop:Msg}
  END
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,wob:HeadEDIKey)
  BRW1.AddRange(wob:EDI)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(,wob:Manufacturer,1,BRW1)
  BRW1.AddSortOrder(,wob:HeadEDIKey)
  BRW1.AddRange(wob:Manufacturer)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(,wob:JobNumber,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,wob:HeadEDIKey)
  BRW1.AddRange(wob:EDI)
  BIND('tmp:Tag',tmp:Tag)
  BIND('tmp:BranchID',tmp:BranchID)
  BIND('tmp:JobNumber',tmp:JobNumber)
  BIND('tmp:RepairedAt',tmp:RepairedAt)
  BIND('tmp:ExchangedAt',tmp:ExchangedAt)
  BIND('tmp:ResubmitFlag',tmp:ResubmitFlag)
  BIND('tmp:BouncerClaim',tmp:BouncerClaim)
  BIND('tmp:Blank',tmp:Blank)
  BIND('tmp:RRCClaimStatus',tmp:RRCClaimStatus)
  BIND('Manufacturer_Temp',Manufacturer_Temp)
  ?Browse:1{PROP:IconList,1} = '~block_red.ico'
  ?Browse:1{PROP:IconList,2} = '~notick1.ico'
  ?Browse:1{PROP:IconList,3} = '~tick1.ico'
  BRW1.AddField(tmp:Tag,BRW1.Q.tmp:Tag)
  BRW1.AddField(wob:RefNumber,BRW1.Q.wob:RefNumber)
  BRW1.AddField(wob:JobNumber,BRW1.Q.wob:JobNumber)
  BRW1.AddField(wob:HeadAccountNumber,BRW1.Q.wob:HeadAccountNumber)
  BRW1.AddField(job:Ref_Number,BRW1.Q.job:Ref_Number)
  BRW1.AddField(tmp:BranchID,BRW1.Q.tmp:BranchID)
  BRW1.AddField(tmp:JobNumber,BRW1.Q.tmp:JobNumber)
  BRW1.AddField(tmp:RepairedAt,BRW1.Q.tmp:RepairedAt)
  BRW1.AddField(job:Account_Number,BRW1.Q.job:Account_Number)
  BRW1.AddField(job:ESN,BRW1.Q.job:ESN)
  BRW1.AddField(job:Date_Completed,BRW1.Q.job:Date_Completed)
  BRW1.AddField(job:Model_Number,BRW1.Q.job:Model_Number)
  BRW1.AddField(job:Warranty_Charge_Type,BRW1.Q.job:Warranty_Charge_Type)
  BRW1.AddField(job:Repair_Type_Warranty,BRW1.Q.job:Repair_Type_Warranty)
  BRW1.AddField(tmp:ExchangedAt,BRW1.Q.tmp:ExchangedAt)
  BRW1.AddField(job:MSN,BRW1.Q.job:MSN)
  BRW1.AddField(job:Order_Number,BRW1.Q.job:Order_Number)
  BRW1.AddField(job:Fault_Code1,BRW1.Q.job:Fault_Code1)
  BRW1.AddField(job:Fault_Code2,BRW1.Q.job:Fault_Code2)
  BRW1.AddField(job:Fault_Code3,BRW1.Q.job:Fault_Code3)
  BRW1.AddField(job:Fault_Code4,BRW1.Q.job:Fault_Code4)
  BRW1.AddField(job:Fault_Code5,BRW1.Q.job:Fault_Code5)
  BRW1.AddField(job:Fault_Code6,BRW1.Q.job:Fault_Code6)
  BRW1.AddField(job:Fault_Code7,BRW1.Q.job:Fault_Code7)
  BRW1.AddField(job:Fault_Code8,BRW1.Q.job:Fault_Code8)
  BRW1.AddField(job:Fault_Code9,BRW1.Q.job:Fault_Code9)
  BRW1.AddField(job:Fault_Code10,BRW1.Q.job:Fault_Code10)
  BRW1.AddField(job:Fault_Code11,BRW1.Q.job:Fault_Code11)
  BRW1.AddField(job:Fault_Code12,BRW1.Q.job:Fault_Code12)
  BRW1.AddField(jobe:POPConfirmed,BRW1.Q.jobe:POPConfirmed)
  BRW1.AddField(jobe:PendingClaimColour,BRW1.Q.jobe:PendingClaimColour)
  BRW1.AddField(tmp:ResubmitFlag,BRW1.Q.tmp:ResubmitFlag)
  BRW1.AddField(tmp:BouncerClaim,BRW1.Q.tmp:BouncerClaim)
  BRW1.AddField(tmp:Blank,BRW1.Q.tmp:Blank)
  BRW1.AddField(tmp:RRCClaimStatus,BRW1.Q.tmp:RRCClaimStatus)
  BRW1.AddField(wob:RecordNumber,BRW1.Q.wob:RecordNumber)
  BRW1.AddField(wob:EDI,BRW1.Q.wob:EDI)
  BRW1.AddField(wob:Manufacturer,BRW1.Q.wob:Manufacturer)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?tmp:SelectManufacturer{Prop:Checked} = True
    UNHIDE(?Manufacturer_Temp)
    UNHIDE(?LookupManufacturer)
  END
  IF ?tmp:SelectManufacturer{Prop:Checked} = False
    HIDE(?Manufacturer_Temp)
    HIDE(?LookupManufacturer)
  END
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:DISCOUNT.Close
    Relate:EDIBATCH.Close
    Relate:EXCHANGE.Close
    Relate:VATCODE.Close
    Relate:WEBJOB.Close
    !Extension Templete Code:XploreOOPBrowse,AfterFileClose
    Xplore1.EraseVisual()                             !Xplore
  END
  !Extension Templete Code:XploreOOPBrowse,BeforeWindowClosing
  IF ThisWindow.Opened                                !Xplore
    Xplore1.sq.Col = Xplore1.CurrentCol
    GET(Xplore1.sq,Xplore1.sq.Col)
    IF Xplore1.ListType = 1 AND BRW1.ViewOrder = False !Xplore
      BRW1.SequenceNbr = 0                            !Xplore
    END                                               !Xplore
    Xplore1.PutInix('ARCClaims','?Browse:1',BRW1.SequenceNbr,Xplore1.sq.AscDesc) !Xplore
  END                                                 !Xplore
  !Extension Templete Code:XploreOOPBrowse,EndOfProcedure
  Xplore1.Kill()                                      !Xplore
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    BrowseEDIManufacturers
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  Xplore1.Upper = True                                !Xplore
  PARENT.SetAlerts
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'SetAlerts',PRIORITY(8000)
  Xplore1.AddField(tmp:Tag,BRW1.Q.tmp:Tag)
  Xplore1.AddField(wob:RefNumber,BRW1.Q.wob:RefNumber)
  Xplore1.AddField(wob:JobNumber,BRW1.Q.wob:JobNumber)
  Xplore1.AddField(wob:HeadAccountNumber,BRW1.Q.wob:HeadAccountNumber)
  Xplore1.AddField(job:Ref_Number,BRW1.Q.job:Ref_Number)
  Xplore1.AddField(tmp:BranchID,BRW1.Q.tmp:BranchID)
  Xplore1.AddField(tmp:JobNumber,BRW1.Q.tmp:JobNumber)
  Xplore1.AddField(tmp:RepairedAt,BRW1.Q.tmp:RepairedAt)
  Xplore1.AddField(job:Account_Number,BRW1.Q.job:Account_Number)
  Xplore1.AddField(job:ESN,BRW1.Q.job:ESN)
  Xplore1.AddField(job:Date_Completed,BRW1.Q.job:Date_Completed)
  Xplore1.AddField(job:Model_Number,BRW1.Q.job:Model_Number)
  Xplore1.AddField(job:Warranty_Charge_Type,BRW1.Q.job:Warranty_Charge_Type)
  Xplore1.AddField(job:Repair_Type_Warranty,BRW1.Q.job:Repair_Type_Warranty)
  Xplore1.AddField(tmp:ExchangedAt,BRW1.Q.tmp:ExchangedAt)
  Xplore1.AddField(job:MSN,BRW1.Q.job:MSN)
  Xplore1.AddField(job:Order_Number,BRW1.Q.job:Order_Number)
  Xplore1.AddField(job:Fault_Code1,BRW1.Q.job:Fault_Code1)
  Xplore1.AddField(job:Fault_Code2,BRW1.Q.job:Fault_Code2)
  Xplore1.AddField(job:Fault_Code3,BRW1.Q.job:Fault_Code3)
  Xplore1.AddField(job:Fault_Code4,BRW1.Q.job:Fault_Code4)
  Xplore1.AddField(job:Fault_Code5,BRW1.Q.job:Fault_Code5)
  Xplore1.AddField(job:Fault_Code6,BRW1.Q.job:Fault_Code6)
  Xplore1.AddField(job:Fault_Code7,BRW1.Q.job:Fault_Code7)
  Xplore1.AddField(job:Fault_Code8,BRW1.Q.job:Fault_Code8)
  Xplore1.AddField(job:Fault_Code9,BRW1.Q.job:Fault_Code9)
  Xplore1.AddField(job:Fault_Code10,BRW1.Q.job:Fault_Code10)
  Xplore1.AddField(job:Fault_Code11,BRW1.Q.job:Fault_Code11)
  Xplore1.AddField(job:Fault_Code12,BRW1.Q.job:Fault_Code12)
  Xplore1.AddField(jobe:POPConfirmed,BRW1.Q.jobe:POPConfirmed)
  Xplore1.AddField(jobe:PendingClaimColour,BRW1.Q.jobe:PendingClaimColour)
  Xplore1.AddField(tmp:ResubmitFlag,BRW1.Q.tmp:ResubmitFlag)
  Xplore1.AddField(tmp:BouncerClaim,BRW1.Q.tmp:BouncerClaim)
  Xplore1.AddField(tmp:Blank,BRW1.Q.tmp:Blank)
  Xplore1.AddField(tmp:RRCClaimStatus,BRW1.Q.tmp:RRCClaimStatus)
  Xplore1.AddField(wob:RecordNumber,BRW1.Q.wob:RecordNumber)
  Xplore1.AddField(wob:EDI,BRW1.Q.wob:EDI)
  Xplore1.AddField(wob:Manufacturer,BRW1.Q.wob:Manufacturer)
  BRW1.FileOrderNbr = BRW1.AddSortOrder(,wob:HeadEDIKey) !Xplore Sort Order for File Sequence
  BRW1.AddRange(wob:EDI)                              !Xplore
  BRW1.SetOrder('')                                   !Xplore
  BRW1.ViewOrder = True                               !Xplore
  Xplore1.AddAllColumnSortOrders(1)                   !Xplore
  BRW1.ViewOrder = False                              !Xplore


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?BrowseAudit
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?BrowseAudit, Accepted)
      glo:Select12    = brw1.q.job:Ref_Number
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?BrowseAudit, Accepted)
    OF ?tmp:JobType
      !Extension Templete Code:XploreOOPBrowse,ControlEventHandling,?tmp:JobType,'NewSelection',PRIORITY(4000)
      BRW1.ViewOrder = False                          !Xplore
      BRW1.FileSeqOn = False                          !Xplore
      Xplore1.EraseVisual()                           !Xplore
      Xplore1.SetFilters()
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020541'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020541'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020541'&'0')
      ***
    OF ?ResubmitAccept
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ResubmitAccept, Accepted)
      EDI# = 0
      Case Missive('Do you wish to RESUBMIT the claim or acknowledge the REJECTion?','ServiceBase 3g',|
                     'mquest.jpg','\Cancel|Reject|Resubmit') 
          Of 3 ! Resubmit Button
              EDI# = 1
          Of 2 ! Reject Button
              EDI# = 2
          Of 1 ! Cancel Button
      End ! Case Missive
      
      If EDI#
          Loop   !makes edi_reason compulsory here
              Glo:EDI_Reason = ''
              Get_EDI_Reason   !writes to glo:EDI_Reason string 60
              if Clip(glo:edi_reason) = '' then
                  Case Missive('If you do not give a reason this process cannot be completed.'&|
                    '<13,10>'&|
                    '<13,10>Do you want to RETURN to enter a reason, or CANCEL this process?','ServiceBase 3g',|
                                 'mquest.jpg','\Cancel|/Return') 
                      Of 2 ! Return Button
                      Of 1 ! Cancel Button
                          break
                  End ! Case Missive
              ELSE
                  break
              END !If glo:edi_reason is blank
          End !loop
      
          If Clip(glo:edi_reason) <> ''
              Pending# = False
              Access:WEBJOB.Clearkey(wob:RefNumberKey)
              wob:RefNumber   = brw1.q.job:Ref_Number
              If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                  !Found
                  Access:JOBS.Clearkey(job:Ref_Number_Key)
                  job:Ref_Number  = wob:RefNumber
                  If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Found
                      Access:JOBSE.Clearkey(jobe:RefNumberKey)
                      jobe:RefNumber  = job:Ref_Number
                      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                          !Found
      
                      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                          !Error
                      End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      
                      !If job has been rejected via the OBF/2nd Year process, then
                      !jobe:OBFProcessed = 3. In this case do NOT change job:edi
                      !the job should not be visible in the normal warranty browses
                      Case EDI#
                          Of 1
                              If jobe:OBFProcessed = 3
                                  jobe:OBFProcessed = 0
                              Else !If jobe:OBFProcessed = 3
                                  job:EDI = 'NO'
                                  ! Inserting (DBH 10/04/2006) #7253 - Job will return to the pending screen
                                  Pending# = True
                                  ! End (DBH 10/04/2006) #7253
                                  job:EDI_Batch_Number = ''
                              End !If jobe:OBFProcessed = 3
                              
                              wob:EDI = 'NO'
                              jobe:WarrantyClaimStatus = 'PENDING'
                              jobe:WarrantyStatusDate = Today()
                              
                          Of 2
                              If jobe:OBFProcessed = 3
                                  jobe:OBFProcessed = 4
                              Else !If jobe:OBFProcessed = 3
                                  job:EDI = 'REJ'
                              End !If jobe:OBFProcessed = 3
                              
                              wob:EDI = 'AAJ'
                              jobe:WarrantyClaimStatus = 'FINAL REJECTION'
                              jobe:WarrantyStatusDate = Today()
      
                      End !Case EDI#
      ! Changing (DBH 10/04/2006) #7252 - Reorder to make sure the job is updated
      !                 Access:WEBJOB.Update()
      !                 Access:JOBSE.Update()
      !
      !                 If access:jobs.update() = Level:Benign
      ! to (DBH 10/04/2006) #7252
      
                      If access:jobs.update() = Level:Benign
                          Access:WEBJOB.Update()
                          Access:JOBSE.Update()
                          ! Inserting (DBH 10/04/2006) #7252 - UPdate the "In pending" date
                          If Pending# And job:EDI = 'NO'
                              Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
                              jobe2:RefNumber = job:Ref_Number
                              If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
                                  ! Found
                                  jobe2:InPendingDate = Today()
                                  Access:JOBSE2.TryUpdate()
                              Else ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
                                  ! Error
                                  If Access:JOBSE2.PrimeRecord() = Level:Benign
                                      jobe2:RefNumber = job:Ref_Number
                                      jobe2:InPendingDate = Today()
                                      If Access:JOBSE2.TryInsert() = Level:Benign
                                          ! Insert Successful
      
                                      Else ! If Access:JOBSE2.TryInsert() = Level:Benign
                                          ! Insert Failed
                                          Access:JOBSE2.CancelAutoInc()
                                      End ! If Access:JOBSE2.TryInsert() = Level:Benign
                                  End !If Access:JOBSE2.PrimeRecord() = Level:Benign
                              End ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
                          End ! If Pending# And job:EDI = 'NO'
                          ! End (DBH 10/04/2006) #7252
                      ! End (DBH 10/04/2006) #7252
                          Case EDI#
                              Of 1
                                  If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM RESUBMITTED','REASON: ' & CLip(Glo:EDI_Reason))
      
                                  End ! If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM RESUBMITTED','REASON: ' & CLip(Glo:EDI_Reason))
                              Of 2
                                  If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM REJECTION ACKNOWLEDGED','REASON: ' & CLip(Glo:EDI_Reason))
      
                                  End ! If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM RESUBMITTED','REASON: ' & CLip(Glo:EDI_Reason))
                          End !Case EDI#
                      End !If access:jobs.update() = Level:Benign
                      access:jobnotes.clearkey(jbn:RefNumberKey)
      
                  Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Error
                  End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      
              Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                  !Error
              End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
          END !If glo:edi reason is not blank
      End !EDI#
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ResubmitAccept, Accepted)
    OF ?Blue
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Blue, Accepted)
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber = brw1.q.job:Ref_Number
      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          !Found
          If jobe:PendingClaimColour = 2
              jobe:PendingClaimColour = 0
          Else !If jobe:ClaimColour = 1
              jobe:PendingClaimColour = 2
          End !If jobe:ClaimColour = 1
          Access:JOBSE.Update()
      End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      Brw1.ResetQueue(Reset:Queue)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Blue, Accepted)
    OF ?Red
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Red, Accepted)
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber = brw1.q.job:Ref_Number
      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          !Found
          If jobe:PendingClaimColour = 3
              jobe:PendingClaimColour = 0
          Else !If jobe:ClaimColour = 1
              jobe:PendingClaimColour = 3
          End !If jobe:ClaimColour = 1
          Access:JOBSE.Update()
      End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      Brw1.ResetQueue(Reset:Queue)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Red, Accepted)
    OF ?Green
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Green, Accepted)
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber = brw1.q.job:Ref_Number
      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          !Found
          If jobe:PendingClaimColour = 1
              jobe:PendingClaimColour = 0
          Else !If jobe:ClaimColour = 1
              jobe:PendingClaimColour = 1
          End !If jobe:ClaimColour = 1
          Access:JOBSE.Update()
      End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      Brw1.ResetQueue(Reset:Queue)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Green, Accepted)
    OF ?BrowseAudit
      ThisWindow.Update
      Browse_Audit
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?BrowseAudit, Accepted)
      glo:Select12 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?BrowseAudit, Accepted)
    OF ?View
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?View, Accepted)
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number  = brw1.q.wob:RefNumber
      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Found
          GlobalRequest = ChangeRecord
          Update_Jobs_Rapid
      Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Error
      End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?View, Accepted)
    OF ?Valudate_Claims
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Valudate_Claims, Accepted)
      !Show the correct description in the message box - 3822 (DBH: 21-01-2004)
      Case tmp:JobType
          Of 'NO'
              MessageText" = 'Pending Claims'
          Of 'YES'
              MessageText" = 'In Process Claims'
          Of 'AAJ'
              MessageText" = 'Accepted Rejected Claims'
          Of 'APP'
              MessageText" = 'Approved Claims'
          Of 'EXC'
              MessageText" = 'Rejected Claims'
          Else
              MessageText" = 'Claims'
      End !tmp:JobType
      
      
      Case Missive('This will valuate the ' & Clip(MessageText") & '.'&|
        '<13,10>'&|
        '<13,10>This may take a long time depending on the amount of claims.'&|
        '<13,10>Are you sure?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes') 
          Of 2 ! Yes Button
          
      
              Prog.ProgressSetup(1000)
      
              claim_value$ = 0
      
              Save_wob_ID = Access:WEBJOB.SaveFile()
              Access:WEBJOB.ClearKey(wob:HeadEDIKey)
              wob:HeadAccountNumber = tmp:ARCLocation
              wob:EDI               = tmp:JobType
              If tmp:SelectManufacturer
                  wob:Manufacturer      = Manufacturer_Temp
              End !If tmp:SelectManufacturer
              Set(wob:HeadEDIKey,wob:HeadEDIKey)
              Loop
                  If Access:WEBJOB.NEXT()
                     Break
                  End !If
                  If wob:HeadAccountNumber <> tmp:ARCLocation      |
                  Or wob:EDI               <> tmp:JobType
                      Break
                  End
                  If tmp:SelectManufacturer
                      If wob:Manufacturer <> Manufacturer_Temp
                          Break
                      End !If wob:Manufacturer <> Manufacturer_Temp
                  End !If tmp:SelectManufacturer
      
                  If Prog.InsideLoop()
                      Break
                  End !If Prog.InsideLoop()
      
                  Access:JOBS.Clearkey(job:Ref_Number_Key)
                  job:Ref_Number  = wob:RefNumber
                  If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Found
      
                  Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Error
                  End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      
                  Access:MANUFACT.Clearkey(man:Manufacturer_Key)
                  man:Manufacturer    = job:Manufacturer
                  If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
                      !Found
      
                  Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
                      !Error
                  End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
      
                  !Do not reprice - L945 (DBH: 03-09-2003)
      !            JobPricingRoutine
      !
      !            Access:JOBS.Update()
      !            Access:JOBSE.Update()
          
                  count# += 1
                  If GETINI('STOCK','UseVirtualPrice',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                      if jobe:ConfirmClaimAdjustment
                          claim_value$ += Round((Round(jobe:SubTotalAdjustment,.01) + Round(job:courier_cost_warranty,.01)),.01)
                      else
                          claim_value$ += Round(Round(jobe:ClaimValue,.01) + Round(job:parts_cost_warranty,.01) + Round(job:courier_cost_warranty,.01),.01)
                      end
                  Else !If GETINI('STOCK','UseVirtualPrice',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                      if jobe:ConfirmClaimAdjustment
                          claim_value$ += Round((Round(jobe:LabourAdjustment,.01) + Round(jobe:PartsAdjustment,.01) + Round(job:courier_cost_warranty,.01)),.01)
                      else
                          claim_value$ += Round((Round(job:labour_cost_warranty,.01) + Round(job:parts_cost_warranty,.01) + Round(job:courier_cost_warranty,.01)),.01)
                      end
                  End !If GETINI('STOCK','UseVirtualPrice',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      
              End !Loop
              Access:WEBJOB.RestoreFile(Save_wob_ID)
      
              Prog.ProgressFinish()
      
              Case Missive('Number of claims: ' & Count# & '.'&|
                '<13,10>'&|
                '<13,10>Value of claims: ' & Format(Claim_Value$,@n14.2) & '.','ServiceBase 3g',|
                             'midea.jpg','/OK') 
                  Of 1 ! OK Button
              End ! Case Missive
          Of 1 ! No Button
      End ! Case Missive
      BRW1.ResetSort(1)
      Select(?Browse:1)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Valudate_Claims, Accepted)
    OF ?Manufacturer_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Manufacturer_Temp, Accepted)
      BRW1.ResetSort(1)
      Post(Event:Accepted,?tmp:JobType)
      IF Manufacturer_Temp OR ?Manufacturer_Temp{Prop:Req}
        man:Manufacturer = Manufacturer_Temp
        !Save Lookup Field Incase Of error
        look:Manufacturer_Temp        = Manufacturer_Temp
        IF Access:MANUFACT.TryFetch(man:Manufacturer_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            Manufacturer_Temp = man:Manufacturer
          ELSE
            !Restore Lookup On Error
            Manufacturer_Temp = look:Manufacturer_Temp
            SELECT(?Manufacturer_Temp)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Manufacturer_Temp, Accepted)
    OF ?LookupManufacturer
      ThisWindow.Update
      man:Manufacturer = Manufacturer_Temp
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          Manufacturer_Temp = man:Manufacturer
          Select(?+1)
      ELSE
          Select(?Manufacturer_Temp)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?Manufacturer_Temp)
    OF ?tmp:JobType
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:JobType, Accepted)
      BRW1.ResetSort(1)
      If tmp:JobType = 'EXC'
          ?ResubmitAccept{prop:Hide} = 0
      Else
          ?ResubmitAccept{prop:Hide} = 1
      End !tmp:JobType = 'EXC'
      !ThisMakeOver.Refresh()
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:JobType, Accepted)
    OF ?tmp:SelectManufacturer
      IF ?tmp:SelectManufacturer{Prop:Checked} = True
        UNHIDE(?Manufacturer_Temp)
        UNHIDE(?LookupManufacturer)
      END
      IF ?tmp:SelectManufacturer{Prop:Checked} = False
        HIDE(?Manufacturer_Temp)
        HIDE(?LookupManufacturer)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SelectManufacturer, Accepted)
      BRW1.ResetSort(1)
      Post(Event:Accepted,?tmp:JobType)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SelectManufacturer, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'TakeEvent'
  IF Xplore1.IgnoreEvent = True                       !Xplore
     Xplore1.IgnoreEvent = False                      !Xplore
     CYCLE                                            !Xplore
  END                                                 !Xplore
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:PreAlertKey
      IF KEYCODE() = MouseRight                       !Xplore
         SETKEYCODE(0)                                !Xplore
         CYCLE                                        !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
      If KeyCode() = MouseLeft2 Or KeyCode() = EnterKey
          Post(Event:Accepted,?View)
      End !KeyCode() = MouseLeft2 Or KeyCode() = EnterKey
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CurrentTab, NewSelection)
      If Choice(?CurrentTab) <> 1
          ?ResubmitAccept{prop:Hide} = 0
      Else !Choice(?CurrentTab) <> 1
          ?ResubmitAccept{prop:Hide} = 1
      End !Choice(?CurrentTab) <> 1
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CurrentTab, NewSelection)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Post(Event:Accepted,?tmp:JobType)
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'OpenWindow',PRIORITY(8000)
      POST(xpEVENT:Xplore + 1)                        !Xplore
      ALERT(AltF12)                                   !Xplore
      ALERT(AltF11)                                   !Xplore
      ALERT(AltR)                                     !Xplore
      ALERT(AltM)                                     !Xplore
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'GainFocus',PRIORITY(8000)
      Xplore1.GetColumnInfo()                         !Xplore
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
Local.FinalRejection      Procedure(Byte    func:Type)
Code
    Access:WEBJOB.Clearkey(wob:RefNumberKey)
    wob:RefNumber   = job:Ref_Number
    If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        !Found

    Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        !Error
    End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign

    If func:Type
        !About the change a key element
        pos = Position(job:EDI_Key)
    End !If func:Type

    job:edi_batch_number = ''
    job:edi = 'REJ'   !Accepted as rejected
    wob:edi = 'REJ'
    wob:OracleExportNumber = 0
    access:webjob.update()
    If Sub(job:Current_Status,1,3) = '905'
       GetStatus(910,0,'JOB')
    End !If Sub(job:Current_Status) = '905'
    If access:jobs.update() = Level:Benign

        If func:Type
            !Reset after changing a key element
            Reset(job:EDI_Key,pos)
        End !If func:Type

        If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM FINAL REJECTION','REASON: ' & Clip(glo:EDI_Reason))

        End ! If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM FINAL REJECTION','REASON: ' & Clip(glo:EDI_Reason))
    End !If access:jobs.update() = Level:Benign
    access:jobnotes.clearkey(jbn:RefNumberKey)
    jbn:RefNumber = WOB:REFnUMBER
    if access:jobnotes.tryfetch(jbn:RefNumberKey)
        jbn:Engineers_Notes = jbn:Engineers_Notes &'<13,10>'& Clip(Glo:EDI_Reason)
        access:jobnotes.update()
    END !if jobnotes.tryfetch
    !Write the new warranty status to the file
    Access:JOBSE.Clearkey(jobe:RefNumberKey)
    jobe:RefNumber  = job:Ref_Number
    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
       !Found
       jobe:WarrantyClaimStatus = 'FINAL REJECTION'
       jobe:WarrantyStatusDate  = Today()
       Access:JOBSE.Update()
    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
       !Error
    End !If Access:JOBSE.Tryfetch(jobe:RefNumberKe
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Upper(tmp:SelectManufacturer) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:ARCLocation
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:JobType
  ELSIF Upper(tmp:SelectManufacturer) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:ARCLocation
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:JobType
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = Manufacturer_Temp
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:ARCLocation
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:JobType
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.GetFreeElementName PROCEDURE

ReturnValue          ANY

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'GetFreeElementName'
  IF BRW1.ViewOrder = True                            !Xplore
     RETURN('UPPER(' & CLIP(BRW1.LocatorField) & ')') !Xplore
  END
  ReturnValue = PARENT.GetFreeElementName()
  RETURN ReturnValue


BRW1.GetFreeElementPosition PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'GetFreeElementPosition'
  IF BRW1.ViewOrder = True                            !Xplore
     RETURN(1)                                        !Xplore
  END
  ReturnValue = PARENT.GetFreeElementPosition()
  RETURN ReturnValue


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeControlRefresh,1
   COMPILE('***NotC4***',_VER_C5)
  IF EVENT()>=XpEvent:AddFilter1 AND EVENT()<=XpEvent:VoidFilter
     IF Xplore1.FilterShared OR EVENT()=XpEvent:VoidFilter
        CS# = POINTER(SELF.Sort)
        LOOP I# = 1 TO RECORDS(SELF.Sort)
             PARENT.SetSort(I#)
             SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
        END
        PARENT.SetSort(CS#)
     ELSE
        SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
     END
  END
   !***NotC4***
  IF BRW1.ViewOrder = True                            !Xplore
    SavePtr# = POINTER(Xplore1.sq)                    !Xplore
    Xplore1.SetupOrder(BRW1.SortOrderNbr)             !Xplore
    GET(Xplore1.sq,SavePtr#)                          !Xplore
    SETCURSOR(Cursor:Wait)
    R# = SELF.SetSort(BRW1.SortOrderNbr,Force)        !Xplore
    SETCURSOR()
    RETURN R#                                         !Xplore
  ELSIF BRW1.FileSeqOn = True                         !Xplore
    RETURN SELF.SetSort(BRW1.FileOrderNbr,Force)      !Xplore
  END                                                 !Xplore
  IF Upper(tmp:SelectManufacturer) = 0
    RETURN SELF.SetSort(1,Force)
  ELSIF Upper(tmp:SelectManufacturer) = 1
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  Charge_Repair_Type_Temp = CLIP(job:Warranty_Charge_Type) & ' / ' & CLIP(job:Repair_Type_Warranty)
  PARENT.SetQueueRecord
  SELF.Q.tmp:Tag_NormalFG = -1
  SELF.Q.tmp:Tag_NormalBG = -1
  SELF.Q.tmp:Tag_SelectedFG = -1
  SELF.Q.tmp:Tag_SelectedBG = -1
  IF (tmp:Tag = '*')
    SELF.Q.tmp:Tag_Icon = 3
  ELSE
    SELF.Q.tmp:Tag_Icon = 2
  END
  SELF.Q.wob:RefNumber_NormalFG = -1
  SELF.Q.wob:RefNumber_NormalBG = -1
  SELF.Q.wob:RefNumber_SelectedFG = -1
  SELF.Q.wob:RefNumber_SelectedBG = -1
  SELF.Q.wob:JobNumber_NormalFG = -1
  SELF.Q.wob:JobNumber_NormalBG = -1
  SELF.Q.wob:JobNumber_SelectedFG = -1
  SELF.Q.wob:JobNumber_SelectedBG = -1
  SELF.Q.wob:HeadAccountNumber_NormalFG = -1
  SELF.Q.wob:HeadAccountNumber_NormalBG = -1
  SELF.Q.wob:HeadAccountNumber_SelectedFG = -1
  SELF.Q.wob:HeadAccountNumber_SelectedBG = -1
  SELF.Q.job:Ref_Number_NormalFG = -1
  SELF.Q.job:Ref_Number_NormalBG = -1
  SELF.Q.job:Ref_Number_SelectedFG = -1
  SELF.Q.job:Ref_Number_SelectedBG = -1
  SELF.Q.tmp:BranchID_NormalFG = -1
  SELF.Q.tmp:BranchID_NormalBG = -1
  SELF.Q.tmp:BranchID_SelectedFG = -1
  SELF.Q.tmp:BranchID_SelectedBG = -1
  SELF.Q.tmp:JobNumber_NormalFG = -1
  SELF.Q.tmp:JobNumber_NormalBG = -1
  SELF.Q.tmp:JobNumber_SelectedFG = -1
  SELF.Q.tmp:JobNumber_SelectedBG = -1
  SELF.Q.tmp:RepairedAt_NormalFG = -1
  SELF.Q.tmp:RepairedAt_NormalBG = -1
  SELF.Q.tmp:RepairedAt_SelectedFG = -1
  SELF.Q.tmp:RepairedAt_SelectedBG = -1
  SELF.Q.job:Account_Number_NormalFG = -1
  SELF.Q.job:Account_Number_NormalBG = -1
  SELF.Q.job:Account_Number_SelectedFG = -1
  SELF.Q.job:Account_Number_SelectedBG = -1
  SELF.Q.job:ESN_NormalFG = -1
  SELF.Q.job:ESN_NormalBG = -1
  SELF.Q.job:ESN_SelectedFG = -1
  SELF.Q.job:ESN_SelectedBG = -1
  SELF.Q.job:Date_Completed_NormalFG = -1
  SELF.Q.job:Date_Completed_NormalBG = -1
  SELF.Q.job:Date_Completed_SelectedFG = -1
  SELF.Q.job:Date_Completed_SelectedBG = -1
  SELF.Q.job:Model_Number_NormalFG = -1
  SELF.Q.job:Model_Number_NormalBG = -1
  SELF.Q.job:Model_Number_SelectedFG = -1
  SELF.Q.job:Model_Number_SelectedBG = -1
  SELF.Q.job:Warranty_Charge_Type_NormalFG = -1
  SELF.Q.job:Warranty_Charge_Type_NormalBG = -1
  SELF.Q.job:Warranty_Charge_Type_SelectedFG = -1
  SELF.Q.job:Warranty_Charge_Type_SelectedBG = -1
  SELF.Q.job:Repair_Type_Warranty_NormalFG = -1
  SELF.Q.job:Repair_Type_Warranty_NormalBG = -1
  SELF.Q.job:Repair_Type_Warranty_SelectedFG = -1
  SELF.Q.job:Repair_Type_Warranty_SelectedBG = -1
  SELF.Q.tmp:ExchangedAt_NormalFG = -1
  SELF.Q.tmp:ExchangedAt_NormalBG = -1
  SELF.Q.tmp:ExchangedAt_SelectedFG = -1
  SELF.Q.tmp:ExchangedAt_SelectedBG = -1
  SELF.Q.job:MSN_NormalFG = -1
  SELF.Q.job:MSN_NormalBG = -1
  SELF.Q.job:MSN_SelectedFG = -1
  SELF.Q.job:MSN_SelectedBG = -1
  SELF.Q.job:Order_Number_NormalFG = -1
  SELF.Q.job:Order_Number_NormalBG = -1
  SELF.Q.job:Order_Number_SelectedFG = -1
  SELF.Q.job:Order_Number_SelectedBG = -1
  SELF.Q.job:Fault_Code1_NormalFG = -1
  SELF.Q.job:Fault_Code1_NormalBG = -1
  SELF.Q.job:Fault_Code1_SelectedFG = -1
  SELF.Q.job:Fault_Code1_SelectedBG = -1
  SELF.Q.job:Fault_Code2_NormalFG = -1
  SELF.Q.job:Fault_Code2_NormalBG = -1
  SELF.Q.job:Fault_Code2_SelectedFG = -1
  SELF.Q.job:Fault_Code2_SelectedBG = -1
  SELF.Q.job:Fault_Code3_NormalFG = -1
  SELF.Q.job:Fault_Code3_NormalBG = -1
  SELF.Q.job:Fault_Code3_SelectedFG = -1
  SELF.Q.job:Fault_Code3_SelectedBG = -1
  SELF.Q.job:Fault_Code4_NormalFG = -1
  SELF.Q.job:Fault_Code4_NormalBG = -1
  SELF.Q.job:Fault_Code4_SelectedFG = -1
  SELF.Q.job:Fault_Code4_SelectedBG = -1
  SELF.Q.job:Fault_Code5_NormalFG = -1
  SELF.Q.job:Fault_Code5_NormalBG = -1
  SELF.Q.job:Fault_Code5_SelectedFG = -1
  SELF.Q.job:Fault_Code5_SelectedBG = -1
  SELF.Q.job:Fault_Code6_NormalFG = -1
  SELF.Q.job:Fault_Code6_NormalBG = -1
  SELF.Q.job:Fault_Code6_SelectedFG = -1
  SELF.Q.job:Fault_Code6_SelectedBG = -1
  SELF.Q.job:Fault_Code7_NormalFG = -1
  SELF.Q.job:Fault_Code7_NormalBG = -1
  SELF.Q.job:Fault_Code7_SelectedFG = -1
  SELF.Q.job:Fault_Code7_SelectedBG = -1
  SELF.Q.job:Fault_Code8_NormalFG = -1
  SELF.Q.job:Fault_Code8_NormalBG = -1
  SELF.Q.job:Fault_Code8_SelectedFG = -1
  SELF.Q.job:Fault_Code8_SelectedBG = -1
  SELF.Q.job:Fault_Code9_NormalFG = -1
  SELF.Q.job:Fault_Code9_NormalBG = -1
  SELF.Q.job:Fault_Code9_SelectedFG = -1
  SELF.Q.job:Fault_Code9_SelectedBG = -1
  SELF.Q.job:Fault_Code10_NormalFG = -1
  SELF.Q.job:Fault_Code10_NormalBG = -1
  SELF.Q.job:Fault_Code10_SelectedFG = -1
  SELF.Q.job:Fault_Code10_SelectedBG = -1
  SELF.Q.job:Fault_Code11_NormalFG = -1
  SELF.Q.job:Fault_Code11_NormalBG = -1
  SELF.Q.job:Fault_Code11_SelectedFG = -1
  SELF.Q.job:Fault_Code11_SelectedBG = -1
  SELF.Q.job:Fault_Code12_NormalFG = -1
  SELF.Q.job:Fault_Code12_NormalBG = -1
  SELF.Q.job:Fault_Code12_SelectedFG = -1
  SELF.Q.job:Fault_Code12_SelectedBG = -1
  SELF.Q.jobe:POPConfirmed_NormalFG = -1
  SELF.Q.jobe:POPConfirmed_NormalBG = -1
  SELF.Q.jobe:POPConfirmed_SelectedFG = -1
  SELF.Q.jobe:POPConfirmed_SelectedBG = -1
  SELF.Q.jobe:PendingClaimColour_NormalFG = -1
  SELF.Q.jobe:PendingClaimColour_NormalBG = -1
  SELF.Q.jobe:PendingClaimColour_SelectedFG = -1
  SELF.Q.jobe:PendingClaimColour_SelectedBG = -1
  SELF.Q.tmp:ResubmitFlag_NormalFG = -1
  SELF.Q.tmp:ResubmitFlag_NormalBG = -1
  SELF.Q.tmp:ResubmitFlag_SelectedFG = -1
  SELF.Q.tmp:ResubmitFlag_SelectedBG = -1
  SELF.Q.tmp:BouncerClaim_NormalFG = -1
  SELF.Q.tmp:BouncerClaim_NormalBG = -1
  SELF.Q.tmp:BouncerClaim_SelectedFG = -1
  SELF.Q.tmp:BouncerClaim_SelectedBG = -1
  IF (tmp:BouncerClaim = 1)
    SELF.Q.tmp:BouncerClaim_Icon = 1
  ELSE
    SELF.Q.tmp:BouncerClaim_Icon = 2
  END
  SELF.Q.tmp:Blank_NormalFG = -1
  SELF.Q.tmp:Blank_NormalBG = -1
  SELF.Q.tmp:Blank_SelectedFG = -1
  SELF.Q.tmp:Blank_SelectedBG = -1
   
   
   IF (jobe:PendingClaimColour = 1)
     SELF.Q.tmp:Tag_NormalFG = 32768
     SELF.Q.tmp:Tag_NormalBG = 16777215
     SELF.Q.tmp:Tag_SelectedFG = 16777215
     SELF.Q.tmp:Tag_SelectedBG = 32768
   ELSIF(jobe:PendingClaimColour = 2)
     SELF.Q.tmp:Tag_NormalFG = 16711680
     SELF.Q.tmp:Tag_NormalBG = 16777215
     SELF.Q.tmp:Tag_SelectedFG = 16777215
     SELF.Q.tmp:Tag_SelectedBG = 16711680
   ELSIF(jobe:PendingClaimColour = 3)
     SELF.Q.tmp:Tag_NormalFG = 255
     SELF.Q.tmp:Tag_NormalBG = 16777215
     SELF.Q.tmp:Tag_SelectedFG = 16777215
     SELF.Q.tmp:Tag_SelectedBG = 255
   ELSE
     SELF.Q.tmp:Tag_NormalFG = 0
     SELF.Q.tmp:Tag_NormalBG = 16777215
     SELF.Q.tmp:Tag_SelectedFG = 16777215
     SELF.Q.tmp:Tag_SelectedBG = 0
   END
   IF (jobe:PendingClaimColour = 1)
     SELF.Q.wob:RefNumber_NormalFG = 32768
     SELF.Q.wob:RefNumber_NormalBG = 16777215
     SELF.Q.wob:RefNumber_SelectedFG = 16777215
     SELF.Q.wob:RefNumber_SelectedBG = 32768
   ELSIF(jobe:PendingClaimColour = 2)
     SELF.Q.wob:RefNumber_NormalFG = 16711680
     SELF.Q.wob:RefNumber_NormalBG = 16777215
     SELF.Q.wob:RefNumber_SelectedFG = 16777215
     SELF.Q.wob:RefNumber_SelectedBG = 16711680
   ELSIF(jobe:PendingClaimColour = 3)
     SELF.Q.wob:RefNumber_NormalFG = 255
     SELF.Q.wob:RefNumber_NormalBG = 16777215
     SELF.Q.wob:RefNumber_SelectedFG = 16777215
     SELF.Q.wob:RefNumber_SelectedBG = 255
   ELSE
     SELF.Q.wob:RefNumber_NormalFG = 0
     SELF.Q.wob:RefNumber_NormalBG = 16777215
     SELF.Q.wob:RefNumber_SelectedFG = 16777215
     SELF.Q.wob:RefNumber_SelectedBG = 0
   END
   IF (jobe:PendingClaimColour = 1)
     SELF.Q.wob:JobNumber_NormalFG = 32768
     SELF.Q.wob:JobNumber_NormalBG = 16777215
     SELF.Q.wob:JobNumber_SelectedFG = 16777215
     SELF.Q.wob:JobNumber_SelectedBG = 32768
   ELSIF(jobe:PendingClaimColour = 2)
     SELF.Q.wob:JobNumber_NormalFG = 16711680
     SELF.Q.wob:JobNumber_NormalBG = 16777215
     SELF.Q.wob:JobNumber_SelectedFG = 16777215
     SELF.Q.wob:JobNumber_SelectedBG = 16711680
   ELSIF(jobe:PendingClaimColour = 3)
     SELF.Q.wob:JobNumber_NormalFG = 255
     SELF.Q.wob:JobNumber_NormalBG = 16777215
     SELF.Q.wob:JobNumber_SelectedFG = 16777215
     SELF.Q.wob:JobNumber_SelectedBG = 255
   ELSE
     SELF.Q.wob:JobNumber_NormalFG = 0
     SELF.Q.wob:JobNumber_NormalBG = 16777215
     SELF.Q.wob:JobNumber_SelectedFG = 16777215
     SELF.Q.wob:JobNumber_SelectedBG = 0
   END
   IF (jobe:PendingClaimColour = 1)
     SELF.Q.wob:HeadAccountNumber_NormalFG = 32768
     SELF.Q.wob:HeadAccountNumber_NormalBG = 16777215
     SELF.Q.wob:HeadAccountNumber_SelectedFG = 16777215
     SELF.Q.wob:HeadAccountNumber_SelectedBG = 32768
   ELSIF(jobe:PendingClaimColour = 2)
     SELF.Q.wob:HeadAccountNumber_NormalFG = 16711680
     SELF.Q.wob:HeadAccountNumber_NormalBG = 16777215
     SELF.Q.wob:HeadAccountNumber_SelectedFG = 16777215
     SELF.Q.wob:HeadAccountNumber_SelectedBG = 16711680
   ELSIF(jobe:PendingClaimColour = 3)
     SELF.Q.wob:HeadAccountNumber_NormalFG = 255
     SELF.Q.wob:HeadAccountNumber_NormalBG = 16777215
     SELF.Q.wob:HeadAccountNumber_SelectedFG = 16777215
     SELF.Q.wob:HeadAccountNumber_SelectedBG = 255
   ELSE
     SELF.Q.wob:HeadAccountNumber_NormalFG = 0
     SELF.Q.wob:HeadAccountNumber_NormalBG = 16777215
     SELF.Q.wob:HeadAccountNumber_SelectedFG = 16777215
     SELF.Q.wob:HeadAccountNumber_SelectedBG = 0
   END
   IF (jobe:PendingClaimColour = 1)
     SELF.Q.job:Ref_Number_NormalFG = 32768
     SELF.Q.job:Ref_Number_NormalBG = 16777215
     SELF.Q.job:Ref_Number_SelectedFG = 16777215
     SELF.Q.job:Ref_Number_SelectedBG = 32768
   ELSIF(jobe:PendingClaimColour = 2)
     SELF.Q.job:Ref_Number_NormalFG = 16711680
     SELF.Q.job:Ref_Number_NormalBG = 16777215
     SELF.Q.job:Ref_Number_SelectedFG = 16777215
     SELF.Q.job:Ref_Number_SelectedBG = 16711680
   ELSIF(jobe:PendingClaimColour = 3)
     SELF.Q.job:Ref_Number_NormalFG = 255
     SELF.Q.job:Ref_Number_NormalBG = 16777215
     SELF.Q.job:Ref_Number_SelectedFG = 16777215
     SELF.Q.job:Ref_Number_SelectedBG = 255
   ELSE
     SELF.Q.job:Ref_Number_NormalFG = 0
     SELF.Q.job:Ref_Number_NormalBG = 16777215
     SELF.Q.job:Ref_Number_SelectedFG = 16777215
     SELF.Q.job:Ref_Number_SelectedBG = 0
   END
   IF (jobe:PendingClaimColour = 1)
     SELF.Q.tmp:BranchID_NormalFG = 32768
     SELF.Q.tmp:BranchID_NormalBG = 16777215
     SELF.Q.tmp:BranchID_SelectedFG = 16777215
     SELF.Q.tmp:BranchID_SelectedBG = 32768
   ELSIF(jobe:PendingClaimColour = 2)
     SELF.Q.tmp:BranchID_NormalFG = 16711680
     SELF.Q.tmp:BranchID_NormalBG = 16777215
     SELF.Q.tmp:BranchID_SelectedFG = 16777215
     SELF.Q.tmp:BranchID_SelectedBG = 16711680
   ELSIF(jobe:PendingClaimColour = 3)
     SELF.Q.tmp:BranchID_NormalFG = 255
     SELF.Q.tmp:BranchID_NormalBG = 16777215
     SELF.Q.tmp:BranchID_SelectedFG = 16777215
     SELF.Q.tmp:BranchID_SelectedBG = 255
   ELSE
     SELF.Q.tmp:BranchID_NormalFG = 0
     SELF.Q.tmp:BranchID_NormalBG = 16777215
     SELF.Q.tmp:BranchID_SelectedFG = 16777215
     SELF.Q.tmp:BranchID_SelectedBG = 0
   END
   IF (jobe:PendingClaimColour = 1)
     SELF.Q.tmp:JobNumber_NormalFG = 32768
     SELF.Q.tmp:JobNumber_NormalBG = 16777215
     SELF.Q.tmp:JobNumber_SelectedFG = 16777215
     SELF.Q.tmp:JobNumber_SelectedBG = 32768
   ELSIF(jobe:PendingClaimColour = 2)
     SELF.Q.tmp:JobNumber_NormalFG = 16711680
     SELF.Q.tmp:JobNumber_NormalBG = 16777215
     SELF.Q.tmp:JobNumber_SelectedFG = 16777215
     SELF.Q.tmp:JobNumber_SelectedBG = 16711680
   ELSIF(jobe:PendingClaimColour = 3)
     SELF.Q.tmp:JobNumber_NormalFG = 255
     SELF.Q.tmp:JobNumber_NormalBG = 16777215
     SELF.Q.tmp:JobNumber_SelectedFG = 16777215
     SELF.Q.tmp:JobNumber_SelectedBG = 255
   ELSE
     SELF.Q.tmp:JobNumber_NormalFG = 0
     SELF.Q.tmp:JobNumber_NormalBG = 16777215
     SELF.Q.tmp:JobNumber_SelectedFG = 16777215
     SELF.Q.tmp:JobNumber_SelectedBG = 0
   END
   IF (jobe:PendingClaimColour = 1)
     SELF.Q.tmp:RepairedAt_NormalFG = 32768
     SELF.Q.tmp:RepairedAt_NormalBG = 16777215
     SELF.Q.tmp:RepairedAt_SelectedFG = 16777215
     SELF.Q.tmp:RepairedAt_SelectedBG = 32768
   ELSIF(jobe:PendingClaimColour = 2)
     SELF.Q.tmp:RepairedAt_NormalFG = 16711680
     SELF.Q.tmp:RepairedAt_NormalBG = 16777215
     SELF.Q.tmp:RepairedAt_SelectedFG = 16777215
     SELF.Q.tmp:RepairedAt_SelectedBG = 16711680
   ELSIF(jobe:PendingClaimColour = 3)
     SELF.Q.tmp:RepairedAt_NormalFG = 255
     SELF.Q.tmp:RepairedAt_NormalBG = 16777215
     SELF.Q.tmp:RepairedAt_SelectedFG = 16777215
     SELF.Q.tmp:RepairedAt_SelectedBG = 255
   ELSE
     SELF.Q.tmp:RepairedAt_NormalFG = 0
     SELF.Q.tmp:RepairedAt_NormalBG = 16777215
     SELF.Q.tmp:RepairedAt_SelectedFG = 16777215
     SELF.Q.tmp:RepairedAt_SelectedBG = 0
   END
   IF (jobe:PendingClaimColour = 1)
     SELF.Q.job:Account_Number_NormalFG = 32768
     SELF.Q.job:Account_Number_NormalBG = 16777215
     SELF.Q.job:Account_Number_SelectedFG = 16777215
     SELF.Q.job:Account_Number_SelectedBG = 32768
   ELSIF(jobe:PendingClaimColour = 2)
     SELF.Q.job:Account_Number_NormalFG = 16711680
     SELF.Q.job:Account_Number_NormalBG = 16777215
     SELF.Q.job:Account_Number_SelectedFG = 16777215
     SELF.Q.job:Account_Number_SelectedBG = 16711680
   ELSIF(jobe:PendingClaimColour = 3)
     SELF.Q.job:Account_Number_NormalFG = 255
     SELF.Q.job:Account_Number_NormalBG = 16777215
     SELF.Q.job:Account_Number_SelectedFG = 16777215
     SELF.Q.job:Account_Number_SelectedBG = 255
   ELSE
     SELF.Q.job:Account_Number_NormalFG = 0
     SELF.Q.job:Account_Number_NormalBG = 16777215
     SELF.Q.job:Account_Number_SelectedFG = 16777215
     SELF.Q.job:Account_Number_SelectedBG = 0
   END
   IF (jobe:PendingClaimColour = 1)
     SELF.Q.job:ESN_NormalFG = 32768
     SELF.Q.job:ESN_NormalBG = 16777215
     SELF.Q.job:ESN_SelectedFG = 16777215
     SELF.Q.job:ESN_SelectedBG = 32768
   ELSIF(jobe:PendingClaimColour = 2)
     SELF.Q.job:ESN_NormalFG = 16711680
     SELF.Q.job:ESN_NormalBG = 16777215
     SELF.Q.job:ESN_SelectedFG = 16777215
     SELF.Q.job:ESN_SelectedBG = 16711680
   ELSIF(jobe:PendingClaimColour = 3)
     SELF.Q.job:ESN_NormalFG = 255
     SELF.Q.job:ESN_NormalBG = 16777215
     SELF.Q.job:ESN_SelectedFG = 16777215
     SELF.Q.job:ESN_SelectedBG = 255
   ELSE
     SELF.Q.job:ESN_NormalFG = 0
     SELF.Q.job:ESN_NormalBG = 16777215
     SELF.Q.job:ESN_SelectedFG = 16777215
     SELF.Q.job:ESN_SelectedBG = 0
   END
   IF (jobe:PendingClaimColour = 1)
     SELF.Q.job:Date_Completed_NormalFG = 32768
     SELF.Q.job:Date_Completed_NormalBG = 16777215
     SELF.Q.job:Date_Completed_SelectedFG = 16777215
     SELF.Q.job:Date_Completed_SelectedBG = 32768
   ELSIF(jobe:PendingClaimColour = 2)
     SELF.Q.job:Date_Completed_NormalFG = 16711680
     SELF.Q.job:Date_Completed_NormalBG = 16777215
     SELF.Q.job:Date_Completed_SelectedFG = 16777215
     SELF.Q.job:Date_Completed_SelectedBG = 16711680
   ELSIF(jobe:PendingClaimColour = 3)
     SELF.Q.job:Date_Completed_NormalFG = 255
     SELF.Q.job:Date_Completed_NormalBG = 16777215
     SELF.Q.job:Date_Completed_SelectedFG = 16777215
     SELF.Q.job:Date_Completed_SelectedBG = 255
   ELSE
     SELF.Q.job:Date_Completed_NormalFG = 0
     SELF.Q.job:Date_Completed_NormalBG = 16777215
     SELF.Q.job:Date_Completed_SelectedFG = 16777215
     SELF.Q.job:Date_Completed_SelectedBG = 0
   END
   IF (jobe:PendingClaimColour = 1)
     SELF.Q.job:Model_Number_NormalFG = 32768
     SELF.Q.job:Model_Number_NormalBG = 16777215
     SELF.Q.job:Model_Number_SelectedFG = 16777215
     SELF.Q.job:Model_Number_SelectedBG = 32768
   ELSIF(jobe:PendingClaimColour = 2)
     SELF.Q.job:Model_Number_NormalFG = 16711680
     SELF.Q.job:Model_Number_NormalBG = 16777215
     SELF.Q.job:Model_Number_SelectedFG = 16777215
     SELF.Q.job:Model_Number_SelectedBG = 16711680
   ELSIF(jobe:PendingClaimColour = 3)
     SELF.Q.job:Model_Number_NormalFG = 255
     SELF.Q.job:Model_Number_NormalBG = 16777215
     SELF.Q.job:Model_Number_SelectedFG = 16777215
     SELF.Q.job:Model_Number_SelectedBG = 255
   ELSE
     SELF.Q.job:Model_Number_NormalFG = 0
     SELF.Q.job:Model_Number_NormalBG = 16777215
     SELF.Q.job:Model_Number_SelectedFG = 16777215
     SELF.Q.job:Model_Number_SelectedBG = 0
   END
   IF (jobe:PendingClaimColour = 1)
     SELF.Q.job:Warranty_Charge_Type_NormalFG = 32768
     SELF.Q.job:Warranty_Charge_Type_NormalBG = 16777215
     SELF.Q.job:Warranty_Charge_Type_SelectedFG = 16777215
     SELF.Q.job:Warranty_Charge_Type_SelectedBG = 32768
   ELSIF(jobe:PendingClaimColour = 2)
     SELF.Q.job:Warranty_Charge_Type_NormalFG = 16711680
     SELF.Q.job:Warranty_Charge_Type_NormalBG = 16777215
     SELF.Q.job:Warranty_Charge_Type_SelectedFG = 16777215
     SELF.Q.job:Warranty_Charge_Type_SelectedBG = 16711680
   ELSIF(jobe:PendingClaimColour = 3)
     SELF.Q.job:Warranty_Charge_Type_NormalFG = 255
     SELF.Q.job:Warranty_Charge_Type_NormalBG = 16777215
     SELF.Q.job:Warranty_Charge_Type_SelectedFG = 16777215
     SELF.Q.job:Warranty_Charge_Type_SelectedBG = 255
   ELSE
     SELF.Q.job:Warranty_Charge_Type_NormalFG = 0
     SELF.Q.job:Warranty_Charge_Type_NormalBG = 16777215
     SELF.Q.job:Warranty_Charge_Type_SelectedFG = 16777215
     SELF.Q.job:Warranty_Charge_Type_SelectedBG = 0
   END
   IF (jobe:PendingClaimColour = 1)
     SELF.Q.job:Repair_Type_Warranty_NormalFG = 32768
     SELF.Q.job:Repair_Type_Warranty_NormalBG = 16777215
     SELF.Q.job:Repair_Type_Warranty_SelectedFG = 16777215
     SELF.Q.job:Repair_Type_Warranty_SelectedBG = 32768
   ELSIF(jobe:PendingClaimColour = 2)
     SELF.Q.job:Repair_Type_Warranty_NormalFG = 16711680
     SELF.Q.job:Repair_Type_Warranty_NormalBG = 16777215
     SELF.Q.job:Repair_Type_Warranty_SelectedFG = 16777215
     SELF.Q.job:Repair_Type_Warranty_SelectedBG = 16711680
   ELSIF(jobe:PendingClaimColour = 3)
     SELF.Q.job:Repair_Type_Warranty_NormalFG = 255
     SELF.Q.job:Repair_Type_Warranty_NormalBG = 16777215
     SELF.Q.job:Repair_Type_Warranty_SelectedFG = 16777215
     SELF.Q.job:Repair_Type_Warranty_SelectedBG = 255
   ELSE
     SELF.Q.job:Repair_Type_Warranty_NormalFG = 0
     SELF.Q.job:Repair_Type_Warranty_NormalBG = 16777215
     SELF.Q.job:Repair_Type_Warranty_SelectedFG = 16777215
     SELF.Q.job:Repair_Type_Warranty_SelectedBG = 0
   END
   IF (jobe:PendingClaimColour = 1)
     SELF.Q.tmp:ExchangedAt_NormalFG = 32768
     SELF.Q.tmp:ExchangedAt_NormalBG = 16777215
     SELF.Q.tmp:ExchangedAt_SelectedFG = 16777215
     SELF.Q.tmp:ExchangedAt_SelectedBG = 32768
   ELSIF(jobe:PendingClaimColour = 2)
     SELF.Q.tmp:ExchangedAt_NormalFG = 16711680
     SELF.Q.tmp:ExchangedAt_NormalBG = 16777215
     SELF.Q.tmp:ExchangedAt_SelectedFG = 16777215
     SELF.Q.tmp:ExchangedAt_SelectedBG = 16711680
   ELSIF(jobe:PendingClaimColour = 3)
     SELF.Q.tmp:ExchangedAt_NormalFG = 255
     SELF.Q.tmp:ExchangedAt_NormalBG = 16777215
     SELF.Q.tmp:ExchangedAt_SelectedFG = 16777215
     SELF.Q.tmp:ExchangedAt_SelectedBG = 255
   ELSE
     SELF.Q.tmp:ExchangedAt_NormalFG = 0
     SELF.Q.tmp:ExchangedAt_NormalBG = 16777215
     SELF.Q.tmp:ExchangedAt_SelectedFG = 16777215
     SELF.Q.tmp:ExchangedAt_SelectedBG = 0
   END
   IF (jobe:PendingClaimColour = 1)
     SELF.Q.job:MSN_NormalFG = 32768
     SELF.Q.job:MSN_NormalBG = 16777215
     SELF.Q.job:MSN_SelectedFG = 16777215
     SELF.Q.job:MSN_SelectedBG = 32768
   ELSIF(jobe:PendingClaimColour = 2)
     SELF.Q.job:MSN_NormalFG = 16711680
     SELF.Q.job:MSN_NormalBG = 16777215
     SELF.Q.job:MSN_SelectedFG = 16777215
     SELF.Q.job:MSN_SelectedBG = 16711680
   ELSIF(jobe:PendingClaimColour = 3)
     SELF.Q.job:MSN_NormalFG = 255
     SELF.Q.job:MSN_NormalBG = 16777215
     SELF.Q.job:MSN_SelectedFG = 16777215
     SELF.Q.job:MSN_SelectedBG = 255
   ELSE
     SELF.Q.job:MSN_NormalFG = 0
     SELF.Q.job:MSN_NormalBG = 16777215
     SELF.Q.job:MSN_SelectedFG = 16777215
     SELF.Q.job:MSN_SelectedBG = 0
   END
   IF (jobe:PendingClaimColour = 1)
     SELF.Q.job:Order_Number_NormalFG = 32768
     SELF.Q.job:Order_Number_NormalBG = 16777215
     SELF.Q.job:Order_Number_SelectedFG = 16777215
     SELF.Q.job:Order_Number_SelectedBG = 32768
   ELSIF(jobe:PendingClaimColour = 2)
     SELF.Q.job:Order_Number_NormalFG = 16711680
     SELF.Q.job:Order_Number_NormalBG = 16777215
     SELF.Q.job:Order_Number_SelectedFG = 16777215
     SELF.Q.job:Order_Number_SelectedBG = 16711680
   ELSIF(jobe:PendingClaimColour = 3)
     SELF.Q.job:Order_Number_NormalFG = 255
     SELF.Q.job:Order_Number_NormalBG = 16777215
     SELF.Q.job:Order_Number_SelectedFG = 16777215
     SELF.Q.job:Order_Number_SelectedBG = 255
   ELSE
     SELF.Q.job:Order_Number_NormalFG = 0
     SELF.Q.job:Order_Number_NormalBG = 16777215
     SELF.Q.job:Order_Number_SelectedFG = 16777215
     SELF.Q.job:Order_Number_SelectedBG = 0
   END
   IF (jobe:PendingClaimColour = 1)
     SELF.Q.job:Fault_Code1_NormalFG = 32768
     SELF.Q.job:Fault_Code1_NormalBG = 16777215
     SELF.Q.job:Fault_Code1_SelectedFG = 16777215
     SELF.Q.job:Fault_Code1_SelectedBG = 32768
   ELSIF(jobe:PendingClaimColour = 2)
     SELF.Q.job:Fault_Code1_NormalFG = 16711680
     SELF.Q.job:Fault_Code1_NormalBG = 16777215
     SELF.Q.job:Fault_Code1_SelectedFG = 16777215
     SELF.Q.job:Fault_Code1_SelectedBG = 16711680
   ELSIF(jobe:PendingClaimColour = 3)
     SELF.Q.job:Fault_Code1_NormalFG = 255
     SELF.Q.job:Fault_Code1_NormalBG = 16777215
     SELF.Q.job:Fault_Code1_SelectedFG = 16777215
     SELF.Q.job:Fault_Code1_SelectedBG = 255
   ELSE
     SELF.Q.job:Fault_Code1_NormalFG = 0
     SELF.Q.job:Fault_Code1_NormalBG = 16777215
     SELF.Q.job:Fault_Code1_SelectedFG = 16777215
     SELF.Q.job:Fault_Code1_SelectedBG = 0
   END
   IF (jobe:PendingClaimColour = 1)
     SELF.Q.job:Fault_Code2_NormalFG = 32768
     SELF.Q.job:Fault_Code2_NormalBG = 16777215
     SELF.Q.job:Fault_Code2_SelectedFG = 16777215
     SELF.Q.job:Fault_Code2_SelectedBG = 32768
   ELSIF(jobe:PendingClaimColour = 2)
     SELF.Q.job:Fault_Code2_NormalFG = 16711680
     SELF.Q.job:Fault_Code2_NormalBG = 16777215
     SELF.Q.job:Fault_Code2_SelectedFG = 16777215
     SELF.Q.job:Fault_Code2_SelectedBG = 16711680
   ELSIF(jobe:PendingClaimColour = 3)
     SELF.Q.job:Fault_Code2_NormalFG = 255
     SELF.Q.job:Fault_Code2_NormalBG = 16777215
     SELF.Q.job:Fault_Code2_SelectedFG = 16777215
     SELF.Q.job:Fault_Code2_SelectedBG = 255
   ELSE
     SELF.Q.job:Fault_Code2_NormalFG = 0
     SELF.Q.job:Fault_Code2_NormalBG = 16777215
     SELF.Q.job:Fault_Code2_SelectedFG = 16777215
     SELF.Q.job:Fault_Code2_SelectedBG = 0
   END
   IF (jobe:PendingClaimColour = 1)
     SELF.Q.job:Fault_Code3_NormalFG = 32768
     SELF.Q.job:Fault_Code3_NormalBG = 16777215
     SELF.Q.job:Fault_Code3_SelectedFG = 16777215
     SELF.Q.job:Fault_Code3_SelectedBG = 32768
   ELSIF(jobe:PendingClaimColour = 2)
     SELF.Q.job:Fault_Code3_NormalFG = 16711680
     SELF.Q.job:Fault_Code3_NormalBG = 16777215
     SELF.Q.job:Fault_Code3_SelectedFG = 16777215
     SELF.Q.job:Fault_Code3_SelectedBG = 16711680
   ELSIF(jobe:PendingClaimColour = 3)
     SELF.Q.job:Fault_Code3_NormalFG = 255
     SELF.Q.job:Fault_Code3_NormalBG = 16777215
     SELF.Q.job:Fault_Code3_SelectedFG = 16777215
     SELF.Q.job:Fault_Code3_SelectedBG = 255
   ELSE
     SELF.Q.job:Fault_Code3_NormalFG = 0
     SELF.Q.job:Fault_Code3_NormalBG = 16777215
     SELF.Q.job:Fault_Code3_SelectedFG = 16777215
     SELF.Q.job:Fault_Code3_SelectedBG = 0
   END
   IF (jobe:PendingClaimColour = 1)
     SELF.Q.job:Fault_Code4_NormalFG = 32768
     SELF.Q.job:Fault_Code4_NormalBG = 16777215
     SELF.Q.job:Fault_Code4_SelectedFG = 16777215
     SELF.Q.job:Fault_Code4_SelectedBG = 32768
   ELSIF(jobe:PendingClaimColour = 2)
     SELF.Q.job:Fault_Code4_NormalFG = 16711680
     SELF.Q.job:Fault_Code4_NormalBG = 16777215
     SELF.Q.job:Fault_Code4_SelectedFG = 16777215
     SELF.Q.job:Fault_Code4_SelectedBG = 16711680
   ELSIF(jobe:PendingClaimColour = 3)
     SELF.Q.job:Fault_Code4_NormalFG = 255
     SELF.Q.job:Fault_Code4_NormalBG = 16777215
     SELF.Q.job:Fault_Code4_SelectedFG = 16777215
     SELF.Q.job:Fault_Code4_SelectedBG = 255
   ELSE
     SELF.Q.job:Fault_Code4_NormalFG = 0
     SELF.Q.job:Fault_Code4_NormalBG = 16777215
     SELF.Q.job:Fault_Code4_SelectedFG = 16777215
     SELF.Q.job:Fault_Code4_SelectedBG = 0
   END
   IF (jobe:PendingClaimColour = 1)
     SELF.Q.job:Fault_Code5_NormalFG = 32768
     SELF.Q.job:Fault_Code5_NormalBG = 16777215
     SELF.Q.job:Fault_Code5_SelectedFG = 16777215
     SELF.Q.job:Fault_Code5_SelectedBG = 32768
   ELSIF(jobe:PendingClaimColour = 2)
     SELF.Q.job:Fault_Code5_NormalFG = 16711680
     SELF.Q.job:Fault_Code5_NormalBG = 16777215
     SELF.Q.job:Fault_Code5_SelectedFG = 16777215
     SELF.Q.job:Fault_Code5_SelectedBG = 16711680
   ELSIF(jobe:PendingClaimColour = 3)
     SELF.Q.job:Fault_Code5_NormalFG = 255
     SELF.Q.job:Fault_Code5_NormalBG = 16777215
     SELF.Q.job:Fault_Code5_SelectedFG = 16777215
     SELF.Q.job:Fault_Code5_SelectedBG = 255
   ELSE
     SELF.Q.job:Fault_Code5_NormalFG = 0
     SELF.Q.job:Fault_Code5_NormalBG = 16777215
     SELF.Q.job:Fault_Code5_SelectedFG = 16777215
     SELF.Q.job:Fault_Code5_SelectedBG = 0
   END
   IF (jobe:PendingClaimColour = 1)
     SELF.Q.job:Fault_Code6_NormalFG = 32768
     SELF.Q.job:Fault_Code6_NormalBG = 16777215
     SELF.Q.job:Fault_Code6_SelectedFG = 16777215
     SELF.Q.job:Fault_Code6_SelectedBG = 32768
   ELSIF(jobe:PendingClaimColour = 2)
     SELF.Q.job:Fault_Code6_NormalFG = 16711680
     SELF.Q.job:Fault_Code6_NormalBG = 16777215
     SELF.Q.job:Fault_Code6_SelectedFG = 16777215
     SELF.Q.job:Fault_Code6_SelectedBG = 16711680
   ELSIF(jobe:PendingClaimColour = 3)
     SELF.Q.job:Fault_Code6_NormalFG = 255
     SELF.Q.job:Fault_Code6_NormalBG = 16777215
     SELF.Q.job:Fault_Code6_SelectedFG = 16777215
     SELF.Q.job:Fault_Code6_SelectedBG = 255
   ELSE
     SELF.Q.job:Fault_Code6_NormalFG = 0
     SELF.Q.job:Fault_Code6_NormalBG = 16777215
     SELF.Q.job:Fault_Code6_SelectedFG = 16777215
     SELF.Q.job:Fault_Code6_SelectedBG = 0
   END
   IF (jobe:PendingClaimColour = 1)
     SELF.Q.job:Fault_Code7_NormalFG = 32768
     SELF.Q.job:Fault_Code7_NormalBG = 16777215
     SELF.Q.job:Fault_Code7_SelectedFG = 16777215
     SELF.Q.job:Fault_Code7_SelectedBG = 32768
   ELSIF(jobe:PendingClaimColour = 2)
     SELF.Q.job:Fault_Code7_NormalFG = 16711680
     SELF.Q.job:Fault_Code7_NormalBG = 16777215
     SELF.Q.job:Fault_Code7_SelectedFG = 16777215
     SELF.Q.job:Fault_Code7_SelectedBG = 16711680
   ELSIF(jobe:PendingClaimColour = 3)
     SELF.Q.job:Fault_Code7_NormalFG = 255
     SELF.Q.job:Fault_Code7_NormalBG = 16777215
     SELF.Q.job:Fault_Code7_SelectedFG = 16777215
     SELF.Q.job:Fault_Code7_SelectedBG = 255
   ELSE
     SELF.Q.job:Fault_Code7_NormalFG = 0
     SELF.Q.job:Fault_Code7_NormalBG = 16777215
     SELF.Q.job:Fault_Code7_SelectedFG = 16777215
     SELF.Q.job:Fault_Code7_SelectedBG = 0
   END
   IF (jobe:PendingClaimColour = 1)
     SELF.Q.job:Fault_Code8_NormalFG = 32768
     SELF.Q.job:Fault_Code8_NormalBG = 16777215
     SELF.Q.job:Fault_Code8_SelectedFG = 16777215
     SELF.Q.job:Fault_Code8_SelectedBG = 32768
   ELSIF(jobe:PendingClaimColour = 2)
     SELF.Q.job:Fault_Code8_NormalFG = 16711680
     SELF.Q.job:Fault_Code8_NormalBG = 16777215
     SELF.Q.job:Fault_Code8_SelectedFG = 16777215
     SELF.Q.job:Fault_Code8_SelectedBG = 16711680
   ELSIF(jobe:PendingClaimColour = 3)
     SELF.Q.job:Fault_Code8_NormalFG = 255
     SELF.Q.job:Fault_Code8_NormalBG = 16777215
     SELF.Q.job:Fault_Code8_SelectedFG = 16777215
     SELF.Q.job:Fault_Code8_SelectedBG = 255
   ELSE
     SELF.Q.job:Fault_Code8_NormalFG = 0
     SELF.Q.job:Fault_Code8_NormalBG = 16777215
     SELF.Q.job:Fault_Code8_SelectedFG = 16777215
     SELF.Q.job:Fault_Code8_SelectedBG = 0
   END
   IF (jobe:PendingClaimColour = 1)
     SELF.Q.job:Fault_Code9_NormalFG = 32768
     SELF.Q.job:Fault_Code9_NormalBG = 16777215
     SELF.Q.job:Fault_Code9_SelectedFG = 16777215
     SELF.Q.job:Fault_Code9_SelectedBG = 32768
   ELSIF(jobe:PendingClaimColour = 2)
     SELF.Q.job:Fault_Code9_NormalFG = 16711680
     SELF.Q.job:Fault_Code9_NormalBG = 16777215
     SELF.Q.job:Fault_Code9_SelectedFG = 16777215
     SELF.Q.job:Fault_Code9_SelectedBG = 16711680
   ELSIF(jobe:PendingClaimColour = 3)
     SELF.Q.job:Fault_Code9_NormalFG = 255
     SELF.Q.job:Fault_Code9_NormalBG = 16777215
     SELF.Q.job:Fault_Code9_SelectedFG = 16777215
     SELF.Q.job:Fault_Code9_SelectedBG = 255
   ELSE
     SELF.Q.job:Fault_Code9_NormalFG = 0
     SELF.Q.job:Fault_Code9_NormalBG = 16777215
     SELF.Q.job:Fault_Code9_SelectedFG = 16777215
     SELF.Q.job:Fault_Code9_SelectedBG = 0
   END
   IF (jobe:PendingClaimColour = 1)
     SELF.Q.job:Fault_Code10_NormalFG = 32768
     SELF.Q.job:Fault_Code10_NormalBG = 16777215
     SELF.Q.job:Fault_Code10_SelectedFG = 16777215
     SELF.Q.job:Fault_Code10_SelectedBG = 32768
   ELSIF(jobe:PendingClaimColour = 2)
     SELF.Q.job:Fault_Code10_NormalFG = 16711680
     SELF.Q.job:Fault_Code10_NormalBG = 16777215
     SELF.Q.job:Fault_Code10_SelectedFG = 16777215
     SELF.Q.job:Fault_Code10_SelectedBG = 16711680
   ELSIF(jobe:PendingClaimColour = 3)
     SELF.Q.job:Fault_Code10_NormalFG = 255
     SELF.Q.job:Fault_Code10_NormalBG = 16777215
     SELF.Q.job:Fault_Code10_SelectedFG = 16777215
     SELF.Q.job:Fault_Code10_SelectedBG = 255
   ELSE
     SELF.Q.job:Fault_Code10_NormalFG = 0
     SELF.Q.job:Fault_Code10_NormalBG = 16777215
     SELF.Q.job:Fault_Code10_SelectedFG = 16777215
     SELF.Q.job:Fault_Code10_SelectedBG = 0
   END
   IF (jobe:PendingClaimColour = 1)
     SELF.Q.job:Fault_Code11_NormalFG = 32768
     SELF.Q.job:Fault_Code11_NormalBG = 16777215
     SELF.Q.job:Fault_Code11_SelectedFG = 16777215
     SELF.Q.job:Fault_Code11_SelectedBG = 32768
   ELSIF(jobe:PendingClaimColour = 2)
     SELF.Q.job:Fault_Code11_NormalFG = 16711680
     SELF.Q.job:Fault_Code11_NormalBG = 16777215
     SELF.Q.job:Fault_Code11_SelectedFG = 16777215
     SELF.Q.job:Fault_Code11_SelectedBG = 16711680
   ELSIF(jobe:PendingClaimColour = 3)
     SELF.Q.job:Fault_Code11_NormalFG = 255
     SELF.Q.job:Fault_Code11_NormalBG = 16777215
     SELF.Q.job:Fault_Code11_SelectedFG = 16777215
     SELF.Q.job:Fault_Code11_SelectedBG = 255
   ELSE
     SELF.Q.job:Fault_Code11_NormalFG = 0
     SELF.Q.job:Fault_Code11_NormalBG = 16777215
     SELF.Q.job:Fault_Code11_SelectedFG = 16777215
     SELF.Q.job:Fault_Code11_SelectedBG = 0
   END
   IF (jobe:PendingClaimColour = 1)
     SELF.Q.job:Fault_Code12_NormalFG = 32768
     SELF.Q.job:Fault_Code12_NormalBG = 16777215
     SELF.Q.job:Fault_Code12_SelectedFG = 16777215
     SELF.Q.job:Fault_Code12_SelectedBG = 32768
   ELSIF(jobe:PendingClaimColour = 2)
     SELF.Q.job:Fault_Code12_NormalFG = 16711680
     SELF.Q.job:Fault_Code12_NormalBG = 16777215
     SELF.Q.job:Fault_Code12_SelectedFG = 16777215
     SELF.Q.job:Fault_Code12_SelectedBG = 16711680
   ELSIF(jobe:PendingClaimColour = 3)
     SELF.Q.job:Fault_Code12_NormalFG = 255
     SELF.Q.job:Fault_Code12_NormalBG = 16777215
     SELF.Q.job:Fault_Code12_SelectedFG = 16777215
     SELF.Q.job:Fault_Code12_SelectedBG = 255
   ELSE
     SELF.Q.job:Fault_Code12_NormalFG = 0
     SELF.Q.job:Fault_Code12_NormalBG = 16777215
     SELF.Q.job:Fault_Code12_SelectedFG = 16777215
     SELF.Q.job:Fault_Code12_SelectedBG = 0
   END
   IF (jobe:PendingClaimColour = 1)
     SELF.Q.jobe:POPConfirmed_NormalFG = 32768
     SELF.Q.jobe:POPConfirmed_NormalBG = 16777215
     SELF.Q.jobe:POPConfirmed_SelectedFG = 16777215
     SELF.Q.jobe:POPConfirmed_SelectedBG = 32768
   ELSIF(jobe:PendingClaimColour = 2)
     SELF.Q.jobe:POPConfirmed_NormalFG = 16711680
     SELF.Q.jobe:POPConfirmed_NormalBG = 16777215
     SELF.Q.jobe:POPConfirmed_SelectedFG = 16777215
     SELF.Q.jobe:POPConfirmed_SelectedBG = 16711680
   ELSIF(jobe:PendingClaimColour = 3)
     SELF.Q.jobe:POPConfirmed_NormalFG = 255
     SELF.Q.jobe:POPConfirmed_NormalBG = 16777215
     SELF.Q.jobe:POPConfirmed_SelectedFG = 16777215
     SELF.Q.jobe:POPConfirmed_SelectedBG = 255
   ELSE
     SELF.Q.jobe:POPConfirmed_NormalFG = 0
     SELF.Q.jobe:POPConfirmed_NormalBG = 16777215
     SELF.Q.jobe:POPConfirmed_SelectedFG = 16777215
     SELF.Q.jobe:POPConfirmed_SelectedBG = 0
   END
   IF (jobe:PendingClaimColour = 1)
     SELF.Q.jobe:PendingClaimColour_NormalFG = 32768
     SELF.Q.jobe:PendingClaimColour_NormalBG = 16777215
     SELF.Q.jobe:PendingClaimColour_SelectedFG = 16777215
     SELF.Q.jobe:PendingClaimColour_SelectedBG = 32768
   ELSIF(jobe:PendingClaimColour = 2)
     SELF.Q.jobe:PendingClaimColour_NormalFG = 16711680
     SELF.Q.jobe:PendingClaimColour_NormalBG = 16777215
     SELF.Q.jobe:PendingClaimColour_SelectedFG = 16777215
     SELF.Q.jobe:PendingClaimColour_SelectedBG = 16711680
   ELSIF(jobe:PendingClaimColour = 3)
     SELF.Q.jobe:PendingClaimColour_NormalFG = 255
     SELF.Q.jobe:PendingClaimColour_NormalBG = 16777215
     SELF.Q.jobe:PendingClaimColour_SelectedFG = 16777215
     SELF.Q.jobe:PendingClaimColour_SelectedBG = 255
   ELSE
     SELF.Q.jobe:PendingClaimColour_NormalFG = 0
     SELF.Q.jobe:PendingClaimColour_NormalBG = 16777215
     SELF.Q.jobe:PendingClaimColour_SelectedFG = 16777215
     SELF.Q.jobe:PendingClaimColour_SelectedBG = 0
   END
   IF (jobe:PendingClaimColour = 1)
     SELF.Q.tmp:ResubmitFlag_NormalFG = 32768
     SELF.Q.tmp:ResubmitFlag_NormalBG = 16777215
     SELF.Q.tmp:ResubmitFlag_SelectedFG = 16777215
     SELF.Q.tmp:ResubmitFlag_SelectedBG = 32768
   ELSIF(jobe:PendingClaimColour = 2)
     SELF.Q.tmp:ResubmitFlag_NormalFG = 16711680
     SELF.Q.tmp:ResubmitFlag_NormalBG = 16777215
     SELF.Q.tmp:ResubmitFlag_SelectedFG = 16777215
     SELF.Q.tmp:ResubmitFlag_SelectedBG = 16711680
   ELSIF(jobe:PendingClaimColour = 3)
     SELF.Q.tmp:ResubmitFlag_NormalFG = 255
     SELF.Q.tmp:ResubmitFlag_NormalBG = 16777215
     SELF.Q.tmp:ResubmitFlag_SelectedFG = 16777215
     SELF.Q.tmp:ResubmitFlag_SelectedBG = 255
   ELSE
     SELF.Q.tmp:ResubmitFlag_NormalFG = 0
     SELF.Q.tmp:ResubmitFlag_NormalBG = 16777215
     SELF.Q.tmp:ResubmitFlag_SelectedFG = 16777215
     SELF.Q.tmp:ResubmitFlag_SelectedBG = 0
   END
   IF (jobe:PendingClaimColour = 1)
     SELF.Q.tmp:BouncerClaim_NormalFG = 32768
     SELF.Q.tmp:BouncerClaim_NormalBG = 16777215
     SELF.Q.tmp:BouncerClaim_SelectedFG = 16777215
     SELF.Q.tmp:BouncerClaim_SelectedBG = 32768
   ELSIF(jobe:PendingClaimColour = 2)
     SELF.Q.tmp:BouncerClaim_NormalFG = 16711680
     SELF.Q.tmp:BouncerClaim_NormalBG = 16777215
     SELF.Q.tmp:BouncerClaim_SelectedFG = 16777215
     SELF.Q.tmp:BouncerClaim_SelectedBG = 16711680
   ELSIF(jobe:PendingClaimColour = 3)
     SELF.Q.tmp:BouncerClaim_NormalFG = 255
     SELF.Q.tmp:BouncerClaim_NormalBG = 16777215
     SELF.Q.tmp:BouncerClaim_SelectedFG = 16777215
     SELF.Q.tmp:BouncerClaim_SelectedBG = 255
   ELSE
     SELF.Q.tmp:BouncerClaim_NormalFG = 0
     SELF.Q.tmp:BouncerClaim_NormalBG = 16777215
     SELF.Q.tmp:BouncerClaim_SelectedFG = 16777215
     SELF.Q.tmp:BouncerClaim_SelectedBG = 0
   END
   IF (jobe:PendingClaimColour = 1)
     SELF.Q.tmp:Blank_NormalFG = 32768
     SELF.Q.tmp:Blank_NormalBG = 16777215
     SELF.Q.tmp:Blank_SelectedFG = 16777215
     SELF.Q.tmp:Blank_SelectedBG = 32768
   ELSIF(jobe:PendingClaimColour = 2)
     SELF.Q.tmp:Blank_NormalFG = 16711680
     SELF.Q.tmp:Blank_NormalBG = 16777215
     SELF.Q.tmp:Blank_SelectedFG = 16777215
     SELF.Q.tmp:Blank_SelectedBG = 16711680
   ELSIF(jobe:PendingClaimColour = 3)
     SELF.Q.tmp:Blank_NormalFG = 255
     SELF.Q.tmp:Blank_NormalBG = 16777215
     SELF.Q.tmp:Blank_SelectedFG = 16777215
     SELF.Q.tmp:Blank_SelectedBG = 255
   ELSE
     SELF.Q.tmp:Blank_NormalFG = 0
     SELF.Q.tmp:Blank_NormalBG = 16777215
     SELF.Q.tmp:Blank_SelectedFG = 16777215
     SELF.Q.tmp:Blank_SelectedBG = 0
   END


BRW1.TakeEvent PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'TakeEvent'
  Xplore1.LastEvent = EVENT()                         !Xplore
  IF FOCUS() = ?Browse:1                              !Xplore
    CASE EVENT()                                      !Xplore
    OF Event:AlertKey                                 !Xplore
      CASE KEYCODE()                                  !Xplore
      OF AltF11                                       !Xplore
         Xplore1.AdjustAllColumns()                   !Xplore
      OF AltF12                                       !Xplore
         Xplore1.ResetToDefault()                     !Xplore
      OF AltR                                         !Xplore
         Xplore1.ToggleBar()                          !Xplore
      OF AltM                                         !Xplore
         Xplore1.InvokePopup()                        !Xplore
      END                                             !Xplore
    OF xpEVENT:RightButtonUp                          !Xplore
       Xplore1.RightButtonUp                          !Xplore
    OF xpEVENT:LeftButtonUp                           !Xplore
       BRW1.FileSeqOn = False                         !Xplore
       Xplore1.LeftButtonUp(0)                        !Xplore
    OF xpEVENT:Reset
       ThisWindow.Reset(False)                        !Xplore
    ELSE
       !BRW1.SavePosition()                           !Xplore
       Xplore1.HandleMyEvents()                       !Xplore
       !BRW1.RestorePosition()                        !Xplore
    END !CASE EVENT()                                 !Xplore
  END !IF FOCUS() = ?Browse:1                         !Xplore
  PARENT.TakeEvent


BRW1.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF RECORDS(SELF.ListQueue)
    CASE KEYCODE()
    OF MouseLeft2
      !Extension Templete Code:XploreOOPBrowse,BrowseBoxDoubleClickHandler
      IF Xplore1.LeftButton2()                        !Xplore
        RETURN(0)                                     !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  Access:tradeacc.clearkey(tra:Account_Number_Key)
  tra:account_number = wob:HeadAccountNumber
  if access:tradeacc.fetch(tra:account_number_key) then
      !can't find the tradeaccount
      tmp:JobNumber = job:ref_number
  ELSe
      tmp:jobNumber = wob:JobNumber
      tmp:BranchID  = tra:BranchIdentification
  END!if traders fetch
  tmp:JobNumber = Clip(wob:RefNumber) & '-' & Clip(tra:BranchIdentification) & wob:JobNumber
  
  Access:JOBS.Clearkey(job:Ref_Number_Key)
  job:Ref_Number  = wob:RefNumber
  If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      !Found
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber  = job:Ref_Number
      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          !Found
  
      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          !Error
      End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      !Error
  End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
  BRW1::RecordStatus=ReturnValue
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  RETURN ReturnValue

!=======================================================
!Extension Templete Code:XploreOOPBrowse,LocalProcedures
!=======================================================
BRW1.OrderInit PROCEDURE(<KEY K>)
!This method is located here (in BRW1) because SELF.ORDER queue is protected in the ViewManager
  CODE
  !0{PROP:Text} = 'SortOrder=' & POINTER(SELF.ORDER) & ' order=' & SELF.ORDER.Order & ' fe=' & SELF.ORDER.FreeElement
  CLEAR(SELF.ORDER.LimitType)                         !Xplore
  FREE(SELF.ORDER.Filter)                             !Xplore
  DISPOSE(SELF.ORDER.Filter)                          !Xplore
  SELF.ORDER.Filter &= NEW FilterQueue                !Xplore
  DISPOSE(SELF.ORDER.RangeList)                       !Xplore
  SELF.Order.RangeList &= NEW BufferedPairsClass      !Xplore
  SELF.ORDER.MainKey &= K                             !Xplore
  SELF.Order.RangeList.Init                           !Xplore
  DISPOSE(SELF.Order.Order)                           !Xplore
  PUT(SELF.Order)                                     !Xplore
  RETURN                                              !Xplore
BRW1.SavePosition PROCEDURE()
  CODE
  SELF.Savedposition = SELF.Primary.Me.SaveFile()     !Xplore
  RETURN
BRW1.RestorePosition PROCEDURE()
  CODE
  SELF.Primary.Me.RestoreFile(SELF.SavedPosition)     !Xplore
  RETURN
BRW1.RecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  !Done here because Order is PROTECTED
  IF SELF.Order.MainKey &= NULL                       !Xplore
    TotalRecords = 0                                  !Xplore
  ELSE                                                !Xplore
    TotalRecords = RECORDS(SELF.Order.MainKey)        !Xplore  
  END                                                 !Xplore
  RETURN TotalRecords                                 !Xplore
BRW1.ResetPairsQ PROCEDURE()
  CODE
Xplore1.MyPreviewer PROCEDURE(PreviewQueue PQ,REPORT CPCS)
ReturnValue LONG
SaveCopies  LONG
  CODE
  !CPCS Previewer (c) copyright Creative PC Solutions, 1995-97
  ReturnValue                  = PrintPreview(PQ,0,,CPCS,0,,,,)
  CASE ReturnValue
  OF RequestCompleted !1
     SaveCopies                = PRINTER{PROPPRINT:Copies}
     PRINTER{PROPPRINT:Copies} = CPCS{PROPPRINT:Copies}
     HandleCopies(PQ,CPCS{PROPPRINT:Copies})
     CPCS{PROP:FlushPreview}   = True
     PRINTER{PROPPRINT:Copies} = SaveCopies
  OF RequestCancelled !2
  END
  RETURN (False)
!================================================================================
!Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*STRING ColumnString,*CSTRING Pic)
Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*CSTRING Pic)
ColumnPicture CSTRING(20)
ColumnString  STRING(80)
XpDay         SHORT
XpMonth       SHORT
XpTime        STRING(8)
NumericValue  LONG
NumericSlot   LONG
NumericStart  LONG
Total         DECIMAL(11,2)
TextSfx       STRING(20)
  CODE
  ColumnPicture = CLIP(Pic)
  ColumnString  = CLIP(LEFT(FORMAT(WHAT(SELF.BQ,QFieldNumber),ColumnPicture)))
  Total         = 0
  TextSfx       = ''
  CASE QFieldNumber
  !
  ! Syntax of BrowseBox Queue Field Names are: BRW1.Q.PRE:Field  NOT XploreOOPListChild or =>5501
  !
  OF 1 !tmp:Tag
  OF 7 !wob:RefNumber
  OF 12 !wob:JobNumber
  OF 17 !wob:HeadAccountNumber
  OF 22 !job:Ref_Number
  OF 27 !tmp:BranchID
        ColumnString     = SUB(LEFT(BRW1.Q.tmp:BranchID),1,20)
  OF 32 !tmp:JobNumber
        ColumnString     = SUB(LEFT(BRW1.Q.tmp:JobNumber),1,20)
  OF 37 !tmp:RepairedAt
  OF 42 !job:Account_Number
  OF 47 !job:ESN
  OF 52 !job:Date_Completed
  OF 57 !job:Model_Number
  OF 62 !job:Warranty_Charge_Type
  OF 67 !job:Repair_Type_Warranty
  OF 72 !tmp:ExchangedAt
  OF 77 !job:MSN
  OF 82 !job:Order_Number
  OF 87 !job:Fault_Code1
  OF 92 !job:Fault_Code2
  OF 97 !job:Fault_Code3
  OF 102 !job:Fault_Code4
  OF 107 !job:Fault_Code5
  OF 112 !job:Fault_Code6
  OF 117 !job:Fault_Code7
  OF 122 !job:Fault_Code8
  OF 127 !job:Fault_Code9
  OF 132 !job:Fault_Code10
  OF 137 !job:Fault_Code11
  OF 142 !job:Fault_Code12
  OF 147 !jobe:POPConfirmed
  OF 152 !jobe:PendingClaimColour
  OF 157 !tmp:ResubmitFlag
  OF 162 !tmp:BouncerClaim
  OF 168 !tmp:Blank
  OF 173 !tmp:RRCClaimStatus
  OF 174 !wob:RecordNumber
  OF 175 !wob:EDI
  OF 176 !wob:Manufacturer
  END !CASE QFieldNumber
  SELF.QSel.Name     = CLIP(ColumnString)
  GET(SELF.QSel,SELF.QSel.Name)
  IF ERRORCODE()
     SELF.QSel.Name  = CLIP(ColumnString)
     SELF.QSel.Count = 0
     SELF.QSel.Total = 0
     ADD(SELF.QSel,+SELF.QSel.Name)
     GET(SELF.QSel,+SELF.QSel.Name)
  END
  SELF.QSel.Count   += 1
  SELF.QSel.Total   += Total
  TextSfx            = FORMAT(SELF.QSEL.Total,ColumnPicture)
  SELF.QSel.TextSfx  = TextSfx
  PUT(SELF.QSel)
  RETURN

!================================================================================
Xplore1.SetNewOrderFields PROCEDURE()
  CODE
  BRW1.LocatorField  = SELF.sq.Field
  RETURN
!================================================================================
Xplore1.AddColumnSortOrder PROCEDURE(LONG SortQRecord)
  CODE
  !!SELF.FQ.SortField = SELF.BC.AddSortOrder(Xplore1Step38,wob:HeadEDIKey)
  SELF.FQ.SortField = SELF.BC.AddSortOrder(,wob:HeadEDIKey)
  EXECUTE SortQRecord
    BEGIN
      Xplore1Step1.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator1)
      Xplore1Locator1.Init(,tmp:Tag,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step2.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore1Locator2)
      Xplore1Locator2.Init(,wob:RefNumber,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step3.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore1Locator3)
      Xplore1Locator3.Init(,wob:JobNumber,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step4.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator4)
      Xplore1Locator4.Init(,wob:HeadAccountNumber,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step5.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore1Locator5)
      Xplore1Locator5.Init(,job:Ref_Number,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step6.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
    END !BEGIN
    BEGIN
      Xplore1Step8.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator8)
      Xplore1Locator8.Init(,tmp:RepairedAt,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step9.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator9)
      Xplore1Locator9.Init(,job:Account_Number,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step10.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator10)
      Xplore1Locator10.Init(,job:ESN,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step11.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator11)
      Xplore1Locator11.Init(,job:Date_Completed,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step12.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator12)
      Xplore1Locator12.Init(,job:Model_Number,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step13.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator13)
      Xplore1Locator13.Init(,job:Warranty_Charge_Type,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step14.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator14)
      Xplore1Locator14.Init(,job:Repair_Type_Warranty,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step15.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator15)
      Xplore1Locator15.Init(,tmp:ExchangedAt,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step16.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator16)
      Xplore1Locator16.Init(,job:MSN,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step17.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator17)
      Xplore1Locator17.Init(,job:Order_Number,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step18.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator18)
      Xplore1Locator18.Init(,job:Fault_Code1,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step19.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator19)
      Xplore1Locator19.Init(,job:Fault_Code2,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step20.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator20)
      Xplore1Locator20.Init(,job:Fault_Code3,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step21.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator21)
      Xplore1Locator21.Init(,job:Fault_Code4,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step22.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator22)
      Xplore1Locator22.Init(,job:Fault_Code5,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step23.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator23)
      Xplore1Locator23.Init(,job:Fault_Code6,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step24.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator24)
      Xplore1Locator24.Init(,job:Fault_Code7,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step25.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator25)
      Xplore1Locator25.Init(,job:Fault_Code8,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step26.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator26)
      Xplore1Locator26.Init(,job:Fault_Code9,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step27.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator27)
      Xplore1Locator27.Init(,job:Fault_Code10,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step28.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator28)
      Xplore1Locator28.Init(,job:Fault_Code11,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step29.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator29)
      Xplore1Locator29.Init(,job:Fault_Code12,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step30.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator30)
      Xplore1Locator30.Init(,jobe:POPConfirmed,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step31.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator31)
      Xplore1Locator31.Init(,jobe:PendingClaimColour,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step32.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator32)
      Xplore1Locator32.Init(,tmp:ResubmitFlag,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step33.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator33)
      Xplore1Locator33.Init(,tmp:BouncerClaim,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step34.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator34)
      Xplore1Locator34.Init(,tmp:Blank,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step35.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator35)
      Xplore1Locator35.Init(,tmp:RRCClaimStatus,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step36.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore1Locator36)
      Xplore1Locator36.Init(,wob:RecordNumber,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step37.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator37)
      Xplore1Locator37.Init(,wob:EDI,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step38.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator38)
      Xplore1Locator38.Init(,wob:Manufacturer,1,SELF.BC)
    END !BEGIN
  END !EXECUTE
  SELF.BC.AddRange(wob:EDI)
  RETURN
!================================================================================
Xplore1.SetFilters PROCEDURE()
I     LONG
J     LONG
SaveQ LONG
  CODE
  SaveQ = POINTER(SELF.Fq)
  LOOP I = 1 to RECORDS(SELF.Fq)
    GET(SELF.Fq,I)
    IF NOT SELF.FQ.Sortfield
      CYCLE
    END
    LOOP J = 1 TO 2
      IF SELF.BC.SetSort(SELF.Fq.SortField + J - 1).
      IF Upper(tmp:SelectManufacturer) = 0
        BRW1.OrderInit(wob:HeadEDIKey)
        BRW1.AddRange(wob:EDI)
        SELF.FindAnySubFields(1,J-1)
      ELSIF Upper(tmp:SelectManufacturer) = 1
        BRW1.OrderInit(wob:HeadEDIKey)
        BRW1.AddRange(wob:Manufacturer)
        SELF.FindAnySubFields(1,J-1)
      ELSE
        BRW1.OrderInit(wob:HeadEDIKey)
        BRW1.AddRange(wob:EDI)
        SELF.FindAnySubFields(1,J-1)
      END !IF ...
    END !LOOP J = 1 TO 2
  END !LOOP
  GET(SELF.Fq,SaveQ)
  RETURN
!================================================================================
Xplore1.GetRecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  TotalRecords = BRW1.RecordsInKey()
  IF TotalRecords = 0
     TotalRecords = RECORDS(WEBJOB)
  END
  RETURN TotalRecords
!================================================================================
Xplore1.ListBoxData PROCEDURE()
XpFileFields    GROUP,STATIC
                PSTRING('tmp:Tag')                    !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:Tag')                    !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:Tag')                    !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(1)                               !Icon Specified
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('wob:RefNumber')              !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Job No')                     !Header
                PSTRING('@n_8')                       !Picture
                PSTRING('Link to JOBS RefNumber')     !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('wob:JobNumber')              !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Job Number')                 !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Job Number')                 !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('wob:HeadAccountNumber')      !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Header Account Number')      !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Header Account Number')      !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Ref_Number')             !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Job No')                     !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Job No')                     !Description
                STRING('R')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tmp:BranchID')               !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:BranchID')               !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:BranchID')               !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('N')                           !Locator Type
                !-------------------------
                PSTRING('tmp:JobNumber')              !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:JobNumber')              !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:JobNumber')              !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tmp:RepairedAt')             !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:RepairedAt')             !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:RepairedAt')             !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Account_Number')         !Field Name
                SHORT(60)                             !Default Column Width
                PSTRING('Account Number')             !Header
                PSTRING('@s15')                       !Picture
                PSTRING('Account Number')             !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:ESN')                    !Field Name
                SHORT(80)                             !Default Column Width
                PSTRING('I.M.E.I. Number')            !Header
                PSTRING('@s16')                       !Picture
                PSTRING('I.M.E.I. Number')            !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Date_Completed')         !Field Name
                SHORT(40)                             !Default Column Width
                PSTRING('Completed')                  !Header
                PSTRING('@D6b')                       !Picture
                PSTRING('Completed')                  !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Model_Number')           !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Model Number')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Model Number')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Warranty_Charge_Type')   !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Charge Type')                !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Charge Type')                !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Repair_Type_Warranty')   !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Repair Type')                !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Repair Type')                !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tmp:ExchangedAt')            !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:ExchangedAt')            !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:ExchangedAt')            !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:MSN')                    !Field Name
                SHORT(80)                             !Default Column Width
                PSTRING('MSN')                        !Header
                PSTRING('@s14')                       !Picture
                PSTRING('MSN')                        !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Order_Number')           !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Order Number')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Order Number')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Fault_Code1')            !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Fault Code 1')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Fault Code 1')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Fault_Code2')            !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Fault Code 2')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Fault Code 2')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Fault_Code3')            !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Fault Code 3')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Fault Code 3')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Fault_Code4')            !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Fault Code 4')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Fault Code 4')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Fault_Code5')            !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Fault Code 5')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Fault Code 5')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Fault_Code6')            !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Fault Code 6')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Fault Code 6')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Fault_Code7')            !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Fault Code 7')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Fault Code 7')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Fault_Code8')            !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Fault Code 8')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Fault Code 8')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Fault_Code9')            !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Fault Code 9')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Fault Code 9')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Fault_Code10')           !Field Name
                SHORT(1020)                           !Default Column Width
                PSTRING('Fault Code 10')              !Header
                PSTRING('@s255')                      !Picture
                PSTRING('Fault Code 10')              !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Fault_Code11')           !Field Name
                SHORT(1020)                           !Default Column Width
                PSTRING('Fault Code 11')              !Header
                PSTRING('@s255')                      !Picture
                PSTRING('Fault Code 11')              !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Fault_Code12')           !Field Name
                SHORT(1020)                           !Default Column Width
                PSTRING('Fault Code 12')              !Header
                PSTRING('@s255')                      !Picture
                PSTRING('Fault Code 12')              !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('jobe:POPConfirmed')          !Field Name
                SHORT(4)                              !Default Column Width
                PSTRING('POP Confirmed')              !Header
                PSTRING('@n1')                        !Picture
                PSTRING('POPConfirmed')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('jobe:PendingClaimColour')    !Field Name
                SHORT(4)                              !Default Column Width
                PSTRING('Pending Claim Colour')       !Header
                PSTRING('@n1')                        !Picture
                PSTRING('Pending Claim Colour')       !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tmp:ResubmitFlag')           !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:ResubmitFlag')           !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:ResubmitFlag')           !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tmp:BouncerClaim')           !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:BouncerClaim')           !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:BouncerClaim')           !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(1)                               !Icon Specified
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tmp:Blank')                  !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:Blank')                  !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:Blank')                  !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tmp:RRCClaimStatus')         !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:RRCClaimStatus')         !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:RRCClaimStatus')         !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('wob:RecordNumber')           !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Record Number')              !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Record Number')              !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('wob:EDI')                    !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('EDI')                        !Header
                PSTRING('@s3')                        !Picture
                PSTRING('EDI')                        !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('wob:Manufacturer')           !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Manufacturer')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Manufacturer')               !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                END
XpDim           SHORT(38)
!--------------------------
XpSortFields    GROUP,STATIC
                PSTRING('tmp:BranchID')               !Column Field
                SHORT(1)                              !Nos of Sort Fields
                PSTRING('tmp:BranchID')               !Sort Field Seq:1
                BYTE(0)                               !Ascending
                PSTRING('')                           !Picture
                !-------------------------
                PSTRING('tmp:JobNumber')              !Column Field
                SHORT(2)                              !Nos of Sort Fields
                PSTRING('wob:EDI')                    !Sort Field Seq:1
                BYTE(0)                               !Ascending
                PSTRING('@s3')                        !Picture
                PSTRING('wob:RefNumber')              !Sort Field Seq:2
                BYTE(0)                               !Ascending
                PSTRING('@s8')                        !Picture
                !-------------------------
                END
XpSortDim       SHORT(2)
!--------------------------
XpAddSortFields GROUP,STATIC
                BYTE(0)                               !1=Apply to all columns 0=Apply only to non-specific columns
                SHORT(0)                              !Nos of Additional Sort Fields
                END
  CODE
  SELF.qdim     = XpDim
  SELF.gr.lbf  &= XpFileFields
  SELF.qdimS    = XpSortDim
  SELF.grS.sf  &= XpSortFields
  SELF.grA.sf  &= XpAddSortFields
  SELF.GetQfields()
  DO CreateFormatQs
  RETURN

CreateFormatQs ROUTINE
  SELF.CreateFormatQ(1)

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
