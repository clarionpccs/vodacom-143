

   MEMBER('vodr0045.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0002.INC'),ONCE        !Local module procedure declarations
                     END


ThirdPartyDespatchReport PROCEDURE                    !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::7:TAGFLAG          BYTE(0)
DASBRW::7:TAGDISPSTATUS    BYTE(0)
DASBRW::7:QUEUE           QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
tmp:VersionNumber    STRING(30)
Parameter_Group      GROUP,PRE()
LOC:EndDate          DATE
LOC:StartDate        DATE
                     END
Local_Group          GROUP,PRE(LOC)
ApplicationName      STRING('ServiceBase')
CompanyName          STRING(30)
CountAllJobs         LONG
CountJobs            LONG
DesktopPath          STRING(255)
FileName             STRING(255)
Path                 STRING(255)
ProgramName          STRING(50)
SectionName          STRING(30)
Text                 STRING(255)
UserName             STRING(50)
Version              STRING(8)
WorkingHours         DECIMAL(7,2)
RepairCentreType     STRING(3)
                     END
Misc_Group           GROUP,PRE()
OPTION1              SHORT
RecordCount          LONG
                     END
SummaryQueue         QUEUE,PRE(sq)
CompanyName          STRING(30)
JobCount             LONG
                     END
ManufacturerQueue    QUEUE,PRE(manQ)
CompanyName          STRING(30)
ModelNumber          STRING(30)
Count                LONG
                     END
Clipboard_Group      GROUP,PRE(clip)
OriginalValue        STRING(255)
Saved                BYTE
Value                CSTRING(200)
                     END
Excel_Group          GROUP,PRE()
Excel                SIGNED
excel:DateFormat     STRING('dd mmm yyyy')
excel:ColumnName     STRING(50)
excel:ColumnWidth    REAL
excel:CommentText    STRING(255)
excel:OperatingSystem REAL
excel:Visible        BYTE
                     END
Sheet_Group          GROUP,PRE(sheet)
TempLastCol          STRING(1)
HeadLastCol          STRING('C')
DataLastCol          STRING('O')
HeadSummaryRow       LONG(9)
DataSectionRow       LONG(9)
DataHeaderRow        LONG(11)
                     END
HeaderQueue          QUEUE,PRE(head)
ColumnName           STRING(30)
ColumnWidth          REAL
NumberFormat         STRING(20)
                     END
HeadAccount_Queue    QUEUE,PRE(haQ)
AccountNumber        STRING(15)
AccountName          STRING(30)
BranchIdentification STRING(2)
IncludeSaturday      LONG
IncludeSunday        LONG
                     END
SubAccount_Queue     QUEUE,PRE(saQ)
AccountNumber        STRING(15)
AccountName          STRING(30)
HeadAccountNumber    STRING(15)
HeadAccountName      STRING(30)
BranchIdentification STRING(2)
                     END
Debug_Group          GROUP,PRE(debug)
Active               LONG
Count                LONG
                     END
GUIMode              BYTE
LocalHeadAccount     STRING(30)
LocalTag             STRING(1)
LocalTimeOut         LONG
DoAll                STRING(1)
WebJOB_OK            BYTE
tmp:StatusReceivedAtARC STRING(30)
save_aud_id          USHORT,AUTO
save_tra_id          USHORT,AUTO
save_trb_id          USHORT,AUTO
ReturnedQueue        QUEUE,PRE(retque)
CompanyName          STRING(30)
Count                LONG
TotalHours           LONG
                     END
FDB5::View:FileDrop  VIEW(TRDPARTY)
                       PROJECT(trd:Company_Name)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?LOC:CompanyName
trd:Company_Name       LIKE(trd:Company_Name)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
LocalTag               LIKE(LocalTag)                 !List box control field - type derived from local data
LocalTag_Icon          LONG                           !Entry's icon ID
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
MainWindow           WINDOW('Report'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(60,38,560,360),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(64,56,552,44),USE(?PanelTip),FILL(0D6EAEFH)
                       GROUP('Top Tip'),AT(68,60,544,36),USE(?GroupTip),BOXED,TRN
                       END
                       STRING(@s255),AT(72,76,496,),USE(SRN:TipText),TRN
                       BUTTON,AT(576,64,36,32),USE(?ButtonHelp),TRN,FLAT,ICON('F1Help.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Third Party Despatch Report Criteria'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,104,552,258),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Criteria'),USE(?Tab1)
                           OPTION('1'),AT(213,106,219,36),USE(OPTION1),COLOR(0D6E7EFH),TIP('Choose wether to report on one or all agents'),BEVEL(0,0,01H)
                             RADIO(' All 3rd Party Agents'),AT(213,108),USE(?Option1:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),TIP('Report on all agents'),VALUE('1')
                             RADIO('Agent'),AT(213,126,35,12),USE(?Option1:Radio2),RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),TIP('Report on a single agent'),VALUE('2')
                           END
                           BUTTON('&Rev tags'),AT(417,124,50,13),USE(?DASREVTAG),DISABLE,HIDE
                           LIST,AT(303,128,117,10),USE(LOC:CompanyName),IMM,VSCROLL,FONT('Arial',8,010101H,FONT:bold),COLOR(COLOR:White),TIP('Choose a third party agent from the list'),FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDrop)
                           BUTTON('sho&W tags'),AT(417,138,51,15),USE(?DASSHOWTAG),DISABLE,HIDE
                           STRING('Start Despatch Date'),AT(213,150,72,12),USE(?String4),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@D08B),AT(303,150,81,10),USE(LOC:StartDate),IMM,FONT('Arial',8,010101H,FONT:bold),COLOR(COLOR:White),TIP('Enter the earliest despatched date to show on report'),REQ
                           STRING('End Despatch Date'),AT(213,172),USE(?String5),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@D08B),AT(303,172,81,10),USE(LOC:EndDate),IMM,FONT('Arial',8,010101H,FONT:bold),COLOR(COLOR:White),TIP('Enter the latest despatched date to show on report'),REQ
                           BUTTON,AT(387,168),USE(?PopCalendar),IMM,TRN,FLAT,LEFT,TIP('Click to show calendar'),ICON('lookupp.jpg')
                           BUTTON,AT(387,146),USE(?PopCalendar:1),TRN,FLAT,LEFT,TIP('Click to show calendar'),ICON('lookupp.jpg')
                           CHECK('Show Excel'),AT(432,190),USE(excel:Visible),LEFT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),TIP('Tick To Show Excel During Report Creation.<13,10>Only Tick if you are having problems' &|
   ' <13,10>with Excel reports.'),VALUE('1','0')
                           LIST,AT(192,202,296,156),USE(?List),IMM,VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)FJ@s1@60L(2)|F~Account Number~@s15@120L(2)|F~Company Name~@s30@'),FROM(Queue:Browse)
                           BUTTON,AT(496,250),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(496,282),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(496,316),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                           CHECK('All Repair Centres'),AT(192,190),USE(DoAll),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('Y','N')
                         END
                       END
                       BUTTON,AT(480,366),USE(?OkButton),TRN,FLAT,LEFT,TIP('Click to create Excel report'),ICON('printp.jpg'),DEFAULT
                       BUTTON,AT(548,366),USE(?CancelButton),TRN,FLAT,LEFT,TIP('Click to close this form'),ICON('cancelp.jpg')
                       PROMPT('Report Version'),AT(68,376),USE(?ReportVersion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDBTrdParty          CLASS(FileDropClass)             !File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

BRW6                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW6::Sort0:Locator  StepLocatorClass                 !Default Locator
    MAP
DateToString        PROCEDURE( DATE ), STRING
GetHeadAccount PROCEDURE( STRING )
GetSubAccount PROCEDURE( STRING )
GetWebJobNumber PROCEDURE( LONG ), STRING
LoadAUDSTATS   PROCEDURE( LONG, STRING ), LONG ! BOOL
LoadJOBS            PROCEDURE( LONG ), LONG ! BOOL
LoadJOBS_BatchNumber   PROCEDURE( LONG, LONG ), LONG ! BOOL
LoadJOBTHIRD   PROCEDURE( LONG ), LONG ! BOOL
LoadSUBTRACC   PROCEDURE( STRING ), LONG ! BOOL
LoadTRADEACC   PROCEDURE( STRING ), LONG ! BOOL
LoadTRDBATCH   PROCEDURE( STRING, LONG ), LONG ! BOOL
LoadTRDPARTY   PROCEDURE( STRING, LONG ), LONG ! BOOL
LoadUSERS            PROCEDURE( STRING ), LONG ! BOOL
LoadWEBJOB      PROCEDURE( LONG ), LONG ! BOOL
HoursBetween                PROCEDURE  (TIME, TIME),LONG

NumberToColumn PROCEDURE( LONG ), STRING
UpdateModelQueue  PROCEDURE( STRING, STRING )
UpdateSummaryQueue  PROCEDURE( STRING )
WriteColumn PROCEDURE( STRING, LONG = False )
WriteDebug      PROCEDURE( STRING )

WorkingDaysBetween          PROCEDURE  (DATE, DATE, BYTE, BYTE), LONG
WorkingHoursBetween         PROCEDURE  (DATE, DATE, TIME, TIME, LONG, LONG,BYTE),LONG
    END !MAP
!---   Excel EQUATES   -------------------------------------------------------------
xlCalculationManual    EQUATE(0FFFFEFD9h) ! XlCalculation
xlCalculationAutomatic EQUATE(0FFFFEFF7h) ! XlCalculation
!----------------------------------------------------

!----------------------------------------------------
xlNone       EQUATE(0FFFFEFD2h)
xlContinuous EQUATE(        1 ) ! xlFillStyle
xlThin       EQUATE(        2 ) ! XlBorderWeight
!----------------------------------------------------

!----------------------------------------------------
xlAutomatic   EQUATE(0FFFFEFF7h) ! Constants.
xlSolid       EQUATE(        1 ) ! Constants.
xlLeft        EQUATE(0FFFFEFDDh) ! Constants.
xlRight       EQUATE(0FFFFEFC8h) ! Constants.
xlCenter      EQUATE(0FFFFEFF4h) ! Constants.
xlLastCell    EQUATE(       11 ) ! Constants.
xlTop         EQUATE(0FFFFEFC0h) ! Constants.
xlBottom      EQUATE(0FFFFEFF5h) ! Constants.

xlTopToBottom EQUATE(        1 ) ! Constants.
!----------------------------------------------------

!----------------------------------------------------
xlDiagonalDown     EQUATE( 5) ! XlBordersIndex
xlDiagonalUp       EQUATE( 6) ! XlBordersIndex
xlEdgeLeft         EQUATE( 7) ! XlBordersIndex
xlEdgeTop          EQUATE( 8) ! XlBordersIndex
xlEdgeBottom       EQUATE( 9) ! XlBordersIndex
xlEdgeRight        EQUATE(10) ! XlBordersIndex
xlInsideVertical   EQUATE(11) ! XlBordersIndex
xlInsideHorizontal EQUATE(12) ! XlBordersIndex
!----------------------------------------------------

!----------------------------------------------------
xlA1         EQUATE(        1 ) ! XlReferenceStyle
xlR1C1       EQUATE(0FFFFEFCAh) ! XlReferenceStyle
!----------------------------------------------------

!----------------------------------------------------
xlFilterCopy    EQUATE(2) ! XlFilterAction
xlFilterInPlace EQUATE(1) ! XlFilterAction
!----------------------------------------------------

!----------------------------------------------------
xlGuess EQUATE(0) ! XlYesNoGuess
xlYes   EQUATE(1) ! XlYesNoGuess
xlNo    EQUATE(2) ! XlYesNoGuess
!----------------------------------------------------

!----------------------------------------------------
xlAscending  EQUATE(1) ! XlSortOrder
xlDescending EQUATE(2) ! XlSortOrder
!----------------------------------------------------

!----------------------------------------------------
xlLandscape EQUATE(2) ! XlPageOrientation
xlPortrait  EQUATE(1) ! XlPageOrientation
!----------------------------------------------------

!----------------------------------------------------
xlDownThenOver EQUATE( 1 ) ! XlOrder
xlOverThenDown EQUATE( 2 ) ! XlOrder
!-----------------------------------------------------------------------------------
!---   MS Office EQUATES   ---------------------------------------------------------
!----------------------------------------------------
msoCTrue          EQUATE(        1 ) ! MsoTriState
msoFalse          EQUATE(        0 ) ! MsoTriState
msoTriStateMixed  EQUATE(0FFFFFFFEh) ! MsoTriState
msoTriStateToggle EQUATE(0FFFFFFFDh) ! MsoTriState
msoTrue           EQUATE(0FFFFFFFFh) ! MsoTriState
!----------------------------------------------------

!----------------------------------------------------
msoScaleFromBottomRight EQUATE(2) ! MsoScaleFrom
msoScaleFromMiddle      EQUATE(1) ! MsoScaleFrom
msoScaleFromTopLeft     EQUATE(0) ! MsoScaleFrom
!----------------------------------------------------

!----------------------------------------------------
msoPropertyTypeBoolean EQUATE(2) ! MsoDocProperties
msoPropertyTypeDate    EQUATE(3) ! MsoDocProperties
msoPropertyTypeFloat   EQUATE(5) ! MsoDocProperties
msoPropertyTypeNumber  EQUATE(1) ! MsoDocProperties
msoPropertyTypeString  EQUATE(4) ! MsoDocProperties
!----------------------------------------------------
!-----------------------------------------------------------------------------------
!---   ProgressBar Declarations   --------------------------------------------------
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte
 
progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY,DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
!-----------------------------------------------------------------------------------
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::7:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW6.UpdateBuffer
   glo:Queue2.Pointer2 = tra:RecordNumber
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = tra:RecordNumber
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    LocalTag = 'Y'
  ELSE
    DELETE(glo:Queue2)
    LocalTag = ''
  END
    Queue:Browse.LocalTag = LocalTag
  IF (LocalTag = 'Y')
    Queue:Browse.LocalTag_Icon = 2
  ELSE
    Queue:Browse.LocalTag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::7:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW6.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = tra:RecordNumber
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::7:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW6.Reset
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::7:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::7:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::7:QUEUE = glo:Queue2
    ADD(DASBRW::7:QUEUE)
  END
  FREE(glo:Queue2)
  BRW6.Reset
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::7:QUEUE.Pointer2 = tra:RecordNumber
     GET(DASBRW::7:QUEUE,DASBRW::7:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = tra:RecordNumber
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::7:DASSHOWTAG Routine
   CASE DASBRW::7:TAGDISPSTATUS
   OF 0
      DASBRW::7:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::7:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::7:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW6.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!-----------------------------------------------------------------------------------
OKButtonPressed         ROUTINE
    DATA
    CODE
         
        !-----------------------------------------------------------------
        Access:TRADEACC_ALIAS.CLEARKEY(tra_ali:Account_Number_Key)
        SET(tra_ali:Account_Number_Key,tra_ali:Account_Number_Key)
        LOOP
          IF Access:TRADEACC_ALIAS.NEXT() THEN BREAK.
          IF tra_ali:Account_Number <> LocalHeadAccount THEN
             IF tra_ali:RemoteRepairCentre = 0 THEN CYCLE.
          END !IF
          IF tra_ali:Account_Number = 'XXXRRC' THEN CYCLE.
          IF DoAll <> 'Y' THEN
             glo:Queue2.Pointer2 = tra_ali:RecordNumber
             GET(glo:Queue2,glo:Queue2.Pointer2)
             IF ERROR() THEN CYCLE.
          END !IF
          CASE tra_ali:Account_Number
            OF LocalHeadAccount
                LOC:RepairCentreType = 'ARC'
                !MESSAGE('ARC')
            OF 'XXXRRC' ! Is this a Vodacom exclusive ?
                !WriteDebug('OKButtonPressed(XXXRRC)')
                CYCLE

          ELSE
                IF tra_ali:RemoteRepairCentre = 0
                    !WriteDebug('OKButtonPressed(NOT RRC)')
                    CYCLE
                END !IF

                LOC:RepairCentreType = 'RRC'

          END !CASE

          LOC:FileName = ''
          DO ExportSetup
          IF LOC:FileName = '' THEN CYCLE.
          DO ExportBody
          DO ExportFinalize
        END !LOOP
        POST(Event:CloseWindow)
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------------------------------------------
ExportSetup                             ROUTINE
    DATA
temp DATE
    CODE
        !-----------------------------------------------------------------
        CancelPressed = False
        !-----------------------------------------------------------------
        IF CLIP(LOC:FileName) = ''
            DO LoadFileDialog
        END !IF

        !INCLUDE('Version.INC')

!        IF LOC:FileName = ''
!            Case MessageEx('No filename chosen.<10,13>   Enter a filename then try again', LOC:ApplicationName,|
!                           'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
!                Of 1 ! &OK Button
!
!            End!Case MessageEx
!
!            CancelPressed = True
!
!            EXIT
!        END !IF LOC:FileName = ''
        IF LOC:FileName <> '' THEN
           IF LOWER(RIGHT(LOC:FileName, 4)) <> '.xls'
              LOC:FileName = CLIP(LOC:FileName) & '.xls'
           END !IF
           !-----------------------------------------------------------------
           SETCURSOR(CURSOR:Wait)

           DO ProgressBar_Setup
           !-----------------------------------------------------------------
           DO XL_Setup

           DO XL_AddWorkbook
           !-----------------------------------------------------------------
        END !IF
    EXIT
ExportBody                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        FREE(SummaryQueue)
        CLEAR(SummaryQueue)
        FREE(ReturnedQueue)
        CLEAR(ReturnedQueue)

        FREE(ManufacturerQueue)
        CLEAR(ManufacturerQueue)
        FREE(HeaderQueue)
        CLEAR(HeaderQueue)
        FREE(HeadAccount_Queue)
        CLEAR(HeadAccount_Queue)
        FREE(SubAccount_Queue)
        CLEAR(SubAccount_Queue)

        !-----------------------------------------------------------------
        DO CreateTitleSheet
        DO CreateDataSheets
        DO WriteHeadSummary
        !-----------------------------------------------------------------
    EXIT
ExportFinalize                          ROUTINE
    DATA
FilenameLength LONG
StartAt        LONG
SUBLength      LONG
    CODE
        !-----------------------------------------------------------------
        DO ResetClipboard

        IF CancelPressed
            DO XL_DropAllWorksheets
            DO XL_Finalize

            EXIT
        END !IF
        !-----------------------------------------------------------------
        Excel{'Sheets("Sheet3").Select'}
        Excel{'ActiveWindow.SelectedSheets.Delete'}

        Excel{'Sheets("Summary").Select'}
        Excel{'Range("A1").Select'}

        Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(LOC:FileName)) & '")'}
        Excel{'Application.ActiveWorkBook.Close()'}
        !-----------------------------------------------------------------
        DO XL_Finalize

        DO ProgressBar_Finalise
        !-----------------------------------------------------------------
        SETCURSOR()

!        IF MATCH(LOC:Filename, CLIP(LOC:DesktopPath) & '*')
!            FilenameLength = LEN(CLIP(LOC:Filename   ))
!            StartAt        = LEN(CLIP(LOC:DesktopPath)) + 1
!            SUBLength      = FilenameLength - StartAt + 1
!
!            Case MessageEx('Export Completed.'                                & |
!                           '<13,10>'                                          & |
!                           '<13,10>Spreadsheet saved to your Desktop'         & |
!                           '<13,10>'                                          & |
!                           '<13,10>' & SUB(LOC:Filename, StartAt, SUBLength),   |
!                         LOC:ApplicationName,                                   |
!                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
!            Of 1 ! &OK Button
!            End!Case MessageEx
!        ELSE
!            Case MessageEx('Export Completed.'                        & |
!                           '<13,10>'                                  & |
!                           '<13,10>Spreadsheet saved to '             & |
!                           '<13,10>'                                  & |
!                           '<13,10>' & CLIP(LOC:Filename),              |
!                         LOC:ApplicationName,                           |
!                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
!            Of 1 ! &OK Button
!            End!Case MessageEx
!        END !IF
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------------------------------------------
CreateTitleSheet                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        WriteDebug('CreateTitleSheet START')

        Excel{'ActiveWorkBook.Sheets("Sheet1").Select'}
        !-----------------------------------------------------------------
        LOC:Text          = 'Summary'
        sheet:TempLastCol = sheet:HeadLastCol

        DO CreateWorksheetHeader
        !-----------------------------------------------------------------
        DO XL_SetWorksheetPortrait

        Excel{'ActiveSheet.Columns("A:' & sheet:HeadLastCol & '").ColumnWidth'} = 30
        !-----------------------------------------------------------------
        Excel{'Range("A' & sheet:HeadSummaryRow & '").Select'}
            DO XL_SetBold
            Excel{'ActiveCell.Formula'} = 'Summary'

        Excel{'Range("B' & sheet:HeadSummaryRow & '").Select'}
            DO XL_SetBold
            Excel{'ActiveCell.Formula'} = 'Sent To 3rd Party'


        Excel{'Range("A' & sheet:HeadSummaryRow & ':' & sheet:HeadLastCol & sheet:HeadSummaryRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------------------------
        ! Prepare For summary details
        !
        Excel{'Range("A' & sheet:HeadSummaryRow+1 & '").Select'}

        WriteDebug('CreateTitleSheet END')
        !-----------------------------------------------------------------
    EXIT
CreateDataSheets                                             ROUTINE
    DATA
    CODE
        !---------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF

        WriteDebug('CreateDataSheets START')
        !---------------------------------------------------------
        DO InitColumns
        !---------------------------------------------------------
        IF Option1 = 1
            WriteDebug('CreateDataSheets Option1 = 1')

            Access:TRDPARTY.ClearKey(trd:Company_Name_Key)
                trd:Company_Name = ''
            SET(trd:Company_Name_Key, trd:Company_Name_Key)

            LOOP WHILE Access:TRDPARTY.NEXT() = Level:Benign
                IF CancelPressed
                    EXIT
                END !IF

                LOC:CompanyName = trd:Company_Name
                DO CreateDataSheet
            END !LOOP

        ELSE
            WriteDebug('CreateDataSheets ELSE Option1')
            DO CreateDataSheet

        END !IF

        WriteDebug('CreateDataSheets End')
        !---------------------------------------------------------
    EXIT

CreateDataSheet                                                      ROUTINE
    DATA
First long(True)
    CODE
        !---------------------------------------------------------
        WriteDebug('CreateDataSheet(' & CLIP(LOC:CompanyName) & ')')
        !---------------------------------------------------------
        !Progress:Text    = CLIP(LOC:CompanyName)
        RecordsToProcess = (LOC:EndDate - LOC:StartDate) * 1000

        DO ProgressBar_Loop
        !---------------------------------------------------------
        DO XL_AddSheet
            DO XL_SetWorksheetLandscape

        LOC:SectionName   = LEFT(CLIP(LOC:CompanyName), 30)
        IF CLIP(LOC:SectionName) = ''
            LOC:SectionName = 'Detailed'
        END !IF
        excel:CommentText = ''
        sheet:TempLastCol = sheet:DataLastCol

        DO CreateSectionHeader
        !---------------------------------------------------------
        LOC:CountJobs = 0

        Save_trb_ID = Access:TRDBATCH.SaveFile()
        Access:TRDBATCH.ClearKey(trb:CompanyDespatchedKey)
        trb:Company_Name   = loc:CompanyName
        trb:DateDespatched = loc:StartDate
        Set(trb:CompanyDespatchedKey,trb:CompanyDespatchedKey)
        Loop
            If Access:TRDBATCH.NEXT()
               Break
            End !If
            If trb:Company_Name   <> loc:CompanyName      |
            Or trb:DateDespatched > loc:EndDate       |
                Then Break.  ! End If

            !-----------------------------------------------------
            WriteDebug('CreateDataSheet(WriteColumns, Date="' & FORMAT(trb:Date, @D8) & '", StartDate="' & FORMAT(LOC:StartDate, @D8) & '", EndDate="' & FORMAT(LOC:EndDate, @D8) & '")')

            Access:JOBS.CLEARKEY(job:Ref_Number_Key)
            job:Ref_Number = trb:Ref_Number
            IF ~Access:JOBS.Fetch(job:Ref_Number_Key) THEN
               Access:SUBTRACC.CLEARKEY(sub:Account_Number_Key)
               sub:Account_Number = job:Account_Number
               IF Access:SUBTRACC.Fetch(sub:Account_Number_Key) THEN
                  IF job:Account_Number <> tra_ali:Account_Number
                    IF LOC:RepairCentreType = 'RRC'
                       CYCLE
                    END
                  END
               ELSE
                   IF sub:Main_Account_Number <> tra_ali:Account_Number
                     IF LOC:RepairCentreType = 'RRC'
                       CYCLE
                     END
                   END
               END !IF
            ELSE
                CYCLE
            END !IF
            DO WriteColumns
            !-----------------------------------------------------
        End !Loop
        Access:TRDBATCH.RestoreFile(Save_trb_ID)

        DO ProgressBar_LoopPost
        IF CancelPressed
            EXIT
        END !IF
        !---------------------------------------------------------
        RecordCount = LOC:CountJobs

        DO WriteDataSummary

        WriteDebug('CreateDataSheet END')
        !---------------------------------------------------------
    EXIT
!-----------------------------------------------------------------------------------
CreateSectionHeader                                        ROUTINE
    DATA
LastColumn STRING(1)
    CODE
        !-----------------------------------------------       
        LOC:Text = CLIP(LOC:SectionName)

        DO CreateWorksheetHeader
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataSectionRow & ':' & sheet:DataLastCol & sheet:DataSectionRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & sheet:DataSectionRow & '").Select'}
            Excel{'ActiveCell.Formula'}         = 'Section Name:'
            DO XL_ColRight
                Excel{'ActiveCell.Formula'}     = CLIP(LOC:SectionName)
                DO XL_SetBold
                DO XL_HorizontalAlignmentLeft

        Excel{'Range("E' & sheet:DataSectionRow & '").Select'}
            Excel{'ActiveCell.Formula'}         = 'Total Records:'
            DO XL_HorizontalAlignmentRight
            DO XL_ColRight
                Excel{'ActiveCell.Formula'}     = 0
                DO XL_HorizontalAlignmentLeft
        !-----------------------------------------------
        DO SetColumns

        Excel{'Range("A' & sheet:DataHeaderRow+1 & '").Select'}
        !-----------------------------------------------
    EXIT
CreateWorksheetHeader                                                               ROUTINE
    DATA
CurrentRow LONG(4)
    CODE
        !-----------------------------------------------       
        Excel{'ActiveSheet.Name'} = CLIP(LOC:Text)
        !-----------------------------------------------       
        Excel{'Range("A1").Select'}
            Excel{'ActiveCell.Formula'}   = CLIP(LOC:ProgramName) & ' ' & CLIP(tra_ali:Account_Number)
            DO XL_SetBold
            Excel{'ActiveCell.Font.Size'} = 16

        Excel{'Range("D3").Select'}
            Excel{'ActiveCell.Formula'}   = Clip(tmp:VersionNUmber)
            DO XL_SetBold
            Excel{'ActiveCell.Font.Size'} = 8


        Excel{'Range("A1:' & sheet:TempLastCol & '1").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------
        Excel{'Range("A3:' & sheet:TempLastCol & '3").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A3").Select'}
            Excel{'ActiveCell.Formula'}          = 'Criteria'
            DO XL_SetBold
        !-----------------------------------------------
        Excel{'Range("A' & CurrentRow & '").Select'}
            Excel{'ActiveCell.FormulaR1C1'}      = 'Start Date:'
            DO XL_ColRight
                DO XL_FormatDate
                DO XL_HorizontalAlignmentLeft
                Excel{'ActiveCell.Formula'}      = LEFT(FORMAT(LOC:StartDate, @D8))

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.FormulaR1C1'}      = 'End Date:'
                DO XL_ColRight
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft
                    Excel{'ActiveCell.Formula'}      = LEFT(FORMAT(LOC:EndDate, @D8))

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}          = 'Created By:'
                DO XL_ColRight
                    Excel{'ActiveCell.Formula'}      = LOC:UserName

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}          = 'Date Created:'
                DO XL_ColRight
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft
                    Excel{'ActiveCell.Formula'}      = FORMAT(TODAY(), @D8)

        Excel{'Range("A4:' & sheet:TempLastCol & CurrentRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------
    EXIT
!-----------------------------------------------------------------------------------
InitColumns                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        FREE(HeaderQueue)

        head:ColumnName       = 'SB Job Number'
            head:ColumnWidth  = 13.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)

        head:ColumnName       = 'Franchise/Branch Name'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64)
            ADD(HeaderQueue)

        head:ColumnName       = 'IMEI Number'
            head:ColumnWidth  = 15.00
            head:NumberFormat = '###############'
            ADD(HeaderQueue)

        head:ColumnName       = 'MSN'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Manufacturer'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Model Number'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Sub Account Name'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Booking In Date'
            head:ColumnWidth  = 12.75
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)

        head:ColumnName       = 'Batch Date'
            head:ColumnWidth  = 12.75
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)

        head:ColumnName       = 'Date Despatched'
            head:ColumnWidth  = 12.75
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)

        head:ColumnName       = '3rd Party TAT'
            head:ColumnWidth  = 12.75
            head:NumberFormat = '###0'
            ADD(HeaderQueue)

        head:ColumnName       = 'Status Working Hours'
            head:ColumnWidth  = 20.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)

        head:ColumnName       = 'Returned'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        !-----------------------------------------------
        sheet:DataLastCol = NumberToColumn(RECORDS(HeaderQueue))
        !-----------------------------------------------
    EXIT

!        head:ColumnName       = 'zzz'
!            head:ColumnWidth  = 11.00
!            head:NumberFormat = '###0'
!            head:NumberFormat = '#,##0.00'
!            head:NumberFormat = '0'
!            head:NumberFormat = '0000'
!            head:NumberFormat = excel:DateFormat
!            head:NumberFormat = chr(64) ! Text (AT) Sign
!            ADD(HeaderQueue)
!
!        head:ColumnName       = 'zzz'
!            head:ColumnWidth  = 11.00
!            head:NumberFormat = '###0'
!            head:NumberFormat = '#,##0.00'
!            head:NumberFormat = '0'
!            head:NumberFormat = '0000'
!            head:NumberFormat = excel:DateFormat
!            head:NumberFormat = chr(64) ! Text (AT) Sign
!            ADD(HeaderQueue)
!
!        head:ColumnName       = 'zzz'
!            head:ColumnWidth  = 11.00
!            head:NumberFormat = '###0'
!            head:NumberFormat = '#,##0.00'
!            head:NumberFormat = '0'
!            head:NumberFormat = '0000'
!            head:NumberFormat = excel:DateFormat
!            head:NumberFormat = chr(64) ! Text (AT) Sign
!            ADD(HeaderQueue)
!
!        head:ColumnName       = 'zzz'
!            head:ColumnWidth  = 11.00
!            head:NumberFormat = '###0'
!            head:NumberFormat = '#,##0.00'
!            head:NumberFormat = '0'
!            head:NumberFormat = '0000'
!            head:NumberFormat = excel:DateFormat
!            head:NumberFormat = chr(64) ! Text (AT) Sign
!            ADD(HeaderQueue)
!
!        head:ColumnName       = 'zzz'
!            head:ColumnWidth  = 11.00
!            head:NumberFormat = '###0'
!            head:NumberFormat = '#,##0.00'
!            head:NumberFormat = '0'
!            head:NumberFormat = '0000'
!            head:NumberFormat = excel:DateFormat
!            head:NumberFormat = chr(64) ! Text (AT) Sign
!            ADD(HeaderQueue)
FormatColumns                                                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF RecordCount < 1
            EXIT
        END !IF
        !-----------------------------------------------
        LOOP x# = 1 TO RECORDS(HeaderQueue)
            !-------------------------------------------
            GET(HeaderQueue, x#)
            IF ERRORCODE()
                CYCLE
            END !IF

            Excel{'Range("' & CHR(64+x#) & sheet:DataHeaderRow+1 & ':' & CHR(64+x#) & sheet:DataHeaderRow+RecordCount & '").Select'}
                Excel{'Selection.NumberFormat'} = head:NumberFormat
                !DO XL_HighLight ! DEBUG
                DO XL_HorizontalAlignmentRight
            !-------------------------------------------
        END !LOOP
        !-----------------------------------------------
    EXIT
SetColumns                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow & ':' & sheet:DataLastCol & sheet:DataHeaderRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & sheet:DataHeaderRow  & '").Select'}
        !-----------------------------------------------
        LOOP x# = 1 TO RECORDS(HeaderQueue)
            GET(HeaderQueue, x#)

            Excel{'ActiveCell.Formula'}         = head:ColumnName
                Excel{'ActiveCell.ColumnWidth'} = head:ColumnWidth
                DO XL_ColRight
        END !LOOP
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow+1  & '").Select'}
        Excel{'ActiveWindow.FreezePanes'} = True
        !-----------------------------------------------
    EXIT
WriteColumns                                                       ROUTINE

    DATA
StatusWorkingDays LONG
local:ReturnTime  TIME
local:ReturnDate  Date
local:SendTime    TIME
local:SendDate    Date
local:tattime     Long
    CODE
        !-----------------------------------------------
        WriteDebug('WriteColumns(' & trb:Ref_Number & ')')

        DO ProgressBar_Loop
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------
        IF NOT LoadJOBS(trb:Ref_Number)
            EXIT
!        ELSIF NOT LoadJOBTHIRD(trb:Ref_Number)
 !           EXIT
        END !IF

        Access:JOBSE.Clearkey(jobe:RefNumberKey)
        jobe:RefNumber  = job:Ref_Number
        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            !Found

        Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            !Error
        End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

        Access:JOBTHIRD.ClearKey(jot:ThirdPartyKey)
        jot:ThirdPartyNumber = trb:RecordNumber
        If Access:JOBTHIRD.TryFetch(jot:ThirdPartyKey) = Level:Benign
            !Found

        Else !If Access:JOBTHIRD.TryFetch(jot:ThirdPartyKey) = Level:Benign
            !Error
            Exit
        End !If Access:JOBTHIRD.TryFetch(jot:ThirdPartyKey) = Level:Benign

        IF LoadAUDSTATS( job:Ref_Number, job:Current_Status)
            StatusWorkingDays = WorkingDaysBetween(aus:DateChanged, TODAY(), haQ:IncludeSaturday, haQ:IncludeSunday)
        ELSE
            StatusWorkingDays = 0
        END !IF LoadAUDSTATS( job:Ref_Number, job:Current_Status)
        WebJOB_OK         = LoadWEBJOB(trb:Ref_Number)
        GetSubAccount(job:Account_Number)
        !===============================================
        RecordCount      += 1
        DO ProgressBar_Loop
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------
        LOC:CountJobs    += 1
        LOC:CountAllJobs += 1
        

        UpdateSummaryQueue(LOC:CompanyName)
!        UpdateModelQueue(job:Manufacturer, job:Model_Number)
        !-----------------------------------------------

        !Job Number
        WriteColumn(trb:Ref_Number)

        !Franchise Branch Name
        !Save Account Details, and then get the booking account name
        Save_tra_ID = Access:TRADEACC.SaveFile()
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = wob:HeadAccountNumber
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            WriteColumn(tra:Company_Name)
        Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
        End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        Access:TRADEACC.RestoreFile(Save_tra_ID)

        


        IF WebJOB_OK THEN
!           WriteColumn( wob:JobNumber )
        ELSE
!            WriteColumn ('Unable to load WebJob')
        END !IF
        !IMEI Number
        WriteColumn( '''' & jot:OutIMEI, True  )
        !MSN
        WriteColumn( job:MSN            )
        !Manufacturer
        WriteColumn( job:Manufacturer   )
        !Model Number
        WriteColumn( job:Model_Number   )
        !Sub Account Number
        WriteColumn( saQ:AccountName   )
        !Booking In Date
        !Get the date booked, or the date that waybill confirmed at ARC
        If jobe:WebJob
            Access:AUDSTATS.ClearKey(aus:NewStatusKey)
            aus:RefNumber = job:Ref_Number
            aus:Type      = 'JOB'
            aus:NewStatus = tmp:StatusReceivedAtARC
            If Access:AUDSTATS.TryFetch(aus:NewStatusKey) = Level:Benign
                !Found
                WriteColumn( DateToString(aus:DateChanged)           )
            Else !If Access:AUDSTATS.TryFetch(aus:NewStatusKey) = Level:Benign
                !Error
                WriteColumn( DateToString(job:date_booked)           )
            End !If Access:AUDSTATS.TryFetch(aus:NewStatusKey) = Level:Benign
        Else !If jobe:WebJob
            WriteColumn( DateToString(job:date_booked)           )
        End !If jobe:WebJob
        !Batch Date
        WriteColumn( DateToString(trb:Date)           )

        !Date Despatched

        !Workout Sent and Returned Start TIMES
        local:Returntime = trb:TimeReturn
        local:ReturnDate = trb:DateReturn
!
!        Save_aud_ID = Access:AUDIT.SaveFile()
!        Access:AUDIT.ClearKey(aud:Action_Key)
!        aud:Ref_Number = job:Ref_Number
!        aud:Action     = '3RD PARTY AGENT: UNIT RETURNED'
!        Set(aud:Action_Key,aud:Action_Key)
!        Loop
!            If Access:AUDIT.NEXT()
!               Break
!            End !If
!            If aud:Ref_Number <> job:Ref_Number      |
!            Or aud:Action     <> '3RD PARTY AGENT: UNIT RETURNED'      |
!                Then Break.  ! End If
!            If Instring('BATCH NUMBER: ' & Clip(trb:Batch_Number),aud:Notes,1,1)
!                local:ReturnTime = aud:Time
!                local:ReturnDate = aud:Date
!                Break
!            End !If Instring('BATCH NUMBER: ' & Clip(trb:Batch_Number),1,1)
!        End !Loop
!        Access:AUDIT.RestoreFile(Save_aud_ID)

        local:Sendtime = trb:TimeDespatched
        local:SendDate = trb:DateDespatched

        If trb:DateDespatched = trb:Date
            !For backwards compatibility, if the new despatch date
            !is the same and the batch creation date, then
            !this might mean it's an older job - L842 (DBH: 21-07-2003)
            Save_aud_ID = Access:AUDIT.SaveFile()
            Access:AUDIT.ClearKey(aud:Action_Key)
            aud:Ref_Number = job:Ref_Number
            aud:Action     = '3RD PARTY AGENT: UNIT SENT'
            Set(aud:Action_Key,aud:Action_Key)
            Loop
                If Access:AUDIT.NEXT()
                   Break
                End !If
                If aud:Ref_Number <> job:Ref_Number      |
                Or aud:Action     <> '3RD PARTY AGENT: UNIT SENT'      |
                    Then Break.  ! End If
                Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
                aud2:AUDRecordNumber = aud:Record_Number
                IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
                    If Instring('BATCH NUMBER: ' & Clip(trb:Batch_Number),aud2:Notes,1,1)
                        !Only count if the date is higher than the recorded Date Despatched.
                        !This will probably mean it's an older job - L842 (DBH: 21-07-2003)
                        If aud:Date > trb:DateDespatched
                            local:SendTime = aud:Time
                            local:SendDate = aud:Date
                        End !If aud:Date > trb:DateDespatched
                        Break
                    End !If Instring('BATCH NUMBER: ' & Clip(trb:Batch_Number),1,1)

                END ! IF
            End !Loop
            Access:AUDIT.RestoreFile(Save_aud_ID)
        End !If trb:DateDespatched = trb:Date

        !Vodacom don't always despatch before receving, therefore there is no
        !despatch date. In that case, use the Audit date, which SHOULD be the same
        If jot:DateDespatched = 0 And local:SendDate <> 0
            jot:DateDespatched = local:SendDate
        End !If jot:DateDespatched = 0 And local:SentDate <> 0

        WriteColumn( DateToString(jot:DateDespatched) )

        !3rd Party TAT
        !Status Working Hours
        If jot:DateIn = 0
            WriteColumn(0)
            !Don't count weekends and 24hours a day - L876 (DBH: 14-07-2003)
            WriteColumn(WorkingHoursBetween(jot:DateDespatched,Today(),local:SendTime,Clock(),0,0,1) )

        Else !If jot:DateDespatched = 0
            IF local:SendDate <> 0
                local:Tattime = WorkingHoursBetween(local:SendDate,jot:DateIn,local:SendTime,local:ReturnTime,0,0,1)  !Added By Neil
            ELSE
                local:Tattime = WorkingHoursBetween(jot:DateDespatched,jot:DateIn,local:SendTime,local:ReturnTime,0,0,1)
            END
            WriteColumn(local:Tattime)

            WriteColumn(0)
        End !If jot:DateDespatched = 0

        !Returned
        IF jot:DateIn <> 0
            WriteColumn ( 'RETURNED' )
            !Add to returned queue
            retque:CompanyName  = loc:CompanyName
            Get(ReturnedQueue,retque:CompanyName)
            If Error()
                retque:CompanyName  = loc:CompanyName
                retque:Count = 1
                retque:TotalHours = local:Tattime
                Add(ReturnedQueue)
            Else !If Error()
                retque:Count += 1
                retque:TotalHours += local:Tattime
                Put(ReturnedQueue)
            End !If Error()

        ELSE
            WriteColumn ( 'AT 3RD PARTY SITE' )
        END
        !WriteColumn( GetWebJobNumber(job:Ref_Number)  )
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            DO PassViaClipboard
        END !IF
        !-----------------------------------------------
        DO XL_ColFirst
        DO XL_RowDown
        !-----------------------------------------------
    EXIT
!-----------------------------------------------------------------------------------
WriteDataSummary                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        ! summary details
        !                                                                             
        Excel{'ActiveWorkBook.Sheets("' & CLIP(LOC:SectionName) & '").Select'}

        DO FormatColumns

        Excel{'Range("F' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentLeft
            Excel{'ActiveCell.Formula'}         = RecordCount

        Excel{'Range("G' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentRight
            Excel{'ActiveCell.Formula'}         = 'Showing'

        Excel{'Range("H' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentLeft
            Excel{'ActiveCell.Formula'}         = '=SUBTOTAL(2, A' & sheet:DataHeaderRow+1 & ':A' & sheet:DataHeaderRow+RecordCount & ')'

        Excel{'Range("A' & sheet:DataHeaderRow  & '").Select'}
            DO XL_DataAutoFilter
        !-----------------------------------------------------------------      
    EXIT
WriteHeadSummary                                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        ! summary sheet details
        !
        Excel{'ActiveWorkBook.Sheets("Summary").Select'}
        !-----------------------------------------------------------------
        IF LOC:CountAllJobs < 1
            Excel{'ActiveCell.Formula'} = 'No Third Party Repairers Found'

            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO WriteSummary
        DO WriteReturnedSummary
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------------------------------------------
WriteSummary                                        ROUTINE
    DATA
QueueIndex   LONG
ResultsCount LONG
FirstRow     LONG
LastRow      LONG
local:TotalCount Long
    CODE
        !-----------------------------------------------------------------
        ! Totals
        !
        ResultsCount = RECORDS(SummaryQueue)
        WriteDebug('WriteSummary(' & ResultsCount  & ')')

        IF ResultsCount < 1
            EXIT
        END !IF
        !-----------------------------------------------------------------
        FirstRow = Excel{'ActiveCell.Row'}  ! first blank line
!        Excel{'Range("A' & FirstRow & ':' & sheet:HeadLastCol & FirstRow & '").Select'}
!            DO XL_SetTitle
!            DO XL_SetBorder

!        Excel{'Range("A' & FirstRow & '").Select'}
!            Excel{'ActiveCell.Offset(0, 0).Formula'}          = 'Third Party Repairer'
!            Excel{'ActiveCell.Offset(0, 1).Formula'}          = 'Count'

!        FirstRow += 1                      ! headers row
        Excel{'Range("A' & FirstRow & '").Select'}

        LastRow   = FirstRow + ResultsCount

        SORT(SummaryQueue, +sq:CompanyName)

        local:TotalCount = 0

        LOOP QueueIndex = 1 TO RECORDS(SummaryQueue)
            !-------------------------------------------------------------
            GET(SummaryQueue, QueueIndex)

            Excel{'ActiveCell.Offset(0, 0).Formula'}          = sq:CompanyName
            Excel{'ActiveCell.Offset(0, 1).Formula'}          = sq:JobCount
            local:Totalcount += sq:JobCount

            DO XL_RowDown
            !-------------------------------------------------------------
        END !LOOP

!        Excel{'Range("A' & FirstRow-1 & ':' & sheet:HeadLastCol & LastRow & '").Select'}
!            Excel{'Selection.AutoFilter'}

        Excel{'Range("A' & LastRow+1 & '").Select'}
        Excel{'ActiveCell.Offset(0, 0).Formula'}          = 'Total'
        Excel{'ActiveCell.Offset(0, 1).Formula'}          = local:TotalCount

        !-----------------------------------------------------------------
    EXIT

WriteManufacturerSummary                                              ROUTINE
    DATA
QueueIndex   LONG
ResultsCount LONG
FirstRow     LONG
LastRow      LONG
    CODE

        !Was the manufacturer summary.. is now the returned and average tat summary
        !-----------------------------------------------------------------
        ! summary sheet details
        !
        Excel{'ActiveWorkBook.Sheets("Summary").Select'}
        !-----------------------------------------------------------------
        ResultsCount = RECORDS(ManufacturerQueue)

        WriteDebug('WriteManufacturerSummary(' & ResultsCount  & ')')

        IF ResultsCount < 1
            EXIT
        END !IF
        !-----------------------------------------------------------------
        ! Totals
        !
        FirstRow = Excel{'ActiveCell.Row'}  ! headers row

        Excel{'Range("A' & FirstRow & ':' & sheet:HeadLastCol & FirstRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & FirstRow & '").Select'}
            DO XL_SetBold
            Excel{'ActiveCell.Offset(0, 0).Formula'}          = 'Summary'
            DO XL_SetBold
            Excel{'ActiveCell.Offset(0, 1).Formula'}          = 'Returned From 3rd Party'
            DO XL_SetBold
            Excel{'ActiveCell.Offset(0, 2).Formula'}          = 'Average TAT (Hours)'

        FirstRow += 1                      
        LastRow   = FirstRow + ResultsCount

        Excel{'Range("A' & FirstRow & '").Select'}
        SORT(ManufacturerQueue, +manQ:CompanyName, +manQ:ModelNumber)

        LOOP QueueIndex = 1 TO ResultsCount
            !-------------------------------------------------------------
            GET(ManufacturerQueue, QueueIndex)

            Excel{'ActiveCell.Offset(0, 0).Formula'}          = manQ:CompanyName
            Excel{'ActiveCell.Offset(0, 1).Formula'}          = '''' & manQ:ModelNumber
            Excel{'ActiveCell.Offset(0, 2).Formula'}          = manQ:Count

            DO XL_RowDown
            !-------------------------------------------------------------
        END !LOOP

        Excel{'Range("A' & LastRow+1 & '").Select'}
        !-----------------------------------------------------------------
    EXIT
WriteReturnedSummary                                              ROUTINE
    DATA
QueueIndex   LONG
ResultsCount LONG
FirstRow     LONG
LastRow      LONG
local:TotalReturnedCount Long
local:TotalAverage Long
    CODE

        !Was the manufacturer summary.. is now the returned and average tat summary
        !-----------------------------------------------------------------
        ! summary sheet details
        !
        Excel{'ActiveWorkBook.Sheets("Summary").Select'}
        !-----------------------------------------------------------------
        ResultsCount = RECORDS(ReturnedQueue)


        IF ResultsCount < 1
            EXIT
        END !IF
        !-----------------------------------------------------------------
        ! Totals
        !
        FirstRow = Excel{'ActiveCell.Row'} + 2 ! headers row

        Excel{'Range("A' & FirstRow & ':' & sheet:HeadLastCol & FirstRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & FirstRow & ':C' & FirstRow & '").Select'}
            DO XL_SetBold

        Excel{'Range("A' & FirstRow & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'}          = 'Summary'
            Excel{'ActiveCell.Offset(0, 1).Formula'}          = 'Returned From 3rd Party'
            Excel{'ActiveCell.Offset(0, 2).Formula'}          = 'Average TAT (Hours)'



        FirstRow += 1                      
        LastRow   = FirstRow + ResultsCount

        Excel{'Range("A' & FirstRow & '").Select'}

        Sort(ReturnedQueue,retque:CompanyName)

        local:TotalReturnedCount = 0

        Loop x# = 1 To Records(ReturnedQueue)
            Get(ReturnedQueue,x#)
            Excel{'ActiveCell.Offset(0, 0).Formula'}          = retque:CompanyName
            Excel{'ActiveCell.Offset(0, 1).Formula'}          = retque:Count
            Excel{'ActiveCell.Offset(0, 2).Formula'}          = INT(retque:TotalHours/retque:Count)
            DO XL_RowDown
            local:TotalReturnedCount += retque:Count
            local:TotalAverage += retque:TotalHours

        End !Loop x# = 1 To Records(ReturnedQueue)

        Excel{'Range("A' & LastRow+1 & '").Select'}
        Excel{'ActiveCell.Offset(0, 0).Formula'}          = 'Total'
        Excel{'ActiveCell.Offset(0, 1).Formula'}          = local:TotalReturnedCount
        Excel{'ActiveCell.Offset(0, 2).Formula'}          = INT(local:TotalAverage/local:TotalReturnedCount)

        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------------------------------------------
PassViaClipboard                                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            clip:OriginalValue = CLIPBOARD()
            clip:Saved         = True
        END !IF
        !-----------------------------------------------
        SETCLIPBOARD(CLIP(clip:Value))
            Excel{'ActiveSheet.Paste()'}
        clip:Value = ''
        !-----------------------------------------------
    EXIT

ResetClipboard                                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            EXIT
        END !IF

        SETCLIPBOARD(clip:OriginalValue)
        
        clip:Saved = False
        !-----------------------------------------------
    EXIT
!-----------------------------------------------------------------------------------
GetUserName                            ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE
        !-----------------------------------------------
        CommandLine = CLIP(COMMAND(''))

!        LOC:UserName = LOC:ApplicationName
!
!        Case MessageEx('Fail User Check For "' & CLIP(CommandLine) & '"?', LOC:ApplicationName & ' DEBUG',|
!                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
!            Of 1 ! &Yes Button
!                POST(Event:CloseWindow)
!
!                EXIT
!            Of 2 ! &No Button
!                EXIT
!        End!Case MessageEx
        !-----------------------------------------------
        tmpPos = INSTRING('%', CommandLine)
        IF NOT tmpPos
            Case Missive('You must run this report from ServiceBase''s "Custom Reports".','ServiceBase 3g',|
                           'midea.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

            POST(Event:CloseWindow)

           EXIT
        END !IF tmpPos
        !-----------------------------------------------
        SET(USERS)
        Access:USERS.Clearkey(use:Password_Key)
        glo:Password = CLIP(SUB(CommandLine, tmpPos + 1, 30))
      
        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key)
            Case Missive('Error! Cannot find the user''s details.','ServiceBase 3g',|
                           'midea.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

            POST(Event:CloseWindow)

           EXIT
        End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !-----------------------------------------------
        LOC:UserName = use:Forename

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = use:Surname
        ELSE
            LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname 
        END !IF

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = '<' & use:User_Code & '>'
        END !IF
        !-----------------------------------------------
    EXIT
GetFileName                             ROUTINE ! Generate default file name
    DATA
local:Desktop         CString(255)
DeskTopExists   BYTE
ApplicationPath STRING(255)
excel:ProgramName       Cstring(255)
    CODE
    excel:ProgramName = 'Third Party Despatch Report'
    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

    !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excel:ProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    loc:Path = local:Desktop
    loc:FileName = Clip(local:Desktop) & '\' & CLIP(tra_ali:Account_Number) & ' ' & Format(Today(),@d12)
!
!
!GetFileName                             ROUTINE ! Generate default file name
!    DATA
!Desktop         CString(255)
!DeskTopExists   BYTE
!ApplicationPath STRING(255)
!    CODE
!        !-----------------------------------------------
!        ! Generate default file name
!        !
!        !-----------------------------------------------
!        ! 4 Dec 2001 John Stop using def:Export_Path as previously directed
!        ! 4 Dec 2001 John Use generated date to create file name not parameters LOC:StartDate,LOC:EndDate
!        !
!        Include('Version.inc')
!        IF CLIP(LOC:FileName) <> ''
!            ! Already generated 
!            EXIT
!        END !IF
!
!        DeskTopExists = False
!
!        SHGetSpecialFolderPath( GetDesktopWindow(), Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
!        LOC:DesktopPath = Desktop
!
!        ApplicationPath = Desktop & '\' & LOC:ApplicationName & ' Export'
!        IF EXISTS(CLIP(ApplicationPath))
!            LOC:Path      = CLIP(ApplicationPath) & '\'
!            DeskTopExists = True
!
!        ELSE
!            Desktop  = CLIP(ApplicationPath)
!
!            IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!            ELSE
!!                MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path      = CLIP(ApplicationPath) & '\'
!                !DeskTopExists = True
!            END !IF
!
!        END !IF
!
!        IF DeskTopExists
!            ApplicationPath = CLIP(LOC:Path) & CLIP(LOC:ProgramName)
!            Desktop         = CLIP(ApplicationPath)
!
!            IF NOT EXISTS(CLIP(ApplicationPath))
!                IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                    MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!                ELSE
!!                    MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!                END !IF
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path = CLIP(ApplicationPath) & '\'
!            END !IF
!        END !IF
!
!        LOC:FileName = SHORTPATH(CLIP(LOC:Path)) & CLIP(tra_ali:Account_Number) & ' VODR0045 ' & FORMAT(TODAY(), @D12) & '.xls'
!        !-----------------------------------------------
!    EXIT
LoadFileDialog                          ROUTINE ! Ask user if file name is empty
    DATA
OriginalPath STRING(255)
    CODE
        !-----------------------------------------------
        IF CLIP(LOC:Filename) = ''
            DO GetFileName 
        END !IF

!        OriginalPath = PATH()
!        SETPATH(LOC:Path) ! Required for Win95/98
!            IF NOT FILEDIALOG('Save Spreadsheet', LOC:Filename, 'Microsoft Excel Workbook|*.XLS', |
!                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)
!
!                LOC:Filename = ''
!            END !IF
!        SETPATH(OriginalPath)

        UPDATE()
        DISPLAY()
        !-----------------------------------------------
    EXIT
!-----------------------------------------------------------------------------------
ProgressBar_Setup                       ROUTINE
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
            RecordsPerCycle      = 25
            RecordsProcessed     = 0
            RecordsToProcess     = 10 !***The Number Of Records, or at least a guess***
            PercentProgress      = 0
            Progress:thermometer = 0

            thiswindow.reset(1) !This may error, if so, remove this line.
            open(progresswindow)
            progresswindow{PROP:Timer} = 1
         
            ?progress:userstring{prop:text} = 'Running...'
            ?progress:pcttext{prop:text}    = '0% Completed'
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***

ProgressBar_LoopPre          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***

        RecordsProcessed = 0
        RecordCount      = 0

        !?Progress:UserString{PROP:Text} = CLIP(Progress:Text)

        !RecordsToProcess = RECORDS(JOBS) !***The Number Of Records, or at least a guess***
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
        !-----------------------------------------------
    EXIT

ProgressBar_Loop             ROUTINE
    DATA
newPctText STRING(255)
    CODE
        !-----------------------------------------------
        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****

        Do ProgressBar_GetNextRecord2 !Necessary

        Do ProgressBar_CancelCheck
        If CancelPressed
            CLOSE(ProgressWindow)
        End!If 
        !-----------------------------------------------
        newPctText = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

        IF ?progress:pcttext{prop:text} <> newPctText
            ?progress:pcttext{prop:text} = newPctText

            Display()
        END !IF

        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****
        !-----------------------------------------------
    EXIT

ProgressBar_LoopPost         ROUTINE
    DATA
newPctText STRING(255)
    CODE
        !-----------------------------------------------
        ?Progress:UserString{PROP:Text} = ' Done(' & RecordsProcessed & '/' & RecordCount & ')'

        newPctText = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

        IF ?progress:pcttext{prop:text} <> newPctText
            ?progress:pcttext{prop:text} = newPctText
        END !IF
        !-----------------------------------------------
    EXIT

ProgressBar_Finalise                        ROUTINE
        !**** End Of Loop ***
            Do ProgressBar_EndPrintRun
            close(progresswindow)
        !**** End Of Loop ***
        ! when in report procedure->LocalResponse = RequestCompleted

ProgressBar_GetNextRecord2                  routine
    recordsprocessed += 1
    recordsthiscycle += 1

    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        !?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end

ProgressBar_CancelCheck                     routine
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept

    If cancel# = 1
        BEEP(BEEP:SystemAsterisk)  ;  YIELD()
        Case Missive('Are you sure you want to cancel?','ServiceBase 3g',|
                       'midea.jpg','\No|/Yes') 
            Of 2 ! Yes Button
                CancelPressed = True
            Of 1 ! No Button
        End ! Case Missive
    End!If cancel# = 1

ProgressBar_EndPrintRun                     routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()

RefreshWindow                          ROUTINE
    !|
    !| This routine is used to keep all displays and control templates current.
    !|
    !IF window{Prop:AcceptAll} THEN EXIT.
    IF MainWindow{Prop:AcceptAll} THEN EXIT.
    
    DISPLAY()
    !ForceRefresh = False
!-----------------------------------------------------------------------------------
XL_Setup                                                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel = Create(0, Create:OLE)
        Excel{PROP:Create} = 'Excel.Application'

        Excel{'Application.DisplayAlerts'}  = False         ! no alerts to the user (do you want to save the file?) ! MOVED 10 BDec 2001 John
        Excel{'Application.ScreenUpdating'} = excel:Visible ! False
        Excel{'Application.Visible'}        = excel:Visible ! False

        Excel{'Application.Calculation'}    = xlCalculationManual
        
        DO XL_GetOperatingSystem
        !-----------------------------------------------
    EXIT
XL_Finalize                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Calculation'}    = xlCalculationAutomatic

        Excel{PROP:DEACTIVATE}
        !-----------------------------------------------
    EXIT
XL_AddSheet                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveWorkBook.Sheets("Sheet3").Select'}

        Excel{'ActiveWorkBook.Sheets.Add'}
        !-----------------------------------------------
    EXIT
XL_AddWorkbook               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Workbooks.Add()'}

        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Title")'}            = CLIP(LOC:ProgramName) & ' ' & CLIP(tra_ali:Account_Number)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Author")'}           = CLIP(LOC:UserName)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Application Name")'} = LOC:ApplicationName
        !-----------------------------------------------
        ! 4 Dec 2001 John
        ! Delete empty sheets

        Excel{'Sheets("Sheet2").Select'}
        Excel{'ActiveWindow.SelectedSheets.Delete'}

        Excel{'Sheets("Sheet1").Select'}
        !-----------------------------------------------
    EXIT
XL_SaveAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Save()'}
            Excel{'ActiveWorkBook.Close()'}
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_DropAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Close(' & FALSE & ')'}
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_AddComment                ROUTINE
    DATA
xlComment CSTRING(20)
    CODE
        !-----------------------------------------------
        Excel{'Selection.AddComment("' & CLIP(excel:CommentText) & '")'}

!        xlComment{'Shape.IncrementLeft'} = 127.50
!        xlComment{'Shape.IncrementTop'}  =   8.25
!        xlComment{'Shape.ScaleWidth'}    =   1.76
!        xlComment{'Shape.ScaleHeight'}   =   0.69
        !-----------------------------------------------
    EXIT
XL_RowDown                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(1, 0).Select'}
        !-----------------------------------------------
    EXIT
XL_ColLeft                   ROUTINE
    DATA
CurrentCol LONG
    CODE
        !-----------------------------------------------
        CurrentCol = Excel{'ActiveCell.Col'}
        IF CurrentCol = 1
            EXIT
        END !IF

        Excel{'ActiveCell.Offset(0, -1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColRight                  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, 1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirstSelect                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, Cells(' & Excel{'ActiveCell.Row'} & ', 1)).Select'}
        !Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirst                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_SetLastCell                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_SetGrid                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlDiagonalDown & ').LineStyle'} = xlNone
        Excel{'Selection.Borders(' & xlDiagonalUp   & ').LineStyle'} = xlNone

        DO XL_SetBorder

        Excel{'Selection.Borders(' & xlInsideVertical & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideVertical & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideVertical & ').ColorIndex'} = xlAutomatic

        Excel{'Selection.Borders(' & xlInsideHorizontal & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorder                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetBorderLeft
        DO XL_SetBorderRight
        DO XL_SetBorderBottom
        DO XL_SetBorderTop
        !-----------------------------------------------
    EXIT
XL_SetBorderLeft                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeLeft & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeLeft & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeLeft & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderRight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeRight & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeRight & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeRight & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderBottom                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeBottom & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeBottom & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeBottom & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderTop                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeTop & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeTop & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeTop & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_HighLight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 6 ! YELLOW
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
XL_SetTitle                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 15 ! GREY
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetWrapText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.WrapText'}    = True
        !-----------------------------------------------
    EXIT
XL_SetGreyText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.ColorIndex'}    = 16 ! grey
        !-----------------------------------------------
    EXIT
XL_SetBold                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.Bold'} = True
        !-----------------------------------------------
    EXIT
XL_FormatDate                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = excel:DateFormat
        !-----------------------------------------------
    EXIT
XL_FormatNumber                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_FormatTextBox                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.ShapeRange.IncrementLeft'} = 127.5
        Excel{'Selection.ShapeRange.IncrementTop'}  =   8.25
!        Excel{'Selection.ShapeRange.ScaleWidth'}    =  '1.76, ' & msoFalse & ', ' & msoScaleFromBottomRight
!        Excel{'Selection.ShapeRange.ScaleHeight'}   =  '0.69, ' & msoFalse & ', ' & msoScaleFromTopLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentCentre            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlCenter
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentLeft              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentRight             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlRight
        !-----------------------------------------------
    EXIT
XL_VerticalAlignmentTop                 ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.VerticalAlignment'} = xlTop
        !-----------------------------------------------
    EXIT
XL_VerticalAlignmentCentre            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.VerticalAlignment'} = xlCenter
        !-----------------------------------------------
    EXIT
XL_SetColumnHeader                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetTitle
        DO XL_SetGrid
        DO XL_SetWrapText
        DO XL_SetBold
        !-----------------------------------------------
    EXIT
XL_SetColumn_Date                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = 'dd Mmm yyyy'

!        !Excel{'ActiveCell.FormulaR1C1'}  = LEFT(FORMAT(ret:Invoice_Date, @D8-))
        !-----------------------------------------------
    EXIT
XL_SetColumn_Number                     ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_SetColumn_Percent                    ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '0.00%'
!        Excel{'ActiveCell.NumberFormat'} = CHR(64)       ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_SetColumn_Text                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'} = CHR(64) ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_DataSelectRange                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_DataHideDuplicates                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AdvancedFilter Action:=' & xlFilterInPlace & ', Unique:=' & True }
        !-----------------------------------------------
    EXIT
XL_DataSortRange                            ROUTINE
    DATA
    CODE                     
        !-----------------------------------------------
        DO XL_DataSelectRange
        Excel{'ActiveWindow.ScrollColumn'} = 1
        Excel{'Selection.Sort Key1:=Range("' & LOC:Text & '"), Order1:=' & xlAscending & ', Header:=' & xlGuess & |
            ', OrderCustom:=1, MatchCase:=' & False & ', Orientation:=' & xlTopToBottom}
        !-----------------------------------------------
    EXIT
XL_DataAutoFilter                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AutoFilter'}
        !-----------------------------------------------
    EXIT
XL_SetWorksheetLandscape                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'}     = xlLandscape
!        Excel{'ActiveSheet.PageSetup.FitToPagesWide'} = 1
!        Excel{'ActiveSheet.PageSetup.FitToPagesTall'} = 9999
!        Excel{'ActiveSheet.PageSetup.Order'}          = xlOverThenDown
        !-----------------------------------------------
    EXIT
XL_SetWorksheetPortrait                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'} = xlPortrait
        !-----------------------------------------------
    EXIT
XL_PrintTitleRows                               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.PrintTitleRows'} = CLIP(LOC:Text)
        !-----------------------------------------------
    EXIT
XL_GetCurrentCell                               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOC:Text = Excel{'Application.ConvertFormula( "RC", ' & xlR1C1 & ', ' & xlA1 & ')'}
        !-----------------------------------------------
    EXIT

XL_MergeCells                                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlCenter
        Excel{'Selection.VerticalAlignment'}   = xlBottom
        Excel{'Selection.MergeCells'}          = True
        !-----------------------------------------------
    EXIT

XL_GetOperatingSystem                                                          ROUTINE
    DATA
OperatingSystem     STRING(50)
tmpLen              LONG
LEN_OperatingSystem LONG
    CODE
        !-----------------------------------------------
        excel:OperatingSystem = 0.0    ! Default/error value
        OperatingSystem       = Excel{'Application.OperatingSystem'}
        LEN_OperatingSystem   = LEN(CLIP(OperatingSystem))

        LOOP x# = LEN_OperatingSystem TO 1 BY -1

            CASE SUB(OperatingSystem, x#, 1)
            OF '0' OROF'1' OROF '2' OROF '3' OROF '4' OROF '5' OROF '6' OROF '7' OROF '8' OROF '9' OROF '.'
                ! NULL
            ELSE
                tmpLen                = LEN_OperatingSystem-x#+1
                excel:OperatingSystem = CLIP(SUB(OperatingSystem, x#+1, tmpLen))

                EXIT
            END !CASE
        END !LOOP
        !-----------------------------------------------
    EXIT
!-----------------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020606'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ThirdPartyDespatchReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:TRADEACC_ALIAS.Open
  Relate:WEBJOB.Open
  Access:TRDBATCH.UseFile
  Access:JOBTHIRD.UseFile
  Access:JOBS.UseFile
  Access:SUBTRACC.UseFile
  Access:AUDSTATS.UseFile
  Access:AUDIT2.UseFile
  SELF.FilesOpened = True
  BRW6.Init(?List,Queue:Browse.ViewPosition,BRW6::View:Browse,Queue:Browse,Relate:TRADEACC,SELF)
  OPEN(MainWindow)
  SELF.Opened=True
  ! ========= Set Report Version =============
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5001'
  ?ReportVersion{prop:Text} = tmp:VersionNumber
      LOC:ProgramName       = '3rd Party Despatched Units Report'  ! Job=2055    Cust=N###
  
      excel:Visible      = False
  
      LOC:EndDate        = TODAY()
      LOC:StartDate      = DATE( MONTH(LOC:EndDate), 1, YEAR(LOC:EndDate) )
  
      SET(defaults)
      access:defaults.next()
  
      DO GetUserName
  
      debug:Active = True
  
      Option1 = 1 !
  
      IF GUIMode = 1 THEN
         LocalTimeOut = 500
         DoAll = 'Y'
         !MainWindow{PROP:ICONIZE} = TRUE
      END !IF
      LocalHeadAccount = GETINI('BOOKING','HEADACCOUNT','',CLIP(PATH()) & '\SB2KDEF.INI')
  
      tmp:StatusReceivedAtARC = GETINI('RRC','StatusReceivedAtARC',,CLIP(PATH())&'\SB2KDEF.INI')
  
      DISPLAY
  
  ?LOC:CompanyName{prop:vcr} = TRUE
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW6.Q &= Queue:Browse
  BRW6.AddSortOrder(,tra:Account_Number_Key)
  BRW6.AddLocator(BRW6::Sort0:Locator)
  BRW6::Sort0:Locator.Init(,tra:Account_Number,1,BRW6)
  BRW6.SetFilter('((tra:RemoteRepairCentre = 1 OR tra:Account_Number = LocalHeadAccount) AND (tra:Account_Number <<> ''XXXRRC''))')
  BIND('LocalTag',LocalTag)
  BIND('LocalHeadAccount',LocalHeadAccount)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW6.AddField(LocalTag,BRW6.Q.LocalTag)
  BRW6.AddField(tra:Account_Number,BRW6.Q.tra:Account_Number)
  BRW6.AddField(tra:Company_Name,BRW6.Q.tra:Company_Name)
  BRW6.AddField(tra:RecordNumber,BRW6.Q.tra:RecordNumber)
  FDBTrdParty.Init(?LOC:CompanyName,Queue:FileDrop.ViewPosition,FDB5::View:FileDrop,Queue:FileDrop,Relate:TRDPARTY,ThisWindow)
  FDBTrdParty.Q &= Queue:FileDrop
  FDBTrdParty.AddSortOrder(trd:Company_Name_Key)
  FDBTrdParty.AppendOrder('trd:Company_Name')
  FDBTrdParty.AddField(trd:Company_Name,FDBTrdParty.Q.trd:Company_Name)
  FDBTrdParty.AddUpdateField(trd:Company_Name,LOC:CompanyName)
  ThisWindow.AddItem(FDBTrdParty.WindowComponent)
  FDBTrdParty.DefaultFill = 0
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List{Prop:Alrt,239} = SpaceKey
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?DoAll
      IF DoAll = 'Y' THEN
         HIDE(?List)
         HIDE(?DASTAG)
         HIDE(?DASTAGALL)
         HIDE(?DASUNTAGALL)
      ELSE
          UNHIDE(?List)
          UNHIDE(?DASTAG)
          UNHIDE(?DASTAGALL)
          UNHIDE(?DASUNTAGALL)
      END !IF
    OF ?OkButton
      DO OKButtonPressed
    OF ?CancelButton
       POST(Event:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020606'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020606'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020606'&'0')
      ***
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          LOC:EndDate = TINCALENDARStyle1(LOC:EndDate)
          Display(?LOC:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:1
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          LOC:StartDate = TINCALENDARStyle1()
          Display(?LOC:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?LOC:CompanyName
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?List
    CASE EVENT()
    OF EVENT:AlertKey
      IF RECORDS(BRW6) <> 0 AND DoAll <> 'Y' AND KeyCode() = MouseLeft2 THEN
         POST(Event:Accepted,?DASTAG)
      END !IF
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?Option1:Radio1
          LOC:CompanyName = ''
          Display(?LOC:CompanyName)
          DISABLE(?LOC:CompanyName)
    OF ?Option1:Radio2
          LOC:CompanyName = ''
          ENABLE(?LOC:CompanyName)
          Display(?LOC:CompanyName)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

!-----------------------------------------------------------------------------------
DateToString        PROCEDURE( IN:Date )! STRING
    CODE
        !-----------------------------------------------------------------
        RETURN LEFT(FORMAT(IN:Date, @D8))
        !-----------------------------------------------------------------
GetHeadAccount PROCEDURE( IN:AccountNumber )
    CODE
        !-----------------------------------------------------------------  
        haq:AccountNumber  = IN:AccountNumber
        GET(HeadAccount_Queue, +haq:AccountNumber)

        CASE ERRORCODE()
        OF 00 ! FOUND
            !
        OF 30 ! NOT Found
            ! Not in queue - ADD
            haq:AccountNumber            = IN:AccountNumber

            IF LoadTRADEACC(IN:AccountNumber)
                haq:AccountName          = tra:Company_Name
                haq:BranchIdentification = tra:BranchIdentification

                        IF tra:IncludeSaturday = 'YES'
                            haQ:IncludeSaturday = True
                        END !IF

                        IF tra:IncludeSunday = 'YES'
                            haQ:IncludeSunday = True
                        END !IF

            ELSE
                haq:AccountName          = '*T'
                haq:BranchIdentification = '*T'
            END !IF

            ADD(HeadAccount_Queue, +haq:AccountNumber)
        ELSE
            CancelPressed = True
        END !IF
        !-----------------------------------------------------------------
HoursBetween    PROCEDURE  (StartTime, EndTime)!LONG
Hours LONG
    CODE
        !-----------------------------------------------------------------
        IF StartTime = EndTime
            RETURN 0
        ELSIF StartTime > EndTime
            RETURN HoursBetween(EndTime, StartTime)
        END !IF

        Hours = Round((EndTime - StartTime) / (60 * 60 * 100),1)  ! 100 100ths = 1sec, 60x1sec = 1min, 60x1min=1 hour

        RETURN Hours
        !-----------------------------------------------------------------
GetSubAccount PROCEDURE( IN:AccountNumber )
Temp LONG
    CODE
        !-----------------------------------------------------------------  
        saq:AccountNumber  = IN:AccountNumber
        GET(SubAccount_Queue, +saq:AccountNumber)

        CASE ERRORCODE()
        OF 00 ! FOUND
            !
        OF 30 ! NOT Found
            ! Not in queue - ADD
            saq:AccountNumber            = job:Account_Number

            IF LoadSUBTRACC(job:Account_Number)
                GetHeadAccount(wob:HeadAccountNumber) !Changed by Neil!

                saq:AccountName          = sub:Company_Name
                saQ:HeadAccountNumber    = sub:Main_Account_Number
                saQ:HeadAccountName      = haQ:AccountName
                saQ:BranchIdentification = haQ:BranchIdentification

            ELSE
                saq:AccountName          = ''
                saq:HeadAccountNumber    = ''
                saQ:HeadAccountName      = ''
                saQ:BranchIdentification = ''
            END !IF

            ADD(SubAccount_Queue, +saq:AccountNumber)
        ELSE
            CancelPressed = True
        END !CASE
        !-----------------------------------------------------------------
GetWebJobNumber PROCEDURE( IN:JobNumber )! STRING
    CODE
        !-----------------------------------------------
        IF NOT LoadWEBJOB( job:Ref_Number)
            RETURN '*'
        END !IF

        RETURN IN:JobNumber & '-' & saQ:BranchIdentification & wob:JobNumber
        !-----------------------------------------------
LoadAUDSTATS   PROCEDURE( IN:JobNumber, IN:CurrentStatus )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        WriteDebug('LoadAUDSTATS(' & IN:JobNumber & ', "' & CLIP(IN:CurrentStatus) & '")')

        Access:LOCATLOG.ClearKey(aus:NewStatusKey)
            aus:Type      = 'JOB'
            aus:RefNumber = IN:JobNumber
            aus:NewStatus = IN:CurrentStatus
        SET(aus:NewStatusKey, aus:NewStatusKey)

        IF NOT Access:AUDSTATS.NEXT() = Level:Benign
            WriteDebug('LoadAUDSTATS(EOF)')

            RETURN False
        END !IF

        IF NOT aus:Type = 'JOB'
            WriteDebug('LoadAUDSTATS(EOI aus:Type = "JOB")')

            RETURN False
        END !IF

        IF NOT aus:RefNumber = IN:JobNumber
            WriteDebug('LoadAUDSTATS(EOI)')

            RETURN False
        END !IF

        IF NOT aus:NewStatus = IN:CurrentStatus
            WriteDebug('LoadAUDSTATS(EOI)')

            RETURN False
        END !IF
               
        WriteDebug('LoadAUDSTATS(OK)')

        RETURN True
        !-----------------------------------------------
LoadJOBS   PROCEDURE( IN:JobNumber )! LONG
    CODE
        !-----------------------------------------------
        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = IN:JobNumber

        IF Access:JOBS.TryFetch(job:Ref_Number_Key) <> Level:Benign
            RETURN False
        END !IF

        IF NOT job:Ref_Number = IN:JobNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadJOBS_BatchNumber   PROCEDURE( IN:BatchNumber, IN:First )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        WriteDebug('LoadJOBS_BatchNumber(' & IN:BatchNumber & ', ' & IN:First & ')')

        Access:JOBS.ClearKey(job:Batch_Number_Key)
            job:Batch_Number = IN:BatchNumber
        SET(job:Batch_Number_Key, job:Batch_Number_Key)

        IF Access:JOBS.NEXT() <> Level:Benign
            WriteDebug('LoadJOBS_BatchNumber(False), EOF')

            RETURN False
        END !IF

        IF NOT job:Batch_Number = IN:BatchNumber
            WriteDebug('LoadJOBS_BatchNumber(False), job:Batch_Number(' & job:Batch_Number & ') <> IN:BatchNumber(' & IN:BatchNumber & ')')

            RETURN False
        END !IF

        WriteDebug('LoadJOBS_BatchNumber(True, Job="' & job:Ref_Number & '")')
        RETURN True
        !-----------------------------------------------
LoadJOBTHIRD   PROCEDURE( IN:JobNumber )! LONG
    CODE
        !-----------------------------------------------
        WriteDebug('LoadJOBTHIRD(' & IN:JobNumber & ')')

        Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
        jot:RefNumber = IN:JobNumber

        IF Access:JOBTHIRD.TryFetch(jot:RefNumberKey) <> Level:Benign
            WriteDebug('LoadJOBTHIRD() False')

            RETURN False
        END !IF

        IF NOT jot:RefNumber = IN:JobNumber
            WriteDebug('LoadJOBTHIRD() False')

            RETURN False
        END !IF

        WriteDebug('LoadJOBTHIRD() True')
        RETURN True
        !-----------------------------------------------
LoadSUBTRACC   PROCEDURE( IN:AccountNumber )! LONG
    CODE
        !-----------------------------------------------
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = IN:AccountNumber

        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key) <> Level:Benign
            RETURN False
        END !IF

        IF sub:Account_Number <> IN:AccountNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadTRADEACC   PROCEDURE( IN:AccountNumber )! LONG
    CODE
        !-----------------------------------------------
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = IN:AccountNumber

        IF Access:TRADEACC.TryFetch(tra:Account_Number_Key) <> Level:Benign
            RETURN False
        END !IF

        IF tra:Account_Number <> IN:AccountNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadTRDBATCH   PROCEDURE( IN:CompanyName, IN:First )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        WriteDebug('LoadTRDBATCH(' & CLIP(IN:CompanyName) & ', ' & IN:First & ')')

        IF IN:First
            WriteDebug('LoadTRDBATCH(IN:First)')

            Access:TRDBATCH.ClearKey(trb:Company_Batch_ESN_Key)
                trb:Company_Name = IN:CompanyName
                !trb:Batch_Number
                !trb:ESN
            SET(trb:Company_Batch_ESN_Key, trb:Company_Batch_ESN_Key)
        END !IF

        IF Access:TRDBATCH.NEXT() <> Level:Benign
            WriteDebug('LoadTRDBATCH(False)')

            RETURN False
        END !IF

        IF NOT trb:Company_Name = IN:CompanyName
            WriteDebug('LoadTRDBATCH(False), trb:Company_Name(' & CLIP(trb:Company_Name) & ') <> IN:CompanyName(' & CLIP(IN:CompanyName) & ')')

            RETURN False
        END !IF

        WriteDebug('LoadTRDBATCH(True="' & trb:Batch_Number & '")')
        RETURN True
        !-----------------------------------------------
LoadTRDPARTY   PROCEDURE( IN:CompanyName, IN:First )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        IF IN:First
            Access:TRDPARTY.ClearKey(trd:Company_Name_Key)
                trd:Company_Name = IN:CompanyName
            SET(trd:Company_Name_Key, trd:Company_Name_Key)
        END !IF

        IF Access:TRDPARTY.NEXT() <> Level:Benign
            RETURN False
        END !IF

        IF NOT trd:Company_Name = IN:CompanyName
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadUSERS            PROCEDURE( IN:UserCode )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:USERS.ClearKey(use:User_Code_Key)
            use:User_Code = IN:UserCode
        SET(use:User_Code_Key, use:User_Code_Key)

        IF Access:USERS.NEXT() <> Level:Benign
            RETURN False
        END !IF

        IF NOT use:User_Code = IN:UserCode
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadWEBJOB      PROCEDURE( IN:JobNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
            wob:RefNumber = IN:JobNumber
        SET(wob:RefNumberKey, wob:RefNumberKey)

        IF Access:WEBJOB.NEXT() <> Level:Benign
            RETURN False
        END !IF

        IF NOT wob:RefNumber = IN:JobNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
WorkingDaysBetween PROCEDURE  ( IN:StartDate, IN:EndDate, IN:IncludeSaturday, IN:IncludeSunday )!,long ! Declare Procedure
Weeks        LONG

DaysDiff     LONG
Days         LONG(0)

DaysPerWeek  LONG(5)
TempDate1    DATE
TempDate2    DATE
local:Weekends   Long(0)
    CODE

        Days = in:EndDate - in:StartDate
        Loop Days# = in:StartDate To in:EndDate
            Case Days# %7
                Of 0
                    If ~in:IncludeSunday
                        local:Weekends += 1
                    End !If in:IncludeSaturday
                Of 6
                    If ~in:IncludeSaturday
                        local:Weekends += 1
                    End !If ~in:IncludeSaturday

            End !Case Days#
        End !Loop Days# = in:StartDate To in:EndDate

        Return Days - local:Weekends

        !-------------------------------------------
        WriteDebug('WorkingDaysBetween(' & DateToString(IN:StartDate) & ', ' & DateToString(IN:EndDate) & ', ' & IN:IncludeSaturday & ', ' & IN:IncludeSunday & ')')
        !-------------------------------------------
        IF (IN:StartDate = 0)
            WriteDebug('WorkingDaysBetween(IF (IN:StartDate = 0))')
            RETURN 0
        ELSIF (IN:EndDate = 0)
            WriteDebug('WorkingDaysBetween(ELSIF (IN:EndDate = 0))')
            RETURN 0
        ELSIF (IN:StartDate = IN:EndDate)
            WriteDebug('WorkingDaysBetween(IF (IN:StartDate = IN:EndDate))')
            RETURN 0
        ELSIF (IN:StartDate > IN:EndDate)
            WriteDebug('WorkingDaysBetween(IF (IN:StartDate > IN:EndDate)')
            RETURN WorkingDaysBetween(IN:EndDate, IN:StartDate, IN:IncludeSaturday, IN:IncludeSunday)
        END !IF
        !-------------------------------------------
        IF IN:IncludeSaturday
            DaysPerWeek += 1
        END !IF

        IF IN:IncludeSunday
            DaysPerWeek += 1
        END !IF
        !-------------------------------------------
        TempDate1 = IN:StartDate
        
        LOOP
            If (TempDate1 = IN:EndDate) Then
                WriteDebug('WorkingDaysBetween(If (TempDate1 = IN:EndDate) Then)')
                RETURN Days
            End !If

            CASE (TempDate1 % 7)
            OF 0
                If IN:IncludeSunday
                    Days += 1
                End !If
            OF 5
                BREAK
            OF 6
                If IN:IncludeSaturday
                    Days += 1
                End !If
            ELSE
                Days += 1
            END !CASE

            TempDate1 += 1

        END !LOOP
        !-------------------------------------------
        TempDate2     = IN:EndDate
        
        LOOP
            If (TempDate1 = TempDate2) Then
                WriteDebug('WorkingDaysBetween(If (TempDate1 = TempDate2) Then)')
                RETURN Days
            End !If

            CASE (TempDate2 % 7)
            OF 0
                If IN:IncludeSunday
                    Days += 1
                End !If
            OF 5
                BREAK
            OF 6
                If IN:IncludeSaturday
                    Days += 1
                End !If
            ELSE
                Days += 1
            END !CASE

            TempDate2 -= 1

        END !LOOP
        !-------------------------------------------
        Weeks = (TempDate2 - TempDate1) / 7
        !-------------------------------------------
!        message('DaysBefore           =' & Days                  & '<13,10>' & |
!                '(Weeks * DaysPerWeek)=' & (Weeks * DaysPerWeek) & '<13,10>' & |
!                ' ===================== <13,10>'                             & |
!                'TOTAL                =' & Days + (Weeks * DaysPerWeek) )
        WriteDebug('WorkingDaysBetween(OK="' & Days + (Weeks * DaysPerWeek) & '")')
        RETURN Days + (Weeks * DaysPerWeek)
        !-------------------------------------------
WorkingHoursBetween PROCEDURE  (StartDate, EndDate, StartTime, EndTime, IN:IncludeSaturday, IN:IncludeSunday,AllHours) ! LONG
DaysBetween   LONG
Hours         LONG
local:WorkingDays   Long
local:CountMondays  Long
    CODE
        DaysBetween = WorkingDaysBetween(StartDate, EndDate, IN:IncludeSaturday, IN:IncludeSunday)
        Hours       = 0

        If AllHours
            !Now count 24hr and NOT weekends - L876 (DBH: 14-07-2003)
            IF DaysBetween = 0
                Hours += HoursBetween(StartTime, EndTime)
            ELSIF DaysBetween = 1
                Hours += HoursBetween(StartTime, Deformat('23:59',@t1))
                Hours += HoursBetween(0, EndTime)
            ELSE
                Hours  = (DaysBetween - 1) * HoursBetween(0, Deformat('23:59',@t1))
                Hours += HoursBetween(StartTime, Deformat('23:59',@t1))
                Hours += HoursBetween(0, EndTime)
            END !IF


        Else !If AllHours
            IF DaysBetween = 0
                Hours += HoursBetween(StartTime, EndTime)
            ELSIF DaysBetween = 1
                Hours += HoursBetween(StartTime, tra_ali:EndWorkHours)
                Hours += HoursBetween(tra_ali:StartWorkHours, EndTime)
            ELSE
                Hours  = (DaysBetween - 1) * HoursBetween(tra_ali:StartWorkHours, tra_ali:EndWorkHours)
                Hours += HoursBetween(StartTime, tra_ali:EndWorkHours)
                Hours += HoursBetween(tra_ali:StartWorkHours, EndTime)
            END !IF

        End !If AllHours


        RETURN Hours
NumberToColumn PROCEDURE(IN:Long)! STRING
FirstCol STRING(1)
Temp     LONG
    CODE
        !-----------------------------------------------
        Temp = IN:Long - 1

        FirstCol = Temp / 26
        IF FirstCol = 0
            RETURN CHR(Temp+65)
        END !IF

        RETURN CHR(FirstCol+64) & CHR( (Temp % 26)+65 )
        !-----------------------------------------------
UpdateModelQueue  PROCEDURE( IN:CompanyName, IN:ModelNumber )
    CODE
        !-----------------------------------------------
        manQ:CompanyName = IN:CompanyName
        manQ:ModelNumber  = IN:ModelNumber
        GET(ManufacturerQueue, +manQ:CompanyName, +manQ:ModelNumber)

        CASE ERRORCODE()
        OF 00
            ! Found
        OF 30
            ! NOT Found
            CLEAR(ManufacturerQueue)
                manQ:CompanyName  = IN:CompanyName
                manQ:ModelNumber  = IN:ModelNumber
                manQ:Count        = 0
            ADD(ManufacturerQueue, +manQ:CompanyName, +manQ:ModelNumber)
        ELSE
            CancelPressed = True

            RETURN
        END !CASE

        manQ:Count += 1
        PUT(ManufacturerQueue, +manQ:CompanyName, +manQ:ModelNumber)
        !-----------------------------------------------
UpdateSummaryQueue  PROCEDURE( IN:CompanyName )
    CODE
        !-----------------------------------------------
        sq:CompanyName = IN:CompanyName
        GET(SummaryQueue, +sq:CompanyName)

        CASE ERRORCODE()
        OF 00
            ! Found
        OF 30
            ! NOT Found
            CLEAR(SummaryQueue)
                sq:CompanyName = IN:CompanyName
                sq:JobCount    = 0
            ADD(SummaryQueue, +sq:CompanyName)
        ELSE
            CancelPressed = True

            RETURN
        END !CASE

        sq:JobCount += 1
        PUT(SummaryQueue, +sq:CompanyName)
        !-----------------------------------------------
WriteColumn PROCEDURE( IN:String, IN:StartNewRow )
Temp STRING(255)
    CODE
        !-----------------------------------------------
        Temp = IN:String
        IF CLIP(Temp) = ''
            Temp = ''' '
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            IF IN:StartNewRow
                clip:Value = Temp
            ELSE
                clip:Value = CLIP(clip:Value) & '<09>' & Temp
            END !IF

            RETURN
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
WriteDebug      PROCEDURE( IN:Message )
    CODE
        !-----------------------------------------------
        !1IF NOT debug:Active
            RETURN
        !END !IF

        debug:Count += 1

        PUTINI(CLIP(LOC:ProgramName), debug:Count, IN:Message, 'C:\Debug.ini')
        !-----------------------------------------------
!-----------------------------------------------------------------------------------
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW6.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = tra:RecordNumber
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      LocalTag = ''
    ELSE
      LocalTag = 'Y'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (LocalTag = 'Y')
    SELF.Q.LocalTag_Icon = 2
  ELSE
    SELF.Q.LocalTag_Icon = 1
  END


BRW6.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW6.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW6.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW6::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW6::RecordStatus=ReturnValue
  IF BRW6::RecordStatus NOT=Record:OK THEN RETURN BRW6::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = tra:RecordNumber
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::7:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW6::RecordStatus
  RETURN ReturnValue

