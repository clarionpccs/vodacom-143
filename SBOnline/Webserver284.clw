

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER284.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER089.INC'),ONCE        !Req'd for module callout resolution
                     END


frmFulfilRequest     PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locStockRefNo        LONG                                  !
FoundAnyPart         BYTE                                  !
FoundOrderedPart     BYTE                                  !
locJobNumber         LONG                                  !
RapidQueue           QUEUE,PRE(rapque)                     !
SessionID            LONG                                  !
RecordNumber         LONG                                  !
                     END                                   !
RapidJobQueue        QUEUE,PRE(jobque)                     !
SessionID            LONG                                  !
JobNumber            LONG                                  !
                     END                                   !
                    MAP
RemovePartFromStock     Procedure(Long func:StockRefNumber,Long func:PartRecordNumber,Long func:Quantity,String func:PartType,String func:PartNumber,*STRING pError),BYTE  !StockRefNumber, PartRecordNumber, Quantity, PartType, Quantity
AddFaulty               Procedure(String  func:Type)
FindOtherParts          Procedure(Long func:JobNumber,Long func:RecordNumber),BYTE    !Job Number, Record Number
ShowError               PROCEDURE(STRING pText)
                    END
errorText                   STRING(255)
FilesOpened     Long
STOFAULT::State  USHORT
USERS::State  USHORT
STOCK::State  USHORT
JOBS::State  USHORT
WARPARTS::State  USHORT
PARTS::State  USHORT
ESTPARTS::State  USHORT
STOCKALL::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('frmFulfilRequest')
  loc:formname = 'frmFulfilRequest_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('frmFulfilRequest','')
    p_web._DivHeader('frmFulfilRequest',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferfrmFulfilRequest',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmFulfilRequest',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmFulfilRequest',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmFulfilRequest',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmFulfilRequest',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_frmFulfilRequest',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(STOFAULT)
  p_web._OpenFile(USERS)
  p_web._OpenFile(STOCK)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(WARPARTS)
  p_web._OpenFile(PARTS)
  p_web._OpenFile(ESTPARTS)
  p_web._OpenFile(STOCKALL)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(STOFAULT)
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(ESTPARTS)
  p_Web._CloseFile(STOCKALL)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('frmFulfilRequest_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('frmFulfilRequest_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  IF (p_web.IfExistsValue('FulfilType'))
      p_web.StoreValue('FulFilType')
  END  
  !region FulFil Single Item
    IF (p_web.GSV('FulFilType') = 'SINGLE')
        IF (p_web.IfExistsValue('stl:RecordNumber'))
            p_web.StoreValue('stl:RecordNumber')
            Access:STOCKALL.Clearkey(stl:RecordNumberKey)
            stl:RecordNumber    = p_web.GSV('stl:RecordNumber')
            If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
                p_web.SSV('txtErrorText','')
                p_web.SSV('txtRequestFulfilled','Request Fulfilled')
  
                Case stl:PartType
                Of 'CHA'
                    Access:PARTS.Clearkey(par:RecordNumberKey)
                    par:Record_Number   = stl:PartRecordNumber
                    If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
                  !Found
                        locStockRefNo  = par:Part_Ref_Number
                    Else ! If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
                  !Error
                        
  !                      p_web.SSV('txtErrorText','The selected part does not exist. The entry will be removed from the list.')
  !                      p_web.SSV('txtRequestFulfilled','')
                        Access:STOCKALL.DeleteRecord(0)
                        ShowError('The selected part does not exist. The entry will be removed from the list.')
                    End !If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
                Of 'WAR'
                    Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
                    wpr:Record_Number   = stl:PartRecordNumber
                    If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                  !Found
                        locStockRefNo  = wpr:Part_Ref_Number
  
                    Else ! If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                  !Error
  !                      p_web.SSV('txtErrorText','The selected part does not exist. The entry will be removed from the list.')
  !                      p_web.SSV('txtRequestFulfilled','')
                        Access:STOCKALL.DeleteRecord(0)
                        ShowError('The selected part does not exist. The entry will be removed from the list.')
                    End !If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                End !Case stl:PartType
  
                If RemovePartFromStock(locStockRefNo,stl:PartRecordNumber,stl:Quantity,stl:PartType,stl:PartNumber,errorText) = Level:Benign
  
          !Added 05/12/02 - L393 / VP109 -
          !If all warranty/chargeable/estimate parts are now in stock - change job status to 330 - Parts Ordered
          !And if not using loc:UseRapidStock - if all parts allocated change job status to 310 allocated to engineer
  
                    FoundAnyPart = false
                    FoundOrderedPart = false
  
          !first check the chargeable parts
                    access:Parts.clearkey(par:Part_Number_Key)
                    par:Ref_Number = stl:JobNumber
                    set(par:Part_Number_Key,par:Part_Number_Key)
                    Loop !to find an unallocated part
                        if access:Parts.next() then
                  !leave no more jobs
                            break
                        ELSE
                            if par:Ref_Number <> stl:JobNumber then
                      !Leave no more matching jobs
                                break
                            ELSE
                      !found a part for this job
                                if par:PartAllocated then
                          !ignore this it was allocated
                                ELSE
                          !found an unallocated part!
                                    FoundAnyPart = true
                                    If par:order_number <> ''
                                        If par:date_received = ''
                                            FoundOrderedPart = 1
  
                                        Else!If par:date_received = ''
                                  !ignore this it has been received
                                        End!If par:date_received = ''
                                    End
                                END !if partAllocated
                            END !If ref_numbers disagree
                        END !if access:jobs.next()
                        if FoundAnyPart and FoundORderedPart then break.
                    End !loop to find an unallocated part
  
                    if FoundAnyPart and FoundOrderedPart then
              !I dont need to check any further
                    ELSE
              !Check warranty parts
                        access:warParts.clearkey(wpr:Part_Number_Key)
                        wpr:Ref_Number = stl:JobNumber
                        set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                        Loop !to find an unallocated part
                            if access:WarParts.next() then
                      !leave no more jobs
                                break
                            ELSE
                                if wpr:Ref_Number <> stl:JobNumber then
                          !Leave no more matching jobs
                                    break
                                ELSE
                          !found a part for this job
                                    if wpr:PartAllocated then
                              !ignore this it was allocated
                                    ELSE
                              !found an unallocated part!
                                        FoundAnyPart = true
                                        If wpr:order_number <> ''
                                            If wpr:date_received = ''
                                                FoundOrderedPart = 1
                                            Else!If par:date_received = ''
                                      !ignore this it has been received
                                            End!If par:date_received = ''
                                        End
  
                                    END !if partAllocated
                                END !If ref_numbers disagree
                            END !if access:jobs.next()
                            if foundanypart and foundorderedPart then break.
                        End !loop to find an unallocated part
  
                    END !if I have what I need already
  
                    if FoundAnyPart and foundorderedPart then
              !I dont need to check this
                    ELSE
              !Check estimate parts
                        access:EstParts.clearkey(epr:Part_Number_Key)
                        epr:Ref_Number = stl:JobNumber
                        set(epr:Part_Number_Key,epr:Part_Number_Key)
                        Loop !to find an unallocated part
                            if access:EstParts.next() then
                      !leave no more jobs
                                break
                            ELSE
                                if epr:Ref_Number <> stl:JobNumber then
                          !Leave no more matching jobs
                                    break
                                ELSE
                          !found a part for this job
                                    if epr:PartAllocated then
                              !ignore this it was allocated
                                    ELSE
                              !found an unallocated part!
                                        FoundAnyPart = true
                                        If epr:order_number <> ''
                                            If epr:date_received = ''
                                                FoundOrderedPart = 1
                                            Else!If par:date_received = ''
                                      !ignore this it has been received
                                            End!If par:date_received = ''
                                        End
  
                                    END !if partAllocated
                                END !If ref_numbers disagree
                            END !if access:jobs.next()
                            if foundanypart and foundorderedpart then break.
                        End !loop to find an unallocated part
                    END !if I haven't FoundAnyPart
  
  
                    if FoundOrderedPart = false or FoundAnyPArt = false
               !change job status to 330 then to 315 as needed
                        Access:JOBS.Clearkey(job:Ref_Number_Key)
                        job:Ref_Number  = stl:JobNumber
                        If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                            Error# = 0
  
                            Pointer# = Pointer(JOBS)
                            Hold(JOBS,1)
                            Get(JOBS,Pointer#)
                            If Errorcode() = 43
  !                        Case Missive('This job (' & Clip(brw2.q.stl:JobNumber) & ') is currently in use by another station. Unable to update status. '&|
  !                            '<13,10>You will have to do this manually.','ServiceBase 3g',|
  !                            'mstop.jpg','/OK')
  !                        Of 1 ! OK Button
  !                        End ! Case Missive
  !                        Error# = 1
            !          !Post(Event:CloseWindow)
                            ELSE !If Errorcode() = 43
                        !Found
                                if FoundOrderedPart = false
                                    getstatus(330,0,'JOB')  !Spares Requested
                                END !If no outstanding orders
                                if FoundAnyPart = false
                                    getstatus(345,0,'JOB')  !In repair
                                end !found no unallocated parts
                                Access:JOBS.TryUpdate()
                            End  !If Errorcode() = 43
                            Release(JOBS)
                        END !if tryfetch on jobs
                    END !if I haven't FoundAnyPart
          !end of Added 05/12/02 - L393 / VP109 -
  
          !Remove Entry from browse - 3451 (DBH: 28-10-2003)
                    Access:STOCKALL.DeleteRecord(0)
                ELSE
                    ShowError(errorText)
                End !Local.RemovePartFromStock(tmp:StockRefNo,stl:PartRecordNumber,stl:Quantity,stl:PartType,stl:PartNumber)
  
            Else ! If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
      !Error
  !              p_web.SSV('txtErrorText','An error occurred. Try again.')
  !                p_web.SSV('txtRequestFulfilled','')
                ShowError('Unable to get Stock Record. Try Again.')
  
            End !If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
        ELSE
  !          p_web.SSV('txtErrorText','An error occurred. Try again.')
  !            p_web.SSV('txtRequestFulfilled','')
            ShowError('Unable to get Stock Record. Try Again.')
        END
    END ! IF (p_web.GSV('FulFilType') = 'SINGLE')
  !endregion
  !region Fulfil Multiple
    IF (p_web.GSV('FulFilType') = 'MULTIPLE')
        p_web.SSV('txtErrorText','')
        p_web.SSV('txtRequestFulfilled','Request(s) Fulfilled')
        LOOP x# = 1 TO RECORDS(RapidQueue)
            GET(RapidQueue,x#)
            IF (RapidQueue.SessionID <> p_web.SessionID)
                CYCLE
            END
            Delete(RapidQueue)
        END
  
        LOOP x# = 1 TO RECORDS(RapidJobQueue)
            GET(RapidJobQueue,x#)
            IF (RapidJobQueue.SessionID <> p_web.SessionID)
                CYCLE
            END
            Delete(RapidJobQueue)
        END
  
        Access:STOCKALL.ClearKey(stl:StatusJobNumberKey)
        stl:Location  = p_web.GSV('Default:SiteLocation')
        stl:Status    = 'WEB'
        Set(stl:StatusJobNumberKey,stl:StatusJobNumberKey)
        Loop
            If Access:STOCKALL.NEXT()
                Break
            End !If
            If stl:Location  <> p_web.GSV('Default:SiteLocation')      |
                Or stl:Status    <> 'WEB'      |
                Then Break.  ! End If
            rapque:RecordNumber = stl:RecordNumber
            rapque:SessionID = p_web.SessionID
            Add(RapidQueue)
        End !Loop
  
        Sort(RapidQueue,rapque:RecordNumber)
  
        Loop x# = 1 To Records(RapidQueue)
            Get(RapidQueue,x#)
            IF (RapidQueue.SessionID <> p_web.SessionID)
                CYCLE
            END
  
            Access:STOCKALL.Clearkey(stl:RecordNumberKEy)
            stl:RecordNumber  = rapque:RecordNumber
            If Access:STOCKALL.Tryfetch(stl:RecordNumberKEy) = Level:Benign
              !Found
              !This is from stock
              !Has this job already been tried and failed?
                Sort(RapidJobQueue,jobque:JobNumber)
                jobque:JobNumber    = stl:JobNumber
                Get(RapidJobQueue,jobque:JobNumber)
                If ~Error()
                    Cycle
                End !If ~Error()
  
                locStockRefNo = stl:PartRefNumber
  
                Case stl:PartType
                OF 'CHA'
                    Access:Parts.ClearKey(par:recordnumberkey)
                    par:Record_Number = stl:PartRecordNumber
                    IF Access:Parts.Fetch(par:recordnumberkey)
                          !Error!
                        Cycle
                    ELSE
  
                    END
                OF 'WAR'
                    Access:WarParts.ClearKey(wpr:recordnumberkey)
                    wpr:Record_Number = stl:PartRecordNumber
                    IF Access:WarParts.Fetch(wpr:recordnumberkey)
                          !Error!
                        Cycle
                    ELSE
  
                    END
                End
  
                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                sto:Ref_Number  = locStockRefNo
                If Access:STOCK.Tryfetch(sto:Ref_Number_Key)
                 !Cannot find stock entry
                    Cycle
                END
                If stl:Quantity > sto:Quantity_Stock
                  !Cannot fullfill request
                    Cycle
                Else !stl:Quantity > sto:Quantity_Stock
                  !
                End !stl:Quantity > sto:Quantity_Stock
  
                locJobNumber   = stl:JobNumber
  
                If (FindOtherParts(stl:JobNumber,stl:RecordNumber) = Level:Benign)
                  !All parts are ok to be taken
                    Access:STOCKALL.ClearKey(stl:StatusJobNumberKey)
                    stl:Location  = p_web.GSV('Default:SiteLocation')
                    stl:Status    = 'WEB'
                    stl:JobNumber = locJobNumber
                    Set(stl:StatusJobNumberKey,stl:StatusJobNumberKey)
                    Loop
                        If Access:STOCKALL.NEXT()
                            Break
                        End !If
                        If stl:Location  <> p_web.GSV('Default:SiteLocation')      |
                            Or stl:Status    <> 'WEB'      |
                            Or stl:JobNumber <> locJobNumber |
                            Then Break.  ! End If
                        If RemovePartFromStock(stl:PartRefNumber,stl:PartRecordNumber,stl:Quantity,stl:PartType,stl:PartNumber,errorText) = Level:Benign
                            Relate:STOCKALL.Delete(0)
                        End !.RemoveFromStock(par:Part_Ref_Number,par:Record_Number,par:Quantity,'C',par:Part_Number)
                    End !Loop
  
                Else !If Local.FindOtherParts(stl:JobNumber,stl:RecordNumber)
                  !Not all parts can be fullfilled
                  !Add job number to queue, so no other parts for this job will be picked
                    Sort(RapidJobQueue,jobque:JobNumber)
                    jobque:JobNumber = locJobNumber
                    jobque:SessionID = p_web.SessionID
                    Add(RapidJobQueue)
                End !If Local.FindOtherParts(stl:JobNumber,stl:RecordNumber)
  
  
            Else!If Access:RAPIDSTOCK.TryFetch(stl:RecordNumberKey) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:RAPIDSTOCK.TryFetch(stl:RecordNumberKey) = Level:Benign
  
          !Added 05/12/02 - L393 / VP109 -
          !If all warranty/chargeable/estimate parts are now in stock - change job status to 330 - Parts Ordered
          !And if not using loc:UseRapidStock - if all parts allocated change job status to 310 allocated to engineer
  
            Access:STOCKALL.Clearkey(stl:RecordNumberKey)
            stl:recordNumber    = rapque:RecordNumber
            If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
              !Found
              !Reget the record to check the status bits below
            Else ! If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
              !Error
            End !If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
  
  
            FoundAnyPart = false
            FoundOrderedPart = false
  
          !first check the chargeable parts
            access:Parts.clearkey(par:Part_Number_Key)
            par:Ref_Number = stl:JobNumber
            set(par:Part_Number_Key,par:Part_Number_Key)
            Loop !to find an unallocated part
                if access:Parts.next() then
                  !leave no more jobs
                    break
                ELSE
                    if par:Ref_Number <> stl:JobNumber then
                      !Leave no more matching jobs
                        break
                    ELSE
                      !found a part for this job
                        if par:PartAllocated then
                          !ignore this it was allocated
                        ELSE
                          !found an unallocated part!
                            FoundAnyPart = true
                            If par:order_number <> ''
                                If par:date_received = ''
                                    FoundOrderedPart = 1
  
                                Else!If par:date_received = ''
                                  !ignore this it has been received
                                End!If par:date_received = ''
                            End
                        END !if partAllocated
                    END !If ref_numbers disagree
                END !if access:jobs.next()
                if FoundAnyPart and FoundORderedPart then break.
            End !loop to find an unallocated part
  
            if FoundAnyPart and FoundOrderedPart then
              !I dont need to check any further
            ELSE
              !Check warranty parts
                access:warParts.clearkey(wpr:Part_Number_Key)
                wpr:Ref_Number = stl:JobNumber
                set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                Loop !to find an unallocated part
                    if access:WarParts.next() then
                      !leave no more jobs
                        break
                    ELSE
                        if wpr:Ref_Number <> stl:JobNumber then
                          !Leave no more matching jobs
                            break
                        ELSE
                          !found a part for this job
                            if wpr:PartAllocated then
                              !ignore this it was allocated
                            ELSE
                              !found an unallocated part!
                                FoundAnyPart = true
                                If wpr:order_number <> ''
                                    If wpr:date_received = ''
                                        FoundOrderedPart = 1
                                    Else!If par:date_received = ''
                                      !ignore this it has been received
                                    End!If par:date_received = ''
                                End
  
                            END !if partAllocated
                        END !If ref_numbers disagree
                    END !if access:jobs.next()
                    if foundanypart and foundorderedPart then break.
                End !loop to find an unallocated part
  
            END !if I have what I need already
  
            if FoundAnyPart and foundorderedPart then
              !I dont need to check this
            ELSE
              !Check estimate parts
                access:EstParts.clearkey(epr:Part_Number_Key)
                epr:Ref_Number = stl:JobNumber
                set(epr:Part_Number_Key,epr:Part_Number_Key)
                Loop !to find an unallocated part
                    if access:EstParts.next() then
                      !leave no more jobs
                        break
                    ELSE
                        if epr:Ref_Number <> stl:JobNumber then
                          !Leave no more matching jobs
                            break
                        ELSE
                          !found a part for this job
                            if epr:PartAllocated then
                              !ignore this it was allocated
                            ELSE
                              !found an unallocated part!
                                FoundAnyPart = true
                                If epr:order_number <> ''
                                    If epr:date_received = ''
                                        FoundOrderedPart = 1
                                    Else!If par:date_received = ''
                                      !ignore this it has been received
                                    End!If par:date_received = ''
                                End
  
                            END !if partAllocated
                        END !If ref_numbers disagree
                    END !if access:jobs.next()
                    if foundanypart and foundorderedpart then break.
                End !loop to find an unallocated part
            END !if I haven't FoundAnyPart
  
  
            if FoundOrderedPart = false or FoundAnyPArt = false
               !change job status to 330 then to 315 as needed
                Access:JOBS.Clearkey(job:Ref_Number_Key)
                job:Ref_Number  = stl:JobNumber
                If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                    Error# = 0
  
                    Pointer# = Pointer(JOBS)
                    Hold(JOBS,1)
                    Get(JOBS,Pointer#)
                    If Errorcode() = 43
                        CYCLE
  !                    Case Missive('This job is currently in use by another station. Unable to update status. You will have to do this manually.','ServiceBase 3g',|
  !                        'mstop.jpg','/OK')
  !                    Of 1 ! OK Button
  !                    End ! Case Missive
                        Error# = 1
            !          !Post(Event:CloseWindow)
                    ELSE !If Errorcode() = 43
                        !Found
                        if FoundOrderedPart = false
                            getstatus(330,0,'JOB')  !Spares Requested
                        END !If no outstanding orders
                        if FoundAnyPart = false
                            getstatus(345,0,'JOB')  !In repair
                        end !found no unallocated parts
                        Access:JOBS.TryUpdate()
                    End  !If Errorcode() = 43
                    Release(JOBS)
                END !if tryfetch on jobs
            END !if I haven't FoundAnyPart
  
          !end of Added 05/12/02 - L393 / VP109 -
  
        End !x# = 1 To Records(RapidQueue)
  
    END ! IF (p_web.GSV('FulFilType') = 'MULTIPLE')
  !endregion  
      p_web.site.SaveButton.TextValue = 'OK'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'frmStockAllocation'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('frmFulfilRequest_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('frmFulfilRequest_ChainTo')
    loc:formaction = p_web.GetSessionValue('frmFulfilRequest_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="frmFulfilRequest" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="frmFulfilRequest" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="frmFulfilRequest" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Fulfil Request') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Fulfil Request',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_frmFulfilRequest">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_frmFulfilRequest" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_frmFulfilRequest')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Fulfil Request') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_frmFulfilRequest')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_frmFulfilRequest'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_frmFulfilRequest')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Fulfil Request') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_frmFulfilRequest_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Fulfil Request')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Fulfil Request')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Fulfil Request')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Fulfil Request')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::txtRequestFulfilled
      do Comment::txtRequestFulfilled
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::txtErrorText
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::txtErrorText
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::txtRequestFulfilled  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtRequestFulfilled',p_web.GetValue('NewValue'))
    do Value::txtRequestFulfilled
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtRequestFulfilled  Routine
  p_web._DivHeader('frmFulfilRequest_' & p_web._nocolon('txtRequestFulfilled') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold large')&'">' & p_web.Translate(p_web.GSV('txtRequestFulfilled'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::txtRequestFulfilled  Routine
    loc:comment = ''
  p_web._DivHeader('frmFulfilRequest_' & p_web._nocolon('txtRequestFulfilled') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::txtErrorText  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtErrorText',p_web.GetValue('NewValue'))
    do Value::txtErrorText
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtErrorText  Routine
  p_web._DivHeader('frmFulfilRequest_' & p_web._nocolon('txtErrorText') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('red bold large')&'">' & p_web.Translate(p_web.GSV('txtErrorText'),1) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::txtErrorText  Routine
    loc:comment = ''
  p_web._DivHeader('frmFulfilRequest_' & p_web._nocolon('txtErrorText') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('frmFulfilRequest_form:ready_',1)
  p_web.SetSessionValue('frmFulfilRequest_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_frmFulfilRequest',0)

PreCopy  Routine
  p_web.SetValue('frmFulfilRequest_form:ready_',1)
  p_web.SetSessionValue('frmFulfilRequest_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_frmFulfilRequest',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('frmFulfilRequest_form:ready_',1)
  p_web.SetSessionValue('frmFulfilRequest_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('frmFulfilRequest:Primed',0)

PreDelete       Routine
  p_web.SetValue('frmFulfilRequest_form:ready_',1)
  p_web.SetSessionValue('frmFulfilRequest_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('frmFulfilRequest:Primed',0)
  p_web.setsessionvalue('showtab_frmFulfilRequest',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('frmFulfilRequest_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('frmFulfilRequest_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('frmFulfilRequest:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')
SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('locStockRefNo',locStockRefNo) ! LONG
     p_web.SSV('FoundAnyPart',FoundAnyPart) ! BYTE
     p_web.SSV('FoundOrderedPart',FoundOrderedPart) ! BYTE
     p_web.SSV('locJobNumber',locJobNumber) ! LONG
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     locStockRefNo = p_web.GSV('locStockRefNo') ! LONG
     FoundAnyPart = p_web.GSV('FoundAnyPart') ! BYTE
     FoundOrderedPart = p_web.GSV('FoundOrderedPart') ! BYTE
     locJobNumber = p_web.GSV('locJobNumber') ! LONG
RemovePartFromStock   Procedure(Long func:StockRefNumber,Long func:PartRecordNumber,Long func:Quantity,String func:PartType,String func:PartNumber,*STRING pError)  !StockRefNumber, PartRecordNumber, Quantity, PartType, Quantity
Code
    Access:STOCK.Clearkey(sto:Ref_Number_Key)
    sto:Ref_Number  = func:StockRefNumber
    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
        !Found
        ! #11309 Make sure STock Item is not in use (DBH: 30/06/2010)
        IF (BHFileInUse(STOCK))
            p_web.SSV('txtErrorText','Error: The selected stock item is in use: ' & sto:Part_Number)
            p_web.SSV('txtRequestFulfilled','')
            pError = 'Error: The selected stock item is in use.'
            Return Level:Fatal
        End

        If sto:Quantity_Stock < func:Quantity

            p_web.SSV('txtErrorText','Error: There are insufficient items in stock: ' & sto:Part_Number)
            p_web.SSV('txtRequestFulfilled','')
            pError = 'Error: There are insufficient items in stock.'
            !There isn't enough in stock anymore
            Return Level:Fatal
        End !If sto:Quantity < func:Quantity

        If func:PartNumber = 'EXCH'
!            Case Missive('The selected part is an "Exchange Part".'&|
!                '<13,10>Click "Allocate Exchange" to allocate an exchange unit.','ServiceBase 3g',|
!                'mstop.jpg','/OK')
!            Of 1 ! OK Button
!            End ! Case Missive
            p_web.SSV('txtErrorText','Error: This in an "Exchange Part"')
            p_web.SSV('txtRequestFulfilled','')
            pError = 'Error: This in an "Exchange Part"'
        Else !If stl:PartNumber = 'EXCH'
            Error# = 0
            Case func:PartType
            Of 'CHA'
                Access:PARTS.Clearkey(par:recordnumberkey)
                par:Record_Number   = func:PartRecordNumber
                If Access:PARTS.Tryfetch(par:recordnumberkey) = Level:Benign
                        !Found
                    Access:JOBS.Clearkey(job:Ref_Number_Key)
                    job:Ref_Number  = par:Ref_Number
                    If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                            !Found

                    Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                            !Error
                        p_web.SSV('txtErrorText','Error: Cannot find job number ' & par:Ref_Number)
                        p_web.SSV('txtRequestFulfilled','')
                        pError = 'Error: Cannot find the selected job.'
                        Return Level:Fatal
                    End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                    Access:STOCK.Clearkey(sto:Ref_Number_Key)
                    sto:Ref_Number  = par:Part_Ref_Number
                    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Found
                            ! #11309 Make sure STock Item is not in use (DBH: 30/06/2010)
                        IF (BHFileInUse(STOCk))
                            p_web.SSV('txtErrorText','Error: The selected stock item is in use: ' & sto:Part_Number)
                            p_web.SSV('txtRequestFulfilled','')
                            pError = 'Error: The selected stock part is in use'
                            Return Level:Fatal
                        End

                        If sto:ReturnFaultySpare

                            Access:USERS.Clearkey(use:User_Code_Key)
                            use:User_Code = job:Engineer
                            If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  !Found

                            Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  !Error
                            End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                            p_web.SSV('txtErrorText','This faulty part must be returned before a new part can be issued.<br/>' & |
                                '<br/>' & |
                                'Part: ' & CLIP(sto:Part_Number) & ' - ' & CLIP(sto:Description) & '<br/>' & |
                                'Job: ' & CLIP(job:Ref_Number) & ' - ' & CLIP(use:Forename) & ' ' & CLIP(use:Surname))
!                            Case Missive('This faulty part must be returned before a new part can be issued:'&|
!                                '<13,10>Part: ' & Clip(sto:Part_Number) & ' - ' & Clip(sto:Description) & '.'&|
!                                '<13,10>Job: ' & Clip(par:Ref_Number) & ' - ' & Clip(use:Forename) & ' ' & Clip(use:Surname) & '.','ServiceBase 3g',|
!                                'mquest.jpg','Decline|Confirm')
!                            Of 2 ! Confirm Button
                                AddFaulty('C')
!                            Of 1 ! Decline Button
!                                Error# = 1
!                            End ! Case Missive
                        End !If sto:ReturnFaultySpare
                    Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Error
                    End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                Else ! If Access:PARTS.Tryfetch(par:Record_Number_Key) = Level:Benign
                        !Error
                    Error# = 1
                End !If AccessPARTS.Tryfetch(parRecord_Number_Key) = LevelBenign
            Of 'WAR'
                Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
                wpr:Record_Number   = func:PartRecordNumber
                If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                        !Found
                    Access:JOBS.Clearkey(job:Ref_Number_Key)
                    job:Ref_Number  = wpr:Ref_Number
                    If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                            !Found

                    Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                            !Error
                        p_web.SSV('txtErrorText','Error: Cannot find job number ' & wpr:Ref_Number)
                        p_web.SSV('txtRequestFulfilled','')
                        pError = 'Error: Cannot find the selected job.'
                        Return Level:Fatal

                    End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                    Access:STOCK.Clearkey(sto:Ref_Number_Key)
                    sto:Ref_Number  = wpr:Part_Ref_Number
                    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Found
                            ! #11309 Make sure STock Item is not in use (DBH: 30/06/2010)
                        IF (BHFileInUse(STOCK))
                            p_web.SSV('txtErrorText','Error: The selected stock item is in use: ' & sto:Part_Number)
                            p_web.SSV('txtRequestFulfilled','')
                            pError = 'Error: The selected stock item is in use'
                            Return Level:Fatal
                        End

                        If sto:ReturnFaultySpare
                            Access:USERS.Clearkey(use:User_Code_Key)
                            use:User_Code = job:Engineer
                            If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  !Found

                            Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  !Error
                            End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                            p_web.SSV('txtErrorText','This faulty part must be returned before a new part can be issued.<br/>' & |
                                '<br/>' & |
                                'Part: ' & CLIP(sto:Part_Number) & ' - ' & CLIP(sto:Description) & '<br/>' & |
                                'Job: ' & CLIP(job:Ref_Number) & ' - ' & CLIP(use:Forename) & ' ' & CLIP(use:Surname))
!                            Case Missive('This faulty part must be returned before a new part can be issued:'&|
!                                '<13,10>Part: ' & Clip(sto:Part_Number) & ' - ' & Clip(sto:Description) & '.'&|
!                                '<13,10>Job: ' & Clip(job:Ref_Number) & ' - ' & Clip(use:Forename) & ' ' & Clip(use:Surname) & '.','ServiceBase 3g',|
!                                'mquest.jpg','Decline|Confirm')
!                            Of 2 ! Confirm Button
                                AddFaulty('W')
!                            Of 1 ! Decline Button
!                                Error# = 1
!                            End ! Case Missive

                        End !If sto:ReturnFaultySpare

                    Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Error
                    End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign

                Else ! If Access:WARPARTS.Tryfetch(wpr:Record_Number_Key) = Level:Benign
                        !Error
                    Error# = 1
                End !If AccessWARPARTS.Tryfetch(wprRecord_Number_Key) = LevelBenign
            Of 'EST'
                Access:ESTPARTS.Clearkey(epr:Record_Number_Key)
                epr:Record_Number   = func:PartRecordNumber
                If Access:ESTPARTS.Tryfetch(epr:Record_Number_Key) = Level:Benign
                        !Found
                    Access:JOBS.Clearkey(job:Ref_Number_Key)
                    job:Ref_Number  = epr:Ref_Number
                    If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                            !Found

                    Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                            !Error
                        p_web.SSV('txtErrorText','Error: Cannot find job number ' & epr:Ref_Number)
                        p_web.SSV('txtRequestFulfilled','')
                        pError = 'Error: Cannot find the selected job.'
                        Return Level:Fatal

                    End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                    Access:STOCK.Clearkey(sto:Ref_Number_Key)
                    sto:Ref_Number  = epr:Part_Ref_Number
                    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Found
                            ! #11309 Make sure STock Item is not in use (DBH: 30/06/2010)
                        IF (BHFileINUse(STOCK))
                            p_web.SSV('txtErrorText','Error: The selected stock item is in use: ' & sto:Part_Number)
                            p_web.SSV('txtRequestFulfilled','')
                            pError = 'Error: The selected stock item is in use'
                            Return Level:Fatal
                        End

                        If sto:ReturnFaultySpare
                            Access:USERS.Clearkey(use:User_Code_Key)
                            use:User_Code = job:Engineer
                            If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  !Found

                            Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  !Error
                            End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                            p_web.SSV('txtErrorText','This faulty part must be returned before a new part can be issued.<br/>' & |
                                '<br/>' & |
                                'Part: ' & CLIP(sto:Part_Number) & ' - ' & CLIP(sto:Description) & '<br/>' & |
                                'Job: ' & CLIP(job:Ref_Number) & ' - ' & CLIP(use:Forename) & ' ' & CLIP(use:Surname))
!                            Case Missive('This faulty part must be returned before a new part can be issued:'&|
!                                '<13,10>Part: ' & Clip(sto:Part_Number) & ' - ' & Clip(sto:Description) & '.'&|
!                                '<13,10>Job: ' & Clip(job:Ref_Number) & ' - ' & Clip(use:Forename) & ' ' & Clip(use:Surname) & '.','ServiceBase 3g',|
!                                'mquest.jpg','Decline|Confirm')
!                            Of 2 ! Confirm Button
                                AddFaulty('E')
!                            Of 1 ! Decline Button
!                                Error# = 1
!                            End ! Case Missive

                        End !If sto:ReturnFaultySpare

                    Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Error
                    End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign

                Else ! If Access:ESTPARTS.Tryfetch(epr:RecordNumberKey) = Level:Benign
                        !Error
                    Error# = 1
                End !If Access:ESTPARTS.Tryfetch(epr:RecordNumberKey) = Level:Benign
            End !Case stl:PartType
        End !If func:PartNumber = 'EXCH'

        If Error# = 0
            sto:Quantity_Stock  -= func:Quantity
            If sto:Quantity_Stock < 0
                sto:Quantity_Stock = 0
            End !If sto:Quantity_Stock < 0
            IF Access:STOCK.Update()
                !Error
            END

            Case func:PartType
            Of 'CHA'
                Access:PARTS.Clearkey(par:RecordNumberKey)
                par:Record_Number   = func:PartRecordNumber
                If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
                    ! Found
                    If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                        'DEC', | ! Transaction_Type
                        par:Despatch_Note_Number, | ! Depatch_Note_Number
                        par:Ref_Number, | ! Job_Number
                        0, | ! Sales_Number
                        func:Quantity, | ! Quantity
                        par:Purchase_Cost, | ! Purchase_Cost
                        par:Sale_Cost, | ! Sale_Cost
                        par:Retail_Cost, | ! Retail_Cost
                        'STOCK DECREMENTED', | ! Notes
                        '', |
                        p_web.GSV('BookingUsercode'), |
                        sto:Quantity_Stock) ! Information
                        ! Added OK

                    Else ! AddToStockHistory
                        ! Error
                    End ! AddToStockHistory
                    par:WebOrder    = False
                    par:Status      = ''
                    par:PartAllocated   = 1
                    Access:PARTS.Update()
                Else ! If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
                    ! Error
                End ! If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
            Of 'WAR'
                Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
                wpr:Record_Number   = func:PartRecordNumber
                If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                    ! Found
                    If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                        'DEC', | ! Transaction_Type
                        wpr:Despatch_Note_Number, | ! Depatch_Note_Number
                        wpr:Ref_Number, | ! Job_Number
                        0, | ! Sales_Number
                        func:Quantity, | ! Quantity
                        wpr:Purchase_Cost, | ! Purchase_Cost
                        wpr:Sale_Cost, | ! Sale_Cost
                        wpr:Retail_Cost, | ! Retail_Cost
                        'STOCK DECREMENTED', | ! Notes
                        '', |
                        p_web.GSV('BookingUsercode'), |
                        sto:Quantity_Stock) ! Information
                        ! Added OK

                    Else ! AddToStockHistory
                        ! Error
                    End ! AddToStockHistory
                    wpr:WebOrder    = False
                    wpr:Status      = ''
                    wpr:PartAllocated   = 1
                    Access:WARPARTS.Update()
                Else ! If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                    ! Error
                End ! If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
            End ! Case func:PartType
            ! #10396 Suspend part if none in stock and suspended at main store. (DBH: 16/07/2010)
            If (sto:Location <> VodacomClass.MainStoreLocation())
                If (VodacomClass.MainStoreSuspended(sto:Part_Number))
                    if (sto:Quantity_Stock = 0)
                    ! Suspend Item
                        sto:Suspend = 1
                        If (Access:STOCK.TryUpdate() = Level:Benign)
                            if (AddToStockHistory(sto:Ref_Number, |
                                'ADD',|
                                '',|
                                0, |
                                0, |
                                0, |
                                sto:Purchase_Cost,|
                                sto:Sale_Cost, |
                                sto:Retail_Cost, |
                                'PART SUSPENDED',|
                                '', |
                                p_web.GSV('BookingUserCode'), |
                                sto:Quantity_Stock))
                            end
                        ENd ! If (Access:STOCK.TryUpdate() = Level:Benign)
                    else
                    ! Show warning
!                        Beep(Beep:SystemExclamation)  ;  Yield()
!                        Case Missive('This part has been suspended at Main Store. It will not be available for ordering.'&|
!                            '|'&|
!                            '|Current Stock Level: ' & Clip(sto:Quantity_stock) & '.','ServiceBase',|
!                            'mexclam.jpg','/&OK')
!                        Of 1 ! &OK Button
!                        End!Case Message
                    end
                end ! If (MainStoreSuspened(sto:Part_Number))
            End
        Else !If Error# = 0
            Return Level:Fatal
        End !If Error# = 0
    Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
        p_web.SSV('txtErrorText','Error: The selected stock item cannot be found. The line will be removed: ' & func:PartNumber)
        p_web.SSV('txtRequestFulfilled','')
        RemoveFromStockAllocation(func:PartRecordNumber,func:PartType)
        pError = 'Error: The selected stock item cannot be found. The line will be removed.'

!        !Error
!        !Assert(0,'<13,10>Fetch Error<13,10>')
!        Access:JOBS.Clearkey(job:Ref_Number_Key)
!        job:Ref_Number  = stl:JobNumber
!        If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
!            !Found
!
!        Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
!            !Error
!        End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
!
!        Access:USERS.Clearkey(use:User_Code_Key)
!        use:User_COde   = job:Engineer
!        If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
!            !Found
!
!        Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
!            !Error
!        End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
!
!        !Blank entries are still being left after a refresh.
!        !This will ensure that they can be deleted. - 3451 (DBH: 28-10-2003)
!        Case Missive('Unable to fund the selected part in Stock Control.'&|
!            '<13,10>Part:' & Clip(stl:PartNumber) & ' - ' & Clip(stl:Description) & '.'&|
!            '<13,10>Job: ' & Clip(job:Ref_NUmber) & ' - ' & Clip(use:Forename) & ' ' & Clip(use:Surname) &|
!            '<13,10>Do you wish to REMOVE this part line from Stock Allocation?','ServiceBase 3g',|
!            'mquest.jpg','\Cancel|Remove')
!        Of 2 ! Remove Button
!            RemoveFromStockAllocation(func:PartRecordNumber,func:PartType)
!        Of 1 ! Cancel Button
!        End ! Case Missive
!        Return Level:Fatal
    End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
    Return Level:Benign
AddFaulty     Procedure(String  func:Type)
Code
    If Access:STOFAULT.PrimeRecord() = Level:Benign
        Case func:Type
        Of 'W'
            stf:PartNumber  = wpr:Part_Number
            stf:Description = wpr:Description
            stf:Quantity    = wpr:Quantity
            stf:PurchaseCost    = wpr:Purchase_Cost
        Of 'C'
            stf:PartNumber  = par:Part_Number
            stf:Description = par:Description
            stf:Quantity    = par:Quantity
            stf:PurchaseCost    = par:Purchase_Cost
        Of 'E'
            stf:PartNumber  = epr:Part_Number
            stf:Description = epr:Description
            stf:Quantity    = epr:Quantity
            stf:PurchaseCost    = epr:Purchase_Cost
        End !Case func:Type

        stf:ModelNumber = job:Model_Number
        stf:IMEI        = job:ESN
        stf:Engineer    = job:Engineer
        stf:StoreUserCode   = p_web.GSV('BookingUserCode')
        stf:PartType        = 'FAU'
        If Access:STOFAULT.TryInsert() = Level:Benign
            !Insert Successful

        Else !If Access:STOFAULT.TryInsert() = Level:Benign
            !Insert Failed
            Access:STOFAULT.CancelAutoInc()
        End !If Access:STOFAULT.TryInsert() = Level:Benign
    End !If Access:STOFAULT.PrimeRecord() = Level:Benign
FindOtherParts        Procedure(Long func:JobNumber,Long func:RecordNumber)    !Job Number, Record Number
Code
    Found# = 0
        !Lets find some other parts in the allocation list

    Access:STOCKALL.ClearKey(stl:StatusJobNumberKey)
    stl:Location  = p_web.GSV('Default:SiteLocation')
    stl:Status    = 'WEB'
    Set(stl:StatusJobNumberKey,stl:StatusJobNumberKey)
    Loop
        If Access:STOCKALL.NEXT()
            Break
        End !If
        If stl:Location  <> p_web.GSV('Default:SiteLocation')      |
            Or stl:Status    <> 'WEB'      |
            Then Break.  ! End If

            !Only look for parts from the same job
        If stl:JobNumber <> func:JobNumber
            Cycle
        End !If stl:JobNumber <> tmp:JobNumber
            !Make sure you haven't found the original record
        If stl:RecordNumber = func:RecordNumber
            Cycle
        End !If stl:RecordNumber = rapque:RecordNumber

            !Are there any in stock
        Case stl:PartType
        Of 'CHA'
            Access:PARTS.Clearkey(par:RecordNumberKey)
            par:Record_Number   = stl:PartRecordNumber
            If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
                        !Found
                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                sto:Ref_Number  = par:Part_Ref_Number
                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Found
                    If par:Quantity > sto:Quantity_Stock
                        Found# = 1
                        Break
                    End !If par:Quantity < sto:Quantity_Stock
                Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Error
                    Found# = 1
                    Break
                End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            Else ! If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
                        !Error
            End !If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
        Of 'WAR'
            Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
            wpr:Record_Number   = stl:PartRecordNumber
            If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                        !Found
                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                sto:Ref_Number  = wpr:Part_Ref_Number
                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Found
                    If wpr:Quantity > sto:Quantity_Stock
                        Found# = 1
                        Break
                    End !If wpr:Quantity > sto:Quantity_Stock
                Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Error
                    Found# = 1
                    Break
                End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            Else ! If Access:WARPARTS.Tryfetch(war:RecordNumberKey) = Level:Benign
                        !Error
            End !If Access:WARPARTS.Tryfetch(war:RecordNumberKey) = Level:Benign
        End !Case stl:PartType
    End !Loop

    Return Found#
ShowError           PROCEDURE(STRING pText)
    CODE
        
        CreateScript(packet,'alert("' & pText & '")')
        CreateScript(packet,'window.open("frmStockAllocation","_self")')
