

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER403.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER404.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER405.INC'),ONCE        !Req'd for module callout resolution
                     END


BrowseAuditedStockUnits PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(STOAUDIT)
                      Project(stoa:Internal_AutoNumber)
                      Project(stoa:Original_Level)
                      Project(stoa:New_Level)
                      Project(stoa:Shelf_Location)
                      Join(sto:Ref_Number_Key,stoa:Stock_Ref_No)      !Compile errors? Make sure you've got a key for both files in the relation:  <=> STOCK
                        Project(sto:Description)
                        Project(sto:Part_Number)
                        Project(sto:Part_Number)
                      END
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
  CODE
  GlobalErrors.SetProcedureName('BrowseAuditedStockUnits')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseAuditedStockUnits:NoForm')
      loc:NoForm = p_web.GetValue('BrowseAuditedStockUnits:NoForm')
      loc:FormName = p_web.GetValue('BrowseAuditedStockUnits:FormName')
    else
      loc:FormName = 'BrowseAuditedStockUnits_frm'
    End
    p_web.SSV('BrowseAuditedStockUnits:NoForm',loc:NoForm)
    p_web.SSV('BrowseAuditedStockUnits:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseAuditedStockUnits:NoForm')
    loc:FormName = p_web.GSV('BrowseAuditedStockUnits:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseAuditedStockUnits') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseAuditedStockUnits')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('adiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(STOAUDIT,stoa:Internal_AutoNumber_Key,loc:vorder)
    If False
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseAuditedStockUnits:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseAuditedStockUnits:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowseAuditedStockUnits:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseAuditedStockUnits:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseAuditedStockUnits:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 15
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseAuditedStockUnits_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseAuditedStockUnits_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 6
    Loc:LocateField = ''
  of 7
    Loc:LocateField = ''
  of 8
    Loc:LocateField = ''
  of 9
    Loc:LocateField = ''
  end
  if loc:vorder = ''
    loc:vorder = '+stoa:Audit_Ref_No,+UPPER(stoa:Confirmed),+UPPER(stoa:Site_Location),+UPPER(stoa:Shelf_Location),+UPPER(stoa:Second_Location),+stoa:Internal_AutoNumber'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('sto:Description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('BrowseAuditedStockUnits_LocatorPic','@s30')
  Of upper('sto:Part_Number')
    loc:SortHeader = p_web.Translate('Part Number')
    p_web.SetSessionValue('BrowseAuditedStockUnits_LocatorPic','@s30')
  Of upper('stoa:Original_Level')
    loc:SortHeader = p_web.Translate('Orig Qty')
    p_web.SetSessionValue('BrowseAuditedStockUnits_LocatorPic','@n-14')
  Of upper('stoa:New_Level')
    loc:SortHeader = p_web.Translate('Audit Qty')
    p_web.SetSessionValue('BrowseAuditedStockUnits_LocatorPic','@n-14')
  Of upper('stoa:Shelf_Location')
    loc:SortHeader = p_web.Translate('Shelf Location')
    p_web.SetSessionValue('BrowseAuditedStockUnits_LocatorPic','@s30')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BrowseAuditedStockUnits:LookupFrom')
  End!Else
  loc:formaction = 'BrowseAuditedStockUnits'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseAuditedStockUnits:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseAuditedStockUnits:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseAuditedStockUnits:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="STOAUDIT"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="stoa:Internal_AutoNumber_Key"></input><13,10>'
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseAuditedStockUnits',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowseAuditedStockUnits',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseAuditedStockUnits.locate(''Locator2BrowseAuditedStockUnits'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseAuditedStockUnits.cl(''BrowseAuditedStockUnits'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="BrowseAuditedStockUnits_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('BrowseTable')&'" id="BrowseAuditedStockUnits_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Description')&'">'&p_web.Translate('Description')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Part Number')&'">'&p_web.Translate('Part Number')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
  If (p_web.GSV('stom:CompleteType') <> 'S') AND  true
        packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Stock Qty')&'">'&p_web.Translate('Orig Qty')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
  End ! Field condition
  If (p_web.GSV('stom:CompleteType') <> 'S') AND  true
        packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Stock Qty')&'">'&p_web.Translate('Audit Qty')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
  End ! Field condition
        packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Shelf Location')&'">'&p_web.Translate('Shelf Location')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
  If (p_web.GSV('Hide:StockupdateButtons') <> 1 AND p_web.GSV('stom:Send') <> 'S' AND p_web.GSV('locBrowseType') = 'Y') AND  true
        packet = clip(packet) & '<th>'&NBSP&'</th>'&CRLF ! no heading for this column  Increase
        do AddPacket
        loc:columns += 1
  End ! Field condition
  If (p_web.GSV('Hide:StockupdateButtons') <> 1 AND p_web.GSV('stom:Send') <> 'S' AND p_web.GSV('locBrowseType') = 'Y') AND  true
        packet = clip(packet) & '<th>'&NBSP&'</th>'&CRLF ! no heading for this column  Decrease
        do AddPacket
        loc:columns += 1
  End ! Field condition
  If (p_web.GSV('Hide:StockupdateButtons') <> 1 AND p_web.GSV('stom:Send') <> 'S' AND p_web.GSV('locBrowseType') = 'Y') AND  true
        packet = clip(packet) & '<th>'&NBSP&'</th>'&CRLF ! no heading for this column  Change
        do AddPacket
        loc:columns += 1
  End ! Field condition
  If (p_web.GSV('stom:CompleteType') = 'M') AND  true
        packet = clip(packet) & '<th>'&NBSP&'</th>'&CRLF ! no heading for this column  AuditPart
        do AddPacket
        loc:columns += 1
  End ! Field condition
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,15,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('stoa:internal_autonumber',lower(Thisview{prop:order}),1,1) = 0 !and STOAUDIT{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'stoa:Internal_AutoNumber'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('stoa:Internal_AutoNumber'),p_web.GetValue('stoa:Internal_AutoNumber'),p_web.GetSessionValue('stoa:Internal_AutoNumber'))
      loc:FilterWas = 'stoa:Audit_Ref_No = ' & p_web.GSV('stom:Audit_No') & ' AND UPPER(stoa:Confirmed) = UPPER(''' & p_web.GSV('locBrowseType') & ''') AND UPPER(stoa:Site_Location) = UPPER(''' & p_web.GSV('BookingSiteLocation') & ''')'
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseAuditedStockUnits',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseAuditedStockUnits_Filter')
    p_web.SetSessionValue('BrowseAuditedStockUnits_FirstValue','')
    p_web.SetSessionValue('BrowseAuditedStockUnits_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,STOAUDIT,stoa:Internal_AutoNumber_Key,loc:PageRows,'BrowseAuditedStockUnits',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If STOAUDIT{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(STOAUDIT,loc:firstvalue)
              Reset(ThisView,STOAUDIT)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If STOAUDIT{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(STOAUDIT,loc:lastvalue)
            Reset(ThisView,STOAUDIT)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
        IF (p_web.GSV('stom:CompleteType') = 'M' AND p_web.GSV('locManufacturer') <> '')
            IF (sto:Manufacturer <> p_web.GSV('locManufacturer'))
                CYCLE
            END ! I F
        END ! IF
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(stoa:Internal_AutoNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseAuditedStockUnits.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseAuditedStockUnits.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseAuditedStockUnits.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseAuditedStockUnits.last();',,loc:nextdisabled)
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseAuditedStockUnits',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseAuditedStockUnits_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseAuditedStockUnits_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowseAuditedStockUnits',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseAuditedStockUnits.locate(''Locator1BrowseAuditedStockUnits'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseAuditedStockUnits.cl(''BrowseAuditedStockUnits'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowseAuditedStockUnits_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseAuditedStockUnits_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseAuditedStockUnits.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseAuditedStockUnits.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseAuditedStockUnits.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseAuditedStockUnits.last();',,loc:nextdisabled)
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = stoa:Internal_AutoNumber
    p_web._thisrow = p_web._nocolon('stoa:Internal_AutoNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseAuditedStockUnits:LookupField')) = stoa:Internal_AutoNumber and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((stoa:Internal_AutoNumber = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowseAuditedStockUnits.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If STOAUDIT{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(STOAUDIT)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If STOAUDIT{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(STOAUDIT)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','stoa:Internal_AutoNumber',clip(loc:field),,loc:checked,,,'onclick="BrowseAuditedStockUnits.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','stoa:Internal_AutoNumber',clip(loc:field),,'checked',,,'onclick="BrowseAuditedStockUnits.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::sto:Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::sto:Part_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      If (p_web.GSV('stom:CompleteType') <> 'S') AND  true
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::stoa:Original_Level
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      End ! Field Condition
      If (p_web.GSV('stom:CompleteType') <> 'S') AND  true
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::stoa:New_Level
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      End ! Field Condition
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::stoa:Shelf_Location
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      If (p_web.GSV('Hide:StockupdateButtons') <> 1 AND p_web.GSV('stom:Send') <> 'S' AND p_web.GSV('locBrowseType') = 'Y') AND  true
          If Loc:Eip = 0
            if false
            elsif STOA:New_Level <= STOA:Original_Level
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            else
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            end
          end ! loc:eip = 0
          do value::Increase
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      End ! Field Condition
      If (p_web.GSV('Hide:StockupdateButtons') <> 1 AND p_web.GSV('stom:Send') <> 'S' AND p_web.GSV('locBrowseType') = 'Y') AND  true
          If Loc:Eip = 0
            if false
            elsif STOA:New_Level => STOA:Original_Level
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            else
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            end
          end ! loc:eip = 0
          do value::Decrease
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      End ! Field Condition
      If (p_web.GSV('Hide:StockupdateButtons') <> 1 AND p_web.GSV('stom:Send') <> 'S' AND p_web.GSV('locBrowseType') = 'Y') AND  true
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::Change
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      End ! Field Condition
      If (p_web.GSV('stom:CompleteType') = 'M') AND  true
          If Loc:Eip = 0
            if false
            elsif p_web.GSV('stom:Send') = 'S' OR p_web.GSV('locBrowseType') = 'Y'
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            else
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            end
          end ! loc:eip = 0
          do value::AuditPart
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      End ! Field Condition
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowseAuditedStockUnits.omv(this);" onMouseOut="BrowseAuditedStockUnits.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowseAuditedStockUnits=new browseTable(''BrowseAuditedStockUnits'','''&clip(loc:formname)&''','''&p_web._jsok('stoa:Internal_AutoNumber',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('stoa:Internal_AutoNumber')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'BrowseAuditedStockUnits.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowseAuditedStockUnits.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseAuditedStockUnits')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseAuditedStockUnits')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseAuditedStockUnits')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseAuditedStockUnits')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(STOAUDIT)
  p_web._CloseFile(STOCK)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(STOAUDIT)
  Bind(stoa:Record)
  Clear(stoa:Record)
  NetWebSetSessionPics(p_web,STOAUDIT)
  p_web._OpenFile(STOCK)
  Bind(sto:Record)
  NetWebSetSessionPics(p_web,STOCK)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('stoa:Internal_AutoNumber',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(STOAUDIT)
  Case upper(p_web.GetValue('_EIPClm'))
  of upper('Increase')
    do Validate::Increase
  of upper('Decrease')
    do Validate::Decrease
  of upper('Change')
    do Validate::Change
  of upper('AuditPart')
    do Validate::AuditPart
  End
  p_web._CloseFile(STOAUDIT)
! ----------------------------------------------------------------------------------------
value::sto:Description   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('sto:Description_'&stoa:Internal_AutoNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(sto:Description,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sto:Part_Number   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('sto:Part_Number_'&stoa:Internal_AutoNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(sto:Part_Number,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::stoa:Original_Level   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If (p_web.GSV('stom:CompleteType') <> 'S')
    if false
    else
      packet = clip(packet) & p_web._DivHeader('stoa:Original_Level_'&stoa:Internal_AutoNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(stoa:Original_Level,'@n-14')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
  End
! ----------------------------------------------------------------------------------------
value::stoa:New_Level   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If (p_web.GSV('stom:CompleteType') <> 'S')
    if false
    else
      packet = clip(packet) & p_web._DivHeader('stoa:New_Level_'&stoa:Internal_AutoNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(stoa:New_Level,'@n-14')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
  End
! ----------------------------------------------------------------------------------------
value::stoa:Shelf_Location   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('stoa:Shelf_Location_'&stoa:Internal_AutoNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(stoa:Shelf_Location,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
Validate::Increase  Routine
  data
loc:was                    Any
loc:result                 Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  stoa:Internal_AutoNumber = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  !#Bryan: Added embed to set extra key components for multi part keys
  loc:result = p_web._GetFile(STOAUDIT,stoa:Internal_AutoNumber_Key)
  p_web.FileToSessionQueue(STOAUDIT)
  do CheckForDuplicate
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::Increase   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If (p_web.GSV('Hide:StockupdateButtons') <> 1 AND p_web.GSV('stom:Send') <> 'S' AND p_web.GSV('locBrowseType') = 'Y')
    if false
    elsif STOA:New_Level <= STOA:Original_Level
      packet = clip(packet) & p_web._DivHeader('Increase_'&stoa:Internal_AutoNumber,,net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok('',0))
    else
      packet = clip(packet) & p_web._DivHeader('Increase_'&stoa:Internal_AutoNumber,,net:crc)
      loc:disabled = ''
      packet = clip(packet) & p_web.CreateButton('button','Increase Qty','Increase Qty','button-flat',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormAuditAddStock?AuditAction=add')  & '&' & p_web._noColon('stoa:Internal_AutoNumber')&'='& p_web.escape(stoa:Internal_AutoNumber) & '' ) & ''','''&clip('_self')&''')',,loc:disabled,,,,,) & '<13,10>' !2
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
  End
! ----------------------------------------------------------------------------------------
Validate::Decrease  Routine
  data
loc:was                    Any
loc:result                 Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  stoa:Internal_AutoNumber = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  !#Bryan: Added embed to set extra key components for multi part keys
  loc:result = p_web._GetFile(STOAUDIT,stoa:Internal_AutoNumber_Key)
  p_web.FileToSessionQueue(STOAUDIT)
  do CheckForDuplicate
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::Decrease   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If (p_web.GSV('Hide:StockupdateButtons') <> 1 AND p_web.GSV('stom:Send') <> 'S' AND p_web.GSV('locBrowseType') = 'Y')
    if false
    elsif STOA:New_Level => STOA:Original_Level
      packet = clip(packet) & p_web._DivHeader('Decrease_'&stoa:Internal_AutoNumber,,net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok('',0))
    else
      packet = clip(packet) & p_web._DivHeader('Decrease_'&stoa:Internal_AutoNumber,,net:crc)
      loc:disabled = ''
      packet = clip(packet) & p_web.CreateButton('button','DecreaseQty','Decrease Qty','button-flat',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormAuditAddStock?AuditAction=del')  & '&' & p_web._noColon('stoa:Internal_AutoNumber')&'='& p_web.escape(stoa:Internal_AutoNumber) & '' ) & ''','''&clip('_self')&''')',,loc:disabled,,,,,) & '<13,10>' !2
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
  End
! ----------------------------------------------------------------------------------------
Validate::Change  Routine
  data
loc:was                    Any
loc:result                 Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  stoa:Internal_AutoNumber = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  !#Bryan: Added embed to set extra key components for multi part keys
  loc:result = p_web._GetFile(STOAUDIT,stoa:Internal_AutoNumber_Key)
  p_web.FileToSessionQueue(STOAUDIT)
  do CheckForDuplicate
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::Change   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If (p_web.GSV('Hide:StockupdateButtons') <> 1 AND p_web.GSV('stom:Send') <> 'S' AND p_web.GSV('locBrowseType') = 'Y')
    if false
    else
      packet = clip(packet) & p_web._DivHeader('Change_'&stoa:Internal_AutoNumber,,net:crc)
      loc:disabled = ''
      packet = clip(packet) & p_web.CreateButton('button','ChangeQty','Change Qty','button-flat',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormStockChangeAuditLevel')  & '&' & p_web._noColon('stoa:Internal_AutoNumber')&'='& p_web.escape(stoa:Internal_AutoNumber) & '' ) & ''','''&clip('_self')&''')',,loc:disabled,,,,,) & '<13,10>' !2
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
  End
! ----------------------------------------------------------------------------------------
Validate::AuditPart  Routine
  data
loc:was                    Any
loc:result                 Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  stoa:Internal_AutoNumber = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  !#Bryan: Added embed to set extra key components for multi part keys
  loc:result = p_web._GetFile(STOAUDIT,stoa:Internal_AutoNumber_Key)
  p_web.FileToSessionQueue(STOAUDIT)
  do CheckForDuplicate
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::AuditPart   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If (p_web.GSV('stom:CompleteType') = 'M')
    if false
    elsif p_web.GSV('stom:Send') = 'S' OR p_web.GSV('locBrowseType') = 'Y'
      packet = clip(packet) & p_web._DivHeader('AuditPart_'&stoa:Internal_AutoNumber,,net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok('',0))
    else
      packet = clip(packet) & p_web._DivHeader('AuditPart_'&stoa:Internal_AutoNumber,,net:crc)
      loc:disabled = ''
      packet = clip(packet) & p_web.CreateButton('button','AuditPart','Audit Part','SmallButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormStockAuditProcess?AuditPart=1')  & '&' & p_web._noColon('stoa:Internal_AutoNumber')&'='& p_web.escape(stoa:Internal_AutoNumber) & '' ) & ''','''&clip('_self')&''')',,loc:disabled,,,,,) & '<13,10>' !2
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
  End
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = stoa:Internal_AutoNumber

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('stoa:Internal_AutoNumber',stoa:Internal_AutoNumber)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('stoa:Internal_AutoNumber',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('stoa:Internal_AutoNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('stoa:Internal_AutoNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
