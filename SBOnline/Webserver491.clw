

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER491.INC'),ONCE        !Local module procedure declarations
                     END


WarrantyClaimsBrowse_NEW PROCEDURE  (NetWebServerWorker p_web)
locJobNumber         STRING(30)                            !
locCurrentStatus     STRING(30)                            !
locBranchID          STRING(2)                             !
locDaysToFinalRejection STRING(5)                          !
locRejectionReason   STRING(255)                           !
locSubmissions       LONG                                  !
                    MAP
SetRejectionReason      PROCEDURE(STRING pType)
                    END !MAP

EDIType                         STRING(3)
StartDate                       DATE()
EndDate                         DATE()
WarrantyManufacturer            STRING(30)
BookingAccount                  STRING(30)
BranchID                        LONG()
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(JOBSWARR)
                      Project(jow:RecordNumber)
                      Project(jow:RefNumber)
                      Project(jow:Manufacturer)
                      Project(jow:DateRejected)
                      Project(jow:DateFinalRejection)
                      Project(jow:ClaimSubmitted)
                      Project(jow:DateAccepted)
                      Project(jow:RRCDateReconciled)
                      Project(jow:Orig_Sub_Date)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
WEBJOB::State  USHORT
JOBS::State  USHORT
JOBSWARR::State  USHORT
AUDIT2::State  USHORT
AUDIT::State  USHORT
TRADEACC::State  USHORT
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('WarrantyClaimsBrowse_NEW')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('WarrantyClaimsBrowse_NEW:NoForm')
      loc:NoForm = p_web.GetValue('WarrantyClaimsBrowse_NEW:NoForm')
      loc:FormName = p_web.GetValue('WarrantyClaimsBrowse_NEW:FormName')
    else
      loc:FormName = 'WarrantyClaimsBrowse_NEW_frm'
    End
    p_web.SSV('WarrantyClaimsBrowse_NEW:NoForm',loc:NoForm)
    p_web.SSV('WarrantyClaimsBrowse_NEW:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('WarrantyClaimsBrowse_NEW:NoForm')
    loc:FormName = p_web.GSV('WarrantyClaimsBrowse_NEW:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('WarrantyClaimsBrowse_NEW') & '_' & lower(loc:parent)
  else
    loc:divname = lower('WarrantyClaimsBrowse_NEW')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(JOBSWARR,jow:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'JOW:REFNUMBER') then p_web.SetValue('WarrantyClaimsBrowse_NEW_sort','1')
    ElsIf (loc:vorder = 'JOW:DATEREJECTED') then p_web.SetValue('WarrantyClaimsBrowse_NEW_sort','11')
    ElsIf (loc:vorder = 'JOW:DATEFINALREJECTION') then p_web.SetValue('WarrantyClaimsBrowse_NEW_sort','15')
    ElsIf (loc:vorder = 'JOW:CLAIMSUBMITTED') then p_web.SetValue('WarrantyClaimsBrowse_NEW_sort','8')
    ElsIf (loc:vorder = 'JOW:RRCDATERECONCILED') then p_web.SetValue('WarrantyClaimsBrowse_NEW_sort','16')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('WarrantyClaimsBrowse_NEW:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('WarrantyClaimsBrowse_NEW:LookupFrom','LookupFrom')
    p_web.StoreValue('WarrantyClaimsBrowse_NEW:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('WarrantyClaimsBrowse_NEW:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('WarrantyClaimsBrowse_NEW:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
    BookingAccount = p_web.GSV('BookingAccount')
    BranchID = p_web.GSV('BookingBranchID')
    StartDate = p_web.GSV('locWarrantyStartDate')
    EndDate = p_web.GSV('locWarrantyEndDate')
    EDIType = p_web.GSV('locEDIType')
    WarrantyManufacturer = p_web.GSV('locWarrantyManufacturer')
    BIND('BookingAccount',BookingAccount)
    BIND('BranchID',BranchID)
    BIND('EndDate',EndDate)
    BIND('StartDate',StartDate)
    BIND('EDIType',EDIType)
    BIND('WarrantyManufacturer',WarrantyManufacturer)
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 15
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('WarrantyClaimsBrowse_NEW_sort',net:DontEvaluate)
  If loc:vordernumber = 0
    loc:vordernumber = 1
  End
  p_web.SetSessionValue('WarrantyClaimsBrowse_NEW_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'jow:RefNumber','-jow:RefNumber')
    Loc:LocateField = 'jow:RefNumber'
  of 11
    loc:vorder = Choose(Loc:SortDirection=1,'jow:DateRejected','-jow:DateRejected')
    Loc:LocateField = 'jow:DateRejected'
  of 15
    loc:vorder = Choose(Loc:SortDirection=1,'jow:DateFinalRejection','-jow:DateFinalRejection')
    Loc:LocateField = 'jow:DateFinalRejection'
  of 8
    loc:vorder = Choose(Loc:SortDirection=1,'jow:ClaimSubmitted','-jow:ClaimSubmitted')
    Loc:LocateField = 'jow:ClaimSubmitted'
  of 16
    loc:vorder = Choose(Loc:SortDirection=1,'jow:RRCDateReconciled','-jow:RRCDateReconciled')
    Loc:LocateField = 'jow:RRCDateReconciled'
  of 18
    Loc:LocateField = ''
  of 17
    Loc:LocateField = ''
  of 19
    Loc:LocateField = ''
  end
  if loc:vorder = ''
    loc:vorder = '+jow:RecordNumber'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('jow:RefNumber')
    loc:SortHeader = p_web.Translate('Job Number')
    p_web.SetSessionValue('WarrantyClaimsBrowse_NEW_LocatorPic','@s30')
  Of upper('job:Account_Number')
    loc:SortHeader = p_web.Translate('Account Number')
    p_web.SetSessionValue('WarrantyClaimsBrowse_NEW_LocatorPic','@s15')
  Of upper('job:Company_Name')
    loc:SortHeader = p_web.Translate('Company Name')
    p_web.SetSessionValue('WarrantyClaimsBrowse_NEW_LocatorPic','@s30')
  Of upper('job:ESN')
    loc:SortHeader = p_web.Translate('I.M.E.I. Number')
    p_web.SetSessionValue('WarrantyClaimsBrowse_NEW_LocatorPic','@s20')
  Of upper('jow:Manufacturer')
    loc:SortHeader = p_web.Translate('Manufacturer')
    p_web.SetSessionValue('WarrantyClaimsBrowse_NEW_LocatorPic','@s30')
  Of upper('locCurrentStatus')
    loc:SortHeader = p_web.Translate('Current Status')
    p_web.SetSessionValue('WarrantyClaimsBrowse_NEW_LocatorPic','@s30')
  Of upper('jow:DateRejected')
    loc:SortHeader = p_web.Translate('Rejected')
    p_web.SetSessionValue('WarrantyClaimsBrowse_NEW_LocatorPic','@d6b')
  Of upper('locSubmissions')
    loc:SortHeader = p_web.Translate('Submissions')
    p_web.SetSessionValue('WarrantyClaimsBrowse_NEW_LocatorPic','@s5')
  Of upper('locDaysToFinalRejection')
    loc:SortHeader = p_web.Translate('Dys To Final Rej')
    p_web.SetSessionValue('WarrantyClaimsBrowse_NEW_LocatorPic','@s5')
  Of upper('jow:DateFinalRejection')
    loc:SortHeader = p_web.Translate('Rejected')
    p_web.SetSessionValue('WarrantyClaimsBrowse_NEW_LocatorPic','@d6b')
  Of upper('locRejectionReason')
    loc:SortHeader = p_web.Translate('Reason')
    p_web.SetSessionValue('WarrantyClaimsBrowse_NEW_LocatorPic','@s255')
  Of upper('job:Location')
    loc:SortHeader = p_web.Translate('Location')
    p_web.SetSessionValue('WarrantyClaimsBrowse_NEW_LocatorPic','@s30')
  Of upper('jow:ClaimSubmitted')
    loc:SortHeader = p_web.Translate('Submitted')
    p_web.SetSessionValue('WarrantyClaimsBrowse_NEW_LocatorPic','@d6')
  Of upper('jow:DateAccepted')
    loc:SortHeader = p_web.Translate('Approved')
    p_web.SetSessionValue('WarrantyClaimsBrowse_NEW_LocatorPic','@d6')
  Of upper('jow:RRCDateReconciled')
    loc:SortHeader = p_web.Translate('Reconciled')
    p_web.SetSessionValue('WarrantyClaimsBrowse_NEW_LocatorPic','@d6')
  Of upper('jow:Orig_Sub_Date')
    loc:SortHeader = p_web.Translate('Orig Sub Date')
    p_web.SetSessionValue('WarrantyClaimsBrowse_NEW_LocatorPic','@d17b')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('WarrantyClaimsBrowse_NEW:LookupFrom')
  End!Else
  loc:formaction = 'WarrantyClaimsBrowse_NEW'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="WarrantyClaimsBrowse_NEW:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="WarrantyClaimsBrowse_NEW:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
    do SendPacket
    Do showwait
    do SendPacket
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('WarrantyClaimsBrowse_NEW:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="JOBSWARR"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="jow:RecordNumberKey"></input><13,10>'
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'WarrantyClaimsBrowse_NEW',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator fixedtd')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2WarrantyClaimsBrowse_NEW',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator fixedtd',,'onchange="WarrantyClaimsBrowse_NEW.locate(''Locator2WarrantyClaimsBrowse_NEW'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'WarrantyClaimsBrowse_NEW.cl(''WarrantyClaimsBrowse_NEW'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="WarrantyClaimsBrowse_NEW_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('BrowseTable')&'" id="WarrantyClaimsBrowse_NEW_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','WarrantyClaimsBrowse_NEW','Job Number','Click here to sort by Job Number',,,90,1)
        Else
          packet = clip(packet) & '<th width="'&clip(90)&'" Title="'&p_web.Translate('Click here to sort by Job Number')&'">'&p_web.Translate('Job Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Account Number')&'">'&p_web.Translate('Account Number')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Company Name')&'">'&p_web.Translate('Company Name')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th width="'&clip(100)&'" Title="'&p_web.Translate('Click here to sort by I.M.E.I. Number')&'">'&p_web.Translate('I.M.E.I. Number')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Manufacturer')&'">'&p_web.Translate('Manufacturer')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th width="'&clip(150)&'">'&p_web.Translate('Current Status')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
  If (p_web.GSV('locEDIType') = 'EXC') AND  true
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'11','WarrantyClaimsBrowse_NEW','Rejected','Click here to sort by Date Rejected',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Date Rejected')&'">'&p_web.Translate('Rejected')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  End ! Field condition
  If (p_web.GSV('locEDIType') = 'EXC') AND  true
        packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Number Of Times Submitted')&'">'&p_web.Translate('Submissions')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
  End ! Field condition
  If (p_web.GSV('locEDIType') = 'EXC') AND  true
        packet = clip(packet) & '<th>'&p_web.Translate('Dys To Final Rej')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
  End ! Field condition
  If (p_web.GSV('locEDIType') = 'AAJ' OR p_web.GSV('locEDIType') = 'REJ') AND  true
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'15','WarrantyClaimsBrowse_NEW','Rejected','Click here to sort by Date Final Rejection',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Date Final Rejection')&'">'&p_web.Translate('Rejected')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  End ! Field condition
  If (p_web.GSV('locEDIType') = 'EXC' OR p_web.GSV('locEDIType') = 'AAJ' OR p_web.GSV('locEDIType') = 'REJ') AND  true
        packet = clip(packet) & '<th>'&p_web.Translate('Reason')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
  End ! Field condition
        packet = clip(packet) & '<th width="'&clip(100)&'" Title="'&p_web.Translate('Click here to sort by Location')&'">'&p_web.Translate('Location')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
  If (p_web.GSV('locEDIType') = 'NO') AND  true
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'8','WarrantyClaimsBrowse_NEW','Submitted','Click here to sort by Claim Submitted',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Claim Submitted')&'">'&p_web.Translate('Submitted')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  End ! Field condition
  If (p_web.GSV('locEDIType') = 'APP') AND  true
        packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Date Accepted')&'">'&p_web.Translate('Approved')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
  End ! Field condition
  If (p_web.GSV('locEDIType') = 'PAY') AND  true
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'16','WarrantyClaimsBrowse_NEW','Reconciled','Click here to sort by Date Reconciled',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Date Reconciled')&'">'&p_web.Translate('Reconciled')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  End ! Field condition
        packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Orig Sub Date')&'">'&p_web.Translate('Orig Sub Date')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th>'&NBSP&'</th>'&CRLF ! no heading for this column  btnAuditTrail
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th>'&NBSP&'</th>'&CRLF ! no heading for this column  btnCustServ
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th>'&NBSP&'</th>'&CRLF ! no heading for this column  EngScreen
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:LocateField = 'jow:DateRejected' then Loc:NoBuffer = 1.
  If Loc:LocateField = 'jow:DateFinalRejection' then Loc:NoBuffer = 1.
  If Loc:LocateField = 'jow:ClaimSubmitted' then Loc:NoBuffer = 1.
  If Loc:LocateField = 'jow:DateAccepted' then Loc:NoBuffer = 1.
  If Loc:LocateField = 'jow:RRCDateReconciled' then Loc:NoBuffer = 1.
  If Loc:LocateField = 'jow:Orig_Sub_Date' then Loc:NoBuffer = 1.
  If Loc:NoBuffer = 0
    Buffer(ThisView,15,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('jow:recordnumber',lower(Thisview{prop:order}),1,1) = 0 !and JOBSWARR{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'jow:RecordNumber'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('jow:RecordNumber'),p_web.GetValue('jow:RecordNumber'),p_web.GetSessionValue('jow:RecordNumber'))
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'WarrantyClaimsBrowse_NEW',Net:Both)
    IF (p_web.GSV('locWarrantyDateFilter') = 0)
        ThisView{PROP:Filter} = 'FALSE'
    ELSE
        
        CASE p_web.GSV('locEDIType')
        OF 'NO'
            IF (p_web.GSV('locWarrantyManufacturerFilter') = 1 AND p_web.GSV('locWarrantyManufacturer') <> '')
                ThisView{Prop:Filter} = 'jow:BranchID = BranchID AND UPPER(jow:RepairedAt) = ''RRC'' AND ' & |
                    'jow:ClaimSubmitted >= StartDate AND jow:ClaimSubmitted <= EndDate AND ' & |
                    'UPPER(jow:RRCStatus) = UPPER(EDIType) AND UPPER(jow:Manufacturer) = UPPER(WarrantyManufacturer)'
    
            ELSE
                ThisView{Prop:Filter} = 'jow:BranchID = BranchID AND UPPER(jow:RepairedAt) = ''RRC'' AND ' & |
                    'jow:ClaimSubmitted >= StartDate AND jow:ClaimSubmitted <= EndDate AND ' & |
                    'UPPER(jow:RRCStatus) = UPPER(EDIType)'
            END ! IF
        OF 'APP'             
            IF (p_web.GSV('locWarrantyManufacturerFilter') = 1 AND p_web.GSV('locWarrantyManufacturer') <> '')       
                ThisView{Prop:Filter} = 'jow:BranchID = BranchID AND UPPER(jow:RepairedAt) = ''RRC'' AND ' & |
                    'UPPER(jow:RRCStatus) = UPPER(EDIType) AND ' & |
                    'UPPER(jow:Manufacturer) = UPPER(WarrantyManufacturer) AND ' & |
                    'jow:DateAccepted >= StartDate AND ' & |
                    'jow:DateAccepted <= EndDate'
                        
            ELSE ! 
                ThisView{Prop:Filter} = 'jow:BranchID = BranchID AND jow:DateAccepted >= StartDate AND ' & |
                    'jow:DateAccepted <= EndDate AND UPPER(jow:RepairedAt) = ''RRC'' AND ' & |
                    'UPPER(jow:RRCStatus) = UPPER(EDIType)'    
            END ! 
        OF 'EXC'
            IF (p_web.GSV('locWarrantyManufacturerFilter') = 1 AND p_web.GSV('locWarrantyManufacturer') <> '')
                ThisView{prop:Filter} = 'jow:BranchID = BranchID AND jow:DateRejected >= StartDate AND ' & |
                    'jow:DateRejected <= EndDate AND UPPER(jow:RepairedAt) = ''RRC'' AND ' & |
                    'UPPER(jow:RRCStatus) = UPPER(EDIType) AND UPPER(jow:Manufacturer) = UPPER(WarrantyManufacturer)'
            ELSE ! IF
                ThisView{prop:Filter} = 'jow:BranchID = BranchID AND jow:DateRejected >= StartDate AND ' & |
                    'jow:DateRejected <= EndDate AND UPPER(jow:RepairedAt) = ''RRC'' AND ' & |
                    'UPPER(jow:RRCStatus) = UPPER(EDIType)'
            END ! IF
        OF 'AAJ' OROF 'REJ'  
            IF (p_web.GSV('locWarrantyManufacturerFilter') = 1 AND p_web.GSV('locWarrantyManufacturer') <> '')
                ThisView{prop:Filter} = 'jow:BranchID = BranchID AND jow:DateFinalRejection >= StartDate AND ' & |
                    'jow:DateFinalRejection <= EndDate AND UPPER(jow:RepairedAt) = ''RRC'' AND ' & |
                    'UPPER(jow:RRCStatus) = UPPER(EDIType)' 
            ELSE
                ThisView{prop:Filter} = 'jow:BranchID = BranchID AND jow:DateFinalRejection >= StartDate AND ' & |
                    'jow:DateFinalRejection <= EndDate AND UPPER(jow:RepairedAt) = ''RRC'' AND ' & |
                    'UPPER(jow:RRCStatus) = UPPER(EDIType) AND UPPER(jow:Manufacturer) = UPPER(WarrantyManufacturer)'
            END
        OF 'PAY'
            IF (p_web.GSV('locWarrantyManufacturerFilter') = 1 AND p_web.GSV('locWarrantyManufacturer') <> '')
                ThisView{prop:Filter} = 'jow:BranchID = BranchID AND UPPER(jow:RepairedAt) = ''RRC'' AND ' & |
                    'UPPER(jow:RRCStatus) = ''PAY'' AND UPPER(jow:Manufacturer) = UPPER(WarrantyManufacturer) AND jow:RRCDateReconciled >= StartDate AND ' & |
                    'jow:RRCDateReconciled <= EndDate'
            ELSE
                ThisView{prop:Filter} = 'jow:BranchID = BranchID AND UPPER(jow:RepairedAt) = ''RRC'' AND ' & |
                    'UPPER(jow:RRCStatus) = ''PAY'' AND jow:RRCDateReconciled >= StartDate AND ' & |
                    'jow:RRCDateReconciled <= EndDate'
            END 
        ELSE
            ThisView{prop:Filter} = FALSE
        END ! CASE
    END ! CASE
    
  
  
  !ThisView{PROP:Filter} = 'jow:BranchID = BranchID AND jow:DateFinalRejection >= ProgressStartDate AND jow:DateFinalRejection <= ProgressEndDate AND ' & |
  !       'UPPER(jow:RepairedAt) = ''RRC'' AND UPPER(jow:RRCStatus) = ''REJ'''
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('WarrantyClaimsBrowse_NEW_Filter')
    p_web.SetSessionValue('WarrantyClaimsBrowse_NEW_FirstValue','')
    p_web.SetSessionValue('WarrantyClaimsBrowse_NEW_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,JOBSWARR,jow:RecordNumberKey,loc:PageRows,'WarrantyClaimsBrowse_NEW',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If JOBSWARR{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(JOBSWARR,loc:firstvalue)
              Reset(ThisView,JOBSWARR)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If JOBSWARR{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(JOBSWARR,loc:lastvalue)
            Reset(ThisView,JOBSWARR)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      ! Load Children
        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = jow:RefNumber
        Access:JOBS.TryFetch(job:Ref_Number_Key)
        
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        Access:WEBJOB.TryFetch(wob:RefNumberKey)
      !        IF (p_web.GSV('locWarrantyManufacturerFilter') = 1)
      !            IF (sbojow:Manufacturer <> p_web.GSV('locWarrantyManufacturer'))
      !                CYCLE
      !            END ! IF
      !        END ! IF
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(jow:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'WarrantyClaimsBrowse_NEW.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'WarrantyClaimsBrowse_NEW.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'WarrantyClaimsBrowse_NEW.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'WarrantyClaimsBrowse_NEW.last();',,loc:nextdisabled)
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'WarrantyClaimsBrowse_NEW',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('WarrantyClaimsBrowse_NEW_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('WarrantyClaimsBrowse_NEW_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator fixedtd')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1WarrantyClaimsBrowse_NEW',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator fixedtd',,'onchange="WarrantyClaimsBrowse_NEW.locate(''Locator1WarrantyClaimsBrowse_NEW'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'WarrantyClaimsBrowse_NEW.cl(''WarrantyClaimsBrowse_NEW'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('WarrantyClaimsBrowse_NEW_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('WarrantyClaimsBrowse_NEW_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'WarrantyClaimsBrowse_NEW.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'WarrantyClaimsBrowse_NEW.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'WarrantyClaimsBrowse_NEW.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'WarrantyClaimsBrowse_NEW.last();',,loc:nextdisabled)
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    !    locJobNumber = CLIP(job:Ref_Number) & '-' & p_web.GSV('locBranchID') & CLIP(wob:JobNumber)    
    
    IF (job:Exchange_Unit_Number > 0)
        locCurrentStatus = 'E' & wob:Exchange_Status
    ELSE
        locCurrentStatus = 'J' & wob:Current_Status
    END ! IF
    
    ! Get Rejection Reason
    SetRejectionReason(jow:RRCStatus)
    loc:field = jow:RecordNumber
    p_web._thisrow = p_web._nocolon('jow:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('WarrantyClaimsBrowse_NEW:LookupField')) = jow:RecordNumber and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((jow:RecordNumber = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="WarrantyClaimsBrowse_NEW.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If JOBSWARR{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(JOBSWARR)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If JOBSWARR{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(JOBSWARR)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','jow:RecordNumber',clip(loc:field),,loc:checked,,,'onclick="WarrantyClaimsBrowse_NEW.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','jow:RecordNumber',clip(loc:field),,'checked',,,'onclick="WarrantyClaimsBrowse_NEW.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip(90)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locJobNumber
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::job:Account_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::job:Company_Name
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip(100)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::job:ESN
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::jow:Manufacturer
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip(150)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locCurrentStatus
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      If (p_web.GSV('locEDIType') = 'EXC') AND  true
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::jow:DateRejected
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      End ! Field Condition
      If (p_web.GSV('locEDIType') = 'EXC') AND  true
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locSubmissions
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      End ! Field Condition
      If (p_web.GSV('locEDIType') = 'EXC') AND  true
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locDaysToFinalRejection
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      End ! Field Condition
      If (p_web.GSV('locEDIType') = 'AAJ' OR p_web.GSV('locEDIType') = 'REJ') AND  true
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::jow:DateFinalRejection
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      End ! Field Condition
      If (p_web.GSV('locEDIType') = 'EXC' OR p_web.GSV('locEDIType') = 'AAJ' OR p_web.GSV('locEDIType') = 'REJ') AND  true
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locRejectionReason
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      End ! Field Condition
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip(100)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::job:Location
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      If (p_web.GSV('locEDIType') = 'NO') AND  true
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::jow:ClaimSubmitted
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      End ! Field Condition
      If (p_web.GSV('locEDIType') = 'APP') AND  true
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::jow:DateAccepted
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      End ! Field Condition
      If (p_web.GSV('locEDIType') = 'PAY') AND  true
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::jow:RRCDateReconciled
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      End ! Field Condition
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::jow:Orig_Sub_Date
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::btnAuditTrail
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::btnCustServ
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::EngScreen
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="WarrantyClaimsBrowse_NEW.omv(this);" onMouseOut="WarrantyClaimsBrowse_NEW.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var WarrantyClaimsBrowse_NEW=new browseTable(''WarrantyClaimsBrowse_NEW'','''&clip(loc:formname)&''','''&p_web._jsok('jow:RecordNumber',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('jow:RecordNumber')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'WarrantyClaimsBrowse_NEW.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'WarrantyClaimsBrowse_NEW.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2WarrantyClaimsBrowse_NEW')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1WarrantyClaimsBrowse_NEW')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1WarrantyClaimsBrowse_NEW')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2WarrantyClaimsBrowse_NEW')
          end
        End
      End
    End
      do SendPacket
      Do afterwait
      do SendPacket
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(JOBSWARR)
  p_web._CloseFile(WEBJOB)
  p_web._CloseFile(JOBS)
  p_web._CloseFile(JOBSWARR)
  p_web._CloseFile(AUDIT2)
  p_web._CloseFile(AUDIT)
  p_web._CloseFile(TRADEACC)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(JOBSWARR)
  Bind(jow:Record)
  Clear(jow:Record)
  NetWebSetSessionPics(p_web,JOBSWARR)
  p_web._OpenFile(WEBJOB)
  Bind(wob:Record)
  NetWebSetSessionPics(p_web,WEBJOB)
  p_web._OpenFile(JOBS)
  Bind(job:Record)
  NetWebSetSessionPics(p_web,JOBS)
  p_web._OpenFile(JOBSWARR)
  Bind(jow:Record)
  NetWebSetSessionPics(p_web,JOBSWARR)
  p_web._OpenFile(AUDIT2)
  Bind(aud2:Record)
  NetWebSetSessionPics(p_web,AUDIT2)
  p_web._OpenFile(AUDIT)
  Bind(aud:Record)
  NetWebSetSessionPics(p_web,AUDIT)
  p_web._OpenFile(TRADEACC)
  Bind(tra:Record)
  NetWebSetSessionPics(p_web,TRADEACC)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('jow:RecordNumber',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
    p_web.SSV('job:Ref_Number',jow:RefNumber)    
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(JOBSWARR)
  Case upper(p_web.GetValue('_EIPClm'))
  of upper('btnAuditTrail')
    do Validate::btnAuditTrail
  of upper('btnCustServ')
    do Validate::btnCustServ
  of upper('EngScreen')
    do Validate::EngScreen
  End
  p_web._CloseFile(JOBSWARR)
! ----------------------------------------------------------------------------------------
value::locJobNumber   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locJobNumber_'&jow:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(wob:RefNumber & '-' & p_web.GSV('BookingBranchID') & wob:JobNumber,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::job:Account_Number   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('job:Account_Number_'&jow:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(job:Account_Number,'@s15')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::job:Company_Name   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('job:Company_Name_'&jow:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(job:Company_Name,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::job:ESN   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('job:ESN_'&jow:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(job:ESN,'@s20')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::jow:Manufacturer   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('jow:Manufacturer_'&jow:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(jow:Manufacturer,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locCurrentStatus   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locCurrentStatus_'&jow:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locCurrentStatus,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::jow:DateRejected   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If (p_web.GSV('locEDIType') = 'EXC')
    if false
    else
      packet = clip(packet) & p_web._DivHeader('jow:DateRejected_'&jow:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(jow:DateRejected,'@d6b')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
  End
! ----------------------------------------------------------------------------------------
value::locSubmissions   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If (p_web.GSV('locEDIType') = 'EXC')
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locSubmissions_'&jow:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locSubmissions,'@s5')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
  End
! ----------------------------------------------------------------------------------------
value::locDaysToFinalRejection   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If (p_web.GSV('locEDIType') = 'EXC')
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locDaysToFinalRejection_'&jow:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locDaysToFinalRejection,'@s5')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
  End
! ----------------------------------------------------------------------------------------
value::jow:DateFinalRejection   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If (p_web.GSV('locEDIType') = 'AAJ' OR p_web.GSV('locEDIType') = 'REJ')
    if false
    else
      packet = clip(packet) & p_web._DivHeader('jow:DateFinalRejection_'&jow:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(jow:DateFinalRejection,'@d6b')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
  End
! ----------------------------------------------------------------------------------------
value::locRejectionReason   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If (p_web.GSV('locEDIType') = 'EXC' OR p_web.GSV('locEDIType') = 'AAJ' OR p_web.GSV('locEDIType') = 'REJ')
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locRejectionReason_'&jow:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locRejectionReason,'@s255')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
  End
! ----------------------------------------------------------------------------------------
value::job:Location   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('job:Location_'&jow:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(job:Location,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::jow:ClaimSubmitted   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If (p_web.GSV('locEDIType') = 'NO')
    if false
    else
      packet = clip(packet) & p_web._DivHeader('jow:ClaimSubmitted_'&jow:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(jow:ClaimSubmitted,'@d6')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
  End
! ----------------------------------------------------------------------------------------
value::jow:DateAccepted   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If (p_web.GSV('locEDIType') = 'APP')
    if false
    else
      packet = clip(packet) & p_web._DivHeader('jow:DateAccepted_'&jow:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(jow:DateAccepted,'@d6')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
  End
! ----------------------------------------------------------------------------------------
value::jow:RRCDateReconciled   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If (p_web.GSV('locEDIType') = 'PAY')
    if false
    else
      packet = clip(packet) & p_web._DivHeader('jow:RRCDateReconciled_'&jow:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(jow:RRCDateReconciled,'@d6')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
  End
! ----------------------------------------------------------------------------------------
value::jow:Orig_Sub_Date   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('jow:Orig_Sub_Date_'&jow:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(jow:Orig_Sub_Date,'@d17b')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
Validate::btnAuditTrail  Routine
  data
loc:was                    Any
loc:result                 Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  jow:RecordNumber = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  !#Bryan: Added embed to set extra key components for multi part keys
  loc:result = p_web._GetFile(JOBSWARR,jow:RecordNumberKey)
  p_web.FileToSessionQueue(JOBSWARR)
  do CheckForDuplicate
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::btnAuditTrail   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('btnAuditTrail_'&jow:RecordNumber,,net:crc)
      loc:disabled = ''
      packet = clip(packet) & p_web.CreateButton('button','btnAuditTrail','Audit Trail','button-flat',loc:formname,,,'window.open('''& p_web._MakeURL(clip('BrowseAuditFilterSimple?job__Ref_Number=' & job:Ref_Number & '&ReturnURL=WarrantyClaims')  & '&' & p_web._noColon('jow:RecordNumber')&'='& p_web.escape(jow:RecordNumber) & '&PressedButton=' ) & ''','''&clip('_self')&''')',,loc:disabled,,,,,) & '<13,10>' !2
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
Validate::btnCustServ  Routine
  data
loc:was                    Any
loc:result                 Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  jow:RecordNumber = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  !#Bryan: Added embed to set extra key components for multi part keys
  loc:result = p_web._GetFile(JOBSWARR,jow:RecordNumberKey)
  p_web.FileToSessionQueue(JOBSWARR)
  do CheckForDuplicate
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::btnCustServ   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('btnCustServ_'&jow:RecordNumber,,net:crc)
      loc:disabled = ''
      packet = clip(packet) & p_web.CreateButton('button','btnCustServ','Cust Serv','button-flat',loc:formname,,,'window.open('''& p_web._MakeURL(clip('CustomerServicesForm?wob__RecordNumber=' & wob:RecordNumber & '&Change_btn=Change&Amend=0&FirstTime=1&CustServReturnURL=WarrantyClaims')& '' ) & ''','''&clip('_self')&''')',,loc:disabled,,,,,) & '<13,10>' !3
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
Validate::EngScreen  Routine
  data
loc:was                    Any
loc:result                 Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  jow:RecordNumber = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  !#Bryan: Added embed to set extra key components for multi part keys
  loc:result = p_web._GetFile(JOBSWARR,jow:RecordNumberKey)
  p_web.FileToSessionQueue(JOBSWARR)
  do CheckForDuplicate
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::EngScreen   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('EngScreen_'&jow:RecordNumber,,net:crc)
      loc:disabled = ''
      If '_self'='_self' and Loc:Disabled = 0
        Loc:Disabled = -1
      End
      ! the button is set as a "submit" button, and this must have a URL
      packet = clip(packet) & p_web.CreateButton('submit','btnEngScreen','Eng Screen','button-flat',loc:formname,p_web._MakeURL(clip('ViewJob?&wob__RecordNumber=' & wob:RecordNumber & '&Change_btn=Change&ViewJobReturnURL=WarrantyClaims')  & '&' & p_web._noColon('jow:RecordNumber')&'='& p_web.escape(jow:RecordNumber) & ''),clip('_self'),,,loc:disabled,,,,,) & '<13,10>' !4
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(JOBSWARR)
  p_web._OpenFile(AUDIT2)
  p_web._OpenFile(AUDIT)
  p_web._OpenFile(TRADEACC)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(JOBSWARR)
  p_Web._CloseFile(AUDIT2)
  p_Web._CloseFile(AUDIT)
  p_Web._CloseFile(TRADEACC)
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = jow:RecordNumber

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('jow:RecordNumber',jow:RecordNumber)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('jow:RecordNumber',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('jow:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('jow:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
showwait  Routine
  packet = clip(packet) & |
    '<<script><13,10>'&|
    'document.getElementById("_busy").style.cssText="visibility:visible";<13,10>'&|
    '<</script><13,10>'&|
    ''
afterwait  Routine
  packet = clip(packet) & |
    '<<script><13,10>'&|
    'document.getElementById("_busy").style.cssText="visibility:hidden";<13,10>'&|
    '<</script><13,10>'&|
    ''
SetRejectionReason  PROCEDURE(STRING pType)

    CODE
    
        locRejectionReason = ''
        locDaysToFinalRejection = ''
        CASE pType
        OF 'EXC'
            
            Access:MANUFACT.ClearKey(man:Manufacturer_Key)
            man:Manufacturer = jow:Manufacturer
            IF (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
                locDaysToFinalRejection = jow:DateRejected + man:ResubmissionLimit - TODAY()
            END ! IF
            
            Access:AUDIT.Clearkey(aud:TypeActionKey)
            aud:Ref_Number = jow:RefNumber
            aud:Type = 'JOB'
            aud:Action = 'WARRANTY CLAIM REJECTED'
            Set(aud:TypeActionKey,aud:TypeActionKey)
            Loop ! Begin Loop
                If Access:AUDIT.Next()
                    Break
                End ! If Access:AUDIT.Next()
                If aud:Ref_Number <> jow:RefNumber
                    Break
                End ! If aud:Ref_Number <> jow:RefNumber
                If aud:Type <> 'JOB'
                    Break
                End ! If aud:Type = 'JOB'
                If aud:Action <> 'WARRANTY CLAIM REJECTED'
                    Break
                End ! If aud:Action <> 'WARRANTY CLAIM REJECTED'
                Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
                aud2:AUDRecordNumber = aud:Record_Number
                IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)

                END ! IF
                locRejectionReason =  Clip(Sub(aud2:Notes,9,60))
                if (jow:DateRejected = 0)
                    jow:DateRejected = aud:Date
                    If man:UseResubmissionLimit
                        locDaysToFinalRejection = jow:DateRejected + man:ResubmissionLimit - TODAY()
                    End ! If man:UseResubmissionLimit
                end

                break
            End ! Loop

            Count# = 1
            Access:AUDIT.Clearkey(aud:TypeActionKey)
            aud:Ref_Number = jow:RefNumber
            aud:Type = 'JOB'
            aud:Action = 'WARRANTY CLAIM RESUBMITTED'
            Set(aud:TypeActionKey,aud:TypeActionKey)
            Loop ! Begin Loop
                If Access:AUDIT.Next()
                    Break
                End ! If Access:AUDIT.Next()
                If aud:Ref_Number <> jow:RefNumber
                    Break
                End ! If aud:Ref_Number <> jow:RefNumber
                If aud:Type <> 'JOB'
                    Break
                End ! If aud:Type = 'JOB'
                If aud:Action <> 'WARRANTY CLAIM RESUBMITTED'
                    Break
                End ! If aud:Action <> 'WARRANTY CLAIM REJECTED'
                Count# += 1
            End ! Loop
            locSubmissions = Count#
        OF 'AAJ'
            Access:AUDIT.Clearkey(aud:TypeActionKey)
            aud:Ref_Number = jow:RefNumber
            aud:Type = 'JOB'
            aud:Action = 'WARRANTY CLAIM REJECTION ACKNOWLEDGED'
            Set(aud:TypeActionKey,aud:TypeActionKey)
            Loop ! Begin Loop
                If Access:AUDIT.Next()
                    Break
                End ! If Access:AUDIT.Next()
                If aud:Ref_Number <> jow:RefNumber
                    Break
                End ! If aud:Ref_Number <> jow:RefNumber
                If aud:Type <> 'JOB'
                    Break
                End ! If aud:Type = 'JOB'
                If aud:Action <> 'WARRANTY CLAIM REJECTION ACKNOWLEDGED'
                    Break
                End ! If aud:Action <> 'WARRANTY CLAIM REJECTED'
                Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
                aud2:AUDRecordNumber = aud:Record_Number
                IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)

                END ! IF
                
                locRejectionReason =  Clip(Sub(aud2:Notes,9,60))
                if (jow:DateFinalRejection = 0)
                    jow:DateFinalRejection = aud:Date
                end
                break
            End ! Loop
        OF 'REJ'
            Access:AUDIT.Clearkey(aud:TypeActionKey)
            aud:Ref_Number = jow:RefNumber
            aud:Type = 'JOB'
            aud:Action = 'WARRANTY CLAIM FINAL REJECTION'
            Set(aud:TypeActionKey,aud:TypeActionKey)
            Loop ! Begin Loop
                If Access:AUDIT.Next()
                    Break
                End ! If Access:AUDIT.Next()
                If aud:Ref_Number <> jow:RefNumber
                    Break
                End ! If aud:Ref_Number <> jow:RefNumber
                If aud:Type <> 'JOB'
                    Break
                End ! If aud:Type = 'JOB'
                If aud:Action <> 'WARRANTY CLAIM FINAL REJECTION'
                    Break
                End ! If aud:Action <> 'WARRANTY CLAIM REJECTED'
                Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
                aud2:AUDRecordNumber = aud:Record_Number
                IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)

                END ! IF
                locRejectionReason = Clip(Sub(aud2:Notes,9,60))
                if (jow:DateFinalRejection = 0)
                    jow:DateFinalRejection = aud:Date
                end
                break
            End ! Loop 
        END !CASE
