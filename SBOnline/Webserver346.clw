

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER346.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER019.INC'),ONCE        !Req'd for module callout resolution
                     END


FormRRCAccounts      PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
                    MAP
CheckOverrideVatNumber  PROCEDURE()
                    END ! MAP
FilesOpened     Long
TRADEACC::State  USHORT
COURIER::State  USHORT
SUBURB::State  USHORT
SUBTRACC::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormRRCAccounts')
  loc:formname = 'FormRRCAccounts_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormRRCAccounts','')
    p_web._DivHeader('FormRRCAccounts',clip('fdiv') & ' ' & clip('FormContent'))
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormRRCAccounts',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of InsertRecord + NET:WEB:StagePost
    do RestoreMem
    do PostInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormRRCAccounts',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:StagePost
    do RestoreMem
    do PostCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormRRCAccounts',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormRRCAccounts',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    If loc:act = InsertRecord
      do PostInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do PostUpdate
    End

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormRRCAccounts',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of DeleteRecord + NET:WEB:StagePost
    do RestoreMem
    do PostDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormRRCAccounts',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(COURIER)
  p_web._OpenFile(SUBURB)
  p_web._OpenFile(SUBTRACC)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(SUBURB)
  p_Web._CloseFile(SUBTRACC)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormRRCAccounts_form:inited_',1)
  p_web.SetValue('UpdateFile','SUBTRACC')
  p_web.SetValue('UpdateKey','sub:RecordNumberKey')
  p_web.SetValue('IDField','sub:RecordNumber')
  do RestoreMem

CancelForm  Routine
  IF p_web.GetSessionValue('FormRRCAccounts:Primed') = 1
    p_web._deleteFile(SUBTRACC)
    p_web.SetSessionValue('FormRRCAccounts:Primed',0)
  End

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  p_web.SetValue('UpdateFile','SUBTRACC')
  p_web.SetValue('UpdateKey','sub:RecordNumberKey')
  If p_web.IfExistsValue('sub:Main_Account_Number')
    p_web.SetPicture('sub:Main_Account_Number','@s15')
  End
  p_web.SetSessionPicture('sub:Main_Account_Number','@s15')
  If p_web.IfExistsValue('sub:Account_Number')
    p_web.SetPicture('sub:Account_Number','@s15')
  End
  p_web.SetSessionPicture('sub:Account_Number','@s15')
  If p_web.IfExistsValue('sub:Company_Name')
    p_web.SetPicture('sub:Company_Name','@s30')
  End
  p_web.SetSessionPicture('sub:Company_Name','@s30')
  If p_web.IfExistsValue('sub:Branch')
    p_web.SetPicture('sub:Branch','@s30')
  End
  p_web.SetSessionPicture('sub:Branch','@s30')
  If p_web.IfExistsValue('sub:Address_Line1')
    p_web.SetPicture('sub:Address_Line1','@s30')
  End
  p_web.SetSessionPicture('sub:Address_Line1','@s30')
  If p_web.IfExistsValue('sub:Address_Line2')
    p_web.SetPicture('sub:Address_Line2','@s30')
  End
  p_web.SetSessionPicture('sub:Address_Line2','@s30')
  If p_web.IfExistsValue('sub:Address_Line3')
    p_web.SetPicture('sub:Address_Line3','@s30')
  End
  p_web.SetSessionPicture('sub:Address_Line3','@s30')
  If p_web.IfExistsValue('sub:Postcode')
    p_web.SetPicture('sub:Postcode','@s15')
  End
  p_web.SetSessionPicture('sub:Postcode','@s15')
  If p_web.IfExistsValue('sub:Hub')
    p_web.SetPicture('sub:Hub','@s30')
  End
  p_web.SetSessionPicture('sub:Hub','@s30')
  If p_web.IfExistsValue('sub:Telephone_Number')
    p_web.SetPicture('sub:Telephone_Number','@s15')
  End
  p_web.SetSessionPicture('sub:Telephone_Number','@s15')
  If p_web.IfExistsValue('sub:Fax_Number')
    p_web.SetPicture('sub:Fax_Number','@s15')
  End
  p_web.SetSessionPicture('sub:Fax_Number','@s15')
  If p_web.IfExistsValue('sub:EmailAddress')
    p_web.SetPicture('sub:EmailAddress','@s255')
  End
  p_web.SetSessionPicture('sub:EmailAddress','@s255')
  If p_web.IfExistsValue('sub:Contact_Name')
    p_web.SetPicture('sub:Contact_Name','@s30')
  End
  p_web.SetSessionPicture('sub:Contact_Name','@s30')
  If p_web.IfExistsValue('sub:Courier_Outgoing')
    p_web.SetPicture('sub:Courier_Outgoing','@s30')
  End
  p_web.SetSessionPicture('sub:Courier_Outgoing','@s30')
  If p_web.IfExistsValue('sub:EstimateIfOver')
    p_web.SetPicture('sub:EstimateIfOver','@n14.2')
  End
  p_web.SetSessionPicture('sub:EstimateIfOver','@n14.2')
  If p_web.IfExistsValue('sub:FinanceContactName')
    p_web.SetPicture('sub:FinanceContactName','@s30')
  End
  p_web.SetSessionPicture('sub:FinanceContactName','@s30')
  If p_web.IfExistsValue('sub:FinanceTelephoneNo')
    p_web.SetPicture('sub:FinanceTelephoneNo','@s30')
  End
  p_web.SetSessionPicture('sub:FinanceTelephoneNo','@s30')
  If p_web.IfExistsValue('sub:FinanceEmailAddress')
    p_web.SetPicture('sub:FinanceEmailAddress','@s255')
  End
  p_web.SetSessionPicture('sub:FinanceEmailAddress','@s255')
  If p_web.IfExistsValue('sub:FinanceAddress1')
    p_web.SetPicture('sub:FinanceAddress1','@s30')
  End
  p_web.SetSessionPicture('sub:FinanceAddress1','@s30')
  If p_web.IfExistsValue('sub:FinanceAddress2')
    p_web.SetPicture('sub:FinanceAddress2','@s30')
  End
  p_web.SetSessionPicture('sub:FinanceAddress2','@s30')
  If p_web.IfExistsValue('sub:FinanceAddress3')
    p_web.SetPicture('sub:FinanceAddress3','@s30')
  End
  p_web.SetSessionPicture('sub:FinanceAddress3','@s30')
  If p_web.IfExistsValue('sub:FinancePostcode')
    p_web.SetPicture('sub:FinancePostcode','@s30')
  End
  p_web.SetSessionPicture('sub:FinancePostcode','@s30')
  If p_web.IfExistsValue('sub:AccountLimit')
    p_web.SetPicture('sub:AccountLimit','@n14.2')
  End
  p_web.SetSessionPicture('sub:AccountLimit','@n14.2')
  If p_web.IfExistsValue('sub:VAT_Number')
    p_web.SetPicture('sub:VAT_Number','@s30')
  End
  p_web.SetSessionPicture('sub:VAT_Number','@s30')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'sub:Address_Line3'
    p_web.setsessionvalue('showtab_FormRRCAccounts',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(SUBURB)
        p_web.setsessionvalue('sub:Postcode',sur:Postcode)
        p_web.setsessionvalue('sub:Hub',sur:Hub)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.sub:Postcode')
  Of 'sub:Courier_Outgoing'
    p_web.setsessionvalue('showtab_FormRRCAccounts',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(COURIER)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.sub:UseAlternativeAdd')
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=File

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormRRCAccounts_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    CheckOverrideVatNumber()
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferFormRRCAccounts')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormRRCAccounts_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormRRCAccounts_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormRRCAccounts_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  packet = clip(packet) & '<input type="hidden" name="SUBTRACC__FileAction" value="'&p_web.getSessionValue('SUBTRACC:FileAction')&'" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="file" value="SUBTRACC" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateFile" value="SUBTRACC" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateKey" value="sub:RecordNumberKey" ></input><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormRRCAccounts" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormRRCAccounts" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormRRCAccounts" ></input><13,10>'
  end

  do SendPacket
  packet = clip(packet) & p_web.CreateInput('hidden','sub:RecordNumber',p_web._jsok(p_web.getSessionValue('sub:RecordNumber'))) & '<13,10>'
  If p_web.Translate('Edit Trade Account') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Edit Trade Account',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormRRCAccounts">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormRRCAccounts" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormRRCAccounts')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('General') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Finance Details') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormRRCAccounts')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormRRCAccounts'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='SUBURB'
            p_web.SetValue('SelectField',clip(loc:formname) & '.sub:Telephone_Number')
    End
    If upper(p_web.getvalue('LookupFile'))='COURIER'
            p_web.SetValue('SelectField',clip(loc:formname) & '.sub:EstimateIfOver')
    End
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.sub:Account_Number')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormRRCAccounts')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('General') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormRRCAccounts_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sub:Main_Account_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sub:Main_Account_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sub:Main_Account_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sub:Account_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sub:Account_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sub:Account_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sub:Company_Name
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sub:Company_Name
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sub:Company_Name
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::hidden
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::hidden
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sub:Branch
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sub:Branch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sub:Branch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sub:Address_Line1
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sub:Address_Line1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sub:Address_Line1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sub:Address_Line2
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sub:Address_Line2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sub:Address_Line2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sub:Address_Line3
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sub:Address_Line3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sub:Address_Line3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sub:Postcode
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sub:Postcode
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sub:Postcode
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sub:Hub
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sub:Hub
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sub:Hub
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sub:Telephone_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sub:Telephone_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sub:Telephone_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sub:Fax_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sub:Fax_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sub:Fax_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sub:EmailAddress
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sub:EmailAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sub:EmailAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sub:Contact_Name
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sub:Contact_Name
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sub:Contact_Name
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sub:Courier_Outgoing
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sub:Courier_Outgoing
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sub:Courier_Outgoing
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sub:UseAlternativeAdd
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sub:UseAlternativeAdd
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sub:UseAlternativeAdd
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sub:ForceOrderNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sub:ForceOrderNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sub:ForceOrderNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sub:ForceEstimate
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sub:ForceEstimate
      do Comment::sub:ForceEstimate
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sub:EstimateIfOver
      do Value::sub:EstimateIfOver
      do Comment::sub:EstimateIfOver
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sub:Stop_Account
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sub:Stop_Account
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sub:Stop_Account
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sub:ForceEndUserName
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sub:ForceEndUserName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sub:ForceEndUserName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Finance Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormRRCAccounts_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Finance Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Finance Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Finance Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Finance Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sub:FinanceContactName
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sub:FinanceContactName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sub:FinanceContactName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::hidden2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::hidden2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sub:FinanceTelephoneNo
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sub:FinanceTelephoneNo
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sub:FinanceTelephoneNo
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sub:FinanceEmailAddress
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sub:FinanceEmailAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sub:FinanceEmailAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sub:FinanceAddress1
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sub:FinanceAddress1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sub:FinanceAddress1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sub:FinanceAddress2
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sub:FinanceAddress2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sub:FinanceAddress2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sub:FinanceAddress3
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sub:FinanceAddress3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sub:FinanceAddress3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sub:FinancePostcode
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sub:FinancePostcode
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sub:FinancePostcode
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sub:AccountLimit
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sub:AccountLimit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sub:AccountLimit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sub:OverrideHeadVATNo
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sub:OverrideHeadVATNo
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sub:OverrideHeadVATNo
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sub:VAT_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sub:VAT_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sub:VAT_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sub:OverrideHeadMobile
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sub:OverrideHeadMobile
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sub:OverrideHeadMobile
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sub:OverrideMobileDefault
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sub:OverrideMobileDefault
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sub:OverrideMobileDefault
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::sub:Main_Account_Number  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Main_Account_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Main Account Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sub:Main_Account_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sub:Main_Account_Number',p_web.GetValue('NewValue'))
    sub:Main_Account_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = sub:Main_Account_Number
    do Value::sub:Main_Account_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sub:Main_Account_Number',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    sub:Main_Account_Number = p_web.GetValue('Value')
  End
  do Value::sub:Main_Account_Number
  do SendAlert

Value::sub:Main_Account_Number  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Main_Account_Number') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sub:Main_Account_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('sub:Main_Account_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''sub:Main_Account_Number'',''formrrcaccounts_sub:main_account_number_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','sub:Main_Account_Number',p_web.GetSessionValueFormat('sub:Main_Account_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:Main_Account_Number') & '_value')

Comment::sub:Main_Account_Number  Routine
      loc:comment = ''
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Main_Account_Number') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sub:Account_Number  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Account_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Account Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sub:Account_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sub:Account_Number',p_web.GetValue('NewValue'))
    sub:Account_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = sub:Account_Number
    do Value::sub:Account_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sub:Account_Number',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    sub:Account_Number = p_web.GetValue('Value')
  End
  If sub:Account_Number = ''
    loc:Invalid = 'sub:Account_Number'
    loc:alert = p_web.translate('Account Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    sub:Account_Number = Upper(sub:Account_Number)
    p_web.SetSessionValue('sub:Account_Number',sub:Account_Number)
  do Value::sub:Account_Number
  do SendAlert

Value::sub:Account_Number  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Account_Number') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sub:Account_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('sub:Account_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If sub:Account_Number = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''sub:Account_Number'',''formrrcaccounts_sub:account_number_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','sub:Account_Number',p_web.GetSessionValueFormat('sub:Account_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:Account_Number') & '_value')

Comment::sub:Account_Number  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Account_Number') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sub:Company_Name  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Company_Name') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Company Name')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sub:Company_Name  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sub:Company_Name',p_web.GetValue('NewValue'))
    sub:Company_Name = p_web.GetValue('NewValue') !FieldType= STRING Field = sub:Company_Name
    do Value::sub:Company_Name
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sub:Company_Name',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sub:Company_Name = p_web.GetValue('Value')
  End
    sub:Company_Name = Upper(sub:Company_Name)
    p_web.SetSessionValue('sub:Company_Name',sub:Company_Name)
  do Value::sub:Company_Name
  do SendAlert

Value::sub:Company_Name  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Company_Name') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sub:Company_Name
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('sub:Company_Name')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''sub:Company_Name'',''formrrcaccounts_sub:company_name_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','sub:Company_Name',p_web.GetSessionValueFormat('sub:Company_Name'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:Company_Name') & '_value')

Comment::sub:Company_Name  Routine
      loc:comment = ''
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Company_Name') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::hidden  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('hidden',p_web.GetValue('NewValue'))
    do Value::hidden
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::hidden  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('hidden') & '_value','adiv')
  loc:extra = ''
  ! --- HIDDEN --- 
    packet = clip(packet) & p_web.CreateInput('hidden','',p_web.GetSessionValue('')) & '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::hidden  Routine
    loc:comment = ''
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('hidden') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sub:Branch  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Branch') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Branch')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sub:Branch  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sub:Branch',p_web.GetValue('NewValue'))
    sub:Branch = p_web.GetValue('NewValue') !FieldType= STRING Field = sub:Branch
    do Value::sub:Branch
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sub:Branch',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sub:Branch = p_web.GetValue('Value')
  End
    sub:Branch = Upper(sub:Branch)
    p_web.SetSessionValue('sub:Branch',sub:Branch)
  do Value::sub:Branch
  do SendAlert

Value::sub:Branch  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Branch') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sub:Branch
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('sub:Branch')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''sub:Branch'',''formrrcaccounts_sub:branch_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','sub:Branch',p_web.GetSessionValueFormat('sub:Branch'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:Branch') & '_value')

Comment::sub:Branch  Routine
      loc:comment = ''
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Branch') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sub:Address_Line1  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Address_Line1') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Address')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sub:Address_Line1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sub:Address_Line1',p_web.GetValue('NewValue'))
    sub:Address_Line1 = p_web.GetValue('NewValue') !FieldType= STRING Field = sub:Address_Line1
    do Value::sub:Address_Line1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sub:Address_Line1',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sub:Address_Line1 = p_web.GetValue('Value')
  End
    sub:Address_Line1 = Upper(sub:Address_Line1)
    p_web.SetSessionValue('sub:Address_Line1',sub:Address_Line1)
  do Value::sub:Address_Line1
  do SendAlert

Value::sub:Address_Line1  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Address_Line1') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sub:Address_Line1
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('sub:Address_Line1')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''sub:Address_Line1'',''formrrcaccounts_sub:address_line1_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','sub:Address_Line1',p_web.GetSessionValueFormat('sub:Address_Line1'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:Address_Line1') & '_value')

Comment::sub:Address_Line1  Routine
      loc:comment = ''
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Address_Line1') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sub:Address_Line2  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Address_Line2') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sub:Address_Line2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sub:Address_Line2',p_web.GetValue('NewValue'))
    sub:Address_Line2 = p_web.GetValue('NewValue') !FieldType= STRING Field = sub:Address_Line2
    do Value::sub:Address_Line2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sub:Address_Line2',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sub:Address_Line2 = p_web.GetValue('Value')
  End
    sub:Address_Line2 = Upper(sub:Address_Line2)
    p_web.SetSessionValue('sub:Address_Line2',sub:Address_Line2)
  do Value::sub:Address_Line2
  do SendAlert

Value::sub:Address_Line2  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Address_Line2') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sub:Address_Line2
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('sub:Address_Line2')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''sub:Address_Line2'',''formrrcaccounts_sub:address_line2_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','sub:Address_Line2',p_web.GetSessionValueFormat('sub:Address_Line2'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:Address_Line2') & '_value')

Comment::sub:Address_Line2  Routine
      loc:comment = ''
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Address_Line2') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sub:Address_Line3  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Address_Line3') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Suburb')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sub:Address_Line3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sub:Address_Line3',p_web.GetValue('NewValue'))
    sub:Address_Line3 = p_web.GetValue('NewValue') !FieldType= STRING Field = sub:Address_Line3
    do Value::sub:Address_Line3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sub:Address_Line3',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sub:Address_Line3 = p_web.GetValue('Value')
  End
    sub:Address_Line3 = Upper(sub:Address_Line3)
    p_web.SetSessionValue('sub:Address_Line3',sub:Address_Line3)
  p_Web.SetValue('lookupfield','sub:Address_Line3')
  do AfterLookup
  do Value::sub:Postcode
  do Value::sub:Hub
  do Value::sub:Address_Line3
  do SendAlert
  do Comment::sub:Address_Line3

Value::sub:Address_Line3  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Address_Line3') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sub:Address_Line3
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('sub:Address_Line3')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''sub:Address_Line3'',''formrrcaccounts_sub:address_line3_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('sub:Address_Line3')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','sub:Address_Line3',p_web.GetSessionValue('sub:Address_Line3'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('LookupSuburbs?LookupField=sub:Address_Line3&Tab=1&ForeignField=sur:Suburb&_sort=&Refresh=sort&LookupFrom=FormRRCAccounts&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:Address_Line3') & '_value')

Comment::sub:Address_Line3  Routine
    loc:comment = p_web.Translate('')
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Address_Line3') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:Address_Line3') & '_comment')

Prompt::sub:Postcode  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Postcode') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Postcode')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:Postcode') & '_prompt')

Validate::sub:Postcode  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sub:Postcode',p_web.GetValue('NewValue'))
    sub:Postcode = p_web.GetValue('NewValue') !FieldType= STRING Field = sub:Postcode
    do Value::sub:Postcode
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sub:Postcode',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    sub:Postcode = p_web.GetValue('Value')
  End
  do Value::sub:Postcode
  do SendAlert

Value::sub:Postcode  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Postcode') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sub:Postcode
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('sub:Postcode')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''sub:Postcode'',''formrrcaccounts_sub:postcode_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','sub:Postcode',p_web.GetSessionValueFormat('sub:Postcode'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:Postcode') & '_value')

Comment::sub:Postcode  Routine
      loc:comment = ''
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Postcode') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:Postcode') & '_comment')

Prompt::sub:Hub  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Hub') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Hub')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:Hub') & '_prompt')

Validate::sub:Hub  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sub:Hub',p_web.GetValue('NewValue'))
    sub:Hub = p_web.GetValue('NewValue') !FieldType= STRING Field = sub:Hub
    do Value::sub:Hub
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sub:Hub',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sub:Hub = p_web.GetValue('Value')
  End
  do Value::sub:Hub
  do SendAlert

Value::sub:Hub  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Hub') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sub:Hub
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('sub:Hub')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''sub:Hub'',''formrrcaccounts_sub:hub_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','sub:Hub',p_web.GetSessionValueFormat('sub:Hub'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Hub') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:Hub') & '_value')

Comment::sub:Hub  Routine
      loc:comment = ''
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Hub') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:Hub') & '_comment')

Prompt::sub:Telephone_Number  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Telephone_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Telephone Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sub:Telephone_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sub:Telephone_Number',p_web.GetValue('NewValue'))
    sub:Telephone_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = sub:Telephone_Number
    do Value::sub:Telephone_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sub:Telephone_Number',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    sub:Telephone_Number = p_web.GetValue('Value')
  End
    sub:Telephone_Number = Upper(sub:Telephone_Number)
    p_web.SetSessionValue('sub:Telephone_Number',sub:Telephone_Number)
  do Value::sub:Telephone_Number
  do SendAlert

Value::sub:Telephone_Number  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Telephone_Number') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sub:Telephone_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('sub:Telephone_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''sub:Telephone_Number'',''formrrcaccounts_sub:telephone_number_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','sub:Telephone_Number',p_web.GetSessionValueFormat('sub:Telephone_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:Telephone_Number') & '_value')

Comment::sub:Telephone_Number  Routine
      loc:comment = ''
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Telephone_Number') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sub:Fax_Number  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Fax_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Fax Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sub:Fax_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sub:Fax_Number',p_web.GetValue('NewValue'))
    sub:Fax_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = sub:Fax_Number
    do Value::sub:Fax_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sub:Fax_Number',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    sub:Fax_Number = p_web.GetValue('Value')
  End
    sub:Fax_Number = Upper(sub:Fax_Number)
    p_web.SetSessionValue('sub:Fax_Number',sub:Fax_Number)
  do Value::sub:Fax_Number
  do SendAlert

Value::sub:Fax_Number  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Fax_Number') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sub:Fax_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('sub:Fax_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''sub:Fax_Number'',''formrrcaccounts_sub:fax_number_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','sub:Fax_Number',p_web.GetSessionValueFormat('sub:Fax_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:Fax_Number') & '_value')

Comment::sub:Fax_Number  Routine
      loc:comment = ''
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Fax_Number') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sub:EmailAddress  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:EmailAddress') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Email Address')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sub:EmailAddress  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sub:EmailAddress',p_web.GetValue('NewValue'))
    sub:EmailAddress = p_web.GetValue('NewValue') !FieldType= STRING Field = sub:EmailAddress
    do Value::sub:EmailAddress
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sub:EmailAddress',p_web.dFormat(p_web.GetValue('Value'),'@s255'))
    sub:EmailAddress = p_web.GetValue('Value')
  End
  do Value::sub:EmailAddress
  do SendAlert

Value::sub:EmailAddress  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:EmailAddress') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sub:EmailAddress
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('sub:EmailAddress')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''sub:EmailAddress'',''formrrcaccounts_sub:emailaddress_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','sub:EmailAddress',p_web.GetSessionValueFormat('sub:EmailAddress'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s255'),'Email Address') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:EmailAddress') & '_value')

Comment::sub:EmailAddress  Routine
      loc:comment = ''
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:EmailAddress') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sub:Contact_Name  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Contact_Name') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Contact Name')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sub:Contact_Name  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sub:Contact_Name',p_web.GetValue('NewValue'))
    sub:Contact_Name = p_web.GetValue('NewValue') !FieldType= STRING Field = sub:Contact_Name
    do Value::sub:Contact_Name
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sub:Contact_Name',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sub:Contact_Name = p_web.GetValue('Value')
  End
    sub:Contact_Name = Upper(sub:Contact_Name)
    p_web.SetSessionValue('sub:Contact_Name',sub:Contact_Name)
  do Value::sub:Contact_Name
  do SendAlert

Value::sub:Contact_Name  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Contact_Name') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sub:Contact_Name
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('sub:Contact_Name')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''sub:Contact_Name'',''formrrcaccounts_sub:contact_name_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','sub:Contact_Name',p_web.GetSessionValueFormat('sub:Contact_Name'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:Contact_Name') & '_value')

Comment::sub:Contact_Name  Routine
      loc:comment = ''
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Contact_Name') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sub:Courier_Outgoing  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Courier_Outgoing') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Courier - Outgoing')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sub:Courier_Outgoing  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sub:Courier_Outgoing',p_web.GetValue('NewValue'))
    sub:Courier_Outgoing = p_web.GetValue('NewValue') !FieldType= STRING Field = sub:Courier_Outgoing
    do Value::sub:Courier_Outgoing
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sub:Courier_Outgoing',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sub:Courier_Outgoing = p_web.GetValue('Value')
  End
    sub:Courier_Outgoing = Upper(sub:Courier_Outgoing)
    p_web.SetSessionValue('sub:Courier_Outgoing',sub:Courier_Outgoing)
  p_Web.SetValue('lookupfield','sub:Courier_Outgoing')
  do AfterLookup
  do Value::sub:Courier_Outgoing
  do SendAlert
  do Comment::sub:Courier_Outgoing

Value::sub:Courier_Outgoing  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Courier_Outgoing') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sub:Courier_Outgoing
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('sub:Courier_Outgoing')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''sub:Courier_Outgoing'',''formrrcaccounts_sub:courier_outgoing_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('sub:Courier_Outgoing')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','sub:Courier_Outgoing',p_web.GetSessionValue('sub:Courier_Outgoing'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('SelectCouriers?LookupField=sub:Courier_Outgoing&Tab=1&ForeignField=cou:Courier&_sort=&Refresh=sort&LookupFrom=FormRRCAccounts&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:Courier_Outgoing') & '_value')

Comment::sub:Courier_Outgoing  Routine
    loc:comment = p_web.Translate('')
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Courier_Outgoing') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:Courier_Outgoing') & '_comment')

Prompt::sub:UseAlternativeAdd  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:UseAlternativeAdd') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Use Alternative Delivery Addresses')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sub:UseAlternativeAdd  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sub:UseAlternativeAdd',p_web.GetValue('NewValue'))
    sub:UseAlternativeAdd = p_web.GetValue('NewValue') !FieldType= BYTE Field = sub:UseAlternativeAdd
    do Value::sub:UseAlternativeAdd
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('sub:UseAlternativeAdd',p_web.GetValue('Value'))
    sub:UseAlternativeAdd = p_web.GetValue('Value')
  End
  do Value::sub:UseAlternativeAdd
  do SendAlert

Value::sub:UseAlternativeAdd  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:UseAlternativeAdd') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- sub:UseAlternativeAdd
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''sub:UseAlternativeAdd'',''formrrcaccounts_sub:usealternativeadd_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('sub:UseAlternativeAdd') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','sub:UseAlternativeAdd',clip(1),,loc:readonly,,,loc:javascript,,'Use Alternative Delivery Addresses') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:UseAlternativeAdd') & '_value')

Comment::sub:UseAlternativeAdd  Routine
    loc:comment = ''
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:UseAlternativeAdd') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sub:ForceOrderNumber  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:ForceOrderNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Force Order Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sub:ForceOrderNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sub:ForceOrderNumber',p_web.GetValue('NewValue'))
    sub:ForceOrderNumber = p_web.GetValue('NewValue') !FieldType= BYTE Field = sub:ForceOrderNumber
    do Value::sub:ForceOrderNumber
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('sub:ForceOrderNumber',p_web.GetValue('Value'))
    sub:ForceOrderNumber = p_web.GetValue('Value')
  End
  do Value::sub:ForceOrderNumber
  do SendAlert

Value::sub:ForceOrderNumber  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:ForceOrderNumber') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- sub:ForceOrderNumber
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''sub:ForceOrderNumber'',''formrrcaccounts_sub:forceordernumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('sub:ForceOrderNumber') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','sub:ForceOrderNumber',clip(1),,loc:readonly,,,loc:javascript,,'Force Order Number') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:ForceOrderNumber') & '_value')

Comment::sub:ForceOrderNumber  Routine
    loc:comment = ''
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:ForceOrderNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sub:ForceEstimate  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:ForceEstimate') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Force Estimate')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sub:ForceEstimate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sub:ForceEstimate',p_web.GetValue('NewValue'))
    sub:ForceEstimate = p_web.GetValue('NewValue') !FieldType= BYTE Field = sub:ForceEstimate
    do Value::sub:ForceEstimate
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('sub:ForceEstimate',p_web.GetValue('Value'))
    sub:ForceEstimate = p_web.GetValue('Value')
  End
  do Value::sub:ForceEstimate
  do SendAlert

Value::sub:ForceEstimate  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:ForceEstimate') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- sub:ForceEstimate
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''sub:ForceEstimate'',''formrrcaccounts_sub:forceestimate_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('sub:ForceEstimate') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','sub:ForceEstimate',clip(1),,loc:readonly,,,loc:javascript,,'Force Estimate') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:ForceEstimate') & '_value')

Comment::sub:ForceEstimate  Routine
    loc:comment = ''
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:ForceEstimate') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sub:EstimateIfOver  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:EstimateIfOver') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Estimate If Over')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sub:EstimateIfOver  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sub:EstimateIfOver',p_web.GetValue('NewValue'))
    sub:EstimateIfOver = p_web.GetValue('NewValue') !FieldType= REAL Field = sub:EstimateIfOver
    do Value::sub:EstimateIfOver
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sub:EstimateIfOver',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    sub:EstimateIfOver = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
    sub:EstimateIfOver = Upper(sub:EstimateIfOver)
    p_web.SetSessionValue('sub:EstimateIfOver',sub:EstimateIfOver)
  do Value::sub:EstimateIfOver
  do SendAlert

Value::sub:EstimateIfOver  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:EstimateIfOver') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sub:EstimateIfOver
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('sub:EstimateIfOver')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''sub:EstimateIfOver'',''formrrcaccounts_sub:estimateifover_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','sub:EstimateIfOver',p_web.GetSessionValue('sub:EstimateIfOver'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,'Estimate If Over') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:EstimateIfOver') & '_value')

Comment::sub:EstimateIfOver  Routine
      loc:comment = ''
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:EstimateIfOver') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sub:Stop_Account  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Stop_Account') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Stop Account')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sub:Stop_Account  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sub:Stop_Account',p_web.GetValue('NewValue'))
    sub:Stop_Account = p_web.GetValue('NewValue') !FieldType= STRING Field = sub:Stop_Account
    do Value::sub:Stop_Account
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value','NO')
    end
    p_web.SetSessionValue('sub:Stop_Account',p_web.GetValue('Value'))
    sub:Stop_Account = p_web.GetValue('Value')
  End
  ! Automatic Dictionary Validation
    If InList(clip(sub:Stop_Account) ,'NO','YES' ) = 0
      loc:Invalid = 'sub:Stop_Account'
      loc:Alert = clip('Stop Account') & ' ' & clip(p_web.site.InListText) & p_web._jsok('  NO, YES')
      !exit
    End
  do Value::sub:Stop_Account
  do SendAlert

Value::sub:Stop_Account  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Stop_Account') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- sub:Stop_Account
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''sub:Stop_Account'',''formrrcaccounts_sub:stop_account_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('sub:Stop_Account') = 'YES'
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','sub:Stop_Account',clip('YES'),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:Stop_Account') & '_value')

Comment::sub:Stop_Account  Routine
    loc:comment = ''
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:Stop_Account') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sub:ForceEndUserName  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:ForceEndUserName') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Force End User Name')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sub:ForceEndUserName  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sub:ForceEndUserName',p_web.GetValue('NewValue'))
    sub:ForceEndUserName = p_web.GetValue('NewValue') !FieldType= BYTE Field = sub:ForceEndUserName
    do Value::sub:ForceEndUserName
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('sub:ForceEndUserName',p_web.GetValue('Value'))
    sub:ForceEndUserName = p_web.GetValue('Value')
  End
  do Value::sub:ForceEndUserName
  do SendAlert

Value::sub:ForceEndUserName  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:ForceEndUserName') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- sub:ForceEndUserName
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''sub:ForceEndUserName'',''formrrcaccounts_sub:forceendusername_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('sub:ForceEndUserName') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','sub:ForceEndUserName',clip(1),,loc:readonly,,,loc:javascript,,'Force End User Name') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:ForceEndUserName') & '_value')

Comment::sub:ForceEndUserName  Routine
    loc:comment = ''
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:ForceEndUserName') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sub:FinanceContactName  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:FinanceContactName') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Contact Name')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sub:FinanceContactName  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sub:FinanceContactName',p_web.GetValue('NewValue'))
    sub:FinanceContactName = p_web.GetValue('NewValue') !FieldType= STRING Field = sub:FinanceContactName
    do Value::sub:FinanceContactName
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sub:FinanceContactName',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sub:FinanceContactName = p_web.GetValue('Value')
  End
    sub:FinanceContactName = Upper(sub:FinanceContactName)
    p_web.SetSessionValue('sub:FinanceContactName',sub:FinanceContactName)
  do Value::sub:FinanceContactName
  do SendAlert

Value::sub:FinanceContactName  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:FinanceContactName') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sub:FinanceContactName
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('sub:FinanceContactName')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''sub:FinanceContactName'',''formrrcaccounts_sub:financecontactname_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','sub:FinanceContactName',p_web.GetSessionValueFormat('sub:FinanceContactName'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Contact Name') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:FinanceContactName') & '_value')

Comment::sub:FinanceContactName  Routine
      loc:comment = ''
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:FinanceContactName') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::hidden2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('hidden2',p_web.GetValue('NewValue'))
    do Value::hidden2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::hidden2  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('hidden2') & '_value','adiv')
  loc:extra = ''
  ! --- HIDDEN --- 
    packet = clip(packet) & p_web.CreateInput('hidden','',p_web.GetSessionValue('')) & '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::hidden2  Routine
    loc:comment = ''
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('hidden2') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sub:FinanceTelephoneNo  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:FinanceTelephoneNo') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Contact Tel Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sub:FinanceTelephoneNo  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sub:FinanceTelephoneNo',p_web.GetValue('NewValue'))
    sub:FinanceTelephoneNo = p_web.GetValue('NewValue') !FieldType= STRING Field = sub:FinanceTelephoneNo
    do Value::sub:FinanceTelephoneNo
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sub:FinanceTelephoneNo',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sub:FinanceTelephoneNo = p_web.GetValue('Value')
  End
    sub:FinanceTelephoneNo = Upper(sub:FinanceTelephoneNo)
    p_web.SetSessionValue('sub:FinanceTelephoneNo',sub:FinanceTelephoneNo)
  do Value::sub:FinanceTelephoneNo
  do SendAlert

Value::sub:FinanceTelephoneNo  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:FinanceTelephoneNo') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sub:FinanceTelephoneNo
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('sub:FinanceTelephoneNo')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''sub:FinanceTelephoneNo'',''formrrcaccounts_sub:financetelephoneno_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','sub:FinanceTelephoneNo',p_web.GetSessionValueFormat('sub:FinanceTelephoneNo'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Contact Tel Number') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:FinanceTelephoneNo') & '_value')

Comment::sub:FinanceTelephoneNo  Routine
      loc:comment = ''
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:FinanceTelephoneNo') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sub:FinanceEmailAddress  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:FinanceEmailAddress') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Email Address')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sub:FinanceEmailAddress  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sub:FinanceEmailAddress',p_web.GetValue('NewValue'))
    sub:FinanceEmailAddress = p_web.GetValue('NewValue') !FieldType= STRING Field = sub:FinanceEmailAddress
    do Value::sub:FinanceEmailAddress
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sub:FinanceEmailAddress',p_web.dFormat(p_web.GetValue('Value'),'@s255'))
    sub:FinanceEmailAddress = p_web.GetValue('Value')
  End
  do Value::sub:FinanceEmailAddress
  do SendAlert

Value::sub:FinanceEmailAddress  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:FinanceEmailAddress') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sub:FinanceEmailAddress
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('sub:FinanceEmailAddress')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''sub:FinanceEmailAddress'',''formrrcaccounts_sub:financeemailaddress_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','sub:FinanceEmailAddress',p_web.GetSessionValueFormat('sub:FinanceEmailAddress'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s255'),'Finance Email Address') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:FinanceEmailAddress') & '_value')

Comment::sub:FinanceEmailAddress  Routine
      loc:comment = ''
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:FinanceEmailAddress') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sub:FinanceAddress1  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:FinanceAddress1') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Address')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sub:FinanceAddress1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sub:FinanceAddress1',p_web.GetValue('NewValue'))
    sub:FinanceAddress1 = p_web.GetValue('NewValue') !FieldType= STRING Field = sub:FinanceAddress1
    do Value::sub:FinanceAddress1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sub:FinanceAddress1',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sub:FinanceAddress1 = p_web.GetValue('Value')
  End
    sub:FinanceAddress1 = Upper(sub:FinanceAddress1)
    p_web.SetSessionValue('sub:FinanceAddress1',sub:FinanceAddress1)
  do Value::sub:FinanceAddress1
  do SendAlert

Value::sub:FinanceAddress1  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:FinanceAddress1') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sub:FinanceAddress1
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('sub:FinanceAddress1')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''sub:FinanceAddress1'',''formrrcaccounts_sub:financeaddress1_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','sub:FinanceAddress1',p_web.GetSessionValueFormat('sub:FinanceAddress1'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Address') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:FinanceAddress1') & '_value')

Comment::sub:FinanceAddress1  Routine
      loc:comment = ''
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:FinanceAddress1') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sub:FinanceAddress2  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:FinanceAddress2') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sub:FinanceAddress2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sub:FinanceAddress2',p_web.GetValue('NewValue'))
    sub:FinanceAddress2 = p_web.GetValue('NewValue') !FieldType= STRING Field = sub:FinanceAddress2
    do Value::sub:FinanceAddress2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sub:FinanceAddress2',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sub:FinanceAddress2 = p_web.GetValue('Value')
  End
    sub:FinanceAddress2 = Upper(sub:FinanceAddress2)
    p_web.SetSessionValue('sub:FinanceAddress2',sub:FinanceAddress2)
  do Value::sub:FinanceAddress2
  do SendAlert

Value::sub:FinanceAddress2  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:FinanceAddress2') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sub:FinanceAddress2
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('sub:FinanceAddress2')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''sub:FinanceAddress2'',''formrrcaccounts_sub:financeaddress2_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','sub:FinanceAddress2',p_web.GetSessionValueFormat('sub:FinanceAddress2'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Address') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:FinanceAddress2') & '_value')

Comment::sub:FinanceAddress2  Routine
      loc:comment = ''
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:FinanceAddress2') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sub:FinanceAddress3  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:FinanceAddress3') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Finance')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sub:FinanceAddress3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sub:FinanceAddress3',p_web.GetValue('NewValue'))
    sub:FinanceAddress3 = p_web.GetValue('NewValue') !FieldType= STRING Field = sub:FinanceAddress3
    do Value::sub:FinanceAddress3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sub:FinanceAddress3',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sub:FinanceAddress3 = p_web.GetValue('Value')
  End
    sub:FinanceAddress3 = Upper(sub:FinanceAddress3)
    p_web.SetSessionValue('sub:FinanceAddress3',sub:FinanceAddress3)
  do Value::sub:FinanceAddress3
  do SendAlert

Value::sub:FinanceAddress3  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:FinanceAddress3') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sub:FinanceAddress3
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('sub:FinanceAddress3')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''sub:FinanceAddress3'',''formrrcaccounts_sub:financeaddress3_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','sub:FinanceAddress3',p_web.GetSessionValueFormat('sub:FinanceAddress3'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Finance') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:FinanceAddress3') & '_value')

Comment::sub:FinanceAddress3  Routine
      loc:comment = ''
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:FinanceAddress3') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sub:FinancePostcode  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:FinancePostcode') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Postcode')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sub:FinancePostcode  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sub:FinancePostcode',p_web.GetValue('NewValue'))
    sub:FinancePostcode = p_web.GetValue('NewValue') !FieldType= STRING Field = sub:FinancePostcode
    do Value::sub:FinancePostcode
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sub:FinancePostcode',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sub:FinancePostcode = p_web.GetValue('Value')
  End
    sub:FinancePostcode = Upper(sub:FinancePostcode)
    p_web.SetSessionValue('sub:FinancePostcode',sub:FinancePostcode)
  do Value::sub:FinancePostcode
  do SendAlert

Value::sub:FinancePostcode  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:FinancePostcode') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sub:FinancePostcode
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('sub:FinancePostcode')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''sub:FinancePostcode'',''formrrcaccounts_sub:financepostcode_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','sub:FinancePostcode',p_web.GetSessionValueFormat('sub:FinancePostcode'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Postcode') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:FinancePostcode') & '_value')

Comment::sub:FinancePostcode  Routine
      loc:comment = ''
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:FinancePostcode') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sub:AccountLimit  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:AccountLimit') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Account Limit')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sub:AccountLimit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sub:AccountLimit',p_web.GetValue('NewValue'))
    sub:AccountLimit = p_web.GetValue('NewValue') !FieldType= REAL Field = sub:AccountLimit
    do Value::sub:AccountLimit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sub:AccountLimit',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    sub:AccountLimit = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::sub:AccountLimit
  do SendAlert

Value::sub:AccountLimit  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:AccountLimit') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sub:AccountLimit
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('sub:AccountLimit')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''sub:AccountLimit'',''formrrcaccounts_sub:accountlimit_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','sub:AccountLimit',p_web.GetSessionValue('sub:AccountLimit'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,'Account Limit') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:AccountLimit') & '_value')

Comment::sub:AccountLimit  Routine
      loc:comment = ''
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:AccountLimit') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sub:OverrideHeadVATNo  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:OverrideHeadVATNo') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Override Head Account VAT No')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sub:OverrideHeadVATNo  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sub:OverrideHeadVATNo',p_web.GetValue('NewValue'))
    sub:OverrideHeadVATNo = p_web.GetValue('NewValue') !FieldType= BYTE Field = sub:OverrideHeadVATNo
    do Value::sub:OverrideHeadVATNo
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('sub:OverrideHeadVATNo',p_web.GetValue('Value'))
    sub:OverrideHeadVATNo = p_web.GetValue('Value')
  End
    CheckOverrideVatNumber()  
  do Value::sub:OverrideHeadVATNo
  do SendAlert
  do Value::sub:VAT_Number  !1

Value::sub:OverrideHeadVATNo  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:OverrideHeadVATNo') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- sub:OverrideHeadVATNo
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''sub:OverrideHeadVATNo'',''formrrcaccounts_sub:overrideheadvatno_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('sub:OverrideHeadVATNo')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('sub:OverrideHeadVATNo') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','sub:OverrideHeadVATNo',clip(1),,loc:readonly,,,loc:javascript,,'Override Head Account V.A.T. No') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:OverrideHeadVATNo') & '_value')

Comment::sub:OverrideHeadVATNo  Routine
    loc:comment = ''
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:OverrideHeadVATNo') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sub:VAT_Number  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:VAT_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('V.A.T. Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sub:VAT_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sub:VAT_Number',p_web.GetValue('NewValue'))
    sub:VAT_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = sub:VAT_Number
    do Value::sub:VAT_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sub:VAT_Number',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sub:VAT_Number = p_web.GetValue('Value')
  End
    sub:VAT_Number = Upper(sub:VAT_Number)
    p_web.SetSessionValue('sub:VAT_Number',sub:VAT_Number)
  do Value::sub:VAT_Number
  do SendAlert

Value::sub:VAT_Number  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:VAT_Number') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sub:VAT_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('sub:OverrideHeadVATNo') = 0,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('sub:OverrideHeadVATNo') = 0
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('sub:VAT_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''sub:VAT_Number'',''formrrcaccounts_sub:vat_number_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','sub:VAT_Number',p_web.GetSessionValueFormat('sub:VAT_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:VAT_Number') & '_value')

Comment::sub:VAT_Number  Routine
      loc:comment = ''
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:VAT_Number') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sub:OverrideHeadMobile  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:OverrideHeadMobile') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Override Head Acc Mandatory Mobile Check')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sub:OverrideHeadMobile  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sub:OverrideHeadMobile',p_web.GetValue('NewValue'))
    sub:OverrideHeadMobile = p_web.GetValue('NewValue') !FieldType= BYTE Field = sub:OverrideHeadMobile
    do Value::sub:OverrideHeadMobile
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('sub:OverrideHeadMobile',p_web.GetValue('Value'))
    sub:OverrideHeadMobile = p_web.GetValue('Value')
  End
  do Value::sub:OverrideHeadMobile
  do SendAlert
  do Value::sub:OverrideMobileDefault  !1

Value::sub:OverrideHeadMobile  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:OverrideHeadMobile') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- sub:OverrideHeadMobile
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''sub:OverrideHeadMobile'',''formrrcaccounts_sub:overrideheadmobile_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('sub:OverrideHeadMobile')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('sub:OverrideHeadMobile') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','sub:OverrideHeadMobile',clip(1),,loc:readonly,,,loc:javascript,,'Override Head Account Mandatory Mobile Check') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:OverrideHeadMobile') & '_value')

Comment::sub:OverrideHeadMobile  Routine
    loc:comment = ''
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:OverrideHeadMobile') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sub:OverrideMobileDefault  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:OverrideMobileDefault') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Override Mandatory Mobile Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sub:OverrideMobileDefault  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sub:OverrideMobileDefault',p_web.GetValue('NewValue'))
    sub:OverrideMobileDefault = p_web.GetValue('NewValue') !FieldType= BYTE Field = sub:OverrideMobileDefault
    do Value::sub:OverrideMobileDefault
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('sub:OverrideMobileDefault',p_web.GetValue('Value'))
    sub:OverrideMobileDefault = p_web.GetValue('Value')
  End
  do Value::sub:OverrideMobileDefault
  do SendAlert

Value::sub:OverrideMobileDefault  Routine
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:OverrideMobileDefault') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- sub:OverrideMobileDefault
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''sub:OverrideMobileDefault'',''formrrcaccounts_sub:overridemobiledefault_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('sub:OverrideHeadMobile') = 0,'disabled','')
  If p_web.GetSessionValue('sub:OverrideMobileDefault') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','sub:OverrideMobileDefault',clip(1),,loc:readonly,,,loc:javascript,,'Override Mandatory Mobile Number') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCAccounts_' & p_web._nocolon('sub:OverrideMobileDefault') & '_value')

Comment::sub:OverrideMobileDefault  Routine
    loc:comment = ''
  p_web._DivHeader('FormRRCAccounts_' & p_web._nocolon('sub:OverrideMobileDefault') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormRRCAccounts_sub:Main_Account_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sub:Main_Account_Number
      else
        do Value::sub:Main_Account_Number
      end
  of lower('FormRRCAccounts_sub:Account_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sub:Account_Number
      else
        do Value::sub:Account_Number
      end
  of lower('FormRRCAccounts_sub:Company_Name_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sub:Company_Name
      else
        do Value::sub:Company_Name
      end
  of lower('FormRRCAccounts_sub:Branch_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sub:Branch
      else
        do Value::sub:Branch
      end
  of lower('FormRRCAccounts_sub:Address_Line1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sub:Address_Line1
      else
        do Value::sub:Address_Line1
      end
  of lower('FormRRCAccounts_sub:Address_Line2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sub:Address_Line2
      else
        do Value::sub:Address_Line2
      end
  of lower('FormRRCAccounts_sub:Address_Line3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sub:Address_Line3
      else
        do Value::sub:Address_Line3
      end
  of lower('FormRRCAccounts_sub:Postcode_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sub:Postcode
      else
        do Value::sub:Postcode
      end
  of lower('FormRRCAccounts_sub:Hub_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sub:Hub
      else
        do Value::sub:Hub
      end
  of lower('FormRRCAccounts_sub:Telephone_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sub:Telephone_Number
      else
        do Value::sub:Telephone_Number
      end
  of lower('FormRRCAccounts_sub:Fax_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sub:Fax_Number
      else
        do Value::sub:Fax_Number
      end
  of lower('FormRRCAccounts_sub:EmailAddress_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sub:EmailAddress
      else
        do Value::sub:EmailAddress
      end
  of lower('FormRRCAccounts_sub:Contact_Name_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sub:Contact_Name
      else
        do Value::sub:Contact_Name
      end
  of lower('FormRRCAccounts_sub:Courier_Outgoing_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sub:Courier_Outgoing
      else
        do Value::sub:Courier_Outgoing
      end
  of lower('FormRRCAccounts_sub:UseAlternativeAdd_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sub:UseAlternativeAdd
      else
        do Value::sub:UseAlternativeAdd
      end
  of lower('FormRRCAccounts_sub:ForceOrderNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sub:ForceOrderNumber
      else
        do Value::sub:ForceOrderNumber
      end
  of lower('FormRRCAccounts_sub:ForceEstimate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sub:ForceEstimate
      else
        do Value::sub:ForceEstimate
      end
  of lower('FormRRCAccounts_sub:EstimateIfOver_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sub:EstimateIfOver
      else
        do Value::sub:EstimateIfOver
      end
  of lower('FormRRCAccounts_sub:Stop_Account_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sub:Stop_Account
      else
        do Value::sub:Stop_Account
      end
  of lower('FormRRCAccounts_sub:ForceEndUserName_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sub:ForceEndUserName
      else
        do Value::sub:ForceEndUserName
      end
  of lower('FormRRCAccounts_sub:FinanceContactName_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sub:FinanceContactName
      else
        do Value::sub:FinanceContactName
      end
  of lower('FormRRCAccounts_sub:FinanceTelephoneNo_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sub:FinanceTelephoneNo
      else
        do Value::sub:FinanceTelephoneNo
      end
  of lower('FormRRCAccounts_sub:FinanceEmailAddress_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sub:FinanceEmailAddress
      else
        do Value::sub:FinanceEmailAddress
      end
  of lower('FormRRCAccounts_sub:FinanceAddress1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sub:FinanceAddress1
      else
        do Value::sub:FinanceAddress1
      end
  of lower('FormRRCAccounts_sub:FinanceAddress2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sub:FinanceAddress2
      else
        do Value::sub:FinanceAddress2
      end
  of lower('FormRRCAccounts_sub:FinanceAddress3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sub:FinanceAddress3
      else
        do Value::sub:FinanceAddress3
      end
  of lower('FormRRCAccounts_sub:FinancePostcode_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sub:FinancePostcode
      else
        do Value::sub:FinancePostcode
      end
  of lower('FormRRCAccounts_sub:AccountLimit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sub:AccountLimit
      else
        do Value::sub:AccountLimit
      end
  of lower('FormRRCAccounts_sub:OverrideHeadVATNo_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sub:OverrideHeadVATNo
      else
        do Value::sub:OverrideHeadVATNo
      end
  of lower('FormRRCAccounts_sub:VAT_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sub:VAT_Number
      else
        do Value::sub:VAT_Number
      end
  of lower('FormRRCAccounts_sub:OverrideHeadMobile_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sub:OverrideHeadMobile
      else
        do Value::sub:OverrideHeadMobile
      end
  of lower('FormRRCAccounts_sub:OverrideMobileDefault_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sub:OverrideMobileDefault
      else
        do Value::sub:OverrideMobileDefault
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormRRCAccounts_form:ready_',1)
  p_web.SetSessionValue('FormRRCAccounts_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormRRCAccounts',0)
  sub:Main_Account_Number = p_web.GSV('BookingAccount')
  p_web.SetSessionValue('sub:Main_Account_Number',sub:Main_Account_Number)

PreCopy  Routine
  p_web.SetValue('FormRRCAccounts_form:ready_',1)
  p_web.SetSessionValue('FormRRCAccounts_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormRRCAccounts',0)
  p_web._PreCopyRecord(SUBTRACC,sub:RecordNumberKey)
  ! here we need to copy the non-unique fields across
  sub:Main_Account_Number = p_web.GSV('BookingAccount')
  p_web.SetSessionValue('sub:Main_Account_Number',p_web.GSV('BookingAccount'))

PreUpdate       Routine
  p_web.SetValue('FormRRCAccounts_form:ready_',1)
  p_web.SetSessionValue('FormRRCAccounts_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormRRCAccounts:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormRRCAccounts_form:ready_',1)
  p_web.SetSessionValue('FormRRCAccounts_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormRRCAccounts:Primed',0)
  p_web.setsessionvalue('showtab_FormRRCAccounts',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
          If p_web.IfExistsValue('sub:UseAlternativeAdd') = 0
            p_web.SetValue('sub:UseAlternativeAdd',0)
            sub:UseAlternativeAdd = 0
          End
          If p_web.IfExistsValue('sub:ForceOrderNumber') = 0
            p_web.SetValue('sub:ForceOrderNumber',0)
            sub:ForceOrderNumber = 0
          End
          If p_web.IfExistsValue('sub:ForceEstimate') = 0
            p_web.SetValue('sub:ForceEstimate',0)
            sub:ForceEstimate = 0
          End
          If p_web.IfExistsValue('sub:Stop_Account') = 0
            p_web.SetValue('sub:Stop_Account','NO')
            sub:Stop_Account = 'NO'
          End
          If p_web.IfExistsValue('sub:ForceEndUserName') = 0
            p_web.SetValue('sub:ForceEndUserName',0)
            sub:ForceEndUserName = 0
          End
          If p_web.IfExistsValue('sub:OverrideHeadVATNo') = 0
            p_web.SetValue('sub:OverrideHeadVATNo',0)
            sub:OverrideHeadVATNo = 0
          End
          If p_web.IfExistsValue('sub:OverrideHeadMobile') = 0
            p_web.SetValue('sub:OverrideHeadMobile',0)
            sub:OverrideHeadMobile = 0
          End
        If (p_web.GSV('sub:OverrideHeadMobile') = 0)
          If p_web.IfExistsValue('sub:OverrideMobileDefault') = 0
            p_web.SetValue('sub:OverrideMobileDefault',0)
            sub:OverrideMobileDefault = 0
          End
        End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  ! Override Poxy Dictionary Validation Checks
  
    sub:Account_Type = 'CREDIT'       !CASH|CREDIT
    sub:Allow_Cash_Sales = 'NO'     !?????????
    sub:Use_Collection_Address = 'NO'
    sub:Use_Delivery_Address = 'NO'
    sub:Use_Customer_Address = 'NO'
    
  do CompleteCheckBoxes
  do ValidateRecord
  do CheckForDuplicate

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord
  do CheckForDuplicate

ValidateUpdate  Routine
  ! Override Poxy Dictionary Validation Checks
    IF (sub:Account_Type = '')
        sub:Account_Type = 'CREDIT'       !CASH|CREDIT
    END ! IF
    IF (sub:Use_Collection_Address = '')
        sub:Use_Collection_Address = 'NO'
    END !IF
    IF (sub:Use_Delivery_Address = '')
        sub:Use_Delivery_Address = 'NO'
    END !IF
    IF (sub:Use_Customer_Address = '')
        sub:Use_Customer_Address = 'NO'
    END !IF
    
  do CompleteCheckBoxes
  do ValidateRecord
  do CheckForDuplicate
CheckForDuplicate  Routine
  If loc:invalid <> '' then exit. ! no need to check, record is already invalid
  If ans = 0 then exit. ! no need to check, as no action happening
  If p_web.GetSessionValue('FormRRCAccounts:Primed') = 0 and Ans = InsertRecord
    Get(SUBTRACC,0)
  End
  ! Check for duplicates
  If Duplicate(sub:Main_Account_Key)
    loc:Invalid = 'sub:Main_Account_Number'
    loc:Alert = clip(p_web.site.DuplicateText) & ' Main_Account_Key --> '&clip('Main Account Number')&', '&clip('Account Number')&''
  End
  If Duplicate(sub:Account_Number_Key)
    loc:Invalid = 'sub:Account_Number'
    loc:Alert = clip(p_web.site.DuplicateText) & ' Account_Number_Key --> '&clip('Account Number')&' = ' & clip(sub:Account_Number)
  End

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormRRCAccounts_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormRRCAccounts_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  If Loc:Invalid <> '' then exit.
        If sub:Account_Number = ''
          loc:Invalid = 'sub:Account_Number'
          loc:alert = p_web.translate('Account Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          sub:Account_Number = Upper(sub:Account_Number)
          p_web.SetSessionValue('sub:Account_Number',sub:Account_Number)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
          sub:Company_Name = Upper(sub:Company_Name)
          p_web.SetSessionValue('sub:Company_Name',sub:Company_Name)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
          sub:Branch = Upper(sub:Branch)
          p_web.SetSessionValue('sub:Branch',sub:Branch)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
          sub:Address_Line1 = Upper(sub:Address_Line1)
          p_web.SetSessionValue('sub:Address_Line1',sub:Address_Line1)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
          sub:Address_Line2 = Upper(sub:Address_Line2)
          p_web.SetSessionValue('sub:Address_Line2',sub:Address_Line2)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
          sub:Address_Line3 = Upper(sub:Address_Line3)
          p_web.SetSessionValue('sub:Address_Line3',sub:Address_Line3)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
          sub:Telephone_Number = Upper(sub:Telephone_Number)
          p_web.SetSessionValue('sub:Telephone_Number',sub:Telephone_Number)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
          sub:Fax_Number = Upper(sub:Fax_Number)
          p_web.SetSessionValue('sub:Fax_Number',sub:Fax_Number)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
          sub:Contact_Name = Upper(sub:Contact_Name)
          p_web.SetSessionValue('sub:Contact_Name',sub:Contact_Name)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
          sub:Courier_Outgoing = Upper(sub:Courier_Outgoing)
          p_web.SetSessionValue('sub:Courier_Outgoing',sub:Courier_Outgoing)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
          sub:EstimateIfOver = Upper(sub:EstimateIfOver)
          p_web.SetSessionValue('sub:EstimateIfOver',sub:EstimateIfOver)
        If loc:Invalid <> '' then exit.
  ! Automatic Dictionary Validation
    If InList(clip(sub:Stop_Account) ,'NO','YES' ) = 0
      loc:Invalid = 'sub:Stop_Account'
      loc:Alert = clip('Stop Account') & ' ' & clip(p_web.site.InListText) & p_web._jsok('  NO, YES')
      !exit
    End
  If Loc:Invalid <> '' then exit.
  ! tab = 2
    loc:InvalidTab += 1
  If Loc:Invalid <> '' then exit.
          sub:FinanceContactName = Upper(sub:FinanceContactName)
          p_web.SetSessionValue('sub:FinanceContactName',sub:FinanceContactName)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
          sub:FinanceTelephoneNo = Upper(sub:FinanceTelephoneNo)
          p_web.SetSessionValue('sub:FinanceTelephoneNo',sub:FinanceTelephoneNo)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
          sub:FinanceAddress1 = Upper(sub:FinanceAddress1)
          p_web.SetSessionValue('sub:FinanceAddress1',sub:FinanceAddress1)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
          sub:FinanceAddress2 = Upper(sub:FinanceAddress2)
          p_web.SetSessionValue('sub:FinanceAddress2',sub:FinanceAddress2)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
          sub:FinanceAddress3 = Upper(sub:FinanceAddress3)
          p_web.SetSessionValue('sub:FinanceAddress3',sub:FinanceAddress3)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
          sub:FinancePostcode = Upper(sub:FinancePostcode)
          p_web.SetSessionValue('sub:FinancePostcode',sub:FinancePostcode)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
          sub:VAT_Number = Upper(sub:VAT_Number)
          p_web.SetSessionValue('sub:VAT_Number',sub:VAT_Number)
        If loc:Invalid <> '' then exit.
  ! The following fields are not on the form, but need to be checked anyway.
  ! Automatic Dictionary Validation
    If InList(clip(sub:Account_Type) ,'CASH','CREDIT' ) = 0
      loc:Invalid = 'sub:Account_Type'
      loc:Alert = clip('sub:Account_Type') & ' ' & clip(p_web.site.InListText) & p_web._jsok('  CASH, CREDIT')
      !exit
    End
  If Loc:Invalid <> '' then exit.
  ! Automatic Dictionary Validation
    If InList(clip(sub:Allow_Cash_Sales) ,'NO','YES' ) = 0
      loc:Invalid = 'sub:Allow_Cash_Sales'
      loc:Alert = clip('sub:Allow_Cash_Sales') & ' ' & clip(p_web.site.InListText) & p_web._jsok('  NO, YES')
      !exit
    End
  If Loc:Invalid <> '' then exit.
  ! Automatic Dictionary Validation
    If InList(clip(sub:Use_Collection_Address) ,'NO','YES' ) = 0
      loc:Invalid = 'sub:Use_Collection_Address'
      loc:Alert = clip('sub:Use_Collection_Address') & ' ' & clip(p_web.site.InListText) & p_web._jsok('  NO, YES')
      !exit
    End
  If Loc:Invalid <> '' then exit.
  ! Automatic Dictionary Validation
    If InList(clip(sub:Use_Delivery_Address) ,'NO','YES' ) = 0
      loc:Invalid = 'sub:Use_Delivery_Address'
      loc:Alert = clip('sub:Use_Delivery_Address') & ' ' & clip(p_web.site.InListText) & p_web._jsok('  NO, YES')
      !exit
    End
  If Loc:Invalid <> '' then exit.
! NET:WEB:StagePOST
PostInsert      Routine

PostCopy        Routine
  p_web.SetSessionValue('FormRRCAccounts:Primed',0)

PostUpdate      Routine
  p_web.SetSessionValue('FormRRCAccounts:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')

PostDelete      Routine
CheckOverrideVatNumber      PROCEDURE()
    CODE
        IF (p_web.GSV('sub:OverrideHeadVatNo') = 0)
            !Fill in the Head Vat Number if not used - TrkBs: 5364 (DBH: 17-03-2005)
            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = p_web.GSV('sub:Main_Account_Number')
            If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Found
                p_web.SSV('sub:Vat_Number',tra:Vat_Number)
            Else !If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Error
            End !If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign

        END ! IF
        
            
