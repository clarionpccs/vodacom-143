

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER082.INC'),ONCE        !Local module procedure declarations
                     END


SetJobType           PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locChargeableJob     STRING(3)                             !
locWarrantyJob       STRING(3)                             !
locCChargeType       STRING(30)                            !
locWChargeType       STRING(30)                            !
locCRepairType       STRING(30)                            !
locWRepairType       STRING(30)                            !
locConfirmationText  STRING(100)                           !
locErrorText         STRING(100)                           !
FilesOpened     Long
STOCKALL::State  USHORT
WARPARTS::State  USHORT
PARTS::State  USHORT
REPTYDEF::State  USHORT
CHARTYPE::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
locCChargeType_OptionView   View(CHARTYPE)
                          Project(cha:Charge_Type)
                        End
locCRepairType_OptionView   View(REPTYDEF)
                          Project(rtd:Repair_Type)
                        End
locWChargeType_OptionView   View(CHARTYPE)
                          Project(cha:Charge_Type)
                        End
locWRepairType_OptionView   View(REPTYDEF)
                          Project(rtd:Repair_Type)
                        End
locClass                    CLASS
validateWarrantyParts           PROCEDURE(),BYTE
validateChargeableParts         PROCEDURE(),BYTE
                            END
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('SetJobType')
  loc:formname = 'SetJobType_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('SetJobType','')
    p_web._DivHeader('SetJobType',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferSetJobType',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferSetJobType',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferSetJobType',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_SetJobType',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferSetJobType',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_SetJobType',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
deleteSessionValues ROUTINE
    p_web.DeleteSessionValue('ReadOnly:ChargeableJob')
    p_web.DeleteSessionValue('ReadOnly:WarrantyJob')
    p_web.DeleteSessionValue('ReadOnly:CRepairType')
    p_web.DeleteSessionValue('ReadOnly:CChargeType')
    p_web.DeleteSessionValue('ReadOnly:WRepairType')
    p_web.DeleteSessionValue('ReadOnly:WChargeType')
    p_web.DeleteSessionValue('locConfirmationText')
    p_web.DeleteSessionValue('locCChargeType')
    p_web.DeleteSessionValue('locChargeableJob')
    p_web.DeleteSessionValue('locCRepairType')
    p_web.DeleteSessionValue('locWarrantyJob')
    p_web.DeleteSessionValue('locWChargeType')
    p_web.DeleteSessionValue('locWRepairType')
enableFields        ROUTINE  
    p_web.SSV('ReadOnly:ChargeableJOb',0)
    p_web.SSV('ReadOnly:WarrantyJob',0)
    p_web.SSV('ReadOnly:CRepairType',0)
    p_web.SSV('ReadOnly:CChargeType',0)
    p_web.SSV('ReadOnly:WRepairType',0)
    p_web.SSV('ReadOnly:WChargeType',0)  
disableFields       ROUTINE
    p_web.SSV('ReadOnly:ChargeableJOb',1)
    p_web.SSV('ReadOnly:WarrantyJob',1)
    p_web.SSV('ReadOnly:CRepairType',1)
    p_web.SSV('ReadOnly:CChargeType',1)
    p_web.SSV('ReadOnly:WRepairType',1)
    p_web.SSV('ReadOnly:WChargeType',1)    
    

    
  
loadJobData         ROUTINE
    p_web.SSV('locChargeableJob',p_web.GSV('job:Chargeable_Job'))
    p_web.SSV('locWarrantyJob',p_web.GSV('job:Warranty_Job'))
    p_web.SSV('locCChargeType',p_web.GSV('job:Charge_Type'))
    p_web.SSV('locCRepairType',p_web.GSV('job:Repair_Type'))
    p_web.SSV('locWChargeType',p_web.GSV('job:Warranty_Charge_Type'))
    p_web.SSV('locWRepairType',p_web.GSV('job:Repair_Type_Warranty'))
resetScreen         ROUTINE
    DO enablefields
    DO loadJobData

    p_web.SSV('Hide:ConfirmButton',1)
    p_web.SSV('Hide:CancelButton',1)
    p_web.SSV('Hide:SplitJobButton',1)
    p_web.SSV('locConfirmationText','')
    p_web.SSV('locErrorText','')
    
OpenFiles  ROUTINE
  p_web._OpenFile(STOCKALL)
  p_web._OpenFile(WARPARTS)
  p_web._OpenFile(PARTS)
  p_web._OpenFile(REPTYDEF)
  p_web._OpenFile(CHARTYPE)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(STOCKALL)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(CHARTYPE)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('SetJobType_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locChargeableJob',locChargeableJob)
  p_web.SetSessionValue('locCChargeType',locCChargeType)
  p_web.SetSessionValue('locCRepairType',locCRepairType)
  p_web.SetSessionValue('locWarrantyJob',locWarrantyJob)
  p_web.SetSessionValue('locWChargeType',locWChargeType)
  p_web.SetSessionValue('locWRepairType',locWRepairType)
  p_web.SetSessionValue('locErrorText',locErrorText)
  p_web.SetSessionValue('locConfirmationText',locConfirmationText)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locChargeableJob')
    locChargeableJob = p_web.GetValue('locChargeableJob')
    p_web.SetSessionValue('locChargeableJob',locChargeableJob)
  End
  if p_web.IfExistsValue('locCChargeType')
    locCChargeType = p_web.GetValue('locCChargeType')
    p_web.SetSessionValue('locCChargeType',locCChargeType)
  End
  if p_web.IfExistsValue('locCRepairType')
    locCRepairType = p_web.GetValue('locCRepairType')
    p_web.SetSessionValue('locCRepairType',locCRepairType)
  End
  if p_web.IfExistsValue('locWarrantyJob')
    locWarrantyJob = p_web.GetValue('locWarrantyJob')
    p_web.SetSessionValue('locWarrantyJob',locWarrantyJob)
  End
  if p_web.IfExistsValue('locWChargeType')
    locWChargeType = p_web.GetValue('locWChargeType')
    p_web.SetSessionValue('locWChargeType',locWChargeType)
  End
  if p_web.IfExistsValue('locWRepairType')
    locWRepairType = p_web.GetValue('locWRepairType')
    p_web.SetSessionValue('locWRepairType',locWRepairType)
  End
  if p_web.IfExistsValue('locErrorText')
    locErrorText = p_web.GetValue('locErrorText')
    p_web.SetSessionValue('locErrorText',locErrorText)
  End
  if p_web.IfExistsValue('locConfirmationText')
    locConfirmationText = p_web.GetValue('locConfirmationText')
    p_web.SetSessionValue('locConfirmationText',locConfirmationText)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('SetJobType_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
      ! Generate Form
      DO loadJobData
      p_web.SSV('Hide:ConfirmButton',1)
      p_web.SSV('Hide:CancelButton',1)
      p_web.SSV('Hide:SplitJobButton',1)
      DO enableFields
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locChargeableJob = p_web.RestoreValue('locChargeableJob')
 locCChargeType = p_web.RestoreValue('locCChargeType')
 locCRepairType = p_web.RestoreValue('locCRepairType')
 locWarrantyJob = p_web.RestoreValue('locWarrantyJob')
 locWChargeType = p_web.RestoreValue('locWChargeType')
 locWRepairType = p_web.RestoreValue('locWRepairType')
 locErrorText = p_web.RestoreValue('locErrorText')
 locConfirmationText = p_web.RestoreValue('locConfirmationText')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'BillingConfirmation?passedURL=ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('SetJobType_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('SetJobType_ChainTo')
    loc:formaction = p_web.GetSessionValue('SetJobType_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="SetJobType" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="SetJobType" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="SetJobType" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Set Job Type') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Set Job Type',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_SetJobType">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_SetJobType" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_SetJobType')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Chargeable Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Warranty Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_SetJobType')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_SetJobType'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locChargeableJob')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab4'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_SetJobType')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab4'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Chargeable Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_SetJobType_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Chargeable Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Chargeable Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Chargeable Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Chargeable Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&130&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locChargeableJob
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locChargeableJob
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locChargeableJob
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&130&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locCChargeType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locCChargeType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locCChargeType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&130&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locCRepairType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locCRepairType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locCRepairType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Warranty Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_SetJobType_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Warranty Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Warranty Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Warranty Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Warranty Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&130&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locWarrantyJob
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locWarrantyJob
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locWarrantyJob
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&130&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locWChargeType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locWChargeType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locWChargeType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&130&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locWRepairType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locWRepairType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locWRepairType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel4">'&CRLF &|
                                    '  <div id="panel4Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel4Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_SetJobType_4">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab4" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab4">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab4">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab4">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::locErrorText
      do Comment::locErrorText
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::locConfirmationText
      do Comment::locConfirmationText
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonConfirm
      do Comment::buttonConfirm
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonSplitJob
      do Comment::buttonSplitJob
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonCancel
      do Comment::buttonCancel
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locChargeableJob  Routine
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locChargeableJob') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Chargeable Job')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('locChargeableJob') & '_prompt')

Validate::locChargeableJob  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locChargeableJob',p_web.GetValue('NewValue'))
    locChargeableJob = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locChargeableJob
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value','NO')
    end
    p_web.SetSessionValue('locChargeableJob',p_web.GetValue('Value'))
    locChargeableJob = p_web.GetValue('Value')
  End
      ! Chargeable Job
      DO disableFields
      p_web.SSV('Hide:ConfirmButton',0)
      p_web.SSV('Hide:CancelButton',0)
      p_web.SSV('Hide:SplitJobButton',1)
      p_web.SSV('JobTypeAction','')
  
  
      IF ( p_web.GSV('locChargeableJob') = 'YES')
          IF (p_web.GSV('locWarrantyJob') = 'YES')
              p_web.SSV('locConfirmationText','Confirm that you wish to make this job a Split Warranty/Chargeable Job')
              p_web.SSV('Hide:SplitJobButton',0)
              p_web.SSV('JobTypeAction','CHATOSPLIT')
          ELSE
              p_web.SSV('locConfirmationText','Confirm that you wish to make this job Chargeable')
              p_web.SSV('JobTypeAction','CHA')
          END
          p_web.SSV('ConfirmButtonText','Chargeable')
      ELSE
  
          CASE locClass.validateChargeableParts()
          OF 1
              DO resetScreen
              p_web.ssv('locErrorText','Cannot make job Warranty Only. There are chargeable parts attached.')
          OF 2
              DO resetScreen
              p_web.ssv('locErrorText','Cannot make job Warranty Only. This job is an estimate.')
          ELSE
              
              p_web.SSV('locConfirmationText','Confirm that you wish to make this job Warranty Only')
              p_web.SSV('ConfirmButtonText','Warranty')
              p_web.SSV('JobTypeAction','WAR')            
          END
          
      END
  do Value::locChargeableJob
  do SendAlert
  do Prompt::locCChargeType
  do Value::locCChargeType  !1
  do Comment::locCChargeType
  do Prompt::locCRepairType
  do Value::locCRepairType  !1
  do Comment::locCRepairType
  do Value::buttonConfirm  !1
  do Value::locConfirmationText  !1
  do Value::locWChargeType  !1
  do Value::locWRepairType  !1
  do Value::locWarrantyJob  !1
  do Value::buttonCancel  !1
  do Value::buttonSplitJob  !1
  do Value::locErrorText  !1

Value::locChargeableJob  Routine
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locChargeableJob') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locChargeableJob
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locChargeableJob'',''setjobtype_locchargeablejob_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locChargeableJob')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('ReadOnly:ChargeableJob')= 1,'disabled','')
  If p_web.GetSessionValue('locChargeableJob') = 'YES'
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locChargeableJob',clip('YES'),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('locChargeableJob') & '_value')

Comment::locChargeableJob  Routine
    loc:comment = ''
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locChargeableJob') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locCChargeType  Routine
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locCChargeType') & '_prompt',Choose(p_web.GSV('locChargeableJob') <> 'YES','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Chargeable Type')
  If p_web.GSV('locChargeableJob') <> 'YES'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('locCChargeType') & '_prompt')

Validate::locCChargeType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locCChargeType',p_web.GetValue('NewValue'))
    locCChargeType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locCChargeType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locCChargeType',p_web.GetValue('Value'))
    locCChargeType = p_web.GetValue('Value')
  End
  do Value::locCChargeType
  do SendAlert
  do Value::locCRepairType  !1

Value::locCChargeType  Routine
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locCChargeType') & '_value',Choose(p_web.GSV('locChargeableJob') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locChargeableJob') <> 'YES')
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locCChargeType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locCChargeType'',''setjobtype_loccchargetype_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locCChargeType')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  packet = clip(packet) & p_web.CreateSelect('locCChargeType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locCChargeType') = 0
    p_web.SetSessionValue('locCChargeType','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('locCChargeType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(STOCKALL)
  bind(stl:Record)
  p_web._OpenFile(WARPARTS)
  bind(wpr:Record)
  p_web._OpenFile(PARTS)
  bind(par:Record)
  p_web._OpenFile(REPTYDEF)
  bind(rtd:Record)
  p_web._OpenFile(CHARTYPE)
  bind(cha:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(locCChargeType_OptionView)
  locCChargeType_OptionView{prop:filter} = 'Upper(cha:Warranty) = ''NO'''
  locCChargeType_OptionView{prop:order} = 'UPPER(cha:Charge_Type)'
  Set(locCChargeType_OptionView)
  Loop
    Next(locCChargeType_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('locCChargeType') = 0
      p_web.SetSessionValue('locCChargeType',cha:Charge_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,cha:Charge_Type,choose(cha:Charge_Type = p_web.getsessionvalue('locCChargeType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(locCChargeType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(STOCKALL)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(CHARTYPE)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('locCChargeType') & '_value')

Comment::locCChargeType  Routine
    loc:comment = ''
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locCChargeType') & '_comment',Choose(p_web.GSV('locChargeableJob') <> 'YES','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locChargeableJob') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('locCChargeType') & '_comment')

Prompt::locCRepairType  Routine
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locCRepairType') & '_prompt',Choose(p_web.GSV('locChargeableJob') <> 'YES','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Repair Type')
  If p_web.GSV('locChargeableJob') <> 'YES'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('locCRepairType') & '_prompt')

Validate::locCRepairType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locCRepairType',p_web.GetValue('NewValue'))
    locCRepairType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locCRepairType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locCRepairType',p_web.GetValue('Value'))
    locCRepairType = p_web.GetValue('Value')
  End
  do Value::locCRepairType
  do SendAlert

Value::locCRepairType  Routine
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locCRepairType') & '_value',Choose(p_web.GSV('locChargeableJob') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locChargeableJob') <> 'YES')
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locCRepairType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locCRepairType'',''setjobtype_loccrepairtype_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  packet = clip(packet) & p_web.CreateSelect('locCRepairType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locCRepairType') = 0
    p_web.SetSessionValue('locCRepairType','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('locCRepairType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(STOCKALL)
  bind(stl:Record)
  p_web._OpenFile(WARPARTS)
  bind(wpr:Record)
  p_web._OpenFile(PARTS)
  bind(par:Record)
  p_web._OpenFile(REPTYDEF)
  bind(rtd:Record)
  p_web._OpenFile(CHARTYPE)
  bind(cha:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(locCRepairType_OptionView)
  locCRepairType_OptionView{prop:filter} = 'Upper(rtd:Chargeable) = ''YES'' AND Upper(rtd:Manufacturer) = Upper(''' & p_web.GSV('job:manufacturer') & ''') AND rtd:NotAvailable = 0'
  locCRepairType_OptionView{prop:order} = 'UPPER(rtd:Repair_Type)'
  Set(locCRepairType_OptionView)
  Loop
    Next(locCRepairType_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('locCRepairType') = 0
      p_web.SetSessionValue('locCRepairType',rtd:Repair_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,rtd:Repair_Type,choose(rtd:Repair_Type = p_web.getsessionvalue('locCRepairType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(locCRepairType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(STOCKALL)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(CHARTYPE)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('locCRepairType') & '_value')

Comment::locCRepairType  Routine
    loc:comment = ''
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locCRepairType') & '_comment',Choose(p_web.GSV('locChargeableJob') <> 'YES','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locChargeableJob') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('locCRepairType') & '_comment')

Prompt::locWarrantyJob  Routine
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locWarrantyJob') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Warranty Job')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locWarrantyJob  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locWarrantyJob',p_web.GetValue('NewValue'))
    locWarrantyJob = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locWarrantyJob
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value','NO')
    end
    p_web.SetSessionValue('locWarrantyJob',p_web.GetValue('Value'))
    locWarrantyJob = p_web.GetValue('Value')
  End
      ! Warranty Job
  DO disableFields
  p_web.SSV('JobTypeAction','')
  
  p_web.SSV('Hide:ConfirmButton',0)
  p_web.SSV('Hide:CancelButton',0)
  p_web.SSV('Hide:SplitJobButton',1)
  
  p_web.SSV('locErrorText','')
  
  IF ( p_web.GSV('locWarrantyJob') = 'YES')
      IF (p_web.GSV('locChargeableJob') = 'YES')
          p_web.SSV('locConfirmationText','Confirm that you wish to make this job a Split Warranty/Chargeable Job')
          p_web.SSV('Hide:SplitJobButton',0)
          p_web.SSV('JobTypeAction','WARTOSPLIT')
              
              ! Can't make warranty only.
          Case locClass.validateChargeableParts()
          of 1 ! Chargeable Parts
              p_web.SSV('Hide:ConfirmButton',1)
              p_web.SSV('locConfirmationText','Cannot make Warranty Only job as there are still Chargeable Parts attached. Please confirm if you wish to make this a split job.')
          of 2 ! Estimate
              p_web.SSV('Hide:ConfirmButton',1)
                          p_web.SSV('locConfirmationText','Cannot make Warranty Only job as it is an Estimate. Please confirm if you wish to make this a split job.')
          END
              
      ELSE
          p_web.SSV('locConfirmationText','Confirm that you wish to make this job Warranty')
          p_web.SSV('JobTypeAction','WAR')
      END
      p_web.SSV('ConfirmButtonText','Warranty')
  ELSE
      IF (locClass.validateWarrantyParts())
          p_web.SSV('locErrorText','Warning! There are still warranty parts attached. They will be transferred to Chargeable')
      END
          
      p_web.SSV('locConfirmationText','Confirm that you wish to make this job Chargeable Only')
      p_web.SSV('JobTypeAction','CHA')
      p_web.SSV('ConfirmButtonText','Chargeable')
  END
  do Value::locWarrantyJob
  do SendAlert
  do Prompt::locWChargeType
  do Value::locWChargeType  !1
  do Comment::locWChargeType
  do Prompt::locWRepairType
  do Value::locWRepairType  !1
  do Comment::locWRepairType
  do Value::buttonConfirm  !1
  do Value::buttonCancel  !1
  do Value::locConfirmationText  !1
  do Value::locCChargeType  !1
  do Value::locCRepairType  !1
  do Value::locChargeableJob  !1
  do Value::buttonSplitJob  !1
  do Value::locErrorText  !1

Value::locWarrantyJob  Routine
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locWarrantyJob') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locWarrantyJob
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locWarrantyJob'',''setjobtype_locwarrantyjob_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locWarrantyJob')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('ReadOnly:WarrantyJob') = 1,'disabled','')
  If p_web.GetSessionValue('locWarrantyJob') = 'YES'
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locWarrantyJob',clip('YES'),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('locWarrantyJob') & '_value')

Comment::locWarrantyJob  Routine
    loc:comment = ''
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locWarrantyJob') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locWChargeType  Routine
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locWChargeType') & '_prompt',Choose(p_web.GSV('locWarrantyJob') <> 'YES','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Charge Type')
  If p_web.GSV('locWarrantyJob') <> 'YES'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('locWChargeType') & '_prompt')

Validate::locWChargeType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locWChargeType',p_web.GetValue('NewValue'))
    locWChargeType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locWChargeType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locWChargeType',p_web.GetValue('Value'))
    locWChargeType = p_web.GetValue('Value')
  End
  do Value::locWChargeType
  do SendAlert

Value::locWChargeType  Routine
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locWChargeType') & '_value',Choose(p_web.GSV('locWarrantyJob') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locWarrantyJob') <> 'YES')
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locWChargeType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locWChargeType'',''setjobtype_locwchargetype_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  packet = clip(packet) & p_web.CreateSelect('locWChargeType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locWChargeType') = 0
    p_web.SetSessionValue('locWChargeType','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('locWChargeType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(STOCKALL)
  bind(stl:Record)
  p_web._OpenFile(WARPARTS)
  bind(wpr:Record)
  p_web._OpenFile(PARTS)
  bind(par:Record)
  p_web._OpenFile(REPTYDEF)
  bind(rtd:Record)
  p_web._OpenFile(CHARTYPE)
  bind(cha:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(locWChargeType_OptionView)
  locWChargeType_OptionView{prop:filter} = 'Upper(cha:Warranty) = ''YES'''
  locWChargeType_OptionView{prop:order} = 'UPPER(cha:Charge_Type)'
  Set(locWChargeType_OptionView)
  Loop
    Next(locWChargeType_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('locWChargeType') = 0
      p_web.SetSessionValue('locWChargeType',cha:Charge_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,cha:Charge_Type,choose(cha:Charge_Type = p_web.getsessionvalue('locWChargeType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(locWChargeType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(STOCKALL)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(CHARTYPE)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('locWChargeType') & '_value')

Comment::locWChargeType  Routine
    loc:comment = ''
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locWChargeType') & '_comment',Choose(p_web.GSV('locWarrantyJob') <> 'YES','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locWarrantyJob') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('locWChargeType') & '_comment')

Prompt::locWRepairType  Routine
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locWRepairType') & '_prompt',Choose(p_web.GSV('locWarrantyJob') <> 'YES','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Repair Type')
  If p_web.GSV('locWarrantyJob') <> 'YES'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('locWRepairType') & '_prompt')

Validate::locWRepairType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locWRepairType',p_web.GetValue('NewValue'))
    locWRepairType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locWRepairType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locWRepairType',p_web.GetValue('Value'))
    locWRepairType = p_web.GetValue('Value')
  End
  do Value::locWRepairType
  do SendAlert

Value::locWRepairType  Routine
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locWRepairType') & '_value',Choose(p_web.GSV('locWarrantyJob') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locWarrantyJob') <> 'YES')
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locWRepairType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locWRepairType'',''setjobtype_locwrepairtype_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  packet = clip(packet) & p_web.CreateSelect('locWRepairType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locWRepairType') = 0
    p_web.SetSessionValue('locWRepairType','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('locWRepairType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(STOCKALL)
  bind(stl:Record)
  p_web._OpenFile(WARPARTS)
  bind(wpr:Record)
  p_web._OpenFile(PARTS)
  bind(par:Record)
  p_web._OpenFile(REPTYDEF)
  bind(rtd:Record)
  p_web._OpenFile(CHARTYPE)
  bind(cha:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(locWRepairType_OptionView)
  locWRepairType_OptionView{prop:filter} = 'Upper(rtd:Warranty) = ''YES'' AND Upper(rtd:Manufacturer) = Upper(''' & p_web.GSV('job:manufacturer') & ''') AND rtd:NotAvailable = 0'
  locWRepairType_OptionView{prop:order} = 'UPPER(rtd:Repair_Type)'
  Set(locWRepairType_OptionView)
  Loop
    Next(locWRepairType_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('locWRepairType') = 0
      p_web.SetSessionValue('locWRepairType',rtd:Repair_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,rtd:Repair_Type,choose(rtd:Repair_Type = p_web.getsessionvalue('locWRepairType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(locWRepairType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(STOCKALL)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(CHARTYPE)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('locWRepairType') & '_value')

Comment::locWRepairType  Routine
    loc:comment = ''
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locWRepairType') & '_comment',Choose(p_web.GSV('locWarrantyJob') <> 'YES','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locWarrantyJob') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('locWRepairType') & '_comment')

Validate::locErrorText  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locErrorText',p_web.GetValue('NewValue'))
    locErrorText = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locErrorText
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locErrorText',p_web.GetValue('Value'))
    locErrorText = p_web.GetValue('Value')
  End

Value::locErrorText  Routine
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locErrorText') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locErrorText
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locErrorText'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('locErrorText') & '_value')

Comment::locErrorText  Routine
    loc:comment = ''
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locErrorText') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::locConfirmationText  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locConfirmationText',p_web.GetValue('NewValue'))
    locConfirmationText = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locConfirmationText
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locConfirmationText',p_web.GetValue('Value'))
    locConfirmationText = p_web.GetValue('Value')
  End

Value::locConfirmationText  Routine
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locConfirmationText') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locConfirmationText
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locConfirmationText'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('locConfirmationText') & '_value')

Comment::locConfirmationText  Routine
    loc:comment = ''
  p_web._DivHeader('SetJobType_' & p_web._nocolon('locConfirmationText') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonConfirm  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonConfirm',p_web.GetValue('NewValue'))
    do Value::buttonConfirm
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
      ! Confirm change
  CASE p_web.GSV('JobTypeAction')
  OF 'CHA' ! Make Chargeable Job
      p_web.SSV('locChargeableJob','YES')
      p_web.SSV('locWarrantyJob','NO')
  OF 'WAR' ! Make Warranty Job
      p_web.SSV('locWarrantyJob','YES')
      p_web.SSV('locChargeableJob','NO')
  OF 'CHATOSPLIT' !Make Chargeable From Warranty
          ! Transfer Warranty Parts
      Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
      wpr:Ref_Number = p_web.GSV('job:Ref_Number')
      SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
      LOOP
          IF (Access:WARPARTS.Next())
              BREAK
          END
          IF (wpr:Ref_Number <> p_web.GSV('job:ref_Number'))
              BREAK
          END
          IF (Access:PARTS.PrimeRecord() = Level:Benign)
              recordNo# = par:Record_Number
              par:Record :=: wpr:Record
              par:Record_Number = recordNo#
              IF (Access:PARTS.TryInsert())
                  Access:PARTS.CancelAutoInc()
              END
          END
              ! Is this part in stock allocation. 
          Access:STOCKALL.ClearKey(stl:PartRecordTypeKey)
          stl:PartType = 'WAR'
          stl:PartRecordNumber = wpr:Record_Number
          IF (Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign)
              stl:PartRecordNumber = par:Record_Number
              stl:parttype = 'CHA'
              Access:STOCK.TryUpdate()
          END
          Access:WARPARTS.DeleteRecord(0)
      END
          
      p_web.SSV('locWarrantyJob','NO')
      p_web.SSV('locChargeableJob','YES')
              
  OF 'WARTOSPLIT' !Make Warranty From Chargeable 
          ! Transfer Chargeable Parts
      Access:PARTS.ClearKey(par:Part_Number_Key)
      par:Ref_Number = p_web.GSV('job:ref_number')
      SET(par:Part_Number_Key,par:Part_Number_Key)
      LOOP
          IF (Access:PARTS.Next())
              BREAK
          END
          IF (par:Ref_Number <> p_web.GSV('job:ref_number'))
              BREAK
          END
          IF (Access:WARPARTS.PrimeRecord() = Level:Benign)
              recordNo# = wpr:Record_Number
              wpr:Record :=: par:Record
              wpr:Record_Number = recordNo#
              IF (Access:WARPARTS.TryInsert())
                  Access:WARPARTS.CancelAutoInc()
              END
          END
              ! Is this part in stock allocation?
          Access:STOCKALL.ClearKey(stl:PartRecordTypeKey)
          stl:PartType = 'CHA'
          stl:PartRecordNumber = par:Record_Number
          IF (Access:STOCKALL.Tryfetch(stl:PartRecordTypeKey) = Level:Benign)
              stl:PartRecordNumber = wpr:Record_Number
              stl:PartType = 'WAR'
              Access:STOCKALL.TryUpdate()
          END
          Access:PARTS.DeleteRecord()
      END
      p_web.SSV('locChargeableJob','NO')
      p_web.SSV('locWarrantyJob','YES')
                  
  END
  
  p_web.SSV('job:Chargeable_Job',p_web.GSV('locChargeableJob'))
  p_web.SSV('job:Warranty_Job',p_web.GSV('locWarrantyJob'))
  
  Access:JOBS.ClearKey(job:Ref_Number_Key)
  job:Ref_Number = p_web.GSV('job:ref_Number')
  IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
      p_web.SessionQueueToFile(JOBS)
      Access:JOBS.TryUpdate()
  END!
  
  do resetScreen
  
  
  
  do Value::buttonConfirm
  do SendAlert
  do Value::buttonCancel  !1
  do Value::locConfirmationText  !1
  do Value::buttonCancel  !1
  do Value::buttonSplitJob  !1
  do Prompt::locCChargeType
  do Value::locCChargeType  !1
  do Prompt::locCRepairType
  do Value::locCRepairType  !1
  do Value::locChargeableJob  !1
  do Value::locConfirmationText  !1
  do Prompt::locWChargeType
  do Value::locWChargeType  !1
  do Prompt::locWRepairType
  do Value::locWRepairType  !1
  do Value::locWarrantyJob  !1

Value::buttonConfirm  Routine
  p_web._DivHeader('SetJobType_' & p_web._nocolon('buttonConfirm') & '_value',Choose(p_web.GSV('Hide:ConfirmButton') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ConfirmButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonConfirm'',''setjobtype_buttonconfirm_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ConfirmChange',p_web.GSV('ConfirmButtonText'),'MainButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('buttonConfirm') & '_value')

Comment::buttonConfirm  Routine
    loc:comment = ''
  p_web._DivHeader('SetJobType_' & p_web._nocolon('buttonConfirm') & '_comment',Choose(p_web.GSV('Hide:ConfirmButton') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:ConfirmButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonSplitJob  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonSplitJob',p_web.GetValue('NewValue'))
    do Value::buttonSplitJob
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
      ! Split Job
      ! Confirm change
  p_web.SSV('locChargeableJob','YES')
  p_web.SSV('locWarrantyJob','YES')
  p_web.SSV('job:chargeable_job','YES')
  p_web.SSV('job:warranty_Job','YES')
  
  Access:JOBS.ClearKey(job:Ref_Number_Key)
  job:Ref_Number = p_web.GSV('job:ref_Number')
  IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
      p_web.SessionQueueToFile(JOBS)
      Access:JOBS.TryUpdate()
  END!
  
  do resetScreen
  do Value::buttonSplitJob
  do SendAlert
  do Value::buttonConfirm  !1
  do Value::buttonCancel  !1
  do Prompt::locCRepairType
  do Value::locCRepairType  !1
  do Prompt::locCChargeType
  do Value::locCChargeType  !1
  do Value::locChargeableJob  !1
  do Value::locConfirmationText  !1
  do Prompt::locWChargeType
  do Value::locWChargeType  !1
  do Prompt::locWRepairType
  do Value::locWRepairType  !1
  do Value::locWarrantyJob  !1

Value::buttonSplitJob  Routine
  p_web._DivHeader('SetJobType_' & p_web._nocolon('buttonSplitJob') & '_value',Choose(p_web.GSV('Hide:SplitJobButton') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:SplitJobButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonSplitJob'',''setjobtype_buttonsplitjob_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','SplitJob','Split Job','MainButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('buttonSplitJob') & '_value')

Comment::buttonSplitJob  Routine
    loc:comment = ''
  p_web._DivHeader('SetJobType_' & p_web._nocolon('buttonSplitJob') & '_comment',Choose(p_web.GSV('Hide:SplitJobButton') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:SplitJobButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonCancel  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonCancel',p_web.GetValue('NewValue'))
    do Value::buttonCancel
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
      ! Cancel Change
      DO resetScreen
  do Value::buttonCancel
  do SendAlert
  do Value::buttonConfirm  !1
  do Prompt::locCChargeType
  do Value::locCChargeType  !1
  do Prompt::locCRepairType
  do Value::locCRepairType  !1
  do Prompt::locChargeableJob
  do Value::locChargeableJob  !1
  do Value::locConfirmationText  !1
  do Prompt::locWChargeType
  do Value::locWChargeType  !1
  do Prompt::locWRepairType
  do Value::locWRepairType  !1
  do Value::locWarrantyJob  !1
  do Value::buttonSplitJob  !1

Value::buttonCancel  Routine
  p_web._DivHeader('SetJobType_' & p_web._nocolon('buttonCancel') & '_value',Choose(p_web.GSV('Hide:CancelButton') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CancelButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonCancel'',''setjobtype_buttoncancel_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','Cancel','Cancel','MainButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('SetJobType_' & p_web._nocolon('buttonCancel') & '_value')

Comment::buttonCancel  Routine
    loc:comment = ''
  p_web._DivHeader('SetJobType_' & p_web._nocolon('buttonCancel') & '_comment',Choose(p_web.GSV('Hide:CancelButton') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:CancelButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('SetJobType_locChargeableJob_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locChargeableJob
      else
        do Value::locChargeableJob
      end
  of lower('SetJobType_locCChargeType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locCChargeType
      else
        do Value::locCChargeType
      end
  of lower('SetJobType_locCRepairType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locCRepairType
      else
        do Value::locCRepairType
      end
  of lower('SetJobType_locWarrantyJob_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locWarrantyJob
      else
        do Value::locWarrantyJob
      end
  of lower('SetJobType_locWChargeType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locWChargeType
      else
        do Value::locWChargeType
      end
  of lower('SetJobType_locWRepairType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locWRepairType
      else
        do Value::locWRepairType
      end
  of lower('SetJobType_buttonConfirm_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonConfirm
      else
        do Value::buttonConfirm
      end
  of lower('SetJobType_buttonSplitJob_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonSplitJob
      else
        do Value::buttonSplitJob
      end
  of lower('SetJobType_buttonCancel_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonCancel
      else
        do Value::buttonCancel
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('SetJobType_form:ready_',1)
  p_web.SetSessionValue('SetJobType_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_SetJobType',0)

PreCopy  Routine
  p_web.SetValue('SetJobType_form:ready_',1)
  p_web.SetSessionValue('SetJobType_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_SetJobType',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('SetJobType_form:ready_',1)
  p_web.SetSessionValue('SetJobType_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('SetJobType:Primed',0)

PreDelete       Routine
  p_web.SetValue('SetJobType_form:ready_',1)
  p_web.SetSessionValue('SetJobType_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('SetJobType:Primed',0)
  p_web.setsessionvalue('showtab_SetJobType',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
        If (p_web.GSV('ReadOnly:ChargeableJob')= 1)
          If p_web.IfExistsValue('locChargeableJob') = 0
            p_web.SetValue('locChargeableJob','NO')
            locChargeableJob = 'NO'
          End
        End
        If (p_web.GSV('ReadOnly:WarrantyJob') = 1)
          If p_web.IfExistsValue('locWarrantyJob') = 0
            p_web.SetValue('locWarrantyJob','NO')
            locWarrantyJob = 'NO'
          End
        End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('SetJobType_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
      IF (p_web.GSV('job:Warranty_Job') <> 'YES')
          p_web.SSV('job:Warranty_Charge_Type','')
          p_web.SSV('job:Repair_Type_Warranty','')
      END
      IF (p_web.GSV('job:Chargeable_Job') <> 'YES')
          p_web.SSV('job:Charge_Type','')
          p_web.SSV('job:Repair_Type','')
      END
  
      DO deleteSessionValues
  p_web.DeleteSessionValue('SetJobType_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
  ! tab = 3
    loc:InvalidTab += 1
  ! tab = 4
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('SetJobType:Primed',0)
  p_web.StoreValue('locChargeableJob')
  p_web.StoreValue('locCChargeType')
  p_web.StoreValue('locCRepairType')
  p_web.StoreValue('locWarrantyJob')
  p_web.StoreValue('locWChargeType')
  p_web.StoreValue('locWRepairType')
  p_web.StoreValue('locErrorText')
  p_web.StoreValue('locConfirmationText')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
locClass.validateWarrantyParts      PROCEDURE()
    CODE
            
        found# = 0
        Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
        wpr:Ref_Number = p_web.GSV('job:ref_Number')
        SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
        LOOP
            IF (Access:WARPARTS.Next())
                BREAK
            END
            IF (wpr:Ref_Number <> p_web.GSV('job:ref_number'))
                BREAK
            END
            found# = 1
            BREAK
        END
        
        return found#
        
locClass.validateChargeableParts    PROCEDURE()
    CODE
        found# = 0
        Access:PARTS.ClearKey(par:Part_Number_Key)
        par:Ref_Number = p_web.gsv('job:Ref_number')
        SET(par:Part_Number_Key,par:Part_Number_Key)
        LOOP
            IF (Access:PARTS.Next())
                BREAK
            END
            IF (par:Ref_Number <> p_web.gsv('job:ref_Number'))
                BREAK
            END
            found# = 1
            BREAK
        END

        IF (p_web.gsv('job:estimate') = 'YES')
            found# = 2
        END
        
        RETURN found#
                
