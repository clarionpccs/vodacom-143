

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('WEBSERVER239.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! Window
!!! </summary>
XFiles PROCEDURE 

QuickWindow          WINDOW('Window'),AT(,,260,160),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:DEFAULT),RESIZE, |
  CENTER,GRAY,IMM,HLP('XFiles'),SYSTEM
                       BUTTON('&OK'),AT(140,138,56,18),USE(?Ok),MSG('Accept operation'),TIP('Accept Operation')
                       BUTTON('&Cancel'),AT(200,138,56,18),USE(?Cancel),MSG('Cancel Operation'),TIP('Cancel Operation')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('XFiles')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Ok
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Ok,RequestCancelled)                    ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Ok,RequestCompleted)                    ! Add the close control to the window manger
  END
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:INIDATA.Open                                      ! File INIDATA used by this procedure, so make sure it's RelationManager is open
  Relate:RTNORDER.Open                                     ! File RTNORDER used by this procedure, so make sure it's RelationManager is open
  Relate:SBO_WarrantyClaims.Open                           ! File SBO_WarrantyClaims used by this procedure, so make sure it's RelationManager is open
  Relate:SMSRECVD.Open                                     ! File SMSRECVD used by this procedure, so make sure it's RelationManager is open
  Relate:SMSText.Open                                      ! File SMSText used by this procedure, so make sure it's RelationManager is open
  Relate:STOCKALX.Open                                     ! File STOCKALX used by this procedure, so make sure it's RelationManager is open
  Relate:STOCK_ALIAS.Open                                  ! File STOCK_ALIAS used by this procedure, so make sure it's RelationManager is open
  Relate:TRADEAC2.Open                                     ! File TRADEAC2 used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('XFiles',QuickWindow)                       ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:INIDATA.Close
    Relate:RTNORDER.Close
    Relate:SBO_WarrantyClaims.Close
    Relate:SMSRECVD.Close
    Relate:SMSText.Close
    Relate:STOCKALX.Close
    Relate:STOCK_ALIAS.Close
    Relate:TRADEAC2.Close
  END
  IF SELF.Opened
    INIMgr.Update('XFiles',QuickWindow)                    ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

