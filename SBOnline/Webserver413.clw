

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER413.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
BuildStockAuditShortages PROCEDURE  (NetWebServerWorker p_web,LONG pUpdateProgress,LONG pAuditNo,*LONG pTotalLines,*LONG pTotalAudited) ! Declare Procedure
i                               LONG()
totalRecords                    LONG()
recordsRead                     LONG()
percent                         LONG()
skip LONG()

  CODE
!region Processed Code
        BHAddToDebugLog('BuildStockAuditShortages: Audit No = ' & pAuditNo)
        pTotalLines = 0
        pTotalAudited = 0
		
        Relate:STOAUDIT.Open()
        Relate:STOCK.Open()
        Relate:GENSHORT.Open()
        
        Access:STOAUDIT.ClearKey(stoa:Report_Key)
        stoa:Audit_Ref_No = pAuditNo
        SET(stoa:Report_Key,stoa:Report_Key)
        LOOP UNTIL Access:STOAUDIT.Next() <> Level:Benign
            IF (stoa:Audit_Ref_No <> pAuditNo)
                BREAK
            END ! IF
            totalRecords += 1
            IF (totalRecords > 1000)
                totalRecords = 2000
                BREAK
            END ! IF
        END ! IF
		
		! Clear Q's For Current User
        LOOP i = RECORDS(qStockAudit) TO 1 BY -1
            GET(qStockAudit,i)
            IF (pUpdateProgress = 1)
                UpdateProgress(p_web,percent,p_web.GSV('ReturnURL'),'Preparing..',1)
            END !I F
            
            IF (qStockAudit.SessionID = p_web.SessionID)
                DELETE(qStockAudit)
            END ! IF           
            
        END ! LOOP
		
        Access:STOAUDIT.ClearKey(stoa:Report_Key)
        stoa:Audit_Ref_No = pAuditNo
        SET(stoa:Report_Key,stoa:Report_Key)
        LOOP UNTIL Access:STOAUDIT.Next() <> Level:Benign
            IF (stoa:Audit_Ref_No <> pAuditNo)
                BREAK
            END ! IF
			
            IF (pUpdateProgress = 1)
                recordsRead += 1
                
                skip += 1
                IF skip > 100
                    skip = 0
                    percent = INT((recordsRead / TotalRecords) * 100)
                    IF (percent > 100)
                        recordsRead = 0
                        percent = 0
                    END ! IF
                    UpdateProgress(p_web,percent,p_web.GSV('ReturnURL'),'Build List Shortages & Excesses..',1)
                END ! IF
            END ! IF
            
            Access:STOCK.ClearKey(sto:Ref_Number_Key)
            sto:Ref_Number = stoa:Stock_Ref_No
            IF (Access:STOCK.TryFetch(sto:Ref_Number_Key))
                CYCLE
            END ! !IF
			
            IF (stoa:Confirmed = 'Y')
                pTotalAudited += 1
                pTotalLines += 1
            ELSE
                pTotalLines += 1
            END
            
            BHAddToDebugLog('BuildStockAuditShortages: Before GENSHORT = ' & sto:Part_Number)
			
            Access:GENSHORT.ClearKey(gens:Lock_Down_Key)
            gens:Audit_No = pAuditNo
            gens:Stock_Ref_No = sto:Ref_Number
            SET(gens:Lock_Down_Key,gens:Lock_Down_Key)
            LOOP UNTIL Access:GENSHORT.Next() <> Level:Benign
                IF (gens:Audit_No <> pAuditNo OR |
                    gens:Stock_Ref_No <> sto:Ref_Number)
                    BHAddToDebugLog('BuildStockAuditShortages: ADD Error = ' & ERROR())
                    BREAK
                END ! IF

                IF (gens:STock_Qty <= 0)
                    CLEAR(qStockAudit)
                    qStockAudit.SessionID = p_web.SessionID
                    qStockAudit.RecordNumber = gens:Autonumber_Field
                    qStockAudit.StoAuditRecordNumber = stoa:Internal_AutoNumber
                    qStockAudit.StockRecordNumber = sto:Ref_Number
                    qStockAudit.LinesNotAudited = pTotalLines - pTotalAudited
                    qStockAudit.PercentageAudited = (pTotalAudited / pTotalLines) * 100
                    qStockAudit.StockQty = gens:Stock_Qty
                    qStockAudit.ShortagesExcess = 'S'
                    ADD(qStockAudit)
                    BHAddToDebugLog('BuildStockAuditShortages: ADD Error = ' & ERROR())
                END ! IF
				
                IF (gens:Stock_Qty >= 0)
                    CLEAR(qStockAudit)
                    qStockAudit.SessionID = p_web.SessionID
                    qStockAudit.RecordNumber = gens:Autonumber_Field
                    qStockAudit.StoAuditRecordNumber = stoa:Internal_AutoNumber
                    qStockAudit.StockRecordNumber = sto:Ref_Number
					! qStockAudit.LinesNotAudited = locTotalLinesInAudit - locTotalAudited
					! qStockAudit.PercentageAudited = (locTotalAudited / locTotalLinesInAudit) * 100
                    qStockAudit.StockQty = gens:Stock_Qty
                    qStockAudit.ShortagesExcess = 'E'
                    ADD(qStockAudit)
                    BHAddToDebugLog('BuildStockAuditShortages: ADD Error = ' & ERROR())
                END ! IF
				
            END ! LOOP
			
        END ! LOOP

        Relate:GENSHORT.Close()
        Relate:STOCK.Close()
        Relate:STOAUDIT.Close()   
        
        BHAddToDebugLog('BuildStockAuditShortages: Records = ' & RECORDS(qStockAudit))
!endregion
