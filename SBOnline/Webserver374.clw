

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER374.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER103.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER285.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER375.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER486.INC'),ONCE        !Req'd for module callout resolution
                     END


FormGlobalIMEISearch PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
ChooseAddress        LONG                                  !
locCompanyName       STRING(30)                            !
locAddress1          STRING(30)                            !
locAddress2          STRING(30)                            !
locAddress3          STRING(30)                            !
locPostcode          STRING(30)                            !
locTelephone         STRING(30)                            !
locFaxNumber         STRING(30)                            !
locUnitType          STRING(30)                            !
locJobStatus         STRING(30)                            !
locLoanStatus        STRING(30)                            !
locExchangeStatus    STRING(30)                            !
locExchangeIMEI      STRING(30)                            !
                    MAP
DisplayJob          PROCEDURE()
                    END ! MAP
FilesOpened     Long
JOBNOTES::State  USHORT
JOBSE::State  USHORT
EXCHANGE::State  USHORT
JOBS::State  USHORT
WEBJOB::State  USHORT
SBO_OutParts::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormGlobalIMEISearch')
  loc:formname = 'FormGlobalIMEISearch_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormGlobalIMEISearch','')
    p_web._DivHeader('FormGlobalIMEISearch',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormGlobalIMEISearch',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormGlobalIMEISearch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormGlobalIMEISearch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormGlobalIMEISearch',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormGlobalIMEISearch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormGlobalIMEISearch',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(JOBNOTES)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(EXCHANGE)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(SBO_OutParts)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBNOTES)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(SBO_OutParts)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormGlobalIMEISearch_form:inited_',1)
  do RestoreMem

CancelForm  Routine
    ClearTagFile(p_web)  

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  If p_web.GSV('mobilesearch') <> 1
    loc:TabNumber += 1
  End
  If p_web.GSV('mobilesearch') = 1
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('ChooseAddress',ChooseAddress)
  p_web.SetSessionValue('locCompanyName',locCompanyName)
  p_web.SetSessionValue('locUnitType',locUnitType)
  p_web.SetSessionValue('locAddress1',locAddress1)
  p_web.SetSessionValue('locJobStatus',locJobStatus)
  p_web.SetSessionValue('locAddress2',locAddress2)
  p_web.SetSessionValue('locLoanStatus',locLoanStatus)
  p_web.SetSessionValue('locAddress3',locAddress3)
  p_web.SetSessionValue('locExchangeStatus',locExchangeStatus)
  p_web.SetSessionValue('locPostcode',locPostcode)
  p_web.SetSessionValue('locExchangeIMEI',locExchangeIMEI)
  p_web.SetSessionValue('locTelephone',locTelephone)
  p_web.SetSessionValue('locFaxNumber',locFaxNumber)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('ChooseAddress')
    ChooseAddress = p_web.GetValue('ChooseAddress')
    p_web.SetSessionValue('ChooseAddress',ChooseAddress)
  End
  if p_web.IfExistsValue('locCompanyName')
    locCompanyName = p_web.GetValue('locCompanyName')
    p_web.SetSessionValue('locCompanyName',locCompanyName)
  End
  if p_web.IfExistsValue('locUnitType')
    locUnitType = p_web.GetValue('locUnitType')
    p_web.SetSessionValue('locUnitType',locUnitType)
  End
  if p_web.IfExistsValue('locAddress1')
    locAddress1 = p_web.GetValue('locAddress1')
    p_web.SetSessionValue('locAddress1',locAddress1)
  End
  if p_web.IfExistsValue('locJobStatus')
    locJobStatus = p_web.GetValue('locJobStatus')
    p_web.SetSessionValue('locJobStatus',locJobStatus)
  End
  if p_web.IfExistsValue('locAddress2')
    locAddress2 = p_web.GetValue('locAddress2')
    p_web.SetSessionValue('locAddress2',locAddress2)
  End
  if p_web.IfExistsValue('locLoanStatus')
    locLoanStatus = p_web.GetValue('locLoanStatus')
    p_web.SetSessionValue('locLoanStatus',locLoanStatus)
  End
  if p_web.IfExistsValue('locAddress3')
    locAddress3 = p_web.GetValue('locAddress3')
    p_web.SetSessionValue('locAddress3',locAddress3)
  End
  if p_web.IfExistsValue('locExchangeStatus')
    locExchangeStatus = p_web.GetValue('locExchangeStatus')
    p_web.SetSessionValue('locExchangeStatus',locExchangeStatus)
  End
  if p_web.IfExistsValue('locPostcode')
    locPostcode = p_web.GetValue('locPostcode')
    p_web.SetSessionValue('locPostcode',locPostcode)
  End
  if p_web.IfExistsValue('locExchangeIMEI')
    locExchangeIMEI = p_web.GetValue('locExchangeIMEI')
    p_web.SetSessionValue('locExchangeIMEI',locExchangeIMEI)
  End
  if p_web.IfExistsValue('locTelephone')
    locTelephone = p_web.GetValue('locTelephone')
    p_web.SetSessionValue('locTelephone',locTelephone)
  End
  if p_web.IfExistsValue('locFaxNumber')
    locFaxNumber = p_web.GetValue('locFaxNumber')
    p_web.SetSessionValue('locFaxNumber',locFaxNumber)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormGlobalIMEISearch_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    ! Pass "mobilesearch" to show mobile or imei browse
    IF (p_web.IfExistsValue('mobilesearch'))
        p_web.StoreValue('mobilesearch')
    END ! IF    
    
    IF (p_web.GSV('mobilesearch') = 1)
        IF (p_web.GSV('GlobalMobileNumber') = '')
            CreateScript(packet,'alert("You have not entered a Mobile Number.");window.open("FormJobProgressCriteria","_self")')
            DO SendPacket
            EXIT
        END ! IF
        
        err# = 0
        ! Show error if there are no records
        Access:WEBJOB.ClearKey(wob:HeadMobileNumberKey)
        wob:HeadAccountNumber = p_web.GSV('BookingAccount')
        wob:MobileNumber = p_web.GSV('GlobalMobileNumber')
        SET(wob:HeadMobileNumberKey,wob:HeadMobileNumberKey)
        IF (Access:WEBJOB.Next() = Level:Benign)
            IF (wob:HeadAccountNumber <> p_web.GSV('BookingAccount') OR  |
                wob:MobileNumber <> p_web.GSV('GlobalMobileNumber') )
                err# = 1
                        
            END  ! IF
        ELSE
            err# = 1
        END  ! IF
        IF (err# = 1)
            CreateScript(packet,'alert("There are no jobs that match the selected Mobile Number.");window.open("FormJobProgressCriteria","_self")')
            DO SendPacket
            EXIT
        END ! IF
        
    END ! IF
    
    
      p_web.site.CancelButton.TextValue = 'Close'
      p_web.site.CancelButton.Image = 'images/psave.png'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 ChooseAddress = p_web.RestoreValue('ChooseAddress')
 locCompanyName = p_web.RestoreValue('locCompanyName')
 locUnitType = p_web.RestoreValue('locUnitType')
 locAddress1 = p_web.RestoreValue('locAddress1')
 locJobStatus = p_web.RestoreValue('locJobStatus')
 locAddress2 = p_web.RestoreValue('locAddress2')
 locLoanStatus = p_web.RestoreValue('locLoanStatus')
 locAddress3 = p_web.RestoreValue('locAddress3')
 locExchangeStatus = p_web.RestoreValue('locExchangeStatus')
 locPostcode = p_web.RestoreValue('locPostcode')
 locExchangeIMEI = p_web.RestoreValue('locExchangeIMEI')
 locTelephone = p_web.RestoreValue('locTelephone')
 locFaxNumber = p_web.RestoreValue('locFaxNumber')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferFormGlobalIMEISearch')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormGlobalIMEISearch_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormGlobalIMEISearch_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormGlobalIMEISearch_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'FormJobProgressCriteria'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormGlobalIMEISearch" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormGlobalIMEISearch" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormGlobalIMEISearch" ></input><13,10>'
  end

  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormGlobalIMEISearch">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormGlobalIMEISearch" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormGlobalIMEISearch')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
        If p_web.GSV('mobilesearch') <> 1
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Jobs Matching I.M.E.I. No') & ''''
        End
        If p_web.GSV('mobilesearch') = 1
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Jobs Matching Mobile No') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Actions') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormGlobalIMEISearch')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormGlobalIMEISearch'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormGlobalIMEISearch_BrowseGlobalIMEISearch_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormGlobalIMEISearch_BrowseMobileNoSearch_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormGlobalIMEISearch_BrowseChargeableParts_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormGlobalIMEISearch_BrowseWarrantyParts_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    ElsIf p_web.GSV('mobilesearch') <> 1
    ElsIf p_web.GSV('mobilesearch') = 1
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
          If p_web.GSV('mobilesearch') <> 1
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          End
          If p_web.GSV('mobilesearch') = 1
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab4'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormGlobalIMEISearch')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
    if p_web.GSV('mobilesearch') <> 1
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
    end
    if p_web.GSV('mobilesearch') = 1
      packet = clip(packet) & 'roundCorners(''tab4'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
  If p_web.GSV('mobilesearch') <> 1
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Jobs Matching I.M.E.I. No') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormGlobalIMEISearch_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Jobs Matching I.M.E.I. No')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Jobs Matching I.M.E.I. No')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Jobs Matching I.M.E.I. No')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Jobs Matching I.M.E.I. No')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwGlobalIMEISearch
      do Comment::brwGlobalIMEISearch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      DisplayJob()  
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnTagAll
      do Comment::btnTagAll
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnUnTagAll
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::btnUnTagAll
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab1  Routine
  If p_web.GSV('mobilesearch') = 1
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel4">'&CRLF &|
                                    '  <div id="panel4Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Jobs Matching Mobile No') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel4Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormGlobalIMEISearch_4">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Jobs Matching Mobile No')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab4" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab4">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Jobs Matching Mobile No')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Jobs Matching Mobile No')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Jobs Matching Mobile No')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwMobileSearch
      do Comment::brwMobileSearch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      DisplayJob()  
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormGlobalIMEISearch_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::ChooseAddress
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&' colspan="2">'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::ChooseAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&' colspan="2">'
      loc:columncounter += 1
      do SendPacket
      do Comment::ChooseAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locCompanyName
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locCompanyName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locCompanyName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locUnitType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locUnitType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locUnitType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAddress1
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAddress1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locAddress1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locJobStatus
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locJobStatus
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locJobStatus
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAddress2
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAddress2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locAddress2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locLoanStatus
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locLoanStatus
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locLoanStatus
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAddress3
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAddress3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locAddress3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locExchangeStatus
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locExchangeStatus
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locExchangeStatus
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locPostcode
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locPostcode
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locPostcode
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locExchangeIMEI
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locExchangeIMEI
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locExchangeIMEI
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locTelephone
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locTelephone
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locTelephone
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFaxNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locFaxNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locFaxNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwChargeableParts
      do Comment::brwChargeableParts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwWarrantyparts
      do Comment::brwWarrantyparts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab3  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Actions') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormGlobalIMEISearch_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnCosts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::btnCosts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnPrintJobCard
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::btnPrintJobCard
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnBouncerHistory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::btnBouncerHistory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnSearchUsingExchange
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::btnSearchUsingExchange
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnShowFaultDescription
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::btnShowFaultDescription
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnEngineerNotes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::btnEngineerNotes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::brwGlobalIMEISearch  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwGlobalIMEISearch',p_web.GetValue('NewValue'))
    do Value::brwGlobalIMEISearch
  Else
    p_web.StoreValue('sout:RecordNumber')
  End
    DisplayJob()  
  do SendAlert
  do Value::btnSearchUsingExchange  !1
  do Value::locAddress1  !1
  do Value::locCompanyName  !1
  do Value::locAddress2  !1
  do Value::locAddress3  !1
  do Value::locPostcode  !1
  do Value::locTelephone  !1
  do Value::locFaxNumber  !1
  do Value::locExchangeIMEI  !1
  do Value::locExchangeStatus  !1
  do Value::locJobStatus  !1
  do Value::locLoanStatus  !1
  do Value::locUnitType  !1
  do Value::brwChargeableParts  !1
  do Value::brwWarrantyparts  !1
  do Value::btnShowFaultDescription  !1
  do Value::btnEngineerNotes  !1

Value::brwGlobalIMEISearch  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseGlobalIMEISearch --
  p_web.SetValue('BrowseGlobalIMEISearch:NoForm',1)
  p_web.SetValue('BrowseGlobalIMEISearch:FormName',loc:formname)
  p_web.SetValue('BrowseGlobalIMEISearch:parentIs','Form')
  p_web.SetValue('_parentProc','FormGlobalIMEISearch')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormGlobalIMEISearch_BrowseGlobalIMEISearch_embedded_div')&'"><!-- Net:BrowseGlobalIMEISearch --></div><13,10>'
    p_web._DivHeader('FormGlobalIMEISearch_' & lower('BrowseGlobalIMEISearch') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormGlobalIMEISearch_' & lower('BrowseGlobalIMEISearch') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseGlobalIMEISearch --><13,10>'
  end
  do SendPacket

Comment::brwGlobalIMEISearch  Routine
    loc:comment = ''
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('brwGlobalIMEISearch') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnTagAll  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnTagAll',p_web.GetValue('NewValue'))
    do Value::btnTagAll
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    ! Tag All
    ClearTagFile(p_web)
  
    Access:SBO_OutParts.ClearKey(sout:PartNumberKey)
    sout:SessionID = p_web.SessionID
    SET(sout:PartNumberKey,sout:PartNumberKey)
    LOOP UNTIL Access:SBO_OutParts.Next() <> Level:Benign
        IF (sout:SessionID <> p_web.SessionID)
            BREAK
        END ! IF
        Access:TAGFILE.Clearkey(tag:keyTagged)
        tag:sessionID    = p_web.SessionID
        tag:taggedValue    = sout:PartNumber
        if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
      ! Found
            tag:tagged = 1
            access:TAGFILE.tryUpdate()
        else ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
      ! Error
            if (Access:TAGFILE.PrimeRecord() = Level:Benign)
                tag:sessionID    = p_web.sessionID
                tag:taggedValue    = sout:PartNumber
                tag:tagged = 1
                if (Access:TAGFILE.TryInsert() = Level:Benign)
              ! Inserted
                else ! if (Access:TAGFILE.TryInsert() = Level:Benign)
              ! Error
                end ! if (Access:TAGFILE.TryInsert() = Level:Benign)
            end ! if (Access:TAGFILE.PrimeRecord() = Level:Benign)
        end ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)  
    END ! LOOP
  do SendAlert
  do Value::brwGlobalIMEISearch  !1

Value::btnTagAll  Routine
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('btnTagAll') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnTagAll'',''formglobalimeisearch_btntagall_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnTagAll','Tag All','MainButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormGlobalIMEISearch_' & p_web._nocolon('btnTagAll') & '_value')

Comment::btnTagAll  Routine
    loc:comment = ''
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('btnTagAll') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnUnTagAll  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnUnTagAll',p_web.GetValue('NewValue'))
    do Value::btnUnTagAll
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    ClearTagFile(p_web)  
  do SendAlert
  do Value::brwGlobalIMEISearch  !1

Value::btnUnTagAll  Routine
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('btnUnTagAll') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnUnTagAll'',''formglobalimeisearch_btnuntagall_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnUnTagAll','Untag All','MainButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormGlobalIMEISearch_' & p_web._nocolon('btnUnTagAll') & '_value')

Comment::btnUnTagAll  Routine
    loc:comment = ''
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('btnUnTagAll') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::brwMobileSearch  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwMobileSearch',p_web.GetValue('NewValue'))
    do Value::brwMobileSearch
  Else
    p_web.StoreValue('wob:RecordNumber')
  End
    DisplayJob()  
  
  ! After Validate New Value --��
    do Value::btnSearchUsingExchange  !1
    do Value::locAddress1  !1
    do Value::locCompanyName  !1
    do Value::locAddress2  !1
    do Value::locAddress3  !1
    do Value::locPostcode  !1
    do Value::locTelephone  !1
    do Value::locFaxNumber  !1
    do Value::locExchangeIMEI  !1
    do Value::locExchangeStatus  !1
    do Value::locJobStatus  !1
    do Value::locLoanStatus  !1
    do Value::locUnitType  !1
    do Value::brwChargeableParts  !1
    do Value::brwWarrantyparts  !1
    do Value::btnShowFaultDescription  !1  
    do Value::btnEngineerNotes  !1
  do SendAlert

Value::brwMobileSearch  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseMobileNoSearch --
  p_web.SetValue('BrowseMobileNoSearch:NoForm',1)
  p_web.SetValue('BrowseMobileNoSearch:FormName',loc:formname)
  p_web.SetValue('BrowseMobileNoSearch:parentIs','Form')
  p_web.SetValue('_parentProc','FormGlobalIMEISearch')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormGlobalIMEISearch_BrowseMobileNoSearch_embedded_div')&'"><!-- Net:BrowseMobileNoSearch --></div><13,10>'
    p_web._DivHeader('FormGlobalIMEISearch_' & lower('BrowseMobileNoSearch') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormGlobalIMEISearch_' & lower('BrowseMobileNoSearch') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseMobileNoSearch --><13,10>'
  end
  do SendPacket

Comment::brwMobileSearch  Routine
    loc:comment = ''
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('brwMobileSearch') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::ChooseAddress  Routine
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('ChooseAddress') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Choose Address')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::ChooseAddress  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('ChooseAddress',p_web.GetValue('NewValue'))
    ChooseAddress = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::ChooseAddress
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('ChooseAddress',p_web.GetValue('Value'))
    ChooseAddress = p_web.GetValue('Value')
  End
    DisplayJob()  
  do Value::ChooseAddress
  do SendAlert
  do Value::locCompanyName  !1
  do Value::locAddress1  !1
  do Value::locAddress2  !1
  do Value::locAddress3  !1
  do Value::locPostcode  !1
  do Value::locTelephone  !1
  do Value::locFaxNumber  !1

Value::ChooseAddress  Routine
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('ChooseAddress') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- ChooseAddress
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('ChooseAddress')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('ChooseAddress') = 0
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''ChooseAddress'',''formglobalimeisearch_chooseaddress_value'',1,'''&clip(0)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('ChooseAddress')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','ChooseAddress',clip(0),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'ChooseAddress_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Invoice') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('ChooseAddress') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''ChooseAddress'',''formglobalimeisearch_chooseaddress_value'',1,'''&clip(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('ChooseAddress')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','ChooseAddress',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'ChooseAddress_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Collection') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('ChooseAddress') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''ChooseAddress'',''formglobalimeisearch_chooseaddress_value'',1,'''&clip(2)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('ChooseAddress')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','ChooseAddress',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'ChooseAddress_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Delivery') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormGlobalIMEISearch_' & p_web._nocolon('ChooseAddress') & '_value')

Comment::ChooseAddress  Routine
    loc:comment = ''
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('ChooseAddress') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locCompanyName  Routine
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locCompanyName') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Company Name:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locCompanyName  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locCompanyName',p_web.GetValue('NewValue'))
    locCompanyName = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locCompanyName
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locCompanyName',p_web.GetValue('Value'))
    locCompanyName = p_web.GetValue('Value')
  End

Value::locCompanyName  Routine
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locCompanyName') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locCompanyName
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locCompanyName'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormGlobalIMEISearch_' & p_web._nocolon('locCompanyName') & '_value')

Comment::locCompanyName  Routine
    loc:comment = ''
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locCompanyName') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locUnitType  Routine
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locUnitType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Unit Type:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locUnitType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locUnitType',p_web.GetValue('NewValue'))
    locUnitType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locUnitType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locUnitType',p_web.GetValue('Value'))
    locUnitType = p_web.GetValue('Value')
  End

Value::locUnitType  Routine
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locUnitType') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locUnitType
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locUnitType'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormGlobalIMEISearch_' & p_web._nocolon('locUnitType') & '_value')

Comment::locUnitType  Routine
    loc:comment = ''
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locUnitType') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locAddress1  Routine
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locAddress1') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Address:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locAddress1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAddress1',p_web.GetValue('NewValue'))
    locAddress1 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAddress1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locAddress1',p_web.GetValue('Value'))
    locAddress1 = p_web.GetValue('Value')
  End

Value::locAddress1  Routine
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locAddress1') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locAddress1
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locAddress1'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormGlobalIMEISearch_' & p_web._nocolon('locAddress1') & '_value')

Comment::locAddress1  Routine
    loc:comment = ''
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locAddress1') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locJobStatus  Routine
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locJobStatus') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Job Status:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locJobStatus  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locJobStatus',p_web.GetValue('NewValue'))
    locJobStatus = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locJobStatus
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locJobStatus',p_web.GetValue('Value'))
    locJobStatus = p_web.GetValue('Value')
  End

Value::locJobStatus  Routine
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locJobStatus') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locJobStatus
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locJobStatus'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormGlobalIMEISearch_' & p_web._nocolon('locJobStatus') & '_value')

Comment::locJobStatus  Routine
    loc:comment = ''
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locJobStatus') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locAddress2  Routine
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locAddress2') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locAddress2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAddress2',p_web.GetValue('NewValue'))
    locAddress2 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAddress2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locAddress2',p_web.GetValue('Value'))
    locAddress2 = p_web.GetValue('Value')
  End

Value::locAddress2  Routine
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locAddress2') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locAddress2
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locAddress2'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormGlobalIMEISearch_' & p_web._nocolon('locAddress2') & '_value')

Comment::locAddress2  Routine
    loc:comment = ''
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locAddress2') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locLoanStatus  Routine
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locLoanStatus') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Loan Status:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locLoanStatus  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locLoanStatus',p_web.GetValue('NewValue'))
    locLoanStatus = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locLoanStatus
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locLoanStatus',p_web.GetValue('Value'))
    locLoanStatus = p_web.GetValue('Value')
  End

Value::locLoanStatus  Routine
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locLoanStatus') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locLoanStatus
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locLoanStatus'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormGlobalIMEISearch_' & p_web._nocolon('locLoanStatus') & '_value')

Comment::locLoanStatus  Routine
    loc:comment = ''
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locLoanStatus') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locAddress3  Routine
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locAddress3') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locAddress3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAddress3',p_web.GetValue('NewValue'))
    locAddress3 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAddress3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locAddress3',p_web.GetValue('Value'))
    locAddress3 = p_web.GetValue('Value')
  End

Value::locAddress3  Routine
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locAddress3') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locAddress3
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locAddress3'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormGlobalIMEISearch_' & p_web._nocolon('locAddress3') & '_value')

Comment::locAddress3  Routine
    loc:comment = ''
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locAddress3') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locExchangeStatus  Routine
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locExchangeStatus') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Exchange Status:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locExchangeStatus  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locExchangeStatus',p_web.GetValue('NewValue'))
    locExchangeStatus = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locExchangeStatus
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locExchangeStatus',p_web.GetValue('Value'))
    locExchangeStatus = p_web.GetValue('Value')
  End

Value::locExchangeStatus  Routine
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locExchangeStatus') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locExchangeStatus
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locExchangeStatus'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormGlobalIMEISearch_' & p_web._nocolon('locExchangeStatus') & '_value')

Comment::locExchangeStatus  Routine
    loc:comment = ''
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locExchangeStatus') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locPostcode  Routine
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locPostcode') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Postcode:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locPostcode  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locPostcode',p_web.GetValue('NewValue'))
    locPostcode = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locPostcode
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locPostcode',p_web.GetValue('Value'))
    locPostcode = p_web.GetValue('Value')
  End

Value::locPostcode  Routine
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locPostcode') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locPostcode
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locPostcode'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormGlobalIMEISearch_' & p_web._nocolon('locPostcode') & '_value')

Comment::locPostcode  Routine
    loc:comment = ''
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locPostcode') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locExchangeIMEI  Routine
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locExchangeIMEI') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Exchang IMEI No:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locExchangeIMEI  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locExchangeIMEI',p_web.GetValue('NewValue'))
    locExchangeIMEI = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locExchangeIMEI
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locExchangeIMEI',p_web.GetValue('Value'))
    locExchangeIMEI = p_web.GetValue('Value')
  End

Value::locExchangeIMEI  Routine
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locExchangeIMEI') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locExchangeIMEI
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locExchangeIMEI'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormGlobalIMEISearch_' & p_web._nocolon('locExchangeIMEI') & '_value')

Comment::locExchangeIMEI  Routine
    loc:comment = ''
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locExchangeIMEI') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locTelephone  Routine
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locTelephone') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Telephone No:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locTelephone  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locTelephone',p_web.GetValue('NewValue'))
    locTelephone = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locTelephone
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locTelephone',p_web.GetValue('Value'))
    locTelephone = p_web.GetValue('Value')
  End

Value::locTelephone  Routine
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locTelephone') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locTelephone
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locTelephone'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormGlobalIMEISearch_' & p_web._nocolon('locTelephone') & '_value')

Comment::locTelephone  Routine
    loc:comment = ''
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locTelephone') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locFaxNumber  Routine
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locFaxNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Fax No:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locFaxNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFaxNumber',p_web.GetValue('NewValue'))
    locFaxNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFaxNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locFaxNumber',p_web.GetValue('Value'))
    locFaxNumber = p_web.GetValue('Value')
  End

Value::locFaxNumber  Routine
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locFaxNumber') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locFaxNumber
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locFaxNumber'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormGlobalIMEISearch_' & p_web._nocolon('locFaxNumber') & '_value')

Comment::locFaxNumber  Routine
    loc:comment = ''
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('locFaxNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::brwChargeableParts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwChargeableParts',p_web.GetValue('NewValue'))
    do Value::brwChargeableParts
  Else
    p_web.StoreValue('par:Record_Number')
  End

Value::brwChargeableParts  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('job:Chargeable_Job') <> 'YES',1,0))
  ! --- BROWSE ---  BrowseChargeableParts --
  p_web.SetValue('BrowseChargeableParts:NoForm',1)
  p_web.SetValue('BrowseChargeableParts:FormName',loc:formname)
  p_web.SetValue('BrowseChargeableParts:parentIs','Form')
  p_web.SetValue('_parentProc','FormGlobalIMEISearch')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormGlobalIMEISearch_BrowseChargeableParts_embedded_div')&'"><!-- Net:BrowseChargeableParts --></div><13,10>'
    p_web._DivHeader('FormGlobalIMEISearch_' & lower('BrowseChargeableParts') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormGlobalIMEISearch_' & lower('BrowseChargeableParts') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseChargeableParts --><13,10>'
  end
  do SendPacket

Comment::brwChargeableParts  Routine
    loc:comment = ''
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('brwChargeableParts') & '_comment',Choose(p_web.GSV('job:Chargeable_Job') <> 'YES','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Chargeable_Job') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::brwWarrantyparts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwWarrantyparts',p_web.GetValue('NewValue'))
    do Value::brwWarrantyparts
  Else
    p_web.StoreValue('wpr:Record_Number')
  End

Value::brwWarrantyparts  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('job:Warranty_Job') <> 'YES',1,0))
  ! --- BROWSE ---  BrowseWarrantyParts --
  p_web.SetValue('BrowseWarrantyParts:NoForm',1)
  p_web.SetValue('BrowseWarrantyParts:FormName',loc:formname)
  p_web.SetValue('BrowseWarrantyParts:parentIs','Form')
  p_web.SetValue('_parentProc','FormGlobalIMEISearch')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormGlobalIMEISearch_BrowseWarrantyParts_embedded_div')&'"><!-- Net:BrowseWarrantyParts --></div><13,10>'
    p_web._DivHeader('FormGlobalIMEISearch_' & lower('BrowseWarrantyParts') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormGlobalIMEISearch_' & lower('BrowseWarrantyParts') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseWarrantyParts --><13,10>'
  end
  do SendPacket

Comment::brwWarrantyparts  Routine
    loc:comment = ''
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('brwWarrantyparts') & '_comment',Choose(p_web.GSV('job:Warranty_Job') <> 'YES','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Warranty_Job') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnCosts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnCosts',p_web.GetValue('NewValue'))
    do Value::btnCosts
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnCosts  Routine
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('btnCosts') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnCosts','Costs','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('ViewCosts?' &'ViewCostsReturnURL=FormGlobalIMEISearch')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()

Comment::btnCosts  Routine
    loc:comment = ''
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('btnCosts') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnPrintJobCard  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnPrintJobCard',p_web.GetValue('NewValue'))
    do Value::btnPrintJobCard
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do SendAlert
  do Value::btnPrintJobCard  !1

Value::btnPrintJobCard  Routine
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('btnPrintJobCard') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnPrintJobCard'',''formglobalimeisearch_btnprintjobcard_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnPrintJobCard','Print Job Card','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('JobCard?' &'rnd=' & RANDOM(1,1000))) & ''','''&clip('_blank')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormGlobalIMEISearch_' & p_web._nocolon('btnPrintJobCard') & '_value')

Comment::btnPrintJobCard  Routine
    loc:comment = ''
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('btnPrintJobCard') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnBouncerHistory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnBouncerHistory',p_web.GetValue('NewValue'))
    do Value::btnBouncerHistory
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do Value::btnBouncerHistory
  do SendAlert

Value::btnBouncerHistory  Routine
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('btnBouncerHistory') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnBouncerHistory'',''formglobalimeisearch_btnbouncerhistory_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnBouncerHistory','Bouncer History','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('BouncerHistory?' &'showall=1&imei=' & p_web.GSV('GlobalIMEINumber') & '&rnd=' & RANDOM(1,1000))) & ''','''&clip('_blank')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormGlobalIMEISearch_' & p_web._nocolon('btnBouncerHistory') & '_value')

Comment::btnBouncerHistory  Routine
    loc:comment = ''
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('btnBouncerHistory') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnSearchUsingExchange  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnSearchUsingExchange',p_web.GetValue('NewValue'))
    do Value::btnSearchUsingExchange
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do Value::btnSearchUsingExchange
  do SendAlert
  do Value::brwGlobalIMEISearch  !1

Value::btnSearchUsingExchange  Routine
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('btnSearchUsingExchange') & '_value',Choose(p_web.GSV('job:Exchange_Unit_Number') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Exchange_Unit_Number') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnSearchUsingExchange'',''formglobalimeisearch_btnsearchusingexchange_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnSearchUsingExchange','Search Using Exchange IMEI','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PageProcess?' &'ProcessType=GlobalIMEISearch&ReturnURL=FormJobProgressCriteria&RedirectURL=FormGlobalIMEISearch&recno=' & p_web.GSV('sout:RecordNumber'))) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormGlobalIMEISearch_' & p_web._nocolon('btnSearchUsingExchange') & '_value')

Comment::btnSearchUsingExchange  Routine
    loc:comment = ''
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('btnSearchUsingExchange') & '_comment',Choose(p_web.GSV('job:Exchange_Unit_Number') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Exchange_Unit_Number') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnShowFaultDescription  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnShowFaultDescription',p_web.GetValue('NewValue'))
    do Value::btnShowFaultDescription
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnShowFaultDescription  Routine
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('btnShowFaultDescription') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&clip('ShowNotes(''Fault Description'',''' & BHStripNonAlphaNum(p_web.GSV('jbn:Fault_Description'),' ') & ''')')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnFaultDescription','Fault Description','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormGlobalIMEISearch_' & p_web._nocolon('btnShowFaultDescription') & '_value')

Comment::btnShowFaultDescription  Routine
    loc:comment = ''
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('btnShowFaultDescription') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnEngineerNotes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnEngineerNotes',p_web.GetValue('NewValue'))
    do Value::btnEngineerNotes
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnEngineerNotes  Routine
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('btnEngineerNotes') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&clip('ShowNotes(''Engineers Notes'',''' & BHStripNonAlphaNum(p_web.GSV('jbn:Engineers_Notes'),' ') & ''')')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnEngineerNotes','Engineers Notes','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormGlobalIMEISearch_' & p_web._nocolon('btnEngineerNotes') & '_value')

Comment::btnEngineerNotes  Routine
    loc:comment = ''
  p_web._DivHeader('FormGlobalIMEISearch_' & p_web._nocolon('btnEngineerNotes') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormGlobalIMEISearch_BrowseGlobalIMEISearch_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::brwGlobalIMEISearch
      else
        do Value::brwGlobalIMEISearch
      end
  of lower('FormGlobalIMEISearch_btnTagAll_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnTagAll
      else
        do Value::btnTagAll
      end
  of lower('FormGlobalIMEISearch_btnUnTagAll_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnUnTagAll
      else
        do Value::btnUnTagAll
      end
  of lower('FormGlobalIMEISearch_BrowseMobileNoSearch_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::brwMobileSearch
      else
        do Value::brwMobileSearch
      end
  of lower('FormGlobalIMEISearch_ChooseAddress_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::ChooseAddress
      else
        do Value::ChooseAddress
      end
  of lower('FormGlobalIMEISearch_btnPrintJobCard_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnPrintJobCard
      else
        do Value::btnPrintJobCard
      end
  of lower('FormGlobalIMEISearch_btnBouncerHistory_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnBouncerHistory
      else
        do Value::btnBouncerHistory
      end
  of lower('FormGlobalIMEISearch_btnSearchUsingExchange_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnSearchUsingExchange
      else
        do Value::btnSearchUsingExchange
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormGlobalIMEISearch_form:ready_',1)
  p_web.SetSessionValue('FormGlobalIMEISearch_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormGlobalIMEISearch',0)

PreCopy  Routine
  p_web.SetValue('FormGlobalIMEISearch_form:ready_',1)
  p_web.SetSessionValue('FormGlobalIMEISearch_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormGlobalIMEISearch',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormGlobalIMEISearch_form:ready_',1)
  p_web.SetSessionValue('FormGlobalIMEISearch_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormGlobalIMEISearch:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormGlobalIMEISearch_form:ready_',1)
  p_web.SetSessionValue('FormGlobalIMEISearch_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormGlobalIMEISearch:Primed',0)
  p_web.setsessionvalue('showtab_FormGlobalIMEISearch',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If p_web.GSV('mobilesearch') <> 1
  End
  If p_web.GSV('mobilesearch') = 1
  End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormGlobalIMEISearch_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormGlobalIMEISearch_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
  If p_web.GSV('mobilesearch') <> 1
    loc:InvalidTab += 1
  End
  ! tab = 4
  If p_web.GSV('mobilesearch') = 1
    loc:InvalidTab += 1
  End
  ! tab = 3
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormGlobalIMEISearch:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('ChooseAddress')
  p_web.StoreValue('locCompanyName')
  p_web.StoreValue('locUnitType')
  p_web.StoreValue('locAddress1')
  p_web.StoreValue('locJobStatus')
  p_web.StoreValue('locAddress2')
  p_web.StoreValue('locLoanStatus')
  p_web.StoreValue('locAddress3')
  p_web.StoreValue('locExchangeStatus')
  p_web.StoreValue('locPostcode')
  p_web.StoreValue('locExchangeIMEI')
  p_web.StoreValue('locTelephone')
  p_web.StoreValue('locFaxNumber')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('ChooseAddress',ChooseAddress) ! LONG
     p_web.SSV('locCompanyName',locCompanyName) ! STRING(30)
     p_web.SSV('locAddress1',locAddress1) ! STRING(30)
     p_web.SSV('locAddress2',locAddress2) ! STRING(30)
     p_web.SSV('locAddress3',locAddress3) ! STRING(30)
     p_web.SSV('locPostcode',locPostcode) ! STRING(30)
     p_web.SSV('locTelephone',locTelephone) ! STRING(30)
     p_web.SSV('locFaxNumber',locFaxNumber) ! STRING(30)
     p_web.SSV('locUnitType',locUnitType) ! STRING(30)
     p_web.SSV('locJobStatus',locJobStatus) ! STRING(30)
     p_web.SSV('locLoanStatus',locLoanStatus) ! STRING(30)
     p_web.SSV('locExchangeStatus',locExchangeStatus) ! STRING(30)
     p_web.SSV('locExchangeIMEI',locExchangeIMEI) ! STRING(30)
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     ChooseAddress = p_web.GSV('ChooseAddress') ! LONG
     locCompanyName = p_web.GSV('locCompanyName') ! STRING(30)
     locAddress1 = p_web.GSV('locAddress1') ! STRING(30)
     locAddress2 = p_web.GSV('locAddress2') ! STRING(30)
     locAddress3 = p_web.GSV('locAddress3') ! STRING(30)
     locPostcode = p_web.GSV('locPostcode') ! STRING(30)
     locTelephone = p_web.GSV('locTelephone') ! STRING(30)
     locFaxNumber = p_web.GSV('locFaxNumber') ! STRING(30)
     locUnitType = p_web.GSV('locUnitType') ! STRING(30)
     locJobStatus = p_web.GSV('locJobStatus') ! STRING(30)
     locLoanStatus = p_web.GSV('locLoanStatus') ! STRING(30)
     locExchangeStatus = p_web.GSV('locExchangeStatus') ! STRING(30)
     locExchangeIMEI = p_web.GSV('locExchangeIMEI') ! STRING(30)
DisplayJob          PROCEDURE()
errorOccurred   LONG()
    CODE
        p_web.SSV('Job:ViewOnly',1) ! So update buttons don't appear on parts browse
        
        LOOP 1 TIMES
            IF (p_web.GSV('mobilesearch') = 1)
                                
                Access:WEBJOB.ClearKey(wob:RecordNumberKey)
                wob:RecordNumber = p_web.GSV('wob:RecordNumber')
                IF (Access:WEBJOB.TryFetch(wob:RecordNumberKey))
                    errorOccurred = TRUE
                    BREAK
                END ! IF
                
                Access:JOBS.ClearKey(job:Ref_Number_Key)
                job:Ref_Number = wob:RefNumber
                IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
                    errorOccurred = TRUE
                    BREAK
                END ! IF
                
                p_web.FileToSessionQueue(JOBS)
                p_web.FileToSessionQueue(WEBJOB)
            ELSE ! IF
                Access:SBO_OutParts.ClearKey(sout:RecordNumberKey)
                sout:RecordNumber = p_web.GSV('sout:RecordNumber')
                IF (Access:SBO_OutParts.TryFetch(sout:RecordNumberKey))
                    errorOccurred = TRUE
                    BREAK
                END ! IF
                
                Access:JOBS.ClearKey(job:Ref_Number_Key)
                job:Ref_Number = sout:PartNumber
                IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
                    errorOccurred = TRUE
                    BREAK
                END ! IF
                
                p_web.FileToSessionQueue(JOBS)
                
                Access:WEBJOB.ClearKey(wob:RefNumberKey)
                wob:RefNumber = job:Ref_Number
                IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                    p_web.FileToSessionQueue(WEBJOB)
                END !IF

                
             END ! IF
        END ! LOOP
        
        IF (errorOccurred)
            p_web.SSV('locCompanyName','')
            p_web.SSV('locAddress1','')
            p_web.SSV('locAddress2','')
            p_web.SSV('locAddress3','')
            p_web.SSV('locPostCode','')
            p_web.SSV('locTelephone','')
            p_web.SSV('locFaxNumber','')
            p_web.SSV('locUnitType','')
            p_web.SSV('locJobStatus','')
            p_web.SSV('locLoanStatus','')
            p_web.SSV('locExchangeStatus','')
            p_web.SSV('locExchangeIMEI','')
            p_web.SSV('job:Ref_Number','')
            p_web.SSV('job:Chargeable_Job','')
            p_web.SSV('job:Warranty_Job','')
        ELSE
            p_web.SSV('locUnitType',job:Unit_Type)
            p_web.SSV('locJobStatus',job:Current_Status)
            p_web.SSV('locLoanStatus',job:Loan_Status)
            p_web.SSV('locExchangeStatus',job:Exchange_Status)
            p_web.SSV('locExchangeIMEI','')
            IF (job:Exchange_Unit_Number > 0)
                Access:EXCHANGE.TryFetch(xch:Ref_Number_Key)
                xch:Ref_Number = job:Exchange_Unit_Number
                IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                    p_web.SSV('locExchangeIMEI',xch:ESN)                        
                END ! IF
            END !I F
                
            CASE p_web.GSV('ChooseAddress')
            OF 1 ! Collection
                p_web.SSV('locCompanyName',job:Company_Name_Delivery)
                p_web.SSV('locAddress1',job:Address_Line1_Delivery)
                p_web.SSV('locAddress2',job:Address_Line2_Delivery)
                p_web.SSV('locAddress3',job:Address_Line3_Delivery)
                p_web.SSV('locPostCode',job:Postcode_Delivery)
                p_web.SSV('locTelephone',job:Telephone_Delivery)
                p_web.SSV('locFaxNumber','')
            OF 2 ! Delivery
                p_web.SSV('locCompanyName',job:Company_Name_Collection)
                p_web.SSV('locAddress1',job:Address_Line1_Collection)
                p_web.SSV('locAddress2',job:Address_Line2_Collection)
                p_web.SSV('locAddress3',job:Address_Line3_Collection)
                p_web.SSV('locPostCode',job:Postcode_Collection)
                p_web.SSV('locTelephone',job:Telephone_Collection)
                p_web.SSV('locFaxNumber','')
            ELSE ! INVOICE
                p_web.SSV('locCompanyName',job:Company_Name)
                p_web.SSV('locAddress1',job:Address_Line1)
                p_web.SSV('locAddress2',job:Address_Line2)
                p_web.SSV('locAddress3',job:Address_Line3)
                p_web.SSV('locPostCode',job:Postcode)
                p_web.SSV('locTelephone',job:Telephone_Number)
                p_web.SSV('locFaxNumber',job:Fax_Number)
            END ! IF
                
            p_web.SSV('tmp:JobNumber',job:Ref_Number)
            
            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = job:Ref_Number
            IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                p_web.FileToSessionQueue(JOBSE)
            END ! IF
            Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
            jbn:RefNumber = job:Ref_Number
            IF (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)
                p_web.FileToSessionQueue(JOBNOTES)
            END !I F

        END ! IF
        
