

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER061.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
NextWaybillNumber    PROCEDURE                             ! Declare Procedure
locKeyField          LONG                                  !
locWaybillNumber     LONG                                  !
WAYBILLS::State  USHORT
FilesOpened     BYTE(0)

  CODE
    DO OpenFiles
    DO SaveFiles
    
    ! Remove blanks
    Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
    way:WayBillNumber = 0
    SET(way:WayBillNumberKey,way:WayBillNumberKey)
    LOOP UNTIL Access:WAYBILLS.Next()
        IF (way:WayBillNumber <> 0)
            BREAK
        END
        Relate:WAYBILLS.delete(0)
    END
    
    locWaybillNumber = 0
    locKeyField = 0
    LOOP
        Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
        way:WayBillNumber = 99999999
        SET(way:WayBillNumberKey,way:WayBillNumberKey)
        LOOP 
            IF (Access:WAYBILLS.Previous())
                locKeyfield = 1
                BREAK
            END
            locKeyfield = way:WayBillNumber + 1
            BREAK
        END
        
        Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
        way:WayBillNumber = locKeyField
        IF (Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign)
            ! Double check the number doesn't already exists
            IF locKeyField = 1
                
                BREAK
            END
            CYCLE
        ELSE
            ! Doesn't exist. Add it
            IF (Access:WAYBILLS.PrimeRecord() = Level:Benign)
                way:WayBillNumber = locKeyField
                IF (Access:WAYBILLS.TryInsert() = Level:Benign)
                    locWaybillNumber = way:WayBillNumber
                    BREAK
                ELSE
                    Access:WAYBILLS.CancelAutoInc()
                END
            END
        END
       
    END
    
    DO RestoreFiles
    DO CloseFiles
    
    RETURN locWayBillNumber
    
    
SaveFiles  ROUTINE
  WAYBILLS::State = Access:WAYBILLS.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF WAYBILLS::State <> 0
    Access:WAYBILLS.RestoreFile(WAYBILLS::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:WAYBILLS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:WAYBILLS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:WAYBILLS.Close
     FilesOpened = False
  END
