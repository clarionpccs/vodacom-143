

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER058.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
JobInUse             PROCEDURE  (fSessionValue)            ! Declare Procedure

  CODE
    rtnValue# = 0
    pointer# = Pointer(JOBS)
    Hold(JOBS,1)
    Get(JOBS,pointer#)
    if (errorcode()  = 43)
        rtnValue# = 1
    else
        relate:JOBSLOCK.open()
        
        ! First step. Remove any blanks from the file
        Access:JOBSLOCK.ClearKey(lock:JobNumberKey)
        lock:JobNumber = 0
        SET(lock:JobNumberKey,lock:JobNumberKey)
        LOOP
            IF (Access:JOBSLOCK.Next())
                BREAK
            END
            IF (lock:JobNumber <> 0)
                BREAK
            END
            Access:JOBSLOCK.DeleteRecord(0)
        END
        
        access:JOBSLOCK.clearkey(lock:jobNumberKey)
        lock:jobNUmber = job:ref_number
        if (access:JOBSLOCK.tryfetch(lock:jobNumberKey) = level:benign)
            if (lock:SessionValue <> fSessionValue)
                if (TODAY() = lock:DateLocked and clock() < (lock:timelocked + 60000)) ! 10 Minutes Time Out
                ! If it's the same user then there's no need to give the locked error?!
                    rtnValue# = 1
                end
                
            end
        end
        relate:JOBSLOCK.close()
        

    end ! if (errorcode()  = 43)
    return rtnValue#
