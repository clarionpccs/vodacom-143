

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('WEBSERVER297.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
MultipleDespatchNote PROCEDURE (NetWebServerWorker p_web)

  ! The NetTalk Extension to ABC report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
Webmaster_Group      GROUP,PRE(tmp)                        !
Ref_Number           STRING(20)                            !
                     END                                   !
save_jea_id          USHORT,AUTO                           !
tmp:PrintedBy        STRING(255)                           !
save_jpt_id          USHORT,AUTO                           !
print_group          QUEUE,PRE(prngrp)                     !
esn                  STRING(30)                            !
msn                  STRING(30)                            !
job_number           REAL                                  !
                     END                                   !
Invoice_Address_Group GROUP,PRE(INVGRP)                    !
Postcode             STRING(15)                            !
Company_Name         STRING(30)                            !
Address_Line1        STRING(30)                            !
Address_Line2        STRING(30)                            !
Address_Line3        STRING(30)                            !
Telephone_Number     STRING(15)                            !
Fax_Number           STRING(15)                            !
                     END                                   !
Delivery_Address_Group GROUP,PRE(DELGRP)                   !
Postcode             STRING(15)                            !
Company_Name         STRING(30)                            !
Address_Line1        STRING(30)                            !
Address_Line2        STRING(30)                            !
Address_Line3        STRING(30)                            !
Telephone_Number     STRING(15)                            !
Fax_Number           STRING(15)                            !
                     END                                   !
Unit_Details_Group   GROUP,PRE(UNIGRP)                     !
Model_Number         STRING(30)                            !
Manufacturer         STRING(30)                            !
ESN                  STRING(16)                            !
MSN                  STRING(16)                            !
CRepairType          STRING(30)                            !
WRepairType          STRING(30)                            !
                     END                                   !
Unit_Ref_Number_Temp REAL                                  !
Despatch_Type_Temp   STRING(3)                             !
Invoice_Account_Number_Temp STRING(15)                     !
Delivery_Account_Number_Temp STRING(15)                    !
count_temp           REAL                                  !
model_number_temp    STRING(50)                            !
tmp:DefaultTelephone STRING(20)                            !
tmp:DefaultFax       STRING(20)                            !
tmp:DefaultEmailAddress STRING(255)                        !
tmp:Courier          STRING(30)                            !
tmp:ConsignmentNumber STRING(30)                           !
tmp:DespatchBatchNumber STRING(30)                         !
tmp:AccountNumber    STRING(30)                            !
tmp:Type             STRING(3)                             !
Process:View         VIEW(COURIER)
                       PROJECT(cou:Courier)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

report               REPORT,AT(396,3865,7521,6490),PRE(rpt),PAPER(PAPER:A4),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(375,823,7521,2542),USE(?unnamed)
                         STRING('Courier:'),AT(4896,104),USE(?String23),FONT(,8),TRN
                         STRING(@s30),AT(6042,104),USE(tmp:Courier),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),LEFT,TRN
                         STRING('Consignment No:'),AT(4896,260),USE(?String27),FONT(,8),TRN
                         STRING(@s30),AT(6042,260),USE(tmp:ConsignmentNumber),FONT('Arial',8,,FONT:bold,CHARSET:ANSI), |
  LEFT,TRN
                         STRING('Despatch Date:'),AT(4896,417),USE(?String27:2),FONT(,8),TRN
                         STRING('Despatch Batch No:'),AT(4896,729,990,208),USE(?String28:2),FONT(,8),TRN
                         STRING(@s30),AT(6042,729),USE(tmp:DespatchBatchNumber),FONT('Arial',8,,FONT:bold,CHARSET:ANSI), |
  LEFT,TRN
                         STRING('Page Number:'),AT(4896,885),USE(?String62),FONT('Arial',8,,FONT:regular,CHARSET:ANSI), |
  TRN
                         STRING(@n3),AT(6042,885),USE(?ReportPageNo),FONT(,8,,FONT:bold),PAGENO,TRN
                         STRING(@s30),AT(4073,1354),USE(INVGRP:Company_Name),FONT('Arial',8,,FONT:regular,CHARSET:ANSI), |
  LEFT,TRN
                         STRING(@s30),AT(4073,1510),USE(INVGRP:Address_Line1),FONT('Arial',8,,FONT:regular,CHARSET:ANSI), |
  LEFT,TRN
                         STRING('Account No:'),AT(208,2292),USE(?String38),FONT(,8),TRN
                         STRING(@s15),AT(885,2292),USE(GLO:Select1),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),LEFT, |
  TRN
                         STRING('Tel:'),AT(4073,2135),USE(?String36:2),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),LEFT, |
  TRN
                         STRING('Fax:'),AT(5521,2135),USE(?String37:2),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),LEFT, |
  TRN
                         STRING(@s15),AT(4375,2135),USE(INVGRP:Telephone_Number),FONT('Arial',8,,FONT:regular,CHARSET:ANSI), |
  LEFT,TRN
                         STRING(@s15),AT(4688,2292),USE(GLO:Select1,,?glo:select1:2),FONT('Arial',8,,FONT:regular,CHARSET:ANSI), |
  LEFT,TRN
                         STRING('Account No:'),AT(4073,2292),USE(?String38:2),FONT(,8),TRN
                         STRING(@s15),AT(5833,2135),USE(INVGRP:Fax_Number),FONT('Arial',8,,FONT:regular,CHARSET:ANSI), |
  LEFT,TRN
                         STRING(@s30),AT(4073,1667),USE(INVGRP:Address_Line2),FONT('Arial',8,,FONT:regular,CHARSET:ANSI), |
  LEFT,TRN
                         STRING(@s30),AT(4073,1823),USE(INVGRP:Address_Line3),FONT('Arial',8,,FONT:regular,CHARSET:ANSI), |
  LEFT,TRN
                         STRING('Tel:'),AT(208,2135),USE(?String36),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),LEFT, |
  TRN
                         STRING('Fax:'),AT(1719,2135),USE(?String37),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),LEFT, |
  TRN
                         STRING(@s15),AT(4073,1979),USE(INVGRP:Postcode),FONT('Arial',8,,FONT:regular,CHARSET:ANSI), |
  LEFT,TRN
                         STRING('dd/mm/yyyy'),AT(6042,417),USE(?ReportDateStamp),FONT(,8,,FONT:bold),TRN
                       END
detail                 DETAIL,AT(0,0,,146),USE(?detailband)
                         STRING(@s30),AT(156,0),USE(prngrp:esn),FONT('Arial',7,,FONT:regular,CHARSET:ANSI),LEFT,TRN
                         STRING(@s20),AT(3021,0),USE(UNIGRP:CRepairType),FONT('Arial',7,,FONT:regular,CHARSET:ANSI), |
  LEFT,TRN
                         STRING(@s30),AT(1042,0),USE(prngrp:msn),FONT('Arial',7,,FONT:regular,CHARSET:ANSI),LEFT,TRN
                         STRING(@s3),AT(5729,0),USE(Despatch_Type_Temp),FONT('Arial',7,,FONT:regular,CHARSET:ANSI), |
  LEFT,TRN
                         STRING(@s25),AT(6042,0),USE(job:Order_Number),FONT('Arial',7,,,CHARSET:ANSI),LEFT,TRN
                         STRING(@s20),AT(4115,0),USE(UNIGRP:WRepairType),FONT('Arial',7,,FONT:regular,CHARSET:ANSI), |
  LEFT,TRN
                         STRING(@s20),AT(1927,0),USE(UNIGRP:Model_Number),FONT('Arial',7,,FONT:regular,CHARSET:ANSI), |
  LEFT,TRN
                         STRING(@s20),AT(5208,0),USE(tmp:Ref_Number),FONT('Arial',7,,FONT:regular,CHARSET:ANSI),LEFT, |
  TRN
                       END
Totals                 DETAIL,AT(0,0,,354),USE(?Totals)
                         LINE,AT(198,52,7135,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('Total Lines:'),AT(260,104,677,156),USE(?String59),FONT('Arial',8,,FONT:bold,CHARSET:ANSI), |
  LEFT,TRN
                         STRING(@s8),AT(1042,104),USE(count_temp),FONT(,8,,FONT:bold),TRN
                       END
                       FOOTER,AT(365,10344,7521,854),USE(?unnamed:2)
                         LINE,AT(208,52,7135,0),USE(?Line1:2),COLOR(COLOR:Black)
                         TEXT,AT(208,104,7135,677),USE(stt:Text),FONT(,8)
                       END
                       FORM,AT(385,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTDET.GIF'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),FONT(,16,,FONT:bold),LEFT,TRN
                         STRING('MULTIPLE DESPATCH NOTE'),AT(4271,0,3177,260),USE(?string20),FONT(,16,,FONT:bold),RIGHT, |
  TRN
                         STRING(@s30),AT(156,260,3844,156),USE(def:Address_Line1),FONT(,9),TRN
                         STRING(@s30),AT(156,417,3844,156),USE(def:Address_Line2),FONT(,9),TRN
                         STRING(@s30),AT(156,573,3844,156),USE(def:Address_Line3),FONT(,9),TRN
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),FONT(,9),TRN
                         STRING(@s255),AT(521,1354,3542,208),USE(tmp:DefaultEmailAddress),FONT(,9),TRN
                         STRING('Tel:'),AT(156,1042),USE(?telephone),FONT(,9),TRN
                         STRING(@s20),AT(521,1042),USE(tmp:DefaultTelephone),FONT(,9),TRN
                         STRING('Fax:'),AT(156,1198),USE(?fax),FONT(,9),TRN
                         STRING(@s20),AT(521,1198),USE(tmp:DefaultFax),FONT(,9),TRN
                         STRING('INVOICE ADDRESS'),AT(208,1510),USE(?String29),FONT(,9,,FONT:bold),TRN
                         STRING('Email:'),AT(156,1354),USE(?Email),FONT(,9),TRN
                         STRING('DELIVERY ADDRESS'),AT(4063,1510),USE(?String29:2),FONT(,9,,FONT:bold),TRN
                         STRING('Model Number'),AT(1938,3177),USE(?string44),FONT(,7,,FONT:bold),TRN
                         STRING('I.M.E.I. Number.'),AT(156,3177),USE(?string25),FONT('Arial',7,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING('M.S.N.'),AT(1052,3177),USE(?string25:2),FONT('Arial',7,,FONT:bold,CHARSET:ANSI),TRN
                         STRING('Type'),AT(5729,3177),USE(?string25:3),FONT('Arial',7,,FONT:bold,CHARSET:ANSI),TRN
                         STRING('Order Number'),AT(6052,3177),USE(?string25:5),FONT('Arial',7,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING('War Repair Type'),AT(4125,3177),USE(?string44:3),FONT(,7,,FONT:bold),TRN
                         STRING('Char Repair Type'),AT(3031,3177),USE(?string44:2),FONT(,7,,FONT:bold),TRN
                         STRING('Job No'),AT(5219,3177),USE(?string25:4),FONT('Arial',7,,FONT:bold,CHARSET:ANSI),TRN
                       END
                     END
!*** PDF-Tools v4.1 Start (All)
!--- ClarioNET NOT Enabled
!--- PDFXTABCTemplate: True  Level: 4
!--- PDFXTABCTemplate: RPM: False
!--- PDFXTABCTemplate: Icetips: False
PDFXTR1               PDFXToolsReportClass                 !  Added by PDF-Tools Templates v4.1
PDFXTR1:rtn           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXTR1:vpf           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Orientation      BYTE(0)                      !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:TopMargin        REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:BottomMargin     REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:LeftMargin       REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:RightMargin      REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Selection        STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PDFXT4:Papers:Queue           QUEUE,PRE()                  !  Added by PDF-Tools Templates v4.1
PLQ:Description                 STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PLQ:PaperCode                   BYTE                       !  Added by PDF-Tools Templates v4.1
                              END                          !  Added by PDF-Tools Templates v4.1
!*** PDF-Tools v4.1 End
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepStringClass                       ! Progress Manager
Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('MultipleDespatchNote')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
        tmp:ConsignmentNumber = p_web.GSV('locWaybillNumber')
        tmp:DespatchBatchNumber = ''
        tmp:Courier = p_web.GSV('MultipleDespatch:Courier')
        tmp:AccountNumber = p_web.GSV('MultipleDespatch:AccountNumber') 
        tmp:Type = p_web.GSV('MultipleDespatch:Type')
  Relate:COURIER.SetOpenRelated()
  Relate:COURIER.Open                                      ! File COURIER used by this procedure, so make sure it's RelationManager is open
  Relate:DEFAULTS.Open                                     ! File DEFAULTS used by this procedure, so make sure it's RelationManager is open
  Relate:DEFPRINT.Open                                     ! File DEFPRINT used by this procedure, so make sure it's RelationManager is open
  Relate:EXCHANGE.Open                                     ! File EXCHANGE used by this procedure, so make sure it's RelationManager is open
  Relate:MULDESP.SetOpenRelated()
  Relate:MULDESP.Open                                      ! File MULDESP used by this procedure, so make sure it's RelationManager is open
  Relate:STANTEXT.Open                                     ! File STANTEXT used by this procedure, so make sure it's RelationManager is open
  Relate:WEBJOB.Open                                       ! File WEBJOB used by this procedure, so make sure it's RelationManager is open
  Access:MULDESPJ.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:USERS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:LOAN.UseFile                                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBS.UseFile                                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
        !printed by
        set(defaults)
        access:defaults.next()
  
        access:users.clearkey(use:User_Code_Key)
        use:User_Code =p_web.GSV('BookingUserCode') ! ! Do not use Global Vars!! (DBH: 21/11/2014) glo:password
        access:users.fetch(use:User_Code_Key)
        tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  
        !Hide Despatch Address
        If HideDespAdd# = 1
            Settarget(Report)
            ?def:User_Name{prop:Hide} = 1
            ?def:Address_Line1{prop:Hide} = 1
            ?def:Address_Line2{prop:Hide} = 1
            ?def:Address_Line3{prop:Hide} = 1
            ?def:Postcode{prop:hide} = 1
            ?tmp:DefaultTelephone{prop:hide} = 1
            ?tmp:DefaultFax{prop:hide} = 1
            ?telephone{prop:hide} = 1
            ?fax{prop:hide} = 1
            ?email{prop:hide} = 1
            ?tmp:DefaultEmailAddress{prop:Hide} = 1
            Settarget()
        End!If HideDespAdd# = 1
        HideDespAdd# = 0
  
        If tmp:Type = 'TRA'
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number  = tmp:AccountNumber
            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Found
                INVGRP:Postcode           = tra:Postcode
                INVGRP:Company_Name       = tra:Company_Name
                INVGRP:Address_Line1      = tra:Address_Line1
                INVGRP:Address_Line2      = tra:Address_Line2
                INVGRP:Address_Line3      = tra:Address_Line3
                INVGRP:Telephone_Number   = tra:Telephone_Number
                INVGRP:Fax_Number         = tra:Fax_Number
                DELGRP:Postcode           = tra:postcode
                DELGRP:Company_Name       = tra:company_name
                DELGRP:Address_Line1      = tra:address_line1
                DELGRP:Address_Line2      = tra:address_line2
                DELGRP:Address_Line3      = tra:address_line3
                DELGRP:Telephone_Number   = tra:telephone_number
                DELGRP:Fax_Number         = tra:fax_number
  
            Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Error
            End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        Else !tmp:Type = 'TRA'
    
            access:subtracc.clearkey(sub:account_number_key)
            sub:account_number = tmp:AccountNumber
            if access:subtracc.fetch(sub:account_number_key) = level:benign
                access:tradeacc.clearkey(tra:account_number_key)
                tra:account_number = sub:main_account_number
                if access:tradeacc.fetch(tra:account_number_key) = level:benign
                    if tra:invoice_sub_accounts = 'YES'
                        If sub:HideDespAdd = 1
                            HideDespAdd# = 1
                        End!If sub:HideDespAdd = 1
                    Else
                        If tra:HideDespAdd = 1
                            HideDespAdd# = 1
                        End!If sub:HideDespAdd = 1
                    End
                    if tra:use_sub_accounts = 'YES'
                        DELGRP:Postcode           = sub:postcode
                        DELGRP:Company_Name       = sub:company_name
                        DELGRP:Address_Line1      = sub:address_line1
                        DELGRP:Address_Line2      = sub:address_line2
                        DELGRP:Address_Line3      = sub:address_line3
                        DELGRP:Telephone_Number   = sub:telephone_number
                        DELGRP:Fax_Number         = sub:fax_number
                        If tra:invoice_sub_accounts = 'YES'
                            INVGRP:Postcode           = sub:Postcode
                            INVGRP:Company_Name       = sub:Company_Name
                            INVGRP:Address_Line1      = sub:Address_Line1
                            INVGRP:Address_Line2      = sub:Address_Line2
                            INVGRP:Address_Line3      = sub:Address_Line3
                            INVGRP:Telephone_Number   = sub:Telephone_Number
                            INVGRP:Fax_Number         = sub:Fax_Number
                        Else
                            INVGRP:Postcode           = tra:Postcode
                            INVGRP:Company_Name       = tra:Company_Name
                            INVGRP:Address_Line1      = tra:Address_Line1
                            INVGRP:Address_Line2      = tra:Address_Line2
                            INVGRP:Address_Line3      = tra:Address_Line3
                            INVGRP:Telephone_Number   = tra:Telephone_Number
                            INVGRP:Fax_Number         = tra:Fax_Number
                        End!If tra:invoice_sub_accounts = 'YES'
                    else!if tra:use_sub_accounts = 'YES'
                        INVGRP:Postcode           = tra:Postcode
                        INVGRP:Company_Name       = tra:Company_Name
                        INVGRP:Address_Line1      = tra:Address_Line1
                        INVGRP:Address_Line2      = tra:Address_Line2
                        INVGRP:Address_Line3      = tra:Address_Line3
                        INVGRP:Telephone_Number   = tra:Telephone_Number
                        INVGRP:Fax_Number         = tra:Fax_Number
                        DELGRP:Postcode           = tra:postcode
                        DELGRP:Company_Name       = tra:company_name
                        DELGRP:Address_Line1      = tra:address_line1
                        DELGRP:Address_Line2      = tra:address_line2
                        DELGRP:Address_Line3      = tra:address_line3
                        DELGRP:Telephone_Number   = tra:telephone_number
                        DELGRP:Fax_Number         = tra:fax_number
                    end!if tra:use_sub_accounts = 'YES'
                end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
            end!if access:subtracc.fetch(sub:account_number_key) = level:benign
        End !tmp:Type = 'TRA'
        access:stantext.clearkey(stt:description_key)
        stt:description = 'DESPATCH NOTE'
        access:stantext.fetch(stt:description_key)
  
        !Hide Despatch Address
        If HideDespAdd# = 1
            Settarget(Report)
            ?def:User_Name{prop:Hide} = 1
            ?def:Address_Line1{prop:Hide} = 1
            ?def:Address_Line2{prop:Hide} = 1
            ?def:Address_Line3{prop:Hide} = 1
            ?def:Postcode{prop:hide} = 1
            ?tmp:DefaultTelephone{prop:hide} = 1
            ?tmp:DefaultFax{prop:hide} = 1
            ?telephone{prop:hide} = 1
            ?fax{prop:hide} = 1
            ?email{prop:hide} = 1
            tmp:DefaultEmailAddress{prop:Hide} = 1
            Settarget()
        End!If HideDespAdd# = 1
  
  
        !Alternative Contact Nos
        UseHead# = 0
        If tmp:Type = 'TRA'
            UseHead# = 1
        End !tmp:Type = 'TRA'
        Case VodacomClass.UseAlternativeContactNumbers(tmp:AccountNumber,useHead#)
        Of 1
            tmp:DefaultEmailAddress = tra:AltEmailAddress
            tmp:DefaultTelephone    = tra:AltTelephoneNumber
            tmp:DefaultFax          = tra:AltFaxNumber
        Of 2
            tmp:DefaultEmailAddress = sub:AltEmailAddress
            tmp:DefaultTelephone    = sub:AltTelephoneNumber
            tmp:DefaultFax          = sub:AltFaxNumber
        Else !UseAlternativeContactNos(job_ali:Account_Number)
            tmp:DefaultEmailAddress = def:EmailAddress
        End !UseAlternativeContactNos(job_ali:Account_Number)
  Do DefineListboxStyle
  INIMgr.Fetch('MultipleDespatchNote',ProgressWindow)      ! Restore window settings from non-volatile store
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    loc:pdfname = '.\$$$' & format(random(1,99999),@n05) &'.pdf'
    ProgressWindow{prop:hide} = 1
  End
  ProgressMgr.Init(ScrollSort:AllowAlpha+ScrollSort:AllowNumeric,ScrollBy:RunTime)
  ThisReport.Init(Process:View, Relate:COURIER, ?Progress:PctText, Progress:Thermometer, ProgressMgr, cou:Courier)
  ThisReport.CaseSensitiveValue = FALSE
  ThisReport.AddSortOrder(cou:Courier_Key)
  ThisReport.AddRange(cou:Courier,tmp:Courier)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:COURIER.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      Loc:html = '<script type="text/javascript">alert('''&clip('No Records')&''');top.close();</script>'
      p_web.ParseHTML(loc:html)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:COURIER.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:EXCHANGE.Close
    Relate:MULDESP.Close
    Relate:STANTEXT.Close
    Relate:WEBJOB.Close
  END
  IF SELF.Opened
    INIMgr.Update('MultipleDespatchNote',ProgressWindow)   ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.OpenReport()
  !*** PDF-Tools v4.1 Start (ABC)
  IF ~ReturnValue                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.Init(report, loc:PDFName,,PDFA_OFF)            !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPaper(CHOOSE(SYSTEM{PROP:Autopaper}=True,report{PROPPRINT:Paper},PAPER:USER), CHOOSE(report{PROP:Thous}=True,PROP:Thous,CHOOSE(report{PROP:MM}=True,PROP:MM,CHOOSE(report{PROP:Points}=True,PROP:Points,0))), report{PROPPRINT:PaperWidth}, report{PROPPRINT:PaperHeight}, report{PROP:Landscape}) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetCompression(TRUE,TRUE,FALSE,COMPRTYPE_C_AUTO,75,COMPRTYPE_I_AUTO,COMPRTYPE_M_AUTO) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetAuthor('ServiceBase 3g')          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetTitle('Despatch Note')            !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetSubject('ServiceBase 3g')         !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPdfCreator('ServiceBase 3g')      !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetKeywords()                        !  Added by PDF-Tools Templates v4.1
    PDFXTR1.EnableLinkAnalyzer(True)             !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetSpecVersion(SPECVERSION_15,TRUE)  !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPageLayout(PAGELAYOUT_SINGLEPAGE) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPageMode(PAGEMODE_NONE)           !  Added by PDF-Tools Templates v4.1
    IF PAGEMODE_NONE = PAGEMODE_FULLSCREEN       !  Added by PDF-Tools Templates v4.1
      PDFXTR1:vpf = VP_FSPM_NONE                 !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
      PDFXTR1:vpf = 0                            !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDETOOLBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDEMENUBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDEWINDOWUI). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_FITWINDOW). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_CENTERWINDOW). !  Added by PDF-Tools Templates v4.1
      IF TRUE                                     THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_DISPLAYDOCTITLE). !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetViewerPrefs(PDFXTR1:vpf)          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPreview(False)                    !  Added by PDF-Tools Templates v4.1
  END
  !*** PDF-Tools v4.1 End
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@d06)
  END
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeCloseEvent()
  !*** PDF-Tools v4.1 Start (ABC)
  PDFXTR1.Kill()
  !*** PDF-Tools v4.1 End
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  Free(print_group)
  Clear(print_group)
  
  !  Loop x# = 1 To Records(glo:q_jobnumber)
  !      Get(glo:q_jobnumber,x#)
  !  
  !      access:jobs.clearkey(job:ref_number_key)
  !      job:ref_number = glo:job_number_pointer
  !      if access:jobs.fetch(job:ref_number_key) = Level:Benign
  !          prngrp:esn        = job:esn
  !          prngrp:msn        = job:msn
  !          prngrp:job_number = job:ref_number
  !          Add(print_group)
  !      End!if access:jobs.fetch(job:ref_number_key) = Level:Benign
  !  End!Loop x# = 1 To Records(glo:q_jobnumber)
        
        ! we are assuming that this document is only going to be used for multiple despatch!!
        Access:MULDESPJ.ClearKey(mulj:JobNumberKey)
        mulj:RefNumber = p_web.GSV('muld:RecordNumber')
        SET(mulj:JobNumberKey,mulj:JobNumberKey)
        LOOP UNTIL Access:MULDESPJ.Next()
            IF (mulj:RefNumber <> p_web.GSV('muld:RecordNumber'))
                BREAK
           END
            prngrp:esn        = mulj:IMEINumber
            prngrp:msn        = mulj:MSN
            prngrp:job_number = mulj:JobNumber
            Add(print_group)
            !STOP(mulj:JobNumber)
        END !LOOP
        
  
  Sort(print_group,prngrp:esn,prngrp:msn)
  
  Loop x# = 1 To Records(print_group)
      Get(print_group,x#)
      access:jobs.clearkey(job:ref_number_key)
      job:ref_number = prngrp:job_number
      if access:jobs.fetch(job:ref_number_key) = Level:Benign
          If job:Chargeable_Job = 'YES'
              unigrp:CRepairType = job:Repair_Type
          Else !If job:Chargeable_Job = 'YES'
              unigrp:CRepairType  = ''
          End !If job:Chargeable_Job = 'YES'
          If job:Warranty_job = 'YES'
              unigrp:WRepairType = job:Repair_Type_Warranty
          Else !If job:Warranty_job = 'YES'
              unigrp:WRepairType = ''
          End !If job:Warranty_job = 'YES'
          If JOB:Consignment_Number = tmp:ConsignmentNumber
              INVGRP:Postcode         = job:postcode
              INVGRP:Company_Name     = job:company_name
              INVGRP:Address_Line1    = job:address_line1
              INVGRP:Address_Line2    = job:address_line2
              INVGRP:Address_Line3    = job:address_line3
              INVGRP:Telephone_Number = job:telephone_number
              INVGRP:Fax_Number       = job:fax_number
              UNIGRP:Model_Number     = job:Model_Number
              UNIGRP:Manufacturer     = job:manufacturer
              UNIGRP:ESN              = job:esn
              UNIGRP:MSN              = job:msn
              despatch_Type_temp  = 'JOB'
          End!If JOB:Incoming_Consignment_Number = glo:select2
          If JOB:Exchange_Consignment_Number = tmp:ConsignmentNumber
              access:exchange.clearkey(xch:ref_number_key)
              xch:ref_number = job:exchange_unit_number
              if access:exchange.fetch(xch:ref_number_key) = Level:Benign
                  UNIGRP:Model_Number = XCH:Model_Number
                  UNIGRP:Manufacturer = xch:manufacturer
                  UNIGRP:ESN          = xch:esn
                  UNIGRP:MSN          = xch:msn
              end
              despatch_Type_temp  = 'EXC'
          End!If JOB:Exchange_Consignment_Number = glo:select2
          If JOB:Loan_Consignment_Number = tmp:ConsignmentNumber
              access:loan.clearkey(loa:ref_number_key)
              loa:ref_number = job:loan_unit_number
              if access:loan.fetch(loa:ref_number_key) = Level:Benign
                  UNIGRP:Model_Number = loa:Model_Number
                  UNIGRP:Manufacturer = loa:manufacturer
                  UNIGRP:ESN          = loa:esn
                  UNIGRP:MSN          = loa:msn
              end
              despatch_Type_temp  = 'LOA'
          End!If JOB:Exchange_Consignment_Number = glo:select2
          Print(Rpt:detail)
      end!if access:jobs.fetch(job:ref_number_key) = Level:Benign
  
  End!Loop x# = 1 To Records(print_group)
  count_temp = Records(print_group)
  Print(rpt:totals)
  
  setcursor()
  ReturnValue = PARENT.TakeRecord()
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  !*** PDF-Tools v4.1 Start (ABC)
  ReturnValue = False                            !  Added by PDF-Tools Templates v4.1
    PDFXTR1:rtn = PDFXTR1.Generate(SELF.ImageQueue,True, , ) !  Added by PDF-Tools Templates v4.1
    IF PDFXTR1:rtn THEN                          !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
  RETURN ReturnValue
  !*** PDF-Tools v4.1 End
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
  RETURN ReturnValue

