

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER040.INC'),ONCE        !Local module procedure declarations
                     END


InsertJob_Finished   PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:AuditNotes       STRING(255)                           !tmp:AuditNotes
tmp:FoundAccessory   STRING(30)                            !Found Accessory
FilesOpened     Long
JOBACC::State  USHORT
JOBS::State  USHORT
WEBJOB::State  USHORT
JOBNOTES::State  USHORT
JOBSE::State  USHORT
JOBSE2::State  USHORT
JOBSOBF::State  USHORT
AUDIT::State  USHORT
WEBJOBNO::State  USHORT
JOBOUTFL::State  USHORT
JOBSTAGE::State  USHORT
STAHEAD::State  USHORT
STATUS::State  USHORT
SUBTRACC::State  USHORT
TRADEACC::State  USHORT
TRANTYPE::State  USHORT
SUBEMAIL::State  USHORT
TRAEMAIL::State  USHORT
STARECIP::State  USHORT
DEFAULTS::State  USHORT
WAYBAWT::State  USHORT
AUDITE::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('InsertJob_Finished')
  loc:formname = 'InsertJob_Finished_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('InsertJob_Finished','')
    p_web._DivHeader('InsertJob_Finished',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferInsertJob_Finished',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferInsertJob_Finished',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferInsertJob_Finished',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_InsertJob_Finished',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferInsertJob_Finished',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_InsertJob_Finished',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(JOBACC)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(JOBNOTES)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(JOBSE2)
  p_web._OpenFile(JOBSOBF)
  p_web._OpenFile(AUDIT)
  p_web._OpenFile(WEBJOBNO)
  p_web._OpenFile(JOBOUTFL)
  p_web._OpenFile(JOBSTAGE)
  p_web._OpenFile(STAHEAD)
  p_web._OpenFile(STATUS)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(TRANTYPE)
  p_web._OpenFile(SUBEMAIL)
  p_web._OpenFile(TRAEMAIL)
  p_web._OpenFile(STARECIP)
  p_web._OpenFile(DEFAULTS)
  p_web._OpenFile(WAYBAWT)
  p_web._OpenFile(AUDITE)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBACC)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(JOBNOTES)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBSE2)
  p_Web._CloseFile(JOBSOBF)
  p_Web._CloseFile(AUDIT)
  p_Web._CloseFile(WEBJOBNO)
  p_Web._CloseFile(JOBOUTFL)
  p_Web._CloseFile(JOBSTAGE)
  p_Web._CloseFile(STAHEAD)
  p_Web._CloseFile(STATUS)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(TRANTYPE)
  p_Web._CloseFile(SUBEMAIL)
  p_Web._CloseFile(TRAEMAIL)
  p_Web._CloseFile(STARECIP)
  p_Web._CloseFile(DEFAULTS)
  p_Web._CloseFile(WAYBAWT)
  p_Web._CloseFile(AUDITE)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('InsertJob_Finished_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('InsertJob_Finished_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  !Prime
  p_web.site.SaveButton.TextValue = 'Finished'
  p_web.SetSessionValue('tmp:JobNumber',p_web.GetSessionValue('job:Ref_Number'))
  p_web.SetSessionValue('ReadyForNewJobBooking','')
  
  ! If multiple job booking, redirect back to booking screen
  if (p_web.GSV('mj:InProgress') = 1)
      p_web.SSV('SaveURL','PreNewJobBooking?mjb=1') ! Pass back variable so that can check for Duplicate Tabs
      p_web.SSV('mj:PreviousJobNumber',p_web.GSV('tmp:JobNumber'))
  ELSE
      p_web.SSV('SaveURL','IndexPage')
  END
  
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('SaveURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('InsertJob_Finished_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('InsertJob_Finished_ChainTo')
    loc:formaction = p_web.GetSessionValue('InsertJob_Finished_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="InsertJob_Finished" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="InsertJob_Finished" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="InsertJob_Finished" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Job Booked') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Job Booked',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_InsertJob_Finished">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_InsertJob_Finished" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_InsertJob_Finished')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_InsertJob_Finished')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_InsertJob_Finished'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_InsertJob_Finished')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_InsertJob_Finished_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
    If p_web.GetSessionValue('job:Ref_Number') <> ''
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::JobFinished
      do Comment::JobFinished
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GetSessionValue('job:Ref_Number') = ''
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::JobFailed
      do Comment::JobFailed
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GetSessionValue('job:Ref_Number') <> ''
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::Button:PrintJobCard
      do Comment::Button:PrintJobCard
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    end
    do SendPacket
    If p_web.GetSessionValue('job:Ref_Number') <> ''
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::Button:PrintJobReceipt
      do Comment::Button:PrintJobReceipt
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::JobFinished  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('JobFinished',p_web.GetValue('NewValue'))
    do Value::JobFinished
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::JobFinished  Routine
  p_web._DivHeader('InsertJob_Finished_' & p_web._nocolon('JobFinished') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate('Job Number ' & p_web.GetSessionValue('tmp:JobNumber') & ' inserted successfully.',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::JobFinished  Routine
    loc:comment = ''
  p_web._DivHeader('InsertJob_Finished_' & p_web._nocolon('JobFinished') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::JobFailed  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('JobFailed',p_web.GetValue('NewValue'))
    do Value::JobFailed
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::JobFailed  Routine
  p_web._DivHeader('InsertJob_Finished_' & p_web._nocolon('JobFailed') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBoldLarge')&'">' & p_web.Translate('An error has occured creating the job. Please try again.',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::JobFailed  Routine
    loc:comment = ''
  p_web._DivHeader('InsertJob_Finished_' & p_web._nocolon('JobFailed') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::Button:PrintJobCard  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('Button:PrintJobCard',p_web.GetValue('NewValue'))
    do Value::Button:PrintJobCard
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::Button:PrintJobCard  Routine
  p_web._DivHeader('InsertJob_Finished_' & p_web._nocolon('Button:PrintJobCard') & '_value',Choose(p_web.GetSessionValue('Hide:PrintJobCard') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:PrintJobCard') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','Print Job Card','Print Job Card','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('JobCard?var=' & RANDOM(1,9999999))) & ''','''&clip('_blank')&''')',loc:javascript,0,'/images/printer.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::Button:PrintJobCard  Routine
    loc:comment = ''
  p_web._DivHeader('InsertJob_Finished_' & p_web._nocolon('Button:PrintJobCard') & '_comment',Choose(p_web.GetSessionValue('Hide:PrintJobCard') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('Hide:PrintJobCard') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::Button:PrintJobReceipt  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('Button:PrintJobReceipt',p_web.GetValue('NewValue'))
    do Value::Button:PrintJobReceipt
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::Button:PrintJobReceipt  Routine
  p_web._DivHeader('InsertJob_Finished_' & p_web._nocolon('Button:PrintJobReceipt') & '_value',Choose(p_web.GetSessionValue('Hide:PrintJobReceipt') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:PrintJobReceipt') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','Print Job Receipt','Print Job Receipt','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('JobReceipt?var=' & RANDOM(1,9999999))) & ''','''&clip('_blank')&''')',loc:javascript,0,'/images/printer.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::Button:PrintJobReceipt  Routine
    loc:comment = ''
  p_web._DivHeader('InsertJob_Finished_' & p_web._nocolon('Button:PrintJobReceipt') & '_comment',Choose(p_web.GetSessionValue('Hide:PrintJobReceipt') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('Hide:PrintJobReceipt') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('InsertJob_Finished_form:ready_',1)
  p_web.SetSessionValue('InsertJob_Finished_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_InsertJob_Finished',0)

PreCopy  Routine
  p_web.SetValue('InsertJob_Finished_form:ready_',1)
  p_web.SetSessionValue('InsertJob_Finished_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_InsertJob_Finished',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('InsertJob_Finished_form:ready_',1)
  p_web.SetSessionValue('InsertJob_Finished_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('InsertJob_Finished:Primed',0)

PreDelete       Routine
  p_web.SetValue('InsertJob_Finished_form:ready_',1)
  p_web.SetSessionValue('InsertJob_Finished_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('InsertJob_Finished:Primed',0)
  p_web.setsessionvalue('showtab_InsertJob_Finished',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('InsertJob_Finished_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('InsertJob_Finished_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
      ClearJobVariables(p_web)
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('InsertJob_Finished:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
