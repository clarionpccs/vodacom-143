

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER012.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
AddToAudit           PROCEDURE  (NetWebServerWorker p_web,LONG pJobNumber,STRING pType,STRING pAction,STRING pNotes) ! Declare Procedure
FilesOpened     BYTE(0)

  CODE
    IF (pAction <> '' AND pType <> '')
        
        DO OpenFiles

        If Access:AUDIT.PrimeRecord() = Level:Benign
            aud:Date        = Today()
            aud:Time        = Clock()
            aud:Ref_Number  = pJobNumber
            aud:User        = p_web.GSV('BookingUserCode')
            aud:Action      = pAction
            aud:Type        = pType
            If Access:AUDIT.TryInsert() = Level:Benign
                ! Insert Successful

                If Access:AUDITE.PrimeRecord() = Level:Benign
                    aude:RefNumber = aud:Record_Number
                    aude:IPAddress = p_web.RequestData.FromIP
                    aude:HostName = 'SB Online'
                    If Access:AUDITE.TryInsert() = Level:Benign
                        ! Insert Successful
                    Else ! If Access:AUDITE.TryInsert() = Level:Benign
                        ! Insert Failed
                        Access:AUDITE.CancelAutoInc()
                    End ! If Access:AUDITE.TryInsert() = Level:Benign
                End !If Access:AUDITE.PrimeRecord() = Level:Benign
                
                IF (Access:AUDIT2.PrimeRecord() = Level:Benign)
                    aud2:AUDRecordNumber    = aud:Record_Number
                    aud2:Notes              = pNotes
                    IF (Access:AUDIT2.TryInsert())
                        Access:AUDIT2.CancelAutoInc()
                    END ! IF
                END ! IF
            Else ! If Access:AUDIT.TryInsert() = Level:Benign
                ! Insert Failed
                Access:AUDIT.CancelAutoInc()
            End ! If Access:AUDIT.TryInsert() = Level:Benign
        End !If Access:AUDIT.PrimeRecord() = Level:Benign
        ! Clear fields to stop being written incorrectly (DBH: 02/03/2009)

        DO CloseFiles
    end ! if (p_web.GSV('AddToAudit:Action') <> '' and p_web.GSV('AddToAudit:Type') <> '')
!--------------------------------------
OpenFiles  ROUTINE
  Access:AUDIT2.Open                                       ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:AUDIT2.UseFile                                    ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:AUDIT.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:AUDIT.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:AUDITE.Open                                       ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:AUDITE.UseFile                                    ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:AUDIT2.Close
     Access:AUDIT.Close
     Access:AUDITE.Close
     FilesOpened = False
  END
