

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER362.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER030.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER361.INC'),ONCE        !Req'd for module callout resolution
                     END


RapEngReallocateJobs PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locNewEngineer       STRING(30)                            !
locPassword          STRING(30)                            !
locCurrentEngineer   STRING(100)                           !
locNewSkillLevel     STRING(30)                            !
locSaveEngineer      STRING(30)                            !
FilesOpened     Long
JOBSENG::State  USHORT
JOBSE::State  USHORT
JOBS::State  USHORT
SBO_GenericFile::State  USHORT
TagFile::State  USHORT
USERS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
locNewEngineer_OptionView   View(USERS)
                          Project(use:User_Code)
                        End
  CODE
  GlobalErrors.SetProcedureName('RapEngReallocateJobs')
  loc:formname = 'RapEngReallocateJobs_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('RapEngReallocateJobs','')
    p_web._DivHeader('RapEngReallocateJobs',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferRapEngReallocateJobs',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferRapEngReallocateJobs',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferRapEngReallocateJobs',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_RapEngReallocateJobs',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferRapEngReallocateJobs',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_RapEngReallocateJobs',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(JOBSENG)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(SBO_GenericFile)
  p_web._OpenFile(TagFile)
  p_web._OpenFile(USERS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBSENG)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(SBO_GenericFile)
  p_Web._CloseFile(TagFile)
  p_Web._CloseFile(USERS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('RapEngReallocateJobs_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'locNewEngineer'
    p_web.setsessionvalue('showtab_RapEngReallocateJobs',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(USERS)
        p_web.setsessionvalue('locNewSkillLevel',use:SkillLevel)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locNewSkillLevel')
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locCurrentEngineer',locCurrentEngineer)
  p_web.SetSessionValue('locPassword',locPassword)
  p_web.SetSessionValue('locNewEngineer',locNewEngineer)
  p_web.SetSessionValue('locNewSkillLevel',locNewSkillLevel)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locCurrentEngineer')
    locCurrentEngineer = p_web.GetValue('locCurrentEngineer')
    p_web.SetSessionValue('locCurrentEngineer',locCurrentEngineer)
  End
  if p_web.IfExistsValue('locPassword')
    locPassword = p_web.GetValue('locPassword')
    p_web.SetSessionValue('locPassword',locPassword)
  End
  if p_web.IfExistsValue('locNewEngineer')
    locNewEngineer = p_web.GetValue('locNewEngineer')
    p_web.SetSessionValue('locNewEngineer',locNewEngineer)
  End
  if p_web.IfExistsValue('locNewSkillLevel')
    locNewSkillLevel = p_web.GetValue('locNewSkillLevel')
    p_web.SetSessionValue('locNewSkillLevel',locNewSkillLevel)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('RapEngReallocateJobs_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    p_web.SSV('locPassword','')
    p_web.SSV('locNewEngineer','')
    p_web.SSV('locCurrentEngineer','')
    p_web.SSV('locNewSkillLevel','')    
    
    found# = 0
    Access:TagFile.ClearKey(tag:keyTagged)
    tag:SessionID = p_web.SessionID
    SET(tag:keyTagged,tag:keyTagged)
    LOOP UNTIL Access:TagFile.Next() <> Level:Benign
        IF (tag:SessionID <> p_web.SessionID)
            BREAK
        END ! IF
        found# = 1
        BREAK
    END ! LOOP
    
    IF (found# = 0)
        packet = CLIP(packet) & '<script>alert("You have not tagged any jobs.");window.open("RapidEngineerUpdate","_self");</script>'
        DO SendPacket
    END ! IF
    
    Access:USERS.ClearKey(use:User_Code_Key)
    use:User_Code = p_web.GSV('BookingUserCode')
    IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
        p_web.SSV('locCurrentEngineer',CLIP(use:Forename) & ' ' & CLIP(use:Surname) & '. Skill Level: ' & use:SkillLevel)
    END ! IF
      p_web.site.SaveButton.TextValue = 'Allocate'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locCurrentEngineer = p_web.RestoreValue('locCurrentEngineer')
 locPassword = p_web.RestoreValue('locPassword')
 locNewEngineer = p_web.RestoreValue('locNewEngineer')
 locNewSkillLevel = p_web.RestoreValue('locNewSkillLevel')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'RapidEngineerUpdate'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('RapEngReallocateJobs_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('RapEngReallocateJobs_ChainTo')
    loc:formaction = p_web.GetSessionValue('RapEngReallocateJobs_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'RapidEngineerUpdate'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="RapEngReallocateJobs" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="RapEngReallocateJobs" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="RapEngReallocateJobs" ></input><13,10>'
  end

  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_RapEngReallocateJobs">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_RapEngReallocateJobs" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_RapEngReallocateJobs')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Select Engineer') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_RapEngReallocateJobs')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_RapEngReallocateJobs'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locPassword')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_RapEngReallocateJobs')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Select Engineer') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_RapEngReallocateJobs_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Select Engineer')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Select Engineer')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Select Engineer')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Select Engineer')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locCurrentEngineer
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locCurrentEngineer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locCurrentEngineer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locPassword
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locNewEngineer
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locNewEngineer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locNewEngineer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locNewSkillLevel
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locNewSkillLevel
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locNewSkillLevel
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locCurrentEngineer  Routine
  p_web._DivHeader('RapEngReallocateJobs_' & p_web._nocolon('locCurrentEngineer') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Current Engineer')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locCurrentEngineer  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locCurrentEngineer',p_web.GetValue('NewValue'))
    locCurrentEngineer = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locCurrentEngineer
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locCurrentEngineer',p_web.GetValue('Value'))
    locCurrentEngineer = p_web.GetValue('Value')
  End

Value::locCurrentEngineer  Routine
  p_web._DivHeader('RapEngReallocateJobs_' & p_web._nocolon('locCurrentEngineer') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locCurrentEngineer
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locCurrentEngineer'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locCurrentEngineer  Routine
    loc:comment = ''
  p_web._DivHeader('RapEngReallocateJobs_' & p_web._nocolon('locCurrentEngineer') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locPassword  Routine
  p_web._DivHeader('RapEngReallocateJobs_' & p_web._nocolon('locPassword') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Engineer Password')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locPassword  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locPassword',p_web.GetValue('NewValue'))
    locPassword = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locPassword
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locPassword',p_web.GetValue('Value'))
    locPassword = p_web.GetValue('Value')
  End
  If locPassword = ''
    loc:Invalid = 'locPassword'
    loc:alert = p_web.translate('Engineer Password') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locPassword = Upper(locPassword)
    p_web.SetSessionValue('locPassword',locPassword)
    IF (p_web.GSV('locPassword') <> p_web.GSV('BookingUserPassword'))
        loc:invalid = 'locPassword'
        loc:alert = 'Invalid Password'
        p_web.SSV('locPassword','')
    END ! IF    
  do Value::locPassword
  do SendAlert
  do Value::locNewEngineer  !1

Value::locPassword  Routine
  p_web._DivHeader('RapEngReallocateJobs_' & p_web._nocolon('locPassword') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locPassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locPassword')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locPassword = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locPassword'',''rapengreallocatejobs_locpassword_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locPassword')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','locPassword',p_web.GetSessionValueFormat('locPassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('RapEngReallocateJobs_' & p_web._nocolon('locPassword') & '_value')

Comment::locPassword  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('RapEngReallocateJobs_' & p_web._nocolon('locPassword') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locNewEngineer  Routine
  p_web._DivHeader('RapEngReallocateJobs_' & p_web._nocolon('locNewEngineer') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('New Engineer')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locNewEngineer  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locNewEngineer',p_web.GetValue('NewValue'))
    locNewEngineer = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locNewEngineer
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locNewEngineer',p_web.GetValue('Value'))
    locNewEngineer = p_web.GetValue('Value')
  End
  If locNewEngineer = ''
    loc:Invalid = 'locNewEngineer'
    loc:alert = p_web.translate('New Engineer') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locNewEngineer = Upper(locNewEngineer)
    p_web.SetSessionValue('locNewEngineer',locNewEngineer)
    Access:USERS.ClearKey(use:User_Code_Key)
    use:User_Code = p_web.GSV('locNewEngineer')
    IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
        p_web.SSV('locNewSkillLevel',use:SkillLevel)
    END ! IF  
  do Value::locNewEngineer
  do SendAlert
  do Value::locNewSkillLevel  !1

Value::locNewEngineer  Routine
  p_web._DivHeader('RapEngReallocateJobs_' & p_web._nocolon('locNewEngineer') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locNewEngineer')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locNewEngineer = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locNewEngineer'',''rapengreallocatejobs_locnewengineer_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locNewEngineer')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('locPassword') = '','disabled','')
  packet = clip(packet) & p_web.CreateSelect('locNewEngineer',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locNewEngineer') = 0
    p_web.SetSessionValue('locNewEngineer','')
  end
    packet = clip(packet) & p_web.CreateOption('','',choose('' = p_web.getsessionvalue('locNewEngineer')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(JOBSENG)
  bind(joe:Record)
  p_web._OpenFile(JOBSE)
  bind(jobe:Record)
  p_web._OpenFile(JOBS)
  bind(job:Record)
  p_web._OpenFile(SBO_GenericFile)
  bind(sbogen:Record)
  p_web._OpenFile(TagFile)
  bind(tag:Record)
  p_web._OpenFile(USERS)
  bind(use:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(locNewEngineer_OptionView)
  locNewEngineer_OptionView{prop:filter} = 'UPPER(use:Location) = UPPER(''' & p_web.GSV('BookingSiteLocation') & ''') AND UPPER(use:User_Type) = ''ENGINEER'' AND use:Active = ''YES'' AND UPPER(use:User_Code) <> UPPER(''' & p_web.GSV('BookingUserCode') & ''')'
  locNewEngineer_OptionView{prop:order} = 'UPPER(use:Forename)'
  Set(locNewEngineer_OptionView)
  Loop
    Next(locNewEngineer_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('locNewEngineer') = 0
      p_web.SetSessionValue('locNewEngineer',use:User_Code)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(CLIP(use:Forename) & ' ' & CLIP(use:Surname),use:User_Code,choose(use:User_Code = p_web.getsessionvalue('locNewEngineer')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(locNewEngineer_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(JOBSENG)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(SBO_GenericFile)
  p_Web._CloseFile(TagFile)
  p_Web._CloseFile(USERS)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('RapEngReallocateJobs_' & p_web._nocolon('locNewEngineer') & '_value')

Comment::locNewEngineer  Routine
    loc:comment = ''
  p_web._DivHeader('RapEngReallocateJobs_' & p_web._nocolon('locNewEngineer') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locNewSkillLevel  Routine
  p_web._DivHeader('RapEngReallocateJobs_' & p_web._nocolon('locNewSkillLevel') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('New Skill Level')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locNewSkillLevel  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locNewSkillLevel',p_web.GetValue('NewValue'))
    locNewSkillLevel = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locNewSkillLevel
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locNewSkillLevel',p_web.GetValue('Value'))
    locNewSkillLevel = p_web.GetValue('Value')
  End

Value::locNewSkillLevel  Routine
  p_web._DivHeader('RapEngReallocateJobs_' & p_web._nocolon('locNewSkillLevel') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locNewSkillLevel
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locNewSkillLevel'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('RapEngReallocateJobs_' & p_web._nocolon('locNewSkillLevel') & '_value')

Comment::locNewSkillLevel  Routine
    loc:comment = ''
  p_web._DivHeader('RapEngReallocateJobs_' & p_web._nocolon('locNewSkillLevel') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('RapEngReallocateJobs_locPassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locPassword
      else
        do Value::locPassword
      end
  of lower('RapEngReallocateJobs_locNewEngineer_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locNewEngineer
      else
        do Value::locNewEngineer
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('RapEngReallocateJobs_form:ready_',1)
  p_web.SetSessionValue('RapEngReallocateJobs_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_RapEngReallocateJobs',0)

PreCopy  Routine
  p_web.SetValue('RapEngReallocateJobs_form:ready_',1)
  p_web.SetSessionValue('RapEngReallocateJobs_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_RapEngReallocateJobs',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('RapEngReallocateJobs_form:ready_',1)
  p_web.SetSessionValue('RapEngReallocateJobs_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('RapEngReallocateJobs:Primed',0)

PreDelete       Routine
  p_web.SetValue('RapEngReallocateJobs_form:ready_',1)
  p_web.SetSessionValue('RapEngReallocateJobs_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('RapEngReallocateJobs:Primed',0)
  p_web.setsessionvalue('showtab_RapEngReallocateJobs',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('RapEngReallocateJobs_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('RapEngReallocateJobs_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
        If locPassword = ''
          loc:Invalid = 'locPassword'
          loc:alert = p_web.translate('Engineer Password') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locPassword = Upper(locPassword)
          p_web.SetSessionValue('locPassword',locPassword)
        If loc:Invalid <> '' then exit.
        If locNewEngineer = ''
          loc:Invalid = 'locNewEngineer'
          loc:alert = p_web.translate('New Engineer') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locNewEngineer = Upper(locNewEngineer)
          p_web.SetSessionValue('locNewEngineer',locNewEngineer)
    Access:TagFile.ClearKey(tag:keyTagged)
    tag:sessionID = p_web.SessionID
    SET(tag:keyTagged,tag:keyTagged)
    LOOP UNTIL Access:TagFile.Next() <> Level:Benign
        IF (tag:sessionID <> p_web.SessionID)
            BREAK
        END ! IF
        
        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = tag:taggedValue
        IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
            CYCLE
        END ! IF
        
        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = job:Ref_Number
        IF (Access:JOBSE.TryFetch(jobe:RefNumberKey))
        END ! IF
        
        
        locSaveEngineer = job:Engineer
        job:Engineer = p_web.GSV('locNewEngineer')
        IF (Access:JOBS.TryUpdate() = Level:Benign)
            p_web.FileToSessionQueue(JOBS)
            p_web.FileToSessionQueue(JOBSE)
  
            Access:USERS.ClearKey(use:User_Code_Key)
            use:User_Code = job:Engineer
            IF (Access:USERS.TryFetch(use:User_Code_Key))
            END ! IF
            
            AddToAudit(p_web,job:Ref_Number,'JOB','RAPID ENGINEER UPDATE: JOB REALLOCATED','ENGINEER: ' & Clip(use:Forename) & ' ' & Clip(use:Surname))
            
            IF (p_web.GSV('BookingSite') = 'RRC' AND job:Date_Completed <> '' AND JobSentToARC(p_web))
                                            !The engineer is being changed at the RRC after the job has come back from hub
                If Access:JOBSENG.PrimeRecord() = Level:Benign
                    joe:JobNumber     = job:Ref_Number
                    joe:UserCode      = job:Engineer
                    joe:DateAllocated = Today()
                    joe:TimeAllocated = Clock()
                    joe:AllocatedBy   = p_web.GSV('BookingUserCode')
                    joe:Status        = 'ENGINEER QA'
                    joe:StatusDate    = Today()
                    joe:StatusTime    = Clock()
                    joe:EngSkillLevel = p_web.GSV('locNewSkillLevel')
                    joe:JobSkillLevel = jobe:SkillLevel
  
                    If Access:JOBSENG.TryInsert() = Level:Benign
                                    !Insert Successful
                    Else !If Access:JOBSENG.TryInsert() = Level:Benign
                                    !Insert Failed
                    End !If Access:JOBSENG.TryInsert() = Level:Benign
                ELSE ! IF
                                                !Lookup current engineer and mark as escalated
                            !Escalate the current engineer. If attached - L832 (DBH: 08-07-2003
                    If locSaveEngineer <> ''
                        Access:JOBSENG.ClearKey(joe:UserCodeKey)
                        joe:JobNumber     = job:Ref_Number
                        joe:UserCode      = locSaveEngineer
                        joe:DateAllocated = Today()
                        Set(joe:UserCodeKey,joe:UserCodeKey)
                        Loop
                            If Access:JOBSENG.PREVIOUS()
                                Break
                            End !If
                            If joe:JobNumber     <> job:Ref_Number       |
                                Or joe:UserCode      <> locSaveEngineer      |
                                Or joe:DateAllocated > Today()       |
                                Then Break.  ! End If
                            If joe:Status = 'ALLOCATED'
                                joe:Status = 'ESCALATED'
                                joe:StatusDate = Today()
                                joe:StatusTime = Clock()
                                Access:JOBSENG.Update()
                            End !If joe:Status = 'ALLOCATED'
                            Break
                        End !Loop
                    End !If tmp:OldEngineer <> ''
                    If Access:JOBSENG.PrimeRecord() = Level:Benign
                        joe:JobNumber     = job:Ref_Number
                        joe:UserCode      = job:Engineer
                        joe:DateAllocated = Today()
                        joe:TimeAllocated = Clock()
                        joe:AllocatedBy   = p_web.GSV('BookingUserCode')
                        joe:Status        = 'ALLOCATED'
                        joe:StatusDate    = Today()
                        joe:StatusTime    = Clock()
  
                        access:users.clearkey(use:User_Code_Key)
                        use:User_Code   = job:Engineer
                        access:users.fetch(use:User_Code_Key)
                        joe:EngSkillLevel = use:SkillLevel
                        joe:JobSkillLevel = jobe:SkillLevel
  
                        If Access:JOBSENG.TryInsert() = Level:Benign
                                    !Insert Successful
                        Else !If Access:JOBSENG.TryInsert() = Level:Benign
                                    !Insert Failed
                        End !If Access:JOBSENG.TryInsert() = Level:Benign
                    End !If Access:JOBSENG.PrimeRecord() = Level:Benign
                End !If Access:JOBSENG.PrimeRecord() = Level:Benign    
            END ! IF
            
        END ! IF
    END ! LOOP
    ClearTagFile(p_web)
        If loc:Invalid <> '' then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('RapEngReallocateJobs:Primed',0)
  p_web.StoreValue('locCurrentEngineer')
  p_web.StoreValue('locPassword')
  p_web.StoreValue('locNewEngineer')
  p_web.StoreValue('locNewSkillLevel')
SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('locNewEngineer',locNewEngineer) ! STRING(30)
     p_web.SSV('locPassword',locPassword) ! STRING(30)
     p_web.SSV('locCurrentEngineer',locCurrentEngineer) ! STRING(100)
     p_web.SSV('locNewSkillLevel',locNewSkillLevel) ! STRING(30)
     p_web.SSV('locSaveEngineer',locSaveEngineer) ! STRING(30)
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     locNewEngineer = p_web.GSV('locNewEngineer') ! STRING(30)
     locPassword = p_web.GSV('locPassword') ! STRING(30)
     locCurrentEngineer = p_web.GSV('locCurrentEngineer') ! STRING(100)
     locNewSkillLevel = p_web.GSV('locNewSkillLevel') ! STRING(30)
     locSaveEngineer = p_web.GSV('locSaveEngineer') ! STRING(30)
