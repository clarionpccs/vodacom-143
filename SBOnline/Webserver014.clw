

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER014.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
PassExchangeAssessment PROCEDURE  (Long fJobNumber)        ! Declare Procedure
rtnValue             BYTE                                  !
PARTS::State  USHORT
WARPARTS::State  USHORT
MANFAUPA::State  USHORT
MANFAULO::State  USHORT
MANFAULT::State  USHORT
JOBOUTFL::State  USHORT
JOBS::State  USHORT
FilesOpened     BYTE(0)
tmp:FaultCode   STRING(255)

  CODE
    DO openFiles
    DO SaveFiles
    !Loop through outfaults
    Access:JOBS.Clearkey(job:Ref_Number_Key)
    job:Ref_Number = fJobNumber
    IF (Access:JOBS.Tryfetch(job:Ref_Number_Key))
        
    END
    
    rtnValue = 0
    
    Access:JOBOUTFL.ClearKey(joo:JobNumberKey)
    joo:JobNumber = job:Ref_Number
    Set(joo:JobNumberKey,joo:JobNumberKey)
    Loop
        If Access:JOBOUTFL.NEXT()
            Break
        End !If
        If joo:JobNumber <> job:Ref_Number      |
                        Then Break.  ! End If
    !Which is the main out fault?
        Access:MANFAULT.ClearKey(maf:MainFaultKey)
        maf:Manufacturer = job:Manufacturer
        maf:MainFault    = 1
        If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
    !Lookup the Fault Code lookup to see if it's excluded
            Access:MANFAULO.ClearKey(mfo:Field_Key)
            mfo:Manufacturer = job:Manufacturer
            mfo:Field_Number = maf:Field_Number
            mfo:Field        = joo:FaultCode
            Set(mfo:Field_Key,mfo:Field_Key)
            Loop
                If Access:MANFAULO.NEXT()
                    Break
                End !If
                If mfo:Manufacturer <> job:Manufacturer      |
                                Or mfo:Field_Number <> maf:Field_Number      |
                                Or mfo:Field        <> joo:FaultCode      |
                                Then Break.  ! End If
                If Clip(mfo:Description) = Clip(joo:Description)
    !Make sure the descriptions match in case of duplicates
                    If mfo:ReturnToRRC
                        rtnValue = 0
                        Do ExitProc
                    End !If mfo:ReturnToRRC
                    Break
                End !If Clip(mfo:Description) = Clip(joo:Description)
            End !Loop
    
        Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
    !Error
        End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
    
    End !Loop
    
    Access:MANFAUPA.Clearkey(map:MainFaultKey)
    maf:Manufacturer = job:Manufacturer
    map:Manufacturer = job:Manufacturer
    
    map:Manufacturer = job:Manufacturer
    
                        
    
    !Is an outfault records on parts for this manufacturer
        Access:MANFAUPA.ClearKey(map:MainFaultKey)
    map:Manufacturer = job:Manufacturer
    map:MainFault    = 1
        If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
    !Found
    !Loop through the parts as see if any of the faults codes are excluded
            If job:Warranty_Job = 'YES'
    
                Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
                wpr:Ref_Number  = job:Ref_Number
                Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                Loop
                    If Access:WARPARTS.NEXT()
                        Break
                    End !If
                    If wpr:Ref_Number  <> job:Ref_Number      |
                                    Then Break.  ! End If
    !Which is the main out fault?
                    Access:MANFAULT.ClearKey(maf:MainFaultKey)
                    maf:Manufacturer = job:Manufacturer
                    maf:MainFault    = 1
                    If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
    !Lookup the Fault Code lookup to see if it's excluded
    
    !Work out which part fault code is the outfault
    !and use that for the lookup
                        Case map:Field_Number
                        Of 1
                            tmp:FaultCode        = wpr:Fault_Code1
                        Of 2
                            tmp:FaultCode        = wpr:Fault_Code2
                        Of 3
                            tmp:FaultCode        = wpr:Fault_Code3
                        Of 4
                            tmp:FaultCode        = wpr:Fault_Code4
                        Of 5
                            tmp:FaultCode        = wpr:Fault_Code5
                        Of 6
                            tmp:FaultCode        = wpr:Fault_Code6
                        Of 7
                            tmp:FaultCode        = wpr:Fault_Code7
                        Of 8
                            tmp:FaultCode        = wpr:Fault_Code8
                        Of 9
                            tmp:FaultCode        = wpr:Fault_Code9
                        Of 10
                            tmp:FaultCode        = wpr:Fault_Code10
                        Of 11
                            tmp:FaultCode        = wpr:Fault_Code11
                        Of 12
                            tmp:FaultCode        = wpr:Fault_Code12
                        End !Case map:Field_Number
    
                        Access:MANFAULO.ClearKey(mfo:Field_Key)
                        mfo:Manufacturer = job:Manufacturer
                        mfo:Field_Number = maf:Field_Number
                        mfo:Field        = tmp:FaultCode
                        Set(mfo:Field_Key,mfo:Field_Key)
                        Loop
                            If Access:MANFAULO.NEXT()
                                Break
                            End !If
                            If mfo:Manufacturer <> job:Manufacturer      |
                                            Or mfo:Field_Number <> maf:Field_Number      |
                                            Or mfo:Field        <> tmp:FaultCode      |
                                            Then Break.  ! End If
    !This fault relates to a specific part fault code number??
                            If mfo:RelatedPartCode <> 0 And map:UseRelatedJobCode
                                If mfo:RelatedPartCode <> maf:Field_Number
                                    Cycle
                                End !If mfo:RelatedPartCode <> maf:Field_Number
                            End !If mfo:RelatedPartCode <> 0
                            IF mfo:ReturnToRRC
                                rtnValue = 0
                                Do ExitProc
                            End !IF mfo:ExcludeFromBouncer
                        End !Loop
    
                    Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
    !Error
                    End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
    
                End !Loop
            End !If job:Warranty_Job = 'YES'
    
            If job:Chargeable_Job = 'YES'
    !Loop through the parts as see if any of the faults codes are excluded
                Access:PARTS.ClearKey(par:Part_Number_Key)
                par:Ref_Number  = job:Ref_Number
                Set(par:Part_Number_Key,par:Part_Number_Key)
                Loop
                    If Access:PARTS.NEXT()
                        Break
                    End !If
                    If par:Ref_Number  <> job:Ref_Number      |
                                    Then Break.  ! End If
    !Which is the main out fault?
                    Access:MANFAULT.ClearKey(maf:MainFaultKey)
                    maf:Manufacturer = job:Manufacturer
                    maf:MainFault    = 1
                    If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
    !Lookup the Fault Code lookup to see if it's excluded
    
    !Work out which part fault code is the outfault
    !and use that for the lookup
                        Case map:Field_Number
                        Of 1
                            tmp:FaultCode        = par:Fault_Code1
                        Of 2
                            tmp:FaultCode        = par:Fault_Code2
                        Of 3
                            tmp:FaultCode        = par:Fault_Code3
                        Of 4
                            tmp:FaultCode        = par:Fault_Code4
                        Of 5
                            tmp:FaultCode        = par:Fault_Code5
                        Of 6
                            tmp:FaultCode        = par:Fault_Code6
                        Of 7
                            tmp:FaultCode        = par:Fault_Code7
                        Of 8
                            tmp:FaultCode        = par:Fault_Code8
                        Of 9
                            tmp:FaultCode        = par:Fault_Code9
                        Of 10
                            tmp:FaultCode        = par:Fault_Code10
                        Of 11
                            tmp:FaultCode        = par:Fault_Code11
                        Of 12
                            tmp:FaultCode        = par:Fault_Code12
                        End !Case map:Field_Number
    
                        Access:MANFAULO.ClearKey(mfo:Field_Key)
                        mfo:Manufacturer = job:Manufacturer
                        mfo:Field_Number = maf:Field_Number
                        mfo:Field        = tmp:FaultCode
                        Set(mfo:Field_Key,mfo:Field_Key)
                        Loop
                            If Access:MANFAULO.NEXT()
                                Break
                            End !If
                            If mfo:Manufacturer <> job:Manufacturer      |
                                            Or mfo:Field_Number <> maf:Field_Number      |
                                            Or mfo:Field        <> tmp:FaultCode      |
                                            Then Break.  ! End If
    !This fault relates to a specific part fault code number??
                            If mfo:RelatedPartCode <> 0 And map:UseRelatedJobCode
                                If mfo:RelatedPartCode <> maf:Field_Number
                                    Cycle
                                End !If mfo:RelatedPartCode <> maf:Field_Number
                            End !If mfo:RelatedPartCode <> 0
                            IF mfo:ReturnToRRC
                                rtnValue = 0
                                Do ExitProc
                            End !IF mfo:ExcludeFromBouncer
                        End !Loop
    
                    Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
    !Error
                    End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
    
                End !Loop
            End !If job:Chargeable_Job = 'YES'
        Else!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
    !Error
        End!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
    
        rtnValue = 1
        Do ExitProc
    

ExitProc    ROUTINE
    DO RestoreFiles
    DO CloseFiles
    RETURN rtnValue
SaveFiles  ROUTINE
  PARTS::State = Access:PARTS.SaveFile()                   ! Save File referenced in 'Other Files' so need to inform its FileManager
  WARPARTS::State = Access:WARPARTS.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  MANFAUPA::State = Access:MANFAUPA.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  MANFAULO::State = Access:MANFAULO.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  MANFAULT::State = Access:MANFAULT.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  JOBOUTFL::State = Access:JOBOUTFL.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  JOBS::State = Access:JOBS.SaveFile()                     ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF PARTS::State <> 0
    Access:PARTS.RestoreFile(PARTS::State)                 ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF WARPARTS::State <> 0
    Access:WARPARTS.RestoreFile(WARPARTS::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF MANFAUPA::State <> 0
    Access:MANFAUPA.RestoreFile(MANFAUPA::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF MANFAULO::State <> 0
    Access:MANFAULO.RestoreFile(MANFAULO::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF MANFAULT::State <> 0
    Access:MANFAULT.RestoreFile(MANFAULT::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF JOBOUTFL::State <> 0
    Access:JOBOUTFL.RestoreFile(JOBOUTFL::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF JOBS::State <> 0
    Access:JOBS.RestoreFile(JOBS::State)                   ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:PARTS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:PARTS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:WARPARTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:WARPARTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAUPA.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAUPA.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAULO.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAULO.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAULT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAULT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBOUTFL.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBOUTFL.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS.Open                                         ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS.UseFile                                      ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:PARTS.Close
     Access:WARPARTS.Close
     Access:MANFAUPA.Close
     Access:MANFAULO.Close
     Access:MANFAULT.Close
     Access:JOBOUTFL.Close
     Access:JOBS.Close
     FilesOpened = False
  END
