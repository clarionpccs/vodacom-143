

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER211.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
ClearJobVariables    PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure

  CODE
    ! Clear Insert Job Variables
    p_web.DeleteSessionValue('tmp:ESN')
    p_web.DeleteSessionValue('tmp:TransitType')
    p_web.DeleteSessionValue('tmp:Manufacturer')
    p_web.DeleteSessionValue('tmp:ModelNumber')
    If p_web.gsv('BookingSite') = 'ARC'
        p_web.SSV('Filter:TransitType','trt:ARC = 1')
    Else ! If p_web.GetSessionValue('BookingSite') = 'ARC'
        p_web.SSV('Filter:TransitType','trt:RRC = 1')
    End ! If p_web.GetSessionValue('BookingSite') = 'ARC'
    p_web.SSV('tmp:TransitType',GETINI(p_web.GetSessionValue('BookingAccount'), 'InitialTransitType',, CLIP(PATH()) & '\SB2KDEF.INI'))
    p_web.SSV('Comment:TransitType','Required')
    p_web.DeleteSessionValue('Comment:IMEINumber')
    p_web.SSV('Comment:DOP','dd/mm/yyyy')

    p_web.DeleteSessionValue('Filter:Manufacturer')
    p_web.DeleteSessionValue('Filter:ModelNumber')

    p_web.SSV('Hide:IMEINumberButton',1)

    p_web.DeleteSessionValue('OBFValidation')
    p_web.DeleteSessionValue('Prompt:Validation')
    p_web.DeleteSessionValue('Error:Validation')
    p_web.DeleteSessionValue('Passed:Validation')
    p_web.DeleteSessionValue('tmp:DOP')
    p_web.DeleteSessionValue('tmp:OldDOP')
    p_web.SSV('Hide:ChangeDOP',1)
    p_web.SSV('Hide:ChangeDOPButton',1)
    p_web.DeleteSessionValue('tmp:NewDOP')
    p_web.DeleteSessionValue('tmp:ChangeReason')


    p_web.DeleteSessionValue('tmp:DateOfPurchase')

    p_web.DeleteSessionValue('Save:DOP')
    p_web.DeleteSessionValue('Required:DOP')
    p_web.DeleteSessionValue('ReadOnly:DOP')
    p_web.SSV('ReadOnly:Manufacturer',1)
    p_web.SSV('ReadOnly:ModelNumber',1)
    p_web.DeleteSessionValue('ReadOnly:TransitType')
    p_web.DeleteSessionValue('ReadOnly:IMEINumber')
    p_web.DeleteSessionValue('ReadOnly:ChargeableJob')
    p_web.DeleteSessionValue('ReadOnly:WarrantyJob')

    p_web.DeleteSessionValue('tmp:Network')
    p_web.DeleteSessionValue('tmp:ReturnDate')
    p_web.DeleteSessionValue('tmp:DateOfPurchase')
    p_web.DeleteSessionValue('tmp:BOXIMEINumber')
    p_web.DeleteSessionValue('tmp:BranchOfReturn')
    p_web.DeleteSessionValue('tmp:LAccountNumber')
    p_web.DeleteSessionValue('tmp:OriginalAccessories')
    p_web.DeleteSessionValue('tmp:OriginalDealer')
    p_web.DeleteSessionValue('tmp:OriginalManuals')
    p_web.DeleteSessionValue('tmp:OriginalPackaging')
    p_web.DeleteSessionValue('tmp:PhysicalDamage')
    p_web.DeleteSessionValue('tmp:ProofOfPurchase')
    p_web.DeleteSessionValue('tmp:Replacement')
    p_web.DeleteSessionValue('tmp:ReplacementIMEINumber')
    p_web.DeleteSessionValue('StoreReferenceNumber')
    p_web.DeleteSessionValue('tmp:TalkTime')
    p_web.SSV('Hide:PreviousAddress',1)
    p_web.DeleteSessionValue('tmp:StoreReferenceNumber')

    p_web.SSV('ReadyForNewJobBooking',1)
    p_web.SSV('FirstTime',1)

    p_web.DeleteSessionValue('Required:Engineer')

    p_web.DeleteSessionValue('tmp:ExternalDamageCheckList')
    p_web.DeleteSessionValue('tmp:AmendFaultCodes')

    p_web.DeleteSessionValue('tmp:AmendAddressess')

    p_web.DeleteSessionValue('FranchiseAccount')

    p_web.DeleteSessionValue('tmp:AmendAccessories')
    p_web.DeleteSessionValue('tmp:ShowAccessory')
    p_web.DeleteSessionValue('tmp:TheAccessory')
    p_web.DeleteSessionValue('tmp:TheJobAccessory')
    p_web.DeleteSessionValue('tmp:AmendAddresses')
    p_web.DeleteSessionValue('tmp:DOP')
    p_web.DeleteSessionValue('tmp:POPType')
    p_web.DeleteSessionValue('tmp:PreviousAddress')
    p_web.DeleteSessionValue('tmp:UsePreviousAddress')
    p_web.DeleteSessionValue('AmendFaultCodes')
    p_web.DeleteSessionValue('tmp:FaultCode1')
    p_web.DeleteSessionValue('tmp:FaultCode2')
    p_web.DeleteSessionValue('tmp:FaultCode3')
    p_web.DeleteSessionValue('tmp:FaultCode4')
    p_web.DeleteSessionValue('tmp:FaultCode5')
    p_web.DeleteSessionValue('tmp:FaultCode6')
    p_web.DeleteSessionValue('tmp:ExternalDamageChecklist')
    p_web.DeleteSessionValue('filter:WarrantyChargeTYpe')
    p_web.DeleteSessionValue('Comment:POP')
    p_web.SSV('Drop:JobType','-------------------------------------')
    p_web.DeleteSessionValue('Drop:JobTypeDefault')

    ! --- Clear passed mq variables ----
    ! Inserting:  DBH 30/01/2009 #N/A
    p_web.DeleteSessionValue('mq:ChargeableJob')
    p_web.DeleteSessionValue('mq:Charge_Type')
    p_web.DeleteSessionValue('mq:WarrantyJob')
    p_web.DeleteSessionValue('mq:Warranty_Charge_Type')
    p_web.DeleteSessionValue('mq:DOP')
    p_web.DeleteSessionValue('mq:POP')
    p_web.DeleteSessionValue('mq:IMEIValidation')
    p_web.DeleteSessionValue('mq:IMEIValidationText')
    ! End: DBH 30/01/2009 #N/A
    ! -----------------------------------------

