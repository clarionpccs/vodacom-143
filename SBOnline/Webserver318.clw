

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('WEBSERVER318.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER411.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
HandoverReport PROCEDURE (<NetWebServerWorker p_web>)

  ! The NetTalk Extension to ABC report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
LimitValue           LONG                                  !
JobCount             LONG                                  !
TitleText            STRING(35)                            !
tmp:PrintedBy        STRING(50)                            !
tmp:Engineer         STRING(50)                            !
tmp:printed          STRING(20)                            !
Address:User_Name    STRING(50)                            !
address:Address_Line1 STRING(50)                           !
address:Address_Line2 STRING(50)                           !
address:Address_Line3 STRING(50)                           !
address:Post_Code    STRING(20)                            !
address:Telephone_Number STRING(20)                        !
address:Fax_No       STRING(20)                            !
address:Email        STRING(50)                            !
Process:View         VIEW(HANDOJOB)
                       PROJECT(haj:Accessories)
                       PROJECT(haj:IMEINumber)
                       PROJECT(haj:JobNumber)
                       PROJECT(haj:ModelNumber)
                       PROJECT(haj:RefNumber)
                     END
ReportPageNumber     LONG,AUTO
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

Report               REPORT,AT(1000,3698,6250,5990),PRE(RPT),PAPER(PAPER:A4),FONT('Arial',10,,FONT:regular,CHARSET:ANSI), |
  THOUS
                       HEADER,AT(1000,1000,6250,2729),USE(?Header)
                         BOX,AT(3156,812,3042,1385),USE(?BOX1),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1),ROUND
                         BOX,AT(83,2354,6104,292),USE(?BOX2),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1)
                         STRING(@s50),AT(4135,1583),USE(tmp:Engineer),FONT(,,,FONT:bold),TRN
                         STRING(@n_8),AT(4135,1792),USE(LimitValue),FONT(,,,FONT:bold),TRN
                         STRING(@s20),AT(4135,1156),USE(tmp:printed),FONT(,,,FONT:bold),TRN
                         STRING(@s50),AT(4135,927),USE(tmp:PrintedBy,,?tmp:PrintedBy:2),FONT(,,,FONT:bold),TRN
                         STRING(@s50),AT(135,146),USE(Address:User_Name),FONT(,11,,FONT:bold,CHARSET:DEFAULT),TRN
                         STRING(@s50),AT(135,448),USE(address:Address_Line1)
                         STRING(@s50),AT(135,687),USE(address:Address_Line2)
                         STRING(@s50),AT(135,927),USE(address:Address_Line3)
                         STRING(@s20),AT(135,1156),USE(address:Post_Code)
                         STRING(@s20),AT(625,1375),USE(address:Telephone_Number)
                         STRING(@s20),AT(625,1583),USE(address:Fax_No)
                         STRING(@s50),AT(625,1792),USE(address:Email)
                         STRING(@s35),AT(3156,146,3000),USE(TitleText),FONT(,11,,FONT:bold,CHARSET:DEFAULT),RIGHT,TRN
                         STRING('Tel:'),AT(135,1396),USE(?STRING1)
                         STRING('Fax:'),AT(135,1625),USE(?STRING2)
                         STRING('Email:'),AT(135,1771),USE(?STRING3)
                         STRING('Printed By:'),AT(3240,927),USE(?STRING4),TRN
                         STRING('Date Printed:'),AT(3240,1156),USE(?STRING5),TRN
                         STRING('Page Number:'),AT(3240,1375),USE(?STRING6),TRN
                         STRING('User:'),AT(3240,1583),USE(?STRING7),TRN
                         STRING('Report No:'),AT(3240,1792),USE(?STRING8),TRN
                         STRING(@N3),AT(4135,1375),USE(ReportPageNumber),FONT(,,,FONT:bold),TRN
                         STRING('Job No'),AT(135,2406),USE(?STRING9),FONT(,,,FONT:bold),TRN
                         STRING('IMEI Number'),AT(760,2406),USE(?STRING10),FONT(,,,FONT:bold),TRN
                         STRING('Model Number'),AT(1948,2406),USE(?STRING11),FONT(,,,FONT:bold),TRN
                         STRING('Accessories'),AT(3240,2406),USE(?STRING12),FONT(,,,FONT:bold),TRN
                       END
Detail                 DETAIL,AT(0,0,6250,167),USE(?Detail),FONT('Tahoma',8,,,CHARSET:DEFAULT)
                         STRING(@s8),AT(135,-10),USE(haj:JobNumber),TRN
                         STRING(@s30),AT(760,-10),USE(haj:IMEINumber),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),LEFT, |
  TRN
                         STRING(@s30),AT(1948,-10),USE(haj:ModelNumber),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI), |
  LEFT,TRN
                         STRING(@s255),AT(3240,-10),USE(haj:Accessories),LEFT,TRN
                       END
detail1                DETAIL,AT(0,0,6250,469),USE(?DETAIL1)
                         STRING('Total Number of Jobs:'),AT(115,177,1500),USE(?STRING13),FONT(,,,FONT:bold)
                         STRING(@n3),AT(1677,177,344),USE(JobCount),FONT(,,,FONT:bold),LEFT(1)
                         STRING('Processed By:_{16}Received By:_{16}'),AT(2104,177,4031),USE(?STRING14),FONT(,,,FONT:bold)
                         LINE,AT(146,42,5958,0),USE(?LINE1)
                       END
                       FORM,AT(1000,1000,6250,9688),USE(?Form)
                         BOX,AT(83,2562,6104,7073),USE(?BOX3),COLOR(COLOR:Black),LINEWIDTH(1)
                       END
                     END
!*** PDF-Tools v4.1 Start (All)
!--- ClarioNET NOT Enabled
!--- PDFXTABCTemplate: True  Level: 4
!--- PDFXTABCTemplate: RPM: False
!--- PDFXTABCTemplate: Icetips: False
PDFXTR5               PDFXToolsReportClass                 !  Added by PDF-Tools Templates v4.1
PDFXTR5:rtn           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXTR5:vpf           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Orientation      BYTE(0)                      !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:TopMargin        REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:BottomMargin     REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:LeftMargin       REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:RightMargin      REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Selection        STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PDFXT4:Papers:Queue           QUEUE,PRE()                  !  Added by PDF-Tools Templates v4.1
PLQ:Description                 STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PLQ:PaperCode                   BYTE                       !  Added by PDF-Tools Templates v4.1
                              END                          !  Added by PDF-Tools Templates v4.1
!*** PDF-Tools v4.1 End
ThisWindow           CLASS(ReportManager)
EndReport              PROCEDURE(),BYTE,DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.EndReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !print the final bit
      print(rpt:detail1)
  ReturnValue = PARENT.EndReport()
  RETURN ReturnValue


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('HandoverReport')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open                                     ! File DEFAULTS used by this procedure, so make sure it's RelationManager is open
  Relate:HANDOJOB.SetOpenRelated()
  Relate:HANDOJOB.Open                                     ! File HANDOJOB used by this procedure, so make sure it's RelationManager is open
  Relate:JOBS.SetOpenRelated()
  Relate:JOBS.Open                                         ! File JOBS used by this procedure, so make sure it's RelationManager is open
  Access:USERS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:HANDOVER.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
      !preliminaries go here
  
        IF (p_web.IfExistsValue('HandoverRecordNo'))
            p_web.StoreValue('HandoverRecordNo')
        END ! IF
        
      LimitValue = p_web.gsv('HandoverRecordNo')!deformat(p_web.gsv('HandoverRecordNo'))
      
      Access:HANDOVER.clearkey(han:RecordNumberKey)
      han:RecordNumber = LimitValue
        access:handover.fetch(han:RecordNumberKey)
        
      JobCount = 0
      tmp:printed = format(today(),@d06)& ' '&format(clock(),@t01)
  
  
      Set(DEFAULTS)
      Access:DEFAULTS.Next()
  
      if han:RRCAccountNumber <> 'AA20' and han:RRCAccountNumber <> '' then
          !use trade account details
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number  = han:RRCAccountNumber
          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              ! Found
          End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
          Address:User_Name=        tra:Company_Name
          address:Address_Line1=    tra:Address_Line1
          address:Address_Line2=    tra:Address_Line2
          address:Address_Line3=    tra:Address_Line3
          address:Post_Code=        tra:Postcode
          address:Telephone_Number= tra:Telephone_Number
          address:Fax_No=           tra:Fax_Number
          address:Email=            tra:EmailAddress
  
      ELSE
          !use details from the defaults
          Address:User_Name=          def:User_Name
          address:Address_Line1=      def:Address_Line1
          address:Address_Line2=      def:Address_Line2
          address:Address_Line3=      def:Address_Line3
          address:Post_Code=          def:Postcode
          address:Telephone_Number=   def:telephone_Number
          address:Fax_No=             def:Fax_Number
          address:Email=              def:EmailAddress
      END
  
      ! Inserting (DBH 06/06/2006) #7793 - Set the report title
      If han:HandoverType = 'JOB'
          TitleText  = 'HANDOVER CONFIRMATION REPORT'
      Else 
          TitleText = 'HANDOVER EXCHANGE CONF. REPORT'
      End ! If han:HandoverType = 0
  
  
      Access:USERS.Clearkey(use:User_Code_Key)
      use:User_Code   = han:UserCode
      If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
          !Found
          tmp:Engineer    = Clip(use:Forename) & ' ' & Clip(use:Surname)
      Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
          !Error
      End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
  
      Access:USERS.Clearkey(use:User_Code_Key)
      use:User_Code    = p_web.GSV('BookingUserCode')!glo:Password
      If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
          ! Found
          tmp:PrintedBy = Clip(use:Forename) & ' ' & Clip(use:Surname)
      Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          ! Error
      End ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('HandoverReport',ProgressWindow)            ! Restore window settings from non-volatile store
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    loc:pdfname = '.\$$$' & format(random(1,99999),@n05) &'.pdf'
    ProgressWindow{prop:hide} = 1
  End
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:HANDOJOB, ?Progress:PctText, Progress:Thermometer, ProgressMgr, haj:JobNumber)
  ThisReport.AddSortOrder(haj:JobNumberKey)
  ThisReport.AddRange(haj:RefNumber,LimitValue)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:HANDOJOB.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      ReportNoRecords(p_web)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:HANDOJOB.Close
    Relate:JOBS.Close
  END
  IF SELF.Opened
    INIMgr.Update('HandoverReport',ProgressWindow)         ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    Report$?ReportPageNumber{PROP:PageNo} = True
  END
  !*** PDF-Tools v4.1 Start (ABC)
  IF ~ReturnValue                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR5.Init(Report, loc:pdfname,,PDFA_OFF)            !  Added by PDF-Tools Templates v4.1
    PDFXTR5.SetPaper(CHOOSE(SYSTEM{PROP:Autopaper}=True,Report{PROPPRINT:Paper},PAPER:USER), CHOOSE(Report{PROP:Thous}=True,PROP:Thous,CHOOSE(Report{PROP:MM}=True,PROP:MM,CHOOSE(Report{PROP:Points}=True,PROP:Points,0))), Report{PROPPRINT:PaperWidth}, Report{PROPPRINT:PaperHeight}, Report{PROP:Landscape}) !  Added by PDF-Tools Templates v4.1
    PDFXTR5.SetCompression(TRUE,TRUE,FALSE,COMPRTYPE_C_AUTO,75,COMPRTYPE_I_AUTO,COMPRTYPE_M_AUTO) !  Added by PDF-Tools Templates v4.1
    PDFXTR5.SetAuthor('ServiceBase 3g')          !  Added by PDF-Tools Templates v4.1
    PDFXTR5.SetTitle('Handover Report')          !  Added by PDF-Tools Templates v4.1
    PDFXTR5.SetSubject('ServiceBase 3g')         !  Added by PDF-Tools Templates v4.1
    PDFXTR5.SetPdfCreator('ServiceBase 3g')      !  Added by PDF-Tools Templates v4.1
    PDFXTR5.SetKeywords()                        !  Added by PDF-Tools Templates v4.1
    PDFXTR5.EnableLinkAnalyzer(True)             !  Added by PDF-Tools Templates v4.1
    PDFXTR5.SetSpecVersion(SPECVERSION_15,TRUE)  !  Added by PDF-Tools Templates v4.1
    PDFXTR5.SetPageLayout(PAGELAYOUT_SINGLEPAGE) !  Added by PDF-Tools Templates v4.1
    PDFXTR5.SetPageMode(PAGEMODE_NONE)           !  Added by PDF-Tools Templates v4.1
    IF PAGEMODE_NONE = PAGEMODE_FULLSCREEN       !  Added by PDF-Tools Templates v4.1
      PDFXTR5:vpf = VP_FSPM_NONE                 !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
      PDFXTR5:vpf = 0                            !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR5:vpf = BOR(PDFXTR5:vpf,VP_HIDETOOLBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR5:vpf = BOR(PDFXTR5:vpf,VP_HIDEMENUBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR5:vpf = BOR(PDFXTR5:vpf,VP_HIDEWINDOWUI). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR5:vpf = BOR(PDFXTR5:vpf,VP_FITWINDOW). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR5:vpf = BOR(PDFXTR5:vpf,VP_CENTERWINDOW). !  Added by PDF-Tools Templates v4.1
      IF TRUE                                     THEN PDFXTR5:vpf = BOR(PDFXTR5:vpf,VP_DISPLAYDOCTITLE). !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR5.SetViewerPrefs(PDFXTR5:vpf)          !  Added by PDF-Tools Templates v4.1
    PDFXTR5.SetPreview(False)                    !  Added by PDF-Tools Templates v4.1
  END
  !*** PDF-Tools v4.1 End
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeCloseEvent()
  !*** PDF-Tools v4.1 Start (ABC)
  PDFXTR5.Kill()
  !*** PDF-Tools v4.1 End
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
      !when each record is fetched this code is run.
      
      JobCount += 1
  
      If han:HandoverType = 'JOB'
          Access:JOBS.Clearkey(job:Ref_Number_Key)
          job:Ref_Number  = haj:JobNumber
          If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
              ! Found
              If job:Exchange_Unit_Number <> 0
                  haj:Accessories = ''
              End ! If job:Exchange_Unit_Number <> 0
          End ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      End !if job
   
      
      print(rpt:detail)
  ReturnValue = PARENT.TakeRecord()
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  !*** PDF-Tools v4.1 Start (ABC)
  ReturnValue = False                            !  Added by PDF-Tools Templates v4.1
    PDFXTR5:rtn = PDFXTR5.Generate(SELF.ImageQueue,True, , ) !  Added by PDF-Tools Templates v4.1
    IF PDFXTR5:rtn THEN                          !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
  RETURN ReturnValue
  !*** PDF-Tools v4.1 End
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
  RETURN ReturnValue

