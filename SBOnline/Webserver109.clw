

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER109.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
TransferParts_C      PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure

  CODE
    count# = 0

    access:warparts.clearkey(wpr:part_number_key)
    wpr:ref_number  = p_web.GSV('job:ref_number')
    set(wpr:part_number_key,wpr:part_number_key)
    loop
        if access:warparts.next()
           break
        end !if
        if wpr:ref_number  <> p_web.GSV('job:ref_number')      |
            then break.  ! end if
        If wpr:pending_ref_number <> ''
            access:ordpend.clearkey(ope:ref_number_key)
            ope:ref_number = wpr:pending_ref_number
            if access:ordpend.tryfetch(ope:ref_number_key) = Level:Benign
                ope:part_type = 'JOB'
                access:ordpend.update()
            End!if access:ordpend.tryfetch(ope:ref_number_key) = Level:Benign
        End!If wpr:pending_ref_number <> ''
        If wpr:order_number <> ''
            access:ordparts.clearkey(orp:record_number_key)
            orp:record_number   = wpr:order_number
            If access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
                orp:part_type = 'JOB'
                access:ordparts.update()
            End!If access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
        End!If wpr:order_number <> ''
        get(parts,0)
        if access:parts.primerecord() = Level:Benign
            record_number$  = par:record_number
            par:record      :=: wpr:record
            par:record_number   = record_number$
            if access:parts.insert()
                access:parts.cancelautoinc()
            End!if access:parts.insert()
        End!if access:parts.primerecord() = Level:Benign
        Delete(warparts)
        count# += 1
    end !loop

    if count# <> 0
        get(audit,0)
        if access:audit.primerecord() = level:benign
            aud:ref_number    = p_web.GSV('job:ref_number')
            aud:date          = today()
            aud:time          = clock()
            aud:type          = 'JOB'
            !access:users.clearkey(use:password_key)
            !use:password = glo:password
            !access:users.fetch(use:password_key)
            aud:user = p_web.GSV('BookingUserCode') ! Do not use Global Vars!! (DBH: 21/11/2014)use:user_code
            aud:action        = 'WARRANTY PARTS TRANSFERRED TO CHARGEABLE'
            access:audit.insert()
        end!�if access:audit.primerecord() = level:benign

    End!if count# <> 0
    Access:MODELNUM.Clearkey(mod:Model_Number_Key)
    mod:Model_Number    = p_web.GSV('job:Model_NUmber')
    If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
        !Found
        pendingJob(p_web)
        p_web.SSV('job:Manufacturer',mod:Manufacturer)
    Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
