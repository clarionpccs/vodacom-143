

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER371.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER004.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER022.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER204.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER285.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER372.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER373.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER376.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER377.INC'),ONCE        !Req'd for module callout resolution
                     END


FormJobProgressCriteria PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locJobProgressStartDate DATE                               !
locJobProgressEndDate DATE                                 !
locJobProgressLocation LONG                                !
locJobProgressStatusFilter LONG                            !
locJobProgressStatus STRING(30)                            !
locJobProgressModelNumberFilter LONG                       !
locJobProgressModelNumber STRING(30)                       !
locJobProgressAccountNumberFilter LONG                     !
locJobProgressAccountNumber STRING(30)                     !
GlobalIMEINumber     STRING(30)                            !
GlobalMobileNumber   STRING(30)                            !
GlobalJobNumber      STRING(30)                            !
                    MAP
GetDays                 PROCEDURE(LONG pSat,LONG pSun,DATE pDate),LONG
LoadJobIntoQueue        PROCEDURE()
                    END ! MAP
FilesOpened     Long
TRADEACC::State  USHORT
TagFile::State  USHORT
SBO_GenericFile::State  USHORT
SUBTRACC::State  USHORT
MODELNUM::State  USHORT
WEBJOB::State  USHORT
JOBS::State  USHORT
TRAHUBAC::State  USHORT
AUDSTATS::State  USHORT
STATUS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormJobProgressCriteria')
  loc:formname = 'FormJobProgressCriteria_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    VodacomClass.AddToFileLog(p_web.SessionID,'JobProgress','Start') ! Debug
    p_web.FormReady('FormJobProgressCriteria','')
      do SendPacket
      Do UpdateText
      do SendPacket
      Do styles
      do SendPacket
    p_web._DivHeader('FormJobProgressCriteria',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormJobProgressCriteria',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormJobProgressCriteria',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormJobProgressCriteria',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormJobProgressCriteria',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormJobProgressCriteria',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormJobProgressCriteria',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
DeleteSessionValues ROUTINE ! Auto generated by Bryan's Template
    ! Local Variables
    ! p_web.DeleteSessionValue('Ans') ! Excluded
    ! p_web.DeleteSessionValue('locJobProgressStartDate') ! Excluded
    ! p_web.DeleteSessionValue('locJobProgressEndDate') ! Excluded
    p_web.DeleteSessionValue('locJobProgressLocation')
    p_web.DeleteSessionValue('locJobProgressStatusFilter')
    p_web.DeleteSessionValue('locJobProgressStatus')
    p_web.DeleteSessionValue('locJobProgressModelNumberFilter')
    p_web.DeleteSessionValue('locJobProgressModelNumber')
    p_web.DeleteSessionValue('locJobProgressAccountNumberFilter')
    p_web.DeleteSessionValue('locJobProgressAccountNumber')
    p_web.DeleteSessionValue('GlobalIMEINumber')
    p_web.DeleteSessionValue('GlobalMobileNumber')
    p_web.DeleteSessionValue('GlobalJobNumber')

    ! Other Variables
OpenFiles  ROUTINE
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(TagFile)
  p_web._OpenFile(SBO_GenericFile)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(MODELNUM)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(TRAHUBAC)
  p_web._OpenFile(AUDSTATS)
  p_web._OpenFile(STATUS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(TagFile)
  p_Web._CloseFile(SBO_GenericFile)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(MODELNUM)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(TRAHUBAC)
  p_Web._CloseFile(AUDSTATS)
  p_Web._CloseFile(STATUS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormJobProgressCriteria_form:inited_',1)
  do RestoreMem

CancelForm  Routine
    DO DeleteSessionValues  

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('locJobProgressStartDate')
    p_web.SetPicture('locJobProgressStartDate','@d06b')
  End
  p_web.SetSessionPicture('locJobProgressStartDate','@d06b')
  If p_web.IfExistsValue('locJobProgressEndDate')
    p_web.SetPicture('locJobProgressEndDate','@d06b')
  End
  p_web.SetSessionPicture('locJobProgressEndDate','@d06b')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'locJobProgressStatus'
    p_web.setsessionvalue('showtab_FormJobProgressCriteria',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(STATUS)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locJobProgressStatus')
  Of 'locJobProgressStatus'
    p_web.setsessionvalue('showtab_FormJobProgressCriteria',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(STATUS)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locJobProgressStatus')
  Of 'locJobProgressStatus'
    p_web.setsessionvalue('showtab_FormJobProgressCriteria',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(STATUS)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locJobProgressStatus')
  Of 'locJobProgressModelNumber'
    p_web.setsessionvalue('showtab_FormJobProgressCriteria',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MODELNUM)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locJobProgressAccountNumberFilter')
  Of 'locJobProgressAccountNumber'
    p_web.setsessionvalue('showtab_FormJobProgressCriteria',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(SUBTRACC)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locJobProgressAccountNumber')
  Of 'locJobProgressAccountNumber'
    p_web.setsessionvalue('showtab_FormJobProgressCriteria',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(TRAHUBAC)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locJobProgressAccountNumber')
  End
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('GlobalJobNumber',GlobalJobNumber)
  p_web.SetSessionValue('GlobalIMEINumber',GlobalIMEINumber)
  p_web.SetSessionValue('GlobalMobileNumber',GlobalMobileNumber)
  p_web.SetSessionValue('locJobProgressStartDate',locJobProgressStartDate)
  p_web.SetSessionValue('locJobProgressEndDate',locJobProgressEndDate)
  p_web.SetSessionValue('locJobProgressLocation',locJobProgressLocation)
  p_web.SetSessionValue('locJobProgressStatusFilter',locJobProgressStatusFilter)
  p_web.SetSessionValue('locJobProgressStatus',locJobProgressStatus)
  p_web.SetSessionValue('locJobProgressStatus',locJobProgressStatus)
  p_web.SetSessionValue('locJobProgressStatus',locJobProgressStatus)
  p_web.SetSessionValue('locJobProgressModelNumberFilter',locJobProgressModelNumberFilter)
  p_web.SetSessionValue('locJobProgressModelNumber',locJobProgressModelNumber)
  p_web.SetSessionValue('locJobProgressAccountNumberFilter',locJobProgressAccountNumberFilter)
  p_web.SetSessionValue('locJobProgressAccountNumber',locJobProgressAccountNumber)
  p_web.SetSessionValue('locJobProgressAccountNumber',locJobProgressAccountNumber)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('GlobalJobNumber')
    GlobalJobNumber = p_web.GetValue('GlobalJobNumber')
    p_web.SetSessionValue('GlobalJobNumber',GlobalJobNumber)
  End
  if p_web.IfExistsValue('GlobalIMEINumber')
    GlobalIMEINumber = p_web.GetValue('GlobalIMEINumber')
    p_web.SetSessionValue('GlobalIMEINumber',GlobalIMEINumber)
  End
  if p_web.IfExistsValue('GlobalMobileNumber')
    GlobalMobileNumber = p_web.GetValue('GlobalMobileNumber')
    p_web.SetSessionValue('GlobalMobileNumber',GlobalMobileNumber)
  End
  if p_web.IfExistsValue('locJobProgressStartDate')
    locJobProgressStartDate = p_web.dformat(clip(p_web.GetValue('locJobProgressStartDate')),'@d06b')
    p_web.SetSessionValue('locJobProgressStartDate',locJobProgressStartDate)
  End
  if p_web.IfExistsValue('locJobProgressEndDate')
    locJobProgressEndDate = p_web.dformat(clip(p_web.GetValue('locJobProgressEndDate')),'@d06b')
    p_web.SetSessionValue('locJobProgressEndDate',locJobProgressEndDate)
  End
  if p_web.IfExistsValue('locJobProgressLocation')
    locJobProgressLocation = p_web.GetValue('locJobProgressLocation')
    p_web.SetSessionValue('locJobProgressLocation',locJobProgressLocation)
  End
  if p_web.IfExistsValue('locJobProgressStatusFilter')
    locJobProgressStatusFilter = p_web.GetValue('locJobProgressStatusFilter')
    p_web.SetSessionValue('locJobProgressStatusFilter',locJobProgressStatusFilter)
  End
  if p_web.IfExistsValue('locJobProgressStatus')
    locJobProgressStatus = p_web.GetValue('locJobProgressStatus')
    p_web.SetSessionValue('locJobProgressStatus',locJobProgressStatus)
  End
  if p_web.IfExistsValue('locJobProgressStatus')
    locJobProgressStatus = p_web.GetValue('locJobProgressStatus')
    p_web.SetSessionValue('locJobProgressStatus',locJobProgressStatus)
  End
  if p_web.IfExistsValue('locJobProgressStatus')
    locJobProgressStatus = p_web.GetValue('locJobProgressStatus')
    p_web.SetSessionValue('locJobProgressStatus',locJobProgressStatus)
  End
  if p_web.IfExistsValue('locJobProgressModelNumberFilter')
    locJobProgressModelNumberFilter = p_web.GetValue('locJobProgressModelNumberFilter')
    p_web.SetSessionValue('locJobProgressModelNumberFilter',locJobProgressModelNumberFilter)
  End
  if p_web.IfExistsValue('locJobProgressModelNumber')
    locJobProgressModelNumber = p_web.GetValue('locJobProgressModelNumber')
    p_web.SetSessionValue('locJobProgressModelNumber',locJobProgressModelNumber)
  End
  if p_web.IfExistsValue('locJobProgressAccountNumberFilter')
    locJobProgressAccountNumberFilter = p_web.GetValue('locJobProgressAccountNumberFilter')
    p_web.SetSessionValue('locJobProgressAccountNumberFilter',locJobProgressAccountNumberFilter)
  End
  if p_web.IfExistsValue('locJobProgressAccountNumber')
    locJobProgressAccountNumber = p_web.GetValue('locJobProgressAccountNumber')
    p_web.SetSessionValue('locJobProgressAccountNumber',locJobProgressAccountNumber)
  End
  if p_web.IfExistsValue('locJobProgressAccountNumber')
    locJobProgressAccountNumber = p_web.GetValue('locJobProgressAccountNumber')
    p_web.SetSessionValue('locJobProgressAccountNumber',locJobProgressAccountNumber)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormJobProgressCriteria_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    ! #13237 Clear filters when opening the screen (DBH: 21/03/2014)
    IF (p_web.GetValue('FirstTime') = 1)
        ClearSBOGenericFile(p_web)
        p_web.SSV('locJobProgressStartDate','')
        p_web.SSV('locJobProgressEndDate','')
        p_web.SSV('locJobProgressLocation','')
        p_web.SSV('locJobProgressStatusFilter','')
        p_web.SSV('locJobProgressStatus','')
        p_web.SSV('locJobProgressModelNumberFilter','')
        p_web.SSV('locJobProgressModelNumber','')
        p_web.SSV('locJobProgressAccountNumberFilter','')
        p_web.SSV('locJobProgressAccountNumber','')
        p_web.SSV('TotalAvailableJobs','')
        p_web.SSV('BrowseJobsTitle','')
        LoadJobIntoQueue()
    END ! IF
    
    
    ! By default, the date range for the Job Progress browse is 2 days (DBH: 28/11/2013)
    IF (p_web.GSV('locJobProgressStartDate') = '' AND p_web.GSV('locJobProgressEndDate') = '')
        p_web.SSV('locJobProgressStartDate',TODAY() - 2)
        p_web.SSV('locJobProgressEndDate',TODAY())  
    END !  IF   
    
    p_web.SSV('Filter:ModelNumber',TRUE) ! So model number lookup shows ALL model numbers
    p_web.SSV('FranchiseAccount',p_web.GSV('locJobProgressAccountNumberFilter')) ! Set Account Filter  
    
    p_web.DeleteSessionValue('mobilesearch')  ! #13368 Clear variable (DBH: 29/08/2014)
    
    BHAddToDebugLog('FormJobProgressCriteria: GenerateForm. sbogen:recordNumber = ' & p_web.GSV('sbogen:recordNumber'))
      p_web.site.CancelButton.TextValue = 'Close'
      p_web.site.CancelButton.Image = 'images/psave.png'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 GlobalJobNumber = p_web.RestoreValue('GlobalJobNumber')
 GlobalIMEINumber = p_web.RestoreValue('GlobalIMEINumber')
 GlobalMobileNumber = p_web.RestoreValue('GlobalMobileNumber')
 locJobProgressStartDate = p_web.RestoreValue('locJobProgressStartDate')
 locJobProgressEndDate = p_web.RestoreValue('locJobProgressEndDate')
 locJobProgressLocation = p_web.RestoreValue('locJobProgressLocation')
 locJobProgressStatusFilter = p_web.RestoreValue('locJobProgressStatusFilter')
 locJobProgressStatus = p_web.RestoreValue('locJobProgressStatus')
 locJobProgressStatus = p_web.RestoreValue('locJobProgressStatus')
 locJobProgressStatus = p_web.RestoreValue('locJobProgressStatus')
 locJobProgressModelNumberFilter = p_web.RestoreValue('locJobProgressModelNumberFilter')
 locJobProgressModelNumber = p_web.RestoreValue('locJobProgressModelNumber')
 locJobProgressAccountNumberFilter = p_web.RestoreValue('locJobProgressAccountNumberFilter')
 locJobProgressAccountNumber = p_web.RestoreValue('locJobProgressAccountNumber')
 locJobProgressAccountNumber = p_web.RestoreValue('locJobProgressAccountNumber')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferFormJobProgressCriteria')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormJobProgressCriteria_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormJobProgressCriteria_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormJobProgressCriteria_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'IndexPage'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormJobProgressCriteria" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormJobProgressCriteria" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormJobProgressCriteria" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Browse Jobs') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Browse Jobs',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormJobProgressCriteria">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormJobProgressCriteria" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormJobProgressCriteria')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Global Searches') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Criteria') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate(p_web.GSV('BrowseJobsTitle')) & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Tools') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormJobProgressCriteria')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormJobProgressCriteria'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormJobProgressCriteria_BrowseJobProgress_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='STATUS'
          If Not (p_web.GSV('locJobProgressStatusFilter') <> 2)
            p_web.SetValue('SelectField',clip(loc:formname) & '.locJobProgressStatus')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='STATUS'
          If Not (p_web.GSV('locJobProgressStatusFilter') <> 3)
            p_web.SetValue('SelectField',clip(loc:formname) & '.locJobProgressStatus')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='STATUS'
          If Not (p_web.GSV('locJobProgressModelNumberFilter') <> 1)
            p_web.SetValue('SelectField',clip(loc:formname) & '.locJobProgressModelNumber')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='MODELNUM'
          If Not (p_web.GSV('locJobProgressAccountNumberFilter') <> 1)
            p_web.SetValue('SelectField',clip(loc:formname) & '.locJobProgressAccountNumber')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='SUBTRACC'
          If Not (p_web.GSV('locJobProgressAccountNumberFilter') <> 2)
            p_web.SetValue('SelectField',clip(loc:formname) & '.locJobProgressAccountNumber')
          End
    End
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.GlobalJobNumber')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab4'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormJobProgressCriteria')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab4'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel4">'&CRLF &|
                                    '  <div id="panel4Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Global Searches') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel4Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormJobProgressCriteria_4">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Global Searches')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab4" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab4">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Global Searches')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Global Searches')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Global Searches')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::GlobalJobNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&' colspan="3">'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::GlobalJobNumber
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnJobNoSearch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::hiddenField
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::GlobalIMEINumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&' colspan="3">'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::GlobalIMEINumber
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnGlobalIMEISearch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::GlobalMobileNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&' colspan="3">'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::GlobalMobileNumber
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnMobileNoSearch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Criteria') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormJobProgressCriteria_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& ' colspan="1">'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::txtDescription
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&' colspan="6">'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::txtDescription
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locJobProgressStartDate
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locJobProgressStartDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locJobProgressEndDate
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locJobProgressEndDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnFindJobs
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::_Line
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locJobProgressLocation
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&' colspan="3">'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locJobProgressLocation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& ' rowspan="3">'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locJobProgressStatusFilter
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&' colspan="3" rowspan="3">'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locJobProgressStatusFilter
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locJobProgressStatus
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locJobProgressStatus
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locJobProgressStatus_E
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locJobProgressStatus_E
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locJobProgressStatus_L
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locJobProgressStatus_L
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locJobProgressModelNumberFilter
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&' colspan="3">'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locJobProgressModelNumberFilter
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locJobProgressModelNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locJobProgressModelNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& ' rowspan="2">'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locJobProgressAccountNumberFilter
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&' colspan="3" rowspan="2">'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locJobProgressAccountNumberFilter
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locJobProgressAccountNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locJobProgressAccountNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locJobProgressAccountNumber_Generic
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locJobProgressAccountNumber_Generic
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate(p_web.GSV('BrowseJobsTitle')) &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormJobProgressCriteria_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate(p_web.GSV('BrowseJobsTitle'))&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate(p_web.GSV('BrowseJobsTitle'))&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate(p_web.GSV('BrowseJobsTitle'))&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate(p_web.GSV('BrowseJobsTitle'))&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::txtTotalJobs
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::BrowseJobs
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      LoadJobIntoQueue()  
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnTagAll
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnUnTagAll
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::locJobStatus
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab3  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Tools') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormJobProgressCriteria_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Tools')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Tools')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Tools')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Tools')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnSMSTaggedJobs
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnPrintRoutines
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnPaymentDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnChangeStatus
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnChangeCourier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    VodacomClass.AddToFileLog(p_web.SessionID,'JobProgress','Page Loaded') ! Debug    
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::GlobalJobNumber  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('GlobalJobNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Job No Search')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::GlobalJobNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('GlobalJobNumber',p_web.GetValue('NewValue'))
    GlobalJobNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::GlobalJobNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('GlobalJobNumber',p_web.GetValue('Value'))
    GlobalJobNumber = p_web.GetValue('Value')
  End
  do Value::GlobalJobNumber
  do SendAlert

Value::GlobalJobNumber  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('GlobalJobNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- GlobalJobNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('GlobalJobNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''GlobalJobNumber'',''formjobprogresscriteria_globaljobnumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','GlobalJobNumber',p_web.GetSessionValueFormat('GlobalJobNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobProgressCriteria_' & p_web._nocolon('GlobalJobNumber') & '_value')


Validate::btnJobNoSearch  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnJobNoSearch',p_web.GetValue('NewValue'))
    do Value::btnJobNoSearch
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    ! Quick add one record
    ClearSBOGenericFile(p_web)
            
    p_web.SSV('GlobalIMEINumber','')
    p_web.SSV('GlobalMobileNumber','')
    p_web.SSV('BrowseJobsTitle','Matching Job')
            
    Access:WEBJOB.ClearKey(wob:HeadRefNumberKey)
    wob:HeadAccountNumber = p_web.GSV('BookingAccount')
    wob:RefNumber = p_web.GSV('GlobalJobNumber')
    IF (Access:WEBJOB.TryFetch(wob:HeadRefNumberKey) = Level:Benign)
        Access:SBO_GenericFile.ClearKey(sbogen:Long2Key)
        sbogen:SessionID = p_web.SessionID
        sbogen:Long2 = wob:RecordNumber
        IF (Access:SBO_GenericFile.TryFetch(sbogen:Long2Key) = Level:Benign)
                            
        ELSE ! IF
            IF (Access:SBO_GenericFile.PrimeRecord() = Level:Benign)
                sbogen:SessionID = p_web.SessionID
                sbogen:Long1 = wob:RefNumber
                sbogen:Long2 = wob:RecordNumber
                IF (Access:SBO_GenericFile.TryInsert())
                    Access:SBO_GenericFile.CancelAutoInc()
                END ! IF
            END ! IF
        END ! IF
        p_web.SSV('TotalAvailableJobs',1)
    ELSE
        loc:alert = 'Selected Job Number Not Found'
        loc:invalid = 'GlobalJobNumber'
    END ! IF
  do SendAlert
  do Value::BrowseJobs  !1
  do Value::GlobalIMEINumber  !1
  do Value::GlobalJobNumber  !1
  do Value::GlobalMobileNumber  !1
  do Value::locJobStatus  !1

Value::btnJobNoSearch  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('btnJobNoSearch') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnJobNoSearch'',''formjobprogresscriteria_btnjobnosearch_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnJobNoSearch','Search','button-flat',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobProgressCriteria_' & p_web._nocolon('btnJobNoSearch') & '_value')


Validate::hiddenField  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('hiddenField',p_web.GetValue('NewValue'))
    do Value::hiddenField
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::hiddenField  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('hiddenField') & '_value','adiv')
  loc:extra = ''
  ! --- HIDDEN --- 
    packet = clip(packet) & p_web.CreateInput('hidden','',p_web.GetSessionValue('')) & '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::GlobalIMEINumber  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('GlobalIMEINumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Global IMEI No Search')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::GlobalIMEINumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('GlobalIMEINumber',p_web.GetValue('NewValue'))
    GlobalIMEINumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::GlobalIMEINumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('GlobalIMEINumber',p_web.GetValue('Value'))
    GlobalIMEINumber = p_web.GetValue('Value')
  End
  do Value::GlobalIMEINumber
  do SendAlert

Value::GlobalIMEINumber  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('GlobalIMEINumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- GlobalIMEINumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('GlobalIMEINumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''GlobalIMEINumber'',''formjobprogresscriteria_globalimeinumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','GlobalIMEINumber',p_web.GetSessionValueFormat('GlobalIMEINumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobProgressCriteria_' & p_web._nocolon('GlobalIMEINumber') & '_value')


Validate::btnGlobalIMEISearch  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnGlobalIMEISearch',p_web.GetValue('NewValue'))
    do Value::btnGlobalIMEISearch
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnGlobalIMEISearch  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('btnGlobalIMEISearch') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnGlobalIMEISearch','Search','button-flat',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PageProcess?' &'ProcessType=BuildJobsProgressList&ReturnURL=FormJobProgressCriteria&RedirectURL=FormJobProgressCriteria&searchtype=imei')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()


Prompt::GlobalMobileNumber  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('GlobalMobileNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Mobile No Search')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::GlobalMobileNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('GlobalMobileNumber',p_web.GetValue('NewValue'))
    GlobalMobileNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::GlobalMobileNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('GlobalMobileNumber',p_web.GetValue('Value'))
    GlobalMobileNumber = p_web.GetValue('Value')
  End
  do Value::GlobalMobileNumber
  do SendAlert

Value::GlobalMobileNumber  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('GlobalMobileNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- GlobalMobileNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('GlobalMobileNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''GlobalMobileNumber'',''formjobprogresscriteria_globalmobilenumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','GlobalMobileNumber',p_web.GetSessionValueFormat('GlobalMobileNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobProgressCriteria_' & p_web._nocolon('GlobalMobileNumber') & '_value')


Validate::btnMobileNoSearch  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnMobileNoSearch',p_web.GetValue('NewValue'))
    do Value::btnMobileNoSearch
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnMobileNoSearch  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('btnMobileNoSearch') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnMobileNoSearch','Search','button-flat',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PageProcess?' &'ProcessType=BuildJobsProgressList&ReturnURL=FormJobProgressCriteria&RedirectURL=FormJobProgressCriteria&searchtype=mobileno')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()


Prompt::txtDescription  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('txtDescription') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::txtDescription  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtDescription',p_web.GetValue('NewValue'))
    do Value::txtDescription
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtDescription  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('txtDescription') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('bold green')&'">' & p_web.Translate('To display jobs, select a booking date range and press "Build Jobs List". <br>Then use the filters below to limit the data displayed',1) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locJobProgressStartDate  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressStartDate') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Booking Start Date')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locJobProgressStartDate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locJobProgressStartDate',p_web.GetValue('NewValue'))
    locJobProgressStartDate = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locJobProgressStartDate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locJobProgressStartDate',p_web.dFormat(p_web.GetValue('Value'),'@d06b'))
    locJobProgressStartDate = p_web.Dformat(p_web.GetValue('Value'),'@d06b') !
  End
  do Value::locJobProgressStartDate
  do SendAlert
  do Value::btnFindJobs  !1

Value::locJobProgressStartDate  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressStartDate') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locJobProgressStartDate
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locJobProgressStartDate')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locJobProgressStartDate'',''formjobprogresscriteria_locjobprogressstartdate_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobProgressStartDate')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locJobProgressStartDate',p_web.GetSessionValue('locJobProgressStartDate'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06b',loc:javascript,,) & '<13,10>'
  do SendPacket
  packet = clip(packet) & '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar' & |
                  '(locJobProgressStartDate,''dd/mm/yyyy'',this); Date.disabled=true;' & |
                  'sv(''...'',''FormJobProgressCriteria_pickdate_value'',1,FieldValue(this,1));nextFocus(FormJobProgressCriteria_frm,'''',0);" ' & |
                  'value="Select Date" name="Date" type="button">...</button>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressStartDate') & '_value')


Prompt::locJobProgressEndDate  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressEndDate') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('End Date')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locJobProgressEndDate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locJobProgressEndDate',p_web.GetValue('NewValue'))
    locJobProgressEndDate = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locJobProgressEndDate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locJobProgressEndDate',p_web.dFormat(p_web.GetValue('Value'),'@d06b'))
    locJobProgressEndDate = p_web.Dformat(p_web.GetValue('Value'),'@d06b') !
  End
  do Value::locJobProgressEndDate
  do SendAlert
  do Value::btnFindJobs  !1

Value::locJobProgressEndDate  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressEndDate') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locJobProgressEndDate
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locJobProgressEndDate')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locJobProgressEndDate'',''formjobprogresscriteria_locjobprogressenddate_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobProgressEndDate')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locJobProgressEndDate',p_web.GetSessionValue('locJobProgressEndDate'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06b',loc:javascript,,) & '<13,10>'
  do SendPacket
  packet = clip(packet) & '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar' & |
                  '(locJobProgressEndDate,''dd/mm/yyyy'',this); Date.disabled=true;' & |
                  'sv(''...'',''FormJobProgressCriteria_pickdate_value'',1,FieldValue(this,1));nextFocus(FormJobProgressCriteria_frm,'''',0);" ' & |
                  'value="Select Date" name="Date" type="button">...</button>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressEndDate') & '_value')


Validate::btnFindJobs  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnFindJobs',p_web.GetValue('NewValue'))
    do Value::btnFindJobs
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnFindJobs  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('btnFindJobs') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnFindJobs','Build Jobs List','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PageProcess?' &'ProcessType=BuildJobsProgressList&ReturnURL=FormJobProgressCriteria&RedirectURL=FormJobProgressCriteria&searchtype=jobs')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobProgressCriteria_' & p_web._nocolon('btnFindJobs') & '_value')


Validate::_Line  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('_Line',p_web.GetValue('NewValue'))
    do Value::_Line
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::_Line  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('_Line') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locJobProgressLocation  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressLocation') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Job Location Filter')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locJobProgressLocation  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locJobProgressLocation',p_web.GetValue('NewValue'))
    locJobProgressLocation = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locJobProgressLocation
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locJobProgressLocation',p_web.GetValue('Value'))
    locJobProgressLocation = p_web.GetValue('Value')
  End
  do Value::locJobProgressLocation
  do SendAlert
  do Value::BrowseJobs  !1
  do Value::locJobStatus  !1

Value::locJobProgressLocation  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressLocation') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- locJobProgressLocation
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locJobProgressLocation')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locJobProgressLocation') = ''
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locJobProgressLocation'',''formjobprogresscriteria_locjobprogresslocation_value'',1,'''&clip('')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobProgressLocation')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locJobProgressLocation',clip(''),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locJobProgressLocation_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('All Jobs') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locJobProgressLocation') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locJobProgressLocation'',''formjobprogresscriteria_locjobprogresslocation_value'',1,'''&clip(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobProgressLocation')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locJobProgressLocation',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locJobProgressLocation_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('At RRC') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locJobProgressLocation') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locJobProgressLocation'',''formjobprogresscriteria_locjobprogresslocation_value'',1,'''&clip(2)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobProgressLocation')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locJobProgressLocation',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locJobProgressLocation_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Not At RRC') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressLocation') & '_value')


Prompt::locJobProgressStatusFilter  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressStatusFilter') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Status Filter')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locJobProgressStatusFilter  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locJobProgressStatusFilter',p_web.GetValue('NewValue'))
    locJobProgressStatusFilter = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locJobProgressStatusFilter
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locJobProgressStatusFilter',p_web.GetValue('Value'))
    locJobProgressStatusFilter = p_web.GetValue('Value')
  End
  do Value::locJobProgressStatusFilter
  do SendAlert
  do Prompt::locJobProgressStatus
  do Value::locJobProgressStatus  !1
  do Prompt::locJobProgressStatus_E
  do Value::locJobProgressStatus_E  !1
  do Prompt::locJobProgressStatus_L
  do Value::locJobProgressStatus_L  !1
  do Value::BrowseJobs  !1
  do Value::btnSMSTaggedJobs  !1
  do Value::btnTagAll  !1
  do Value::btnUnTagAll  !1

Value::locJobProgressStatusFilter  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressStatusFilter') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- locJobProgressStatusFilter
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locJobProgressStatusFilter')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locJobProgressStatusFilter') = ''
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locJobProgressStatusFilter'',''formjobprogresscriteria_locjobprogressstatusfilter_value'',1,'''&clip('')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobProgressStatusFilter')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locJobProgressStatusFilter',clip(''),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locJobProgressStatusFilter_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('All Jobs') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locJobProgressStatusFilter') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locJobProgressStatusFilter'',''formjobprogresscriteria_locjobprogressstatusfilter_value'',1,'''&clip(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobProgressStatusFilter')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locJobProgressStatusFilter',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locJobProgressStatusFilter_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Job Status') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locJobProgressStatusFilter') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locJobProgressStatusFilter'',''formjobprogresscriteria_locjobprogressstatusfilter_value'',1,'''&clip(2)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobProgressStatusFilter')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locJobProgressStatusFilter',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locJobProgressStatusFilter_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Exchange Status') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locJobProgressStatusFilter') = 3
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locJobProgressStatusFilter'',''formjobprogresscriteria_locjobprogressstatusfilter_value'',1,'''&clip(3)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobProgressStatusFilter')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locJobProgressStatusFilter',clip(3),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locJobProgressStatusFilter_4') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Loan Status') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressStatusFilter') & '_value')


Prompt::locJobProgressStatus  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressStatus') & '_prompt',Choose(p_web.GSV('locJobProgressStatusFilter') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Job Status')
  If p_web.GSV('locJobProgressStatusFilter') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressStatus') & '_prompt')

Validate::locJobProgressStatus  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locJobProgressStatus',p_web.GetValue('NewValue'))
    locJobProgressStatus = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locJobProgressStatus
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locJobProgressStatus',p_web.GetValue('Value'))
    locJobProgressStatus = p_web.GetValue('Value')
  End
  p_Web.SetValue('lookupfield','locJobProgressStatus')
  do AfterLookup
  do Value::locJobProgressStatus
  do SendAlert
  do Value::BrowseJobs  !1
  do Value::btnSMSTaggedJobs  !1
  do Value::btnTagAll  !1
  do Value::btnUnTagAll  !1

Value::locJobProgressStatus  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressStatus') & '_value',Choose(p_web.GSV('locJobProgressStatusFilter') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobProgressStatusFilter') <> 1)
  ! --- STRING --- locJobProgressStatus
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locJobProgressStatus')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(20) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locJobProgressStatus'',''formjobprogresscriteria_locjobprogressstatus_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobProgressStatus')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','locJobProgressStatus',p_web.GetSessionValueFormat('locJobProgressStatus'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectStatus?StatusType=JOB')&'?LookupField=locJobProgressStatus&Tab=3&ForeignField=sts:Status&_sort=sts:Status&Refresh=sort&LookupFrom=FormJobProgressCriteria&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressStatus') & '_value')


Prompt::locJobProgressStatus_E  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressStatus_E') & '_prompt',Choose(p_web.GSV('locJobProgressStatusFilter') <> 2,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Exchange Status')
  If p_web.GSV('locJobProgressStatusFilter') <> 2
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressStatus_E') & '_prompt')

Validate::locJobProgressStatus_E  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locJobProgressStatus_E',p_web.GetValue('NewValue'))
    locJobProgressStatus = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locJobProgressStatus_E
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locJobProgressStatus',p_web.GetValue('Value'))
    locJobProgressStatus = p_web.GetValue('Value')
  End
  p_Web.SetValue('lookupfield','locJobProgressStatus_E')
  do AfterLookup
  do Value::locJobProgressStatus_E
  do SendAlert
  do Value::BrowseJobs  !1
  do Value::btnSMSTaggedJobs  !1
  do Value::btnTagAll  !1
  do Value::btnUnTagAll  !1

Value::locJobProgressStatus_E  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressStatus_E') & '_value',Choose(p_web.GSV('locJobProgressStatusFilter') <> 2,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobProgressStatusFilter') <> 2)
  ! --- STRING --- locJobProgressStatus
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locJobProgressStatus')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(20) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locJobProgressStatus_E'',''formjobprogresscriteria_locjobprogressstatus_e_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobProgressStatus')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','locJobProgressStatus',p_web.GetSessionValueFormat('locJobProgressStatus'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectStatus?StatusType=EXC')&'?LookupField=locJobProgressStatus&Tab=3&ForeignField=sts:Status&_sort=sts:Status&Refresh=sort&LookupFrom=FormJobProgressCriteria&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressStatus_E') & '_value')


Prompt::locJobProgressStatus_L  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressStatus_L') & '_prompt',Choose(p_web.GSV('locJobProgressStatusFilter') <> 3,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Loan Status')
  If p_web.GSV('locJobProgressStatusFilter') <> 3
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressStatus_L') & '_prompt')

Validate::locJobProgressStatus_L  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locJobProgressStatus_L',p_web.GetValue('NewValue'))
    locJobProgressStatus = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locJobProgressStatus_L
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locJobProgressStatus',p_web.GetValue('Value'))
    locJobProgressStatus = p_web.GetValue('Value')
  End
  p_Web.SetValue('lookupfield','locJobProgressStatus_L')
  do AfterLookup
  do Value::locJobProgressStatus_L
  do SendAlert
  do Value::BrowseJobs  !1
  do Value::btnSMSTaggedJobs  !1
  do Value::btnTagAll  !1
  do Value::btnUnTagAll  !1

Value::locJobProgressStatus_L  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressStatus_L') & '_value',Choose(p_web.GSV('locJobProgressStatusFilter') <> 3,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobProgressStatusFilter') <> 3)
  ! --- STRING --- locJobProgressStatus
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locJobProgressStatus')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(20) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locJobProgressStatus_L'',''formjobprogresscriteria_locjobprogressstatus_l_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobProgressStatus')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','locJobProgressStatus',p_web.GetSessionValueFormat('locJobProgressStatus'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectStatus?StatusType=LOA')&'?LookupField=locJobProgressStatus&Tab=3&ForeignField=sts:Status&_sort=sts:Status&Refresh=sort&LookupFrom=FormJobProgressCriteria&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressStatus_L') & '_value')


Prompt::locJobProgressModelNumberFilter  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressModelNumberFilter') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Model Number Filter')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locJobProgressModelNumberFilter  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locJobProgressModelNumberFilter',p_web.GetValue('NewValue'))
    locJobProgressModelNumberFilter = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locJobProgressModelNumberFilter
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locJobProgressModelNumberFilter',p_web.GetValue('Value'))
    locJobProgressModelNumberFilter = p_web.GetValue('Value')
  End
  do Value::locJobProgressModelNumberFilter
  do SendAlert
  do Prompt::locJobProgressModelNumber
  do Value::locJobProgressModelNumber  !1
  do Value::BrowseJobs  !1

Value::locJobProgressModelNumberFilter  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressModelNumberFilter') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- locJobProgressModelNumberFilter
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locJobProgressModelNumberFilter')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locJobProgressModelNumberFilter') = ''
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locJobProgressModelNumberFilter'',''formjobprogresscriteria_locjobprogressmodelnumberfilter_value'',1,'''&clip('')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobProgressModelNumberFilter')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locJobProgressModelNumberFilter',clip(''),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locJobProgressModelNumberFilter_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('All Jobs') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locJobProgressModelNumberFilter') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locJobProgressModelNumberFilter'',''formjobprogresscriteria_locjobprogressmodelnumberfilter_value'',1,'''&clip(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobProgressModelNumberFilter')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locJobProgressModelNumberFilter',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locJobProgressModelNumberFilter_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Select Model Number') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressModelNumberFilter') & '_value')


Prompt::locJobProgressModelNumber  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressModelNumber') & '_prompt',Choose(p_web.GSV('locJobProgressModelNumberFilter') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Model Number')
  If p_web.GSV('locJobProgressModelNumberFilter') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressModelNumber') & '_prompt')

Validate::locJobProgressModelNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locJobProgressModelNumber',p_web.GetValue('NewValue'))
    locJobProgressModelNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locJobProgressModelNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locJobProgressModelNumber',p_web.GetValue('Value'))
    locJobProgressModelNumber = p_web.GetValue('Value')
  End
  p_Web.SetValue('lookupfield','locJobProgressModelNumber')
  do AfterLookup
  do Value::locJobProgressModelNumber
  do SendAlert
  do Value::BrowseJobs  !1

Value::locJobProgressModelNumber  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressModelNumber') & '_value',Choose(p_web.GSV('locJobProgressModelNumberFilter') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobProgressModelNumberFilter') <> 1)
  ! --- STRING --- locJobProgressModelNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locJobProgressModelNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(20) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locJobProgressModelNumber'',''formjobprogresscriteria_locjobprogressmodelnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobProgressModelNumber')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','locJobProgressModelNumber',p_web.GetSessionValueFormat('locJobProgressModelNumber'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('SelectModelNumbers?LookupField=locJobProgressModelNumber&Tab=3&ForeignField=mod:Model_Number&_sort=mod:Model_Number&Refresh=sort&LookupFrom=FormJobProgressCriteria&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressModelNumber') & '_value')


Prompt::locJobProgressAccountNumberFilter  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressAccountNumberFilter') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Account No Filter')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locJobProgressAccountNumberFilter  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locJobProgressAccountNumberFilter',p_web.GetValue('NewValue'))
    locJobProgressAccountNumberFilter = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locJobProgressAccountNumberFilter
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locJobProgressAccountNumberFilter',p_web.GetValue('Value'))
    locJobProgressAccountNumberFilter = p_web.GetValue('Value')
  End
    p_web.SSV('FranchiseAccount',p_web.GSV('locJobProgressAccountNumberFilter')) ! Set Account Filter  
  do Value::locJobProgressAccountNumberFilter
  do SendAlert
  do Value::BrowseJobs  !1
  do Prompt::locJobProgressAccountNumber
  do Value::locJobProgressAccountNumber  !1
  do Prompt::locJobProgressAccountNumber_Generic
  do Value::locJobProgressAccountNumber_Generic  !1

Value::locJobProgressAccountNumberFilter  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressAccountNumberFilter') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- locJobProgressAccountNumberFilter
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locJobProgressAccountNumberFilter')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locJobProgressAccountNumberFilter') = ''
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locJobProgressAccountNumberFilter'',''formjobprogresscriteria_locjobprogressaccountnumberfilter_value'',1,'''&clip('')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobProgressAccountNumberFilter')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locJobProgressAccountNumberFilter',clip(''),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locJobProgressAccountNumberFilter_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('All Jobs') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locJobProgressAccountNumberFilter') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locJobProgressAccountNumberFilter'',''formjobprogresscriteria_locjobprogressaccountnumberfilter_value'',1,'''&clip(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobProgressAccountNumberFilter')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locJobProgressAccountNumberFilter',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locJobProgressAccountNumberFilter_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Franchise Account') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locJobProgressAccountNumberFilter') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locJobProgressAccountNumberFilter'',''formjobprogresscriteria_locjobprogressaccountnumberfilter_value'',1,'''&clip(2)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobProgressAccountNumberFilter')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locJobProgressAccountNumberFilter',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locJobProgressAccountNumberFilter_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Generic Account') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressAccountNumberFilter') & '_value')


Prompt::locJobProgressAccountNumber  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressAccountNumber') & '_prompt',Choose(p_web.GSV('locJobProgressAccountNumberFilter') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Account No Filter')
  If p_web.GSV('locJobProgressAccountNumberFilter') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressAccountNumber') & '_prompt')

Validate::locJobProgressAccountNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locJobProgressAccountNumber',p_web.GetValue('NewValue'))
    locJobProgressAccountNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locJobProgressAccountNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locJobProgressAccountNumber',p_web.GetValue('Value'))
    locJobProgressAccountNumber = p_web.GetValue('Value')
  End
  p_Web.SetValue('lookupfield','locJobProgressAccountNumber')
  do AfterLookup
  do Value::locJobProgressAccountNumber
  do SendAlert
  do Value::BrowseJobs  !1

Value::locJobProgressAccountNumber  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressAccountNumber') & '_value',Choose(p_web.GSV('locJobProgressAccountNumberFilter') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobProgressAccountNumberFilter') <> 1)
  ! --- STRING --- locJobProgressAccountNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locJobProgressAccountNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(20) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locJobProgressAccountNumber'',''formjobprogresscriteria_locjobprogressaccountnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobProgressAccountNumber')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','locJobProgressAccountNumber',p_web.GetSessionValueFormat('locJobProgressAccountNumber'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('LookupRRCAccounts?LookupField=locJobProgressAccountNumber&Tab=3&ForeignField=sub:Account_Number&_sort=sub:Account_Number&Refresh=sort&LookupFrom=FormJobProgressCriteria&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressAccountNumber') & '_value')


Prompt::locJobProgressAccountNumber_Generic  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressAccountNumber_Generic') & '_prompt',Choose(p_web.GSV('locJobProgressAccountNumberFilter') <> 2,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Account No Filter')
  If p_web.GSV('locJobProgressAccountNumberFilter') <> 2
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressAccountNumber_Generic') & '_prompt')

Validate::locJobProgressAccountNumber_Generic  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locJobProgressAccountNumber_Generic',p_web.GetValue('NewValue'))
    locJobProgressAccountNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locJobProgressAccountNumber_Generic
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locJobProgressAccountNumber',p_web.GetValue('Value'))
    locJobProgressAccountNumber = p_web.GetValue('Value')
  End
  p_Web.SetValue('lookupfield','locJobProgressAccountNumber_Generic')
  do AfterLookup
  do Value::locJobProgressAccountNumber_Generic
  do SendAlert

Value::locJobProgressAccountNumber_Generic  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressAccountNumber_Generic') & '_value',Choose(p_web.GSV('locJobProgressAccountNumberFilter') <> 2,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobProgressAccountNumberFilter') <> 2)
  ! --- STRING --- locJobProgressAccountNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locJobProgressAccountNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(20) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locJobProgressAccountNumber_Generic'',''formjobprogresscriteria_locjobprogressaccountnumber_generic_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobProgressAccountNumber')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','locJobProgressAccountNumber',p_web.GetSessionValueFormat('locJobProgressAccountNumber'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('LookupGenericAccounts?LookupField=locJobProgressAccountNumber&Tab=3&ForeignField=TRA1:SubAcc&_sort=TRA1:SubAcc&Refresh=sort&LookupFrom=FormJobProgressCriteria&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobProgressCriteria_' & p_web._nocolon('locJobProgressAccountNumber_Generic') & '_value')


Validate::txtTotalJobs  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtTotalJobs',p_web.GetValue('NewValue'))
    do Value::txtTotalJobs
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtTotalJobs  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('txtTotalJobs') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('bold red')&'">' & p_web.Translate('Total Available Jobs: ' & p_web.GSV('TotalAvailableJobs'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::BrowseJobs  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('BrowseJobs',p_web.GetValue('NewValue'))
    do Value::BrowseJobs
  Else
    p_web.StoreValue('sbogen:RecordNumber')
  End
  LoadJobIntoQueue()  
  do SendAlert
  do Value::btnPrintRoutines  !1
  do Value::btnChangeStatus  !1
  do Value::btnPaymentDetails  !1
  do Value::btnTagAll  !1
  do Value::btnUnTagAll  !1
  do Value::btnChangeCourier  !1
  do Value::locJobStatus  !1

Value::BrowseJobs  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseJobProgress --
  p_web.SetValue('BrowseJobProgress:NoForm',1)
  p_web.SetValue('BrowseJobProgress:FormName',loc:formname)
  p_web.SetValue('BrowseJobProgress:parentIs','Form')
  p_web.SetValue('_parentProc','FormJobProgressCriteria')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormJobProgressCriteria_BrowseJobProgress_embedded_div')&'"><!-- Net:BrowseJobProgress --></div><13,10>'
    p_web._DivHeader('FormJobProgressCriteria_' & lower('BrowseJobProgress') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormJobProgressCriteria_' & lower('BrowseJobProgress') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseJobProgress --><13,10>'
  end
  do SendPacket
  BHAddToDebugLog('FormJobProgressCriteria: "Value::BrowseJobs"')
    LoadJobIntoQueue()
    


Validate::btnTagAll  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnTagAll',p_web.GetValue('NewValue'))
    do Value::btnTagAll
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    ! Tag All
    ClearTagFile(p_web)
    Access:SBO_GenericFile.ClearKey(sbogen:Long1Key)
    sbogen:SessionID = p_web.SessionID
    SET(sbogen:Long1Key,sbogen:Long1Key)
    LOOP UNTIL Access:SBO_GenericFile.Next() <> Level:Benign
        IF (sbogen:SessionID <> p_web.SessionID)
            BREAK
        END ! IF
        IF (Access:TAGFILE.PrimeRecord() = Level:Benign)
            tag:SessionID = p_web.SessionID
            tag:TaggedValue = sbogen:Long1
            tag:Tagged = 1
            IF (Access:TAGFILE.TryInsert())
                Access:TAGFILE.CancelAutoInc()
            END ! IF
        END ! IF
    END ! LOOP
  do SendAlert
  do Value::BrowseJobs  !1

Value::btnTagAll  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('btnTagAll') & '_value',Choose(p_web.GSV('locJobProgressStatusFilter') = 0 OR p_web.GSV('locJobProgressStatus') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobProgressStatusFilter') = 0 OR p_web.GSV('locJobProgressStatus') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnTagAll'',''formjobprogresscriteria_btntagall_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnTagAll','Tag All','MainButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobProgressCriteria_' & p_web._nocolon('btnTagAll') & '_value')


Validate::btnUnTagAll  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnUnTagAll',p_web.GetValue('NewValue'))
    do Value::btnUnTagAll
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  ClearTagFile(p_web)
  do SendAlert
  do Value::BrowseJobs  !1

Value::btnUnTagAll  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('btnUnTagAll') & '_value',Choose(p_web.GSV('locJobProgressStatusFilter') = 0 OR p_web.GSV('locJobProgressStatus') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobProgressStatusFilter') = 0 OR p_web.GSV('locJobProgressStatus') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnUnTagAll'',''formjobprogresscriteria_btnuntagall_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnUnTagAll','Un Tag All','MainButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobProgressCriteria_' & p_web._nocolon('btnUnTagAll') & '_value')


Validate::locJobStatus  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locJobStatus',p_web.GetValue('NewValue'))
    do Value::locJobStatus
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::locJobStatus  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('locJobStatus') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('right')&'">' & p_web.Translate(p_web.GSV('locJobStatus'),1) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobProgressCriteria_' & p_web._nocolon('locJobStatus') & '_value')


Validate::btnSMSTaggedJobs  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnSMSTaggedJobs',p_web.GetValue('NewValue'))
    do Value::btnSMSTaggedJobs
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnSMSTaggedJobs  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('btnSMSTaggedJobs') & '_value',Choose(p_web.GSV('locJobProgressStatusFilter') = 0 OR p_web.GSV('locJobProgressStatus') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobProgressStatusFilter') = 0 OR p_web.GSV('locJobProgressStatus') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnSMSTaggedJobs','SMS Tagged Jobs','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PageProcess?' &'ProcessType=SMSTaggedJobs&RedirectURL=FormJobProgressCriteria')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobProgressCriteria_' & p_web._nocolon('btnSMSTaggedJobs') & '_value')


Validate::btnPrintRoutines  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnPrintRoutines',p_web.GetValue('NewValue'))
    do Value::btnPrintRoutines
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnPrintRoutines  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('btnPrintRoutines') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnPrintRoutines','Print Routines','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PrintRoutines?' &'JobNumber=' & p_web.GSV('job:Ref_Number') & '&PrintRoutineReturnURL=FormJobProgressCriteria&clearvars=1')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobProgressCriteria_' & p_web._nocolon('btnPrintRoutines') & '_value')


Validate::btnPaymentDetails  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnPaymentDetails',p_web.GetValue('NewValue'))
    do Value::btnPaymentDetails
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnPaymentDetails  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('btnPaymentDetails') & '_value',Choose(p_web.GSV('job:Ref_Number') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Ref_Number') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnPaymentDetails','Payment Details','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('DisplayBrowsePayments?' &'DisplayBrowsePaymentsReturnURL=FormJobProgressCriteria')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobProgressCriteria_' & p_web._nocolon('btnPaymentDetails') & '_value')


Validate::btnChangeStatus  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnChangeStatus',p_web.GetValue('NewValue'))
    do Value::btnChangeStatus
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnChangeStatus  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('btnChangeStatus') & '_value',Choose(p_web.GSV('job:Ref_Number') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Ref_Number') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnChangeStatus','Change Status','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormChangeStatus?' &'ChangeStatusReturnURL=FormJobProgressCriteria&clearvars=1')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobProgressCriteria_' & p_web._nocolon('btnChangeStatus') & '_value')


Validate::btnChangeCourier  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnChangeCourier',p_web.GetValue('NewValue'))
    do Value::btnChangeCourier
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnChangeCourier  Routine
  p_web._DivHeader('FormJobProgressCriteria_' & p_web._nocolon('btnChangeCourier') & '_value',Choose(p_web.GSV('job:Ref_Number') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Ref_Number') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnChangeCourier','Change Courier','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormChangeCourier')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobProgressCriteria_' & p_web._nocolon('btnChangeCourier') & '_value')


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormJobProgressCriteria_GlobalJobNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::GlobalJobNumber
      else
        do Value::GlobalJobNumber
      end
  of lower('FormJobProgressCriteria_btnJobNoSearch_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnJobNoSearch
      else
        do Value::btnJobNoSearch
      end
  of lower('FormJobProgressCriteria_GlobalIMEINumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::GlobalIMEINumber
      else
        do Value::GlobalIMEINumber
      end
  of lower('FormJobProgressCriteria_GlobalMobileNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::GlobalMobileNumber
      else
        do Value::GlobalMobileNumber
      end
  of lower('FormJobProgressCriteria_locJobProgressStartDate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locJobProgressStartDate
      else
        do Value::locJobProgressStartDate
      end
  of lower('FormJobProgressCriteria_locJobProgressEndDate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locJobProgressEndDate
      else
        do Value::locJobProgressEndDate
      end
  of lower('FormJobProgressCriteria_locJobProgressLocation_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locJobProgressLocation
      else
        do Value::locJobProgressLocation
      end
  of lower('FormJobProgressCriteria_locJobProgressStatusFilter_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locJobProgressStatusFilter
      else
        do Value::locJobProgressStatusFilter
      end
  of lower('FormJobProgressCriteria_locJobProgressStatus_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locJobProgressStatus
      else
        do Value::locJobProgressStatus
      end
  of lower('FormJobProgressCriteria_locJobProgressStatus_E_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locJobProgressStatus_E
      else
        do Value::locJobProgressStatus_E
      end
  of lower('FormJobProgressCriteria_locJobProgressStatus_L_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locJobProgressStatus_L
      else
        do Value::locJobProgressStatus_L
      end
  of lower('FormJobProgressCriteria_locJobProgressModelNumberFilter_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locJobProgressModelNumberFilter
      else
        do Value::locJobProgressModelNumberFilter
      end
  of lower('FormJobProgressCriteria_locJobProgressModelNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locJobProgressModelNumber
      else
        do Value::locJobProgressModelNumber
      end
  of lower('FormJobProgressCriteria_locJobProgressAccountNumberFilter_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locJobProgressAccountNumberFilter
      else
        do Value::locJobProgressAccountNumberFilter
      end
  of lower('FormJobProgressCriteria_locJobProgressAccountNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locJobProgressAccountNumber
      else
        do Value::locJobProgressAccountNumber
      end
  of lower('FormJobProgressCriteria_locJobProgressAccountNumber_Generic_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locJobProgressAccountNumber_Generic
      else
        do Value::locJobProgressAccountNumber_Generic
      end
  of lower('FormJobProgressCriteria_BrowseJobProgress_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::BrowseJobs
      else
        do Value::BrowseJobs
      end
  of lower('FormJobProgressCriteria_btnTagAll_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnTagAll
      else
        do Value::btnTagAll
      end
  of lower('FormJobProgressCriteria_btnUnTagAll_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnUnTagAll
      else
        do Value::btnUnTagAll
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormJobProgressCriteria_form:ready_',1)
  p_web.SetSessionValue('FormJobProgressCriteria_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormJobProgressCriteria',0)

PreCopy  Routine
  p_web.SetValue('FormJobProgressCriteria_form:ready_',1)
  p_web.SetSessionValue('FormJobProgressCriteria_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormJobProgressCriteria',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormJobProgressCriteria_form:ready_',1)
  p_web.SetSessionValue('FormJobProgressCriteria_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormJobProgressCriteria:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormJobProgressCriteria_form:ready_',1)
  p_web.SetSessionValue('FormJobProgressCriteria_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormJobProgressCriteria:Primed',0)
  p_web.setsessionvalue('showtab_FormJobProgressCriteria',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormJobProgressCriteria_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormJobProgressCriteria_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 4
    loc:InvalidTab += 1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
  ! tab = 3
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormJobProgressCriteria:Primed',0)
  p_web.StoreValue('GlobalJobNumber')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('GlobalIMEINumber')
  p_web.StoreValue('')
  p_web.StoreValue('GlobalMobileNumber')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locJobProgressStartDate')
  p_web.StoreValue('locJobProgressEndDate')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locJobProgressLocation')
  p_web.StoreValue('locJobProgressStatusFilter')
  p_web.StoreValue('locJobProgressStatus')
  p_web.StoreValue('locJobProgressStatus')
  p_web.StoreValue('locJobProgressStatus')
  p_web.StoreValue('locJobProgressModelNumberFilter')
  p_web.StoreValue('locJobProgressModelNumber')
  p_web.StoreValue('locJobProgressAccountNumberFilter')
  p_web.StoreValue('locJobProgressAccountNumber')
  p_web.StoreValue('locJobProgressAccountNumber')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
UpdateText  Routine
  packet = clip(packet) & |
    '<<script><13,10>'&|
    'function UpdateProgress(getText) {{<13,10>'&|
    '    document.getElementById("formjobprogresscriteria_txtupdate_value_div").innerHTML = getText;<13,10>'&|
    '}<13,10>'&|
    '<</script><13,10>'&|
    ''
styles  Routine
  packet = clip(packet) & |
    '<<style><13,10>'&|
    '.box {{<13,10>'&|
    '  display: inline-block;<13,10>'&|
    '}<13,10>'&|
    '.box-left {{<13,10>'&|
    '  width: 90px;<13,10>'&|
    '  float: left;<13,10>'&|
    '}<13,10>'&|
    '.box-right {{<13,10>'&|
    '  float: right;<13,10>'&|
    '  width: 200px;<13,10>'&|
    '}<13,10>'&|
    '.box-data {{<13,10>'&|
    '  font-weight: bold;<13,10>'&|
    '}<13,10>'&|
    '.box-subdata {{<13,10>'&|
    '  font-size: 11px;<13,10>'&|
    '}<13,10>'&|
    '<</style><13,10>'&|
    ''
SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('locJobProgressStartDate',locJobProgressStartDate) ! DATE
     p_web.SSV('locJobProgressEndDate',locJobProgressEndDate) ! DATE
     p_web.SSV('locJobProgressLocation',locJobProgressLocation) ! LONG
     p_web.SSV('locJobProgressStatusFilter',locJobProgressStatusFilter) ! LONG
     p_web.SSV('locJobProgressStatus',locJobProgressStatus) ! STRING(30)
     p_web.SSV('locJobProgressModelNumberFilter',locJobProgressModelNumberFilter) ! LONG
     p_web.SSV('locJobProgressModelNumber',locJobProgressModelNumber) ! STRING(30)
     p_web.SSV('locJobProgressAccountNumberFilter',locJobProgressAccountNumberFilter) ! LONG
     p_web.SSV('locJobProgressAccountNumber',locJobProgressAccountNumber) ! STRING(30)
     p_web.SSV('GlobalIMEINumber',GlobalIMEINumber) ! STRING(30)
     p_web.SSV('GlobalMobileNumber',GlobalMobileNumber) ! STRING(30)
     p_web.SSV('GlobalJobNumber',GlobalJobNumber) ! STRING(30)
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     locJobProgressStartDate = p_web.GSV('locJobProgressStartDate') ! DATE
     locJobProgressEndDate = p_web.GSV('locJobProgressEndDate') ! DATE
     locJobProgressLocation = p_web.GSV('locJobProgressLocation') ! LONG
     locJobProgressStatusFilter = p_web.GSV('locJobProgressStatusFilter') ! LONG
     locJobProgressStatus = p_web.GSV('locJobProgressStatus') ! STRING(30)
     locJobProgressModelNumberFilter = p_web.GSV('locJobProgressModelNumberFilter') ! LONG
     locJobProgressModelNumber = p_web.GSV('locJobProgressModelNumber') ! STRING(30)
     locJobProgressAccountNumberFilter = p_web.GSV('locJobProgressAccountNumberFilter') ! LONG
     locJobProgressAccountNumber = p_web.GSV('locJobProgressAccountNumber') ! STRING(30)
     GlobalIMEINumber = p_web.GSV('GlobalIMEINumber') ! STRING(30)
     GlobalMobileNumber = p_web.GSV('GlobalMobileNumber') ! STRING(30)
     GlobalJobNumber = p_web.GSV('GlobalJobNumber') ! STRING(30)
LoadJobIntoQueue    PROCEDURE()
exchangeStatusDate      DATE()
loanStatusDate          DATE()
jobStatusDate           DATE()
jobStatusDays           STRING(30)
exchangeStatusDays      STRING(30)
loanStatusDays          STRING(30)
    CODE
        
        BHAddToDebugLog('FormJobProgressCriteria: LoadJobIntoQueue(' & p_web.GSV('sbogen:RecordNumber') & ')')
    
        p_web.SSV('locJobStatus','')

        Access:SBO_GenericFile.ClearKey(sbogen:RecordNumberKey)
        sbogen:RecordNumber = p_web.GSV('sbogen:RecordNumber')
        IF (Access:SBO_GenericFile.TryFetch(sbogen:RecordNumberKey) = Level:Benign)
            VodacomClass.AddToFileLog(p_web.SessionID,'JobProgress','Load Highlighted Job ' & sbogen:Long1) ! Debug
        
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = sbogen:Long1
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
        
            ELSE ! IF
            END ! IF
            Access:WEBJOB.ClearKey(wob:RefNumberKey)
            wob:RefNumber = job:Ref_Number
            IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
        
            ELSE ! IF
            END ! IF
            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = job:Ref_Number
            IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
        
            ELSE ! IF
            END ! IF
            p_web.FileToSessionQueue(JOBS)
            p_web.FileToSessionQueue(WEBJOB)
            p_web.FileToSessionQueue(JOBSE)
            VodacomClass.AddToFileLog(p_web.SessionID,'JobProgress','Loaded Highlighted Job ' & sbogen:Long1) ! Debug
        
            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = wob:HeadAccountNumber
            IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
            END ! IF
            
            Access:AUDSTATS.ClearKey(aus:DateChangedKey)
            aus:RefNumber = job:Ref_Number
            aus:Type     = 'JOB'
            aus:DateChanged = TODAY()
            SET(aus:DateChangedKey,aus:DateChangedKey)
            LOOP UNTIL Access:AUDSTATS.Previous() <> Level:Benign
                IF (aus:RefNumber <> job:Ref_Number OR |
                    aus:Type <> 'JOB')
                    BREAK
                END ! IF
                jobStatusDate = aus:DateChanged
                BREAK
            END ! LOOP
            Access:AUDSTATS.ClearKey(aus:DateChangedKey)
            aus:RefNumber = job:Ref_Number
            aus:Type     = 'LOA'
            aus:DateChanged = TODAY()
            SET(aus:DateChangedKey,aus:DateChangedKey)
            LOOP UNTIL Access:AUDSTATS.Previous() <> Level:Benign
                IF (aus:RefNumber <> job:Ref_Number OR |
                    aus:Type <> 'LOA')
                    BREAK
                END ! IF
                loanStatusDate = aus:DateChanged
                BREAK
            END ! LOOP
            Access:AUDSTATS.ClearKey(aus:DateChangedKey)
            aus:RefNumber = job:Ref_Number
            aus:Type     = 'EXC'
            aus:DateChanged = TODAY()
            SET(aus:DateChangedKey,aus:DateChangedKey)
            LOOP UNTIL Access:AUDSTATS.Previous() <> Level:Benign
                IF (aus:RefNumber <> job:Ref_Number OR |
                    aus:Type <> 'EXC')
                    BREAK
                END ! IF
                exchangeStatusDate = aus:DateChanged
                BREAK
            END ! LOOP

            jobStatusDays = GetDays(tra:IncludeSaturday,tra:IncludeSunday,jobStatusDate)
            loanStatusDays = GetDays(tra:IncludeSaturday,tra:IncludeSunday,loanStatusDate)
            exchangeStatusDays = GetDays(tra:IncludeSaturday,tra:IncludeSunday,exchangeStatusDate)

            IF (jobe:WebJob)
                IF (wob:DateJobDespatched <> '')
                    jobStatusDays = 'N/A'
                END ! IF
            ELSE ! IF
                IF (job:Date_Despatched <> '')
                    jobStatusDays = 'N/A'
                END ! IF
            END ! IF

            IF (job:Exchange_Despatched <> '' OR job:Exchange_Unit_Number = 0 OR job:Exchange_Status[1:3] = '901')
                exchangeStatusDays = 'N/A'
            END ! IF
            IF (job:Loan_Despatched <> '' OR job:Loan_Unit_Number = 0 OR job:Loan_Status[1:3] = '901')
                loanStatusDays = 'N/A'
            END ! IF


            
            p_web.SSV('locJobStatus','  <div class="box">' & |
                '    <div class="box-left">' & |
                '      Job Status:' & |
                '    </div>' & |
                '    <div class="box-right">' & |
                '      <div id="jobStatus" class="box-data">' & |
                CLIP(job:Current_Status) & |
                '      </div>' & |
                '      <div class="box-subdata">' & |
                FORMAT(jobStatusDate,@d06b) & ' (Status Days: ' & CLIP(jobStatusDays) & ')' & |
                '      </div>' & |
                '    </div>' & |
                '  </div>' & |
                '    <div class="box">' & |
                '    <div class="box-left">' & |
                '      Exchange Status:' & |
                '    </div>' & |
                '    <div class="box-right">' & |
                '      <div class="box-data">' & |
                CLIP(job:Exchange_Status) & |
                '      </div>' & |
                '      <div class="box-subdata">' & |
                FORMAT(exchangeStatusDate,@d06b) & ' (Status Days: ' & CLIP(exchangeStatusDays) & ')' & |
                '      </div>' & |
                '    </div>' & |
                '  </div>' & |
                '    <div class="box">' & |
                '    <div class="box-left">' & |
                '      Loan Status:' & |
                '    </div>' & |
                '    <div class="box-right">' & |
                '      <div class="box-data">' & |
                CLIP(job:Loan_status) & |
                '      </div>' & |
                '      <div class="box-subdata">' & |
                FORMAT(loanStatusDate,@d06b) & ' (Status Days: ' & CLIP(loanStatusDays) & ')' & |
                '      </div>' & |
                '    </div>' & |
                '  </div>  ')
            
        ELSE ! IF
        END ! IF
        
GetDays             PROCEDURE(LONG pSat,LONG pSun,DATE pDate)!,LONG
locCount                LONG
locDays                 LONG
    CODE
        LOOP
            locCount += 1
            IF (pDate + locCount) % 7 = 0 AND ~pSun
                CYCLE
            END ! IF
            IF (pDate + locCount) % 7 = 6 AND ~pSat
                CYCLE
            END ! IF
            locDays += 1
            IF (pDate + locCount >= TODAY())
                BREAK
            END ! IF
        END ! LOOP
        

        RETURN locDays
