

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER060.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
HubOutOfRegion       PROCEDURE  (STRING pHeadAccountNumber,STRING pHub) ! Declare Procedure
TRAHUBS::State  USHORT
FilesOpened     BYTE(0)

  CODE
    If pHeadAccountNumber = ''
        Return 0
    End ! If pHeadAccountNumber = ''
    If pHub = ''
        Return 0
    End ! If pHub = ''
    Do OpenFiles
    Do SaveFiles

    Found# = 0
    Access:TRAHUBS.Clearkey(trh:HubKey)
    trh:TRADEACCAccountNumber = pHeadAccountNumber
    Set(trh:HubKey,trh:HubKey)
    Loop
        If Access:TRAHUBS.Next()
            Break
        End ! If Access:TRAHUBS.Next()
        If trh:TRADEACCAccountNumber <> pHeadAccountNumber
            Break
        End ! If trh:TRADEACCAccountNumber <> wob:HeadAccountNumber
        Found# = 1
        If trh:Hub = pHub
            Found# = 2
            Break
        End ! If trh:Hub = pHub
    End ! Loop

    Do RestoreFiles
    Do CloseFiles
    !0 = No Relevant Hub Data
    !1 = Hub is out of region
    !2 = Found Hub.
    Return Found#

SaveFiles  ROUTINE
  TRAHUBS::State = Access:TRAHUBS.SaveFile()               ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF TRAHUBS::State <> 0
    Access:TRAHUBS.RestoreFile(TRAHUBS::State)             ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:TRAHUBS.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRAHUBS.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:TRAHUBS.Close
     FilesOpened = False
  END
