

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('WEBSERVER414.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER411.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER413.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! Report the STMASAUD File
!!! </summary>
StockAuditReport PROCEDURE (NetwebServerWorker p_web)

  ! The NetTalk Extension to ABC report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
totalLines              LONG()
totalNumberOfLines      LONG()
totalAudited            LONG()
totalQty                LONG()
totalLineCost           REAL()
i                       LONG()
Progress:Thermometer BYTE                                  !
Address:User_Name    STRING(30)                            !
address:Address_Line1 STRING(30)                           !
address:Address_Line2 STRING(30)                           !
address:Address_Line3 STRING(30)                           !
address:Post_Code    STRING(20)                            !
Address:Telephone_Number STRING(20)                        !
Audit_Qty            LONG                                  !
Line_Cost            REAL                                  !
Orig_Qty             REAL                                  !
Total_Line           REAL                                  !
Total_Audited        LONG                                  !
tmp:printedby        STRING(60)                            !
tmp:TelephoneNumber  STRING(20)                            !
tmp:FaxNumber        STRING(20)                            !
Lines_Not_audited    REAL                                  !
Grand_Available_Qty  LONG                                  !
Percentage_Audited   REAL                                  !
locAuditNumber       LONG                                  !
Total_Lines_in_Audit REAL                                  !
Total_No_Of_Lines    LONG                                  !
Process:View         VIEW(STMASAUD)
                       PROJECT(stom:Audit_No)
                     END
ProgressWindow       WINDOW('Report STMASAUD'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:DEFAULT), |
  DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT,AT(396,2802,7521,7990),PRE(RPT),PAPER(PAPER:A4),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(385,531,7521,1802),USE(?StringCompleted:2)
                         STRING('STOCK AUDIT REPORT'),AT(4948,573),USE(?String3),FONT('Arial',8,,,CHARSET:ANSI),TRN
                         STRING('<<-- Date Stamp -->'),AT(5698,1302,927,135),USE(?ReportDateStamp:2),FONT('Arial',8, |
  ,FONT:regular),TRN
                         STRING(@s30),AT(52,646),USE(address:Address_Line3),FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('Location :'),AT(4948,1115),USE(?String43),FONT('Arial',8),TRN
                         STRING(@s30),AT(5448,1115),USE(stoa:Site_Location),FONT('Arial',8,COLOR:Black,,CHARSET:ANSI), |
  TRN
                         STRING(@pPage <<#p),AT(4948,1458,700,135),USE(?PageCount:2),FONT('Arial',8,,FONT:regular), |
  PAGENO,TRN
                         STRING(@s20),AT(52,833),USE(address:Post_Code),FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(4948,1302),USE(?String16),FONT('Arial',8,,,CHARSET:ANSI),TRN
                         STRING('This Audit is TEMP COMPLETED only'),AT(115,1417),USE(?StringCompleted),FONT(,12,COLOR:Black, |
  FONT:bold,CHARSET:ANSI),HIDE,TRN
                         STRING(@s20),AT(52,1010),USE(Address:Telephone_Number),FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('String 21'),AT(3656,0,3438,260),USE(?String21),FONT('Arial',14,,FONT:bold),RIGHT(10), |
  TRN
                         STRING(@s30),AT(52,271),USE(address:Address_Line1),FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@s30),AT(52,458),USE(address:Address_Line2),FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@s30),AT(52,0),USE(Address:User_Name),FONT('Arial',14,COLOR:Black,FONT:bold,CHARSET:ANSI)
                       END
DETAIL                 DETAIL,AT(0,0,,167),USE(?DetailBand)
                         STRING(@s25),AT(104,0),USE(sto:Part_Number),FONT('Arial',8,,,CHARSET:ANSI),TRN
                         STRING(@s25),AT(1688,0),USE(sto:Description),FONT('Arial',8,,,CHARSET:ANSI),TRN
                         STRING(@n8),AT(5448,0),USE(Audit_Qty),FONT('Arial',8),RIGHT(1),TRN
                         STRING(@n10.2),AT(5969,10),USE(sto:Purchase_Cost),FONT('Arial',8),RIGHT,TRN
                         STRING(@n10.2),AT(6583,10),USE(Line_Cost),FONT('Arial',8),RIGHT,TRN
                         STRING(@s30),AT(3292,0,1406,208),USE(sto:Shelf_Location),FONT('Arial',8)
                         STRING(@n8),AT(4875,0),USE(Orig_Qty),FONT('Arial',8),RIGHT(1)
                       END
detail1                DETAIL,AT(0,0,,52),USE(?unnamed:6),PAGEAFTER(-1)
                       END
detail4                DETAIL,AT(0,0,,969),USE(?unnamed:9)
                         LINE,AT(115,42,2146,0),USE(?Line3),COLOR(COLOR:Black)
                         STRING('Totals'),AT(4844,104),USE(?String28),FONT('Arial',8,,FONT:bold),TRN
                         LINE,AT(5417,73,1833,1),USE(?Line1),COLOR(COLOR:Black)
                         STRING(@n10.2),AT(6615,104),USE(totallinecost),FONT('Arial',8,,FONT:bold),RIGHT,TRN
                         STRING('Stock Lines'),AT(94,260),USE(?String32),FONT('Arial',8,,FONT:bold),TRN
                         STRING(@n-10),AT(1625,260),USE(totallines),FONT('Arial',8),RIGHT(1),TRN
                         STRING(@n-10),AT(1625,427),USE(totalaudited),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),RIGHT(1)
                         STRING('Lines Not Audited'),AT(104,594),USE(?String34),FONT('Arial',8,,FONT:bold),TRN
                         STRING(@n-10),AT(1625,594),USE(Lines_Not_audited),FONT('Arial',8,COLOR:Black,,CHARSET:ANSI), |
  RIGHT(1),TRN
                         STRING('Lines Audited'),AT(104,427),USE(?String26),FONT('Arial',8,,FONT:bold),TRN
                         STRING(@n-10.2),AT(1625,760),USE(Percentage_Audited),FONT('Arial',8,,FONT:bold,CHARSET:ANSI), |
  RIGHT(1)
                         STRING('%'),AT(2240,760),USE(?String30),FONT('Arial',8),TRN
                         STRING('Percentage Audited'),AT(104,760),USE(?String27),FONT('Arial',8,,FONT:bold),TRN
                         STRING('Total Number Of Lines:'),AT(104,94),USE(?String29),FONT('Arial',8,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING(@n-10),AT(1625,94),USE(totalnumberoflines),FONT('Arial',8,,FONT:bold,CHARSET:ANSI), |
  RIGHT(1),TRN
                         STRING(@n-11),AT(5313,104),USE(totalqty),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),RIGHT(1), |
  TRN
                       END
detail2                DETAIL,AT(0,0,,260),USE(?unnamed:7)
                         STRING('NO ITEMS TO REPORT'),AT(115,42,7094,208),USE(?String25),FONT(,12,,FONT:bold),CENTER, |
  TRN
                       END
                       FOOTER,AT(396,10833,7521,177),USE(?unnamed:3)
                       END
                       FORM,AT(365,510,7521,10802),USE(?unnamed)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,10802),USE(?Image1)
                         STRING('Stock Code'),AT(135,1979),USE(?String5),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),TRN
                         STRING('Description'),AT(1719,1979),USE(?String6),FONT('Arial',8,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING('Shelf Location'),AT(3323,1979),USE(?String31),FONT('Arial',8,,FONT:bold),TRN
                         STRING('Orig. Qty'),AT(4938,1979),USE(?String44),FONT('Arial',8,,FONT:bold),TRN
                         STRING('Qty'),AT(5813,1979),USE(?String8),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),TRN
                         STRING('Unit Cost'),AT(6167,1979),USE(?String9),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),TRN
                         STRING('Line Cost'),AT(6771,1979),USE(?String9:2),FONT('Arial',8,,FONT:bold,CHARSET:ANSI), |
  TRN
                       END
                     END
!*** PDF-Tools v4.1 Start (All)
!--- ClarioNET NOT Enabled
!--- PDFXTABCTemplate: True  Level: 4
!--- PDFXTABCTemplate: RPM: False
!--- PDFXTABCTemplate: Icetips: False
PDFXTR3               PDFXToolsReportClass                 !  Added by PDF-Tools Templates v4.1
PDFXTR3:rtn           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXTR3:vpf           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Orientation      BYTE(0)                      !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:TopMargin        REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:BottomMargin     REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:LeftMargin       REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:RightMargin      REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Selection        STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PDFXT4:Papers:Queue           QUEUE,PRE()                  !  Added by PDF-Tools Templates v4.1
PLQ:Description                 STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PLQ:PaperCode                   BYTE                       !  Added by PDF-Tools Templates v4.1
                              END                          !  Added by PDF-Tools Templates v4.1
!*** PDF-Tools v4.1 End
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

ExportLine	ROUTINE
    Access:STOAUDIT.ClearKey(stoa:Internal_AutoNumber_Key)
    stoa:Internal_AutoNumber = qStockAudit.StoAuditRecordNumber
    IF (Access:STOAUDIT.TryFetch(stoa:Internal_AutoNumber_Key) = Level:Benign)
			
    END ! IF
		
    Access:STOCK.ClearKey(sto:Ref_Number_Key)
    sto:Ref_Number = qStockAudit.StockRecordNumber
    IF (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
			
    END ! IF
    
    Audit_Qty = -1 * qStockAudit.StockQty
    Orig_Qty = stoa:Original_Level
    Line_Cost = sto:Purchase_Cost * Audit_Qty
		
    PRINT(rpt:Detail)
    

    totalNumberOfLines += 1
    totalQty += -1 * qStockAudit.StockQty
    totalLineCost += (sto:Purchase_Cost * (-1 * qStockAudit.StockQty))	
		
		
Totals              ROUTINE		
    
        Lines_Not_Audited = totalAudited - totalLines
		
        Percentage_Audited = (totalAudited/totalLines) * 100
		
		IF (totalNumberOfLines = 0)
			PRINT(rpt:Detail2)
		ELSE ! IF
			PRINT(rpt:Detail4)		
		END ! IF
!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('StockAuditReport')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open                                     ! File DEFAULTS used by this procedure, so make sure it's RelationManager is open
  Relate:GENSHORT.Open                                     ! File GENSHORT used by this procedure, so make sure it's RelationManager is open
  Relate:STMASAUD.SetOpenRelated()
  Relate:STMASAUD.Open                                     ! File STMASAUD used by this procedure, so make sure it's RelationManager is open
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:STOAUDIT.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:STOCK.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
    locAuditNumber = p_web.GSV('stom:Audit_No')  
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('StockAuditReport',ProgressWindow)          ! Restore window settings from non-volatile store
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    loc:pdfname = '.\$$$' & format(random(1,99999),@n05) &'.pdf'
    ProgressWindow{prop:hide} = 1
  End
  ProgressMgr.Init(ScrollSort:AllowNumeric+ScrollSort:Descending,)
  ThisReport.Init(Process:View, Relate:STMASAUD, ?Progress:PctText, Progress:Thermometer, ProgressMgr, stom:Audit_No)
  ThisReport.AddSortOrder(stom:AutoIncrement_Key)
  ThisReport.AddRange(stom:Audit_No,locAuditNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:STMASAUD.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      ReportNoRecords(p_web)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:GENSHORT.Close
    Relate:STMASAUD.Close
  END
  IF SELF.Opened
    INIMgr.Update('StockAuditReport',ProgressWindow)       ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.OpenReport()
  !*** PDF-Tools v4.1 Start (ABC)
  IF ~ReturnValue                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.Init(Report, loc:PDFName,,PDFA_OFF)            !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPaper(CHOOSE(SYSTEM{PROP:Autopaper}=True,Report{PROPPRINT:Paper},PAPER:USER), CHOOSE(Report{PROP:Thous}=True,PROP:Thous,CHOOSE(Report{PROP:MM}=True,PROP:MM,CHOOSE(Report{PROP:Points}=True,PROP:Points,0))), Report{PROPPRINT:PaperWidth}, Report{PROPPRINT:PaperHeight}, Report{PROP:Landscape}) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetCompression(TRUE,TRUE,FALSE,COMPRTYPE_C_AUTO,75,COMPRTYPE_I_AUTO,COMPRTYPE_M_AUTO) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetAuthor('ServiceBase 3g')          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetTitle()                           !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetSubject('ServiceBase 3g')         !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPdfCreator('ServiceBase 3g')      !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetKeywords()                        !  Added by PDF-Tools Templates v4.1
    PDFXTR3.EnableLinkAnalyzer(True)             !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetSpecVersion(SPECVERSION_15,TRUE)  !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPageLayout(PAGELAYOUT_SINGLEPAGE) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPageMode(PAGEMODE_NONE)           !  Added by PDF-Tools Templates v4.1
    IF PAGEMODE_NONE = PAGEMODE_FULLSCREEN       !  Added by PDF-Tools Templates v4.1
      PDFXTR3:vpf = VP_FSPM_NONE                 !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
      PDFXTR3:vpf = 0                            !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDETOOLBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDEMENUBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDEWINDOWUI). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_FITWINDOW). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_CENTERWINDOW). !  Added by PDF-Tools Templates v4.1
      IF TRUE                                     THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_DISPLAYDOCTITLE). !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetViewerPrefs(PDFXTR3:vpf)          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPreview(False)                    !  Added by PDF-Tools Templates v4.1
  END
  !*** PDF-Tools v4.1 End
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp:2{PROP:Text} = FORMAT(TODAY(),@d06)
  END
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeCloseEvent()
  !*** PDF-Tools v4.1 Start (ABC)
  PDFXTR3.Kill()
  !*** PDF-Tools v4.1 End
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
        BuildStockAuditShortages(p_web,0,stom:Audit_No,totalLines,totalAudited)
        IF (totalLines = 0)
            ReturnValue = PARENT.TakeRecord()
            RETURN Level:Fatal
        ELSE
            
        
        
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number  = p_web.GSV('BookingAccount')
            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                address:User_Name        = tra:company_name
                address:Address_Line1    = tra:address_line1
                address:Address_Line2    = tra:address_line2
                address:Address_Line3    = tra:address_line3
                address:Post_code         = tra:postcode
                address:Telephone_Number = tra:telephone_number
  
            Else ! If Access:TRADACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
    
            End !If Access:TRADACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        
            SETTARGET(Report)
            ?String21{PROP:Text} = 'SHORTAGES'
            ?String29{PROP:Text} = 'Total Shortage Lines'
        
            IF stom:Complete = 'N' then
                unhide(?StringCompleted)
            END
  
            SETTARGET()
        		! Shortages
            totalQty = 0
            totalLineCost = 0
            totalNumberOfLines = 0
  		
            SORT(qStockAudit,qStockAudit.SessionID,qStockAudit.ShortagesExcess)
            LOOP i = 1 TO RECORDS(qStockAudit)
                GET(qStockAudit,i)
  			
  			
                IF (qStockAudit.SessionID <> p_web.SessionID OR qStockAudit.ShortagesExcess <> 'S')
                    CYCLE
                END ! IF
  			
            
                ReturnValue = PARENT.TakeRecord()
            
                DO ExportLine
            
  			
            END ! LOOP
        
            DO Totals
        
            PRINT(rpt:Detail1)
  
            SETTARGET(Report)
            ?String21{PROP:Text} = 'EXCESSES'
            ?String29{PROP:Text} = 'Total Excesses Lines'
            SETTARGET()
        
  		!Excesses
            totalQty = 0
            totalLineCost = 0
            totalNumberOfLines = 0
  		
            SORT(qStockAudit,qStockAudit.SessionID,qStockAudit.ShortagesExcess)
            LOOP i = 1 TO RECORDS(qStockAudit)
                GET(qStockAudit,i)
  			
  			
                IF (qStockAudit.SessionID <> p_web.SessionID OR qStockAudit.ShortagesExcess <> 'E')
                    CYCLE
                END ! IF
  			
                ReturnValue = PARENT.TakeRecord()
            
                DO ExportLine
            
  			
            END ! LOOP		
  		
            DO Totals
        END ! IF
        
        
        RETURN ReturnValue
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:DETAIL)
  PRINT(RPT:detail1)
  PRINT(RPT:detail4)
  PRINT(RPT:detail2)
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  !*** PDF-Tools v4.1 Start (ABC)
  ReturnValue = False                            !  Added by PDF-Tools Templates v4.1
    PDFXTR3:rtn = PDFXTR3.Generate(SELF.ImageQueue,True, , ) !  Added by PDF-Tools Templates v4.1
    IF PDFXTR3:rtn THEN                          !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
  RETURN ReturnValue
  !*** PDF-Tools v4.1 End
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
  RETURN ReturnValue

