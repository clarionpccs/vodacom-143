

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER331.INC'),ONCE        !Local module procedure declarations
                     END


WarrantyProcessRejection PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locRejectionReason   STRING(255)                           !
FilesOpened     Long
SBO_WarrantyClaims::State  USHORT
JOBSE2::State  USHORT
JOBSE::State  USHORT
JOBS::State  USHORT
WEBJOB::State  USHORT
JOBSWARR::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('WarrantyProcessRejection')
  loc:formname = 'WarrantyProcessRejection_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('WarrantyProcessRejection','')
    p_web._DivHeader('WarrantyProcessRejection',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferWarrantyProcessRejection',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferWarrantyProcessRejection',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferWarrantyProcessRejection',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_WarrantyProcessRejection',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferWarrantyProcessRejection',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_WarrantyProcessRejection',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(SBO_WarrantyClaims)
  p_web._OpenFile(JOBSE2)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(JOBSWARR)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(SBO_WarrantyClaims)
  p_Web._CloseFile(JOBSE2)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(JOBSWARR)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
        IF (p_web.GetValue('RType') = 1)
            p_web.SSV('TabHeading','Resubmit Claim')
        ELSIF (p_web.GetValue('RType') = 2)
            p_web.SSV('TabHeading','Reject Claim')
        ELSE
            packet = CLIP(packet) & '<script>location.href="WarrantyClaims";</script>'          
            DO SendPacket
        END ! IF     
        p_web.SSV('locRejectionReason',' ')
        
        p_web.StoreValue('RType')
            
  p_web.SetValue('WarrantyProcessRejection_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locRejectionReason',locRejectionReason)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locRejectionReason')
    locRejectionReason = p_web.GetValue('locRejectionReason')
    p_web.SetSessionValue('locRejectionReason',locRejectionReason)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('WarrantyProcessRejection_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locRejectionReason = p_web.RestoreValue('locRejectionReason')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'WarrantyClaims'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('WarrantyProcessRejection_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('WarrantyProcessRejection_ChainTo')
    loc:formaction = p_web.GetSessionValue('WarrantyProcessRejection_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'WarrantyClaims'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="WarrantyProcessRejection" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="WarrantyProcessRejection" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="WarrantyProcessRejection" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Process Rejection') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Process Rejection',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_WarrantyProcessRejection">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_WarrantyProcessRejection" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_WarrantyProcessRejection')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate(p_web.GSV('TabHeading')) & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_WarrantyProcessRejection')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_WarrantyProcessRejection'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locRejectionReason')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_WarrantyProcessRejection')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate(p_web.GSV('TabHeading')) &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_WarrantyProcessRejection_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate(p_web.GSV('TabHeading'))&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate(p_web.GSV('TabHeading'))&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate(p_web.GSV('TabHeading'))&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate(p_web.GSV('TabHeading'))&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locRejectionReason
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locRejectionReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locRejectionReason  Routine
  p_web._DivHeader('WarrantyProcessRejection_' & p_web._nocolon('locRejectionReason') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Reason')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locRejectionReason  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locRejectionReason',p_web.GetValue('NewValue'))
    locRejectionReason = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locRejectionReason
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locRejectionReason',p_web.GetValue('Value'))
    locRejectionReason = p_web.GetValue('Value')
  End
  If locRejectionReason = ''
    loc:Invalid = 'locRejectionReason'
    loc:alert = p_web.translate('Reason') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locRejectionReason = Upper(locRejectionReason)
    p_web.SetSessionValue('locRejectionReason',locRejectionReason)
  do Value::locRejectionReason
  do SendAlert

Value::locRejectionReason  Routine
  p_web._DivHeader('WarrantyProcessRejection_' & p_web._nocolon('locRejectionReason') & '_value','adiv')
  loc:extra = ''
  ! --- TEXT --- locRejectionReason
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locRejectionReason')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locRejectionReason = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locRejectionReason'',''warrantyprocessrejection_locrejectionreason_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('locRejectionReason',p_web.GetSessionValue('locRejectionReason'),8,40,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,255,,Net:Web:Control) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyProcessRejection_' & p_web._nocolon('locRejectionReason') & '_value')


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('WarrantyProcessRejection_locRejectionReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locRejectionReason
      else
        do Value::locRejectionReason
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('WarrantyProcessRejection_form:ready_',1)
  p_web.SetSessionValue('WarrantyProcessRejection_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_WarrantyProcessRejection',0)

PreCopy  Routine
  p_web.SetValue('WarrantyProcessRejection_form:ready_',1)
  p_web.SetSessionValue('WarrantyProcessRejection_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_WarrantyProcessRejection',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('WarrantyProcessRejection_form:ready_',1)
  p_web.SetSessionValue('WarrantyProcessRejection_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('WarrantyProcessRejection:Primed',0)

PreDelete       Routine
  p_web.SetValue('WarrantyProcessRejection_form:ready_',1)
  p_web.SetSessionValue('WarrantyProcessRejection_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('WarrantyProcessRejection:Primed',0)
  p_web.setsessionvalue('showtab_WarrantyProcessRejection',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('WarrantyProcessRejection_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('WarrantyProcessRejection_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
        If locRejectionReason = ''
          loc:Invalid = 'locRejectionReason'
          loc:alert = p_web.translate('Reason') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locRejectionReason = Upper(locRejectionReason)
          p_web.SetSessionValue('locRejectionReason',locRejectionReason)
        If loc:Invalid <> '' then exit.
  ! The following fields are not on the form, but need to be checked anyway.
    Access:SBO_WarrantyClaims.ClearKey(sbojow:RecordNumberKey)
    sbojow:RecordNumber = p_web.GSV('sbojow:RecordNumber')
    IF (Access:SBO_WarrantyClaims.TryFetch(sbojow:RecordNumberKey) = Level:Benign)
    END !I F
    
    
    Access:JOBSWARR.ClearKey(jow:RefNumberKey)
    jow:RefNumber = sbojow:JobNumber
    IF (Access:JOBSWARR.TryFetch(jow:RefNumberKey))
        loc:invalid = 'locRejectionReason'
        loc:Alert = 'Unable To Load Job Details'
        EXIT        
    END ! IF
    Access:WEBJOB.ClearKey(wob:RefNumberKey)
    wob:RefNumber = jow:RefNumber
    IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
        loc:invalid = 'locRejectionReason'
        loc:Alert = 'Unable To Load Job Details'
        EXIT
    END ! IF
    Access:JOBS.ClearKey(job:Ref_Number_Key)
    job:Ref_Number = jow:RefNumber
    IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
        loc:invalid = 'locRejectionReason'
        loc:alert = 'Unable To Load Job Details'
        EXIT
    END ! IF
  
    Access:JOBSE.ClearKey(jobe:RefNumberKey)
    jobe:RefNumber = jow:RefNumber
    IF (Access:JOBSE.TryFetch(jobe:RefNumberKey))
        loc:Invalid = 'locRejectionReason'
        loc:Alert = 'Unable To Load Job Details'
        EXIT
    END ! IF
    
    p_web.SSV('Pending',FALSE)
    
    CASE p_web.GSV('RType')
    OF 1
        IF (jobe:OBFProcessed = 3)
            jobe:OBFProcessed = 0
        ELSE
            job:EDI = 'NO'
            job:EDI_Batch_Number = ''
            jow:Status = 'NO'
            jow:Submitted += 1
            jow:ClaimSubmitted = TODAY()
            p_web.SSV('Pending',TRUE)
        END ! IF
      
        wob:EDI = 'NO'
        jow:RRCStatus = 'NO'
        Access:JOBSWARR.TryUpdate()
        
        jobe:WarrantyClaimStatus = 'RESUBMITTED'
        jobe:WarrantyStatusDate = TODAY()
      
    OF 2
        IF (jobe:OBFProcessed = 3)
            jobe:OBFProcessed = 4
        ELSE ! IF
            job:EDI = 'REJ'
            jow:Status = 'REJ'
        END ! IF
      
        wob:EDI = 'AAJ'
        jow:RRCStatus = 'AAJ'
        jow:DateFinalRejection = TODAY()
        Access:JOBSWARR.TryUpdate()
        
  
      
        jobe:WarrantyClaimStatus = 'FINAL REJECTION'
        jobe:WarrantyStatusDate = TODAY()
      
    END ! CASE
    
    Access:SBO_WarrantyClaims.ClearKey(sbojow:JobNumberOnlyKey)
    sbojow:JobNumber = jow:RefNumber
    SET(sbojow:JobNumberOnlyKey,sbojow:JobNumberOnlyKey)
    LOOP UNTIL Access:SBO_WarrantyClaims.Next() <> Level:Benign
        IF (sbojow:JobNumber <> jow:RefNumber)
            BREAK
        END ! IF
        IF (sbojow:RRCStatus <> jow:RRCStatus)
            sbojow:RRCStatus = jow:RRCStatus
            Access:SBO_WarrantyClaims.TryUpdate()
        END ! IF
    END ! LOOP
  
    IF (Access:JOBS.TryUpdate() = Level:Benign)
        Access:WEBJOB.TryUpdate()
        Access:JOBSE.TryUpdate()
      
        IF (p_web.GSV('Pending') = TRUE AND job:EDI = 'NO')
            Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
            jobe2:RefNumber = job:Ref_Number
            IF (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
                jobe2:InPendingDate = TODAY()
                Access:JOBSE2.TryUpdate()
            ELSE ! IF
                IF (Access:JOBSE2.PrimeRecord() = Level:Benign)
                    jobe2:RefNumber = job:Ref_Number
                    jobe2:InPendingDate = TODAY()
                    IF (Access:JOBSE2.TryInsert())
                        Access:JOBSE2.CancelAutoInc()
                    END ! IF
                END ! IF
            END ! IF
        END ! IF
      
        CASE p_web.GSV('RType')
        OF 1
            AddToAudit(p_web,job:Ref_Number,'JOB','WARRANTY CLAIM RESUBMITTED','REASON: ' & p_web.GSV('locRejectionReason'))
        OF 2
            AddToAudit(p_web,job:Ref_Number,'JOB','WARRANTY CLAIM REJECTION ACKNOWLEDGED','REASON: ' & p_web.GSV('locRejectionReason'))
        END ! CASE        
    END ! IF      
    
    
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('WarrantyProcessRejection:Primed',0)
  p_web.StoreValue('locRejectionReason')
