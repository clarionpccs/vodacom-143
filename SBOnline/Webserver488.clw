

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER488.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER024.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER285.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER324.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER325.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER331.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER332.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER489.INC'),ONCE        !Req'd for module callout resolution
                     END


WarrantyClaims       PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locWarrantyDateFilter LONG                                 !
locWarrantyStartDate DATE                                  !
locWarrantyEndDate   DATE                                  !
locWarrantyManufacturerFilter LONG                         !
locWarrantyManufacturer STRING(30)                         !
locEDIType           STRING(3)                             !
locPending           LONG                                  !
locApproved          LONG                                  !
locRejected          LONG                                  !
locAcceptedRejected  LONG                                  !
locFinalRejection    LONG                                  !
locPaid              LONG                                  !
locSelectAll         LONG                                  !
locTotalAvailableJobs LONG                                 !
                    MAP
SelectNextEDIOption     PROCEDURE()
ValuateClaims           PROCEDURE(),STRING
                    END ! MAP
FilesOpened     Long
JOBS::State  USHORT
JOBSE::State  USHORT
SBO_WarrantyClaims::State  USHORT
AUDIT2::State  USHORT
AUDIT::State  USHORT
MANUFACT::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
locWarrantyManufacturer_OptionView   View(MANUFACT)
                          Project(man:Manufacturer)
                          Project(man:Manufacturer)
                        End
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('WarrantyClaims')
  loc:formname = 'WarrantyClaims_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('WarrantyClaims','')
      do SendPacket
      Do messageBox
      do SendPacket
    p_web._DivHeader('WarrantyClaims',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferWarrantyClaims',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferWarrantyClaims',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferWarrantyClaims',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_WarrantyClaims',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferWarrantyClaims',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_WarrantyClaims',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
DeleteSessionValues ROUTINE ! Auto generated by Bryan's Template
    ! Local Variables
    ! p_web.DeleteSessionValue('Ans') ! Excluded
    p_web.DeleteSessionValue('locWarrantyDateFilter')
    p_web.DeleteSessionValue('locWarrantyStartDate')
    p_web.DeleteSessionValue('locWarrantyEndDate')
    p_web.DeleteSessionValue('locWarrantyManufacturerFilter')
    p_web.DeleteSessionValue('locWarrantyManufacturer')
    p_web.DeleteSessionValue('locEDIType')
    p_web.DeleteSessionValue('locPending')
    p_web.DeleteSessionValue('locApproved')
    p_web.DeleteSessionValue('locRejected')
    p_web.DeleteSessionValue('locAcceptedRejected')
    p_web.DeleteSessionValue('locFinalRejection')
    p_web.DeleteSessionValue('locPaid')
    p_web.DeleteSessionValue('locSelectAll')
    p_web.DeleteSessionValue('locTotalAvailableJobs')

    ! Other Variables
OpenFiles  ROUTINE
  p_web._OpenFile(JOBS)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(SBO_WarrantyClaims)
  p_web._OpenFile(AUDIT2)
  p_web._OpenFile(AUDIT)
  p_web._OpenFile(MANUFACT)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(SBO_WarrantyClaims)
  p_Web._CloseFile(AUDIT2)
  p_Web._CloseFile(AUDIT)
  p_Web._CloseFile(MANUFACT)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('WarrantyClaims_form:inited_',1)
  do RestoreMem

CancelForm  Routine
    DO DeleteSessionValues  

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('locWarrantyStartDate')
    p_web.SetPicture('locWarrantyStartDate','@d06')
  End
  p_web.SetSessionPicture('locWarrantyStartDate','@d06')
  If p_web.IfExistsValue('locWarrantyEndDate')
    p_web.SetPicture('locWarrantyEndDate','@d06')
  End
  p_web.SetSessionPicture('locWarrantyEndDate','@d06')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'locWarrantyManufacturer'
    p_web.setsessionvalue('showtab_WarrantyClaims',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MANUFACT)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.')
  End
  If p_web.GSV('locTotalAvailableJobs') > 0
    loc:TabNumber += 1
  End
  If p_web.GSV('locTotalAvailableJobs') > 0
    loc:TabNumber += 1
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locWarrantyStartDate',locWarrantyStartDate)
  p_web.SetSessionValue('locWarrantyEndDate',locWarrantyEndDate)
  p_web.SetSessionValue('locSelectAll',locSelectAll)
  p_web.SetSessionValue('locPending',locPending)
  p_web.SetSessionValue('locApproved',locApproved)
  p_web.SetSessionValue('locRejected',locRejected)
  p_web.SetSessionValue('locAcceptedRejected',locAcceptedRejected)
  p_web.SetSessionValue('locFinalRejection',locFinalRejection)
  p_web.SetSessionValue('locPaid',locPaid)
  p_web.SetSessionValue('locTotalAvailableJobs',locTotalAvailableJobs)
  p_web.SetSessionValue('locEDIType',locEDIType)
  p_web.SetSessionValue('locWarrantyManufacturerFilter',locWarrantyManufacturerFilter)
  p_web.SetSessionValue('locWarrantyManufacturer',locWarrantyManufacturer)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locWarrantyStartDate')
    locWarrantyStartDate = p_web.dformat(clip(p_web.GetValue('locWarrantyStartDate')),'@d06')
    p_web.SetSessionValue('locWarrantyStartDate',locWarrantyStartDate)
  End
  if p_web.IfExistsValue('locWarrantyEndDate')
    locWarrantyEndDate = p_web.dformat(clip(p_web.GetValue('locWarrantyEndDate')),'@d06')
    p_web.SetSessionValue('locWarrantyEndDate',locWarrantyEndDate)
  End
  if p_web.IfExistsValue('locSelectAll')
    locSelectAll = p_web.GetValue('locSelectAll')
    p_web.SetSessionValue('locSelectAll',locSelectAll)
  End
  if p_web.IfExistsValue('locPending')
    locPending = p_web.GetValue('locPending')
    p_web.SetSessionValue('locPending',locPending)
  End
  if p_web.IfExistsValue('locApproved')
    locApproved = p_web.GetValue('locApproved')
    p_web.SetSessionValue('locApproved',locApproved)
  End
  if p_web.IfExistsValue('locRejected')
    locRejected = p_web.GetValue('locRejected')
    p_web.SetSessionValue('locRejected',locRejected)
  End
  if p_web.IfExistsValue('locAcceptedRejected')
    locAcceptedRejected = p_web.GetValue('locAcceptedRejected')
    p_web.SetSessionValue('locAcceptedRejected',locAcceptedRejected)
  End
  if p_web.IfExistsValue('locFinalRejection')
    locFinalRejection = p_web.GetValue('locFinalRejection')
    p_web.SetSessionValue('locFinalRejection',locFinalRejection)
  End
  if p_web.IfExistsValue('locPaid')
    locPaid = p_web.GetValue('locPaid')
    p_web.SetSessionValue('locPaid',locPaid)
  End
  if p_web.IfExistsValue('locTotalAvailableJobs')
    locTotalAvailableJobs = p_web.GetValue('locTotalAvailableJobs')
    p_web.SetSessionValue('locTotalAvailableJobs',locTotalAvailableJobs)
  End
  if p_web.IfExistsValue('locEDIType')
    locEDIType = p_web.GetValue('locEDIType')
    p_web.SetSessionValue('locEDIType',locEDIType)
  End
  if p_web.IfExistsValue('locWarrantyManufacturerFilter')
    locWarrantyManufacturerFilter = p_web.GetValue('locWarrantyManufacturerFilter')
    p_web.SetSessionValue('locWarrantyManufacturerFilter',locWarrantyManufacturerFilter)
  End
  if p_web.IfExistsValue('locWarrantyManufacturer')
    locWarrantyManufacturer = p_web.GetValue('locWarrantyManufacturer')
    p_web.SetSessionValue('locWarrantyManufacturer',locWarrantyManufacturer)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('WarrantyClaims_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    ! Preset Variables
    IF (p_web.GetValue('firsttime') = 1)
        p_web.SSV('locSelectAll',1)
        p_web.SSV('locEDIType','NO')  
        p_web.SSV('locWarrantyStartDate',DATE(MONTH(TODAY()),1,YEAR(TODAY())))
        p_web.SSV('locWarrantyEndDate',TODAY())
        p_web.SSV('locWarrantyManufacturerFilter',0)
        p_web.SSV('locWarrantyManufacturer','')
        ClearSBOWarrantyClaims(p_web)
    ELSE
        ! Auto select the correct radio button
        IF (p_web.GSV('locSelectAll') <> 1)
            CASE p_web.GSV('locEDIType')
            OF 'NO'
                IF (p_web.GSV('locPendingSelected') <> 1)
                    SelectNextEDIOption()
                END ! IF
            OF 'APP'
                IF (p_web.GSV('locApprovedSelected') <> 1)
                    SelectNextEDIOption()
                END ! IF
            OF 'EXC'
                IF (p_web.GSV('locRejectedSelected') <> 1)
                    SelectNextEDIOption()
                END ! IF
            OF 'AAJ'
                IF (p_web.GSV('locAcceptedRejectedSelected') <> 1)
                    SelectNextEDIOption()
                END ! IF
            OF 'REJ'
                IF (p_web.GSV('locFinalRejectionSelected') <> 1)
                    SelectNextEDIOption()
                END ! IF
            OF 'PAY'
                IF (p_web.GSV('locPaidSelected') <> 1)
                    SelectNextEDIOption()
                END ! IF
            END ! CASE
        END ! IF
    END ! IF
    
  !    p_web.SSV('jow:RecordNumber',0) ! Clear record pointer
    p_web.SSV('ViewJobReturnURL','WarrantyClaims')  !Needed so the browse jobs returns here
      p_web.site.CancelButton.TextValue = 'Close'
      p_web.site.CancelButton.Image = 'images/psave.png'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locWarrantyStartDate = p_web.RestoreValue('locWarrantyStartDate')
 locWarrantyEndDate = p_web.RestoreValue('locWarrantyEndDate')
 locSelectAll = p_web.RestoreValue('locSelectAll')
 locPending = p_web.RestoreValue('locPending')
 locApproved = p_web.RestoreValue('locApproved')
 locRejected = p_web.RestoreValue('locRejected')
 locAcceptedRejected = p_web.RestoreValue('locAcceptedRejected')
 locFinalRejection = p_web.RestoreValue('locFinalRejection')
 locPaid = p_web.RestoreValue('locPaid')
 locTotalAvailableJobs = p_web.RestoreValue('locTotalAvailableJobs')
 locEDIType = p_web.RestoreValue('locEDIType')
 locWarrantyManufacturerFilter = p_web.RestoreValue('locWarrantyManufacturerFilter')
 locWarrantyManufacturer = p_web.RestoreValue('locWarrantyManufacturer')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferWarrantyClaims')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('WarrantyClaims_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('WarrantyClaims_ChainTo')
    loc:formaction = p_web.GetSessionValue('WarrantyClaims_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'IndexPage'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="WarrantyClaims" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="WarrantyClaims" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="WarrantyClaims" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Warranty Claims') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Warranty Claims',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_WarrantyClaims">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_WarrantyClaims" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_WarrantyClaims')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Criteria') & ''''
        If p_web.GSV('locTotalAvailableJobs') > 0
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Browse Jobs') & ''''
        End
        If p_web.GSV('locTotalAvailableJobs') > 0
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        End
        Loc:Tabnumber = p_web.getSessionValue('showtab_WarrantyClaims')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_WarrantyClaims'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('WarrantyClaims_WarrantyClaimsBrowse_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locWarrantyStartDate')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          If p_web.GSV('locTotalAvailableJobs') > 0
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          End
          If p_web.GSV('locTotalAvailableJobs') > 0
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          End
          Loc:Tabnumber = p_web.getSessionValue('showtab_WarrantyClaims')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
    if p_web.GSV('locTotalAvailableJobs') > 0
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
    end
    if p_web.GSV('locTotalAvailableJobs') > 0
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
    end
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Criteria') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_WarrantyClaims_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& ' colspan="1">'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::txtMessage
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&' colspan="6">'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::txtMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locWarrantyStartDate
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locWarrantyStartDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locWarrantyEndDate
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locWarrantyEndDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::hidden
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locSelectAll
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locSelectAll
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locPending
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locPending
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locApproved
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locApproved
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locRejected
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locRejected
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAcceptedRejected
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAcceptedRejected
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFinalRejection
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locFinalRejection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locPaid
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locPaid
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& ' colspan="1">'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::txtDateMessage
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&' colspan="6">'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::txtDateMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnBuildJobsList
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locTotalAvailableJobs
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locTotalAvailableJobs
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::__Line
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GSV('locTotalAvailableJobs') > 0
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& ' colspan="1">'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locEDIType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&' colspan="6">'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locEDIType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('locTotalAvailableJobs') > 0
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locWarrantyManufacturerFilter
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locWarrantyManufacturerFilter
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('locTotalAvailableJobs') > 0
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locWarrantyManufacturer
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locWarrantyManufacturer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
  If p_web.GSV('locTotalAvailableJobs') > 0
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Browse Jobs') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_WarrantyClaims_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Jobs')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Jobs')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Jobs')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Jobs')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwWarrantyClaims
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab2  Routine
  If p_web.GSV('locTotalAvailableJobs') > 0
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_WarrantyClaims_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnValuateClaims
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnExport
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GSV('BookingSite') = 'RRC'
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnClaimPaid
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnProcessRejection
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnImportAccRej
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnImportResubmissions
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end


Prompt::txtMessage  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('txtMessage') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::txtMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtMessage',p_web.GetValue('NewValue'))
    do Value::txtMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtMessage  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('txtMessage') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web.Translate('To display jobs, select a date range and press "Build Jobs List"<br/>Then use the filters below to limit the data displayed',1) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locWarrantyStartDate  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('locWarrantyStartDate') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Start Date')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locWarrantyStartDate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locWarrantyStartDate',p_web.GetValue('NewValue'))
    locWarrantyStartDate = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locWarrantyStartDate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locWarrantyStartDate',p_web.dFormat(p_web.GetValue('Value'),'@d06'))
    locWarrantyStartDate = p_web.Dformat(p_web.GetValue('Value'),'@d06') !
  End
  do Value::locWarrantyStartDate
  do SendAlert
  do Value::brwWarrantyClaims  !1

Value::locWarrantyStartDate  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('locWarrantyStartDate') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locWarrantyStartDate
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locWarrantyStartDate')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locWarrantyStartDate'',''warrantyclaims_locwarrantystartdate_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locWarrantyStartDate')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locWarrantyStartDate',p_web.GetSessionValue('locWarrantyStartDate'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06',loc:javascript,,) & '<13,10>'
  do SendPacket
  packet = clip(packet) & '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar' & |
                  '(locWarrantyStartDate,''dd/mm/yyyy'',this); Date.disabled=true;' & |
                  'sv(''...'',''WarrantyClaims_pickdate_value'',1,FieldValue(this,1));nextFocus(WarrantyClaims_frm,'''',0);" ' & |
                  'value="Select Date" name="Date" type="button">...</button>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyClaims_' & p_web._nocolon('locWarrantyStartDate') & '_value')


Prompt::locWarrantyEndDate  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('locWarrantyEndDate') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('End Date')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locWarrantyEndDate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locWarrantyEndDate',p_web.GetValue('NewValue'))
    locWarrantyEndDate = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locWarrantyEndDate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locWarrantyEndDate',p_web.dFormat(p_web.GetValue('Value'),'@d06'))
    locWarrantyEndDate = p_web.Dformat(p_web.GetValue('Value'),'@d06') !
  End
  do Value::locWarrantyEndDate
  do SendAlert
  do Value::brwWarrantyClaims  !1

Value::locWarrantyEndDate  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('locWarrantyEndDate') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locWarrantyEndDate
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locWarrantyEndDate')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locWarrantyEndDate'',''warrantyclaims_locwarrantyenddate_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locWarrantyEndDate')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locWarrantyEndDate',p_web.GetSessionValue('locWarrantyEndDate'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06',loc:javascript,,) & '<13,10>'
  do SendPacket
  packet = clip(packet) & '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar' & |
                  '(locWarrantyEndDate,''dd/mm/yyyy'',this); Date.disabled=true;' & |
                  'sv(''...'',''WarrantyClaims_pickdate_value'',1,FieldValue(this,1));nextFocus(WarrantyClaims_frm,'''',0);" ' & |
                  'value="Select Date" name="Date" type="button">...</button>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyClaims_' & p_web._nocolon('locWarrantyEndDate') & '_value')


Validate::hidden  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('hidden',p_web.GetValue('NewValue'))
    do Value::hidden
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::hidden  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('hidden') & '_value',Choose(TRUE,'hdiv','adiv'))
  loc:extra = ''
  If Not (TRUE)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Prompt::locSelectAll  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('locSelectAll') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Include Jobs For All Claim Types')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyClaims_' & p_web._nocolon('locSelectAll') & '_prompt')

Validate::locSelectAll  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locSelectAll',p_web.GetValue('NewValue'))
    locSelectAll = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locSelectAll
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locSelectAll',p_web.GetValue('Value'))
    locSelectAll = p_web.GetValue('Value')
  End
  do Value::locSelectAll
  do SendAlert
  do Prompt::locSelectAll
  do Prompt::locAcceptedRejected
  do Value::locAcceptedRejected  !1
  do Prompt::locApproved
  do Value::locApproved  !1
  do Prompt::locFinalRejection
  do Value::locFinalRejection  !1
  do Prompt::locPaid
  do Value::locPaid  !1
  do Prompt::locPending
  do Value::locPending  !1
  do Prompt::locRejected
  do Value::locRejected  !1
  do Prompt::locEDIType
  do Value::locEDIType  !1

Value::locSelectAll  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('locSelectAll') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locSelectAll
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locSelectAll'',''warrantyclaims_locselectall_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locSelectAll')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locSelectAll') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locSelectAll',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyClaims_' & p_web._nocolon('locSelectAll') & '_value')


Prompt::locPending  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('locPending') & '_prompt',Choose(p_web.GSV('locSelectAll') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Include Pending Jobs')
  If p_web.GSV('locSelectAll') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyClaims_' & p_web._nocolon('locPending') & '_prompt')

Validate::locPending  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locPending',p_web.GetValue('NewValue'))
    locPending = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locPending
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locPending',p_web.GetValue('Value'))
    locPending = p_web.GetValue('Value')
  End
  do Value::locPending
  do SendAlert
  do Value::locEDIType  !1

Value::locPending  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('locPending') & '_value',Choose(p_web.GSV('locSelectAll') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locSelectAll') = 1)
  ! --- CHECKBOX --- locPending
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locPending'',''warrantyclaims_locpending_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locPending')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locPending') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locPending',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyClaims_' & p_web._nocolon('locPending') & '_value')


Prompt::locApproved  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('locApproved') & '_prompt',Choose(p_web.GSV('locSelectAll') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Include Approved Jobs')
  If p_web.GSV('locSelectAll') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyClaims_' & p_web._nocolon('locApproved') & '_prompt')

Validate::locApproved  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locApproved',p_web.GetValue('NewValue'))
    locApproved = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locApproved
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locApproved',p_web.GetValue('Value'))
    locApproved = p_web.GetValue('Value')
  End
  do Value::locApproved
  do SendAlert
  do Value::locEDIType  !1

Value::locApproved  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('locApproved') & '_value',Choose(p_web.GSV('locSelectAll') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locSelectAll') = 1)
  ! --- CHECKBOX --- locApproved
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locApproved'',''warrantyclaims_locapproved_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locApproved')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locApproved') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locApproved',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyClaims_' & p_web._nocolon('locApproved') & '_value')


Prompt::locRejected  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('locRejected') & '_prompt',Choose(p_web.GSV('locSelectAll') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Include Rejected Jobs')
  If p_web.GSV('locSelectAll') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyClaims_' & p_web._nocolon('locRejected') & '_prompt')

Validate::locRejected  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locRejected',p_web.GetValue('NewValue'))
    locRejected = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locRejected
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locRejected',p_web.GetValue('Value'))
    locRejected = p_web.GetValue('Value')
  End
  do Value::locRejected
  do SendAlert
  do Value::locEDIType  !1

Value::locRejected  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('locRejected') & '_value',Choose(p_web.GSV('locSelectAll') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locSelectAll') = 1)
  ! --- CHECKBOX --- locRejected
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locRejected'',''warrantyclaims_locrejected_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locRejected')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locRejected') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locRejected',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyClaims_' & p_web._nocolon('locRejected') & '_value')


Prompt::locAcceptedRejected  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('locAcceptedRejected') & '_prompt',Choose(p_web.GSV('locSelectAll') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Include Accepted/Rejected Jobs')
  If p_web.GSV('locSelectAll') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyClaims_' & p_web._nocolon('locAcceptedRejected') & '_prompt')

Validate::locAcceptedRejected  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAcceptedRejected',p_web.GetValue('NewValue'))
    locAcceptedRejected = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAcceptedRejected
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locAcceptedRejected',p_web.GetValue('Value'))
    locAcceptedRejected = p_web.GetValue('Value')
  End
  do Value::locAcceptedRejected
  do SendAlert
  do Value::locEDIType  !1

Value::locAcceptedRejected  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('locAcceptedRejected') & '_value',Choose(p_web.GSV('locSelectAll') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locSelectAll') = 1)
  ! --- CHECKBOX --- locAcceptedRejected
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locAcceptedRejected'',''warrantyclaims_locacceptedrejected_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locAcceptedRejected')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locAcceptedRejected') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locAcceptedRejected',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyClaims_' & p_web._nocolon('locAcceptedRejected') & '_value')


Prompt::locFinalRejection  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('locFinalRejection') & '_prompt',Choose(p_web.GSV('locSelectAll') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Include Final Rejection Jobs')
  If p_web.GSV('locSelectAll') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyClaims_' & p_web._nocolon('locFinalRejection') & '_prompt')

Validate::locFinalRejection  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFinalRejection',p_web.GetValue('NewValue'))
    locFinalRejection = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFinalRejection
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locFinalRejection',p_web.GetValue('Value'))
    locFinalRejection = p_web.GetValue('Value')
  End
  do Value::locFinalRejection
  do SendAlert
  do Value::locEDIType  !1

Value::locFinalRejection  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('locFinalRejection') & '_value',Choose(p_web.GSV('locSelectAll') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locSelectAll') = 1)
  ! --- CHECKBOX --- locFinalRejection
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locFinalRejection'',''warrantyclaims_locfinalrejection_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locFinalRejection')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locFinalRejection') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locFinalRejection',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyClaims_' & p_web._nocolon('locFinalRejection') & '_value')


Prompt::locPaid  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('locPaid') & '_prompt',Choose(p_web.GSV('locSelectAll') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Include Paid Jobs')
  If p_web.GSV('locSelectAll') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyClaims_' & p_web._nocolon('locPaid') & '_prompt')

Validate::locPaid  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locPaid',p_web.GetValue('NewValue'))
    locPaid = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locPaid
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locPaid',p_web.GetValue('Value'))
    locPaid = p_web.GetValue('Value')
  End
  do Value::locPaid
  do SendAlert
  do Value::locEDIType  !1

Value::locPaid  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('locPaid') & '_value',Choose(p_web.GSV('locSelectAll') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locSelectAll') = 1)
  ! --- CHECKBOX --- locPaid
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locPaid'',''warrantyclaims_locpaid_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locPaid')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locPaid') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locPaid',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyClaims_' & p_web._nocolon('locPaid') & '_value')


Prompt::txtDateMessage  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('txtDateMessage') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::txtDateMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtDateMessage',p_web.GetValue('NewValue'))
    do Value::txtDateMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtDateMessage  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('txtDateMessage') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('SmallText')&'">' & p_web.Translate('Note: The date range criteria to include jobs will vary depending on the Claim Type of each job:<br/>"Pending" = Date Claimed, "Approved" = Date Accepted, "Rejected" = Date Rejected, "Accepted/Rejected & Final Rejection" = Date Final Rejection and "Paid" = Date Reconciled.',1) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::btnBuildJobsList  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnBuildJobsList',p_web.GetValue('NewValue'))
    do Value::btnBuildJobsList
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnBuildJobsList  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('btnBuildJobsList') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnBuildJobsList','Build Jobs List','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PageProcess?' &'ProcessType=BuildWarrantyClaimsBrowse&ReturnURL=WarrantyClaims')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()


Prompt::locTotalAvailableJobs  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('locTotalAvailableJobs') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Total Available Jobs:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locTotalAvailableJobs  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locTotalAvailableJobs',p_web.GetValue('NewValue'))
    locTotalAvailableJobs = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locTotalAvailableJobs
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locTotalAvailableJobs',p_web.GetValue('Value'))
    locTotalAvailableJobs = p_web.GetValue('Value')
  End

Value::locTotalAvailableJobs  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('locTotalAvailableJobs') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locTotalAvailableJobs
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('red bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locTotalAvailableJobs'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::__Line  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('__Line',p_web.GetValue('NewValue'))
    do Value::__Line
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::__Line  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('__Line') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locEDIType  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('locEDIType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Filter By Claim Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyClaims_' & p_web._nocolon('locEDIType') & '_prompt')

Validate::locEDIType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locEDIType',p_web.GetValue('NewValue'))
    locEDIType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locEDIType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locEDIType',p_web.GetValue('Value'))
    locEDIType = p_web.GetValue('Value')
  End
  do Value::locEDIType
  do SendAlert
  do Value::brwWarrantyClaims  !1
  do Value::btnClaimPaid  !1
  do Value::btnProcessRejection  !1
  do Value::btnImportResubmissions  !1
  do Value::btnImportAccRej  !1
  do Value::btnValuateClaims  !1

Value::locEDIType  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('locEDIType') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- locEDIType
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locEDIType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  if p_web.GSV('locPendingSelected') = 1
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locEDIType') = 'NO'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locEDIType'',''warrantyclaims_loceditype_value'',1,'''&clip('NO')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locEDIType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locEDIType',clip('NO'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locEDIType_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate(p_web.GSV('locPendingCount')) & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  end
  if p_web.GSV('locApprovedSelected') = 1
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locEDIType') = 'APP'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locEDIType'',''warrantyclaims_loceditype_value'',1,'''&clip('APP')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locEDIType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locEDIType',clip('APP'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locEDIType_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate(p_web.GSV('locApprovedCount')) & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  end
  if p_web.GSV('locRejectedSelected') = 1
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locEDIType') = 'EXC'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locEDIType'',''warrantyclaims_loceditype_value'',1,'''&clip('EXC')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locEDIType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locEDIType',clip('EXC'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locEDIType_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate(p_web.GSV('locRejectedCount')) & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  end
  if p_web.GSV('locAcceptedRejectedSelected') = 1
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locEDIType') = 'AAJ'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locEDIType'',''warrantyclaims_loceditype_value'',1,'''&clip('AAJ')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locEDIType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locEDIType',clip('AAJ'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locEDIType_4') & '<13,10>'
    packet = clip(packet) & p_web.Translate(p_web.GSV('locAcceptedRejectedCount')) & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  end
  if p_web.GSV('locFinalRejectionSelected') = 1
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locEDIType') = 'REJ'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locEDIType'',''warrantyclaims_loceditype_value'',1,'''&clip('REJ')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locEDIType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locEDIType',clip('REJ'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locEDIType_5') & '<13,10>'
    packet = clip(packet) & p_web.Translate(p_web.GSV('locFinalRejectionCount')) & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  end
  if p_web.GSV('locPaidSelected') = 1
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locEDIType') = 'PAY'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locEDIType'',''warrantyclaims_loceditype_value'',1,'''&clip('PAY')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locEDIType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locEDIType',clip('PAY'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locEDIType_6') & '<13,10>'
    packet = clip(packet) & p_web.Translate(p_web.GSV('locPaidCount')) & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  end
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyClaims_' & p_web._nocolon('locEDIType') & '_value')


Prompt::locWarrantyManufacturerFilter  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('locWarrantyManufacturerFilter') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Filter By Manufacturer')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locWarrantyManufacturerFilter  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locWarrantyManufacturerFilter',p_web.GetValue('NewValue'))
    locWarrantyManufacturerFilter = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locWarrantyManufacturerFilter
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locWarrantyManufacturerFilter',p_web.GetValue('Value'))
    locWarrantyManufacturerFilter = p_web.GetValue('Value')
  End
  do Value::locWarrantyManufacturerFilter
  do SendAlert
  do Value::brwWarrantyClaims  !1
  do Prompt::locWarrantyManufacturer
  do Value::locWarrantyManufacturer  !1

Value::locWarrantyManufacturerFilter  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('locWarrantyManufacturerFilter') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locWarrantyManufacturerFilter
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locWarrantyManufacturerFilter'',''warrantyclaims_locwarrantymanufacturerfilter_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locWarrantyManufacturerFilter')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locWarrantyManufacturerFilter') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locWarrantyManufacturerFilter',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyClaims_' & p_web._nocolon('locWarrantyManufacturerFilter') & '_value')


Prompt::locWarrantyManufacturer  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('locWarrantyManufacturer') & '_prompt',Choose(p_web.GSV('locWarrantyManufacturerFilter') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Manufacturer')
  If p_web.GSV('locWarrantyManufacturerFilter') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyClaims_' & p_web._nocolon('locWarrantyManufacturer') & '_prompt')

Validate::locWarrantyManufacturer  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locWarrantyManufacturer',p_web.GetValue('NewValue'))
    locWarrantyManufacturer = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locWarrantyManufacturer
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locWarrantyManufacturer',p_web.GetValue('Value'))
    locWarrantyManufacturer = p_web.GetValue('Value')
  End
  do Value::locWarrantyManufacturer
  do SendAlert
  do Value::brwWarrantyClaims  !1

Value::locWarrantyManufacturer  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('locWarrantyManufacturer') & '_value',Choose(p_web.GSV('locWarrantyManufacturerFilter') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locWarrantyManufacturerFilter') <> 1)
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locWarrantyManufacturer')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locWarrantyManufacturer'',''warrantyclaims_locwarrantymanufacturer_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locWarrantyManufacturer')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('locWarrantyManufacturer',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locWarrantyManufacturer') = 0
    p_web.SetSessionValue('locWarrantyManufacturer','')
  end
    packet = clip(packet) & p_web.CreateOption('','',choose('' = p_web.getsessionvalue('locWarrantyManufacturer')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(JOBS)
  bind(job:Record)
  p_web._OpenFile(JOBSE)
  bind(jobe:Record)
  p_web._OpenFile(SBO_WarrantyClaims)
  bind(sbojow:Record)
  p_web._OpenFile(AUDIT2)
  bind(aud2:Record)
  p_web._OpenFile(AUDIT)
  bind(aud:Record)
  p_web._OpenFile(MANUFACT)
  bind(man:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(locWarrantyManufacturer_OptionView)
  locWarrantyManufacturer_OptionView{prop:order} = 'UPPER(man:Manufacturer)'
  Set(locWarrantyManufacturer_OptionView)
  Loop
    Next(locWarrantyManufacturer_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('locWarrantyManufacturer') = 0
      p_web.SetSessionValue('locWarrantyManufacturer',man:Manufacturer)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(man:Manufacturer,man:Manufacturer,choose(man:Manufacturer = p_web.getsessionvalue('locWarrantyManufacturer')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(locWarrantyManufacturer_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(SBO_WarrantyClaims)
  p_Web._CloseFile(AUDIT2)
  p_Web._CloseFile(AUDIT)
  p_Web._CloseFile(MANUFACT)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyClaims_' & p_web._nocolon('locWarrantyManufacturer') & '_value')


Validate::brwWarrantyClaims  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwWarrantyClaims',p_web.GetValue('NewValue'))
    do Value::brwWarrantyClaims
  Else
    p_web.StoreValue('sbojow:RecordNumber')
  End
  do SendAlert
  do Value::btnProcessRejection  !1

Value::brwWarrantyClaims  Routine
  loc:extra = ''
  ! --- BROWSE ---  WarrantyClaimsBrowse --
  p_web.SetValue('WarrantyClaimsBrowse:NoForm',1)
  p_web.SetValue('WarrantyClaimsBrowse:FormName',loc:formname)
  p_web.SetValue('WarrantyClaimsBrowse:parentIs','Form')
  p_web.SetValue('_parentProc','WarrantyClaims')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('WarrantyClaims_WarrantyClaimsBrowse_embedded_div')&'"><!-- Net:WarrantyClaimsBrowse --></div><13,10>'
    p_web._DivHeader('WarrantyClaims_' & lower('WarrantyClaimsBrowse') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('WarrantyClaims_' & lower('WarrantyClaimsBrowse') & '_value')
  else
    packet = clip(packet) & '<!-- Net:WarrantyClaimsBrowse --><13,10>'
  end
  do SendPacket


Validate::btnValuateClaims  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnValuateClaims',p_web.GetValue('NewValue'))
    do Value::btnValuateClaims
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnValuateClaims  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('btnValuateClaims') & '_value',Choose(p_web.GSV('locTotalAvailableJobs') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locTotalAvailableJobs') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&clip('alert(''' & ValuateClaims() & ''')')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnValuateClaims','Valuate Claims','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyClaims_' & p_web._nocolon('btnValuateClaims') & '_value')


Validate::btnExport  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnExport',p_web.GetValue('NewValue'))
    do Value::btnExport
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnExport  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('btnExport') & '_value',Choose(p_web.GSV('locTotalAvailableJobs') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locTotalAvailableJobs') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnExport','Export','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PageProcess?' &'ProcessType=ExportWarrantyClaims')) & ''','''&clip('_blank')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()


Validate::btnClaimPaid  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnClaimPaid',p_web.GetValue('NewValue'))
    do Value::btnClaimPaid
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnClaimPaid  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('btnClaimPaid') & '_value',Choose(p_web.GSV('locEDIType') <> 'APP' OR p_web.GSV('locTotalAvailableJobs') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locEDIType') <> 'APP' OR p_web.GSV('locTotalAvailableJobs') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnClaimPaid','Claim Paid','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('WarrantyClaimPaidCriteria')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyClaims_' & p_web._nocolon('btnClaimPaid') & '_value')


Validate::btnProcessRejection  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnProcessRejection',p_web.GetValue('NewValue'))
    do Value::btnProcessRejection
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnProcessRejection  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('btnProcessRejection') & '_value',Choose(p_web.GSV('locEDIType') <> 'EXC' OR p_web.GSV('locTotalAvailableJobs') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locEDIType') <> 'EXC' OR p_web.GSV('locTotalAvailableJobs') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&clip('mbWarrantyClaims_ProcessRejection(' & p_web.GSV('sbojow:RecordNumber') & ')')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnProcessRejection','Process Rejection','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyClaims_' & p_web._nocolon('btnProcessRejection') & '_value')


Validate::btnImportAccRej  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnImportAccRej',p_web.GetValue('NewValue'))
    do Value::btnImportAccRej
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnImportAccRej  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('btnImportAccRej') & '_value',Choose(p_web.GSV('locEDIType') <> 'EXC' OR p_web.GSV('locTotalAvailableJobs') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locEDIType') <> 'EXC' OR p_web.GSV('locTotalAvailableJobs') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnImportAccRej','Import Accept Rejections','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('ImportFile?' &'ProcessType=RRCWarrantyAccRejImport&ReturnURL=WarrantyClaims&RedirectURL=WarrantyClaims')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyClaims_' & p_web._nocolon('btnImportAccRej') & '_value')


Validate::btnImportResubmissions  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnImportResubmissions',p_web.GetValue('NewValue'))
    do Value::btnImportResubmissions
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnImportResubmissions  Routine
  p_web._DivHeader('WarrantyClaims_' & p_web._nocolon('btnImportResubmissions') & '_value',Choose(p_web.GSV('locEDIType') <> 'EXC' OR p_web.GSV('locTotalAvailableJobs') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locEDIType') <> 'EXC' OR p_web.GSV('locTotalAvailableJobs') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnImportResubmissions','Import Resubmissions','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('ImportFile?' &'ProcessType=RRCWarrantyResubmission&ReturnURL=WarrantyClaims&RedirectURL=WarrantyClaims')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyClaims_' & p_web._nocolon('btnImportResubmissions') & '_value')


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('WarrantyClaims_locWarrantyStartDate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locWarrantyStartDate
      else
        do Value::locWarrantyStartDate
      end
  of lower('WarrantyClaims_locWarrantyEndDate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locWarrantyEndDate
      else
        do Value::locWarrantyEndDate
      end
  of lower('WarrantyClaims_locSelectAll_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locSelectAll
      else
        do Value::locSelectAll
      end
  of lower('WarrantyClaims_locPending_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locPending
      else
        do Value::locPending
      end
  of lower('WarrantyClaims_locApproved_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locApproved
      else
        do Value::locApproved
      end
  of lower('WarrantyClaims_locRejected_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locRejected
      else
        do Value::locRejected
      end
  of lower('WarrantyClaims_locAcceptedRejected_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locAcceptedRejected
      else
        do Value::locAcceptedRejected
      end
  of lower('WarrantyClaims_locFinalRejection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locFinalRejection
      else
        do Value::locFinalRejection
      end
  of lower('WarrantyClaims_locPaid_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locPaid
      else
        do Value::locPaid
      end
  of lower('WarrantyClaims_locEDIType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locEDIType
      else
        do Value::locEDIType
      end
  of lower('WarrantyClaims_locWarrantyManufacturerFilter_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locWarrantyManufacturerFilter
      else
        do Value::locWarrantyManufacturerFilter
      end
  of lower('WarrantyClaims_locWarrantyManufacturer_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locWarrantyManufacturer
      else
        do Value::locWarrantyManufacturer
      end
  of lower('WarrantyClaims_WarrantyClaimsBrowse_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::brwWarrantyClaims
      else
        do Value::brwWarrantyClaims
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('WarrantyClaims_form:ready_',1)
  p_web.SetSessionValue('WarrantyClaims_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_WarrantyClaims',0)

PreCopy  Routine
  p_web.SetValue('WarrantyClaims_form:ready_',1)
  p_web.SetSessionValue('WarrantyClaims_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_WarrantyClaims',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('WarrantyClaims_form:ready_',1)
  p_web.SetSessionValue('WarrantyClaims_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('WarrantyClaims:Primed',0)

PreDelete       Routine
  p_web.SetValue('WarrantyClaims_form:ready_',1)
  p_web.SetSessionValue('WarrantyClaims_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('WarrantyClaims:Primed',0)
  p_web.setsessionvalue('showtab_WarrantyClaims',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
          If p_web.IfExistsValue('locSelectAll') = 0
            p_web.SetValue('locSelectAll',0)
            locSelectAll = 0
          End
      If(p_web.GSV('locSelectAll') = 1)
          If p_web.IfExistsValue('locPending') = 0
            p_web.SetValue('locPending',0)
            locPending = 0
          End
      End
      If(p_web.GSV('locSelectAll') = 1)
          If p_web.IfExistsValue('locApproved') = 0
            p_web.SetValue('locApproved',0)
            locApproved = 0
          End
      End
      If(p_web.GSV('locSelectAll') = 1)
          If p_web.IfExistsValue('locRejected') = 0
            p_web.SetValue('locRejected',0)
            locRejected = 0
          End
      End
      If(p_web.GSV('locSelectAll') = 1)
          If p_web.IfExistsValue('locAcceptedRejected') = 0
            p_web.SetValue('locAcceptedRejected',0)
            locAcceptedRejected = 0
          End
      End
      If(p_web.GSV('locSelectAll') = 1)
          If p_web.IfExistsValue('locFinalRejection') = 0
            p_web.SetValue('locFinalRejection',0)
            locFinalRejection = 0
          End
      End
      If(p_web.GSV('locSelectAll') = 1)
          If p_web.IfExistsValue('locPaid') = 0
            p_web.SetValue('locPaid',0)
            locPaid = 0
          End
      End
    If (p_web.GSV('locTotalAvailableJobs') > 0)
          If p_web.IfExistsValue('locWarrantyManufacturerFilter') = 0
            p_web.SetValue('locWarrantyManufacturerFilter',0)
            locWarrantyManufacturerFilter = 0
          End
    End
  If p_web.GSV('locTotalAvailableJobs') > 0
  End
  If p_web.GSV('locTotalAvailableJobs') > 0
  End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('WarrantyClaims_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('WarrantyClaims_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
  If p_web.GSV('locTotalAvailableJobs') > 0
    loc:InvalidTab += 1
  End
  ! tab = 3
  If p_web.GSV('locTotalAvailableJobs') > 0
    loc:InvalidTab += 1
  End
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('WarrantyClaims:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('locWarrantyStartDate')
  p_web.StoreValue('locWarrantyEndDate')
  p_web.StoreValue('')
  p_web.StoreValue('locSelectAll')
  p_web.StoreValue('locPending')
  p_web.StoreValue('locApproved')
  p_web.StoreValue('locRejected')
  p_web.StoreValue('locAcceptedRejected')
  p_web.StoreValue('locFinalRejection')
  p_web.StoreValue('locPaid')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locTotalAvailableJobs')
  p_web.StoreValue('')
  p_web.StoreValue('locEDIType')
  p_web.StoreValue('locWarrantyManufacturerFilter')
  p_web.StoreValue('locWarrantyManufacturer')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
messageBox  Routine
  packet = clip(packet) & |
    '<<script src="/scripts/pccsMessageBox.js" type="text/javascript"><</script><13,10>'&|
    ''
SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('locWarrantyDateFilter',locWarrantyDateFilter) ! LONG
     p_web.SSV('locWarrantyStartDate',locWarrantyStartDate) ! DATE
     p_web.SSV('locWarrantyEndDate',locWarrantyEndDate) ! DATE
     p_web.SSV('locWarrantyManufacturerFilter',locWarrantyManufacturerFilter) ! LONG
     p_web.SSV('locWarrantyManufacturer',locWarrantyManufacturer) ! STRING(30)
     p_web.SSV('locEDIType',locEDIType) ! STRING(3)
     p_web.SSV('locPending',locPending) ! LONG
     p_web.SSV('locApproved',locApproved) ! LONG
     p_web.SSV('locRejected',locRejected) ! LONG
     p_web.SSV('locAcceptedRejected',locAcceptedRejected) ! LONG
     p_web.SSV('locFinalRejection',locFinalRejection) ! LONG
     p_web.SSV('locPaid',locPaid) ! LONG
     p_web.SSV('locSelectAll',locSelectAll) ! LONG
     p_web.SSV('locTotalAvailableJobs',locTotalAvailableJobs) ! LONG
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     locWarrantyDateFilter = p_web.GSV('locWarrantyDateFilter') ! LONG
     locWarrantyStartDate = p_web.GSV('locWarrantyStartDate') ! DATE
     locWarrantyEndDate = p_web.GSV('locWarrantyEndDate') ! DATE
     locWarrantyManufacturerFilter = p_web.GSV('locWarrantyManufacturerFilter') ! LONG
     locWarrantyManufacturer = p_web.GSV('locWarrantyManufacturer') ! STRING(30)
     locEDIType = p_web.GSV('locEDIType') ! STRING(3)
     locPending = p_web.GSV('locPending') ! LONG
     locApproved = p_web.GSV('locApproved') ! LONG
     locRejected = p_web.GSV('locRejected') ! LONG
     locAcceptedRejected = p_web.GSV('locAcceptedRejected') ! LONG
     locFinalRejection = p_web.GSV('locFinalRejection') ! LONG
     locPaid = p_web.GSV('locPaid') ! LONG
     locSelectAll = p_web.GSV('locSelectAll') ! LONG
     locTotalAvailableJobs = p_web.GSV('locTotalAvailableJobs') ! LONG
SelectNextEDIOption PROCEDURE()!,STRING
RetValue STRING(3)
loopCount LONG
    CODE
        CASE p_web.GSV('locEDIType')
        OF 'NO'
            loopCount = 1
        OF 'APP'
            loopCount = 2
        OF 'EXC'
            loopCount = 3
        OF 'AAJ'
            loopCount = 4
        OF 'REJ'
            loopCount = 5
        OF 'PAY'
            loopCount = 6
        ELSE
            RETURN 
        END ! IF
        LOOP
            loopCount += 1
            IF (loopCount > 6)
                loopCount = 1
            END ! IF
            CASE loopCount
            OF 1
                IF (p_web.GSV('locPendingSelected') = 1)
                    RetValue = 'NO'
                    BREAK
                END ! IF
            OF 2
                IF (p_web.GSV('locApprovedSelected') = 1)
                    RetValue = 'APP'
                    BREAK
                END ! IF
            OF 3
                IF (p_web.GSV('locRejectedSelected') = 1)
                    RetValue = 'EXC'
                    BREAK
                END ! IF
            OF 4
                IF (p_web.GSV('locAcceptedRejectedSelected') = 1)
                    RetValue = 'AAJ'
                    BREAK
                END ! IF
            OF 5
                IF (p_web.GSV('locFinalRejectionSelected') = 1)
                    RetValue = 'REJ'
                    BREAK
                END ! IF
            OF 6
                IF (p_web.GSV('locPaidSelected') = 1)
                    RetValue = 'PAY'
                    BREAK
                END ! IF
            END ! CASE
        END ! LOOP
        
        p_web.SSV('locEDIType',RetValue)
ValuateClaims       PROCEDURE()!,STRING
locClaimValue              REAL()
locCount LONG()
    CODE
    
        locClaimValue = 0
        Access:SBO_WarrantyClaims.ClearKey(sbojow:RRCStatusKey)
        sbojow:SessionID = p_web.SessionID
        sbojow:RRCStatus = p_web.GSV('locEDIType')
        SET(sbojow:RRCStatusKey,sbojow:RRCStatusKey)
        LOOP UNTIL Access:SBO_WarrantyClaims.Next() <> Level:Benign
            IF (sbojow:SessionID <> p_web.SessionID OR |
                sbojow:RRCStatus <> p_web.GSV('locEDIType'))
                BREAK
            END ! IF
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = sbojow:JobNumber
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
                CYCLE
            END ! IF
    
            locCount += 1 
            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = sbojow:JobNumber
            IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                locClaimValue += ROUND(jobe:RRCWLabourCost,.02) + ROUND(jobe:RRCWPartsCost,.02)
                       
            ELSE ! IF
                CYCLE
            END ! IF
    
        END ! LOOP
    
        RETURN 'Number of claims: ' & locCount & '\n\nValue Of Claims: ' & FORMAT(locClaimValue,@n_14.2)
