

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('WEBSERVER479.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER411.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! Report the STATUS File
!!! </summary>
SummaryStatusReport PROCEDURE (NetWebServerWorker p_web)

  ! The NetTalk Extension to ABC report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
firstJobStatus          LONG(1)
firstExchangeStatus     LONG(1)
firstLoanStatus LONG(1)
Progress:Thermometer BYTE                                  !
tmp:DefaultTelephone STRING(20)                            !
tmp:DefaultFax       STRING(20)                            !
locTotalJobs         LONG                                  !
tmp:PrintedBy        STRING(60)                            !
tmp:StartDate        DATE                                  !
tmp:EndDate          DATE                                  !
tmp:NumberRRC        LONG                                  !
locRRCTotal          LONG                                  !
locRecordCount       LONG                                  !
AddressGroup         GROUP,PRE(address)                    !
CompanyName          STRING(30)                            !
AddressLine1         STRING(30)                            !
AddressLine2         STRING(30)                            !
AddressLine3         STRING(30)                            !
Postcode             STRING(30)                            !
TelephoneNumber      STRING(30)                            !
FaxNumber            STRING(30)                            !
EmailAddress         STRING(255)                           !
                     END                                   !
Process:View         VIEW(STATUS)
                       PROJECT(sts:Status)
                     END
ReportPageNumber     LONG,AUTO
ProgressWindow       WINDOW('Report STATUS'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:DEFAULT), |
  DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('Status Summary Report'),AT(396,2760,7521,8531),PRE(RPT),PAPER(PAPER:A4),FONT('Arial', |
  10,,FONT:regular),THOUS
                       HEADER,AT(396,1000,7521,1000),USE(?unnamed)
                         STRING('Date Range: '),AT(5000,156),USE(?String22),FONT(,8),TRN
                         STRING(@d6),AT(5781,156),USE(tmp:StartDate),FONT(,8,,FONT:bold),TRN
                         STRING(@d6),AT(6615,156),USE(tmp:EndDate),FONT(,8,,FONT:bold),TRN
                         STRING('Printed By:'),AT(5000,365),USE(?String24),FONT(,8),TRN
                         STRING(@s60),AT(5781,365),USE(tmp:PrintedBy),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),TRN
                         STRING('Date Printed:'),AT(5000,583),USE(?ReportDatePrompt),FONT(,8),TRN
                         STRING('Page Number:'),AT(5000,781),USE(?String59),FONT(,8),TRN
                         STRING('<<-- Date Stamp -->'),AT(5792,583),USE(?ReportDateStamp),FONT(,8,,FONT:bold),TRN
                         STRING(@N3),AT(5833,792),USE(ReportPageNumber),FONT(,8,,FONT:bold),TRN
                       END
DETAIL                 DETAIL,AT(0,0,,229),USE(?DetailBand)
                         STRING(@s8),AT(5208,0),USE(qStatusReportStatus.RRCNumber),RIGHT,TRN
                         STRING(@s30),AT(2115,0),USE(qStatusReportStatus.Status),LEFT,TRN
                         STRING(@s8),AT(4479,0),USE(qStatusReportStatus.Number),RIGHT,TRN
                       END
totals                 DETAIL,AT(0,0),USE(?totals)
                         LINE,AT(2031,52,3438,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('Total Number Of Jobs:'),AT(2115,135),USE(?String21),TRN
                         STRING(@s8),AT(5208,104),USE(locRRCTotal),FONT(,,,FONT:bold),RIGHT,TRN
                         STRING(@s9),AT(4427,104),USE(locTotalJobs),FONT(,,,FONT:bold),RIGHT,TRN
                       END
JobStatusTitle         DETAIL,AT(0,0,,438),USE(?JobStatusTitle)
                         STRING('Job Status'),AT(3417,0),USE(?String21:2),FONT(,,,FONT:bold),TRN
                         STRING('ARC'),AT(4844,208),USE(?String34),FONT(,,,FONT:bold),TRN
                         STRING('RRC'),AT(5573,208),USE(?String34:2),FONT(,,,FONT:bold),TRN
                       END
ExchangeStatusTitle    DETAIL,AT(0,0,,490),USE(?ExchangeStatusTitle),PAGEBEFORE(-1)
                         STRING('Exchange Status'),AT(3208,0),USE(?String21:3),FONT(,,,FONT:bold),TRN
                       END
LoanStatusTitle        DETAIL,AT(0,0,,469),USE(?LoanStatusTitle),PAGEBEFORE(-1)
                         STRING('Loan Status'),AT(3375,0),USE(?String21:4),FONT(,,,FONT:bold),TRN
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE,AT(0,0,7521,11156),USE(?Image1)
                         STRING(@s30),AT(156,0,3844,240),USE(address:CompanyName),FONT(,16,,FONT:bold),LEFT
                         STRING('SUMMARY STATUS REPORT'),AT(4271,0,3177,260),USE(?String20),FONT(,16,,FONT:bold),RIGHT, |
  TRN
                         STRING(@s30),AT(156,260,3844,156),USE(address:AddressLine1),FONT(,9),TRN
                         STRING(@s30),AT(156,417,3844,156),USE(address:AddressLine2),FONT(,9),TRN
                         STRING(@s30),AT(156,573,3844,156),USE(address:AddressLine3),FONT(,9),TRN
                         STRING(@s30),AT(156,729,1156,156),USE(address:Postcode),FONT(,9),TRN
                         STRING('Tel:'),AT(156,885),USE(?String16),FONT(,9),TRN
                         STRING(@s30),AT(573,885),USE(address:TelephoneNumber),FONT(,9),TRN
                         STRING('Fax: '),AT(156,1042),USE(?String19),FONT(,9),TRN
                         STRING(@s30),AT(573,1042),USE(address:FaxNumber),FONT(,9),TRN
                         STRING(@s255),AT(573,1198,3844,198),USE(address:EmailAddress),FONT(,9),TRN
                         STRING('Email:'),AT(156,1198),USE(?String19:2),FONT(,9),TRN
                         STRING('Status Type'),AT(2135,2083),USE(?String44),FONT(,8,,FONT:bold),TRN
                         STRING('No Of Entries'),AT(4792,2083),USE(?String45),FONT(,8,,FONT:bold),TRN
                       END
                     END
!*** PDF-Tools v4.1 Start (All)
!--- ClarioNET NOT Enabled
!--- PDFXTABCTemplate: True  Level: 4
!--- PDFXTABCTemplate: RPM: False
!--- PDFXTABCTemplate: Icetips: False
PDFXTR3               PDFXToolsReportClass                 !  Added by PDF-Tools Templates v4.1
PDFXTR3:rtn           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXTR3:vpf           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Orientation      BYTE(0)                      !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:TopMargin        REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:BottomMargin     REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:LeftMargin       REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:RightMargin      REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Selection        STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PDFXT4:Papers:Queue           QUEUE,PRE()                  !  Added by PDF-Tools Templates v4.1
PLQ:Description                 STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PLQ:PaperCode                   BYTE                       !  Added by PDF-Tools Templates v4.1
                              END                          !  Added by PDF-Tools Templates v4.1
!*** PDF-Tools v4.1 End
ThisWindow           CLASS(ReportManager)
EndReport              PROCEDURE(),BYTE,DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Next                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepStringClass                       ! Progress Manager
Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.EndReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
        IF (firstJobStatus = FALSE OR firstExchangeStatus = FALSE OR firstLoanStatus = FALSE)
            PRINT(rpt:Totals)  
        END ! IF
  ReturnValue = PARENT.EndReport()
  RETURN ReturnValue


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SummaryStatusReport')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:STATUS.SetOpenRelated()
  Relate:STATUS.Open                                       ! File STATUS used by this procedure, so make sure it's RelationManager is open
  Relate:TRADEACC.SetOpenRelated()
  Relate:TRADEACC.Open                                     ! File TRADEACC used by this procedure, so make sure it's RelationManager is open
  Access:USERS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
        SORT(qStatusReportStatus,qStatusReportStatus.SessionID,qStatusReportStatus.StatusType,qStatusReportStatus.Status)  
        firstJobStatus = TRUE
        firstExchangeStatus = TRUE
        firstLoanStatus = TRUE
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('SummaryStatusReport',ProgressWindow)       ! Restore window settings from non-volatile store
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    loc:pdfname = '.\$$$' & format(random(1,99999),@n05) &'.pdf'
    ProgressWindow{prop:hide} = 1
  End
  ProgressMgr.Init(ScrollSort:AllowAlpha+ScrollSort:AllowNumeric,ScrollBy:RunTime)
  ThisReport.Init(Process:View, Relate:STATUS, ?Progress:PctText, Progress:Thermometer, RECORDS(qStatusReportStatus))
  ThisReport.AddSortOrder()
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      ReportNoRecords(p_web)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:STATUS.Close
    Relate:TRADEACC.Close
  END
  IF SELF.Opened
    INIMgr.Update('SummaryStatusReport',ProgressWindow)    ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Next PROCEDURE

ReturnValue          BYTE,AUTO

Progress BYTE,AUTO
  CODE
      ThisReport.RecordsProcessed+=1
      GET(qStatusReportStatus,ThisReport.RecordsProcessed)
      IF ERRORCODE() THEN
         ReturnValue = Level:Notify
      ELSE
         ReturnValue = Level:Benign
      END
      IF ReturnValue = Level:Notify
          IF ThisReport.RecordsProcessed>RECORDS(qStatusReportStatus)
             SELF.Response = RequestCompleted
             POST(EVENT:CloseWindow)
             RETURN Level:Notify
          ELSE
             SELF.Response = RequestCancelled
             POST(EVENT:CloseWindow)
             RETURN Level:Fatal
          END
      ELSE
         Progress = ThisReport.RecordsProcessed / ThisReport.RecordsToProcess*100
         IF Progress > 100 THEN Progress = 100.
         IF Progress <> Progress:Thermometer
           Progress:Thermometer = Progress
           DISPLAY()
         END
      END
      RETURN Level:Benign
  ReturnValue = PARENT.Next()
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.OpenReport()
        tmp:StartDate = p_web.GSV('locStartDate')
        tmp:EndDate = p_web.GSV('locEndDate')
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = p_web.GSV('BookingAccount')
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
            address:CompanyName     = tra:Company_Name
            address:AddressLine1    = tra:Address_Line1
            address:AddressLine2    = tra:Address_Line2
            address:AddressLine3    = tra:Address_Line3
            address:Postcode        = tra:Postcode
            address:TelephoneNumber = tra:Telephone_Number
            address:FaxNumber       = tra:Fax_Number
            address:EmailAddress    = tra:EmailAddress            
        END ! IF
        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = p_web.GSV('BookingUserCode')
        IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
            tmp:PrintedBy = CLIP(use:Forename) & ' ' & CLIP(use:Surname)
        END ! IF
        
        
  !*** PDF-Tools v4.1 Start (ABC)
  IF ~ReturnValue                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.Init(Report, loc:PDFName,,PDFA_OFF)            !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPaper(CHOOSE(SYSTEM{PROP:Autopaper}=True,Report{PROPPRINT:Paper},PAPER:USER), CHOOSE(Report{PROP:Thous}=True,PROP:Thous,CHOOSE(Report{PROP:MM}=True,PROP:MM,CHOOSE(Report{PROP:Points}=True,PROP:Points,0))), Report{PROPPRINT:PaperWidth}, Report{PROPPRINT:PaperHeight}, Report{PROP:Landscape}) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetCompression(TRUE,TRUE,FALSE,COMPRTYPE_C_AUTO,75,COMPRTYPE_I_AUTO,COMPRTYPE_M_AUTO) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetAuthor('ServiceBase 3g')          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetTitle('Summary Status Report')    !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetSubject('ServiceBase 3g')         !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPdfCreator('ServiceBase 3g')      !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetKeywords()                        !  Added by PDF-Tools Templates v4.1
    PDFXTR3.EnableLinkAnalyzer(True)             !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetSpecVersion(SPECVERSION_15,TRUE)  !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPageLayout(PAGELAYOUT_SINGLEPAGE) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPageMode(PAGEMODE_NONE)           !  Added by PDF-Tools Templates v4.1
    IF PAGEMODE_NONE = PAGEMODE_FULLSCREEN       !  Added by PDF-Tools Templates v4.1
      PDFXTR3:vpf = VP_FSPM_NONE                 !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
      PDFXTR3:vpf = 0                            !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDETOOLBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDEMENUBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDEWINDOWUI). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_FITWINDOW). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_CENTERWINDOW). !  Added by PDF-Tools Templates v4.1
      IF TRUE                                     THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_DISPLAYDOCTITLE). !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetViewerPrefs(PDFXTR3:vpf)          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPreview(False)                    !  Added by PDF-Tools Templates v4.1
  END
  !*** PDF-Tools v4.1 End
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@d06)
  END
  IF ReturnValue = Level:Benign
    Report$?ReportPageNumber{PROP:PageNo} = True
  END
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeCloseEvent()
  !*** PDF-Tools v4.1 Start (ABC)
  PDFXTR3.Kill()
  !*** PDF-Tools v4.1 End
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
        IF (qStatusReportStatus.SessionID <> p_web.SessionID)
            RETURN Level:User
        END !IF
        
        IF (qStatusReportStatus.StatusType = 0)
            IF (firstJobStatus = TRUE)
                PRINT(rpt:JobStatusTitle)
                firstJobStatus = FALSE
            END ! IF
        END ! IF
        
        IF (qStatusReportStatus.StatusType = 1)
            IF (firstExchangeStatus = TRUE)
                ! Were are Job Statuses Printed?
                ! If so, show totals
                IF (firstJobStatus = FALSE)
                    PRINT(rpt:Totals)
                    locTotalJobs = 0
                    firstJobStatus = TRUE
                END ! IF
                PRINT(rpt:ExchangeStatusTitle)
                firstExchangeStatus = FALSE
            END ! IF
            SETTARGET(REPORT)
            ?locRRCTotal{PROP:Hide} = 1
            ?qStatusReportStatus:RRCNumber{PROP:Hide} = 1
            SETTARGET()
        END ! IF
  
        IF (qStatusReportStatus.StatusType = 2)
            IF (firstLoanStatus = TRUE)
                ! Were any job, or loan status printed?
                ! If so, show totals
                IF (firstJobStatus = FALSE OR firstExchangeStatus = FALSE)
                    PRINT(rpt:Totals)
                    locTotalJobs = 0
                    firstJobStatus = TRUE
                    firstExchangeStatus = TRUE
                END ! IF
                PRINT(rpt:LoanStatusTitle)
                firstLoanStatus = FALSE
            END ! IF
            SETTARGET(REPORT)
            ?locRRCTotal{PROP:Hide} = 1
            ?qStatusReportStatus:RRCNumber{PROP:Hide} = 1
            SETTARGET()            
        END ! IF
        
        locTotalJobs += qStatusReportStatus.Number
        locRRCTotal += qStatusReportStatus.RRCNumber
        locRecordCount += 1
        
        
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:DETAIL)
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  !*** PDF-Tools v4.1 Start (ABC)
  ReturnValue = False                            !  Added by PDF-Tools Templates v4.1
    PDFXTR3:rtn = PDFXTR3.Generate(SELF.ImageQueue,True, , ) !  Added by PDF-Tools Templates v4.1
    IF PDFXTR3:rtn THEN                          !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
  RETURN ReturnValue
  !*** PDF-Tools v4.1 End
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
  RETURN ReturnValue

