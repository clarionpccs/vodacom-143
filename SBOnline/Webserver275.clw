

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER275.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER284.INC'),ONCE        !Req'd for module callout resolution
                     END


brwStockAllocation   PROCEDURE  (NetWebServerWorker p_web)
locEngineerName      STRING(60)                            !
locStockQuantity     LONG                                  !
locStockAvailable    BYTE                                  !
locDateReguested     STRING(10)                            !
locTimeRequested     STRING(10)                            !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(STOCKALL)
                      Project(stl:RecordNumber)
                      Project(stl:JobNumber)
                      Project(stl:ShelfLocation)
                      Project(stl:Description)
                      Project(stl:PartNumber)
                      Project(stl:Quantity)
                      Project(stl:PartRecordNumber)
                      Project(stl:PartType)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
STOCKALX::State  USHORT
STOCK::State  USHORT
USERS::State  USHORT
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('brwStockAllocation')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('brwStockAllocation:NoForm')
      loc:NoForm = p_web.GetValue('brwStockAllocation:NoForm')
      loc:FormName = p_web.GetValue('brwStockAllocation:FormName')
    else
      loc:FormName = 'brwStockAllocation_frm'
    End
    p_web.SSV('brwStockAllocation:NoForm',loc:NoForm)
    p_web.SSV('brwStockAllocation:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('brwStockAllocation:NoForm')
    loc:FormName = p_web.GSV('brwStockAllocation:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('brwStockAllocation') & '_' & lower(loc:parent)
  else
    loc:divname = lower('brwStockAllocation')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('adiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(STOCKALL,stl:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'STL:JOBNUMBER') then p_web.SetValue('brwStockAllocation_sort','3')
    ElsIf (loc:vorder = 'STL:SHELFLOCATION') then p_web.SetValue('brwStockAllocation_sort','2')
    ElsIf (loc:vorder = 'STL:DESCRIPTION') then p_web.SetValue('brwStockAllocation_sort','4')
    ElsIf (loc:vorder = 'STL:PARTNUMBER') then p_web.SetValue('brwStockAllocation_sort','5')
    ElsIf (loc:vorder = 'LOCENGINEERNAME') then p_web.SetValue('brwStockAllocation_sort','6')
    ElsIf (loc:vorder = '+STL:ENGINEER') then p_web.SetValue('brwStockAllocation_sort','6')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('brwStockAllocation:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('brwStockAllocation:LookupFrom','LookupFrom')
    p_web.StoreValue('brwStockAllocation:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('brwStockAllocation:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('brwStockAllocation:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 15
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('brwStockAllocation_sort',net:DontEvaluate)
  If loc:vordernumber = 0
    loc:vordernumber = 3
  End
  p_web.SetSessionValue('brwStockAllocation_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'stl:JobNumber','-stl:JobNumber')
    Loc:LocateField = 'stl:JobNumber'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(stl:ShelfLocation)','-UPPER(stl:ShelfLocation)')
    Loc:LocateField = 'stl:ShelfLocation'
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(stl:Description)','-UPPER(stl:Description)')
    Loc:LocateField = 'stl:Description'
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(stl:PartNumber)','-UPPER(stl:PartNumber)')
    Loc:LocateField = 'stl:PartNumber'
  of 6
    loc:vorder = Choose(Loc:SortDirection=1,'+stl:Engineer','-stl:Engineer')
    Loc:LocateField = 'stl:Engineer'
  of 11
    Loc:LocateField = ''
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(stl:Location),+UPPER(stl:Status),+UPPER(stl:PartNumber)'
  end
  If False ! add range fields to sort order
  ElsIf (p_web.GSV('locStockAllocationType') = 'ALL')
  ElsIf (p_web.GSV('locStockAllocationType') <> 'ALL')
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('stl:JobNumber')
    loc:SortHeader = p_web.Translate('Job Number')
    p_web.SetSessionValue('brwStockAllocation_LocatorPic','@s8')
  Of upper('stl:ShelfLocation')
    loc:SortHeader = p_web.Translate('Shelf Location')
    p_web.SetSessionValue('brwStockAllocation_LocatorPic','@s30')
  Of upper('stl:Description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('brwStockAllocation_LocatorPic','@s30')
  Of upper('stl:PartNumber')
    loc:SortHeader = p_web.Translate('Part Number')
    p_web.SetSessionValue('brwStockAllocation_LocatorPic','@s30')
  Of upper('locEngineerName')
  OrOf upper('stl:Engineer')
    loc:SortHeader = p_web.Translate('Engineer Name')
    p_web.SetSessionValue('brwStockAllocation_LocatorPic','@s60')
  Of upper('stl:Quantity')
    loc:SortHeader = p_web.Translate('Quantity')
    p_web.SetSessionValue('brwStockAllocation_LocatorPic','@s8')
  Of upper('locStockQuantity')
    loc:SortHeader = p_web.Translate('Stock Qty')
    p_web.SetSessionValue('brwStockAllocation_LocatorPic','@n_8')
  Of upper('locDateReguested')
    loc:SortHeader = p_web.Translate('Date Requested')
    p_web.SetSessionValue('brwStockAllocation_LocatorPic','@s10')
  Of upper('locTimeRequested')
    loc:SortHeader = p_web.Translate('Time Requested')
    p_web.SetSessionValue('brwStockAllocation_LocatorPic','@s10')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('brwStockAllocation:LookupFrom')
  End!Else
  loc:formaction = 'brwStockAllocation'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="brwStockAllocation:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="brwStockAllocation:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('brwStockAllocation:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="STOCKALL"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="stl:RecordNumberKey"></input><13,10>'
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'brwStockAllocation',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2brwStockAllocation',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="brwStockAllocation.locate(''Locator2brwStockAllocation'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'brwStockAllocation.cl(''brwStockAllocation'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="brwStockAllocation_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('BrowseTable')&'" id="brwStockAllocation_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','brwStockAllocation','Job Number','Click here to sort by Job Number',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Job Number')&'">'&p_web.Translate('Job Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','brwStockAllocation','Shelf Location','Click here to sort by Shelf Location',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Shelf Location')&'">'&p_web.Translate('Shelf Location')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'4','brwStockAllocation','Description','Click here to sort by Description',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Description')&'">'&p_web.Translate('Description')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'5','brwStockAllocation','Part Number','Click here to sort by Part Number',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Part Number')&'">'&p_web.Translate('Part Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'6','brwStockAllocation','Engineer Name',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Engineer Name')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th class="RightJustify" Title="'&p_web.Translate('Click here to sort by Quantity')&'">'&p_web.Translate('Quantity')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th class="RightJustify">'&p_web.Translate('Stock Qty')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th class="CenterJustify">'&p_web.Translate('Available')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th>'&p_web.Translate('Date Requested')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th>'&p_web.Translate('Time Requested')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
  If (p_web.GSV('RapidLocation') = 1) AND  true
        packet = clip(packet) & '<th class="CenterJustify">'&p_web.Translate('Status')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
  End ! Field condition
  If (p_web.GSV('locStockAllocationType') = 'WEB') AND  true
        packet = clip(packet) & '<th>'&NBSP&'</th>'&CRLF ! no heading for this column  FulFilRequest
        do AddPacket
        loc:columns += 1
  End ! Field condition
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,15,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('stl:recordnumber',lower(Thisview{prop:order}),1,1) = 0 !and STOCKALL{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'stl:RecordNumber'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('stl:RecordNumber'),p_web.GetValue('stl:RecordNumber'),p_web.GetSessionValue('stl:RecordNumber'))
  If False  ! Generate Filter
  ElsIf (p_web.GSV('locStockAllocationType') = 'ALL')
      loc:FilterWas = 'UPPER(stl:Location) = UPPER(''' & p_web.GSV('Default:SiteLocation') & ''')'
  ElsIf (p_web.GSV('locStockAllocationType') <> 'ALL')
      loc:FilterWas = 'UPPER(stl:Location) = UPPER(''' & p_web.GSV('Default:SiteLocation') & ''') AND UPPER(stl:Status) = UPPER(''' & p_web.GSV('locStockAllocationType') & ''')'
  Else
        loc:FilterWas = 'UPPER(stl:Location) = UPPER(''' & p_web.GSV('Default:SiteLocation') & ''') AND UPPER(stl:Status) = UPPER(''' & p_web.GSV('locStockAllocationType') & ''')'
  End
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'brwStockAllocation',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('brwStockAllocation_Filter')
    p_web.SetSessionValue('brwStockAllocation_FirstValue','')
    p_web.SetSessionValue('brwStockAllocation_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,STOCKALL,stl:RecordNumberKey,loc:PageRows,'brwStockAllocation',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If STOCKALL{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(STOCKALL,loc:firstvalue)
              Reset(ThisView,STOCKALL)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If STOCKALL{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(STOCKALL,loc:lastvalue)
            Reset(ThisView,STOCKALL)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      Access:USERS.Clearkey(use:User_Code_Key)
      use:User_Code   = stl:Engineer
      If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
          !Found
          locEngineerName    = stl:Engineer & ' (' & Clip(use:Forename) & ' ' & Clip(use:Surname) & ')'
      Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
          !Error
          locEngineerName    = stl:Engineer
      End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      
      !------------------------------------------------------------------
      !Show the pretty green box to show the stock is available
      locStockAvailable = 0
      locStockQuantity = 0
      
      Access:STOCK.Clearkey(sto:Ref_Number_Key)
      sto:Ref_Number  = stl:PartRefNumber
      If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
          !Found
          locStockQuantity   = sto:Quantity_Stock
          If locStockQuantity >= stl:Quantity
              locStockAvailable = 1
          End !If sto:Quantity_Stock <= stl:Quantity
      Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
          !Error
          Access:STOCK.ClearKey(sto:Location_Key)
          sto:Location    = use:Location
          sto:Part_Number = stl:PartNumber
          If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
              !Found
              locStockQuantity = sto:Quantity_Stock
              If locStockQuantity >= stl:Quantity
                  locStockAvailable = 1
              End !If sto:Quantity_Stock <= stl:Quantity
          Else!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
              locStockQuantity = ''
          End!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
      End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
      
      
      
      
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(stl:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'brwStockAllocation.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'brwStockAllocation.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'brwStockAllocation.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'brwStockAllocation.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'brwStockAllocation',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('brwStockAllocation_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('brwStockAllocation_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1brwStockAllocation',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="brwStockAllocation.locate(''Locator1brwStockAllocation'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'brwStockAllocation.cl(''brwStockAllocation'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('brwStockAllocation_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('brwStockAllocation_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'brwStockAllocation.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'brwStockAllocation.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'brwStockAllocation.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'brwStockAllocation.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    Access:StockAlx.clearkey(STLX:KeySTLRecordNumber)
    STLX:StlRecordNumber = stl:RecordNumber
    if access:StockAlx.fetch(STLX:KeySTLRecordNumber)
        locDateReguested = 'UNKNOWN'
        locTimeRequested = 'UNKNOWN'
    ELSE
        locDateReguested = FORMAT(STLX:RequestDate,@d06)
        locTimeRequested = FORMAT(STLX:RequestTime,@T1)
    END
    loc:field = stl:RecordNumber
    p_web._thisrow = p_web._nocolon('stl:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('brwStockAllocation:LookupField')) = stl:RecordNumber and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((stl:RecordNumber = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="brwStockAllocation.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If STOCKALL{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(STOCKALL)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If STOCKALL{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(STOCKALL)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','stl:RecordNumber',clip(loc:field),,loc:checked,,,'onclick="brwStockAllocation.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','stl:RecordNumber',clip(loc:field),,'checked',,,'onclick="brwStockAllocation.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
      IF (loc:Checked = 'checked')
          p_web.SSV('RecordSelected',1)
          p_web.SSV('stl:PartNumber',stl:PartNumber)
      END ! IF (loc:Checked = 'checked')
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::stl:JobNumber
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::stl:ShelfLocation
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::stl:Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::stl:PartNumber
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locEngineerName
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="RightJustify">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::stl:Quantity
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="RightJustify">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locStockQuantity
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
            if false
            elsif locStockAvailable = 1
              packet = clip(packet) & '<td class="CenterJustify">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            elsif locStockAvailable = 0
              packet = clip(packet) & '<td class="CenterJustify">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            else
              packet = clip(packet) & '<td class="CenterJustify">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            end
          end ! loc:eip = 0
          do value::icnStockAvailable
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locDateReguested
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locTimeRequested
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      If (p_web.GSV('RapidLocation') = 1) AND  true
          If Loc:Eip = 0
            if false
            elsif stl:Status = 'WEB'
              packet = clip(packet) & '<td class="CenterJustify">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            elsif stl:Status = 'PIK'
              packet = clip(packet) & '<td class="CenterJustify">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            elsif stl:Status = 'PRO'
              packet = clip(packet) & '<td class="CenterJustify">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            elsif stl:status = 'RET'
              packet = clip(packet) & '<td class="CenterJustify">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            elsif stl:Status = ''
              packet = clip(packet) & '<td class="CenterJustify">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            else
              packet = clip(packet) & '<td class="CenterJustify">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            end
          end ! loc:eip = 0
          do value::icnStatus
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      End ! Field Condition
      If (p_web.GSV('locStockAllocationType') = 'WEB') AND  true
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::FulFilRequest
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      End ! Field Condition
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="brwStockAllocation.omv(this);" onMouseOut="brwStockAllocation.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var brwStockAllocation=new browseTable(''brwStockAllocation'','''&clip(loc:formname)&''','''&p_web._jsok('stl:RecordNumber',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('stl:RecordNumber')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'brwStockAllocation.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'brwStockAllocation.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2brwStockAllocation')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1brwStockAllocation')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1brwStockAllocation')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2brwStockAllocation')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(STOCKALL)
  p_web._CloseFile(STOCKALX)
  p_web._CloseFile(STOCK)
  p_web._CloseFile(USERS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(STOCKALL)
  Bind(stl:Record)
  Clear(stl:Record)
  NetWebSetSessionPics(p_web,STOCKALL)
  p_web._OpenFile(STOCKALX)
  Bind(stlx:Record)
  NetWebSetSessionPics(p_web,STOCKALX)
  p_web._OpenFile(STOCK)
  Bind(sto:Record)
  NetWebSetSessionPics(p_web,STOCK)
  p_web._OpenFile(USERS)
  Bind(use:Record)
  NetWebSetSessionPics(p_web,USERS)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('stl:RecordNumber',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(STOCKALL)
  Case upper(p_web.GetValue('_EIPClm'))
  of upper('FulFilRequest')
    do Validate::FulFilRequest
  End
  p_web._CloseFile(STOCKALL)
! ----------------------------------------------------------------------------------------
value::stl:JobNumber   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('stl:JobNumber_'&stl:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(stl:JobNumber,'@s8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::stl:ShelfLocation   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('stl:ShelfLocation_'&stl:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(stl:ShelfLocation,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::stl:Description   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('stl:Description_'&stl:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(stl:Description,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::stl:PartNumber   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('stl:PartNumber_'&stl:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(stl:PartNumber,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locEngineerName   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locEngineerName_'&stl:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locEngineerName,'@s60')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::stl:Quantity   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('stl:Quantity_'&stl:RecordNumber,'RightJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(stl:Quantity,'@s8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locStockQuantity   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locStockQuantity_'&stl:RecordNumber,'RightJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locStockQuantity,'@n_8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::icnStockAvailable   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    elsif locStockAvailable = 1
      packet = clip(packet) & p_web._DivHeader('icnStockAvailable_'&stl:RecordNumber,'CenterJustify',net:crc)
      packet = clip(packet) & p_web.CreateImage('/images/accept.png','','','',)&'&#160;'
    elsif locStockAvailable = 0
      packet = clip(packet) & p_web._DivHeader('icnStockAvailable_'&stl:RecordNumber,'CenterJustify',net:crc)
      packet = clip(packet) & p_web.CreateImage('/images/reject.png','','','',)&'&#160;'
    else
      packet = clip(packet) & p_web._DivHeader('icnStockAvailable_'&stl:RecordNumber,'CenterJustify',net:crc)
      packet = clip(packet) & p_web.CreateImage('/images/reject.png','','','') & ''
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locDateReguested   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locDateReguested_'&stl:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locDateReguested,'@s10')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locTimeRequested   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locTimeRequested_'&stl:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locTimeRequested,'@s10')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::icnStatus   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If (p_web.GSV('RapidLocation') = 1)
    if false
    elsif stl:Status = 'WEB'
      packet = clip(packet) & p_web._DivHeader('icnStatus_'&stl:RecordNumber,'CenterJustify',net:crc)
      packet = clip(packet) & p_web.CreateImage('/images/bullet_purple.png','','','',)&'&#160;'
    elsif stl:Status = 'PIK'
      packet = clip(packet) & p_web._DivHeader('icnStatus_'&stl:RecordNumber,'CenterJustify',net:crc)
      packet = clip(packet) & p_web.CreateImage('/images/bullet_blue.png','','','',)&'&#160;'
    elsif stl:Status = 'PRO'
      packet = clip(packet) & p_web._DivHeader('icnStatus_'&stl:RecordNumber,'CenterJustify',net:crc)
      packet = clip(packet) & p_web.CreateImage('/images/bullet_pink.png','','','',)&'&#160;'
    elsif stl:status = 'RET'
      packet = clip(packet) & p_web._DivHeader('icnStatus_'&stl:RecordNumber,'CenterJustify',net:crc)
      packet = clip(packet) & p_web.CreateImage('/images/bullet_red.png','','','',)&'&#160;'
    elsif stl:Status = ''
      packet = clip(packet) & p_web._DivHeader('icnStatus_'&stl:RecordNumber,'CenterJustify',net:crc)
      packet = clip(packet) & p_web.CreateImage('/images/bullet_black.png','','','',)&'&#160;'
    else
      packet = clip(packet) & p_web._DivHeader('icnStatus_'&stl:RecordNumber,'CenterJustify',net:crc)
      packet = clip(packet) & p_web.CreateImage('/images/bullet_black.png ','','','') & ''
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
  End
! ----------------------------------------------------------------------------------------
Validate::FulFilRequest  Routine
  data
loc:was                    Any
loc:result                 Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  stl:RecordNumber = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  !#Bryan: Added embed to set extra key components for multi part keys
  loc:result = p_web._GetFile(STOCKALL,stl:RecordNumberKey)
  p_web.FileToSessionQueue(STOCKALL)
  do CheckForDuplicate
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::FulFilRequest   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If (p_web.GSV('locStockAllocationType') = 'WEB')
    if false
    else
      packet = clip(packet) & p_web._DivHeader('FulFilRequest_'&stl:RecordNumber,,net:crc)
      loc:disabled = ''
      packet = clip(packet) & p_web.CreateButton('button','btnFulFilRequest','FulFil Request','SmallButtonNoWidth',loc:formname,,,'window.open('''& p_web._MakeURL(clip('frmFulfilRequest?FulFilType=SINGLE')  & '&' & p_web._noColon('stl:RecordNumber')&'='& p_web.escape(stl:RecordNumber) & '' ) & ''','''&clip('_self')&''')',,loc:disabled,,,,,) & '<13,10>' !2
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
  End
OpenFiles  ROUTINE
  p_web._OpenFile(STOCKALX)
  p_web._OpenFile(STOCK)
  p_web._OpenFile(USERS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(STOCKALX)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(USERS)
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = stl:RecordNumber

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('stl:RecordNumber',stl:RecordNumber)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('stl:RecordNumber',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('stl:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('stl:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
SaveSessionVars  ROUTINE
     p_web.SSV('locEngineerName',locEngineerName) ! STRING(60)
     p_web.SSV('locStockQuantity',locStockQuantity) ! LONG
     p_web.SSV('locStockAvailable',locStockAvailable) ! BYTE
     p_web.SSV('locDateReguested',locDateReguested) ! STRING(10)
     p_web.SSV('locTimeRequested',locTimeRequested) ! STRING(10)
RestoreSessionVars  ROUTINE
     locEngineerName = p_web.GSV('locEngineerName') ! STRING(60)
     locStockQuantity = p_web.GSV('locStockQuantity') ! LONG
     locStockAvailable = p_web.GSV('locStockAvailable') ! BYTE
     locDateReguested = p_web.GSV('locDateReguested') ! STRING(10)
     locTimeRequested = p_web.GSV('locTimeRequested') ! STRING(10)
