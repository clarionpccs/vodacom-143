

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER102.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER108.INC'),ONCE        !Req'd for module callout resolution
                     END


ProofOfPurchase      PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:DOP              DATE                                  !Date Of Purchase
tmp:JobType          STRING(1)                             !
tmp:POPType          STRING(20)                            !
tmp:WarrantyRefNo    STRING(30)                            !
tmp:POP              STRING(1)                             !
locPOPTypePassword   STRING(30)                            !
FilesOpened     Long
ACCAREAS::State  USHORT
USERS::State  USHORT
MANUFACT::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('ProofOfPurchase')
  loc:formname = 'ProofOfPurchase_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('ProofOfPurchase','')
    p_web._DivHeader('ProofOfPurchase',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferProofOfPurchase',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferProofOfPurchase',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferProofOfPurchase',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_ProofOfPurchase',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferProofOfPurchase',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_ProofOfPurchase',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(ACCAREAS)
  p_web._OpenFile(USERS)
  p_web._OpenFile(MANUFACT)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(ACCAREAS)
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(MANUFACT)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  ! Reset Variables
  p_web.SSV('tmp:DOP','')
  p_web.SSV('tmp:JobType','')
  p_web.SSV('tmp:POPType','')
  p_web.SSV('tmp:WarrantyRefNo','')
  p_web.SSV('Comment:DOP','')
  p_web.SSV('Comment:POPTypePassword',kCommentPOPTypePassword)
  p_web.SSV('ReadOnly:POPType',1)
  p_web.SSV('locPOPTypePassword','')
  p_web.SetValue('ProofOfPurchase_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('tmp:DOP')
    p_web.SetPicture('tmp:DOP','@d06b')
  End
  p_web.SetSessionPicture('tmp:DOP','@d06b')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('tmp:DOP',tmp:DOP)
  p_web.SetSessionValue('tmp:POP',tmp:POP)
  p_web.SetSessionValue('locPOPTypePassword',locPOPTypePassword)
  p_web.SetSessionValue('tmp:POPType',tmp:POPType)
  p_web.SetSessionValue('tmp:WarrantyRefNo',tmp:WarrantyRefNo)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:DOP')
    tmp:DOP = p_web.dformat(clip(p_web.GetValue('tmp:DOP')),'@d06b')
    p_web.SetSessionValue('tmp:DOP',tmp:DOP)
  End
  if p_web.IfExistsValue('tmp:POP')
    tmp:POP = p_web.GetValue('tmp:POP')
    p_web.SetSessionValue('tmp:POP',tmp:POP)
  End
  if p_web.IfExistsValue('locPOPTypePassword')
    locPOPTypePassword = p_web.GetValue('locPOPTypePassword')
    p_web.SetSessionValue('locPOPTypePassword',locPOPTypePassword)
  End
  if p_web.IfExistsValue('tmp:POPType')
    tmp:POPType = p_web.GetValue('tmp:POPType')
    p_web.SetSessionValue('tmp:POPType',tmp:POPType)
  End
  if p_web.IfExistsValue('tmp:WarrantyRefNo')
    tmp:WarrantyRefNo = p_web.GetValue('tmp:WarrantyRefNo')
    p_web.SetSessionValue('tmp:WarrantyRefNo',tmp:WarrantyRefNo)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('ProofOfPurchase_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  p_web.SSV('tmp:WarrantyRefNo',p_web.GSV('jobe2:WarrantyRefNo'))
  
  p_web.SSV('tmp:DOP',p_web.GSV('job:DOP'))
  p_web.SSV('tmp:POP',p_web.GSV('job:POP'))
    p_web.SSV('tmp:POPType',p_web.GSV('jobe:POPType'))
    
    
    IF (p_web.IfExistsValue('popNextURL'))
        p_web.StoreValue('popNextURL')
    ELSE
        p_web.SSV('popNextURL','BillingConfirmation')
    END ! IF
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 tmp:DOP = p_web.RestoreValue('tmp:DOP')
 tmp:POP = p_web.RestoreValue('tmp:POP')
 locPOPTypePassword = p_web.RestoreValue('locPOPTypePassword')
 tmp:POPType = p_web.RestoreValue('tmp:POPType')
 tmp:WarrantyRefNo = p_web.RestoreValue('tmp:WarrantyRefNo')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('popNextURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('ProofOfPurchase_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('ProofOfPurchase_ChainTo')
    loc:formaction = p_web.GetSessionValue('ProofOfPurchase_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('popNextURL')
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="ProofOfPurchase" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="ProofOfPurchase" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="ProofOfPurchase" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Proof Of Purchase') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Proof Of Purchase',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_ProofOfPurchase">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_ProofOfPurchase" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_ProofOfPurchase')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Details') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_ProofOfPurchase')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_ProofOfPurchase'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:DOP')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_ProofOfPurchase')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ProofOfPurchase_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:DOP
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:DOP
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:DOP
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:POP
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:POP
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:POP
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locPOPTypePassword
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locPOPTypePassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locPOPTypePassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:POPType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:POPType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:POPType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:WarrantyRefNo
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:WarrantyRefNo
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:WarrantyRefNo
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::tmp:DOP  Routine
  p_web._DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:DOP') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Activation / D.O.P.')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:DOP  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:DOP',p_web.GetValue('NewValue'))
    tmp:DOP = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:DOP
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:DOP',p_web.dFormat(p_web.GetValue('Value'),'@d06b'))
    tmp:DOP = p_web.Dformat(p_web.GetValue('Value'),'@d06b') !
  End
  if (p_web.GSV('tmp:DOP') > today())
      p_web.SSV('comment:DOP','Invalid Date')
      p_web.SSV('tmp:DOP','')
  else !
      p_web.SSV('comment:DOP','')
  end ! if (p_web.GSV('tmp:DOP') > today())
  
  if (p_web.GSV('tmp:POP') = '' and p_web.GSV('tmp:DOP') <> '')
  
      Access:MANUFACT.Clearkey(man:Manufacturer_Key)
      man:Manufacturer    = p_web.GSV('job:Manufacturer')
      if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
          ! Found
      else ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
          ! Error
      end ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
      if (p_web.GSV('tmp:DOP') < (p_web.GSV('job:Date_Booked') - man:Warranty_Period))
          if (p_web.GSV('tmp:DOP') < (p_web.GSV('job:Date_Booked') - 730))
              p_web.SSV('tmp:POP','C')
          else ! if (p_web.GSV('tmp:DOP') < (p_web.GSV('job:Date_Booked') - 730))
              p_web.SSV('tmp:POP','S')
              ! DBH #11344 - Force Chargeable If Man/Model is One Year Warranty Only
              IF (IsOneYearWarranty(p_web.GSV('job:Manufacturer'),p_web.GSV('job:Model_Number')))
                  p_web.SSV('tmp:POP','C')
              END
          end ! if (p_web.GSV('tmp:DOP') < (p_web.GSV('job:Date_Booked') - 730))
      else ! if (p_web.GSV('tmp:DOP') < (p_web.GSV('job:Date_Booked') - man:Warranty_Period)
          p_web.SSV('tmp:POP','F')
      end ! if (p_web.GSV('tmp:DOP') < (p_web.GSV('job:Date_Booked') - man:Warranty_Period)
  end ! if (p_web.GSV('tmp:POP') = '')
  do Value::tmp:DOP
  do SendAlert
  do Comment::tmp:DOP
  do Value::tmp:POP  !1
  do Comment::tmp:POP

Value::tmp:DOP  Routine
  p_web._DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:DOP') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:DOP
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:DOP')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:DOP'',''proofofpurchase_tmp:dop_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:DOP')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:DOP',p_web.GetSessionValue('tmp:DOP'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06b',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ProofOfPurchase_' & p_web._nocolon('tmp:DOP') & '_value')

Comment::tmp:DOP  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:DOP'))
  p_web._DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:DOP') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ProofOfPurchase_' & p_web._nocolon('tmp:DOP') & '_comment')

Prompt::tmp:POP  Routine
  p_web._DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:POP') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Job Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:POP  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:POP',p_web.GetValue('NewValue'))
    tmp:POP = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:POP
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:POP',p_web.GetValue('Value'))
    tmp:POP = p_web.GetValue('Value')
  End
  do Value::tmp:POP
  do SendAlert

Value::tmp:POP  Routine
  p_web._DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:POP') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- tmp:POP
  loc:fieldclass = Choose(sub(' noButton',1,1) = ' ',clip('FormEntry') & ' noButton',' noButton')
  If lower(loc:invalid) = lower('tmp:POP')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:POP') = 'F'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:POP'',''proofofpurchase_tmp:pop_value'',1,'''&clip('F')&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:POP',clip('F'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:POP_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('First Year Warranty') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:POP') = 'S'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:POP'',''proofofpurchase_tmp:pop_value'',1,'''&clip('S')&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:POP',clip('S'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:POP_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Second Year Warranty') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:POP') = 'O'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:POP'',''proofofpurchase_tmp:pop_value'',1,'''&clip('O')&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:POP',clip('O'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:POP_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Out Of Box Failure') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:POP') = 'C'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:POP'',''proofofpurchase_tmp:pop_value'',1,'''&clip('C')&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:POP',clip('C'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:POP_4') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Chargeable') & '<13,10>'
    packet = clip(packet) & p_web.br
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ProofOfPurchase_' & p_web._nocolon('tmp:POP') & '_value')

Comment::tmp:POP  Routine
    loc:comment = ''
  p_web._DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:POP') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ProofOfPurchase_' & p_web._nocolon('tmp:POP') & '_comment')

Prompt::locPOPTypePassword  Routine
  p_web._DivHeader('ProofOfPurchase_' & p_web._nocolon('locPOPTypePassword') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Enter Password')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locPOPTypePassword  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locPOPTypePassword',p_web.GetValue('NewValue'))
    locPOPTypePassword = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locPOPTypePassword
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locPOPTypePassword',p_web.GetValue('Value'))
    locPOPTypePassword = p_web.GetValue('Value')
  End
    locPOPTypePassword = Upper(locPOPTypePassword)
    p_web.SetSessionValue('locPOPTypePassword',locPOPTypePassword)
  p_web.SSV('ReadOnly:POPType',1)
  p_web.SSV('Comment:POPTypePassword',kCommentPOPTypePassword)
  Access:USERS.ClearKey(use:password_key)
  use:Password = p_web.GSV('locPOPTypePassword')
  IF (Access:USERS.TryFetch(use:password_key) = Level:Benign)
      Access:ACCAREAS.ClearKey(acc:Access_level_key)
      acc:User_Level = use:User_Level
      acc:Access_Area = 'AMEND POP TYPE'
      IF (Access:ACCAREAS.TryFetch(acc:Access_level_key) = Level:Benign)
          p_web.SSV('ReadOnly:POPType',0)
  
      ELSE
          p_web.SSV('Comment:POPTypePassword','User Does Not Have Access')
      END
  ELSE
      p_web.SSV('Comment:POPTypePassword','Invalid Password')
  END
  do Value::locPOPTypePassword
  do SendAlert
  do Value::tmp:POPType  !1
  do Comment::locPOPTypePassword

Value::locPOPTypePassword  Routine
  p_web._DivHeader('ProofOfPurchase_' & p_web._nocolon('locPOPTypePassword') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locPOPTypePassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('locPOPTypePassword')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locPOPTypePassword'',''proofofpurchase_locpoptypepassword_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locPOPTypePassword')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','locPOPTypePassword',p_web.GetSessionValueFormat('locPOPTypePassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,'Enter Password And Press [TAB]') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ProofOfPurchase_' & p_web._nocolon('locPOPTypePassword') & '_value')

Comment::locPOPTypePassword  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:POPTypePassword'))
  p_web._DivHeader('ProofOfPurchase_' & p_web._nocolon('locPOPTypePassword') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ProofOfPurchase_' & p_web._nocolon('locPOPTypePassword') & '_comment')

Prompt::tmp:POPType  Routine
  p_web._DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:POPType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('POP Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:POPType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:POPType',p_web.GetValue('NewValue'))
    tmp:POPType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:POPType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:POPType',p_web.GetValue('Value'))
    tmp:POPType = p_web.GetValue('Value')
  End
  do Value::tmp:POPType
  do SendAlert

Value::tmp:POPType  Routine
  p_web._DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:POPType') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:POPType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:POPType'',''proofofpurchase_tmp:poptype_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('ReadOnly:POPType'),'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:POPType',loc:fieldclass,loc:readonly,,,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:POPType') = 0
    p_web.SetSessionValue('tmp:POPType','')
  end
    packet = clip(packet) & p_web.CreateOption('------------------','',choose('' = p_web.getsessionvalue('tmp:POPType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('POP','POP',choose('POP' = p_web.getsessionvalue('tmp:POPType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('BASTION','BASTION',choose('BASTION' = p_web.getsessionvalue('tmp:POPType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('NONE','NONE',choose('NONE' = p_web.getsessionvalue('tmp:POPType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ProofOfPurchase_' & p_web._nocolon('tmp:POPType') & '_value')

Comment::tmp:POPType  Routine
    loc:comment = ''
  p_web._DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:POPType') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:WarrantyRefNo  Routine
  p_web._DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:WarrantyRefNo') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Warranty Ref No')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:WarrantyRefNo  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:WarrantyRefNo',p_web.GetValue('NewValue'))
    tmp:WarrantyRefNo = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:WarrantyRefNo
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:WarrantyRefNo',p_web.GetValue('Value'))
    tmp:WarrantyRefNo = p_web.GetValue('Value')
  End
  do Value::tmp:WarrantyRefNo
  do SendAlert

Value::tmp:WarrantyRefNo  Routine
  p_web._DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:WarrantyRefNo') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:WarrantyRefNo
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:WarrantyRefNo')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:WarrantyRefNo'',''proofofpurchase_tmp:warrantyrefno_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:WarrantyRefNo',p_web.GetSessionValueFormat('tmp:WarrantyRefNo'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ProofOfPurchase_' & p_web._nocolon('tmp:WarrantyRefNo') & '_value')

Comment::tmp:WarrantyRefNo  Routine
      loc:comment = ''
  p_web._DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:WarrantyRefNo') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('ProofOfPurchase_tmp:DOP_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:DOP
      else
        do Value::tmp:DOP
      end
  of lower('ProofOfPurchase_tmp:POP_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:POP
      else
        do Value::tmp:POP
      end
  of lower('ProofOfPurchase_locPOPTypePassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locPOPTypePassword
      else
        do Value::locPOPTypePassword
      end
  of lower('ProofOfPurchase_tmp:POPType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:POPType
      else
        do Value::tmp:POPType
      end
  of lower('ProofOfPurchase_tmp:WarrantyRefNo_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:WarrantyRefNo
      else
        do Value::tmp:WarrantyRefNo
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('ProofOfPurchase_form:ready_',1)
  p_web.SetSessionValue('ProofOfPurchase_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_ProofOfPurchase',0)

PreCopy  Routine
  p_web.SetValue('ProofOfPurchase_form:ready_',1)
  p_web.SetSessionValue('ProofOfPurchase_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_ProofOfPurchase',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('ProofOfPurchase_form:ready_',1)
  p_web.SetSessionValue('ProofOfPurchase_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('ProofOfPurchase:Primed',0)

PreDelete       Routine
  p_web.SetValue('ProofOfPurchase_form:ready_',1)
  p_web.SetSessionValue('ProofOfPurchase_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('ProofOfPurchase:Primed',0)
  p_web.setsessionvalue('showtab_ProofOfPurchase',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('ProofOfPurchase_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  ! Validate
  if (p_web.GSV('tmp:POP') = '')
      loc:alert = 'You have not selected an option'
      loc:invalid = 'tmp:POP'
      exit
  end !if (p_web.GSV('tmp:POP') = '')
  
  case (p_web.GSV('tmp:POP'))
  of 'F' orof 'S' orof 'O'
      if (p_web.GSV('tmp:DOP') = '')
          loc:alert = 'Date Of Purchase Required'
          loc:invalid = 'tmp:DOP'
          exit
      end ! if (p_web.GSV('tmp:DOP') = '')
  end ! case (p_web.GSV('tmp:POP'))
  
  
  Access:MANUFACT.Clearkey(man:Manufacturer_Key)
  man:Manufacturer    = p_web.GSV('job:Manufacturer')
  if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
      ! Found
  else ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
      ! Error
  end ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
  
  if (p_web.GSV('tmp:POP') <> 'C' and man:validateDateCode)
      if (p_web.GSV('tmp:DOP') < (job:date_Booked - man:warranty_period))
          if (p_web.GSV('tmp:DOP') < (job:date_booked - 730))
              ! Out Of 2nd Year
          else ! if (p_web.GSV('tmp:DOP') < (job:date_booked - 730))
              if (p_web.GSV('tmp:POP') <> 'O')
                  p_web.SSV('tmp:POP','S')
              end ! if (p_web.GSV('tmp:POP') <> 'O')
          end ! if (p_web.GSV('tmp:DOP') < (job:date_booked - 730))
      end ! if (p_web.GSV('tmp:DOP') < (job:date_Booked - man:warranty_period))
  end ! if (p_web.GSV('tmp:POP') <> 'C' and man:validateDateCode)
  
  p_web.SSV('job:POP',p_web.GSV('tmp:POP'))
  p_web.SSV('job:DOP',p_web.GSV('tmp:DOP'))
  
  if (p_web.GSV('job:DOP') > 0)
      if (sub(p_web.GSV('job:Current_Status'),1,3) = '130')
          if (p_web.GSV('job:Engineer') = '')
              p_web.SSV('GetStatus:StatusNumber',305)
              p_web.SSV('GetStatus:Type','JOB')
          else ! if (p_web.GSV('job:Engineer') = ''
              p_web.SSV('GetStatus:StatusNumber',sub(p_web.GSV('job:previousStatus'),1,3))
              p_web.SSV('GetStatus:Type','JOB')
          end ! if (p_web.GSV('job:Engineer') = ''
          getStatus(p_web.GSV('GetStatus:StatusNumber'),0,'JOB',p_web)
      end ! if (sub(p_web.GSV('job:Current_Status'),1,3) = '130')
  
      case p_web.GSV('job:POP')
      of 'F'
          p_web.SSV('job:Warranty_job','YES')
          p_web.SSV('job:Warranty_Charge_Type','WARRANTY (MFTR)')
      of 'S'
          p_web.SSV('job:Warranty_job','YES')
          p_web.SSV('job:Warranty_Charge_Type','WARRANTY (2ND YR)')
      of 'O'
          p_web.SSV('job:Warranty_job','YES')
          p_web.SSV('job:Warranty_Charge_Type','WARRANTY (OBF)')
      of 'C'
          p_web.SSV('job:Warranty_job','NO')
          p_web.SSV('job:Chargeable_job','YES')
          p_web.SSV('job:Warranty_Charge_Type','')
          p_web.SSV('job:Charge_Type','NON-WARRANTY')
      end ! case p_web.GSV('job:POP')
  end ! if (p_web.GSV('job:DOP') > 0)
  
  p_web.SSV('jobe:POPType',p_web.GSV('tmp:POPType')) ! #11912 Never saved this field. (Bryan: 14/02/2011)
  
  
  
  p_web.DeleteSessionValue('ProofOfPurchase_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
          locPOPTypePassword = Upper(locPOPTypePassword)
          p_web.SetSessionValue('locPOPTypePassword',locPOPTypePassword)
        If loc:Invalid <> '' then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('ProofOfPurchase:Primed',0)
  p_web.StoreValue('tmp:DOP')
  p_web.StoreValue('tmp:POP')
  p_web.StoreValue('locPOPTypePassword')
  p_web.StoreValue('tmp:POPType')
  p_web.StoreValue('tmp:WarrantyRefNo')
