

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER140.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER139.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Allow48Hour          PROCEDURE  (fIMEINumber,fModelNumber,fAccountNumber) ! Declare Procedure
ESNMODAL::State  USHORT
FilesOpened     BYTE(0)

  CODE
    do openFiles
    do saveFiles

    Return# = 0

    Access:ESNMODEL.ClearKey(esn:ESN_Key)
    esn:ESN          = Sub(fIMEINumber,1,6)
    esn:Model_Number = fModelNumber
    If Access:ESNMODEL.TryFetch(esn:ESN_Key) = Level:Benign
        !Found
        If esn:Include48Hour
            Return# = 1
        End !If esn:Include48Hour
    Else !If Access:ESNMODEL.TryFetch(esn:ESN_Key) = Level:Benign
        !Error
    End !If Access:ESNMODEL.TryFetch(esn:ESN_Key) = Level:Benign

    Access:ESNMODEL.ClearKey(esn:ESN_Key)
    esn:ESN          = Sub(fIMEINumber,1,8)
    esn:Model_Number = fModelNumber
    If Access:ESNMODEL.TryFetch(esn:ESN_Key) = Level:Benign
        !Found
        If esn:Include48Hour
            Return# = 1
        End !If esn:Include48Hour
    Else !If Access:ESNMODEL.TryFetch(esn:ESN_Key) = Level:Benign
        !Error
    End !If Access:ESNMODEL.TryFetch(esn:ESN_Key) = Level:Benign

    ! Inserting (DBH 21/02/2008) # 9717 - If Man/Model is setup for replenishment, then don't use 48 Hour
    If UseReplenishmentProcess(fModelNumber,fAccountNumber) = 1
        Return# = 0
    End ! If UseReplenishmentProcess(f:ModelNumber,fAccountNumber) = 1
    ! End (DBH 21/02/2008) #9717

    do restoreFiles
    do closeFiles

    Return Return#

SaveFiles  ROUTINE
  ESNMODAL::State = Access:ESNMODAL.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF ESNMODAL::State <> 0
    Access:ESNMODAL.RestoreFile(ESNMODAL::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:ESNMODAL.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:ESNMODAL.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:ESNMODAL.Close
     FilesOpened = False
  END
