

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER376.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER344.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER373.INC'),ONCE        !Req'd for module callout resolution
                     END


FormChangeStatus     PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
locStatus            STRING(30)                            !
locReason            STRING(255)                           !
locChooseStatusType  LONG                                  !
locCurrentStatus     STRING(30)                            !
Ans                  LONG                                  !
FilesOpened     Long
WEBJOB::State  USHORT
JOBS::State  USHORT
STATUS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormChangeStatus')
  loc:formname = 'FormChangeStatus_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormChangeStatus','')
    p_web._DivHeader('FormChangeStatus',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormChangeStatus',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormChangeStatus',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormChangeStatus',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormChangeStatus',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormChangeStatus',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormChangeStatus',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
DeleteSessionValues ROUTINE ! Auto generated by Bryan's Template
    ! Local Variables
    p_web.DeleteSessionValue('locStatus')
    p_web.DeleteSessionValue('locReason')
    p_web.DeleteSessionValue('locChooseStatusType')
    p_web.DeleteSessionValue('locCurrentStatus')
    ! p_web.DeleteSessionValue('Ans') ! Excluded

    ! Other Variables
OpenFiles  ROUTINE
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(STATUS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(STATUS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormChangeStatus_form:inited_',1)
  do RestoreMem

CancelForm  Routine
    DO DeleteSessionValues  

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'locStatus'
    p_web.setsessionvalue('showtab_FormChangeStatus',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(STATUS)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locStatus')
  Of 'locStatus'
    p_web.setsessionvalue('showtab_FormChangeStatus',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(STATUS)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locStatus')
  Of 'locStatus'
    p_web.setsessionvalue('showtab_FormChangeStatus',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(STATUS)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locStatus')
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locChooseStatusType',locChooseStatusType)
  p_web.SetSessionValue('locCurrentStatus',locCurrentStatus)
  p_web.SetSessionValue('locStatus',locStatus)
  p_web.SetSessionValue('locStatus',locStatus)
  p_web.SetSessionValue('locStatus',locStatus)
  p_web.SetSessionValue('locReason',locReason)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locChooseStatusType')
    locChooseStatusType = p_web.GetValue('locChooseStatusType')
    p_web.SetSessionValue('locChooseStatusType',locChooseStatusType)
  End
  if p_web.IfExistsValue('locCurrentStatus')
    locCurrentStatus = p_web.GetValue('locCurrentStatus')
    p_web.SetSessionValue('locCurrentStatus',locCurrentStatus)
  End
  if p_web.IfExistsValue('locStatus')
    locStatus = p_web.GetValue('locStatus')
    p_web.SetSessionValue('locStatus',locStatus)
  End
  if p_web.IfExistsValue('locStatus')
    locStatus = p_web.GetValue('locStatus')
    p_web.SetSessionValue('locStatus',locStatus)
  End
  if p_web.IfExistsValue('locStatus')
    locStatus = p_web.GetValue('locStatus')
    p_web.SetSessionValue('locStatus',locStatus)
  End
  if p_web.IfExistsValue('locReason')
    locReason = p_web.GetValue('locReason')
    p_web.SetSessionValue('locReason',locReason)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormChangeStatus_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    IF (p_web.IfExistsValue('ChangeStatusReturnURL'))
        p_web.SToreValue('ChangeStatusReturnURL')
    END ! IF
    IF (p_web.IfExistsValue('clearvars'))
        ! Clear Variables
        p_web.SSV('locStatus','')
        p_web.SSV('locReason','')
        p_web.SSV('locChooseStatusType','')
        p_web.SSV('locCurrentStatus','')
    END ! IF
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locChooseStatusType = p_web.RestoreValue('locChooseStatusType')
 locCurrentStatus = p_web.RestoreValue('locCurrentStatus')
 locStatus = p_web.RestoreValue('locStatus')
 locStatus = p_web.RestoreValue('locStatus')
 locStatus = p_web.RestoreValue('locStatus')
 locReason = p_web.RestoreValue('locReason')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('ChangeStatusReturnURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormChangeStatus_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormChangeStatus_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormChangeStatus_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('ChangeStatusReturnURL')
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormChangeStatus" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormChangeStatus" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormChangeStatus" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Change Status') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Change Status',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormChangeStatus">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormChangeStatus" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormChangeStatus')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Select Status Type') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormChangeStatus')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormChangeStatus'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='STATUS'
          If Not (p_web.GSV('locChooseStatusType') <> 2)
            p_web.SetValue('SelectField',clip(loc:formname) & '.locStatus')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='STATUS'
          If Not (p_web.GSV('locChooseStatusType') <> 3)
            p_web.SetValue('SelectField',clip(loc:formname) & '.locStatus')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='STATUS'
            p_web.SetValue('SelectField',clip(loc:formname) & '.locReason')
    End
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormChangeStatus')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Select Status Type') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormChangeStatus_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Select Status Type')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Select Status Type')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Select Status Type')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Select Status Type')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locChooseStatusType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&' colspan="6">'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locChooseStatusType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::hiddenField
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locCurrentStatus
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locCurrentStatus
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locStatus_J
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locStatus_J
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locStatus_E
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locStatus_E
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locStatus_L
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locStatus_L
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locReason
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locChooseStatusType  Routine
  p_web._DivHeader('FormChangeStatus_' & p_web._nocolon('locChooseStatusType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Status Type To Change')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locChooseStatusType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locChooseStatusType',p_web.GetValue('NewValue'))
    locChooseStatusType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locChooseStatusType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locChooseStatusType',p_web.GetValue('Value'))
    locChooseStatusType = p_web.GetValue('Value')
  End
    p_web.SSV('locStatus','')
    CASE p_web.GSV('locChooseStatusType')
    OF 1 ! Job
        p_web.SSV('locCurrentStatus',p_web.GSV('job:Current_Status'))
    OF 2 ! Exc
        p_web.SSV('locCurrentStatus',p_web.GSV('job:Exchange_Status'))
    OF 3 ! Loa
        p_web.SSV('locCurrentStatus',p_web.GSV('job:Loan_Status'))
    END ! CASE
  do Value::locChooseStatusType
  do SendAlert
  do Prompt::locStatus_E
  do Value::locStatus_E  !1
  do Prompt::locStatus_J
  do Value::locStatus_J  !1
  do Prompt::locStatus_L
  do Value::locStatus_L  !1
  do Prompt::locCurrentStatus
  do Value::locCurrentStatus  !1

Value::locChooseStatusType  Routine
  p_web._DivHeader('FormChangeStatus_' & p_web._nocolon('locChooseStatusType') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- locChooseStatusType
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locChooseStatusType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locChooseStatusType') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locChooseStatusType'',''formchangestatus_locchoosestatustype_value'',1,'''&clip(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locChooseStatusType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locChooseStatusType',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locChooseStatusType_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Job Status') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locChooseStatusType') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locChooseStatusType'',''formchangestatus_locchoosestatustype_value'',1,'''&clip(2)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locChooseStatusType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locChooseStatusType',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locChooseStatusType_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Exchange Status') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locChooseStatusType') = 3
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locChooseStatusType'',''formchangestatus_locchoosestatustype_value'',1,'''&clip(3)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locChooseStatusType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locChooseStatusType',clip(3),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locChooseStatusType_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Loan Status') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChangeStatus_' & p_web._nocolon('locChooseStatusType') & '_value')


Validate::hiddenField  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('hiddenField',p_web.GetValue('NewValue'))
    do Value::hiddenField
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::hiddenField  Routine
  p_web._DivHeader('FormChangeStatus_' & p_web._nocolon('hiddenField') & '_value','adiv')
  loc:extra = ''
  ! --- HIDDEN --- 
    packet = clip(packet) & p_web.CreateInput('hidden','',p_web.GetSessionValue('')) & '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locCurrentStatus  Routine
  p_web._DivHeader('FormChangeStatus_' & p_web._nocolon('locCurrentStatus') & '_prompt',Choose(p_web.GSV('locChooseStatusType') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Current Status:')
  If p_web.GSV('locChooseStatusType') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChangeStatus_' & p_web._nocolon('locCurrentStatus') & '_prompt')

Validate::locCurrentStatus  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locCurrentStatus',p_web.GetValue('NewValue'))
    locCurrentStatus = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locCurrentStatus
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locCurrentStatus',p_web.GetValue('Value'))
    locCurrentStatus = p_web.GetValue('Value')
  End

Value::locCurrentStatus  Routine
  p_web._DivHeader('FormChangeStatus_' & p_web._nocolon('locCurrentStatus') & '_value',Choose(p_web.GSV('locChooseStatusType') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locChooseStatusType') = 0)
  ! --- STRING --- locCurrentStatus
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locCurrentStatus')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locCurrentStatus',p_web.GetSessionValueFormat('locCurrentStatus'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChangeStatus_' & p_web._nocolon('locCurrentStatus') & '_value')


Prompt::locStatus_J  Routine
  p_web._DivHeader('FormChangeStatus_' & p_web._nocolon('locStatus_J') & '_prompt',Choose(p_web.GSV('locChooseStatusType') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('New Status')
  If p_web.GSV('locChooseStatusType') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChangeStatus_' & p_web._nocolon('locStatus_J') & '_prompt')

Validate::locStatus_J  Routine
    If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('Value')
      p_web.GetDescription(STATUS,sts:Status_Key,,sts:Status,sts:Status,p_web.GetValue('Value'))
      loc:lookupdone = 1
    Else
      p_web.GetDescription(STATUS,sts:Status_Key,,sts:Status,sts:Status,p_web.GetSessionValue('locStatus'))
    End
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locStatus_J',p_web.GetValue('NewValue'))
    locStatus = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locStatus_J
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locStatus',p_web.GetValue('Value'))
    locStatus = p_web.GetValue('Value')
  End
  If locStatus = ''
    loc:Invalid = 'locStatus'
    loc:alert = p_web.translate('New Status') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locStatus = Upper(locStatus)
    p_web.SetSessionValue('locStatus',locStatus)
  p_Web.SetValue('lookupfield','locStatus_J')
  do AfterLookup
  do Value::locStatus_J
  do SendAlert

Value::locStatus_J  Routine
  p_web._DivHeader('FormChangeStatus_' & p_web._nocolon('locStatus_J') & '_value',Choose(p_web.GSV('locChooseStatusType') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locChooseStatusType') <> 1)
  ! --- STRING --- locStatus
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locStatus')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locStatus = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locStatus_J'',''formchangestatus_locstatus_j_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locStatus')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','locStatus',p_web.GetSessionValueFormat('locStatus'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectStatus?StatusType=JOB')&'?LookupField=locStatus&Tab=0&ForeignField=sts:Status&_sort=sts:Status&Refresh=sort&LookupFrom=FormChangeStatus&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChangeStatus_' & p_web._nocolon('locStatus_J') & '_value')


Prompt::locStatus_E  Routine
  p_web._DivHeader('FormChangeStatus_' & p_web._nocolon('locStatus_E') & '_prompt',Choose(p_web.GSV('locChooseStatusType') <> 2,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('New Status')
  If p_web.GSV('locChooseStatusType') <> 2
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChangeStatus_' & p_web._nocolon('locStatus_E') & '_prompt')

Validate::locStatus_E  Routine
    If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('Value')
      p_web.GetDescription(STATUS,sts:Status_Key,,sts:Status,sts:Status,p_web.GetValue('Value'))
      loc:lookupdone = 1
    Else
      p_web.GetDescription(STATUS,sts:Status_Key,,sts:Status,sts:Status,p_web.GetSessionValue('locStatus'))
    End
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locStatus_E',p_web.GetValue('NewValue'))
    locStatus = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locStatus_E
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locStatus',p_web.GetValue('Value'))
    locStatus = p_web.GetValue('Value')
  End
  If locStatus = ''
    loc:Invalid = 'locStatus'
    loc:alert = p_web.translate('New Status') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locStatus = Upper(locStatus)
    p_web.SetSessionValue('locStatus',locStatus)
  p_Web.SetValue('lookupfield','locStatus_E')
  do AfterLookup
  do Value::locStatus_E
  do SendAlert

Value::locStatus_E  Routine
  p_web._DivHeader('FormChangeStatus_' & p_web._nocolon('locStatus_E') & '_value',Choose(p_web.GSV('locChooseStatusType') <> 2,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locChooseStatusType') <> 2)
  ! --- STRING --- locStatus
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locStatus')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locStatus = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locStatus_E'',''formchangestatus_locstatus_e_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locStatus')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','locStatus',p_web.GetSessionValueFormat('locStatus'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectStatus?StatusType=EXC')&'?LookupField=locStatus&Tab=0&ForeignField=sts:Status&_sort=sts:Status&Refresh=sort&LookupFrom=FormChangeStatus&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChangeStatus_' & p_web._nocolon('locStatus_E') & '_value')


Prompt::locStatus_L  Routine
  p_web._DivHeader('FormChangeStatus_' & p_web._nocolon('locStatus_L') & '_prompt',Choose(p_web.GSV('locChooseStatusType') <> 3,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('New Status')
  If p_web.GSV('locChooseStatusType') <> 3
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChangeStatus_' & p_web._nocolon('locStatus_L') & '_prompt')

Validate::locStatus_L  Routine
    If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('Value')
      p_web.GetDescription(STATUS,sts:Status_Key,,sts:Status,sts:Status,p_web.GetValue('Value'))
      loc:lookupdone = 1
    Else
      p_web.GetDescription(STATUS,sts:Status_Key,,sts:Status,sts:Status,p_web.GetSessionValue('locStatus'))
    End
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locStatus_L',p_web.GetValue('NewValue'))
    locStatus = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locStatus_L
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locStatus',p_web.GetValue('Value'))
    locStatus = p_web.GetValue('Value')
  End
  If locStatus = ''
    loc:Invalid = 'locStatus'
    loc:alert = p_web.translate('New Status') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locStatus = Upper(locStatus)
    p_web.SetSessionValue('locStatus',locStatus)
  p_Web.SetValue('lookupfield','locStatus_L')
  do AfterLookup
  do Value::locStatus_L
  do SendAlert

Value::locStatus_L  Routine
  p_web._DivHeader('FormChangeStatus_' & p_web._nocolon('locStatus_L') & '_value',Choose(p_web.GSV('locChooseStatusType') <> 3,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locChooseStatusType') <> 3)
  ! --- STRING --- locStatus
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locStatus')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locStatus = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locStatus_L'',''formchangestatus_locstatus_l_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locStatus')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','locStatus',p_web.GetSessionValueFormat('locStatus'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectStatus?StatusType=LOA')&'?LookupField=locStatus&Tab=0&ForeignField=sts:Status&_sort=sts:Status&Refresh=sort&LookupFrom=FormChangeStatus&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChangeStatus_' & p_web._nocolon('locStatus_L') & '_value')


Prompt::locReason  Routine
  p_web._DivHeader('FormChangeStatus_' & p_web._nocolon('locReason') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Reason For Status Change')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locReason  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locReason',p_web.GetValue('NewValue'))
    locReason = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locReason
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locReason',p_web.GetValue('Value'))
    locReason = p_web.GetValue('Value')
  End
  If locReason = ''
    loc:Invalid = 'locReason'
    loc:alert = p_web.translate('Reason For Status Change') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locReason = Upper(locReason)
    p_web.SetSessionValue('locReason',locReason)
  do Value::locReason
  do SendAlert

Value::locReason  Routine
  p_web._DivHeader('FormChangeStatus_' & p_web._nocolon('locReason') & '_value','adiv')
  loc:extra = ''
  ! --- TEXT --- locReason
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locReason')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locReason = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locReason'',''formchangestatus_locreason_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('locReason',p_web.GetSessionValue('locReason'),8,40,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(locReason),,Net:Web:Control) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChangeStatus_' & p_web._nocolon('locReason') & '_value')


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormChangeStatus_locChooseStatusType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locChooseStatusType
      else
        do Value::locChooseStatusType
      end
  of lower('FormChangeStatus_locStatus_J_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locStatus_J
      else
        do Value::locStatus_J
      end
  of lower('FormChangeStatus_locStatus_E_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locStatus_E
      else
        do Value::locStatus_E
      end
  of lower('FormChangeStatus_locStatus_L_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locStatus_L
      else
        do Value::locStatus_L
      end
  of lower('FormChangeStatus_locReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locReason
      else
        do Value::locReason
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormChangeStatus_form:ready_',1)
  p_web.SetSessionValue('FormChangeStatus_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormChangeStatus',0)

PreCopy  Routine
  p_web.SetValue('FormChangeStatus_form:ready_',1)
  p_web.SetSessionValue('FormChangeStatus_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormChangeStatus',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormChangeStatus_form:ready_',1)
  p_web.SetSessionValue('FormChangeStatus_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormChangeStatus:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormChangeStatus_form:ready_',1)
  p_web.SetSessionValue('FormChangeStatus_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormChangeStatus:Primed',0)
  p_web.setsessionvalue('showtab_FormChangeStatus',0)

LoadRelatedRecords  Routine
    p_web.GetDescription(STATUS,sts:Status_Key,,sts:Status,,p_web.GetSessionValue('locStatus'))
    p_web.FileToSessionQueue(STATUS)
    p_web.GetDescription(STATUS,sts:Status_Key,,sts:Status,,p_web.GetSessionValue('locStatus'))
    p_web.FileToSessionQueue(STATUS)
    p_web.GetDescription(STATUS,sts:Status_Key,,sts:Status,,p_web.GetSessionValue('locStatus'))
    p_web.FileToSessionQueue(STATUS)
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
    CASE p_web.GSV('locChooseStatusType')
    OF 1 ! JOB
        IF (p_web.GSV('job:Date_Completed') <> '')
            IF (DoesUserHaveAccess(p_web,'CHANGE COMPLETED STATUS') = FALSE)
                loc:Alert = 'You do not have access to change the status'
                loc:Invalid = 'locChooseStatusType'
                EXIT
            END ! IF
        END ! IF
        IF (p_web.GSV('job:Date_Despatched') <> '')
            IF (DoesUserHaveAccess(p_web,'CHANGE DESPATCHED STATUS') = FALSE)
                loc:Alert = 'You do not have access to change the status'
                loc:Invalid = 'locChooseStatusType'
                EXIT
            END ! IF
        END ! IF 
        IF (SUB(p_web.GSV('locStatus'),1,3) = '799')
            loc:alert = 'Error! You cannot cancel the job from here'
            loc:invalid = 'locChooseStatusType'
            EXIT
        END!  IF
        IF (p_web.GSV('locStatus') <> p_web.GSV('job:Current_Status'))
            GetStatus(SUB(p_web.GSV('locStatus'),0,3),1,'JOB',p_web)
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = p_web.GSV('job:Ref_Number')
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                p_web.SessionQueueToFile(JOBS)
                IF (Access:JOBS.TryUpdate() = Level:Benign)
                    Access:WEBJOB.ClearKey(wob:RefNumberKey)
                    wob:RefNumber = job:Ref_Number
                    IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                        p_web.SessionQueueToFile(WEBJOB)
                        Access:WEBJOB.TryUpdate()
                    END ! IF
                    AddToAudit(p_web,job:Ref_Number,'JOB','MANUAL STATUS CHANGE TO: ' & p_web.GSV('locStatus'),'PREVIOUS STATUS: ' & p_web.GSV('locCurrentStatus') & '<13,10>NEW STATUS: ' & clip(job:Current_Status) &|
                        '<13,10,13,10>REASON:<13,10>' & p_web.GSV('locReason'))
                END ! IF
            END ! IF
        END ! IF
    OF 2 ! EXC
        IF (p_web.GSV('locStatus') <> p_web.GSV('job:Exchange_Status'))
            GetStatus(SUB(p_web.GSV('locStatus'),1,3),1,'EXC',p_web)
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = p_web.GSV('job:Ref_Number')
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                p_web.SessionQueueToFile(JOBS)
                IF (Access:JOBS.TryUpdate() = Level:Benign)
                    Access:WEBJOB.ClearKey(wob:RefNumberKey)
                    wob:RefNumber = job:Ref_Number
                    IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                        p_web.SessionQueueToFile(WEBJOB)
                        Access:WEBJOB.TryUpdate()
                    END ! IF
                    AddToAudit(p_web,job:Ref_Number,'EXC','MANUAL EXCHANGE STATUS CHANGE TO: ' & p_web.GSV('locStatus'),'PREVIOUS STATUS: ' & p_web.GSV('locCurrentStatus') & '<13,10>NEW STATUS: ' & clip(job:Exchange_Status) &|
                        '<13,10,13,10>REASON:<13,10>' & p_web.GSV('locReason'))
                END ! IF
            END ! IF            
        END ! IF
    OF 3 ! LOA
        IF (p_web.GSV('locStatus') <> p_web.GSV('job:Loan_Status'))
            GetStatus(SUB(p_web.GSV('locStatus'),1,3),1,'LOA',p_web)
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = p_web.GSV('job:Ref_Number')
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                p_web.SessionQueueToFile(JOBS)
                IF (Access:JOBS.TryUpdate() = Level:Benign)
                    Access:WEBJOB.ClearKey(wob:RefNumberKey)
                    wob:RefNumber = job:Ref_Number
                    IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                        p_web.SessionQueueToFile(WEBJOB)
                        Access:WEBJOB.TryUpdate()
                    END ! IF
                    AddToAudit(p_web,job:Ref_Number,'LOA','MANUAL LOAN STATUS CHANGE TO: ' & p_web.GSV('locStatus'),'PREVIOUS STATUS: ' & p_web.GSV('locCurrentStatus') & '<13,10>NEW STATUS: ' & clip(job:Loan_Status) &|
                        '<13,10,13,10>REASON:<13,10>' & p_web.GSV('locReason'))
                END ! IF
            END ! IF            
        END ! IF
    END ! CASE
        
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormChangeStatus_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormChangeStatus_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
      If not (p_web.GSV('locChooseStatusType') = 0)
          locCurrentStatus = Upper(locCurrentStatus)
          p_web.SetSessionValue('locCurrentStatus',locCurrentStatus)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('locChooseStatusType') <> 1)
        If locStatus = ''
          loc:Invalid = 'locStatus'
          loc:alert = p_web.translate('New Status') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locStatus = Upper(locStatus)
          p_web.SetSessionValue('locStatus',locStatus)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('locChooseStatusType') <> 2)
        If locStatus = ''
          loc:Invalid = 'locStatus'
          loc:alert = p_web.translate('New Status') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locStatus = Upper(locStatus)
          p_web.SetSessionValue('locStatus',locStatus)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('locChooseStatusType') <> 3)
        If locStatus = ''
          loc:Invalid = 'locStatus'
          loc:alert = p_web.translate('New Status') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locStatus = Upper(locStatus)
          p_web.SetSessionValue('locStatus',locStatus)
        If loc:Invalid <> '' then exit.
      End
        If locReason = ''
          loc:Invalid = 'locReason'
          loc:alert = p_web.translate('Reason For Status Change') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locReason = Upper(locReason)
          p_web.SetSessionValue('locReason',locReason)
        If loc:Invalid <> '' then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormChangeStatus:Primed',0)
  p_web.StoreValue('locChooseStatusType')
  p_web.StoreValue('')
  p_web.StoreValue('locCurrentStatus')
  p_web.StoreValue('locStatus')
  p_web.StoreValue('locStatus')
  p_web.StoreValue('locStatus')
  p_web.StoreValue('locReason')
