

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER316.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER306.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER315.INC'),ONCE        !Req'd for module callout resolution
                     END


QAPass               PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
HideMySave           BYTE                                  !
HideMyCancel         BYTE                                  !
LocalJobNumber       STRING(20)                            !
FoundAccessories     STRING(1000)                          !
SendingString        STRING(255)                           !
ListAccessory        STRING(30)                            !
SelectedCount        LONG                                  !
FileCount            LONG                                  !
BasicCount           LONG                                  !
JobOrLoan            STRING(4)                             !
FilesOpened     Long
LOANACC::State  USHORT
JOBACC::State  USHORT
ACCESSOR::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('QAPass')
  loc:formname = 'QAPass_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('QAPass','')
    p_web._DivHeader('QAPass',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferQAPass',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferQAPass',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferQAPass',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_QAPass',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferQAPass',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_QAPass',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
ValidateAccessories ROUTINE

!this works - the tagging produces FoundAccessories something like
!Found so far: ;|;HEAD CAMERA;|;SECOND BATTERY;|;CHARGER;|;MANUALS;|;GENERIC BATTERY;|;POWER SUPPLY
!to validate this everything in jobacc or loan acc for this job or loan must be in the list - and nothing else
    
    !Set up the variables
    FoundAccessories = p_web.gsv('FoundAccessories')
    SelectedCount = 0
    FileCount     = 0
    SendingString = ''
    
    !first count the number in the list generated by the tagging.
    Loop BasicCount = 1 to len(clip(FoundAccessories))
        if FoundAccessories[BasicCount] = '|' THEN
            SelectedCount += 1
        END !if this element of the string is |
    END !loop along selected list

    !this can be a call to QA a loan phone or a repair or an exchange - the loan is the odd one out - it has its own accessories
    case p_web.gsv('LocalHandsetType')  !REPAIR, THIRD PARTY REPAIR, '2ND EXCHANGE', 'EXCHANGE' or 'LOAN'
    OF 'LOAN'
        
        JobOrLoan = 'loan'
        !CHECK everything in the loan accessories in the list
        Access:LOANACC.ClearKey(lac:Ref_Number_Key)
        lac:Ref_Number = deformat(p_web.GSV('job:Loan_Unit_Number'))
        set(lac:Ref_Number_Key,lac:Ref_Number_Key)
        LOOP
            if Access:LOANACC.next() then BREAK.
            if lac:Ref_Number <> deformat(p_web.GSV('job:Loan_Unit_Number')) then break.
            if instring(clip(lac:Accessory),clip(FoundAccessories),1,1) = 0 THEN
                !error
                SendingString = 'ERROR - There is an accessory associated with this loan that has not been selected. Re-select accessories and attempt validation again or Fail Validation.'
                BREAK
            END !if accessory not found in the list
            
            FileCount += 1
            
        END !loop through matching file entries
        
    ELSE
        !this is a call for the accessories on the original phone on the job
        !used when this is a job or an exchange call doesn't matter which
        JobOrLoan = 'job'

        !check if everything in the job accessories is in the list. (works for Exchange, second exchange or the original phone)
        Access:Jobacc.clearkey(jac:Ref_Number_Key)
        jac:Ref_Number = deformat(p_web.gsv('LocalJobNumber'))
        set(jac:Ref_Number_Key,jac:Ref_Number_Key)
        LOOP
            
            if Access:JOBACC.next() then break.
            if jac:Ref_Number <> deformat(p_web.gsv('LocalJobNumber')) then break.
            
            if instring(clip(jac:Accessory),clip(FoundAccessories),1,1) = 0 THEN
                !error
                SendingString = 'ERROR - There is an accessory associated with this job that has not been selected. Re-select accessories and attempt validation again or Fail Validation.'
                BREAK
            END !if accessory not found in the list
            
            FileCount += 1
            
        END !loop through matching file entries
    END !case of loan or not
    
    if SendingString = ''       !if not then an error has already been found
        !no errors so far - check that the counts match
        if SelectedCount > FileCount THEN
            !special cases where there are no accessories
            !need to allow for 'NONE' and there are none! or for 'BLANK' and None
            if (clip(FoundAccessories) = ';|;NONE' AND FileCount = 0) or |
                (clip(FoundAccessories) = ';|;' AND FileCount = 0)    or |
                (clip(FoundAccessories) = '' AND FileCount = 0) THEN
                SendingString = 'Accessories Validated. '
            ELSE
                SendingString = 'ERROR - More accessories have been selected than are associated with this '&clip(JobOrLoan)&'. Re-select accessories and attempt validation again or Fail Validation.'
            END !if NONE and zero
        ELSE
            if SelectedCount < FileCount THEN
                SendingString = 'Error - Fewer accessories have been selected than are associated with this '&clip(JobOrLoan)&'. Re-select accessories and attempt validation again or Fail Validation.'
            ELSE
                SendingString = 'Accessories Validated. '
            END
        END !if selected > file
    END !SendingString = ''
OpenFiles  ROUTINE
  p_web._OpenFile(LOANACC)
  p_web._OpenFile(JOBACC)
  p_web._OpenFile(ACCESSOR)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(LOANACC)
  p_Web._CloseFile(JOBACC)
  p_Web._CloseFile(ACCESSOR)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
      HideMySave = p_web.gsv('HideMySave')
      HideMyCancel = p_web.gsv('HideMyCancel')
      p_web.ssv('FailReason','PASSED')  !needed to trigger completion on return to qaProcedure    
  p_web.SetValue('QAPass_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('ListAccessory',ListAccessory)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('ListAccessory')
    ListAccessory = p_web.GetValue('ListAccessory')
    p_web.SetSessionValue('ListAccessory',ListAccessory)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('QAPass_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 ListAccessory = p_web.RestoreValue('ListAccessory')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferQAPass')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('QAPass_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('QAPass_ChainTo')
    loc:formaction = p_web.GetSessionValue('QAPass_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="QAPass" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="QAPass" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="QAPass" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Model Accessories') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Model Accessories',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_QAPass">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_QAPass" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_QAPass')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Accessories for '& p_web.GSV('LimitModel')&' Handset') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_QAPass')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_QAPass'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.ListAccessory')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_QAPass')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Accessories for '& p_web.GSV('LimitModel')&' Handset') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_QAPass_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Accessories for '& p_web.GSV('LimitModel')&' Handset')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Accessories for '& p_web.GSV('LimitModel')&' Handset')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Accessories for '& p_web.GSV('LimitModel')&' Handset')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Accessories for '& p_web.GSV('LimitModel')&' Handset')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&50&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::TextShowMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::TextShowMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&50&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::ListAccessory
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::ListAccessory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::ListAccessory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_QAPass_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&50&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::ButtonValidate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::ButtonValidate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&50&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::ButtonMySave
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::ButtonMySave
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&50&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::ButtonMyCancel
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::ButtonMyCancel
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::TextShowMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('TextShowMessage',p_web.GetValue('NewValue'))
    do Value::TextShowMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::TextShowMessage  Routine
  p_web._DivHeader('QAPass_' & p_web._nocolon('TextShowMessage') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('Greenbold')&'">' & p_web.Translate('Hold "CTRL" or "SHIFT" to select more than one accessory',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::TextShowMessage  Routine
    loc:comment = ''
  p_web._DivHeader('QAPass_' & p_web._nocolon('TextShowMessage') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::ListAccessory  Routine
  p_web._DivHeader('QAPass_' & p_web._nocolon('ListAccessory') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::ListAccessory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('ListAccessory',p_web.GetValue('NewValue'))
    ListAccessory = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::ListAccessory
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('ListAccessory',p_web.GetValue('Value'))
    ListAccessory = p_web.GetValue('Value')
  End
  do SendAlert

Value::ListAccessory  Routine
  p_web._DivHeader('QAPass_' & p_web._nocolon('ListAccessory') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('ListAccessory')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''ListAccessory'',''qapass_listaccessory_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('ListAccessory',loc:fieldclass,loc:readonly,20,180,1,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  loc:rowstyle = choose(sub('GreyRegular',1,1)=' ',clip(loc:rowstyle) & 'GreyRegular','GreyRegular')
  if p_web.IfExistsSessionValue('ListAccessory') = 0
    p_web.SetSessionValue('ListAccessory','')
  end
    packet = clip(packet) & p_web.CreateOption('--- No Accessories ---','',choose('' = p_web.getsessionvalue('ListAccessory')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  !this generates the list of possible accessories the model may have.
  Access:ACCESSOR.Clearkey(acr:Accesory_Key)
  acr:Model_NUmber = p_web.GSV('LimitModel')
  Set(acr:Accesory_Key,acr:Accesory_Key)
  Loop
      If Access:ACCESSOR.Next()
          Break
      End ! If Access:ACCESSOR.Next()
      
      If acr:Model_Number <> p_web.GSV('LimitModel')
          Break
      End ! If acr:Model_Number <> p_web.GSV('LimitModel')
  
      loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
      packet = clip(packet) & p_web.CreateOption(Clip(acr:Accessory),acr:Accessory,choose(acr:Accessory = p_web.GSV('ListAccessory')),clip(loc:rowstyle),,)&CRLF
      loc:even = Choose(loc:even=1,2,1)
  End ! Loop
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('QAPass_' & p_web._nocolon('ListAccessory') & '_value')

Comment::ListAccessory  Routine
    loc:comment = ''
  p_web._DivHeader('QAPass_' & p_web._nocolon('ListAccessory') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::ButtonValidate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('ButtonValidate',p_web.GetValue('NewValue'))
    do Value::ButtonValidate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  !validation button pressed
  
      FoundAccessories = p_web.GSV('ListAccessory')    
      p_web.SSV('FoundAccessories',FoundAccessories)
  
      Do ValidateAccessories
  
      !if an error then go onto the fail screen (unless ammended)
      if SendingString[1:5] = 'ERROR' THEN
          p_web.Message('Alert',clip(SendingString),p_web._MessageClass,Net:Send) 
          !force them to cancel not save
          HideMySave = TRUE
          HideMyCancel = false 
          p_web.ssv('PassOrFail','FAIL')
          p_web.ssv('FailReason','ACCESSORIES MISSING')
      ELSE
          !force them to save not cancel
          HideMySave = FALSE
          HideMyCancel = TRUE
          p_web.ssv('PassOrFail','PASS')        
          p_web.ssv('FailReason','PASSED')    !something has to be returned to QAprocedure to trigger the UpdateJob routine
      END
  
      p_web.ssv('HideMySave',HideMySave)
      p_web.ssv('HideMyCancel',HideMyCancel)
  do Value::ButtonValidate
  do SendAlert
  do Value::ButtonMyCancel  !1
  do Value::ButtonMySave  !1

Value::ButtonValidate  Routine
  p_web._DivHeader('QAPass_' & p_web._nocolon('ButtonValidate') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''ButtonValidate'',''qapass_buttonvalidate_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','Validate','Attempt Validation','button-entryfield',loc:formname,,,,loc:javascript,0,'/images/lookup.png',,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('QAPass_' & p_web._nocolon('ButtonValidate') & '_value')

Comment::ButtonValidate  Routine
    loc:comment = ''
  p_web._DivHeader('QAPass_' & p_web._nocolon('ButtonValidate') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::ButtonMySave  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('ButtonMySave',p_web.GetValue('NewValue'))
    do Value::ButtonMySave
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::ButtonMySave  Routine
  p_web._DivHeader('QAPass_' & p_web._nocolon('ButtonMySave') & '_value',Choose(p_web.gsv('HideMySave')=true,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.gsv('HideMySave')=true)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ButtonMySave','Pass Validation','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('QAProcedure')) & ''','''&clip('_self')&''')',loc:javascript,0,'/images/psave.png',,,,'This will pass the current QA')

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('QAPass_' & p_web._nocolon('ButtonMySave') & '_value')

Comment::ButtonMySave  Routine
    loc:comment = ''
  p_web._DivHeader('QAPass_' & p_web._nocolon('ButtonMySave') & '_comment',Choose(p_web.gsv('HideMySave')=true,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.gsv('HideMySave')=true
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::ButtonMyCancel  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('ButtonMyCancel',p_web.GetValue('NewValue'))
    do Value::ButtonMyCancel
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::ButtonMyCancel  Routine
  p_web._DivHeader('QAPass_' & p_web._nocolon('ButtonMyCancel') & '_value',Choose(p_web.gsv('HideMyCancel') = true,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.gsv('HideMyCancel') = true)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ButtonMyCancel','Fail Validation','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('QA_FailReason')) & ''','''&clip('_self')&''')',loc:javascript,0,'/images/pcancel.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('QAPass_' & p_web._nocolon('ButtonMyCancel') & '_value')

Comment::ButtonMyCancel  Routine
    loc:comment = ''
  p_web._DivHeader('QAPass_' & p_web._nocolon('ButtonMyCancel') & '_comment',Choose(p_web.gsv('HideMyCancel') = true,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.gsv('HideMyCancel') = true
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('QAPass_ListAccessory_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::ListAccessory
      else
        do Value::ListAccessory
      end
  of lower('QAPass_ButtonValidate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::ButtonValidate
      else
        do Value::ButtonValidate
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('QAPass_form:ready_',1)
  p_web.SetSessionValue('QAPass_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_QAPass',0)

PreCopy  Routine
  p_web.SetValue('QAPass_form:ready_',1)
  p_web.SetSessionValue('QAPass_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_QAPass',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('QAPass_form:ready_',1)
  p_web.SetSessionValue('QAPass_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('QAPass:Primed',0)

PreDelete       Routine
  p_web.SetValue('QAPass_form:ready_',1)
  p_web.SetSessionValue('QAPass_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('QAPass:Primed',0)
  p_web.setsessionvalue('showtab_QAPass',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('QAPass_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('QAPass_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('QAPass:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('ListAccessory')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
