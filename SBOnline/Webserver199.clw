

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER199.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER059.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER089.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER114.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER188.INC'),ONCE        !Req'd for module callout resolution
                     END


FormDeletePart       PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locPartNumber        STRING(30)                            !
locDescription       STRING(30)                            !
locAlertMessage      STRING(255)                           !
locErrorMessage      STRING(255)                           !
locScrapRestock      BYTE                                  !
tmp:delete           BYTE                                  !
tmp:Scrap            BYTE                                  !
FilesOpened     Long
STOCKALL::State  USHORT
ESTPARTS::State  USHORT
STOFAULT::State  USHORT
STOCK::State  USHORT
PARTS::State  USHORT
JOBS::State  USHORT
WARPARTS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
                    Map
AddSolder               Procedure(String fType)
ShowAlert               Procedure(String fAlert)
RemoveWarrantyPartStockHistory      Procedure(String fInformation,STRING fType)
                    end
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormDeletePart')
  loc:formname = 'FormDeletePart_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormDeletePart','')
    p_web._DivHeader('FormDeletePart',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormDeletePart',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormDeletePart',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormDeletePart',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormDeletePart',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormDeletePart',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormDeletePart',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(STOCKALL)
  p_web._OpenFile(ESTPARTS)
  p_web._OpenFile(STOFAULT)
  p_web._OpenFile(STOCK)
  p_web._OpenFile(PARTS)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(WARPARTS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(STOCKALL)
  p_Web._CloseFile(ESTPARTS)
  p_Web._CloseFile(STOFAULT)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(WARPARTS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormDeletePart_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locPartNumber',locPartNumber)
  p_web.SetSessionValue('locDescription',locDescription)
  p_web.SetSessionValue('locAlertMessage',locAlertMessage)
  p_web.SetSessionValue('locErrorMessage',locErrorMessage)
  p_web.SetSessionValue('locScrapRestock',locScrapRestock)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locPartNumber')
    locPartNumber = p_web.GetValue('locPartNumber')
    p_web.SetSessionValue('locPartNumber',locPartNumber)
  End
  if p_web.IfExistsValue('locDescription')
    locDescription = p_web.GetValue('locDescription')
    p_web.SetSessionValue('locDescription',locDescription)
  End
  if p_web.IfExistsValue('locAlertMessage')
    locAlertMessage = p_web.GetValue('locAlertMessage')
    p_web.SetSessionValue('locAlertMessage',locAlertMessage)
  End
  if p_web.IfExistsValue('locErrorMessage')
    locErrorMessage = p_web.GetValue('locErrorMessage')
    p_web.SetSessionValue('locErrorMessage',locErrorMessage)
  End
  if p_web.IfExistsValue('locScrapRestock')
    locScrapRestock = p_web.GetValue('locScrapRestock')
    p_web.SetSessionValue('locScrapRestock',locScrapRestock)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormDeletePart_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  p_web.site.SaveButton.TextValue = 'Delete'
  p_web.site.SaveButton.Image = 'images/pdelete.png'
  
  p_web.SSV('DeletePart:ViewOnly',0)
  p_web.SSV('Hide:ScrapRestock',0)
  p_web.SSV('locAlertMessage','Click "Delete" to confirm deletion.')
  p_web.SSV('locErrorMessage','')
  p_web.SSV('Hide:ScrapRestock',1)
  p_web.SSV('locScrapRestock',0)
  
  IF (p_web.IfExistsValue('ReturnURL'))
      p_web.StoreValue('ReturnURL')
  END
  IF (p_web.IfExistsValue('FromURL'))
      p_web.StoreValue('FromURL')
  END
  
  If p_web.IfExistsValue('DelType')
      p_web.StoreValue('DelType')
  End ! If p_web.IfExistsValue('a')
  
  IF (p_web.IfExistsSessionValue('stl:RecordNumber') AND p_web.GSV('FromURL') = 'StockAllocation')
  
      Access:STOCKALL.Clearkey(stl:RecordNumberKey)
      stl:RecordNumber    = p_web.GSV('stl:RecordNumber')
      Access:STOCKALL.Tryfetch(stl:RecordNumberKey)
  
  !    Access:STOCKALL.ClearKey(stl:RecordNumberKey)
  !    stl:RecordNumber = p_web.GSV('AllocRecNo')
  !    IF (Access:STOCKALl.TryFetch(stl:RecordNumberKey))
  !    END
  
      p_web.SSV('DelType',stl:PartType)
      CASE p_web.GSV('DelType')
      OF 'WAR'
          p_web.SSV('wpr:Record_Number',stl:PartRecordNumber)
      OF 'EST'
          p_web.SSV('est:Record_Number',stl:PartRecordNumber)
      OF 'CHA'
          p_web.SSV('par:Record_Number',stl:PartRecordNumber)
      END
      Access:JOBS.ClearKey(job:Ref_Number_Key)
      job:Ref_Number = stl:JobNumber
      IF (Access:JOBS.TryFetch(job:Ref_Number_Key)= Level:Benign)
          p_web.FileToSessionQueue(JOBS)
      END
  
  
  ELSE
  
      If p_web.IfExistsValue('wpr:Record_Number')
          p_web.StoreValue('wpr:Record_Number')
      End ! If p_web.IfExistsValue('a')
  
      If p_web.IfExistsValue('par:Record_Number')
          p_web.StoreValue('par:Record_Number')
      End ! If p_web.IfExistsValue('a')
  
  END
  
  
  Case p_web.GSV('DelType')
  OF 'WAR'
      Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
      wpr:Record_Number = p_web.GSV('wpr:Record_Number')
      If (Access:WARPARTS.TryFetch(wpr:RecordNumberKey) = Level:Benign)
  
      End ! End
      p_web.SSV('locPartNumber',wpr:Part_Number)
      p_web.SSV('locDescription',wpr:Description)
  
      IF (wpr:Part_Number = 'EXCH')
          IF (p_web.GSV('job:Exchange_Unit_Number') > 0) ! #11937 Allow to delete an "EXCH" entry if there is no exchange unit attached. (Bryan: 19/01/2011)
              p_web.SSV('locErrorMessage','Click the "Exchange Unit" button on the Amend Job to remove an exchange unit')
          END !IF (p_web.GSV('job:Exchange_Unit_Number') > 0)
      ELSE
          Access:STOCK.ClearKey(sto:Ref_Number_Key)
          sto:Ref_Number = wpr:Part_Ref_Number
          IF (Access:STOCK.TryFetch(sto:Ref_Number_Key))
          END
  
          IF (p_web.GSV('BookingSite') = 'RRC')
              IF (sto:Location = p_web.GSV('ARC:SiteLocation'))
                  p_web.SSV('locErrorMessage','Cannot delete! This part was added at the ARC')
              ELSE
                  IF (p_web.GSV('job:Invoice_Number_Warranty') <> 0)
                      p_web.SSV('locErrorMessage','Cannot delete! The selected job has been invoiced.')
                  END
              END
          ELSE
              IF (sto:Location <> p_web.GSV('ARC:SiteLocation'))
                  p_web.SSV('locErrorMessage','Cannot Delete! This part was added at the RRC')
              ELSE
                  IF (p_web.GSV('job:Invoice_Number_Warranty') <> 0)
                      p_web.SSV('locErrorMessage','Cannot delete! The selected job has been invoiced.')
                  END
              END
          END
  
          IF (wpr:WebOrder)
              p_web.SSV('locAlertMessage','Warning! A Retail Order has been raised for this part.')
          ELSE
              IF (wpr:Adjustment = 'YES')
              ELSE
                  If (sto:Ref_Number > 0)
                      IF (sto:Sundry_Item = 'YES')
                      ELSE
                          IF (RapidLocation(sto:Location) AND p_web.GSV('FromURL') <> 'StockAllocation')
                              IF (sto:AttachBySolder)
                                  p_web.SSV('locAlertMessage','The selected part is "Attached By Solder" and therefore will be scrapped.')
                              ELSE ! IF (sto:AttachBySolder)
                                  IF (wpr:PartAllocated = 0)
                                      p_web.SSV('locAlertMessage','Warning! This part has not yet been allocated.')
                                  ELSE
                                      p_web.SSV('locAlertMessage','This part will be marked for return by allocations.')
                                  END
                              END ! IF (sto:AttachBySolder)
                          ELSE ! IF (RapidLocation(sto:Location))
                              p_web.SSV('locAlertMessage','Select "SCRAP" or "RESTOCK".')
                              p_web.SSV('Hide:ScrapRestock',0)
                          END ! IF (RapidLocation(sto:Location))
  
                      END
  
                  END
  
  
              END
          END
      END
  
  
  OF 'CHA'
      error# = 0
      Access:PARTS.ClearKey(par:recordnumberkey)
      par:Record_Number = p_web.GSV('par:Record_Number')
      IF (Access:PARTS.TryFetch(par:recordnumberkey) = Level:Benign)
      END
      p_web.SSV('locPartNumber',par:Part_Number)
      p_web.SSV('locDescription',par:Description)
  
      IF (par:Part_Number = 'EXCH')
          IF (p_web.GSV('job:Exchange_Unit_Number') > 0) ! #11937 Allow to delete an "EXCH" entry if there is no exchange unit attached. (Bryan: 19/01/2011)
              p_web.SSV('locErrorMessage','Click the "Exchange Unit" button on the Amend Job screen to remove an exchange unit')
              error# = 1
          END
  
      ELSE
          if (par:Status = 'RTS')
              if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'JOB - DELETE PART AW RETURN'))
                  p_web.SSV('locErrorMessage','You do not have access to delete this part')
                  error# = 1
              end ! if
          end ! if
          if (error# = 0)
              Access:STOCK.Clearkey(sto:Ref_Number_Key)
              sto:Ref_Number = par:Part_Ref_Number
              if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
              end ! if
  
              IF (IsJobInvoiced(p_web.GSV('job:Invoice_Number'),p_web) = TRUE)
                  p_web.SSV('locErrorMessage','Cannot delete! The selected job has been invoiced.')
                  ERROR# = 1
              ELSE
                  IF (p_web.GSV('BookingSite') = 'RRC')
                      IF (sto:Location = p_web.GSV('ARC:SiteLocation'))
                          p_web.SSV('locErrorMessage','Cannot delete! This part was added at the ARC')
                          ERROR# = 1
                      END
                  ELSE
                      IF (sto:Location <> p_web.GSV('ARC:SiteLocation'))
                          p_web.SSV('locErrorMessage','Cannot Delete! This part was added at the RRC')
                          ERROR#= 1
                      END
                  END
              END
          end ! if
          IF (ERROR# = 0)
              IF (par:WebOrder)
                  p_web.SSV('locAlertMessage','Warning! A Retail Order has been raised for this part.')
              ELSE
                  IF (par:Adjustment = 'YES')
                  ELSE
                      If (sto:Ref_Number > 0)
                          IF (sto:Sundry_Item = 'YES')
                          ELSE
                              IF (RapidLocation(sto:Location) AND p_web.GSV('FromURL') <> 'StockAllocation')
                                  IF (sto:AttachBySolder)
                                      p_web.SSV('locAlertMessage','The selected part is "Attached By Solder" and therefore will be scrapped.')
                                  ELSE ! IF (sto:AttachBySolder)
                                      IF (par:PartAllocated = 0)
                                          p_web.SSV('locAlertMessage','Warning! This part has not yet been allocated.')
                                      ELSE
                                          p_web.SSV('locAlertMessage','This part will be marked for return by allocations.')
                                      END
                                  END ! IF (sto:AttachBySolder)
                              ELSE ! IF (RapidLocation(sto:Location))
                                  p_web.SSV('locAlertMessage','Select "SCRAP" or "RESTOCK".')
                                  p_web.SSV('Hide:ScrapRestock',0)
                              END ! IF (RapidLocation(sto:Location))
  
                          END
  
                      END
  
  
                  END
              END
          END ! if
  
  
      END ! IF
  OF 'EST'
      error# = 0
      Access:ESTPARTS.ClearKey(epr:record_number_key)
      epr:Record_Number = p_web.GSV('epr:Record_Number')
      IF (Access:ESTPARTS.TryFetch(epr:record_number_key) = Level:Benign)
      END
      p_web.SSV('locPartNumber',epr:Part_Number)
      p_web.SSV('locDescription',epr:Description)
  
      if (epr:Status = 'RTS')
          if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'JOB - DELETE PART AW RETURN'))
              p_web.SSV('locErrorMessage','You do not have access to delete this part')
              error# = 1
          end ! if
      end ! if
      if (error# = 0)
          Access:STOCK.Clearkey(sto:Ref_Number_Key)
          sto:Ref_Number = epr:Part_Ref_Number
          if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
          end ! if
  
          if (p_web.GSV('job:Estimate_Accepted') = 'YES')
              p_web.SSV('locErrorMessage','Cannot delete! The estimate has been accepted.')
              error# = 1
          else
              if (p_web.GSV('job:Estimate_Rejected') = 'YES')
                  p_web.SSV('locErrorMessage','Cannot delete! The estimate has been rejected.')
                  error# = 1
              end
          end
      end ! if
      IF (ERROR# = 0)
              IF (epr:Adjustment = 'YES' or epr:UsedOnRepair = 0)
                  IF (sto:AttachBySolder)
                      p_web.SSV('locAlertMessage','The selected part is "Attached By Solder" and therefore will be scrapped.')
                  END
              ELSE
                  If (sto:Ref_Number > 0)
                      IF (sto:Sundry_Item = 'YES')
                      ELSE
                          IF (RapidLocation(sto:Location) AND p_web.GSV('FromURL') <> 'StockAllocation')
                              IF (sto:AttachBySolder)
                                  p_web.SSV('locAlertMessage','The selected part is "Attached By Solder" and therefore will be scrapped.')
                              ELSE ! IF (sto:AttachBySolder)
                                  IF (epr:PartAllocated = 0)
                                      p_web.SSV('locAlertMessage','Warning! This part has not yet been allocated.')
                                  ELSE
                                      p_web.SSV('locAlertMessage','This part will be marked for return by allocations.')
                                  END
                              END ! IF (sto:AttachBySolder)
                          ELSE ! IF (RapidLocation(sto:Location))
                              p_web.SSV('locAlertMessage','Select "SCRAP" or "RESTOCK".')
                              p_web.SSV('Hide:ScrapRestock',0)
                          END ! IF (RapidLocation(sto:Location))
                      END
                  END
              END
      END ! if
  
  ELSE
  
  END
  
  IF (p_web.GSV('locErrorMessage') <> '')
      p_web.SSV('locAlertMessage','')
      ShowALert(p_web.GSV('locErrorMessage'))
      p_web.SSV('DeletePart:ViewOnly',1)
  END
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locPartNumber = p_web.RestoreValue('locPartNumber')
 locDescription = p_web.RestoreValue('locDescription')
 locAlertMessage = p_web.RestoreValue('locAlertMessage')
 locErrorMessage = p_web.RestoreValue('locErrorMessage')
 locScrapRestock = p_web.RestoreValue('locScrapRestock')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('ReturnURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormDeletePart_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormDeletePart_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormDeletePart_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('ReturnURL')
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
    loc:viewonly = Choose(p_web.GSV('DeletePart:ViewOnly') = 1,1,0)
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormDeletePart" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormDeletePart" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormDeletePart" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Delete Part') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Delete Part',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormDeletePart">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormDeletePart" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormDeletePart')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Part Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormDeletePart')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormDeletePart'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormDeletePart')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Part Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormDeletePart_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locPartNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locPartNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locPartNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locDescription
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locDescription
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locDescription
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormDeletePart_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::locAlertMessage
      do Comment::locAlertMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::locErrorMessage
      do Comment::locErrorMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locScrapRestock
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locScrapRestock
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locScrapRestock
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locPartNumber  Routine
  p_web._DivHeader('FormDeletePart_' & p_web._nocolon('locPartNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Part Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locPartNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locPartNumber',p_web.GetValue('NewValue'))
    locPartNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locPartNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locPartNumber',p_web.GetValue('Value'))
    locPartNumber = p_web.GetValue('Value')
  End
  do Value::locPartNumber
  do SendAlert

Value::locPartNumber  Routine
  p_web._DivHeader('FormDeletePart_' & p_web._nocolon('locPartNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locPartNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locPartNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locPartNumber'',''formdeletepart_locpartnumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locPartNumber',p_web.GetSessionValueFormat('locPartNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormDeletePart_' & p_web._nocolon('locPartNumber') & '_value')

Comment::locPartNumber  Routine
      loc:comment = ''
  p_web._DivHeader('FormDeletePart_' & p_web._nocolon('locPartNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locDescription  Routine
  p_web._DivHeader('FormDeletePart_' & p_web._nocolon('locDescription') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Description')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locDescription  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locDescription',p_web.GetValue('NewValue'))
    locDescription = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locDescription
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locDescription',p_web.GetValue('Value'))
    locDescription = p_web.GetValue('Value')
  End
  do Value::locDescription
  do SendAlert

Value::locDescription  Routine
  p_web._DivHeader('FormDeletePart_' & p_web._nocolon('locDescription') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locDescription
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locDescription')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locDescription'',''formdeletepart_locdescription_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locDescription',p_web.GetSessionValueFormat('locDescription'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormDeletePart_' & p_web._nocolon('locDescription') & '_value')

Comment::locDescription  Routine
      loc:comment = ''
  p_web._DivHeader('FormDeletePart_' & p_web._nocolon('locDescription') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::locAlertMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAlertMessage',p_web.GetValue('NewValue'))
    locAlertMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAlertMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locAlertMessage',p_web.GetValue('Value'))
    locAlertMessage = p_web.GetValue('Value')
  End

Value::locAlertMessage  Routine
  p_web._DivHeader('FormDeletePart_' & p_web._nocolon('locAlertMessage') & '_value',Choose(p_web.GSV('locAlertMessage') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAlertMessage') = '')
  ! --- DISPLAY --- locAlertMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locAlertMessage'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locAlertMessage  Routine
    loc:comment = ''
  p_web._DivHeader('FormDeletePart_' & p_web._nocolon('locAlertMessage') & '_comment',Choose(p_web.GSV('locAlertMessage') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locAlertMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::locErrorMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locErrorMessage',p_web.GetValue('NewValue'))
    locErrorMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locErrorMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locErrorMessage',p_web.GetValue('Value'))
    locErrorMessage = p_web.GetValue('Value')
  End

Value::locErrorMessage  Routine
  p_web._DivHeader('FormDeletePart_' & p_web._nocolon('locErrorMessage') & '_value',Choose(p_web.GSV('locErrorMessage') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locErrorMessage') = '')
  ! --- DISPLAY --- locErrorMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('red bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locErrorMessage'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locErrorMessage  Routine
    loc:comment = ''
  p_web._DivHeader('FormDeletePart_' & p_web._nocolon('locErrorMessage') & '_comment',Choose(p_web.GSV('locErrorMessage') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locErrorMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locScrapRestock  Routine
  p_web._DivHeader('FormDeletePart_' & p_web._nocolon('locScrapRestock') & '_prompt',Choose(p_web.GSV('Hide:ScrapRestock') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Delete Option')
  If p_web.GSV('Hide:ScrapRestock') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locScrapRestock  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locScrapRestock',p_web.GetValue('NewValue'))
    locScrapRestock = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locScrapRestock
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locScrapRestock',p_web.GetValue('Value'))
    locScrapRestock = p_web.GetValue('Value')
  End
  do Value::locScrapRestock
  do SendAlert

Value::locScrapRestock  Routine
  p_web._DivHeader('FormDeletePart_' & p_web._nocolon('locScrapRestock') & '_value',Choose(p_web.GSV('Hide:ScrapRestock') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ScrapRestock') = 1)
  ! --- RADIO --- locScrapRestock
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locScrapRestock')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locScrapRestock') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locScrapRestock'',''formdeletepart_locscraprestock_value'',1,'''&clip(1)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locScrapRestock',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locScrapRestock_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Scrap') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locScrapRestock') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locScrapRestock'',''formdeletepart_locscraprestock_value'',1,'''&clip(2)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locScrapRestock',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locScrapRestock_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Restock') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormDeletePart_' & p_web._nocolon('locScrapRestock') & '_value')

Comment::locScrapRestock  Routine
    loc:comment = ''
  p_web._DivHeader('FormDeletePart_' & p_web._nocolon('locScrapRestock') & '_comment',Choose(p_web.GSV('Hide:ScrapRestock') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:ScrapRestock') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormDeletePart_locPartNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locPartNumber
      else
        do Value::locPartNumber
      end
  of lower('FormDeletePart_locDescription_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locDescription
      else
        do Value::locDescription
      end
  of lower('FormDeletePart_locScrapRestock_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locScrapRestock
      else
        do Value::locScrapRestock
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormDeletePart_form:ready_',1)
  p_web.SetSessionValue('FormDeletePart_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormDeletePart',0)

PreCopy  Routine
  p_web.SetValue('FormDeletePart_form:ready_',1)
  p_web.SetSessionValue('FormDeletePart_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormDeletePart',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormDeletePart_form:ready_',1)
  p_web.SetSessionValue('FormDeletePart_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormDeletePart:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormDeletePart_form:ready_',1)
  p_web.SetSessionValue('FormDeletePart_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormDeletePart:Primed',0)
  p_web.setsessionvalue('showtab_FormDeletePart',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormDeletePart_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  BHAddToDebugLog('DeletePart: ' & p_web.GSV('Hide:ScrapRestock') & ' - ' & p_web.GSV('locScrapRestock'))
  
  if (p_web.GSV('Hide:ScrapRestock') <> 1)
      if (p_web.GSV('locScrapRestock') = 0)
          loc:Invalid = 'locPartNumber'
          loc:Alert = 'You must select "Scrap" or "Restock"'
          exit
      ENd
  End
  p_web.DeleteSessionValue('FormDeletePart_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
  ! tab = 3
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
  ! Delete Part
  tmp:Delete = 0
  tmp:Scrap = 0
  Case p_web.GSV('DelType')
  of 'WAR'
      Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
      wpr:Record_Number = p_web.GSV('wpr:Record_Number')
      If (Access:WARPARTS.TryFetch(wpr:RecordNumberKey))
  
      End
  
      Access:STOCK.Clearkey(sto:Ref_Number_Key)
      sto:Ref_Number = wpr:Part_Ref_Number
      If (Access:STOCK.TryFetch(sto:Ref_Number_Key))
      End
  
      if (wpr:WebOrder)
          !RemoveFromStockAllocation(wpr:Record_Number,'WAR')
            tmp:delete = 1
      else
          IF (wpr:Adjustment = 'YES')
              tmp:delete = 1
          ELSE
              IF (sto:Sundry_Item = 'YES')
                  tmp:delete = 1
              ELSE
  
                  IF (RapidLocation(sto:Location) AND p_web.GSV('FromURL') <> 'StockAllocation')
                      if (sto:AttachBySolder)
                          tmp:Delete = 1
                          tmp:Scrap = 1
                          AddSolder('WAR')
                      else ! if (sto:AttachBySolder)
                          if (wpr:PartAllocated = 0)
  
                              sto:Quantity_STock += wpr:Quantity
                              If (Access:STOCK.TryUpdate() = Level:Benign)
                                  tmp:Delete = 1
                                  RemoveWarrantyPartStockHistory('UNALLOCATED PART REMOVED','WAR')
                                  if (sto:Suspend)
                                      ! Not has stock. Unsuspend part
                                      sto:Suspend = 0
                                      If (Access:STOCK.TryUpdate() = Level:Benign)
                                          if (AddToStockHistory(sto:Ref_Number, |
                                              'ADD', |
                                              '', |
                                              0, |
                                              0, |
                                              0, |
                                              wpr:Purchase_Cost, |
                                              wpr:Sale_Cost, |
                                              wpr:Retail_Cost, |
                                              'PART UNSUSPENDED', |
                                              '', |
                                              p_web.GSV('BookingUserCode'),|
                                              sto:Quantity_Stock))
                                          end
  
                                      end
                                  end
                              End
                          else ! if (wpr:PartAllocated = 0)
                              wpr:PartAllocated = 0
                              wpr:Status = 'RET'
                              Access:WARPARTS.TryUpdate()
                              p_web.FileToSessionQueue(WARPARTS)
                                AddToStockAllocation(wpr:Record_Number,'WAR',wpr:Quantity,'RET',p_web.GSV('job:Engineer'),p_web)
                          end
                      end
                  else ! If (RapidLocation(sto:Location))
                      Case p_web.GSV('locScrapRestock')
                      of 1 ! Scrap
                          tmp:Delete = 1
                          tmp:Scrap = 1
                      of 2 ! Restock
                          sto:Quantity_Stock += wpr:Quantity
                          If (Access:STOCK.TryUpdate() = Level:Benign)
                              tmp:Delete = 1
                              RemoveWarrantyPartStockHistory('','WAR')
                              if (sto:Suspend)
                                  ! Not has stock. Unsuspend part
                                  sto:Suspend = 0
                                  If (Access:STOCK.TryUpdate() = Level:Benign)
                                      if (AddToStockHistory(sto:Ref_Number, |
                                          'ADD', |
                                          '', |
                                          0, |
                                          0, |
                                          0, |
                                          wpr:Purchase_Cost, |
                                          wpr:Sale_Cost, |
                                          wpr:Retail_Cost, |
                                          'PART UNSUSPENDED', |
                                          '', |
                                          p_web.GSV('BookingUserCode'),|
                                          sto:Quantity_Stock))
                                      end
  
                                  end
                              end
  
                          End
  
                      end
  
                  end ! If (RapidLocation(sto:Location))
              END
          END
  
      end
      if (tmp:Scrap)
          RemoveFromSTockAllocation(wpr:Record_Number,'WAR')
  !          p_web.SSV('AddToAudit:Type','JOB')
  !          p_web.SSV('AddToAudit:Action','SCRAP WARRANTY PART: ' & Clip(wpr:Part_Number))
          p_web.SSV('AddToAudit:Notes','DESCRIPTION: ' & Clip(wpr:Description))
          if (sto:AttachBySolder)
              p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & '<13,10,13,10>PART ATTACHED BY SOLDER')
          End
          p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & 'LOCATION: ' & clip(sto:Location) & |
              '<13,10>QTY: ' & wpr:Quantity)
          AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','SCRAP WARRANTY PART: ' & Clip(wpr:Part_Number),p_web.GSV('AddToAudit:Notes'))
      else
  
      end
      IF (tmp:delete = 1)
          RemoveFromSTockAllocation(wpr:Record_Number,'WAR')
  !          p_web.SSV('AddToAudit:Type','JOB')
  !          p_web.SSV('AddToAudit:Action','WARRANTY PART DELETED: ' & Clip(wpr:Part_Number))
          p_web.SSV('AddToAudit:Notes','DESCRIPTION: ' & Clip(wpr:Description))
          if (sto:AttachBySolder)
              p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & '<13,10,13,10>PART ATTACHED BY SOLDER')
          End
          AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','WARRANTY PART DELETED: ' & Clip(wpr:Part_Number),p_web.GSV('AddToAudit:Notes'))
          Relate:WARPARTS.Delete(0)
      END
  
  OF 'CHA'
      Access:PARTS.Clearkey(par:RecordNumberKey)
      par:Record_Number = p_web.GSV('par:Record_Number')
      If (Access:PARTS.TryFetch(par:RecordNumberKey))
  
      End
  
      Access:STOCK.Clearkey(sto:Ref_Number_Key)
      sto:Ref_Number = par:Part_Ref_Number
      If (Access:STOCK.TryFetch(sto:Ref_Number_Key))
      End
      if (par:WebOrder)
          !RemoveFromStockAllocation(par:Record_Number,'CHA')
            tmp:delete = 1
      else
          if (par:Adjustment = 'YES')
              tmp:delete = 1
          ELSE
              IF (sto:Sundry_Item = 'YES')
                  tmp:delete = 1
              ELSE
                  IF (RapidLocation(sto:Location) AND p_web.GSV('FromURL') <> 'StockAllocation')
                      if (sto:AttachBySolder)
                          tmp:Delete = 1
                          tmp:Scrap = 1
                          AddSolder('CHA')
                      else ! if (sto:AttachBySolder)
                          if (par:PartAllocated = 0)
                              sto:Quantity_STock += par:Quantity
                              If (Access:STOCK.TryUpdate() = Level:Benign)
                                  tmp:Delete = 1
                                  RemoveWarrantyPartStockHistory('UNALLOCATED PART REMOVED','CHA')
                                  if (sto:Suspend)
                                      ! Not has stock. Unsuspend part
                                      sto:Suspend = 0
                                      If (Access:STOCK.TryUpdate() = Level:Benign)
                                          if (AddToStockHistory(sto:Ref_Number, |
                                              'ADD', |
                                              '', |
                                              0, |
                                              0, |
                                              0, |
                                              par:Purchase_Cost, |
                                              par:Sale_Cost, |
                                              par:Retail_Cost, |
                                              'PART UNSUSPENDED', |
                                              '', |
                                              p_web.GSV('BookingUserCode'),|
                                              sto:Quantity_Stock))
                                          end
  
                                      end
                                  end
                              End
                          else ! if (wpr:PartAllocated = 0)
                              par:PartAllocated = 0
                              par:Status = 'RET'
                              Access:PARTS.TryUpdate()
                              p_web.FileToSessionQueue(PARTS)
                              AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,'RET',p_web.GSV('job:Engineer'),p_web)
                          end
                      end
                  else ! If (RapidLocation(sto:Location))
                      Case p_web.GSV('locScrapRestock')
                      of 1 ! Scrap
                          tmp:Delete = 1
                          tmp:Scrap = 1
                      of 2 ! Restock
                          sto:Quantity_Stock += par:Quantity
                          If (Access:STOCK.TryUpdate() = Level:Benign)
                              tmp:Delete = 1
                              RemoveWarrantyPartStockHistory('','CHA')
                              if (sto:Suspend)
                                  ! Not has stock. Unsuspend part
                                  sto:Suspend = 0
                                  If (Access:STOCK.TryUpdate() = Level:Benign)
                                      if (AddToStockHistory(sto:Ref_Number, |
                                          'ADD', |
                                          '', |
                                          0, |
                                          0, |
                                          0, |
                                          par:Purchase_Cost, |
                                          par:Sale_Cost, |
                                          par:Retail_Cost, |
                                          'PART UNSUSPENDED', |
                                          '', |
                                          p_web.GSV('BookingUserCode'),|
                                          sto:Quantity_Stock))
                                      end
  
                                  end
                              end
                          End
                      end
                  end ! If (RapidLocation(sto:Location))
              END
          END
      end
      if (tmp:Scrap)
          RemoveFromSTockAllocation(par:Record_Number,'CHA')
  !          p_web.SSV('AddToAudit:Type','JOB')
  !          p_web.SSV('AddToAudit:Action','SCRAP CHARGEABLE PART: ' & Clip(par:Part_Number))
          p_web.SSV('AddToAudit:Notes','DESCRIPTION: ' & Clip(par:Description))
          if (sto:AttachBySolder)
              p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & '<13,10,13,10>PART ATTACHED BY SOLDER')
          End
          p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & 'LOCATION: ' & clip(sto:Location) & |
              '<13,10>QTY: ' & par:Quantity)
          AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','SCRAP CHARGEABLE PART: ' & Clip(par:Part_Number),p_web.GSV('AddToAudit:Notes'))
      else
  
      end
      IF (tmp:delete = 1)
          RemoveFromSTockAllocation(par:Record_Number,'CHA')
          p_web.SSV('AddToAudit:Type','JOB')
          p_web.SSV('AddToAudit:Action','CHARGEABLE PART DELETED: ' & Clip(par:Part_Number))
          p_web.SSV('AddToAudit:Notes','DESCRIPTION: ' & Clip(par:Description))
          if (sto:AttachBySolder)
              p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & '<13,10,13,10>PART ATTACHED BY SOLDER')
          End
          AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','CHARGEABLE PART DELETED: ' & Clip(par:Part_Number),p_web.GSV('AddToAudit:Notes'))
          Relate:PARTS.Delete(0)
      END
  
  OF 'EST'
      Access:ESTPARTS.Clearkey(epr:Record_Number_Key)
      epr:Record_Number = p_web.GSV('epr:Record_Number')
      If (Access:ESTPARTS.TryFetch(epr:Record_Number_Key))
  
      End
  
      Access:STOCK.Clearkey(sto:Ref_Number_Key)
      sto:Ref_Number = epr:Part_Ref_Number
      If (Access:STOCK.TryFetch(sto:Ref_Number_Key))
      End
  
      if (epr:Adjustment = 'YES')
          tmp:delete = 1
          if (sto:AttachBySolder)
              tmp:Delete = 1
              tmp:Scrap = 1
              AddSolder('EST')
          end ! if (sto:AttachBySolder)
  
      ELSE
          IF (sto:Sundry_Item = 'YES')
              tmp:delete = 1
          ELSE
              IF (RapidLocation(sto:Location) AND p_web.GSV('FromURL') <> 'StockAllocation')
                  if (sto:AttachBySolder)
                      tmp:Delete = 1
                      tmp:Scrap = 1
                      AddSolder('EST')
                  else ! if (sto:AttachBySolder)
                      if (epr:PartAllocated = 0)
                          sto:Quantity_STock += epr:Quantity
                          If (Access:STOCK.TryUpdate() = Level:Benign)
                              tmp:Delete = 1
                              RemoveWarrantyPartStockHistory('UNALLOCATED PART REMOVED','EST')
                              if (sto:Suspend)
                                  ! Not has stock. Unsuspend part
                                  sto:Suspend = 0
                                  If (Access:STOCK.TryUpdate() = Level:Benign)
                                      if (AddToStockHistory(sto:Ref_Number, |
                                          'ADD', |
                                          '', |
                                          0, |
                                          0, |
                                          0, |
                                          epr:Purchase_Cost, |
                                          epr:Sale_Cost, |
                                          epr:Retail_Cost, |
                                          'PART UNSUSPENDED', |
                                          '', |
                                          p_web.GSV('BookingUserCode'),|
                                          sto:Quantity_Stock))
                                      end
  
                                  end
                              end
                          End
                      else ! if (wpr:PartAllocated = 0)
                          epr:PartAllocated = 0
                          epr:Status = 'RET'
                          Access:ESTPARTS.TryUpdate()
                          p_web.FileToSessionQueue(ESTPARTS)
                            AddToStockAllocation(epr:Record_Number,'EST',epr:Quantity,'RET',p_web.GSV('job:Engineer'),p_web)
                      end
                  end
              else ! If (RapidLocation(sto:Location))
                  Case p_web.GSV('locScrapRestock')
                  of 1 ! Scrap
                      tmp:Delete = 1
                      tmp:Scrap = 1
                  of 2 ! Restock
                      sto:Quantity_Stock += epr:Quantity
                      If (Access:STOCK.TryUpdate() = Level:Benign)
                          tmp:Delete = 1
                          RemoveWarrantyPartStockHistory('','EST')
                          if (sto:Suspend)
                              ! Not has stock. Unsuspend part
                              sto:Suspend = 0
                              If (Access:STOCK.TryUpdate() = Level:Benign)
                                  if (AddToStockHistory(sto:Ref_Number, |
                                      'ADD', |
                                      '', |
                                      0, |
                                      0, |
                                      0, |
                                      epr:Purchase_Cost, |
                                      epr:Sale_Cost, |
                                      epr:Retail_Cost, |
                                      'PART UNSUSPENDED', |
                                      '', |
                                      p_web.GSV('BookingUserCode'),|
                                      sto:Quantity_Stock))
                                  end
  
                              end
                          end
  
                      End
  
                  end
  
              end ! If (RapidLocation(sto:Location))
          END
  
      END
  
      if (tmp:Scrap)
          if (epr:UsedOnRepair)
              RemoveFromSTockAllocation(epr:Record_Number,'EST')
          end
          p_web.SSV('AddToAudit:Type','JOB')
          p_web.SSV('AddToAudit:Action','SCRAP ESTIMATE PART: ' & Clip(epr:Part_Number))
          p_web.SSV('AddToAudit:Notes','DESCRIPTION: ' & Clip(epr:Description))
          if (sto:AttachBySolder)
              p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & '<13,10,13,10>PART ATTACHED BY SOLDER')
          End
          p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & 'LOCATION: ' & clip(sto:Location) & |
              '<13,10>QTY: ' & epr:Quantity)
          AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','SCRAP ESTIMATE PART: ' & Clip(epr:Part_Number),p_web.GSV('AddToAudit:Notes'))
      else
  
      end
      IF (tmp:delete = 1)
          RemoveFromSTockAllocation(epr:Record_Number,'EST')
          p_web.SSV('AddToAudit:Type','JOB')
          p_web.SSV('AddToAudit:Action','ESTIMATE PART DELETED: ' & Clip(epr:Part_Number))
          p_web.SSV('AddToAudit:Notes','DESCRIPTION: ' & Clip(epr:Description))
          if (sto:AttachBySolder)
              p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & '<13,10,13,10>PART ATTACHED BY SOLDER')
          End
          AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','ESTIMATE PART DELETED: ' & Clip(epr:Part_Number),p_web.GSV('AddToAudit:Notes'))
          Relate:ESTPARTS.Delete(0)
      END
  
  
  end
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormDeletePart:Primed',0)
  p_web.StoreValue('locPartNumber')
  p_web.StoreValue('locDescription')
  p_web.StoreValue('locAlertMessage')
  p_web.StoreValue('locErrorMessage')
  p_web.StoreValue('locScrapRestock')
AddSolder           Procedure(String fType)
    Code

        Case fType
        of 'WAR'
            If Access:STOFAULT.PrimeRecord() = Level:Benign
                stf:PartNumber  = wpr:Part_Number
                stf:Description = wpr:Description
                stf:Quantity    = wpr:Quantity
                stf:PurchaseCost    = wpr:Purchase_Cost
                stf:ModelNumber = p_web.GSV('job:Model_Number')
                stf:IMEI        = p_web.GSV('job:ESN')
                stf:Engineer    = p_web.GSV('job:Engineer')
                stf:StoreUserCode   = p_web.GSV('BookingUserCode')
                stf:PartType        = 'SOL'
                If Access:STOFAULT.TryInsert() = Level:Benign
                    !Insert Successful

                Else !If Access:STOFAULT.TryInsert() = Level:Benign
                    !Insert Failed
                    Access:STOFAULT.CancelAutoInc()
                End !If Access:STOFAULT.TryInsert() = Level:Benign
            End !If Access:STOFAULT.PrimeRecord() = Level:Benign

        of 'CHA'
            If Access:STOFAULT.PrimeRecord() = Level:Benign
                stf:PartNumber  = par:Part_Number
                stf:Description = par:Description
                stf:Quantity    = par:Quantity
                stf:PurchaseCost    = par:Purchase_Cost
                stf:ModelNumber = p_web.GSV('job:Model_Number')
                stf:IMEI        = p_web.GSV('job:ESN')
                stf:Engineer    = p_web.GSV('job:Engineer')
                stf:StoreUserCode   = p_web.GSV('BookingUserCode')
                stf:PartType        = 'SOL'
                If Access:STOFAULT.TryInsert() = Level:Benign
                    !Insert Successful

                Else !If Access:STOFAULT.TryInsert() = Level:Benign
                    !Insert Failed
                    Access:STOFAULT.CancelAutoInc()
                End !If Access:STOFAULT.TryInsert() = Level:Benign
            End !If Access:STOFAULT.PrimeRecord() = Level:Benign

        of 'EST'
            If Access:STOFAULT.PrimeRecord() = Level:Benign
                stf:PartNumber  = epr:Part_Number
                stf:Description = epr:Description
                stf:Quantity    = epr:Quantity
                stf:PurchaseCost    = epr:Purchase_Cost
                stf:ModelNumber = p_web.GSV('job:Model_Number')
                stf:IMEI        = p_web.GSV('job:ESN')
                stf:Engineer    = p_web.GSV('job:Engineer')
                stf:StoreUserCode   = p_web.GSV('BookingUserCode')
                stf:PartType        = 'SOL'
                If Access:STOFAULT.TryInsert() = Level:Benign
                    !Insert Successful

                Else !If Access:STOFAULT.TryInsert() = Level:Benign
                    !Insert Failed
                    Access:STOFAULT.CancelAutoInc()
                End !If Access:STOFAULT.TryInsert() = Level:Benign
            End !If Access:STOFAULT.PrimeRecord() = Level:Benign

        end

RemoveWarrantyPartStockHistory      Procedure(String fInformation,STRING fType)
Code

    CASE fType
    OF 'WAR'
        If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
            'ADD', | ! Transaction_Type
            wpr:Despatch_Note_Number, | ! Depatch_Note_Number
            p_web.GSV('job:Ref_Number'), | ! Job_Number
            0, | ! Sales_Number
            wpr:Quantity, | ! Quantity
            wpr:Purchase_Cost, | ! Purchase_Cost
            wpr:Sale_Cost, | ! Sale_Cost
            wpr:Retail_Cost, | ! Retail_Cost
            fInformation, | ! Notes
            'WARRANTY PART REMOVED FROM JOB', |
            p_web.GSV('BookingUserCode'),|
            sto:Quantity_Stock) ! Information

        Else ! AddToStockHistory
            ! Error
        End ! AddToStockHistory
    OF 'CHA'
        If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
            'ADD', | ! Transaction_Type
            par:Despatch_Note_Number, | ! Depatch_Note_Number
            p_web.GSV('job:Ref_Number'), | ! Job_Number
            0, | ! Sales_Number
            par:Quantity, | ! Quantity
            par:Purchase_Cost, | ! Purchase_Cost
            par:Sale_Cost, | ! Sale_Cost
            par:Retail_Cost, | ! Retail_Cost
            fInformation, | ! Notes
            'WARRANTY PART REMOVED FROM JOB', |
            p_web.GSV('BookingUserCode'),|
            sto:Quantity_Stock) ! Information

        Else ! AddToStockHistory
            ! Error
        End ! AddToStockHistory
    END ! Case

ShowAlert     Procedure(String fAlert)
Code
    packet = clip(packet) & '<script language="JavaScript" type="text/javascript">alert("' & clip(fAlert) & '")</script>'
    do sendPacket
