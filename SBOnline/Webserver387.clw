

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER387.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER285.INC'),ONCE        !Req'd for module callout resolution
                     END


FormWIPAuditCriteria PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locWIPSerialsScannedLocation LONG                          !
locWIPSerialsNotScanned LONG                               !
locWIPSerialsScanned LONG                                  !
FilesOpened     Long
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormWIPAuditCriteria')
  loc:formname = 'FormWIPAuditCriteria_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormWIPAuditCriteria','')
    p_web._DivHeader('FormWIPAuditCriteria',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormWIPAuditCriteria',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormWIPAuditCriteria',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormWIPAuditCriteria',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormWIPAuditCriteria',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormWIPAuditCriteria',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormWIPAuditCriteria',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormWIPAuditCriteria_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locWIPSerialsScanned',locWIPSerialsScanned)
  p_web.SetSessionValue('locWIPSerialsScannedLocation',locWIPSerialsScannedLocation)
  p_web.SetSessionValue('locWIPSerialsNotScanned',locWIPSerialsNotScanned)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locWIPSerialsScanned')
    locWIPSerialsScanned = p_web.GetValue('locWIPSerialsScanned')
    p_web.SetSessionValue('locWIPSerialsScanned',locWIPSerialsScanned)
  End
  if p_web.IfExistsValue('locWIPSerialsScannedLocation')
    locWIPSerialsScannedLocation = p_web.GetValue('locWIPSerialsScannedLocation')
    p_web.SetSessionValue('locWIPSerialsScannedLocation',locWIPSerialsScannedLocation)
  End
  if p_web.IfExistsValue('locWIPSerialsNotScanned')
    locWIPSerialsNotScanned = p_web.GetValue('locWIPSerialsNotScanned')
    p_web.SetSessionValue('locWIPSerialsNotScanned',locWIPSerialsNotScanned)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormWIPAuditCriteria_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    IF (p_web.IfExistsValue('wim:Audit_Number'))
        Access:WIPAMF.ClearKey(wim:Audit_Number_Key)
        wim:Audit_Number = p_web.GetValue('wim:Audit_Number')
        IF (Access:WIPAMF.TryFetch(wim:Audit_Number_Key))
        END ! IF
        p_web.FileToSessionQueue(WIPAMF)
    END ! IF
  
    p_web.SSV('locWIPSerialsScannedLocation','')
    p_web.SSV('locWIPSerialsNotScanned','')
    p_web.SSV('locWIPSerialsScanned','')
    
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locWIPSerialsScanned = p_web.RestoreValue('locWIPSerialsScanned')
 locWIPSerialsScannedLocation = p_web.RestoreValue('locWIPSerialsScannedLocation')
 locWIPSerialsNotScanned = p_web.RestoreValue('locWIPSerialsNotScanned')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferFormWIPAuditCriteria')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormWIPAuditCriteria_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormWIPAuditCriteria_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormWIPAuditCriteria_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'FormWIPAudits'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormWIPAuditCriteria" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormWIPAuditCriteria" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormWIPAuditCriteria" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('WIP Audit Report') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('WIP Audit Report',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormWIPAuditCriteria">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormWIPAuditCriteria" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormWIPAuditCriteria')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Criteria') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormWIPAuditCriteria')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormWIPAuditCriteria'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locWIPSerialsScanned')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormWIPAuditCriteria')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Criteria') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormWIPAuditCriteria_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locWIPSerialsScanned
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locWIPSerialsScanned
      do Comment::locWIPSerialsScanned
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnPrint1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::btnPrint1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locWIPsSerialScannedLocation
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locWIPsSerialScannedLocation
      do Comment::locWIPsSerialScannedLocation
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnPrint2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::btnPrint2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locWIPSerialsNotScanned
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locWIPSerialsNotScanned
      do Comment::locWIPSerialsNotScanned
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnPrint3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::btnPrint3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnExport
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::btnExport
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locWIPSerialsScanned  Routine
  p_web._DivHeader('FormWIPAuditCriteria_' & p_web._nocolon('locWIPSerialsScanned') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Serials Scanned')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locWIPSerialsScanned  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locWIPSerialsScanned',p_web.GetValue('NewValue'))
    locWIPSerialsScanned = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locWIPSerialsScanned
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locWIPSerialsScanned',p_web.GetValue('Value'))
    locWIPSerialsScanned = p_web.GetValue('Value')
  End
  do Value::locWIPSerialsScanned
  do SendAlert
  do Value::btnPrint1  !1
  do Value::btnExport  !1

Value::locWIPSerialsScanned  Routine
  p_web._DivHeader('FormWIPAuditCriteria_' & p_web._nocolon('locWIPSerialsScanned') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locWIPSerialsScanned
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locWIPSerialsScanned'',''formwipauditcriteria_locwipserialsscanned_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locWIPSerialsScanned')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locWIPSerialsScanned') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locWIPSerialsScanned',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormWIPAuditCriteria_' & p_web._nocolon('locWIPSerialsScanned') & '_value')

Comment::locWIPSerialsScanned  Routine
    loc:comment = ''
  p_web._DivHeader('FormWIPAuditCriteria_' & p_web._nocolon('locWIPSerialsScanned') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnPrint1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnPrint1',p_web.GetValue('NewValue'))
    do Value::btnPrint1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnPrint1  Routine
  p_web._DivHeader('FormWIPAuditCriteria_' & p_web._nocolon('btnPrint1') & '_value',Choose(p_web.GSV('locWIPSerialsScanned') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locWIPSerialsScanned') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnPrint','Print','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('WIPAuditReport?' &'type=scanned')) & ''','''&clip('_blank')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormWIPAuditCriteria_' & p_web._nocolon('btnPrint1') & '_value')

Comment::btnPrint1  Routine
    loc:comment = ''
  p_web._DivHeader('FormWIPAuditCriteria_' & p_web._nocolon('btnPrint1') & '_comment',Choose(p_web.GSV('locWIPSerialsScanned') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locWIPSerialsScanned') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locWIPsSerialScannedLocation  Routine
  p_web._DivHeader('FormWIPAuditCriteria_' & p_web._nocolon('locWIPsSerialScannedLocation') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Serials Scanned But Not In Location')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locWIPsSerialScannedLocation  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locWIPsSerialScannedLocation',p_web.GetValue('NewValue'))
    locWIPSerialsScannedLocation = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locWIPsSerialScannedLocation
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locWIPSerialsScannedLocation',p_web.GetValue('Value'))
    locWIPSerialsScannedLocation = p_web.GetValue('Value')
  End
  do Value::locWIPsSerialScannedLocation
  do SendAlert
  do Value::btnExport  !1
  do Value::btnPrint2  !1

Value::locWIPsSerialScannedLocation  Routine
  p_web._DivHeader('FormWIPAuditCriteria_' & p_web._nocolon('locWIPsSerialScannedLocation') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locWIPSerialsScannedLocation
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locWIPsSerialScannedLocation'',''formwipauditcriteria_locwipsserialscannedlocation_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locWIPSerialsScannedLocation')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locWIPSerialsScannedLocation') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locWIPSerialsScannedLocation',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormWIPAuditCriteria_' & p_web._nocolon('locWIPsSerialScannedLocation') & '_value')

Comment::locWIPsSerialScannedLocation  Routine
    loc:comment = ''
  p_web._DivHeader('FormWIPAuditCriteria_' & p_web._nocolon('locWIPsSerialScannedLocation') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnPrint2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnPrint2',p_web.GetValue('NewValue'))
    do Value::btnPrint2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnPrint2  Routine
  p_web._DivHeader('FormWIPAuditCriteria_' & p_web._nocolon('btnPrint2') & '_value',Choose(p_web.GSV('locWIPSerialsScannedLocation') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locWIPSerialsScannedLocation') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnPrint2','Print','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('WIPAuditReport?' &'type=scannedlocation')) & ''','''&clip('_blank')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormWIPAuditCriteria_' & p_web._nocolon('btnPrint2') & '_value')

Comment::btnPrint2  Routine
    loc:comment = ''
  p_web._DivHeader('FormWIPAuditCriteria_' & p_web._nocolon('btnPrint2') & '_comment',Choose(p_web.GSV('locWIPSerialsScannedLocation') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locWIPSerialsScannedLocation') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locWIPSerialsNotScanned  Routine
  p_web._DivHeader('FormWIPAuditCriteria_' & p_web._nocolon('locWIPSerialsNotScanned') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Serials Not Scanned')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locWIPSerialsNotScanned  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locWIPSerialsNotScanned',p_web.GetValue('NewValue'))
    locWIPSerialsNotScanned = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locWIPSerialsNotScanned
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locWIPSerialsNotScanned',p_web.GetValue('Value'))
    locWIPSerialsNotScanned = p_web.GetValue('Value')
  End
  do Value::locWIPSerialsNotScanned
  do SendAlert
  do Value::btnExport  !1
  do Value::btnPrint3  !1

Value::locWIPSerialsNotScanned  Routine
  p_web._DivHeader('FormWIPAuditCriteria_' & p_web._nocolon('locWIPSerialsNotScanned') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locWIPSerialsNotScanned
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locWIPSerialsNotScanned'',''formwipauditcriteria_locwipserialsnotscanned_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locWIPSerialsNotScanned')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locWIPSerialsNotScanned') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locWIPSerialsNotScanned',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormWIPAuditCriteria_' & p_web._nocolon('locWIPSerialsNotScanned') & '_value')

Comment::locWIPSerialsNotScanned  Routine
    loc:comment = ''
  p_web._DivHeader('FormWIPAuditCriteria_' & p_web._nocolon('locWIPSerialsNotScanned') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnPrint3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnPrint3',p_web.GetValue('NewValue'))
    do Value::btnPrint3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnPrint3  Routine
  p_web._DivHeader('FormWIPAuditCriteria_' & p_web._nocolon('btnPrint3') & '_value',Choose(p_web.GSV('locWIPSerialsNotScanned') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locWIPSerialsNotScanned') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnPrint3','Print','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('WIPAuditReport_NotScanned')) & ''','''&clip('_blank')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormWIPAuditCriteria_' & p_web._nocolon('btnPrint3') & '_value')

Comment::btnPrint3  Routine
    loc:comment = ''
  p_web._DivHeader('FormWIPAuditCriteria_' & p_web._nocolon('btnPrint3') & '_comment',Choose(p_web.GSV('locWIPSerialsNotScanned') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locWIPSerialsNotScanned') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnExport  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnExport',p_web.GetValue('NewValue'))
    do Value::btnExport
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnExport  Routine
  p_web._DivHeader('FormWIPAuditCriteria_' & p_web._nocolon('btnExport') & '_value',Choose(p_web.GSV('locWIPSerialsScannedLocation') <> 1 AND p_web.GSV('locWIPSerialsNotScanned') <> 1 AND p_web.GSV('locWIPSerialsScanned') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locWIPSerialsScannedLocation') <> 1 AND p_web.GSV('locWIPSerialsNotScanned') <> 1 AND p_web.GSV('locWIPSerialsScanned') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnExport','Export','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PageProcess?' &'ProcessType=WIPAuditExport')) & ''','''&clip('_blank')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormWIPAuditCriteria_' & p_web._nocolon('btnExport') & '_value')

Comment::btnExport  Routine
    loc:comment = ''
  p_web._DivHeader('FormWIPAuditCriteria_' & p_web._nocolon('btnExport') & '_comment',Choose(p_web.GSV('locWIPSerialsScannedLocation') <> 1 AND p_web.GSV('locWIPSerialsNotScanned') <> 1 AND p_web.GSV('locWIPSerialsScanned') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locWIPSerialsScannedLocation') <> 1 AND p_web.GSV('locWIPSerialsNotScanned') <> 1 AND p_web.GSV('locWIPSerialsScanned') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormWIPAuditCriteria_locWIPSerialsScanned_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locWIPSerialsScanned
      else
        do Value::locWIPSerialsScanned
      end
  of lower('FormWIPAuditCriteria_locWIPsSerialScannedLocation_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locWIPsSerialScannedLocation
      else
        do Value::locWIPsSerialScannedLocation
      end
  of lower('FormWIPAuditCriteria_locWIPSerialsNotScanned_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locWIPSerialsNotScanned
      else
        do Value::locWIPSerialsNotScanned
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormWIPAuditCriteria_form:ready_',1)
  p_web.SetSessionValue('FormWIPAuditCriteria_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormWIPAuditCriteria',0)

PreCopy  Routine
  p_web.SetValue('FormWIPAuditCriteria_form:ready_',1)
  p_web.SetSessionValue('FormWIPAuditCriteria_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormWIPAuditCriteria',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormWIPAuditCriteria_form:ready_',1)
  p_web.SetSessionValue('FormWIPAuditCriteria_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormWIPAuditCriteria:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormWIPAuditCriteria_form:ready_',1)
  p_web.SetSessionValue('FormWIPAuditCriteria_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormWIPAuditCriteria:Primed',0)
  p_web.setsessionvalue('showtab_FormWIPAuditCriteria',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
          If p_web.IfExistsValue('locWIPSerialsScanned') = 0
            p_web.SetValue('locWIPSerialsScanned',0)
            locWIPSerialsScanned = 0
          End
          If p_web.IfExistsValue('locWIPSerialsScannedLocation') = 0
            p_web.SetValue('locWIPSerialsScannedLocation',0)
            locWIPSerialsScannedLocation = 0
          End
          If p_web.IfExistsValue('locWIPSerialsNotScanned') = 0
            p_web.SetValue('locWIPSerialsNotScanned',0)
            locWIPSerialsNotScanned = 0
          End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormWIPAuditCriteria_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormWIPAuditCriteria_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormWIPAuditCriteria:Primed',0)
  p_web.StoreValue('locWIPSerialsScanned')
  p_web.StoreValue('')
  p_web.StoreValue('locWIPSerialsScannedLocation')
  p_web.StoreValue('')
  p_web.StoreValue('locWIPSerialsNotScanned')
  p_web.StoreValue('')
  p_web.StoreValue('')
