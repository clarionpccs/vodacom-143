

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER360.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER361.INC'),ONCE        !Req'd for module callout resolution
                     END


RapEngAllocateJob    PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
locOldEngineer       STRING(30)                            !
Ans                  LONG                                  !
locJobNumber         STRING(30)                            !
locPassword          STRING(30)                            !
FilesOpened     Long
JOBSENG::State  USHORT
USERS::State  USHORT
WEBJOB::State  USHORT
JOBSE::State  USHORT
CONTHIST::State  USHORT
JOBS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('RapEngAllocateJob')
  loc:formname = 'RapEngAllocateJob_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('RapEngAllocateJob','')
    p_web._DivHeader('RapEngAllocateJob',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferRapEngAllocateJob',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferRapEngAllocateJob',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferRapEngAllocateJob',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_RapEngAllocateJob',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferRapEngAllocateJob',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_RapEngAllocateJob',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(JOBSENG)
  p_web._OpenFile(USERS)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(CONTHIST)
  p_web._OpenFile(JOBS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBSENG)
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(CONTHIST)
  p_Web._CloseFile(JOBS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('RapEngAllocateJob_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locPassword',locPassword)
  p_web.SetSessionValue('locJobNumber',locJobNumber)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locPassword')
    locPassword = p_web.GetValue('locPassword')
    p_web.SetSessionValue('locPassword',locPassword)
  End
  if p_web.IfExistsValue('locJobNumber')
    locJobNumber = p_web.GetValue('locJobNumber')
    p_web.SetSessionValue('locJobNumber',locJobNumber)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('RapEngAllocateJob_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    p_web.SSV('locPassword','')
    p_web.SSV('locJobNumber','')
      p_web.site.SaveButton.TextValue = 'Allocate'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locPassword = p_web.RestoreValue('locPassword')
 locJobNumber = p_web.RestoreValue('locJobNumber')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('NextURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('RapEngAllocateJob_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('RapEngAllocateJob_ChainTo')
    loc:formaction = p_web.GetSessionValue('RapEngAllocateJob_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'RapidEngineerUpdate'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="RapEngAllocateJob" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="RapEngAllocateJob" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="RapEngAllocateJob" ></input><13,10>'
  end

  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_RapEngAllocateJob">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_RapEngAllocateJob" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_RapEngAllocateJob')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Allocate Job') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_RapEngAllocateJob')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_RapEngAllocateJob'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locPassword')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_RapEngAllocateJob')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Allocate Job') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_RapEngAllocateJob_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Allocate Job')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Allocate Job')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Allocate Job')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Allocate Job')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locPassword
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locJobNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locJobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locJobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locPassword  Routine
  p_web._DivHeader('RapEngAllocateJob_' & p_web._nocolon('locPassword') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Engineer Password')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locPassword  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locPassword',p_web.GetValue('NewValue'))
    locPassword = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locPassword
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locPassword',p_web.GetValue('Value'))
    locPassword = p_web.GetValue('Value')
  End
  If locPassword = ''
    loc:Invalid = 'locPassword'
    loc:alert = p_web.translate('Engineer Password') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locPassword = Upper(locPassword)
    p_web.SetSessionValue('locPassword',locPassword)
    IF (p_web.GSV('locPassword') <> p_web.GSV('BookingUserPassword'))
        loc:invalid = 'locPassword'
        loc:alert = 'Invalid Password'
        p_web.SSV('locPassword','')
    END ! IF
    
  do Value::locPassword
  do SendAlert
  do Value::locJobNumber  !1

Value::locPassword  Routine
  p_web._DivHeader('RapEngAllocateJob_' & p_web._nocolon('locPassword') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locPassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locPassword')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locPassword = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locPassword'',''rapengallocatejob_locpassword_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locPassword')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','locPassword',p_web.GetSessionValueFormat('locPassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('RapEngAllocateJob_' & p_web._nocolon('locPassword') & '_value')

Comment::locPassword  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('RapEngAllocateJob_' & p_web._nocolon('locPassword') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locJobNumber  Routine
  p_web._DivHeader('RapEngAllocateJob_' & p_web._nocolon('locJobNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Job Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locJobNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locJobNumber',p_web.GetValue('NewValue'))
    locJobNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locJobNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locJobNumber',p_web.GetValue('Value'))
    locJobNumber = p_web.GetValue('Value')
  End
  If locJobNumber = ''
    loc:Invalid = 'locJobNumber'
    loc:alert = p_web.translate('Job Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    Access:JOBS.ClearKey(job:Ref_Number_Key)
    job:Ref_Number = p_web.GSV('locJobNumber')
    IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
        IF (job:Engineer <> '' AND job:Engineer <> p_web.GSV('BookingUserCode'))
            loc:alert = 'Warning! This job is allocated to another engineer.<13,10,13,10>Click ''Allocate'' to allocate to yourself.'
        ELSE
            IF (job:Engineer = p_web.GSV('BookingUserCode'))
                loc:alert = 'Note! You are already allocated to this job.<13,10,13,10>Click ''Allocate'' to re-allocate this job to yourself.'
            END ! IF
        END ! IF
    ELSE
        loc:invalid = 'locJobNumber'
        loc:alert = 'Invalid Job Number'
    END ! IF
    
  do Value::locJobNumber
  do SendAlert

Value::locJobNumber  Routine
  p_web._DivHeader('RapEngAllocateJob_' & p_web._nocolon('locJobNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locJobNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('locPassword') = '','readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('locPassword') = ''
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locJobNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locJobNumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locJobNumber'',''rapengallocatejob_locjobnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locJobNumber',p_web.GetSessionValueFormat('locJobNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('RapEngAllocateJob_' & p_web._nocolon('locJobNumber') & '_value')

Comment::locJobNumber  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('RapEngAllocateJob_' & p_web._nocolon('locJobNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('RapEngAllocateJob_locPassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locPassword
      else
        do Value::locPassword
      end
  of lower('RapEngAllocateJob_locJobNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locJobNumber
      else
        do Value::locJobNumber
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('RapEngAllocateJob_form:ready_',1)
  p_web.SetSessionValue('RapEngAllocateJob_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_RapEngAllocateJob',0)

PreCopy  Routine
  p_web.SetValue('RapEngAllocateJob_form:ready_',1)
  p_web.SetSessionValue('RapEngAllocateJob_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_RapEngAllocateJob',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('RapEngAllocateJob_form:ready_',1)
  p_web.SetSessionValue('RapEngAllocateJob_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('RapEngAllocateJob:Primed',0)

PreDelete       Routine
  p_web.SetValue('RapEngAllocateJob_form:ready_',1)
  p_web.SetSessionValue('RapEngAllocateJob_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('RapEngAllocateJob:Primed',0)
  p_web.setsessionvalue('showtab_RapEngAllocateJob',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('RapEngAllocateJob_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('RapEngAllocateJob_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
        If locPassword = ''
          loc:Invalid = 'locPassword'
          loc:alert = p_web.translate('Engineer Password') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locPassword = Upper(locPassword)
          p_web.SetSessionValue('locPassword',locPassword)
        If loc:Invalid <> '' then exit.
        If locJobNumber = ''
          loc:Invalid = 'locJobNumber'
          loc:alert = p_web.translate('Job Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
  ! The following fields are not on the form, but need to be checked anyway.
    p_web.SSV('NextURL','RapidEngineerUpdate')
    
    Access:JOBS.ClearKey(job:Ref_Number_Key)
    job:Ref_Number = p_web.GSV('locJobNumber')
    IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
        loc:Invalid = 'locJobNumber'
        loc:Alert = 'Unable to find selected Job Number'
        EXIT
    ELSE ! IF        
        ! Allocate Job
        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = job:Ref_Number
        IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
        END ! IF
        
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
        END ! IF
        
        p_web.FileToSessionQueue(JOBS)
        p_web.FileToSessionQueue(WEBJOB)
        p_web.FileToSessionQueue(JOBSE)
        
        GetStatus('310',1,'JOB',p_web)
  
        locOldEngineer = job:Engineer
        job:Engineer = p_web.GSV('BookingUserCode')
        IF (Access:JOBS.TryUpdate() = Level:Benign)
            Access:USERS.ClearKey(use:User_Code_Key)
            use:User_Code = p_web.GSV('BookingUserCode')
            IF (Access:USERS.TryFetch(use:User_Code_Key))
            END ! IF
            
            AddToAudit(p_web,job:Ref_Number,'JOB','RAPID ENGINEER UPDATE: JOB REALLOCATED','ENGINEER: ' & Clip(use:Forename) & ' ' & Clip(use:Surname))    
            
            IF (p_web.GSV('BookingSite') = 'RRC' AND job:Date_Completed <> '' AND JobSentToARC(p_web))
                ! The engineer is being changed at the RRC after the job has come back from RRC
                If Access:JOBSENG.PrimeRecord() = Level:Benign
                    joe:JobNumber     = job:Ref_Number
                    joe:UserCode      = job:Engineer
                    joe:DateAllocated = Today()
                    joe:TimeAllocated = Clock()
                    joe:AllocatedBy   = use:User_Code
                    joe:Status        = 'ENGINEER QA'
                    joe:StatusDate    = Today()
                    joe:StatusTime    = Clock()
                    joe:EngSkillLevel = use:SkillLevel
                    joe:JobSkillLevel = jobe:SkillLevel
  
                    If Access:JOBSENG.TryInsert() = Level:Benign
                            !Insert Successful
                    Else !If Access:JOBSENG.TryInsert() = Level:Benign
                            !Insert Failed
                    End !If Access:JOBSENG.TryInsert() = Level:Benign
                End !If Access:JOBSENG.PrimeRecord() = Level:Benign
            ELSE ! IF
                IF (locOldEngineer <> '')
                    Access:JOBSENG.ClearKey(joe:UserCodeKey)
                    joe:JobNumber     = job:Ref_Number
                    joe:UserCode      = locOldEngineer
                    joe:DateAllocated = Today()
                    Set(joe:UserCodeKey,joe:UserCodeKey)
                    Loop
                        If Access:JOBSENG.PREVIOUS()
                            Break
                        End !If
                        If joe:JobNumber     <> job:Ref_Number       |
                            Or joe:UserCode      <> locOldEngineer      |
                            Or joe:DateAllocated > Today()       |
                            Then Break.  ! End If
                        If joe:Status = 'ALLOCATED'
                            joe:Status = 'ESCALATED'
                            joe:StatusDate = Today()
                            joe:StatusTime = Clock()
                            Access:JOBSENG.Update()
                        End !If joe:Status = 'ALLOCATED'
                        Break
                    End !Loop
                END ! IF
                
                If Access:JOBSENG.PrimeRecord() = Level:Benign
                    joe:JobNumber     = job:Ref_Number
                    joe:UserCode      = job:Engineer
                    joe:DateAllocated = Today()
                    joe:TimeAllocated = Clock()
                    joe:AllocatedBy   = use:User_Code
                    joe:Status        = 'ALLOCATED'
                    joe:StatusDate    = Today()
                    joe:StatusTime    = Clock()
                    joe:EngSkillLevel = use:SkillLevel
                    joe:JobSkillLevel = jobe:SkillLevel
  
                    If Access:JOBSENG.TryInsert() = Level:Benign
                            !Insert Successful
                    Else !If Access:JOBSENG.TryInsert() = Level:Benign
                            !Insert Failed
                    End !If Access:JOBSENG.TryInsert() = Level:Benign
                End !If Access:JOBSENG.PrimeRecord() = Level:Benign
                
            END ! IF
            
        END ! IF
       
        !TB12540 - Show contact history if there is a sticky note
        !need to find one that has not been cancelled, and is valid for this request
        Access:CONTHIST.ClearKey(cht:KeyRefSticky)
        cht:Ref_Number = job:Ref_Number
        cht:SN_StickyNote = 'Y'
        SET(cht:KeyRefSticky,cht:KeyRefSticky)      
        LOOP UNTIL Access:CONTHIST.Next() <> Level:Benign
            IF (cht:Ref_Number <> job:Ref_Number OR | 
                cht:SN_StickyNote <> 'Y')
                BREAK
            END ! IF
  
            IF cht:SN_Completed <> 'Y' AND cht:SN_EngAlloc = 'Y'
                p_web.SSV('NextURL','BrowseContactHistory?type=RapidEngineerUpdate')
                BREAK
            END
        END
        
    END ! IF  
    packet = CLIP(packet) & '<script>window.open("' & p_web.GSV('NextURL') & '","_self");</script>'
    DO SendPacket
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('RapEngAllocateJob:Primed',0)
  p_web.StoreValue('locPassword')
  p_web.StoreValue('locJobNumber')
SaveSessionVars  ROUTINE
     p_web.SSV('locOldEngineer',locOldEngineer) ! STRING(30)
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('locJobNumber',locJobNumber) ! STRING(30)
     p_web.SSV('locPassword',locPassword) ! STRING(30)
RestoreSessionVars  ROUTINE
     locOldEngineer = p_web.GSV('locOldEngineer') ! STRING(30)
     Ans = p_web.GSV('Ans') ! LONG
     locJobNumber = p_web.GSV('locJobNumber') ! STRING(30)
     locPassword = p_web.GSV('locPassword') ! STRING(30)
