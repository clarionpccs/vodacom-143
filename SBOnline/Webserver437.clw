

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('WEBSERVER437.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! Loan Order
!!! </summary>
LoanNewOrder PROCEDURE (<NetWebServerWorker p_web>)

  ! The NetTalk Extension to ABC report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
locRecordNumber      LONG                                  !
Address:User_Name    STRING(30)                            !
address:Address_Line1 STRING(30)                           !
address:Address_Line2 STRING(30)                           !
address:Address_Line3 STRING(30)                           !
address:Post_Code    STRING(20)                            !
address:Telephone_Number STRING(20)                        !
address:Fax_No       STRING(20)                            !
address:Email        STRING(50)                            !
tmp:DOP              DATE                                  !
locOrderNumber       LONG                                  !
locOrderDate         DATE                                  !
locExchangeCost      REAL                                  !
locTotalQty          LONG                                  !
Process:View         VIEW(LOAORDR)
                       PROJECT(lor:Manufacturer)
                       PROJECT(lor:Model_Number)
                       PROJECT(lor:OrderNumber)
                       PROJECT(lor:Qty_Required)
                     END
ProgressWindow       WINDOW('Report EXCHOR48'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE,CENTER, |
  GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT,AT(385,2865,7521,7344),PRE(RPT),PAPER(PAPER:A4),FONT('Tahoma',10,,FONT:regular),THOUS
                       HEADER,AT(385,531,7521,1802),USE(?unnamed:2)
                         STRING(@s30),AT(52,646),USE(address:Address_Line3),FONT('Tahoma',8,,,CHARSET:ANSI),TRN
                         STRING(@s20),AT(52,833),USE(address:Post_Code),FONT('Tahoma',8,,,CHARSET:ANSI),TRN
                         STRING('Date Printed:'),AT(4333,958),USE(?String16),FONT('Tahoma',8,,,CHARSET:ANSI),TRN
                         STRING('Email:'),AT(52,1302),USE(?String33),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI), |
  TRN
                         STRING(@s50),AT(365,1302),USE(address:Email),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI), |
  TRN
                         STRING('<<-- Date Stamp -->'),AT(5292,958,1667),USE(?ReportDateStamp),FONT('Tahoma',8,,FONT:bold), |
  RIGHT,TRN
                         STRING(@s20),AT(365,990),USE(address:Telephone_Number),FONT('Tahoma',8,,,CHARSET:ANSI),TRN
                         STRING('Fax:'),AT(52,1146),USE(?String34),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI), |
  TRN
                         STRING(@s20),AT(365,1146),USE(address:Fax_No),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI), |
  TRN
                         STRING('Tel:'),AT(52,990),USE(?String32),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI), |
  TRN
                         STRING(@s30),AT(52,271),USE(address:Address_Line1),FONT('Tahoma',8,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(52,458),USE(address:Address_Line2),FONT('Tahoma',8,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(52,0),USE(Address:User_Name),FONT('Tahoma',14,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING('Order No / Date:'),AT(4333,625,917,167),USE(?String16:2),FONT('Tahoma',8,,,CHARSET:ANSI), |
  TRN
                         STRING(@n_6),AT(5208,625,667,167),USE(locRecordNumber),FONT('Tahoma',8,,FONT:bold),RIGHT,TRN
                         STRING(@d06),AT(5958,625,1000,167),USE(locOrderDate),FONT('Tahoma',8,,FONT:bold),RIGHT,TRN
                       END
detail1                DETAIL,AT(0,0,,167),USE(?unnamed:4)
                         STRING(@s30),AT(104,0),USE(lor:Manufacturer),FONT('Tahoma',8,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(2135,0,,208),USE(lor:Model_Number),FONT('Tahoma',8),TRN
                         STRING(@n12.2),AT(6333,0),USE(locExchangeCost),FONT(,8),RIGHT,TRN
                         STRING(@s8),AT(4083,0,583,167),USE(lor:Qty_Required),FONT('Tahoma',8),RIGHT,TRN
                       END
                       FOOTER,AT(396,10833,7521,302),USE(?unnamed:3)
                         LINE,AT(104,52,2146,0),USE(?Line3),COLOR(COLOR:Black)
                         STRING('Total Quantity Ordered: '),AT(104,104),USE(?String29),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING(@n_9),AT(1583,104,1000,167),USE(locTotalQty),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI), |
  TRN
                       END
                       FORM,AT(365,510,7521,10802),USE(?unnamed)
                         BOX,AT(104,2292,7344,7448),USE(?Box2),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                         BOX,AT(104,1875,7344,365),USE(?Box1),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1),ROUND
                         BOX,AT(4167,365,3229,1406),USE(?Box3),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                         STRING('LOANS ORDERED'),AT(5417,42),USE(?String3),FONT('Tahoma',16,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING('Quantity'),AT(4198,1979),USE(?String5),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),TRN
                         STRING('Price'),AT(6833,1979),USE(?strPrice),FONT(,8,,FONT:bold),TRN
                         STRING('Manufacturer'),AT(156,1979),USE(?String6),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING('Model Number'),AT(2167,1979),USE(?String31),FONT('Tahoma',8,,FONT:bold),TRN
                       END
                     END
!*** PDF-Tools v4.1 Start (All)
!--- ClarioNET NOT Enabled
!--- PDFXTABCTemplate: True  Level: 4
!--- PDFXTABCTemplate: RPM: False
!--- PDFXTABCTemplate: Icetips: False
PDFXTR1               PDFXToolsReportClass                 !  Added by PDF-Tools Templates v4.1
PDFXTR1:rtn           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXTR1:vpf           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Orientation      BYTE(0)                      !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:TopMargin        REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:BottomMargin     REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:LeftMargin       REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:RightMargin      REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Selection        STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PDFXT4:Papers:Queue           QUEUE,PRE()                  !  Added by PDF-Tools Templates v4.1
PLQ:Description                 STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PLQ:PaperCode                   BYTE                       !  Added by PDF-Tools Templates v4.1
                              END                          !  Added by PDF-Tools Templates v4.1
!*** PDF-Tools v4.1 End
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('LoanNewOrder')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:EXCHORNO.SetOpenRelated()
  Relate:EXCHORNO.Open                                     ! File EXCHORNO used by this procedure, so make sure it's RelationManager is open
  Relate:LOANORNO.SetOpenRelated()
  Relate:LOANORNO.Open                                     ! File LOANORNO used by this procedure, so make sure it's RelationManager is open
  Relate:MODELNUM.SetOpenRelated()
  Relate:MODELNUM.Open                                     ! File MODELNUM used by this procedure, so make sure it's RelationManager is open
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  tra:Account_Number  = p_web.GSV('BookingAccount')
  IF Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
     !Found
     address:User_Name = tra:company_name
     address:Address_Line1 = tra:address_line1
     address:Address_Line2 = tra:address_line2
     address:Address_Line3 = tra:address_line3
     address:Post_code = tra:postcode
     address:Telephone_Number = tra:telephone_number
     address:Fax_No = tra:Fax_Number
     address:Email = tra:EmailAddress
  ELSE ! If Access:TRADACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Error
  END !If Access:TRADACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
        locRecordNumber = p_web.GSV('lno:RecordNumber')
        locOrderDate = p_web.GSV('lno:DateCreated')
  
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('LoanNewOrder',ProgressWindow)              ! Restore window settings from non-volatile store
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    loc:pdfname = '.\$$$' & format(random(1,99999),@n05) &'.pdf'
    ProgressWindow{prop:hide} = 1
  End
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:LOAORDR, ?Progress:PctText, Progress:Thermometer, ProgressMgr, lor:OrderNumber)
  ThisReport.AddSortOrder(lor:OrderNumberOnlyKey)
  ThisReport.AddRange(lor:OrderNumber,locRecordNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:LOAORDR.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      Loc:html = '<script type="text/javascript">alert('''&clip('No Records')&''');top.close();</script>'
      p_web.ParseHTML(loc:html)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHORNO.Close
    Relate:LOANORNO.Close
    Relate:MODELNUM.Close
  END
  IF SELF.Opened
    INIMgr.Update('LoanNewOrder',ProgressWindow)           ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.OpenReport()
  !*** PDF-Tools v4.1 Start (ABC)
  IF ~ReturnValue                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.Init(Report, loc:PDFName,,PDFA_OFF)            !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPaper(CHOOSE(SYSTEM{PROP:Autopaper}=True,Report{PROPPRINT:Paper},PAPER:USER), CHOOSE(Report{PROP:Thous}=True,PROP:Thous,CHOOSE(Report{PROP:MM}=True,PROP:MM,CHOOSE(Report{PROP:Points}=True,PROP:Points,0))), Report{PROPPRINT:PaperWidth}, Report{PROPPRINT:PaperHeight}, Report{PROP:Landscape}) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetCompression(TRUE,TRUE,FALSE,COMPRTYPE_C_AUTO,75,COMPRTYPE_I_AUTO,COMPRTYPE_M_AUTO) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetAuthor('ServiceBase 3g')          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetTitle('Exchange Order')           !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetSubject('ServiceBase 3g')         !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPdfCreator('ServiceBase 3g')      !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetKeywords()                        !  Added by PDF-Tools Templates v4.1
    PDFXTR1.EnableLinkAnalyzer(True)             !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetSpecVersion(SPECVERSION_15,TRUE)  !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPageLayout(PAGELAYOUT_SINGLEPAGE) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPageMode(PAGEMODE_NONE)           !  Added by PDF-Tools Templates v4.1
    IF PAGEMODE_NONE = PAGEMODE_FULLSCREEN       !  Added by PDF-Tools Templates v4.1
      PDFXTR1:vpf = VP_FSPM_NONE                 !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
      PDFXTR1:vpf = 0                            !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDETOOLBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDEMENUBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDEWINDOWUI). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_FITWINDOW). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_CENTERWINDOW). !  Added by PDF-Tools Templates v4.1
      IF TRUE                                     THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_DISPLAYDOCTITLE). !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetViewerPrefs(PDFXTR1:vpf)          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPreview(False)                    !  Added by PDF-Tools Templates v4.1
  END
  !*** PDF-Tools v4.1 End
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@d06)
  END
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeCloseEvent()
  !*** PDF-Tools v4.1 Start (ABC)
  PDFXTR1.Kill()
  !*** PDF-Tools v4.1 End
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
        Access:MODELNUM.ClearKey(mod:Manufacturer_Key)
        mod:Manufacturer = lor:Manufacturer
        mod:Model_Number = lor:Model_Number
        IF (Access:MODELNUM.TryFetch(mod:Manufacturer_Key) = Level:Benign)
            locExchangeCost = mod:LoanReplacementValue * lor:Qty_Required
        ELSE ! IF
            locExchangeCost = 0
        END ! IF
  locTotalQty += lor:Qty_Required
  PRINT(RPT:detail1)
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  !*** PDF-Tools v4.1 Start (ABC)
  ReturnValue = False                            !  Added by PDF-Tools Templates v4.1
    PDFXTR1:rtn = PDFXTR1.Generate(SELF.ImageQueue,True, , ) !  Added by PDF-Tools Templates v4.1
    IF PDFXTR1:rtn THEN                          !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
  RETURN ReturnValue
  !*** PDF-Tools v4.1 End
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
  RETURN ReturnValue

