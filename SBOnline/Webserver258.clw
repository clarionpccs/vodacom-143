

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER258.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
SetLoginDefaults     PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)

  CODE
!region Processed Code    
    Do OpenFiles

    Access:USERS.Clearkey(use:Password_Key)
    use:Password = p_web.GSV('loc:Password')
    IF (Access:USERS.Tryfetch(use:Password_Key) = Level:Benign)
        Access:TRADEACC.Clearkey(tra:SiteLocationKey)
        tra:SiteLocation = use:Location
        IF (Access:TRADEACC.Tryfetch(tra:SiteLocationKey) = Level:Benign)
            p_web.SSV('BookingBranchID',tra:BranchIdentification)
            p_web.SSV('BookingAccount',Clip(tra:Account_Number))
            p_web.SSV('BookingName',Clip(DoCaps(tra:Company_Name)))
            p_web.SSV('BookingUserPassword',use:Password)
            p_web.SSV('BookingUserCode',use:User_Code)
            p_web.SSV('BookingSiteLocation',tra:SiteLocation)
            p_web.SSV('BookingRestrictParts',use:RestrictParts)
            p_web.SSV('BookingRestrictChargeable',use:RestrictChargeable)
            p_web.SSV('BookingSkillLevel',use:SkillLevel)
            p_web.SSV('BookingStoresAccount',tra:StoresAccount)
            If tra:Account_Number = GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI')
                p_web.SSV('BookingSite','ARC')
                !p_web.SSV('Filter:BrowseJobs','Upper(job:Location) <> <39,39> And Upper(job:ESN) <> <39,39>')
                p_web.SSV('Filter:BrowseJobs','')

            Else ! If tra:Account_Number = GETINI('BOOKING','HeadAccountNumber',,Clip(Path()) & '\SB2KDEF.INI')
                p_web.SSV('BookingSite','RRC')
                !p_web.SSV('Filter:BrowseJobs','Upper(wob:HeadAccountNumber) = Upper(<39>' & clip(tra:Account_Number) & '<39>)  And Upper(job:Location) <> <39,39> And Upper(job:ESN) <> <39,39>')
                p_web.SSV('Filter:BrowseJobs','Upper(wob:HeadAccountNumber) = Upper(<39>' & clip(tra:Account_Number) & '<39>)')
            End ! If tra:Account_Number = GETINI('BOOKING','HeadAccountNumber',,Clip(Path()) & '\SB2KDEF.INI')
            ! Set Some Defaults
            p_web.SSV('Default:InTransitPUP',GETINI('RRC','InTransitToPUPLocation',,Path() & '\SB2KDEF.INI'))
            p_web.SSV('Default:PUPLocation',GETINI('RRC','AtPUPLocation',,Path() & '\SB2KDEF.INI'))
            p_web.SSV('Default:ARCLocation',GETINI('RRC','ARCLocation',,CLIP(PATH())&'\SB2KDEF.INI'))
            p_web.SSV('Default:StatusSendToRRC',GETINI('RRC','StatusSendToRRC',,CLIP(PATH())&'\SB2KDEF.INI'))
            p_web.SSV('Default:DespatchToCustomer',GETINI('RRC','DespatchToCustomer',,CLIP(PATH())&'\SB2KDEF.INI'))
            p_web.SSV('Default:InTransitARC',GETINI('RRC','InTransit',,CLIP(PATH())&'\SB2KDEF.INI'))
            p_web.SSV('Default:RRCLocation',GETINI('RRC','RRCLocation',,CLIP(PATH())&'\SB2KDEF.INI'))
            p_web.SSV('Default:StatusSendToARC',GETINI('RRC','StatusSendToARC',,CLIP(PATH())&'\SB2KDEF.INI'))
            p_web.SSV('Default:InTransitRRC',GETINI('RRC','InTransitRRC',,CLIP(PATH())&'\SB2KDEF.INI'))
            p_web.SSV('Default:StatusReceivedFromPUP',GETINI('RRC','StatusReceivedFromPUP',,Clip(Path()) & '\SB2KDEF.INI'))
            p_web.SSV('Default:StatusReceivedAtRRC',GETINI('RRC','StatusReceivedAtRRC',,Clip(Path()) & '\SB2KDEF.INI'))
            p_web.SSV('Default:StatusRRCReceivedQuery',GETINI('RRC','StatusRRCReceivedQuery',,Clip(Path()) & '\SB2KDEF.INI'))
            p_web.SSV('Default:ExchangeStatusReceivedAtRRC',GETINI('RRC','ExchangeStatusReceivedAtRRC',,Clip(Path()) & '\SB2KDEF.INI'))
            p_web.SSV('Default:AccountNumber',tra:Account_Number)
            p_web.SSV('Default:SiteLocation',tra:SiteLocation)
            p_web.SSV('Default:ValidationURL',Clip(GETINI('URL','IMEIValidation',,Clip(Path()) & '\SB2KDEF.INI')))

            p_web.SSV('Default:TermsText',GETINI('PRINTING','TermsText',,PATH() & '\SB2KDEF.INI')) ! #12265 Set default for "terms" text (Bryan: 30/08/2011)
            p_web.DeleteSessionValue('loc:Password')
            glo:Password = use:Password

            p_web.SSV('ARC:AccountNumber',GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI'))

            p_web.SSV('LoginDetails1','Site: ' & clip(tra:Company_Name))
            p_web.SSV('LoginDetails2','User: ' & clip(use:forename) & ' ' & clip(use:Surname))

            !TB13153 - Hide buttons according to trade account defaults  (J 17/09/13)
            !first two are in the tradeacc file ...
            if tra:UseSBOnline THEN         !now shows as "Job Booking" on the trade account page
                p_web.SSV('Hide:ButtonCreateNewJob',0)  
            ELSE
                p_web.SSV('Hide:ButtonCreateNewJob',1)      !hides single and multiple jobs and import buttons
            END
            if tra:SBOnlineJobProgress THEN 
                p_web.SSV('Hide:ButtonJobSearch',0)
            ELSE
                p_web.SSV('Hide:ButtonJobSearch',1)         !also hides the print routines
            END

            !Most are in TradeAc2
            Access:Tradeac2.ClearKey(TRA2:KeyAccountNumber)
            TRA2:Account_Number = tra:Account_Number
            if access:tradeac2.Fetch(TRA2:KeyAccountNumber)
                !Assume all these are turned off
                p_web.SSV('Hide:ButtonDespatch',1)
                p_web.ssv('Hide:ButtonStock',1)
                p_web.ssv('Hide:RapidButtons',1)
                p_web.ssv('Hide:WarrClaims',1)
                p_web.ssv('Hide:WayBill',1)
                p_web.ssv('Hide:TradeAccs',1)
                p_web.SSV('Hide:EngUpdate',1) ! #13101
                p_web.SSV('Hide:Audits',1)
                p_web.SSV('Hide:SBOUsers',1)
                p_web.SSV('Hide:SBOLoanExchange',1)
                p_web.SSV('Hide:SBOBouncers',1)
                p_web.SSV('Hide:SBOReports',1)
            ELSE
                !now for all the others ...
                IF TRA2:SBOnlineDespatch THEN
                    p_web.SSV('Hide:ButtonDespatch',0)
                ELSE
                    p_web.SSV('Hide:ButtonDespatch',1)
                END                
                IF TRA2:SBOnlineStock THEN
                    p_web.SSV('Hide:ButtonStock',0)
                ELSE
                    p_web.ssv('Hide:ButtonStock',1)
                END
                IF TRA2:SBOnlineQAEtc THEN
                    p_web.ssv('Hide:RapidButtons',0)
                ELSE
                    p_web.ssv('Hide:RapidButtons',1)
                END
                IF TRA2:SBOnlineWarrClaims THEN
                    p_web.ssv('Hide:WarrClaims',0)
                ELSE
                    p_web.ssv('Hide:WarrClaims',1)
                END
                IF TRA2:SBOnlineWaybills THEN
                    p_web.ssv('Hide:WayBill',0)
                ELSE
                    p_web.ssv('Hide:WayBill',1)
                END
                if TRA2:SBOnlineTradeAccs THEN
                    p_web.ssv('Hide:TradeAccs',0)
                ELSE
                    p_web.ssv('Hide:TradeAccs',1)
                END
                                    
                IF (TRA2:SBOnlineEngineer) ! #13101 Add option for Show/Hide Engineer update (Ph8) (DBH: 22/10/2013)
                    p_web.SSV('Hide:EngUpdate',0)
                ELSE
                    p_web.SSV('Hide:EngUpdate',1)
                END ! IF
                IF (TRA2:SBOnlineAudits) ! #13102 Add option to show/hide audit (Ph9) (DBH: 16/12/2013)
                    p_web.SSV('Hide:Audits',0)
                ELSE
                    p_web.SSV('Hide:Audits',1)
                END ! IF
                ! #13103 New defaults (DBH: 27/01/2014)
                IF (TRA2:SBOnlineUsers)
                    p_web.SSV('Hide:SBOUsers',0)
                ELSE
                    p_web.SSV('Hide:SBOUsers',1)
                END ! IF
                IF (TRA2:SBOnlineLoansExch)
                    p_web.SSV('Hide:SBOLoanExchange',0)
                ELSE
                    p_web.SSV('Hide:SBOLoanExchange',1)
                END ! IF
                IF (TRA2:SBOnlineBouncers)
                    p_web.SSV('Hide:SBOBouncers',0)
                ELSE
                    p_web.SSV('Hide:SBOBouncers',1)
                END ! IF
                IF (TRA2:SBOnlineReports)
                    p_web.SSV('Hide:SBOReports',0)
                ELSE
                    p_web.SSV('Hide:SBOReports',1)
                END ! IF                
            END
            
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number    = GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI')
            if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                ! Found
                p_web.SSV('ARC:SiteLocation',tra:SiteLocation)
            else ! if (Access:TRADE.TryFetch(tra:Account_Number_Key) = Level:Benign)
                ! Error
            end ! if (Access:TRADE.TryFetch(tra:Account_Number_Key) = Level:Benign)

            IF (SecurityCheckFailed(use:Password,'JOBS - INSERT')) ! #11682 Check access. (Bryan: 07/09/2010)
                p_web.SSV('Hide:ButtonCreateNewJob',1)
            ELSE
                !TB13153 - dont change this from what was set above for tra:UseSBOnline (J 17/09/13)
                !p_web.SSV('Hide:ButtonCreateNewJob',0)
            END

            IF (SecurityCheckFailed(use:Password,'JOBS - CHANGE'))
                p_web.SSV('Hide:ButtonJobSearch',1)
            ELSE
                !TB13153 - dont change this from what was set above for tra:SBOnlineJobProgress (J 17/09/13)
                !p_web.SSV('Hide:ButtonJobSearch',0)
            END
            
            ! Set which invoice to print
            IF (GETINI('PRINTING','PrintA5Invoice',,PATH() & '\SB2KDEF.INI') <> 1)
                p_web.SSV('Document:Invoice','vSingleInvoice')
            ELSE ! 
                p_web.SSV('Document:Invoice','InvoiceNote')
            END ! 
        
        END  !IF
    END ! IF

    Do CloseFiles

    p_web.SSV('NewPasswordRequired','')
    p_web.SSV('UserMobileRequired','')
!endregion
!--------------------------------------
OpenFiles  ROUTINE
  Access:TRADEAC2.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEAC2.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:USERS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:USERS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:TRADEAC2.Close
     Access:TRADEACC.Close
     Access:USERS.Close
     FilesOpened = False
  END
