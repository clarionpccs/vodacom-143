/* This document is Copyright 2006 by CapeSoft Software (Pty) Ltd.
   Any copying, reproducing, or editing of this document is prohibited unless
   you have permission from CapeSoft.
   This script is shipped as part of the NetTalk 4 Product Suite.
   www.capesoft.com
*/

function enableButtons(f,s,c,d) {
 // alert('s: ' + s + '; c: ' + c + '; d: ' + d);
  if(s==1) {
    f.Select_btn.disabled = false;
  }
  if(c==1) {
    f.Change_btn.disabled = false;
  }
  if(d==1) {
    f.Deleteb_btn.disabled = false;
  }
}

function chooseAction(mes,frm,atru,afal) {
  if (confirm(mes) == true) {
    if (frm){
      frm.action = atru;
    }
    return true;
  } else {
    if (afal){
      if (frm){
        frm.action = afal;
      }
      return true;
    }
    return false;
  }
}

function prepareDelete(f,p) {
  if (confirm('Are you sure you want to delete this record?') == true) {
    f.action = p;
	return true;
  } else {
    return false;
  }
}

function reloadFrame(f,r) {
  if(r.indexOf('left.htm') > 0) {
  } else {
    f.location.reload(true);
  }
}

function GreenBar(table) {
  for (var i = 0; i < table.tBodies[0].rows.length; i++) {
    if (table.tBodies[0].rows[i].cells[0].innerHTML != '')
      table.tBodies[0].rows[i].bgColor = '#BBEEFF';
    else {
      if (i%2 ==0)
        table.tBodies[0].rows[i].bgColor = '#FFFFFF'
      else
        table.tBodies[0].rows[i].bgColor = '#dfe3ee';
    }
  }
}

function ClickRow(table,idd,hf) {
  for (var i = 0; i < table.tBodies[0].rows.length; i++) {
    if (table.tBodies[0].rows[i]==idd){
      table.tBodies[0].rows[i].bgColor = '#BBEEFF';
      table.tBodies[0].rows[i].cells[0].innerHTML = hf;
    } else {
      table.tBodies[0].rows[i].cells[0].innerHTML = '';
      if (i%2 ==0)
        table.tBodies[0].rows[i].bgColor = '#FFFFFF'
      else
        table.tBodies[0].rows[i].bgColor = '#dfe3ee';
    }
  }
}


