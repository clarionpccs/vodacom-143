

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER084.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! Use DoesUserHaveAccess(p_web,AccessArea) instead!
!!! </summary>
SecurityCheckFailed  PROCEDURE  (f:Password,f:Area)        ! Declare Procedure
USERS::State  USHORT
ACCAREAS::State  USHORT
FilesOpened     BYTE(0)

  CODE
    Do OpenFiles
    Do SaveFiles

    Return# = 1

    Access:USERS.Clearkey(use:Password_Key)
    use:Password = f:Password
    If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
        Access:ACCAREAS.Clearkey(acc:Access_Level_Key)
        acc:User_Level = use:User_Level
        acc:Access_Area = f:Area
        If Access:ACCAREAS.TryFetch(acc:Access_Level_Key) = Level:Benign
            Return# = 0
        End ! If Access:ACCAREAS.TryFetch(acc:Access_Level_Key) = Level:Benign
    End ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign

    Do RestoreFiles
    Do CloseFiles
    Return Return#
SaveFiles  ROUTINE
  USERS::State = Access:USERS.SaveFile()                   ! Save File referenced in 'Other Files' so need to inform its FileManager
  ACCAREAS::State = Access:ACCAREAS.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF USERS::State <> 0
    Access:USERS.RestoreFile(USERS::State)                 ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF ACCAREAS::State <> 0
    Access:ACCAREAS.RestoreFile(ACCAREAS::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:USERS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:USERS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:ACCAREAS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:ACCAREAS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:USERS.Close
     Access:ACCAREAS.Close
     FilesOpened = False
  END
