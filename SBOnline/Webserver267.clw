

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER267.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER273.INC'),ONCE        !Req'd for module callout resolution
                     END


frmMakeTaggedOrders  PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locWebOrderNumber    LONG                                  !
FilesOpened     Long
ORDHEAD::State  USHORT
ORDITEMS::State  USHORT
TagFile::State  USHORT
ORDWEBPR::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('frmMakeTaggedOrders')
  loc:formname = 'frmMakeTaggedOrders_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('frmMakeTaggedOrders','')
    p_web._DivHeader('frmMakeTaggedOrders',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferfrmMakeTaggedOrders',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmMakeTaggedOrders',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmMakeTaggedOrders',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmMakeTaggedOrders',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmMakeTaggedOrders',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_frmMakeTaggedOrders',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(ORDHEAD)
  p_web._OpenFile(ORDITEMS)
  p_web._OpenFile(TagFile)
  p_web._OpenFile(ORDWEBPR)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(ORDHEAD)
  p_Web._CloseFile(ORDITEMS)
  p_Web._CloseFile(TagFile)
  p_Web._CloseFile(ORDWEBPR)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('frmMakeTaggedOrders_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('frmMakeTaggedOrders_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
      p_web.site.SaveButton.TextValue = 'Create Order'
      p_web.site.SaveButton.Class = 'button-entryfield'
    IF (CheckIfTagged(p_web) = 0)
        CreateScript(packet,'alert("You have not tagged any parts");window.open("frmPendingWebOrder","_self")')
        DO SendPacket
        EXIT
    END
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'FormBrowseStock'
    loc:formactiontarget = '_self'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('frmMakeTaggedOrders_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('frmMakeTaggedOrders_ChainTo')
    loc:formaction = p_web.GetSessionValue('frmMakeTaggedOrders_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'FormBrowseStock'
  loc:FormActionCancelTarget = '_self'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="frmMakeTaggedOrders" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="frmMakeTaggedOrders" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="frmMakeTaggedOrders" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Create Web Order') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Create Web Order',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_frmMakeTaggedOrders">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_frmMakeTaggedOrders" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_frmMakeTaggedOrders')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Parts Order') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_frmMakeTaggedOrders')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_frmMakeTaggedOrders'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_frmMakeTaggedOrders')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Parts Order') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_frmMakeTaggedOrders_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Parts Order')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Parts Order')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Parts Order')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Parts Order')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::txtMessage
      do Comment::txtMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnPrintOrder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::btnPrintOrder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::txtMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtMessage',p_web.GetValue('NewValue'))
    do Value::txtMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtMessage  Routine
  p_web._DivHeader('frmMakeTaggedOrders_' & p_web._nocolon('txtMessage') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web.Translate('Print Order, the click "Create Order" to complete the process.',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::txtMessage  Routine
    loc:comment = ''
  p_web._DivHeader('frmMakeTaggedOrders_' & p_web._nocolon('txtMessage') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnPrintOrder  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnPrintOrder',p_web.GetValue('NewValue'))
    do Value::btnPrintOrder
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do Value::btnPrintOrder
  do SendAlert

Value::btnPrintOrder  Routine
  p_web._DivHeader('frmMakeTaggedOrders_' & p_web._nocolon('btnPrintOrder') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnPrintOrder'',''frmmaketaggedorders_btnprintorder_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnPrintOrder','Print Order','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PendingWebOrderReport?' &'rnd=' & RANDOM(1,1000))) & ''','''&clip('_blank')&''')',loc:javascript,0,'/images/printer.png',,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('frmMakeTaggedOrders_' & p_web._nocolon('btnPrintOrder') & '_value')

Comment::btnPrintOrder  Routine
    loc:comment = ''
  p_web._DivHeader('frmMakeTaggedOrders_' & p_web._nocolon('btnPrintOrder') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('frmMakeTaggedOrders_btnPrintOrder_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnPrintOrder
      else
        do Value::btnPrintOrder
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('frmMakeTaggedOrders_form:ready_',1)
  p_web.SetSessionValue('frmMakeTaggedOrders_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_frmMakeTaggedOrders',0)

PreCopy  Routine
  p_web.SetValue('frmMakeTaggedOrders_form:ready_',1)
  p_web.SetSessionValue('frmMakeTaggedOrders_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_frmMakeTaggedOrders',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('frmMakeTaggedOrders_form:ready_',1)
  p_web.SetSessionValue('frmMakeTaggedOrders_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('frmMakeTaggedOrders:Primed',0)

PreDelete       Routine
  p_web.SetValue('frmMakeTaggedOrders_form:ready_',1)
  p_web.SetSessionValue('frmMakeTaggedOrders_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('frmMakeTaggedOrders:Primed',0)
  p_web.setsessionvalue('showtab_frmMakeTaggedOrders',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('frmMakeTaggedOrders_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  Access:TagFile.ClearKey(tag:keyTagged)
  tag:sessionID = p_web.SessionID
  SET(tag:keyTagged,tag:keyTagged)
  LOOP UNTIL Access:TagFile.Next()
  
      IF (tag:sessionID <> p_web.SessionID)
          BREAK
      END
  
      locWebOrderNumber = 0
      Access:ORDHEAD.ClearKey(orh:ProcessSaleNoKey)
      orh:procesed    = 0
      Set(orh:ProcessSaleNoKey,orh:ProcessSaleNoKey)
      Loop
          If Access:ORDHEAD.NEXT()
              Break
          End !If
          If orh:procesed    <> 0      |
              Then Break.  ! End If
          If orh:Account_No = p_web.GSV('BookingStoresAccount')
              locWebOrderNumber = orh:Order_no
              Break
          End !If orh:Account_No = job:Account_Number
      End !Loop
  
      Access:ORDWEBPR.ClearKey(orw:PartNumberKey)
      orw:AccountNumber = p_web.GSV('BookingStoresAccount')
      orw:PartNumber = tag:taggedValue
      If Access:ORDWEBPR.TryFetch(orw:PartNumberKey) = Level:Benign
                      !Found
          If (locWebOrderNumber > 0)
                          !This is an order raised for this account,
                          !now lets see if the same part has been ordered and add to that
              Found# = 0
              Access:ORDITEMS.ClearKey(ori:Keyordhno)
              ori:ordhno = locWebOrderNumber
              Set(ori:Keyordhno,ori:Keyordhno)
              Loop
                  If Access:ORDITEMS.NEXT()
                      Break
                  End !If
                  If ori:ordhno <> locWebOrderNumber      |
                      Then Break.  ! End If
                  If ori:partno = orw:PartNumber And ori:partdiscription = orw:Description
                      ori:qty += orw:Quantity
                      Access:ORDITEMS.Update()
                      Found# = 1
                      Break
                  End !ori:partdiscription = wpr:Description
              End !Loop
  
              If Found# = 0
                  !Make a new part line
                  If Access:ORDITEMS.PrimeRecord() = Level:Benign
                      ori:ordhno          = locWebOrderNumber
                                  !ori:manufact        = job:Manufacturer
                      ori:qty             = orw:Quantity
                      ori:itemcost        = orw:ItemCost
                      ori:totalcost       = orw:ItemCost * orw:Quantity
                      ori:partno          = orw:PartNumber
                      ori:partdiscription = orw:Description
                      If Access:ORDITEMS.TryInsert() = Level:Benign
                                      !Insert Successful
                      Else !If Access:ORDITEMS.TryInsert() = Level:Benign
                                      !Insert Failed
                      End !If Access:ORDITEMS.TryInsert() = Level:Benign
                  End !If Access:ORDITEMS.PrimeRecord() = Level:Benign
              End !If Found# = 0
          Else !If tmp:WebOrderNumber
              !There is no existing orders for this account
              If Access:ORDHEAD.PrimeRecord() = Level:Benign
                  orh:account_no      = p_web.GSV('BookingStoresAccount')
  
                              !Get Sub or Trade Account details?
                      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                      tra:Account_Number  = p_web.GSV('BookingAccount')
                      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                          !Found
                          orh:CustName        = tra:Company_Name
                          orh:CustAdd1        = tra:Address_Line1
                          orh:CustAdd2        = tra:Address_Line2
                          orh:CustAdd3        = tra:Address_Line3
                          orh:CustPostCode    = tra:Postcode
                          orh:CustTel         = tra:Telephone_Number
                          orh:CustFax         = tra:Fax_Number
  
                          orh:dName           = tra:Company_Name
                          orh:dAdd1           = tra:Address_Line1
                          orh:dAdd2           = tra:Address_Line2
                          orh:dAdd3           = tra:Address_Line3
                          orh:dPostCode       = tra:Postcode
                          orh:dTel            = tra:Telephone_Number
                          orh:dFax            = tra:Fax_Number
  
                      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                          !Error
                      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
                  orh:thedate         = Today()
                  orh:thetime         = Clock()
                  orh:procesed        = 0
                  orh:WhoBooked       = p_web.GSV('BookingUserCode')
                  orh:Department      = ''
                  orh:CustOrderNumber = ''
                  If Access:ORDHEAD.TryInsert() = Level:Benign
                                  !Insert Successful
                                  !Make a new part line
                      If Access:ORDITEMS.PrimeRecord() = Level:Benign
                          ori:ordhno          = orh:Order_No
                                      !ori:manufact        = job:Manufacturer
                          ori:qty             = orw:Quantity
                          ori:itemcost        = orw:ItemCost
                          ori:totalcost       = orw:ItemCost * orw:Quantity
                          ori:partno          = orw:PartNumber
                          ori:partdiscription = orw:Description
                          If Access:ORDITEMS.TryInsert() = Level:Benign
                                          !Insert Successful
                          Else !If Access:ORDITEMS.TryInsert() = Level:Benign
                                          !Insert Failed
                          End !If Access:ORDITEMS.TryInsert() = Level:Benign
                      End !If Access:ORDITEMS.PrimeRecord() = Level:Benign
  
                  Else !If Access:ORDHEAD.TryInsert() = Level:Benign
                                  !Insert Failed
                  End !If Access:ORDHEAD.TryInsert() = Level:Benign
              End !If Access:ORDHEAD.PrimeRecord() = Level:Benign
          End !If tmp:WebOrderNumber
          Delete(ORDWEBPR)
      Else!If Access:ORDWEBPR.TryFetch(orw:RecordNumberKey) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
      End!If Access:ORDWEBPR.TryFetch(orw:RecordNumberKey) = Level:Benign
  End !Loop x# = 1 To Records(glo:Queue)
  
  ClearTagFile(p_web)
  p_web.DeleteSessionValue('frmMakeTaggedOrders_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('frmMakeTaggedOrders:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')
SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('locWebOrderNumber',locWebOrderNumber) ! LONG
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     locWebOrderNumber = p_web.GSV('locWebOrderNumber') ! LONG
