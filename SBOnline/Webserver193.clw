

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER193.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
validFreeTextPart    PROCEDURE  (String fType, String fUserCode, String fManufacturer, String fModelNumber,String fPartNumber) ! Declare Procedure
USERS::State  USHORT
LOCATION::State  USHORT
STOCK::State  USHORT
STOMODEL::State  USHORT
FilesOpened     BYTE(0)

  CODE
! Return
! 0 - VALID
! 1 - Invalid User
! 2 - Inactive Site
! 3 - Suspended Stock Item
! 4 - Invalid Stock Item
! 5 - Restricted User

    do openFiles
    do saveFiles

    returnValue# = 0
    Access:USERS.Clearkey(use:user_code_key)
    use:user_code    = fUserCode
    if (Access:USERS.TryFetch(use:user_code_key) = Level:Benign)
        ! Found
    else ! if (Access:USERS.TryFetch(use:user_code_key) = Level:Benign)
        ! Error
        returnValue# = 1
    end ! if (Access:USERS.TryFetch(use:user_code_key) = Level:Benign)

    if (returnValue# = 0)
        Access:LOCATION.Clearkey(loc:Location_Key)
        loc:Location    = use:Location
        if (Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign)
            ! Found
            if (loc:Active = 0)
                returnValue# = 2
            end ! if (loc:Active = 0)
        else ! if (Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign)
            ! Error
        end ! if (Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign)
    end ! if (returnValue# = 0)

    if (returnValue# = 0)
        Access:STOCK.Clearkey(sto:Location_Manufacturer_Key)
        sto:Location    = use:Location
        sto:Manufacturer    = fManufacturer
        sto:Part_Number    = fPartNumber
        if (Access:STOCK.TryFetch(sto:Location_Manufacturer_Key) = Level:Benign)
            ! Found
            Access:STOMODEL.Clearkey(stm:Mode_Number_Only_Key)
            stm:Ref_Number    = sto:Ref_Number
            stm:Model_Number    = fModelNumber
            if (Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign)
                ! Found
                if (sto:Suspend)
                   returnValue# = 3
                else ! if (sto:Suspend)
                    if (fType = 'C')
                        if (use:RestrictParts And use:RestrictChargeable And sto:SkillLevel > use:SkillLevel)
                            returnValue# = 5
                        else ! if (use:RestrictParts And use:RestrictChargeable And sto:SkillLevel > use:SkillLevel)
                            
                        end ! if (use:RestrictParts And use:RestrictChargeable And sto:SkillLevel > use:SkillLevel)
                    end !if (fType = 'C')
                
                    if (fType = 'W')
                        if (use:RestrictParts And use:RestrictWarranty And sto:SkillLevel > use:SkillLevel)
                            returnValue# = 5
                        else ! if (use:RestrictParts And use:RestrictChargeable And sto:SkillLevel > use:SkillLevel)

                        end ! if (use:RestrictParts And use:RestrictChargeable And sto:SkillLevel > use:SkillLevel)
                    end !if (fType = 'C')
                end ! if (sto:Suspend)
            else ! if (Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign)
                ! Error
                returnValue# = 4
            end ! if (Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign)
        else ! if (Access:STOCK.TryFetch(sto:Location_Manufacture_Key) = Level:Benign)
            ! Error
            returnValue# = 4
        end ! if (Access:STOCK.TryFetch(sto:Location_Manufacture_Key) = Level:Benign)
    end ! if (returnValue# = 0)

    do restoreFiles
    do closeFiles


    return returnValue#
SaveFiles  ROUTINE
  USERS::State = Access:USERS.SaveFile()                   ! Save File referenced in 'Other Files' so need to inform its FileManager
  LOCATION::State = Access:LOCATION.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  STOCK::State = Access:STOCK.SaveFile()                   ! Save File referenced in 'Other Files' so need to inform its FileManager
  STOMODEL::State = Access:STOMODEL.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF USERS::State <> 0
    Access:USERS.RestoreFile(USERS::State)                 ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF LOCATION::State <> 0
    Access:LOCATION.RestoreFile(LOCATION::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF STOCK::State <> 0
    Access:STOCK.RestoreFile(STOCK::State)                 ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF STOMODEL::State <> 0
    Access:STOMODEL.RestoreFile(STOMODEL::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:USERS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:USERS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:LOCATION.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:LOCATION.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCK.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCK.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:STOMODEL.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOMODEL.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:USERS.Close
     Access:LOCATION.Close
     Access:STOCK.Close
     Access:STOMODEL.Close
     FilesOpened = False
  END
