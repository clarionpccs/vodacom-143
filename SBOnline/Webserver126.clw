

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER126.INC'),ONCE        !Local module procedure declarations
                     END


FormRepairNotes      PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
JOBRPNOT::State  USHORT
USERS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormRepairNotes')
  loc:formname = 'FormRepairNotes_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormRepairNotes','')
    p_web._DivHeader('FormRepairNotes',clip('fdiv') & ' ' & clip('FormContent'))
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormRepairNotes',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of InsertRecord + NET:WEB:StagePost
    do RestoreMem
    do PostInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormRepairNotes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:StagePost
    do RestoreMem
    do PostCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormRepairNotes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormRepairNotes',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    If loc:act = InsertRecord
      do PostInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do PostUpdate
    End

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormRepairNotes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of DeleteRecord + NET:WEB:StagePost
    do RestoreMem
    do PostDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormRepairNotes',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(JOBRPNOT)
  p_web._OpenFile(USERS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBRPNOT)
  p_Web._CloseFile(USERS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormRepairNotes_form:inited_',1)
  p_web.SetValue('UpdateFile','JOBRPNOT')
  p_web.SetValue('UpdateKey','jrn:RecordNumberKey')
  p_web.SetValue('IDField','jrn:RecordNumber')
  do RestoreMem

CancelForm  Routine
  IF p_web.GetSessionValue('FormRepairNotes:Primed') = 1
    p_web._deleteFile(JOBRPNOT)
    p_web.SetSessionValue('FormRepairNotes:Primed',0)
  End

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  p_web.SetValue('UpdateFile','JOBRPNOT')
  p_web.SetValue('UpdateKey','jrn:RecordNumberKey')
  If p_web.IfExistsValue('jrn:TheTime')
    p_web.SetPicture('jrn:TheTime','@t1b')
  End
  p_web.SetSessionPicture('jrn:TheTime','@t1b')
  If p_web.IfExistsValue('jrn:TheDate')
    p_web.SetPicture('jrn:TheDate',p_web.site.DatePicture)
  End
  p_web.SetSessionPicture('jrn:TheDate',p_web.site.DatePicture)
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=File

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormRepairNotes_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferFormRepairNotes')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormRepairNotes_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormRepairNotes_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormRepairNotes_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  packet = clip(packet) & '<input type="hidden" name="JOBRPNOT__FileAction" value="'&p_web.getSessionValue('JOBRPNOT:FileAction')&'" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="file" value="JOBRPNOT" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateFile" value="JOBRPNOT" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateKey" value="jrn:RecordNumberKey" ></input><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormRepairNotes" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormRepairNotes" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormRepairNotes" ></input><13,10>'
  end

  do SendPacket
  packet = clip(packet) & p_web.CreateInput('hidden','jrn:RecordNumber',p_web._jsok(p_web.getSessionValue('jrn:RecordNumber'))) & '<13,10>'
  If p_web.Translate('Insert / Amend Repair Notes') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Insert / Amend Repair Notes',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormRepairNotes">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormRepairNotes" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormRepairNotes')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormRepairNotes')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormRepairNotes'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.jrn:Notes')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormRepairNotes')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormRepairNotes_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jrn:TheDate
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jrn:TheDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jrn:TheTime
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jrn:TheTime
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jrn:Notes
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jrn:Notes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::jrn:TheDate  Routine
  p_web._DivHeader('FormRepairNotes_' & p_web._nocolon('jrn:TheDate') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Date')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jrn:TheDate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jrn:TheDate',p_web.GetValue('NewValue'))
    jrn:TheDate = p_web.GetValue('NewValue') !FieldType= DATE Field = jrn:TheDate
    do Value::jrn:TheDate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jrn:TheDate',p_web.dformat(clip(p_web.GetValue('Value')),p_web.site.DatePicture))
    jrn:TheDate = p_web.dformat(clip(p_web.GetValue('Value')),p_web.site.DatePicture)
  End
  do Value::jrn:TheDate
  do SendAlert

Value::jrn:TheDate  Routine
  p_web._DivHeader('FormRepairNotes_' & p_web._nocolon('jrn:TheDate') & '_value','adiv')
  loc:extra = ''
  ! --- DATE --- jrn:TheDate
    loc:AutoComplete = 'autocomplete="off"'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('jrn:TheDate')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jrn:TheDate'',''formrepairnotes_jrn:thedate_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:lookuponly = ''
  packet = clip(packet) & p_web.CreateInput('text','jrn:TheDate',p_web.GetSessionValue('jrn:TheDate'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.site.datepicture,loc:javascript,,'Date') & '<13,10>'
  if not loc:readonly
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' '&p_web._nocolon('sv(''jrn:TheDate'',''formrepairnotes_jrn:thedate_value'',1,FieldValue(this,1))')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:DateLookupButton,loc:formname,,,|
    'sd('''&clip(loc:formName)&''','''&p_web._nocolon('jrn:TheDate')&''','''&clip(upper(p_web.site.datepicture))&''','&lower(p_web._nocolon('''FormRepairNotes_jrn:TheDate_value'''))&');','onfocus="rad();" ')

  End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRepairNotes_' & p_web._nocolon('jrn:TheDate') & '_value')


Prompt::jrn:TheTime  Routine
  p_web._DivHeader('FormRepairNotes_' & p_web._nocolon('jrn:TheTime') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Time')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jrn:TheTime  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jrn:TheTime',p_web.GetValue('NewValue'))
    jrn:TheTime = p_web.GetValue('NewValue') !FieldType= TIME Field = jrn:TheTime
    do Value::jrn:TheTime
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jrn:TheTime',p_web.dFormat(p_web.GetValue('Value'),'@t1b'))
    jrn:TheTime = p_web.Dformat(p_web.GetValue('Value'),'@t1b') !
  End
  do Value::jrn:TheTime
  do SendAlert

Value::jrn:TheTime  Routine
  p_web._DivHeader('FormRepairNotes_' & p_web._nocolon('jrn:TheTime') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- jrn:TheTime
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('jrn:TheTime')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jrn:TheTime'',''formrepairnotes_jrn:thetime_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jrn:TheTime',p_web.GetSessionValue('jrn:TheTime'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@t1b',loc:javascript,,'Time') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRepairNotes_' & p_web._nocolon('jrn:TheTime') & '_value')


Prompt::jrn:Notes  Routine
  p_web._DivHeader('FormRepairNotes_' & p_web._nocolon('jrn:Notes') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Notes')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jrn:Notes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jrn:Notes',p_web.GetValue('NewValue'))
    jrn:Notes = p_web.GetValue('NewValue') !FieldType= STRING Field = jrn:Notes
    do Value::jrn:Notes
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jrn:Notes',p_web.dFormat(p_web.GetValue('Value'),'@s255'))
    jrn:Notes = p_web.GetValue('Value')
  End
  If jrn:Notes = ''
    loc:Invalid = 'jrn:Notes'
    loc:alert = p_web.translate('Notes') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    jrn:Notes = Upper(jrn:Notes)
    p_web.SetSessionValue('jrn:Notes',jrn:Notes)
  do Value::jrn:Notes
  do SendAlert

Value::jrn:Notes  Routine
  p_web._DivHeader('FormRepairNotes_' & p_web._nocolon('jrn:Notes') & '_value','adiv')
  loc:extra = ''
  ! --- TEXT --- jrn:Notes
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('jrn:Notes')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If jrn:Notes = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jrn:Notes'',''formrepairnotes_jrn:notes_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('jrn:Notes',p_web.GetSessionValue('jrn:Notes'),8,40,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,255,'Notes',Net:Web:Control) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRepairNotes_' & p_web._nocolon('jrn:Notes') & '_value')


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormRepairNotes_jrn:TheDate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jrn:TheDate
      else
        do Value::jrn:TheDate
      end
  of lower('FormRepairNotes_jrn:TheTime_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jrn:TheTime
      else
        do Value::jrn:TheTime
      end
  of lower('FormRepairNotes_jrn:Notes_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jrn:Notes
      else
        do Value::jrn:Notes
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormRepairNotes_form:ready_',1)
  p_web.SetSessionValue('FormRepairNotes_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormRepairNotes',0)
  jrn:User = p_web.GSV('BookingUserCode')
  p_web.SetSessionValue('jrn:User',jrn:User)
  jrn:RefNumber = p_web.GSV('wob:RefNumber')
  p_web.SetSessionValue('jrn:RefNumber',jrn:RefNumber)
  jrn:TheDate = Today()
  p_web.SetSessionValue('jrn:TheDate',jrn:TheDate)
  jrn:TheTime = Clock()
  p_web.SetSessionValue('jrn:TheTime',jrn:TheTime)

PreCopy  Routine
  p_web.SetValue('FormRepairNotes_form:ready_',1)
  p_web.SetSessionValue('FormRepairNotes_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormRepairNotes',0)
  p_web._PreCopyRecord(JOBRPNOT,jrn:RecordNumberKey)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormRepairNotes_form:ready_',1)
  p_web.SetSessionValue('FormRepairNotes_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormRepairNotes:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormRepairNotes_form:ready_',1)
  p_web.SetSessionValue('FormRepairNotes_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormRepairNotes:Primed',0)
  p_web.setsessionvalue('showtab_FormRepairNotes',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormRepairNotes_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormRepairNotes_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  If Loc:Invalid <> '' then exit.
        If jrn:Notes = ''
          loc:Invalid = 'jrn:Notes'
          loc:alert = p_web.translate('Notes') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          jrn:Notes = Upper(jrn:Notes)
          p_web.SetSessionValue('jrn:Notes',jrn:Notes)
        If loc:Invalid <> '' then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostInsert      Routine

PostCopy        Routine
  p_web.SetSessionValue('FormRepairNotes:Primed',0)

PostUpdate      Routine
  p_web.SetSessionValue('FormRepairNotes:Primed',0)

PostDelete      Routine
