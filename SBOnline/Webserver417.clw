

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER417.INC'),ONCE        !Local module procedure declarations
                     END


FormEngineersPerformanceDefaults PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locHubPoints         LONG                                  !
locEscalatedPoints   LONG                                  !
locEngineerQAPoints  LONG                                  !
locExchangePoints    LONG                                  !
FilesOpened     Long
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormEngineersPerformanceDefaults')
  loc:formname = 'FormEngineersPerformanceDefaults_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormEngineersPerformanceDefaults','')
    p_web._DivHeader('FormEngineersPerformanceDefaults',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormEngineersPerformanceDefaults',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormEngineersPerformanceDefaults',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormEngineersPerformanceDefaults',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormEngineersPerformanceDefaults',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormEngineersPerformanceDefaults',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormEngineersPerformanceDefaults',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormEngineersPerformanceDefaults_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('locHubPoints')
    p_web.SetPicture('locHubPoints','@n_8')
  End
  p_web.SetSessionPicture('locHubPoints','@n_8')
  If p_web.IfExistsValue('locEscalatedPoints')
    p_web.SetPicture('locEscalatedPoints','@n_8')
  End
  p_web.SetSessionPicture('locEscalatedPoints','@n_8')
  If p_web.IfExistsValue('locEngineerQAPoints')
    p_web.SetPicture('locEngineerQAPoints','@n_8')
  End
  p_web.SetSessionPicture('locEngineerQAPoints','@n_8')
  If p_web.IfExistsValue('locExchangePoints')
    p_web.SetPicture('locExchangePoints','@n_8')
  End
  p_web.SetSessionPicture('locExchangePoints','@n_8')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locHubPoints',locHubPoints)
  p_web.SetSessionValue('locEscalatedPoints',locEscalatedPoints)
  p_web.SetSessionValue('locEngineerQAPoints',locEngineerQAPoints)
  p_web.SetSessionValue('locExchangePoints',locExchangePoints)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locHubPoints')
    locHubPoints = p_web.dformat(clip(p_web.GetValue('locHubPoints')),'@n_8')
    p_web.SetSessionValue('locHubPoints',locHubPoints)
  End
  if p_web.IfExistsValue('locEscalatedPoints')
    locEscalatedPoints = p_web.dformat(clip(p_web.GetValue('locEscalatedPoints')),'@n_8')
    p_web.SetSessionValue('locEscalatedPoints',locEscalatedPoints)
  End
  if p_web.IfExistsValue('locEngineerQAPoints')
    locEngineerQAPoints = p_web.dformat(clip(p_web.GetValue('locEngineerQAPoints')),'@n_8')
    p_web.SetSessionValue('locEngineerQAPoints',locEngineerQAPoints)
  End
  if p_web.IfExistsValue('locExchangePoints')
    locExchangePoints = p_web.dformat(clip(p_web.GetValue('locExchangePoints')),'@n_8')
    p_web.SetSessionValue('locExchangePoints',locExchangePoints)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormEngineersPerformanceDefaults_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    p_web.SSV('locHubPoints',GETINI('ENGINEERSTATUS','HubPoints',,CLIP(PATH())&'\SB2KDEF.INI'))
    p_web.SSV('locEscalatedPoints',GETINI('ENGINEERSTATUS','EscalatedPoints',,CLIP(PATH())&'\SB2KDEF.INI'))
    p_web.SSV('locEngineerQAPoints',GETINI('ENGINEERSTATUS','EngineerQAPoints',,CLIP(PATH())&'\SB2KDEF.INI'))
    p_web.SSV('locExchangePoints',GETINI('ENGINEERSTATUS','ExchangePoints',,CLIP(PATH())&'\SB2KDEF.INI'))
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locHubPoints = p_web.RestoreValue('locHubPoints')
 locEscalatedPoints = p_web.RestoreValue('locEscalatedPoints')
 locEngineerQAPoints = p_web.RestoreValue('locEngineerQAPoints')
 locExchangePoints = p_web.RestoreValue('locExchangePoints')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'BrowseEngineersPerformance'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormEngineersPerformanceDefaults_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormEngineersPerformanceDefaults_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormEngineersPerformanceDefaults_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'BrowseEngineersPerformance'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormEngineersPerformanceDefaults" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormEngineersPerformanceDefaults" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormEngineersPerformanceDefaults" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Engineer Status Weighting Defaults') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Engineer Status Weighting Defaults',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormEngineersPerformanceDefaults">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormEngineersPerformanceDefaults" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormEngineersPerformanceDefaults')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Defaults') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormEngineersPerformanceDefaults')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormEngineersPerformanceDefaults'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locHubPoints')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormEngineersPerformanceDefaults')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Defaults') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormEngineersPerformanceDefaults_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Defaults')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Defaults')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Defaults')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Defaults')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locHubPoints
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locHubPoints
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locHubPoints
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locEscalatedPoints
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locEscalatedPoints
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locEscalatedPoints
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locEngineerQAPoints
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locEngineerQAPoints
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locEngineerQAPoints
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locExchangePoints
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locExchangePoints
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locExchangePoints
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locHubPoints  Routine
  p_web._DivHeader('FormEngineersPerformanceDefaults_' & p_web._nocolon('locHubPoints') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Hub Points')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locHubPoints  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locHubPoints',p_web.GetValue('NewValue'))
    locHubPoints = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locHubPoints
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locHubPoints',p_web.dFormat(p_web.GetValue('Value'),'@n_8'))
    locHubPoints = p_web.Dformat(p_web.GetValue('Value'),'@n_8') !
  End
  do Value::locHubPoints
  do SendAlert

Value::locHubPoints  Routine
  p_web._DivHeader('FormEngineersPerformanceDefaults_' & p_web._nocolon('locHubPoints') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locHubPoints
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locHubPoints')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locHubPoints'',''formengineersperformancedefaults_lochubpoints_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locHubPoints',p_web.GetSessionValue('locHubPoints'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n_8',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineersPerformanceDefaults_' & p_web._nocolon('locHubPoints') & '_value')

Comment::locHubPoints  Routine
      loc:comment = ''
  p_web._DivHeader('FormEngineersPerformanceDefaults_' & p_web._nocolon('locHubPoints') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locEscalatedPoints  Routine
  p_web._DivHeader('FormEngineersPerformanceDefaults_' & p_web._nocolon('locEscalatedPoints') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Escalated Points')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locEscalatedPoints  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locEscalatedPoints',p_web.GetValue('NewValue'))
    locEscalatedPoints = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locEscalatedPoints
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locEscalatedPoints',p_web.dFormat(p_web.GetValue('Value'),'@n_8'))
    locEscalatedPoints = p_web.Dformat(p_web.GetValue('Value'),'@n_8') !
  End
  do Value::locEscalatedPoints
  do SendAlert

Value::locEscalatedPoints  Routine
  p_web._DivHeader('FormEngineersPerformanceDefaults_' & p_web._nocolon('locEscalatedPoints') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locEscalatedPoints
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locEscalatedPoints')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locEscalatedPoints'',''formengineersperformancedefaults_locescalatedpoints_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locEscalatedPoints',p_web.GetSessionValue('locEscalatedPoints'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n_8',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineersPerformanceDefaults_' & p_web._nocolon('locEscalatedPoints') & '_value')

Comment::locEscalatedPoints  Routine
      loc:comment = ''
  p_web._DivHeader('FormEngineersPerformanceDefaults_' & p_web._nocolon('locEscalatedPoints') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locEngineerQAPoints  Routine
  p_web._DivHeader('FormEngineersPerformanceDefaults_' & p_web._nocolon('locEngineerQAPoints') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Engineer QA Points')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locEngineerQAPoints  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locEngineerQAPoints',p_web.GetValue('NewValue'))
    locEngineerQAPoints = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locEngineerQAPoints
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locEngineerQAPoints',p_web.dFormat(p_web.GetValue('Value'),'@n_8'))
    locEngineerQAPoints = p_web.Dformat(p_web.GetValue('Value'),'@n_8') !
  End
  do Value::locEngineerQAPoints
  do SendAlert

Value::locEngineerQAPoints  Routine
  p_web._DivHeader('FormEngineersPerformanceDefaults_' & p_web._nocolon('locEngineerQAPoints') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locEngineerQAPoints
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locEngineerQAPoints')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locEngineerQAPoints'',''formengineersperformancedefaults_locengineerqapoints_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locEngineerQAPoints',p_web.GetSessionValue('locEngineerQAPoints'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n_8',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineersPerformanceDefaults_' & p_web._nocolon('locEngineerQAPoints') & '_value')

Comment::locEngineerQAPoints  Routine
      loc:comment = ''
  p_web._DivHeader('FormEngineersPerformanceDefaults_' & p_web._nocolon('locEngineerQAPoints') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locExchangePoints  Routine
  p_web._DivHeader('FormEngineersPerformanceDefaults_' & p_web._nocolon('locExchangePoints') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Exchange Points')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locExchangePoints  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locExchangePoints',p_web.GetValue('NewValue'))
    locExchangePoints = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locExchangePoints
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locExchangePoints',p_web.dFormat(p_web.GetValue('Value'),'@n_8'))
    locExchangePoints = p_web.Dformat(p_web.GetValue('Value'),'@n_8') !
  End
  do Value::locExchangePoints
  do SendAlert

Value::locExchangePoints  Routine
  p_web._DivHeader('FormEngineersPerformanceDefaults_' & p_web._nocolon('locExchangePoints') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locExchangePoints
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locExchangePoints')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locExchangePoints'',''formengineersperformancedefaults_locexchangepoints_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locExchangePoints',p_web.GetSessionValue('locExchangePoints'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n_8',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineersPerformanceDefaults_' & p_web._nocolon('locExchangePoints') & '_value')

Comment::locExchangePoints  Routine
      loc:comment = ''
  p_web._DivHeader('FormEngineersPerformanceDefaults_' & p_web._nocolon('locExchangePoints') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormEngineersPerformanceDefaults_locHubPoints_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locHubPoints
      else
        do Value::locHubPoints
      end
  of lower('FormEngineersPerformanceDefaults_locEscalatedPoints_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locEscalatedPoints
      else
        do Value::locEscalatedPoints
      end
  of lower('FormEngineersPerformanceDefaults_locEngineerQAPoints_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locEngineerQAPoints
      else
        do Value::locEngineerQAPoints
      end
  of lower('FormEngineersPerformanceDefaults_locExchangePoints_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locExchangePoints
      else
        do Value::locExchangePoints
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormEngineersPerformanceDefaults_form:ready_',1)
  p_web.SetSessionValue('FormEngineersPerformanceDefaults_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormEngineersPerformanceDefaults',0)

PreCopy  Routine
  p_web.SetValue('FormEngineersPerformanceDefaults_form:ready_',1)
  p_web.SetSessionValue('FormEngineersPerformanceDefaults_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormEngineersPerformanceDefaults',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormEngineersPerformanceDefaults_form:ready_',1)
  p_web.SetSessionValue('FormEngineersPerformanceDefaults_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormEngineersPerformanceDefaults:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormEngineersPerformanceDefaults_form:ready_',1)
  p_web.SetSessionValue('FormEngineersPerformanceDefaults_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormEngineersPerformanceDefaults:Primed',0)
  p_web.setsessionvalue('showtab_FormEngineersPerformanceDefaults',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
    PUTINI('ENGINEERSTATUS','HubPoints',p_web.GSV('locHubPoints'),CLIP(PATH())&'\SB2KDEF.INI')
    PUTINI('ENGINEERSTATUS','EscalatedPoints',p_web.GSV('locEscalatedPoints'),CLIP(PATH())&'\SB2KDEF.INI')
    PUTINI('ENGINEERSTATUS','EngineerQAPoints',p_web.GSV('locEngineerQAPoints'),CLIP(PATH())&'\SB2KDEF.INI')
    PUTINI('ENGINEERSTATUS','ExchangePoints',p_web.GSV('locExchangePoints'),CLIP(PATH())&'\SB2KDEF.INI')  
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormEngineersPerformanceDefaults_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormEngineersPerformanceDefaults_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormEngineersPerformanceDefaults:Primed',0)
  p_web.StoreValue('locHubPoints')
  p_web.StoreValue('locEscalatedPoints')
  p_web.StoreValue('locEngineerQAPoints')
  p_web.StoreValue('locExchangePoints')
