

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER136.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Is48HourOrderCreated PROCEDURE  (func:Location,func:JobNumber) ! Declare Procedure
EXCHOR48::State  USHORT
FilesOpened     BYTE(0)

  CODE
    Do OpenFiles
    Do SaveFiles

    Return# = 0
    Access:EXCHOR48.ClearKey(ex4:LocationJobKey)
    ex4:Received  = 0
    ex4:Returning = 0
    ex4:Location  = func:Location
    ex4:JobNumber = func:JobNumber
    Set(ex4:LocationJobKey,ex4:LocationJobKey)
    Loop
        If Access:EXCHOR48.NEXT()
           Break
        End !If
        If ex4:Received  <> 0      |
        Or ex4:Returning <> 0      |
        Or ex4:Location  <> func:Location      |
        Or ex4:JobNumber <> func:JobNumber      |
            Then Break.  ! End If
        If ex4:AttachedToJob = 0 And ex4:DateOrdered = 0
            Return# = 1
            Break
        End !If ex4:AttachedToJob = 0 And ex4:DateOrdered = 0
    End !Loop

    Do RestoreFiles
    Do CloseFiles

    Return Return#

    
SaveFiles  ROUTINE
  EXCHOR48::State = Access:EXCHOR48.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF EXCHOR48::State <> 0
    Access:EXCHOR48.RestoreFile(EXCHOR48::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:EXCHOR48.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:EXCHOR48.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:EXCHOR48.Close
     FilesOpened = False
  END
