

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER047.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER046.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER048.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER049.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER050.INC'),ONCE        !Req'd for module callout resolution
                     END


BrowseIMEIHistory    PROCEDURE  (NetWebServerWorker p_web)
tmp:BouncerCompanyName STRING(30)                          !Company Name
tmp:BouncerChargeType STRING(30)                           !Charge Type
tmp:BouncerDays      LONG                                  !Days
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(JOBS_ALIAS)
                      Project(job_ali:Ref_Number)
                      Project(job_ali:Ref_Number)
                      Project(job_ali:date_booked)
                      Project(job_ali:Date_Completed)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
JOBNOTES_ALIAS::State  USHORT
TRADEACC::State  USHORT
SUBTRACC::State  USHORT
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('BrowseIMEIHistory')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseIMEIHistory:NoForm')
      loc:NoForm = p_web.GetValue('BrowseIMEIHistory:NoForm')
      loc:FormName = p_web.GetValue('BrowseIMEIHistory:FormName')
    else
      loc:FormName = 'BrowseIMEIHistory_frm'
    End
    p_web.SSV('BrowseIMEIHistory:NoForm',loc:NoForm)
    p_web.SSV('BrowseIMEIHistory:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseIMEIHistory:NoForm')
    loc:FormName = p_web.GSV('BrowseIMEIHistory:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseIMEIHistory') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseIMEIHistory')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  If p_web.RequestAjax = 0
    packet = clip(packet) & '<table><tr><td valign="top">'
    do SendPacket
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(JOBS_ALIAS,job_ali:Ref_Number_Key,loc:vorder)
    If False
    ElsIf (loc:vorder = 'JOB_ALI:REF_NUMBER') then p_web.SetValue('BrowseIMEIHistory_sort','1')
    ElsIf (loc:vorder = 'TMP:BOUNCERCOMPANYNAME') then p_web.SetValue('BrowseIMEIHistory_sort','2')
    ElsIf (loc:vorder = 'JOB_ALI:DATE_BOOKED') then p_web.SetValue('BrowseIMEIHistory_sort','3')
    ElsIf (loc:vorder = 'JOB_ALI:DATE_COMPLETED') then p_web.SetValue('BrowseIMEIHistory_sort','4')
    ElsIf (loc:vorder = 'TMP:BOUNCERCHARGETYPE') then p_web.SetValue('BrowseIMEIHistory_sort','5')
    ElsIf (loc:vorder = 'TMP:BOUNCERDAYS') then p_web.SetValue('BrowseIMEIHistory_sort','6')
    ElsIf (loc:vorder = 'JBN_ALI:FAULT_DESCRIPTION') then p_web.SetValue('BrowseIMEIHistory_sort','7')
    ElsIf (loc:vorder = 'JBN_ALI:ENGINEERS_NOTES') then p_web.SetValue('BrowseIMEIHistory_sort','8')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseIMEIHistory:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseIMEIHistory:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowseIMEIHistory:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseIMEIHistory:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseIMEIHistory:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:NoSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
    If p_web.IfExistsValue('fromURL')
      p_web.StoreValue('fromURL')
    End
    If p_web.IfExistsValue('currentJob')
      p_web.StoreValue('currentJob')
    End
  
      p_web.site.ChangeButton.TextValue = 'Details'
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseIMEIHistory_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseIMEIHistory_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'job_ali:Ref_Number','-job_ali:Ref_Number')
    Loc:LocateField = 'job_ali:Ref_Number'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'tmp:BouncerCompanyName','-tmp:BouncerCompanyName')
    Loc:LocateField = 'tmp:BouncerCompanyName'
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'job_ali:date_booked','-job_ali:date_booked')
    Loc:LocateField = 'job_ali:date_booked'
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'job_ali:Date_Completed','-job_ali:Date_Completed')
    Loc:LocateField = 'job_ali:Date_Completed'
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'tmp:BouncerChargeType','-tmp:BouncerChargeType')
    Loc:LocateField = 'tmp:BouncerChargeType'
  of 6
    loc:vorder = Choose(Loc:SortDirection=1,'tmp:BouncerDays','-tmp:BouncerDays')
    Loc:LocateField = 'tmp:BouncerDays'
  of 7
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(jbn_ali:Fault_Description)','-UPPER(jbn_ali:Fault_Description)')
    Loc:LocateField = 'jbn_ali:Fault_Description'
  of 8
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(jbn_ali:Engineers_Notes)','-UPPER(jbn_ali:Engineers_Notes)')
    Loc:LocateField = 'jbn_ali:Engineers_Notes'
  end
  if loc:vorder = ''
    loc:vorder = '-job_ali:Ref_Number'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('job_ali:Ref_Number')
    loc:SortHeader = p_web.Translate('Job Number')
    p_web.SetSessionValue('BrowseIMEIHistory_LocatorPic','@s8')
  Of upper('tmp:BouncerCompanyName')
    loc:SortHeader = p_web.Translate('Company Name')
    p_web.SetSessionValue('BrowseIMEIHistory_LocatorPic','@s30')
  Of upper('job_ali:date_booked')
    loc:SortHeader = p_web.Translate('Date Booked')
    p_web.SetSessionValue('BrowseIMEIHistory_LocatorPic','@d6b')
  Of upper('job_ali:Date_Completed')
    loc:SortHeader = p_web.Translate('Date Completed')
    p_web.SetSessionValue('BrowseIMEIHistory_LocatorPic','@D6b')
  Of upper('tmp:BouncerChargeType')
    loc:SortHeader = p_web.Translate('Charge Type')
    p_web.SetSessionValue('BrowseIMEIHistory_LocatorPic','@s30')
  Of upper('tmp:BouncerDays')
    loc:SortHeader = p_web.Translate('Days')
    p_web.SetSessionValue('BrowseIMEIHistory_LocatorPic','@n4')
  Of upper('jbn_ali:Fault_Description')
    loc:SortHeader = p_web.Translate('Fault Description')
    p_web.SetSessionValue('BrowseIMEIHistory_LocatorPic','@s255')
  Of upper('jbn_ali:Engineers_Notes')
    loc:SortHeader = p_web.Translate('Engineers Notes')
    p_web.SetSessionValue('BrowseIMEIHistory_LocatorPic','@s255')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GSV('fromURL')
  End!Else
  loc:CloseAction = p_web.GSV('fromURL')
    loc:formaction = 'ViewBouncerJob'
    loc:formactiontarget = '_self'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseIMEIHistory:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseIMEIHistory:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseIMEIHistory:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="JOBS_ALIAS"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="job_ali:Ref_Number_Key"></input><13,10>'
  end
  If p_web.Translate('Previous Job History') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Previous Job History',0)&'</span>'&CRLF
  End
  If clip('Previous Job History') <> ''
    !packet = clip(packet) & p_web.br !Bryan. Why is this here?
  End
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseIMEIHistory',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowseIMEIHistory',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseIMEIHistory.locate(''Locator2BrowseIMEIHistory'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseIMEIHistory.cl(''BrowseIMEIHistory'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="sortable" id="BrowseIMEIHistory_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="'&clip('BrowseTable')&'" id="BrowseIMEIHistory_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BrowseIMEIHistory','Job Number',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Job Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','BrowseIMEIHistory','Company Name',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Company Name')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','BrowseIMEIHistory','Date Booked',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Date Booked')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'4','BrowseIMEIHistory','Date Completed',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Date Completed')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'5','BrowseIMEIHistory','Charge Type',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Charge Type')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'6','BrowseIMEIHistory','Days',,,'CenterJustify',,1)
        Else
          packet = clip(packet) & '<th class="CenterJustify">'&p_web.Translate('Days')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'7','BrowseIMEIHistory','Fault Description',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Fault Description')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'8','BrowseIMEIHistory','Engineers Notes',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Engineers Notes')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:LocateField = 'job_ali:date_booked' then Loc:NoBuffer = 1.
  If Loc:LocateField = 'job_ali:Date_Completed' then Loc:NoBuffer = 1.
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('job_ali:ref_number',lower(Thisview{prop:order}),1,1) = 0 !and JOBS_ALIAS{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'job_ali:Ref_Number'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('job_ali:Ref_Number'),p_web.GetValue('job_ali:Ref_Number'),p_web.GetSessionValue('job_ali:Ref_Number'))
      loc:FilterWas = 'Upper(job_ali:ESN)  = Upper(''' & p_web.GSV('job:ESN') & ''')'
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseIMEIHistory',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseIMEIHistory_Filter')
    p_web.SetSessionValue('BrowseIMEIHistory_FirstValue','')
    p_web.SetSessionValue('BrowseIMEIHistory_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,JOBS_ALIAS,job_ali:Ref_Number_Key,loc:PageRows,'BrowseIMEIHistory',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If JOBS_ALIAS{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(JOBS_ALIAS,loc:firstvalue)
              Reset(ThisView,JOBS_ALIAS)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If JOBS_ALIAS{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(JOBS_ALIAS,loc:lastvalue)
            Reset(ThisView,JOBS_ALIAS)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      if (p_web.GSV('currentJob') > 0)
          if job_ali:ref_Number = p_web.GSV('currentJob')
              cycle
          end !if job_ali:ref_Number = p_web.GSV('currentJob')
      end ! if (p_web.GSV('currentJob') > 0)
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(job_ali:Ref_Number)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseIMEIHistory.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseIMEIHistory.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseIMEIHistory.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseIMEIHistory.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
    If loc:found
          Loc:IsChange = 0
          If loc:selecting = 0
            If loc:viewonly = 0
              packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:ChangeButton,'BrowseIMEIHistory')
              loc:isChange = 1
            End
          End
          TableQueue.Kind = Net:BeforeTable
          do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseIMEIHistory',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseIMEIHistory_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseIMEIHistory_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowseIMEIHistory',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseIMEIHistory.locate(''Locator1BrowseIMEIHistory'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseIMEIHistory.cl(''BrowseIMEIHistory'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowseIMEIHistory_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseIMEIHistory_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseIMEIHistory.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseIMEIHistory.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseIMEIHistory.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseIMEIHistory.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  If loc:found
        Loc:IsChange = 0
        If loc:selecting = 0
          If loc:viewonly = 0
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:ChangeButton,'BrowseIMEIHistory')
            loc:isChange = 1
          End
        End
        do SendPacket
  End
  End
  If loc:selecting = 0 and loc:parent = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:Formname,loc:CloseAction)
      do SendPacket
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
        Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
        sub:Account_Number  = job_ali:Account_Number
        If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
            !Found
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number  = sub:Main_Account_Number
            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Found
    
            Else ! If Access:tradeacc.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Error
            End !If Access:tradeacc.Tryfetch(tra:Account_Number_Key) = Level:Benign
        Else ! If Access:subtracc.Tryfetch(sub:Account_Number_Key) = Level:Benign
            !Error
        End !If Access:subtracc.Tryfetch(sub:Account_Number_Key) = Level:Benign
        tmp:BouncerCompanyName = tra:Company_Name
        tmp:BouncerDays     = Today() - job_ali:Date_Booked
        If job_ali:Chargeable_Job = 'YES' and job_ali:Warranty_Job = 'YES'
            tmp:BouncerChargeType = job_ali:Charge_Type
            tmp:BouncerChargeType = job_ali:Warranty_Charge_Type
        Else !If job_ali:Chargeale_Job = 'YES' and job_ali:Warranty_Job = 'YES'
            If job_ali:Chargeable_Job = 'YES'
                tmp:BouncerChargeType = job_ali:Charge_Type
            End !If job_ali:Chargeable_Job = 'YES'
            If job_ali:Warranty_Job = 'YES'
                tmp:BouncerChargeType = job_ali:Warranty_Charge_Type
            End !If job_ali:Chargeable_Job = 'YES'
        End !If job_ali:Chargeale_Job = 'YES' and job_ali:Warranty_Job = 'YES'
    
        Access:JOBNOTES_ALIAS.ClearKey(jbn_ali:RefNumberKey)
        jbn_ali:RefNumber = job_ali:Ref_Number
        IF (Access:JOBNOTES_ALIAS.TryFetch(jbn_ali:RefNumberKey))
            Access:JOBNOTES_ALIAS.ClearKey(jbn_ali:RefNumberKey)
        END
    
    
    loc:field = job_ali:Ref_Number
    p_web._thisrow = p_web._nocolon('job_ali:Ref_Number')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseIMEIHistory:LookupField')) = job_ali:Ref_Number and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((job_ali:Ref_Number = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowseIMEIHistory.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:Silent = 0
        if loc:silent = 0
          loc:rowstyle = clip(loc:rowstyle) & 'sv('''',''bounceroutfaults_browseimeihistory'',''refresh='&clip('job_ali:Ref_Number')&''',''' & p_web._jsok(p_web._nocolon('job_ali:Ref_Number')&'='& p_web.escape(loc:field,net:BigPlus))&''',''_ParentProc=BrowseIMEIHistory'',''_Silent='&loc:silent&'''); '
        end
        loc:Silent = 0
        if loc:silent = 0
          loc:rowstyle = clip(loc:rowstyle) & 'sv('''',''bouncerchargeableparts_browseimeihistory'',''refresh='&clip('job_ali:Ref_Number')&''',''' & p_web._jsok(p_web._nocolon('job_ali:Ref_Number')&'='& p_web.escape(loc:field,net:BigPlus))&''',''_ParentProc=BrowseIMEIHistory'',''_Silent='&loc:silent&'''); '
        end
        loc:Silent = 0
        if loc:silent = 0
          loc:rowstyle = clip(loc:rowstyle) & 'sv('''',''bouncerwarrantyparts_browseimeihistory'',''refresh='&clip('job_ali:Ref_Number')&''',''' & p_web._jsok(p_web._nocolon('job_ali:Ref_Number')&'='& p_web.escape(loc:field,net:BigPlus))&''',''_ParentProc=BrowseIMEIHistory'',''_Silent='&loc:silent&'''); '
        end
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If JOBS_ALIAS{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(JOBS_ALIAS)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If JOBS_ALIAS{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(JOBS_ALIAS)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','job_ali:Ref_Number',clip(loc:field),,loc:checked,,,'onclick="BrowseIMEIHistory.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','job_ali:Ref_Number',clip(loc:field),,'checked',,,'onclick="BrowseIMEIHistory.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::job_ali:Ref_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::tmp:BouncerCompanyName
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::job_ali:date_booked
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::job_ali:Date_Completed
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::tmp:BouncerChargeType
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
            if false
            elsif tmp:BouncerDays < 90
              packet = clip(packet) & '<td class="'&clip('BrowseRed')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            else
              packet = clip(packet) & '<td class="CenterJustify">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            end
          end ! loc:eip = 0
          do value::tmp:BouncerDays
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::jbn_ali:Fault_Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::jbn_ali:Engineers_Notes
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowseIMEIHistory.omv(this);" onMouseOut="BrowseIMEIHistory.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowseIMEIHistory=new browseTable(''BrowseIMEIHistory'','''&clip(loc:formname)&''','''&p_web._jsok('job_ali:Ref_Number',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('job_ali:Ref_Number')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''',''ViewBouncerJob'');<13,10>'&|
      'BrowseIMEIHistory.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowseIMEIHistory.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseIMEIHistory')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseIMEIHistory')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseIMEIHistory')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseIMEIHistory')
          end
        End
      End
    End
      do SendPacket
      Do AddPrintButton
      do SendPacket
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(JOBS_ALIAS)
  p_web._CloseFile(JOBNOTES_ALIAS)
  p_web._CloseFile(TRADEACC)
  p_web._CloseFile(SUBTRACC)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(JOBS_ALIAS)
  Bind(job_ali:Record)
  Clear(job_ali:Record)
  NetWebSetSessionPics(p_web,JOBS_ALIAS)
  p_web._OpenFile(JOBNOTES_ALIAS)
  Bind(jbn_ali:Record)
  NetWebSetSessionPics(p_web,JOBNOTES_ALIAS)
  p_web._OpenFile(TRADEACC)
  Bind(tra:Record)
  NetWebSetSessionPics(p_web,TRADEACC)
  p_web._OpenFile(SUBTRACC)
  Bind(sub:Record)
  NetWebSetSessionPics(p_web,SUBTRACC)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('job_ali:Ref_Number',loc:default)
    p_web.SetValue('refresh','first')
    If loc:ParentSilent
      loc:Silent = 1
    Else
      loc:silent = Choose(loc:found=1,0,1)
    End
    if p_web.IfExistsValue('BrowseIMEIHistory:NoForm')
      p_web.SetValue('BouncerOutFaults:NoForm',loc:noform)
      p_web.SetValue('BouncerOutFaults:FormName',loc:formname)
    end
    p_web.SetValue('_ParentProc','BrowseIMEIHistory')
    p_web.SetValue('_Silent',loc:Silent)
    p_web.SetValue('BouncerOutFaults:parentIs','Browse')
    BouncerOutFaults(p_web)
    If loc:ParentSilent
      loc:Silent = 1
    Else
      loc:silent = Choose(loc:found=1,0,1)
    End
    if p_web.IfExistsValue('BrowseIMEIHistory:NoForm')
      p_web.SetValue('BouncerChargeableParts:NoForm',loc:noform)
      p_web.SetValue('BouncerChargeableParts:FormName',loc:formname)
    end
    p_web.SetValue('_ParentProc','BrowseIMEIHistory')
    p_web.SetValue('_Silent',loc:Silent)
    p_web.SetValue('BouncerChargeableParts:parentIs','Browse')
    BouncerChargeableParts(p_web)
    If loc:ParentSilent
      loc:Silent = 1
    Else
      loc:silent = Choose(loc:found=1,0,1)
    End
    if p_web.IfExistsValue('BrowseIMEIHistory:NoForm')
      p_web.SetValue('BouncerWarrantyParts:NoForm',loc:noform)
      p_web.SetValue('BouncerWarrantyParts:FormName',loc:formname)
    end
    p_web.SetValue('_ParentProc','BrowseIMEIHistory')
    p_web.SetValue('_Silent',loc:Silent)
    p_web.SetValue('BouncerWarrantyParts:parentIs','Browse')
    BouncerWarrantyParts(p_web)

StartChildren  Routine
    packet = clip(packet) & '</td>'
    packet = clip(packet) & '</tr>'
    If Loc:ParentSilent = 1
      Loc:Silent = 1
    Else
      loc:silent = Choose(loc:found=1,0,1)
    End
    if p_web.IfExistsValue('BrowseIMEIHistory:NoForm')
      p_web.SetValue('BouncerOutFaults:NoForm',loc:noform)
      p_web.SetValue('BouncerOutFaults:FormName',loc:formname)
    end
    p_web.SetValue('BouncerOutFaults:parentIs','Browse')
    loc:extra = ''
    packet = clip(packet) & '<tr>'&loc:extra&'<td valign="top"><!-- Net:BouncerOutFaults?Refresh=first&_ParentProc=BrowseIMEIHistory&_Silent='&loc:silent&' --></td></tr>'
    If Loc:ParentSilent = 1
      Loc:Silent = 1
    Else
      loc:silent = Choose(loc:found=1,0,1)
    End
    if p_web.IfExistsValue('BrowseIMEIHistory:NoForm')
      p_web.SetValue('BouncerChargeableParts:NoForm',loc:noform)
      p_web.SetValue('BouncerChargeableParts:FormName',loc:formname)
    end
    p_web.SetValue('BouncerChargeableParts:parentIs','Browse')
    loc:extra = ''
    packet = clip(packet) & '<tr>'&loc:extra&'<td valign="top"><!-- Net:BouncerChargeableParts?Refresh=first&_ParentProc=BrowseIMEIHistory&_Silent='&loc:silent&' --></td></tr>'
    If Loc:ParentSilent = 1
      Loc:Silent = 1
    Else
      loc:silent = Choose(loc:found=1,0,1)
    End
    if p_web.IfExistsValue('BrowseIMEIHistory:NoForm')
      p_web.SetValue('BouncerWarrantyParts:NoForm',loc:noform)
      p_web.SetValue('BouncerWarrantyParts:FormName',loc:formname)
    end
    p_web.SetValue('BouncerWarrantyParts:parentIs','Browse')
    loc:extra = ''
    packet = clip(packet) & '<tr>'&loc:extra&'<td valign="top"><!-- Net:BouncerWarrantyParts?Refresh=first&_ParentProc=BrowseIMEIHistory&_Silent='&loc:silent&' --></td></tr>'
    packet = clip(packet) & '</table>'
    p_web._DivHeader(lower('BrowseIMEIHistory_BouncerOutFaults_value'))
    p_web._DivFooter()
    p_web._RegisterDivEx(lower('BrowseIMEIHistory_BouncerOutFaults_value'))
    p_web._DivHeader(lower('BrowseIMEIHistory_BouncerChargeableParts_value'))
    p_web._DivFooter()
    p_web._RegisterDivEx(lower('BrowseIMEIHistory_BouncerChargeableParts_value'))
    p_web._DivHeader(lower('BrowseIMEIHistory_BouncerWarrantyParts_value'))
    p_web._DivFooter()
    p_web._RegisterDivEx(lower('BrowseIMEIHistory_BouncerWarrantyParts_value'))
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::job_ali:Ref_Number   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('job_ali:Ref_Number_'&job_ali:Ref_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(job_ali:Ref_Number,'@s8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::tmp:BouncerCompanyName   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('tmp:BouncerCompanyName_'&job_ali:Ref_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(tmp:BouncerCompanyName,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::job_ali:date_booked   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('job_ali:date_booked_'&job_ali:Ref_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(job_ali:date_booked,'@d6b')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::job_ali:Date_Completed   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('job_ali:Date_Completed_'&job_ali:Ref_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(job_ali:Date_Completed,'@D6b')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::tmp:BouncerChargeType   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('tmp:BouncerChargeType_'&job_ali:Ref_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(tmp:BouncerChargeType,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::tmp:BouncerDays   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    elsif tmp:BouncerDays < 90
      packet = clip(packet) & p_web._DivHeader('tmp:BouncerDays_'&job_ali:Ref_Number,'BrowseRed',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok(tmp:BouncerDays,0))
    else
      packet = clip(packet) & p_web._DivHeader('tmp:BouncerDays_'&job_ali:Ref_Number,'CenterJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(tmp:BouncerDays,'@n4')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::jbn_ali:Fault_Description   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('jbn_ali:Fault_Description_'&job_ali:Ref_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(jbn_ali:Fault_Description,'@s255')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::jbn_ali:Engineers_Notes   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('jbn_ali:Engineers_Notes_'&job_ali:Ref_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(jbn_ali:Engineers_Notes,'@s255')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(JOBNOTES_ALIAS)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(SUBTRACC)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBNOTES_ALIAS)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(SUBTRACC)
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = job_ali:Ref_Number

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('job_ali:Ref_Number',job_ali:Ref_Number)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('job_ali:Ref_Number',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('job_ali:Ref_Number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('job_ali:Ref_Number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
AddPrintButton  Routine
  packet = clip(packet) & |
    '<<button type="button" name="printhistory" id="printhistory" class="button-entryfield"<13,10>'&|
    'onclick="window.open(''BouncerHistory?showall=1&rnd='' + Math.floor((Math.random()*1000)+1),''_blank'');" title="Click here to print Bouncer History Report">History Report<</button><13,10>'&|
    ''
