

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER240.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER111.INC'),ONCE        !Req'd for module callout resolution
                     END


FormStockOrder       PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
LocRefNumber         LONG                                  !
locQuantity          LONG                                  !
locSupplier          STRING(30)                            !
locComments          STRING(255)                           !
locUsage0_7          LONG                                  !
locUsage0_30         LONG                                  !
locUsage31_60        LONG                                  !
locUsage61_90        LONG                                  !
locAverageDailyUse   LONG                                  !
locAverageText       STRING(30)                            !
FilesOpened     Long
STOMODEL_ALIAS::State  USHORT
SUPPLIER::State  USHORT
STOCK::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormStockOrder')
  loc:formname = 'FormStockOrder_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormStockOrder','')
    p_web._DivHeader('FormStockOrder',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormStockOrder',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormStockOrder',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormStockOrder',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormStockOrder',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormStockOrder',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormStockOrder',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(STOMODEL_ALIAS)
  p_web._OpenFile(SUPPLIER)
  p_web._OpenFile(STOCK)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(STOMODEL_ALIAS)
  p_Web._CloseFile(SUPPLIER)
  p_Web._CloseFile(STOCK)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormStockOrder_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('sto:Part_Number')
    p_web.SetPicture('sto:Part_Number','@s30')
  End
  p_web.SetSessionPicture('sto:Part_Number','@s30')
  If p_web.IfExistsValue('sto:Description')
    p_web.SetPicture('sto:Description','@s30')
  End
  p_web.SetSessionPicture('sto:Description','@s30')
  If p_web.IfExistsValue('sto:Sale_Cost')
    p_web.SetPicture('sto:Sale_Cost','@n_14.2')
  End
  p_web.SetSessionPicture('sto:Sale_Cost','@n_14.2')
  If p_web.IfExistsValue('locQuantity')
    p_web.SetPicture('locQuantity','@n_7')
  End
  p_web.SetSessionPicture('locQuantity','@n_7')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('sto:Part_Number',sto:Part_Number)
  p_web.SetSessionValue('sto:Description',sto:Description)
  p_web.SetSessionValue('locUsage0_7',locUsage0_7)
  p_web.SetSessionValue('locUsage0_30',locUsage0_30)
  p_web.SetSessionValue('locUsage31_60',locUsage31_60)
  p_web.SetSessionValue('locUsage61_90',locUsage61_90)
  p_web.SetSessionValue('locAverageText',locAverageText)
  p_web.SetSessionValue('sto:Sale_Cost',sto:Sale_Cost)
  p_web.SetSessionValue('locQuantity',locQuantity)
  p_web.SetSessionValue('locComments',locComments)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('sto:Part_Number')
    sto:Part_Number = p_web.GetValue('sto:Part_Number')
    p_web.SetSessionValue('sto:Part_Number',sto:Part_Number)
  End
  if p_web.IfExistsValue('sto:Description')
    sto:Description = p_web.GetValue('sto:Description')
    p_web.SetSessionValue('sto:Description',sto:Description)
  End
  if p_web.IfExistsValue('locUsage0_7')
    locUsage0_7 = p_web.GetValue('locUsage0_7')
    p_web.SetSessionValue('locUsage0_7',locUsage0_7)
  End
  if p_web.IfExistsValue('locUsage0_30')
    locUsage0_30 = p_web.GetValue('locUsage0_30')
    p_web.SetSessionValue('locUsage0_30',locUsage0_30)
  End
  if p_web.IfExistsValue('locUsage31_60')
    locUsage31_60 = p_web.GetValue('locUsage31_60')
    p_web.SetSessionValue('locUsage31_60',locUsage31_60)
  End
  if p_web.IfExistsValue('locUsage61_90')
    locUsage61_90 = p_web.GetValue('locUsage61_90')
    p_web.SetSessionValue('locUsage61_90',locUsage61_90)
  End
  if p_web.IfExistsValue('locAverageText')
    locAverageText = p_web.GetValue('locAverageText')
    p_web.SetSessionValue('locAverageText',locAverageText)
  End
  if p_web.IfExistsValue('sto:Sale_Cost')
    sto:Sale_Cost = p_web.dformat(clip(p_web.GetValue('sto:Sale_Cost')),'@n_14.2')
    p_web.SetSessionValue('sto:Sale_Cost',sto:Sale_Cost)
  End
  if p_web.IfExistsValue('locQuantity')
    locQuantity = p_web.dformat(clip(p_web.GetValue('locQuantity')),'@n_7')
    p_web.SetSessionValue('locQuantity',locQuantity)
  End
  if p_web.IfExistsValue('locComments')
    locComments = p_web.GetValue('locComments')
    p_web.SetSessionValue('locComments',locComments)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormStockOrder_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  ! Store return URL
  IF (p_web.IfExistsValue('ReturnURL'))
      p_web.StoreValue('ReturnURL')
  END 
  ! Clear values
  p_web.SSV('locQuantity',1)
  p_web.SSV('locComments','')
  
  !attempt to get at BrowseID from BrowseStoModel - did not work - still the delay
  !this is a work round - I can collect and set the stm:recordNumber
  if p_web.gsv('BrowseID') = 'STOMODEL' THEN
      Access:StoModel_Alias.clearkey(stm_ali:RecordNumberKey)
      stm_ali:RecordNumber = p_web.gsv('BrowseID2')
      if access:StoModel_Alias.fetch(stm_ali:RecordNumberKey)
          !error
      END
      LocRefNumber = stm_ali:Ref_Number
  ELSE
        LocRefNumber = p_web.gsv('BrowseID')
        BHAddToDebugLog('FormStockOrder: BrowseID = ' & p_web.GSV('BrowseID'))
  END
  
  Access:STOCK.ClearKey(sto:Ref_Number_Key)
  sto:Ref_Number = LocRefNumber       !was p_web.gsv('BrowseID')  !was p_web.GSV('sto:Ref_Number')
    IF (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
        p_web.FileToSessionQueue(STOCK)
  
  
        VodacomClass.StockUsage(sto:Ref_Number,locUsage0_7,locUsage0_30, |
            locUsage31_60,locUsage61_90,locAverageDailyUse)
  
        If locAverageDailyUse < 1
            locAverageText = '< 1'
        Else!If average_temp < 1
            locAverageText = Int(locAverageDailyUse)
        End!If average_temp < 1
  
        p_web.SSV('locUsage0_7',locUsage0_7)
        p_web.SSV('locUsage0_30',locUsage0_30)
        p_web.SSV('locUsage31_60',locUsage31_60)
        p_web.SSV('locUsage61_90',locUsage61_90)
        p_web.SSV('locAverageText',locAverageText)
    ELSE
  
  
    END
    
    ! #13128 Error if part not selected. (DBH: 18/03/2014)
    IF (p_web.GSV('BrowseID') = '')
        CreateScript(packet,'alert("Please select a part.");window.open("' & p_web.GSV('ReturnURL') & '","_self")')
        DO SendPacket
    END ! IF
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locUsage0_7 = p_web.RestoreValue('locUsage0_7')
 locUsage0_30 = p_web.RestoreValue('locUsage0_30')
 locUsage31_60 = p_web.RestoreValue('locUsage31_60')
 locUsage61_90 = p_web.RestoreValue('locUsage61_90')
 locAverageText = p_web.RestoreValue('locAverageText')
 locQuantity = p_web.RestoreValue('locQuantity')
 locComments = p_web.RestoreValue('locComments')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('ReturnURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormStockOrder_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormStockOrder_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormStockOrder_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('ReturnURL')
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormStockOrder" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormStockOrder" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormStockOrder" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Order Stock') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Order Stock',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormStockOrder">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormStockOrder" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormStockOrder')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Part Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Order Details') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormStockOrder')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormStockOrder'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormStockOrder')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Part Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormStockOrder_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:Part_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:Part_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sto:Part_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:Description
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:Description
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sto:Description
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::txtUsage
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::txtUsage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::txtUsage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locUsage0_7
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locUsage0_7
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locUsage0_7
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locUsage0_30
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locUsage0_30
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locUsage0_30
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locUsage31_60
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locUsage31_60
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locUsage31_60
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locUsage61_90
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locUsage61_90
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locUsage61_90
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAverageText
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAverageText
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locAverageText
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:Sale_Cost
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:Sale_Cost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sto:Sale_Cost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Order Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormStockOrder_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Order Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Order Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Order Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Order Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locQuantity
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locQuantity
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locQuantity
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& ' rowspan="3">'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locComments
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&' valign="top" rowspan="3">'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locComments
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&' rowspan="3">'
      loc:columncounter += 1
      do SendPacket
      do Comment::locComments
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::sto:Part_Number  Routine
  p_web._DivHeader('FormStockOrder_' & p_web._nocolon('sto:Part_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Part Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:Part_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:Part_Number',p_web.GetValue('NewValue'))
    sto:Part_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = sto:Part_Number
    do Value::sto:Part_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sto:Part_Number',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sto:Part_Number = p_web.GetValue('Value')
  End

Value::sto:Part_Number  Routine
  p_web._DivHeader('FormStockOrder_' & p_web._nocolon('sto:Part_Number') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- sto:Part_Number
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Part_Number'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::sto:Part_Number  Routine
    loc:comment = p_web.Translate('Check before Continuing')
  p_web._DivHeader('FormStockOrder_' & p_web._nocolon('sto:Part_Number') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sto:Description  Routine
  p_web._DivHeader('FormStockOrder_' & p_web._nocolon('sto:Description') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Description')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:Description  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:Description',p_web.GetValue('NewValue'))
    sto:Description = p_web.GetValue('NewValue') !FieldType= STRING Field = sto:Description
    do Value::sto:Description
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sto:Description',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sto:Description = p_web.GetValue('Value')
  End

Value::sto:Description  Routine
  p_web._DivHeader('FormStockOrder_' & p_web._nocolon('sto:Description') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- sto:Description
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Description'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::sto:Description  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockOrder_' & p_web._nocolon('sto:Description') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::txtUsage  Routine
  p_web._DivHeader('FormStockOrder_' & p_web._nocolon('txtUsage') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Usage')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::txtUsage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtUsage',p_web.GetValue('NewValue'))
    do Value::txtUsage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtUsage  Routine
  p_web._DivHeader('FormStockOrder_' & p_web._nocolon('txtUsage') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::txtUsage  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockOrder_' & p_web._nocolon('txtUsage') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locUsage0_7  Routine
  p_web._DivHeader('FormStockOrder_' & p_web._nocolon('locUsage0_7') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('0 - 7 Days')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locUsage0_7  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locUsage0_7',p_web.GetValue('NewValue'))
    locUsage0_7 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locUsage0_7
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locUsage0_7',p_web.GetValue('Value'))
    locUsage0_7 = p_web.GetValue('Value')
  End

Value::locUsage0_7  Routine
  p_web._DivHeader('FormStockOrder_' & p_web._nocolon('locUsage0_7') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locUsage0_7
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locUsage0_7'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locUsage0_7  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockOrder_' & p_web._nocolon('locUsage0_7') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locUsage0_30  Routine
  p_web._DivHeader('FormStockOrder_' & p_web._nocolon('locUsage0_30') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('0 - 30 Days')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locUsage0_30  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locUsage0_30',p_web.GetValue('NewValue'))
    locUsage0_30 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locUsage0_30
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locUsage0_30',p_web.GetValue('Value'))
    locUsage0_30 = p_web.GetValue('Value')
  End

Value::locUsage0_30  Routine
  p_web._DivHeader('FormStockOrder_' & p_web._nocolon('locUsage0_30') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locUsage0_30
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locUsage0_30'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locUsage0_30  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockOrder_' & p_web._nocolon('locUsage0_30') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locUsage31_60  Routine
  p_web._DivHeader('FormStockOrder_' & p_web._nocolon('locUsage31_60') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('31 - 60 Days')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locUsage31_60  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locUsage31_60',p_web.GetValue('NewValue'))
    locUsage31_60 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locUsage31_60
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locUsage31_60',p_web.GetValue('Value'))
    locUsage31_60 = p_web.GetValue('Value')
  End

Value::locUsage31_60  Routine
  p_web._DivHeader('FormStockOrder_' & p_web._nocolon('locUsage31_60') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locUsage31_60
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locUsage31_60'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locUsage31_60  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockOrder_' & p_web._nocolon('locUsage31_60') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locUsage61_90  Routine
  p_web._DivHeader('FormStockOrder_' & p_web._nocolon('locUsage61_90') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('61 - 90 Days')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locUsage61_90  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locUsage61_90',p_web.GetValue('NewValue'))
    locUsage61_90 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locUsage61_90
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locUsage61_90',p_web.GetValue('Value'))
    locUsage61_90 = p_web.GetValue('Value')
  End

Value::locUsage61_90  Routine
  p_web._DivHeader('FormStockOrder_' & p_web._nocolon('locUsage61_90') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locUsage61_90
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locUsage61_90'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locUsage61_90  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockOrder_' & p_web._nocolon('locUsage61_90') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locAverageText  Routine
  p_web._DivHeader('FormStockOrder_' & p_web._nocolon('locAverageText') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Average Daily Use')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locAverageText  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAverageText',p_web.GetValue('NewValue'))
    locAverageText = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAverageText
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locAverageText',p_web.GetValue('Value'))
    locAverageText = p_web.GetValue('Value')
  End

Value::locAverageText  Routine
  p_web._DivHeader('FormStockOrder_' & p_web._nocolon('locAverageText') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locAverageText
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locAverageText'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locAverageText  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockOrder_' & p_web._nocolon('locAverageText') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sto:Sale_Cost  Routine
  p_web._DivHeader('FormStockOrder_' & p_web._nocolon('sto:Sale_Cost') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Current Price')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:Sale_Cost  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:Sale_Cost',p_web.GetValue('NewValue'))
    sto:Sale_Cost = p_web.GetValue('NewValue') !FieldType= REAL Field = sto:Sale_Cost
    do Value::sto:Sale_Cost
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sto:Sale_Cost',p_web.dFormat(p_web.GetValue('Value'),'@n_14.2'))
    sto:Sale_Cost = p_web.Dformat(p_web.GetValue('Value'),'@n_14.2') !
  End

Value::sto:Sale_Cost  Routine
  p_web._DivHeader('FormStockOrder_' & p_web._nocolon('sto:Sale_Cost') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- sto:Sale_Cost
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(format(p_web.GetSessionValue('sto:Sale_Cost'),'@n_14.2')) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::sto:Sale_Cost  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockOrder_' & p_web._nocolon('sto:Sale_Cost') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locQuantity  Routine
  p_web._DivHeader('FormStockOrder_' & p_web._nocolon('locQuantity') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Quantity')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locQuantity  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locQuantity',p_web.GetValue('NewValue'))
    locQuantity = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locQuantity
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locQuantity',p_web.dFormat(p_web.GetValue('Value'),'@n_7'))
    locQuantity = p_web.Dformat(p_web.GetValue('Value'),'@n_7') !
  End
  If locQuantity = ''
    loc:Invalid = 'locQuantity'
    loc:alert = p_web.translate('Quantity') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::locQuantity
  do SendAlert

Value::locQuantity  Routine
  p_web._DivHeader('FormStockOrder_' & p_web._nocolon('locQuantity') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locQuantity
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locQuantity')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locQuantity = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locQuantity'',''formstockorder_locquantity_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locQuantity',p_web.GetSessionValue('locQuantity'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n_7',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockOrder_' & p_web._nocolon('locQuantity') & '_value')

Comment::locQuantity  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockOrder_' & p_web._nocolon('locQuantity') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locComments  Routine
  p_web._DivHeader('FormStockOrder_' & p_web._nocolon('locComments') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Comments')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locComments  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locComments',p_web.GetValue('NewValue'))
    locComments = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locComments
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locComments',p_web.GetValue('Value'))
    locComments = p_web.GetValue('Value')
  End
    locComments = Upper(locComments)
    p_web.SetSessionValue('locComments',locComments)
  do Value::locComments
  do SendAlert

Value::locComments  Routine
  p_web._DivHeader('FormStockOrder_' & p_web._nocolon('locComments') & '_value','adiv')
  loc:extra = ''
  ! --- TEXT --- locComments
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('locComments')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locComments'',''formstockorder_loccomments_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('locComments',p_web.GetSessionValue('locComments'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(locComments),,Net:Web:Control) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockOrder_' & p_web._nocolon('locComments') & '_value')

Comment::locComments  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockOrder_' & p_web._nocolon('locComments') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormStockOrder_locQuantity_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locQuantity
      else
        do Value::locQuantity
      end
  of lower('FormStockOrder_locComments_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locComments
      else
        do Value::locComments
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormStockOrder_form:ready_',1)
  p_web.SetSessionValue('FormStockOrder_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormStockOrder',0)

PreCopy  Routine
  p_web.SetValue('FormStockOrder_form:ready_',1)
  p_web.SetSessionValue('FormStockOrder_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormStockOrder',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormStockOrder_form:ready_',1)
  p_web.SetSessionValue('FormStockOrder_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormStockOrder:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormStockOrder_form:ready_',1)
  p_web.SetSessionValue('FormStockOrder_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormStockOrder:Primed',0)
  p_web.setsessionvalue('showtab_FormStockOrder',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormStockOrder_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  ! Create Order
  IF (p_web.GSV('sto:Ref_Number') <> '')
      IF (p_web.GSV('sto:Sundry_Item') = 'YES')
          loc:Alert = 'You cannot order Sundry Items.'
          loc:Invalid = 'sto:Part_Number'
          EXIT
      END
  
      createWebOrder(p_web.GSV('BookingAccount'),p_web.GSV('sto:Part_Number'),p_web.GSV('sto:Description'), |
          p_web.GSV('locQuantity'),p_web.GSV('sto:Retail_Cost'))
  END
  p_web.DeleteSessionValue('FormStockOrder_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
        If locQuantity = ''
          loc:Invalid = 'locQuantity'
          loc:alert = p_web.translate('Quantity') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
          locComments = Upper(locComments)
          p_web.SetSessionValue('locComments',locComments)
        If loc:Invalid <> '' then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormStockOrder:Primed',0)
  p_web.StoreValue('sto:Part_Number')
  p_web.StoreValue('sto:Description')
  p_web.StoreValue('')
  p_web.StoreValue('locUsage0_7')
  p_web.StoreValue('locUsage0_30')
  p_web.StoreValue('locUsage31_60')
  p_web.StoreValue('locUsage61_90')
  p_web.StoreValue('locAverageText')
  p_web.StoreValue('sto:Sale_Cost')
  p_web.StoreValue('locQuantity')
  p_web.StoreValue('locComments')
