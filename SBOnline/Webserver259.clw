

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER259.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER258.INC'),ONCE        !Req'd for module callout resolution
                     END


FormNewPassword      PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locNewPassword       STRING(30)                            !
locConfirmNewPassword STRING(30)                           !
locMobileNumber      STRING(20)                            !
FilesOpened     Long
USERS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormNewPassword')
  loc:formname = 'FormNewPassword_frm'
  WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormNewPassword','')
    p_web._DivHeader('FormNewPassword',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormNewPassword',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormNewPassword',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormNewPassword',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormNewPassword',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormNewPassword',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormNewPassword',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(USERS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(USERS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormNewPassword_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  If p_web.GSV('NewPasswordRequired') = 1
    loc:TabNumber += 1
  End
  If p_web.GSV('UserMobileRequired') 
    loc:TabNumber += 1
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locNewPassword',locNewPassword)
  p_web.SetSessionValue('locConfirmNewPassword',locConfirmNewPassword)
  p_web.SetSessionValue('locMobileNumber',locMobileNumber)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locNewPassword')
    locNewPassword = p_web.GetValue('locNewPassword')
    p_web.SetSessionValue('locNewPassword',locNewPassword)
  End
  if p_web.IfExistsValue('locConfirmNewPassword')
    locConfirmNewPassword = p_web.GetValue('locConfirmNewPassword')
    p_web.SetSessionValue('locConfirmNewPassword',locConfirmNewPassword)
  End
  if p_web.IfExistsValue('locMobileNumber')
    locMobileNumber = p_web.GetValue('locMobileNumber')
    p_web.SetSessionValue('locMobileNumber',locMobileNumber)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormNewPassword_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    p_web.SSV('locNewPassword','')
    p_web.SSV('locConfirmNewPassword','')
    p_web.SSV('locMobileNumber','')
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locNewPassword = p_web.RestoreValue('locNewPassword')
 locConfirmNewPassword = p_web.RestoreValue('locConfirmNewPassword')
 locMobileNumber = p_web.RestoreValue('locMobileNumber')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'IndexPage'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormNewPassword_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormNewPassword_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormNewPassword_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'LoginForm'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormNewPassword" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormNewPassword" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormNewPassword" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Update User Details') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Update User Details',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormNewPassword">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormNewPassword">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormNewPassword')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Login Details') & ''''
        If p_web.GSV('NewPasswordRequired') = 1
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('New Login Password Required') & ''''
        End
        If p_web.GSV('UserMobileRequired') 
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Mobile Number Required') & ''''
        End
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormNewPassword')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormNewPassword'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          If p_web.GSV('NewPasswordRequired') = 1
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          End
          If p_web.GSV('UserMobileRequired') 
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          End
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormNewPassword')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
    if p_web.GSV('NewPasswordRequired') = 1
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
    end
    if p_web.GSV('UserMobileRequired') 
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
    end
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Login Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormNewPassword_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Login Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Login Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Login Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Login Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormTable')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::txtCompany
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::txtUser
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
  If p_web.GSV('NewPasswordRequired') = 1
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('New Login Password Required') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormNewPassword_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('New Login Password Required')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('New Login Password Required')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('New Login Password Required')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('New Login Password Required')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormTable')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locNewPassword
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locNewPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locConfirmNewPassword
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locConfirmNewPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab2  Routine
  If p_web.GSV('UserMobileRequired') 
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Mobile Number Required') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormNewPassword_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Mobile Number Required')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Mobile Number Required')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Mobile Number Required')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Mobile Number Required')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormTable')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locMobileNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locMobileNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end


Validate::txtCompany  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtCompany',p_web.GetValue('NewValue'))
    do Value::txtCompany
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtCompany  Routine
  p_web._DivHeader('FormNewPassword_' & p_web._nocolon('txtCompany') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web.Translate(p_web.GSV('LoginDetails1'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::txtUser  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtUser',p_web.GetValue('NewValue'))
    do Value::txtUser
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtUser  Routine
  p_web._DivHeader('FormNewPassword_' & p_web._nocolon('txtUser') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web.Translate(p_web.GSV('LoginDetails2'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locNewPassword  Routine
  p_web._DivHeader('FormNewPassword_' & p_web._nocolon('locNewPassword') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('New Password:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locNewPassword  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locNewPassword',p_web.GetValue('NewValue'))
    locNewPassword = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locNewPassword
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locNewPassword',p_web.GetValue('Value'))
    locNewPassword = p_web.GetValue('Value')
  End
  If locNewPassword = '' and p_web.GSV('NewPasswordRequired') 
    loc:Invalid = 'locNewPassword'
    loc:alert = p_web.translate('New Password:') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locNewPassword = Upper(locNewPassword)
    p_web.SetSessionValue('locNewPassword',locNewPassword)
  do Value::locNewPassword
  do SendAlert

Value::locNewPassword  Routine
  p_web._DivHeader('FormNewPassword_' & p_web._nocolon('locNewPassword') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locNewPassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If (p_web.GSV('NewPasswordRequired') )
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('locNewPassword')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locNewPassword = '' and (p_web.GSV('NewPasswordRequired') )
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locNewPassword'',''formnewpassword_locnewpassword_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','locNewPassword',p_web.GetSessionValueFormat('locNewPassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormNewPassword_' & p_web._nocolon('locNewPassword') & '_value')


Prompt::locConfirmNewPassword  Routine
  p_web._DivHeader('FormNewPassword_' & p_web._nocolon('locConfirmNewPassword') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Confirm New Password:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locConfirmNewPassword  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locConfirmNewPassword',p_web.GetValue('NewValue'))
    locConfirmNewPassword = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locConfirmNewPassword
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locConfirmNewPassword',p_web.GetValue('Value'))
    locConfirmNewPassword = p_web.GetValue('Value')
  End
  If locConfirmNewPassword = '' and p_web.GSV('NewPasswordRequired') 
    loc:Invalid = 'locConfirmNewPassword'
    loc:alert = p_web.translate('Confirm New Password:') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locConfirmNewPassword = Upper(locConfirmNewPassword)
    p_web.SetSessionValue('locConfirmNewPassword',locConfirmNewPassword)
  do Value::locConfirmNewPassword
  do SendAlert

Value::locConfirmNewPassword  Routine
  p_web._DivHeader('FormNewPassword_' & p_web._nocolon('locConfirmNewPassword') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locConfirmNewPassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If (p_web.GSV('NewPasswordRequired') )
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('locConfirmNewPassword')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locConfirmNewPassword = '' and (p_web.GSV('NewPasswordRequired') )
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locConfirmNewPassword'',''formnewpassword_locconfirmnewpassword_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','locConfirmNewPassword',p_web.GetSessionValueFormat('locConfirmNewPassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormNewPassword_' & p_web._nocolon('locConfirmNewPassword') & '_value')


Prompt::locMobileNumber  Routine
  p_web._DivHeader('FormNewPassword_' & p_web._nocolon('locMobileNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Mobile Number:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locMobileNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locMobileNumber',p_web.GetValue('NewValue'))
    locMobileNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locMobileNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locMobileNumber',p_web.GetValue('Value'))
    locMobileNumber = p_web.GetValue('Value')
  End
  If locMobileNumber = '' and p_web.GSV('UserMobileRequired') 
    loc:Invalid = 'locMobileNumber'
    loc:alert = p_web.translate('Mobile Number:') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locMobileNumber = Upper(locMobileNumber)
    p_web.SetSessionValue('locMobileNumber',locMobileNumber)
  do Value::locMobileNumber
  do SendAlert

Value::locMobileNumber  Routine
  p_web._DivHeader('FormNewPassword_' & p_web._nocolon('locMobileNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locMobileNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If (p_web.GSV('UserMobileRequired') )
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('locMobileNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locMobileNumber = '' and (p_web.GSV('UserMobileRequired') )
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locMobileNumber'',''formnewpassword_locmobilenumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locMobileNumber',p_web.GetSessionValueFormat('locMobileNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormNewPassword_' & p_web._nocolon('locMobileNumber') & '_value')


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormNewPassword_locNewPassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locNewPassword
      else
        do Value::locNewPassword
      end
  of lower('FormNewPassword_locConfirmNewPassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locConfirmNewPassword
      else
        do Value::locConfirmNewPassword
      end
  of lower('FormNewPassword_locMobileNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locMobileNumber
      else
        do Value::locMobileNumber
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormNewPassword_form:ready_',1)
  p_web.SetSessionValue('FormNewPassword_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormNewPassword',0)

PreCopy  Routine
  p_web.SetValue('FormNewPassword_form:ready_',1)
  p_web.SetSessionValue('FormNewPassword_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormNewPassword',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormNewPassword_form:ready_',1)
  p_web.SetSessionValue('FormNewPassword_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormNewPassword:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormNewPassword_form:ready_',1)
  p_web.SetSessionValue('FormNewPassword_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormNewPassword:Primed',0)
  p_web.setsessionvalue('showtab_FormNewPassword',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If p_web.GSV('NewPasswordRequired') = 1
  End
  If p_web.GSV('UserMobileRequired') 
  End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord
    IF (loc:Invalid)
        EXIT
    END
    IF (p_web.GSV('NewPasswordRequired') = 1)
        ! Do the passwords match?
        IF (p_web.GSV('locNewPassword') <> p_web.GSV('locConfirmNewPassword'))
            loc:Invalid = 'locConfirmNewPassword'
            loc:Alert = 'The entered passwords do not match.'
            EXIT
        END
  
        ! Has the password changed?
        IF (p_web.GSV('locNewPassword') = p_web.GSV('loc:Password'))
            loc:Invalid = 'locNewPassword'
            loc:Alert = 'You have not entered a NEW password.'
            EXIT
        END !IF
        ! Is the password already taken?
        Access:USERS.Clearkey(use:Password_Key)
        use:Password = p_web.GSV('locNewPassword')
        IF (Access:USERS.Tryfetch(use:Password_Key) = Level:Benign)
            loc:Invalid = 'locNewPassword'
            loc:Alert = 'The selected password is already in use. Please try again.'
            EXIT
        END  ! IF
  
    END ! IF
  
    IF (p_web.GSV('UserMobileRequired') = 1)
        IF (NOT PassMobileNumberFormat(p_web.GSV('locMobileNumber')))
            loc:Alert = 'The Mobile Number Format Is Not Correct'
            loc:Invalid = 'locMobileNumber'
            EXIT
        END ! IF
        ! All ok, get original user and update it
        Access:USERS.Clearkey(use:Password_Key)
        use:Password = p_web.GSV('loc:Password')
        IF (Access:USERS.Tryfetch(use:Password_Key))
            loc:Alert = 'An Error Has Occurred'
            loc:Invalid = 'locNewPassword'
            EXIT
        END !IF
    END ! IF
  
    ! All ok, get original user and update it
    Access:USERS.Clearkey(use:Password_Key)
    use:Password = p_web.GSV('loc:Password')
    IF (Access:USERS.Tryfetch(use:Password_Key))
        loc:Alert = 'An Error Has Occurred'
        loc:Invalid = 'locNewPassword'
        EXIT
    END !IF
  
    IF (p_web.GSV('NewPasswordRequired') = 1)
        IF (p_web.GSV('locNewPassword') <> '')
            use:Password = p_web.GSV('locNewPassword')
            use:PasswordLastChanged = TODAY()
        END ! IF
    END ! IF
  
    IF (p_web.GSV('UserMobileRequired') = 1)
        IF (p_web.GSV('locMobileNumber') <> '')
            use:MobileNumber = p_web.GSV('locMobileNumber')
        END ! IF
    END ! IF
  
    Access:USERS.TryUpdate()
  
    SetLoginDefaults(p_web)

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormNewPassword_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormNewPassword_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
  If p_web.GSV('NewPasswordRequired') = 1
    loc:InvalidTab += 1
        If locNewPassword = '' and p_web.GSV('NewPasswordRequired') 
          loc:Invalid = 'locNewPassword'
          loc:alert = p_web.translate('New Password:') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locNewPassword = Upper(locNewPassword)
          p_web.SetSessionValue('locNewPassword',locNewPassword)
        If loc:Invalid <> '' then exit.
        If locConfirmNewPassword = '' and p_web.GSV('NewPasswordRequired') 
          loc:Invalid = 'locConfirmNewPassword'
          loc:alert = p_web.translate('Confirm New Password:') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locConfirmNewPassword = Upper(locConfirmNewPassword)
          p_web.SetSessionValue('locConfirmNewPassword',locConfirmNewPassword)
        If loc:Invalid <> '' then exit.
  End
  ! tab = 3
  If p_web.GSV('UserMobileRequired') 
    loc:InvalidTab += 1
        If locMobileNumber = '' and p_web.GSV('UserMobileRequired') 
          loc:Invalid = 'locMobileNumber'
          loc:alert = p_web.translate('Mobile Number:') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locMobileNumber = Upper(locMobileNumber)
          p_web.SetSessionValue('locMobileNumber',locMobileNumber)
        If loc:Invalid <> '' then exit.
  End
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormNewPassword:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locNewPassword')
  p_web.StoreValue('locConfirmNewPassword')
  p_web.StoreValue('locMobileNumber')
