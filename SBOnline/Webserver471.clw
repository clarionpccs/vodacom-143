

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER471.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER285.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER431.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER457.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER463.INC'),ONCE        !Req'd for module callout resolution
                     END


FormHandlingFeeExportReport PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locAllAccounts       LONG                                  !
locGenericAccounts   LONG                                  !
locAllChargeTypes    LONG                                  !
locStartDate         DATE                                  !
locEndDate           DATE                                  !
locReportOrder       LONG                                  !
locAllManufacturers  LONG                                  !
FilesOpened     Long
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormHandlingFeeExportReport')
  loc:formname = 'FormHandlingFeeExportReport_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormHandlingFeeExportReport','')
    p_web._DivHeader('FormHandlingFeeExportReport',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormHandlingFeeExportReport',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormHandlingFeeExportReport',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormHandlingFeeExportReport',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormHandlingFeeExportReport',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormHandlingFeeExportReport',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormHandlingFeeExportReport',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormHandlingFeeExportReport_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('locStartDate')
    p_web.SetPicture('locStartDate','@d06')
  End
  p_web.SetSessionPicture('locStartDate','@d06')
  If p_web.IfExistsValue('locEndDate')
    p_web.SetPicture('locEndDate','@d06')
  End
  p_web.SetSessionPicture('locEndDate','@d06')
AfterLookup Routine
  loc:TabNumber = -1
  If p_web.GetValue('tab') = 1
    loc:TabNumber += 1
  End
  If p_web.GetValue('tab') = 2
    loc:TabNumber += 1
  End
  If p_web.GetValue('tab') = 3
    loc:TabNumber += 1
  End
  If p_web.GetValue('tab') = 4
    loc:TabNumber += 1
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locAllAccounts',locAllAccounts)
  p_web.SetSessionValue('locGenericAccounts',locGenericAccounts)
  p_web.SetSessionValue('locAllChargeTypes',locAllChargeTypes)
  p_web.SetSessionValue('locAllManufacturers',locAllManufacturers)
  p_web.SetSessionValue('locStartDate',locStartDate)
  p_web.SetSessionValue('locEndDate',locEndDate)
  p_web.SetSessionValue('locReportOrder',locReportOrder)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locAllAccounts')
    locAllAccounts = p_web.GetValue('locAllAccounts')
    p_web.SetSessionValue('locAllAccounts',locAllAccounts)
  End
  if p_web.IfExistsValue('locGenericAccounts')
    locGenericAccounts = p_web.GetValue('locGenericAccounts')
    p_web.SetSessionValue('locGenericAccounts',locGenericAccounts)
  End
  if p_web.IfExistsValue('locAllChargeTypes')
    locAllChargeTypes = p_web.GetValue('locAllChargeTypes')
    p_web.SetSessionValue('locAllChargeTypes',locAllChargeTypes)
  End
  if p_web.IfExistsValue('locAllManufacturers')
    locAllManufacturers = p_web.GetValue('locAllManufacturers')
    p_web.SetSessionValue('locAllManufacturers',locAllManufacturers)
  End
  if p_web.IfExistsValue('locStartDate')
    locStartDate = p_web.dformat(clip(p_web.GetValue('locStartDate')),'@d06')
    p_web.SetSessionValue('locStartDate',locStartDate)
  End
  if p_web.IfExistsValue('locEndDate')
    locEndDate = p_web.dformat(clip(p_web.GetValue('locEndDate')),'@d06')
    p_web.SetSessionValue('locEndDate',locEndDate)
  End
  if p_web.IfExistsValue('locReportOrder')
    locReportOrder = p_web.GetValue('locReportOrder')
    p_web.SetSessionValue('locReportOrder',locReportOrder)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormHandlingFeeExportReport_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    IF (p_web.GetValue('FirstTime') = 1)
        ClearTagFile(p_web)   
        ClearGenericTagFile(p_web,'C')
        ClearGenericTagFile(p_web,kSubAccount)
        ClearGenericTagFile(p_web,kGenericAccount)
        p_web.SSV('locAllAccounts',1)
        p_web.SSV('locGenericAccounts','')        
        p_web.SSV('locAllChargeTypes',1)
        p_web.SSV('locStartDate',TODAY())
        p_web.SSV('locEndDate',TODAY())
        p_web.SSV('locAllManufacturers',1)
        p_web.SSV('locReportOrder',0)
    END ! IF
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locAllAccounts = p_web.RestoreValue('locAllAccounts')
 locGenericAccounts = p_web.RestoreValue('locGenericAccounts')
 locAllChargeTypes = p_web.RestoreValue('locAllChargeTypes')
 locAllManufacturers = p_web.RestoreValue('locAllManufacturers')
 locStartDate = p_web.RestoreValue('locStartDate')
 locEndDate = p_web.RestoreValue('locEndDate')
 locReportOrder = p_web.RestoreValue('locReportOrder')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferFormHandlingFeeExportReport')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormHandlingFeeExportReport_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormHandlingFeeExportReport_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormHandlingFeeExportReport_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'FormReports'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormHandlingFeeExportReport" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormHandlingFeeExportReport" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormHandlingFeeExportReport" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Handling & Exchange Fee Report') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Handling & Exchange Fee Report',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormHandlingFeeExportReport">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormHandlingFeeExportReport" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormHandlingFeeExportReport')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
        If p_web.GetValue('tab') = 1
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Trade Account') & ''''
        End
        If p_web.GetValue('tab') = 2
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Charge Types') & ''''
        End
        If p_web.GetValue('tab') = 3
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Manufacturers') & ''''
        End
        If p_web.GetValue('tab') = 4
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Final Settings') & ''''
        End
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormHandlingFeeExportReport')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormHandlingFeeExportReport'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormHandlingFeeExportReport_TagSubAccounts_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormHandlingFeeExportReport_TagWarChargeTypes_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormHandlingFeeExportReport_BrowseTagManufacturers_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    ElsIf p_web.GetValue('tab') = 1
        p_web.SetValue('SelectField',clip(loc:formname) & '.locAllAccounts')
    ElsIf p_web.GetValue('tab') = 2
        p_web.SetValue('SelectField',clip(loc:formname) & '.locAllChargeTypes')
    ElsIf p_web.GetValue('tab') = 3
        p_web.SetValue('SelectField',clip(loc:formname) & '.locAllManufacturers')
    ElsIf p_web.GetValue('tab') = 4
        p_web.SetValue('SelectField',clip(loc:formname) & '.locStartDate')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
          If p_web.GetValue('tab') = 1
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          End
          If p_web.GetValue('tab') = 2
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          End
          If p_web.GetValue('tab') = 3
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab4'''
          End
          If p_web.GetValue('tab') = 4
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          End
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormHandlingFeeExportReport')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
    if p_web.GetValue('tab') = 1
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
    end
    if p_web.GetValue('tab') = 2
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
    end
    if p_web.GetValue('tab') = 3
      packet = clip(packet) & 'roundCorners(''tab4'');'&CRLF
    end
    if p_web.GetValue('tab') = 4
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
    end
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
  If p_web.GetValue('tab') = 1
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Trade Account') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormHandlingFeeExportReport_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Trade Account')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Trade Account')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Trade Account')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Trade Account')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAllAccounts
      do Value::locAllAccounts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locGenericAccounts
      do Value::locGenericAccounts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwAccounts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnUnTagAll
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::__line1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnNext
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab1  Routine
  If p_web.GetValue('tab') = 2
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Charge Types') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormHandlingFeeExportReport_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Charge Types')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Charge Types')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Charge Types')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Charge Types')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAllChargeTypes
      do Value::locAllChargeTypes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwChargeTypes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnUnTagAll2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::__line2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnBack2
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnNext2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab2  Routine
  If p_web.GetValue('tab') = 3
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel4">'&CRLF &|
                                    '  <div id="panel4Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Manufacturers') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel4Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormHandlingFeeExportReport_4">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Manufacturers')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab4" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab4">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Manufacturers')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Manufacturers')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Manufacturers')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAllManufacturers
      do Value::locAllManufacturers
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwManufacturers
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnUntagAll3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::__line3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnBack3
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnNext3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab3  Routine
  If p_web.GetValue('tab') = 4
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Final Settings') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormHandlingFeeExportReport_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Final Settings')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Final Settings')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Final Settings')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Final Settings')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locStartDate
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&400&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locStartDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&400&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::hidden
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locEndDate
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&400&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locEndDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locReportOrder
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&400&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&' colspan="4">'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locReportOrder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&400&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnPrintReport
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnExport
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::__line4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnBack4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end


Prompt::locAllAccounts  Routine
  p_web._DivHeader('FormHandlingFeeExportReport_' & p_web._nocolon('locAllAccounts') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('All Trade Accounts')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locAllAccounts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAllAccounts',p_web.GetValue('NewValue'))
    locAllAccounts = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAllAccounts
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locAllAccounts',p_web.GetValue('Value'))
    locAllAccounts = p_web.GetValue('Value')
  End
  do Value::locAllAccounts
  do SendAlert
  do Value::brwAccounts  !1
  do Prompt::locGenericAccounts
  do Value::locGenericAccounts  !1
  do Value::btnUnTagAll  !1

Value::locAllAccounts  Routine
  p_web._DivHeader('FormHandlingFeeExportReport_' & p_web._nocolon('locAllAccounts') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locAllAccounts
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locAllAccounts'',''formhandlingfeeexportreport_locallaccounts_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locAllAccounts')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locAllAccounts') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locAllAccounts',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormHandlingFeeExportReport_' & p_web._nocolon('locAllAccounts') & '_value')


Prompt::locGenericAccounts  Routine
  p_web._DivHeader('FormHandlingFeeExportReport_' & p_web._nocolon('locGenericAccounts') & '_prompt',Choose(p_web.GSV('locAllAccounts') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Generic Accounts')
  If p_web.GSV('locAllAccounts') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormHandlingFeeExportReport_' & p_web._nocolon('locGenericAccounts') & '_prompt')

Validate::locGenericAccounts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locGenericAccounts',p_web.GetValue('NewValue'))
    locGenericAccounts = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locGenericAccounts
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locGenericAccounts',p_web.GetValue('Value'))
    locGenericAccounts = p_web.GetValue('Value')
  End
  do Value::locGenericAccounts
  do SendAlert
  do Value::brwAccounts  !1

Value::locGenericAccounts  Routine
  p_web._DivHeader('FormHandlingFeeExportReport_' & p_web._nocolon('locGenericAccounts') & '_value',Choose(p_web.GSV('locAllAccounts') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAllAccounts') = 1)
  ! --- CHECKBOX --- locGenericAccounts
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locGenericAccounts'',''formhandlingfeeexportreport_locgenericaccounts_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locGenericAccounts')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locGenericAccounts') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locGenericAccounts',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormHandlingFeeExportReport_' & p_web._nocolon('locGenericAccounts') & '_value')


Validate::brwAccounts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwAccounts',p_web.GetValue('NewValue'))
    do Value::brwAccounts
  Else
    p_web.StoreValue('sub:RecordNumber')
  End

Value::brwAccounts  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('locAllAccounts') = 1,1,0))
  ! --- BROWSE ---  TagSubAccounts --
  p_web.SetValue('TagSubAccounts:NoForm',1)
  p_web.SetValue('TagSubAccounts:FormName',loc:formname)
  p_web.SetValue('TagSubAccounts:parentIs','Form')
  p_web.SetValue('_parentProc','FormHandlingFeeExportReport')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormHandlingFeeExportReport_TagSubAccounts_embedded_div')&'"><!-- Net:TagSubAccounts --></div><13,10>'
    p_web._DivHeader('FormHandlingFeeExportReport_' & lower('TagSubAccounts') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormHandlingFeeExportReport_' & lower('TagSubAccounts') & '_value')
  else
    packet = clip(packet) & '<!-- Net:TagSubAccounts --><13,10>'
  end
  do SendPacket


Validate::btnUnTagAll  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnUnTagAll',p_web.GetValue('NewValue'))
    do Value::btnUnTagAll
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    IF (p_web.GSV('locGenericAccounts') = 0)
        ClearGenericTagFile(p_web,kSubAccount)
    ELSE
        ClearGenericTagFile(p_web,kGenericAccount)
    END ! IF
    
  do SendAlert
  do Value::brwAccounts  !1

Value::btnUnTagAll  Routine
  p_web._DivHeader('FormHandlingFeeExportReport_' & p_web._nocolon('btnUnTagAll') & '_value',Choose(p_web.GSV('locAllAccounts') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAllAccounts') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnUnTagAll'',''formhandlingfeeexportreport_btnuntagall_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnUntagAll','UnTag All','MainButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormHandlingFeeExportReport_' & p_web._nocolon('btnUnTagAll') & '_value')


Validate::__line1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('__line1',p_web.GetValue('NewValue'))
    do Value::__line1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::__line1  Routine
  p_web._DivHeader('FormHandlingFeeExportReport_' & p_web._nocolon('__line1') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::btnNext  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnNext',p_web.GetValue('NewValue'))
    do Value::btnNext
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnNext  Routine
  p_web._DivHeader('FormHandlingFeeExportReport_' & p_web._nocolon('btnNext') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnNext','Next','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormHandlingFeeExportReport?' &'tab=2')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/listnext.png',,,,)

  do SendPacket
  p_web._DivFooter()


Prompt::locAllChargeTypes  Routine
  p_web._DivHeader('FormHandlingFeeExportReport_' & p_web._nocolon('locAllChargeTypes') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('All Charge Types')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locAllChargeTypes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAllChargeTypes',p_web.GetValue('NewValue'))
    locAllChargeTypes = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAllChargeTypes
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locAllChargeTypes',p_web.GetValue('Value'))
    locAllChargeTypes = p_web.GetValue('Value')
  End
  do Value::locAllChargeTypes
  do SendAlert
  do Value::brwChargeTypes  !1
  do Value::btnUnTagAll2  !1

Value::locAllChargeTypes  Routine
  p_web._DivHeader('FormHandlingFeeExportReport_' & p_web._nocolon('locAllChargeTypes') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locAllChargeTypes
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locAllChargeTypes'',''formhandlingfeeexportreport_locallchargetypes_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locAllChargeTypes')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locAllChargeTypes') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locAllChargeTypes',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormHandlingFeeExportReport_' & p_web._nocolon('locAllChargeTypes') & '_value')


Validate::brwChargeTypes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwChargeTypes',p_web.GetValue('NewValue'))
    do Value::brwChargeTypes
  Else
    p_web.StoreValue('cha:Charge_Type')
  End

Value::brwChargeTypes  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('locAllChargeTypes') = 1,1,0))
  ! --- BROWSE ---  TagWarChargeTypes --
  p_web.SetValue('TagWarChargeTypes:NoForm',1)
  p_web.SetValue('TagWarChargeTypes:FormName',loc:formname)
  p_web.SetValue('TagWarChargeTypes:parentIs','Form')
  p_web.SetValue('_parentProc','FormHandlingFeeExportReport')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormHandlingFeeExportReport_TagWarChargeTypes_embedded_div')&'"><!-- Net:TagWarChargeTypes --></div><13,10>'
    p_web._DivHeader('FormHandlingFeeExportReport_' & lower('TagWarChargeTypes') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormHandlingFeeExportReport_' & lower('TagWarChargeTypes') & '_value')
  else
    packet = clip(packet) & '<!-- Net:TagWarChargeTypes --><13,10>'
  end
  do SendPacket


Validate::btnUnTagAll2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnUnTagAll2',p_web.GetValue('NewValue'))
    do Value::btnUnTagAll2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    ClearGenericTagFile(p_web,'C')
  do SendAlert
  do Value::brwChargeTypes  !1

Value::btnUnTagAll2  Routine
  p_web._DivHeader('FormHandlingFeeExportReport_' & p_web._nocolon('btnUnTagAll2') & '_value',Choose(p_web.GSV('locAllChargeTypes') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAllChargeTypes') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnUnTagAll2'',''formhandlingfeeexportreport_btnuntagall2_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnUnTagAll2','UnTag All','MainButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormHandlingFeeExportReport_' & p_web._nocolon('btnUnTagAll2') & '_value')


Validate::__line2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('__line2',p_web.GetValue('NewValue'))
    do Value::__line2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::__line2  Routine
  p_web._DivHeader('FormHandlingFeeExportReport_' & p_web._nocolon('__line2') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::btnBack2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnBack2',p_web.GetValue('NewValue'))
    do Value::btnBack2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnBack2  Routine
  p_web._DivHeader('FormHandlingFeeExportReport_' & p_web._nocolon('btnBack2') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnBack','Back','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormHandlingFeeExportReport?' &'tab=1')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/listback.png',,,,)

  do SendPacket
  p_web._DivFooter()


Validate::btnNext2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnNext2',p_web.GetValue('NewValue'))
    do Value::btnNext2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnNext2  Routine
  p_web._DivHeader('FormHandlingFeeExportReport_' & p_web._nocolon('btnNext2') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnNext','Next','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormHandlingFeeExportReport?' &'tab=3')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/listnext.png',,,,)

  do SendPacket
  p_web._DivFooter()


Prompt::locAllManufacturers  Routine
  p_web._DivHeader('FormHandlingFeeExportReport_' & p_web._nocolon('locAllManufacturers') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('All Manufacturers')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locAllManufacturers  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAllManufacturers',p_web.GetValue('NewValue'))
    locAllManufacturers = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAllManufacturers
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locAllManufacturers',p_web.GetValue('Value'))
    locAllManufacturers = p_web.GetValue('Value')
  End
  do Value::locAllManufacturers
  do SendAlert
  do Value::brwManufacturers  !1
  do Value::btnUntagAll3  !1

Value::locAllManufacturers  Routine
  p_web._DivHeader('FormHandlingFeeExportReport_' & p_web._nocolon('locAllManufacturers') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locAllManufacturers
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locAllManufacturers'',''formhandlingfeeexportreport_locallmanufacturers_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locAllManufacturers')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locAllManufacturers') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locAllManufacturers',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormHandlingFeeExportReport_' & p_web._nocolon('locAllManufacturers') & '_value')


Validate::brwManufacturers  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwManufacturers',p_web.GetValue('NewValue'))
    do Value::brwManufacturers
  Else
    p_web.StoreValue('man:RecordNumber')
  End

Value::brwManufacturers  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('locAllManufacturers') = 1,1,0))
  ! --- BROWSE ---  BrowseTagManufacturers --
  p_web.SetValue('BrowseTagManufacturers:NoForm',1)
  p_web.SetValue('BrowseTagManufacturers:FormName',loc:formname)
  p_web.SetValue('BrowseTagManufacturers:parentIs','Form')
  p_web.SetValue('_parentProc','FormHandlingFeeExportReport')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormHandlingFeeExportReport_BrowseTagManufacturers_embedded_div')&'"><!-- Net:BrowseTagManufacturers --></div><13,10>'
    p_web._DivHeader('FormHandlingFeeExportReport_' & lower('BrowseTagManufacturers') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormHandlingFeeExportReport_' & lower('BrowseTagManufacturers') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseTagManufacturers --><13,10>'
  end
  do SendPacket


Validate::btnUntagAll3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnUntagAll3',p_web.GetValue('NewValue'))
    do Value::btnUntagAll3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    ClearGenericTagFile(p_web,'M')  
  do SendAlert
  do Value::brwManufacturers  !1

Value::btnUntagAll3  Routine
  p_web._DivHeader('FormHandlingFeeExportReport_' & p_web._nocolon('btnUntagAll3') & '_value',Choose(p_web.GSV('locAllManufacturers') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAllManufacturers') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnUntagAll3'',''formhandlingfeeexportreport_btnuntagall3_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnUntagAll','UnTag All','MainButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormHandlingFeeExportReport_' & p_web._nocolon('btnUntagAll3') & '_value')


Validate::__line3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('__line3',p_web.GetValue('NewValue'))
    do Value::__line3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::__line3  Routine
  p_web._DivHeader('FormHandlingFeeExportReport_' & p_web._nocolon('__line3') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::btnBack3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnBack3',p_web.GetValue('NewValue'))
    do Value::btnBack3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnBack3  Routine
  p_web._DivHeader('FormHandlingFeeExportReport_' & p_web._nocolon('btnBack3') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnBack3','Back','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormHandlingFeeExportReport?' &'tab=2')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/listback.png',,,,)

  do SendPacket
  p_web._DivFooter()


Validate::btnNext3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnNext3',p_web.GetValue('NewValue'))
    do Value::btnNext3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnNext3  Routine
  p_web._DivHeader('FormHandlingFeeExportReport_' & p_web._nocolon('btnNext3') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnNext3','Next','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormHandlingFeeExportReport?' &'tab=4')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/listnext.png',,,,)

  do SendPacket
  p_web._DivFooter()


Prompt::locStartDate  Routine
  p_web._DivHeader('FormHandlingFeeExportReport_' & p_web._nocolon('locStartDate') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Start Date')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locStartDate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locStartDate',p_web.GetValue('NewValue'))
    locStartDate = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locStartDate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locStartDate',p_web.dFormat(p_web.GetValue('Value'),'@d06'))
    locStartDate = p_web.Dformat(p_web.GetValue('Value'),'@d06') !
  End
  If locStartDate = ''
    loc:Invalid = 'locStartDate'
    loc:alert = p_web.translate('Start Date') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locStartDate = Upper(locStartDate)
    p_web.SetSessionValue('locStartDate',locStartDate)
  do Value::locStartDate
  do SendAlert

Value::locStartDate  Routine
  p_web._DivHeader('FormHandlingFeeExportReport_' & p_web._nocolon('locStartDate') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locStartDate
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locStartDate')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locStartDate = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locStartDate'',''formhandlingfeeexportreport_locstartdate_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locStartDate',p_web.GetSessionValue('locStartDate'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06',loc:javascript,,) & '<13,10>'
  do SendPacket
  packet = clip(packet) & '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar' & |
                  '(locStartDate,''dd/mm/yyyy'',this); Date.disabled=true;' & |
                  'sv(''...'',''FormHandlingFeeExportReport_pickdate_value'',1,FieldValue(this,1));nextFocus(FormHandlingFeeExportReport_frm,'''',0);" ' & |
                  'value="Select Date" name="Date" type="button">...</button>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormHandlingFeeExportReport_' & p_web._nocolon('locStartDate') & '_value')


Validate::hidden  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('hidden',p_web.GetValue('NewValue'))
    do Value::hidden
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::hidden  Routine
  p_web._DivHeader('FormHandlingFeeExportReport_' & p_web._nocolon('hidden') & '_value','adiv')
  loc:extra = ''
  ! --- HIDDEN --- 
    packet = clip(packet) & p_web.CreateInput('hidden','',p_web.GetSessionValue('')) & '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locEndDate  Routine
  p_web._DivHeader('FormHandlingFeeExportReport_' & p_web._nocolon('locEndDate') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('End Date')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locEndDate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locEndDate',p_web.GetValue('NewValue'))
    locEndDate = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locEndDate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locEndDate',p_web.dFormat(p_web.GetValue('Value'),'@d06'))
    locEndDate = p_web.Dformat(p_web.GetValue('Value'),'@d06') !
  End
  If locEndDate = ''
    loc:Invalid = 'locEndDate'
    loc:alert = p_web.translate('End Date') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locEndDate = Upper(locEndDate)
    p_web.SetSessionValue('locEndDate',locEndDate)
  do Value::locEndDate
  do SendAlert

Value::locEndDate  Routine
  p_web._DivHeader('FormHandlingFeeExportReport_' & p_web._nocolon('locEndDate') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locEndDate
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locEndDate')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locEndDate = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locEndDate'',''formhandlingfeeexportreport_locenddate_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locEndDate',p_web.GetSessionValue('locEndDate'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06',loc:javascript,,) & '<13,10>'
  do SendPacket
  packet = clip(packet) & '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar' & |
                  '(locEndDate,''dd/mm/yyyy'',this); Date.disabled=true;' & |
                  'sv(''...'',''FormHandlingFeeExportReport_pickdate_value'',1,FieldValue(this,1));nextFocus(FormHandlingFeeExportReport_frm,'''',0);" ' & |
                  'value="Select Date" name="Date" type="button">...</button>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormHandlingFeeExportReport_' & p_web._nocolon('locEndDate') & '_value')


Prompt::locReportOrder  Routine
  p_web._DivHeader('FormHandlingFeeExportReport_' & p_web._nocolon('locReportOrder') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Report Order')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locReportOrder  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locReportOrder',p_web.GetValue('NewValue'))
    locReportOrder = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locReportOrder
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locReportOrder',p_web.GetValue('Value'))
    locReportOrder = p_web.GetValue('Value')
  End
  do Value::locReportOrder
  do SendAlert

Value::locReportOrder  Routine
  p_web._DivHeader('FormHandlingFeeExportReport_' & p_web._nocolon('locReportOrder') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- locReportOrder
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locReportOrder')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locReportOrder') = 0
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locReportOrder'',''formhandlingfeeexportreport_locreportorder_value'',1,'''&clip(0)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locReportOrder',clip(0),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locReportOrder_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('By Job Number') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locReportOrder') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locReportOrder'',''formhandlingfeeexportreport_locreportorder_value'',1,'''&clip(1)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locReportOrder',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locReportOrder_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('By Action Date') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locReportOrder') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locReportOrder'',''formhandlingfeeexportreport_locreportorder_value'',1,'''&clip(2)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locReportOrder',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locReportOrder_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('By Manufacturer') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormHandlingFeeExportReport_' & p_web._nocolon('locReportOrder') & '_value')


Validate::btnPrintReport  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnPrintReport',p_web.GetValue('NewValue'))
    do Value::btnPrintReport
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnPrintReport  Routine
  p_web._DivHeader('FormHandlingFeeExportReport_' & p_web._nocolon('btnPrintReport') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnPrintReport','Print Report','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PageProcess?' &'ProcessType=HandlingFeeReportEngine&ReportType=R')) & ''','''&clip('_blank')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()


Validate::btnExport  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnExport',p_web.GetValue('NewValue'))
    do Value::btnExport
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnExport  Routine
  p_web._DivHeader('FormHandlingFeeExportReport_' & p_web._nocolon('btnExport') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnExport','Create Export','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PageProcess?' &'ProcessType=HandlingFeeReportEngine&ReportType=E')) & ''','''&clip('_blank')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()


Validate::__line4  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('__line4',p_web.GetValue('NewValue'))
    do Value::__line4
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::__line4  Routine
  p_web._DivHeader('FormHandlingFeeExportReport_' & p_web._nocolon('__line4') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::btnBack4  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnBack4',p_web.GetValue('NewValue'))
    do Value::btnBack4
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnBack4  Routine
  p_web._DivHeader('FormHandlingFeeExportReport_' & p_web._nocolon('btnBack4') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnback3','Back','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormHandlingFeeExportReport?' &'tab=3')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/listback.png',,,,)

  do SendPacket
  p_web._DivFooter()


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormHandlingFeeExportReport_locAllAccounts_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locAllAccounts
      else
        do Value::locAllAccounts
      end
  of lower('FormHandlingFeeExportReport_locGenericAccounts_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locGenericAccounts
      else
        do Value::locGenericAccounts
      end
  of lower('FormHandlingFeeExportReport_btnUnTagAll_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnUnTagAll
      else
        do Value::btnUnTagAll
      end
  of lower('FormHandlingFeeExportReport_locAllChargeTypes_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locAllChargeTypes
      else
        do Value::locAllChargeTypes
      end
  of lower('FormHandlingFeeExportReport_btnUnTagAll2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnUnTagAll2
      else
        do Value::btnUnTagAll2
      end
  of lower('FormHandlingFeeExportReport_locAllManufacturers_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locAllManufacturers
      else
        do Value::locAllManufacturers
      end
  of lower('FormHandlingFeeExportReport_btnUntagAll3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnUntagAll3
      else
        do Value::btnUntagAll3
      end
  of lower('FormHandlingFeeExportReport_locStartDate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locStartDate
      else
        do Value::locStartDate
      end
  of lower('FormHandlingFeeExportReport_locEndDate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locEndDate
      else
        do Value::locEndDate
      end
  of lower('FormHandlingFeeExportReport_locReportOrder_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locReportOrder
      else
        do Value::locReportOrder
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormHandlingFeeExportReport_form:ready_',1)
  p_web.SetSessionValue('FormHandlingFeeExportReport_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormHandlingFeeExportReport',0)

PreCopy  Routine
  p_web.SetValue('FormHandlingFeeExportReport_form:ready_',1)
  p_web.SetSessionValue('FormHandlingFeeExportReport_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormHandlingFeeExportReport',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormHandlingFeeExportReport_form:ready_',1)
  p_web.SetSessionValue('FormHandlingFeeExportReport_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormHandlingFeeExportReport:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormHandlingFeeExportReport_form:ready_',1)
  p_web.SetSessionValue('FormHandlingFeeExportReport_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormHandlingFeeExportReport:Primed',0)
  p_web.setsessionvalue('showtab_FormHandlingFeeExportReport',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If p_web.GetValue('tab') = 1
          If p_web.IfExistsValue('locAllAccounts') = 0
            p_web.SetValue('locAllAccounts',0)
            locAllAccounts = 0
          End
      If(p_web.GSV('locAllAccounts') = 1)
          If p_web.IfExistsValue('locGenericAccounts') = 0
            p_web.SetValue('locGenericAccounts',0)
            locGenericAccounts = 0
          End
      End
  End
  If p_web.GetValue('tab') = 2
          If p_web.IfExistsValue('locAllChargeTypes') = 0
            p_web.SetValue('locAllChargeTypes',0)
            locAllChargeTypes = 0
          End
  End
  If p_web.GetValue('tab') = 3
          If p_web.IfExistsValue('locAllManufacturers') = 0
            p_web.SetValue('locAllManufacturers',0)
            locAllManufacturers = 0
          End
  End
  If p_web.GetValue('tab') = 4
  End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormHandlingFeeExportReport_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormHandlingFeeExportReport_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
  If p_web.GetValue('tab') = 1
    loc:InvalidTab += 1
  End
  ! tab = 2
  If p_web.GetValue('tab') = 2
    loc:InvalidTab += 1
  End
  ! tab = 4
  If p_web.GetValue('tab') = 3
    loc:InvalidTab += 1
  End
  ! tab = 3
  If p_web.GetValue('tab') = 4
    loc:InvalidTab += 1
        If locStartDate = ''
          loc:Invalid = 'locStartDate'
          loc:alert = p_web.translate('Start Date') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locStartDate = Upper(locStartDate)
          p_web.SetSessionValue('locStartDate',locStartDate)
        If loc:Invalid <> '' then exit.
        If locEndDate = ''
          loc:Invalid = 'locEndDate'
          loc:alert = p_web.translate('End Date') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locEndDate = Upper(locEndDate)
          p_web.SetSessionValue('locEndDate',locEndDate)
        If loc:Invalid <> '' then exit.
  End
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormHandlingFeeExportReport:Primed',0)
  p_web.StoreValue('locAllAccounts')
  p_web.StoreValue('locGenericAccounts')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locAllChargeTypes')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locAllManufacturers')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locStartDate')
  p_web.StoreValue('')
  p_web.StoreValue('locEndDate')
  p_web.StoreValue('locReportOrder')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
