

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER004.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER155.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER349.INC'),ONCE        !Req'd for module callout resolution
                     END


PrintRoutines        PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locJobNumber         STRING(20)                            !
locWaybillNumber     STRING(30)                            !
                    MAP
LookupJobNumber         PROCEDURE()
ShowHideCreateInvoice    PROCEDURE(),LONG
                    END ! MAP
FilesOpened     Long
SBO_GenericFile::State  USHORT
JOBSE2::State  USHORT
INVOICE::State  USHORT
AUDIT::State  USHORT
JOBSINV::State  USHORT
TRADEACC::State  USHORT
WEBJOB::State  USHORT
JOBSE::State  USHORT
JOBS::State  USHORT
WAYBILLJ::State  USHORT
JOBNOTES::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('PrintRoutines')
  loc:formname = 'PrintRoutines_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('PrintRoutines','')
    p_web._DivHeader('PrintRoutines',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferPrintRoutines',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPrintRoutines',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPrintRoutines',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_PrintRoutines',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPrintRoutines',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_PrintRoutines',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
HideButtons         routine
    p_web.SSV('Hide:PrintButtons',1)
    p_web.SSV('Hide:PrintEstimate',1)
    p_web.SSV('Hide:PrintCreditNote',1)
    p_web.SSV('Hide:PrintOBFValidationReport',1)
    p_web.SSV('Hide:PrintInvoice',1)
    p_web.SSV('Hide:PrintLoanCollectionNote',1)
!    p_web.SSV('Hide:PrintJobCard',1)
!    p_web.SSV('Hide:PrintEstimate',1)
!    p_web.SSV('Hide:PrintDespatchNote',1)
HideWaybillButtons  ROUTINE
    p_web.SSV('Hide:PrintWaybill',1)
OpenFiles  ROUTINE
  p_web._OpenFile(SBO_GenericFile)
  p_web._OpenFile(JOBSE2)
  p_web._OpenFile(INVOICE)
  p_web._OpenFile(AUDIT)
  p_web._OpenFile(JOBSINV)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(WAYBILLJ)
  p_web._OpenFile(JOBNOTES)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(SBO_GenericFile)
  p_Web._CloseFile(JOBSE2)
  p_Web._CloseFile(INVOICE)
  p_Web._CloseFile(AUDIT)
  p_Web._CloseFile(JOBSINV)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(WAYBILLJ)
  p_Web._CloseFile(JOBNOTES)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('PrintRoutines_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locJobNumber',locJobNumber)
  p_web.SetSessionValue('locWaybillNumber',locWaybillNumber)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locJobNumber')
    locJobNumber = p_web.GetValue('locJobNumber')
    p_web.SetSessionValue('locJobNumber',locJobNumber)
  End
  if p_web.IfExistsValue('locWaybillNumber')
    locWaybillNumber = p_web.GetValue('locWaybillNumber')
    p_web.SetSessionValue('locWaybillNumber',locWaybillNumber)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('PrintRoutines_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    IF (p_web.GetValue('clearVars') = 1)
        ! If called from Index Page, clear the variables
        ! If returning from a report, do not.
        DO HideButtons
        DO HideWaybillButtons
        p_web.SSV('locJobNumber','')
        p_web.SSV('locWaybillNumber','')
    END ! IF
    
    IF (p_web.IfExistsValue('JobNumber'))
        ! If passed a jobnumber, then prefil
        IF (p_web.GetValue('JobNumber') > 0)
            p_web.SSV('locJobNumber',p_web.GetValue('JobNumber'))
            LookupJobNumber()
        END!  IF
    END ! IF
    
    IF (p_web.IfExistsValue('sbogenRecordNumber'))
        Access:SBO_GenericFile.ClearKey(sbogen:RecordNumberKey)
        sbogen:RecordNumber = p_web.GetValue('sbogenRecordNumber')
        IF (Access:SBO_GenericFile.TryFetch(sbogen:RecordNumberKey) = Level:Benign)
            p_web.SSV('locJobNumber',sbogen:Long1)
            LookupJobNumber()
        END ! IF
    END!  IF
    
    IF (p_web.IfExistsValue('PrintRoutineReturnURL'))
        p_web.StoreValue('PrintRoutineReturnURL')
    END ! IF
    
    
  
  
  !  p_web.site.SaveButton.TextValue = 'OK'
  !  
  !  if (p_web.GSV('PrintRoutines:SecondTime') = 0)
  !      p_web.SSV('locJObNumber','')
  !      p_web.SSV('Comment:JobNumber','Enter Job Number and press [TAB]')
  !      do HideButtons
  !      p_web.SSV('PrintRoutines:SecondTime',1)
  !      p_web.SSV('locWaybillNumber','')
  !      p_web.SSV('Comment:WaybillNumber','Enter Waybill Number and press [TAB]')
  !      do HideWaybillButtons
  !  end ! if (p_web.GSV('PrintRoutines:SecondTime') = 0)
  
      p_web.site.CancelButton.TextValue = 'Close'
      p_web.site.CancelButton.Image = 'images/psave.png'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locJobNumber = p_web.RestoreValue('locJobNumber')
 locWaybillNumber = p_web.RestoreValue('locWaybillNumber')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('PrintRoutineReturnURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('PrintRoutines_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('PrintRoutines_ChainTo')
    loc:formaction = p_web.GetSessionValue('PrintRoutines_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="PrintRoutines" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="PrintRoutines" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="PrintRoutines" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Print Routines') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Print Routines',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_PrintRoutines">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_PrintRoutines" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_PrintRoutines')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Enter Job Number') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Select Waybill Number') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_PrintRoutines')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_PrintRoutines'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locJobNumber')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab4'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab5'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_PrintRoutines')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab4'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab5'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Enter Job Number') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PrintRoutines_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Job Number')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Job Number')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Job Number')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Job Number')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locJobNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locJobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PrintRoutines_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonPrintJobCard
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonPrintEstimate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonPrintDespatchNote
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnCustomerReceipt
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnInvoice
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnCreditNote
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnLoanCollectionNote
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnOBFValidationReport
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::hidden
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel4">'&CRLF &|
                                    '  <div id="panel4Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Select Waybill Number') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel4Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PrintRoutines_4">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Select Waybill Number')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab4" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab4">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Select Waybill Number')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Select Waybill Number')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Select Waybill Number')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locWaybillNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locWaybillNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab3  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel5">'&CRLF &|
                                    '  <div id="panel5Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel5Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PrintRoutines_5">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab5" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab5">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab5">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab5">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonPrintWaybill
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locJobNumber  Routine
  p_web._DivHeader('PrintRoutines_' & p_web._nocolon('locJobNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Job Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locJobNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locJobNumber',p_web.GetValue('NewValue'))
    locJobNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locJobNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locJobNumber',p_web.GetValue('Value'))
    locJobNumber = p_web.GetValue('Value')
  End
  !region  Look For Job
    LookupJobNumber()
    
  
  do Value::locJobNumber
  do SendAlert
  do Value::buttonPrintEstimate  !1
  do Value::buttonPrintJobCard  !1
  do Value::buttonPrintDespatchNote  !1
  do Value::btnCreditNote  !1
  do Value::btnCustomerReceipt  !1
  do Value::btnInvoice  !1
  do Value::btnLoanCollectionNote  !1
  do Value::btnOBFValidationReport  !1

Value::locJobNumber  Routine
  p_web._DivHeader('PrintRoutines_' & p_web._nocolon('locJobNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locJobNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locJobNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locJobNumber'',''printroutines_locjobnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locJobNumber',p_web.GetSessionValueFormat('locJobNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PrintRoutines_' & p_web._nocolon('locJobNumber') & '_value')


Validate::buttonPrintJobCard  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonPrintJobCard',p_web.GetValue('NewValue'))
    do Value::buttonPrintJobCard
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  !update the random bit so it is different next time
  UpdateUniqueBitText(p_web)
  do Value::buttonPrintJobCard
  do SendAlert

Value::buttonPrintJobCard  Routine
  p_web._DivHeader('PrintRoutines_' & p_web._nocolon('buttonPrintJobCard') & '_value',Choose(p_web.GSV('Hide:PrintButtons') = 1 OR p_web.GSV('locJobNumber') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:PrintButtons') = 1 OR p_web.GSV('locJobNumber') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonPrintJobCard'',''printroutines_buttonprintjobcard_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PrintJobCard','Job Card','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('JobCard?var='  & p_web.gsv('UniqueBitText'))) & ''','''&clip('_blank')&''')',loc:javascript,0,'images/printer.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PrintRoutines_' & p_web._nocolon('buttonPrintJobCard') & '_value')


Validate::buttonPrintEstimate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonPrintEstimate',p_web.GetValue('NewValue'))
    do Value::buttonPrintEstimate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  UpdateUniqueBitText(p_web)
  do Value::buttonPrintEstimate
  do SendAlert

Value::buttonPrintEstimate  Routine
  p_web._DivHeader('PrintRoutines_' & p_web._nocolon('buttonPrintEstimate') & '_value',Choose(p_web.GSV('Hide:PrintEstimate') = 1 OR p_web.GSV('locJobNumber') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:PrintEstimate') = 1 OR p_web.GSV('locJobNumber') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonPrintEstimate'',''printroutines_buttonprintestimate_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PrintEstimate','Estimate','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('Estimate?' &'rnd=' & RANDOM(1,1000))) & ''','''&clip('_blank')&''')',loc:javascript,0,'images/printer.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PrintRoutines_' & p_web._nocolon('buttonPrintEstimate') & '_value')


Validate::buttonPrintDespatchNote  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonPrintDespatchNote',p_web.GetValue('NewValue'))
    do Value::buttonPrintDespatchNote
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  UpdateUniqueBitText(p_web)
  do Value::buttonPrintDespatchNote
  do SendAlert

Value::buttonPrintDespatchNote  Routine
  p_web._DivHeader('PrintRoutines_' & p_web._nocolon('buttonPrintDespatchNote') & '_value',Choose(p_web.GSV('Hide:PrintButtons') = 1 OR p_web.GSV('Hide:ButtonDespatch') = 1 OR p_web.GSV('locJobNumber') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:PrintButtons') = 1 OR p_web.GSV('Hide:ButtonDespatch') = 1 OR p_web.GSV('locJobNumber') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonPrintDespatchNote'',''printroutines_buttonprintdespatchnote_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PrintDespatch','Despatch Note','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('DespatchNote?var=' & p_web.gsv('UniqueBitText'))) & ''','''&clip('_blank')&''')',loc:javascript,0,'images/printer.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PrintRoutines_' & p_web._nocolon('buttonPrintDespatchNote') & '_value')


Validate::btnCustomerReceipt  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnCustomerReceipt',p_web.GetValue('NewValue'))
    do Value::btnCustomerReceipt
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do SendAlert
  do Value::btnCustomerReceipt  !1

Value::btnCustomerReceipt  Routine
  p_web._DivHeader('PrintRoutines_' & p_web._nocolon('btnCustomerReceipt') & '_value',Choose(p_web.GSV('Hide:PrintButtons') = 1 OR p_web.GSV('Hide:Waybill') = 1 OR p_web.GSV('locJobNumber') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:PrintButtons') = 1 OR p_web.GSV('Hide:Waybill') = 1 OR p_web.GSV('locJobNumber') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnCustomerReceipt'',''printroutines_btncustomerreceipt_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnCustomerReceipt','Customer Receipt','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('JobReceipt?rnd=' & RANDOM(1,1000))) & ''','''&clip('_blank')&''')',loc:javascript,0,'images/printer.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PrintRoutines_' & p_web._nocolon('btnCustomerReceipt') & '_value')


Validate::btnInvoice  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnInvoice',p_web.GetValue('NewValue'))
    do Value::btnInvoice
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do SendAlert
  do Value::btnInvoice  !1

Value::btnInvoice  Routine
  p_web._DivHeader('PrintRoutines_' & p_web._nocolon('btnInvoice') & '_value',Choose(p_web.GSV('Hide:PrintInvoice') = 1 OR p_web.GSV('Hide:WayBill') = 1 OR p_web.GSV('locJobNumber') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:PrintInvoice') = 1 OR p_web.GSV('Hide:WayBill') = 1 OR p_web.GSV('locJobNumber') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnInvoice'',''printroutines_btninvoice_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnInvoice',p_web.GSV('URL:CreateInvoiceText'),'button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip(p_web.GSV('URL:CreateInvoice'))) & ''','''&clip(p_web.GSV('URL:CreateInvoiceTarget'))&''')',loc:javascript,0,'images/printer.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PrintRoutines_' & p_web._nocolon('btnInvoice') & '_value')


Validate::btnCreditNote  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnCreditNote',p_web.GetValue('NewValue'))
    do Value::btnCreditNote
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do SendAlert
  do Value::btnCreditNote  !1

Value::btnCreditNote  Routine
  p_web._DivHeader('PrintRoutines_' & p_web._nocolon('btnCreditNote') & '_value',Choose(p_web.GSV('Hide:PrintCreditNote') = 1 OR p_web.GSV('Hide:WayBill') = 1 OR p_web.GSV('locJobNumber') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:PrintCreditNote') = 1 OR p_web.GSV('Hide:WayBill') = 1 OR p_web.GSV('locJobNumber') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnCreditNote'',''printroutines_btncreditnote_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnCreditNote','Credit Note','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('BrowseCreditNotes')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/printer.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PrintRoutines_' & p_web._nocolon('btnCreditNote') & '_value')


Validate::btnLoanCollectionNote  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnLoanCollectionNote',p_web.GetValue('NewValue'))
    do Value::btnLoanCollectionNote
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do Value::btnLoanCollectionNote
  do SendAlert

Value::btnLoanCollectionNote  Routine
  p_web._DivHeader('PrintRoutines_' & p_web._nocolon('btnLoanCollectionNote') & '_value',Choose(p_web.GSV('Hide:PrintButtons') = 1 OR p_web.GSV('Hide:PrintLoanCollectionNote') = 1 OR p_web.GSV('locJobNumber') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:PrintButtons') = 1 OR p_web.GSV('Hide:PrintLoanCollectionNote') = 1 OR p_web.GSV('locJobNumber') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnLoanCollectionNote'',''printroutines_btnloancollectionnote_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnLoanCollectionNote','Loan Collection Note','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('LoanCollectionNote?' &'rnd=' & RANDOM(1,1000))) & ''','''&clip('_blank')&''')',loc:javascript,0,'images/printer.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PrintRoutines_' & p_web._nocolon('btnLoanCollectionNote') & '_value')


Validate::btnOBFValidationReport  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnOBFValidationReport',p_web.GetValue('NewValue'))
    do Value::btnOBFValidationReport
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do SendAlert
  do Value::btnOBFValidationReport  !1
  do Value::locJobNumber  !1

Value::btnOBFValidationReport  Routine
  p_web._DivHeader('PrintRoutines_' & p_web._nocolon('btnOBFValidationReport') & '_value',Choose(p_web.GSV('Hide:Waybill') = 1 OR p_web.GSV('Hide:PrintOBFValidationReport') = 1 OR p_web.GSV('locJobNumber') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:Waybill') = 1 OR p_web.GSV('Hide:PrintOBFValidationReport') = 1 OR p_web.GSV('locJobNumber') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnOBFValidationReport'',''printroutines_btnobfvalidationreport_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnOBFValidationReport','OBF Validation Report','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('OBFValidationReport?rnd=' & RANDOM(1,1000))) & ''','''&clip('_blank')&''')',loc:javascript,0,'images/printer.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PrintRoutines_' & p_web._nocolon('btnOBFValidationReport') & '_value')


Validate::hidden  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('hidden',p_web.GetValue('NewValue'))
    do Value::hidden
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::hidden  Routine
  p_web._DivHeader('PrintRoutines_' & p_web._nocolon('hidden') & '_value','adiv')
  loc:extra = ''
  ! --- HIDDEN --- 
    packet = clip(packet) & p_web.CreateInput('hidden','',p_web.GetSessionValue('')) & '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locWaybillNumber  Routine
  p_web._DivHeader('PrintRoutines_' & p_web._nocolon('locWaybillNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Waybill Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locWaybillNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locWaybillNumber',p_web.GetValue('NewValue'))
    locWaybillNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locWaybillNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locWaybillNumber',p_web.GetValue('Value'))
    locWaybillNumber = p_web.GetValue('Value')
  End
  ! region Lookup For Waybill
    Access:WAYBILLJ.Clearkey(waj:DescWaybillNoKey)
    waj:WaybillNumber = p_web.GSV('locWaybillNumber')
    IF (Access:WAYBILLJ.TryFetch(waj:DescWaybillNoKey))
        loc:alert = 'Cannot find the selected Waybill Number'
        loc:invalid = 'locWaybillNumber'
        do HideWaybillButtons
    ELSE
      
        Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
        way:WayBillNumber = waj:WayBillNumber
        IF (Access:WAYBILLS.TryFetch(way:WayBillNumberKey))
            loc:alert = 'Cannot find the selected Waybill Number'
            loc:invalid = 'locWaybillNumber'
            do HideWaybillButtons
  
        ELSE
          ! Get the details from the first job
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = waj:JobNumber
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
                loc:alert = 'Cannot find the job attached to the selected Waybill Number'
                loc:invalid = 'locWaybillNumber'
                do HideWaybillButtons           
            END
          
            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = job:Ref_Number
            IF (Access:JOBSE.TryFetch(jobe:RefNumberKey))
                loc:alert = 'Cannot find the job attached to the selected Waybill Number'
                loc:invalid = 'locWaybillNumber'
                do HideWaybillButtons             
            END
          
            Access:WEBJOB.ClearKey(wob:RefNumberKey)
            wob:RefNumber = job:Ref_Number
            IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
                loc:alert = 'Cannot find the job attached to the selected Waybill Number'
                loc:invalid = 'locWaybillNumber'
                do HideWaybillButtons             
            END
          
  
            error# = 0
            IF (p_web.GSV('BookingSite') = 'RRC')
                Access:TRADEACC.ClearKey(tra:Account_Number_Key)
                tra:Account_Number = p_web.GSV('BookingAccount')
                IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
                END
              
                IF (way:FromAccount <> p_web.GSV('BookingAccount'))
                    loc:alert = 'The selected waybill was not created at this site.'
                    loc:invalid = 'locWaybillNumber'
                    do HideWaybillButtons
                    error# = 1
                ELSE
                    CASE way:WayBillType
                    OF 1 ! RRC to ARC
                        p_web.SSV('Waybill:ToAccount',p_web.GSV('ARC:AccountNumber'))
                        p_web.SSV('Waybill:ToType','TRA')
                    OF 2 !RRC to Customer
                        p_web.SSV('Waybill:ToAccount','')
                        p_web.SSV('Waybill:ToType','CUS')
                    OF 3 ! RRC Exchange to Customer
                        p_web.SSV('Waybill:ToAccount','')
                        p_web.SSV('Waybill:ToType','CUS')
                    ELSE
                        IF (jobe:Sub_Sub_Account <> '')
                            p_web.SSV('Waybill:ToAccount',jobe:Sub_Sub_Account)
                        ELSE
                            p_web.SSV('Waybill:ToAccount',job:Account_Number)
                        END
                    END
                    
                    p_web.SSV('Waybill:ToType','SUB')
                    p_web.SSV('Hide:PrintWaybill',0)
                    p_web.SSV('Comment:WaybillNumber','Waybill Found')
                
                    If GETINI('DESPATCH','OVERWRITECOURIER','N',CLIP(PATH()) & '\SB2KDEF.INI') <> 'Y'
                        p_web.SSV('Waybill:Courier',GETINI('DESPATCH','OVERWRITECOURIER','N',CLIP(PATH()) & '\SB2KDEF.INI'))
                    ELSE
                        p_web.SSV('Waybill:Courier',tra:Courier_Outgoing)
                    END
                    p_web.SSV('Waybill:FromType','TRA')
                    p_web.SSV('Waybill:FromAccount',p_web.GSV('BookingAccount'))
                END
            ELSE ! IF (p_web.GSV('BookingSite') = 'RRC')
                IF (way:FromAccount <> p_web.GSV('ARC:AccountNumber'))
                    loc:alert = 'The selected waybill was not created at this site.'
                    loc:invalid = 'locWaybillNumber'
                    do HideWaybillButtons
                ELSE
                  
                    Access:TRADEACC.ClearKey(tra:Account_Number_Key)
                    tra:Account_Number = p_web.GSV('ARC:AccountNumber')
                    IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
                    END
                  
                    p_web.SSV('Waybill:FromAccount',p_web.GSV('ARC:AccountNumber'))
                    IF (jobe:WebJob = 1)
                        p_web.SSV('Waybill:ToAccount',wob:HeadAccountNumber)
                        p_web.SSV('Waybill:ToType','TRA')
                    ELSE
                        p_web.SSV('Waybill:ToAccount',job:Account_Number)
                        p_web.SSV('Waybill:ToType','SUB')
                    END
                    If GETINI('DESPATCH','OVERWRITECOURIER','N',CLIP(PATH()) & '\SB2KDEF.INI') <> 'Y'
                        p_web.SSV('Waybill:Courier',GETINI('DESPATCH','OVERWRITECOURIER','N',CLIP(PATH()) & '\SB2KDEF.INI'))
                    ELSE
                        p_web.SSV('Waybill:Courier',tra:Courier_Outgoing)
                    END
                    p_web.SSV('Hide:PrintWaybill',0)
                    p_web.SSV('Comment:WaybillNumber','Waybill Found')
                END
            END
          
        end
  
    END
  !endregion  
  do Value::locWaybillNumber
  do SendAlert
  do Value::buttonPrintWaybill  !1

Value::locWaybillNumber  Routine
  p_web._DivHeader('PrintRoutines_' & p_web._nocolon('locWaybillNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locWaybillNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locWaybillNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locWaybillNumber'',''printroutines_locwaybillnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locWaybillNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locWaybillNumber',p_web.GetSessionValueFormat('locWaybillNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PrintRoutines_' & p_web._nocolon('locWaybillNumber') & '_value')


Validate::buttonPrintWaybill  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonPrintWaybill',p_web.GetValue('NewValue'))
    do Value::buttonPrintWaybill
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  !update the random bit so it is different next time
  UpdateUniqueBitText(p_web)
  do Value::buttonPrintWaybill
  do SendAlert

Value::buttonPrintWaybill  Routine
  p_web._DivHeader('PrintRoutines_' & p_web._nocolon('buttonPrintWaybill') & '_value',Choose(p_web.GSV('Hide:PrintWaybill') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:PrintWaybill') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonPrintWaybill'',''printroutines_buttonprintwaybill_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PrintWaybill','Print Waybill','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('Waybill?var=' & p_web.gsv('UniqueBitText'))) & ''','''&clip('_blank')&''')',loc:javascript,0,'images/printer.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PrintRoutines_' & p_web._nocolon('buttonPrintWaybill') & '_value')


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('PrintRoutines_locJobNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locJobNumber
      else
        do Value::locJobNumber
      end
  of lower('PrintRoutines_buttonPrintJobCard_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonPrintJobCard
      else
        do Value::buttonPrintJobCard
      end
  of lower('PrintRoutines_buttonPrintEstimate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonPrintEstimate
      else
        do Value::buttonPrintEstimate
      end
  of lower('PrintRoutines_buttonPrintDespatchNote_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonPrintDespatchNote
      else
        do Value::buttonPrintDespatchNote
      end
  of lower('PrintRoutines_btnCustomerReceipt_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnCustomerReceipt
      else
        do Value::btnCustomerReceipt
      end
  of lower('PrintRoutines_btnInvoice_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnInvoice
      else
        do Value::btnInvoice
      end
  of lower('PrintRoutines_btnCreditNote_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnCreditNote
      else
        do Value::btnCreditNote
      end
  of lower('PrintRoutines_btnLoanCollectionNote_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnLoanCollectionNote
      else
        do Value::btnLoanCollectionNote
      end
  of lower('PrintRoutines_btnOBFValidationReport_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnOBFValidationReport
      else
        do Value::btnOBFValidationReport
      end
  of lower('PrintRoutines_locWaybillNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locWaybillNumber
      else
        do Value::locWaybillNumber
      end
  of lower('PrintRoutines_buttonPrintWaybill_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonPrintWaybill
      else
        do Value::buttonPrintWaybill
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('PrintRoutines_form:ready_',1)
  p_web.SetSessionValue('PrintRoutines_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_PrintRoutines',0)

PreCopy  Routine
  p_web.SetValue('PrintRoutines_form:ready_',1)
  p_web.SetSessionValue('PrintRoutines_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_PrintRoutines',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('PrintRoutines_form:ready_',1)
  p_web.SetSessionValue('PrintRoutines_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('PrintRoutines:Primed',0)

PreDelete       Routine
  p_web.SetValue('PrintRoutines_form:ready_',1)
  p_web.SetSessionValue('PrintRoutines_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('PrintRoutines:Primed',0)
  p_web.setsessionvalue('showtab_PrintRoutines',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('PrintRoutines_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('PrintRoutines_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
  ! tab = 4
    loc:InvalidTab += 1
  ! tab = 5
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
      p_web.deleteSessionValue('PrintRoutines:SecondTime')
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('PrintRoutines:Primed',0)
  p_web.StoreValue('locJobNumber')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locWaybillNumber')
  p_web.StoreValue('')
SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('locJobNumber',locJobNumber) ! STRING(20)
     p_web.SSV('locWaybillNumber',locWaybillNumber) ! STRING(30)
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     locJobNumber = p_web.GSV('locJobNumber') ! STRING(20)
     locWaybillNumber = p_web.GSV('locWaybillNumber') ! STRING(30)
LookupJobNumber     PROCEDURE()
    CODE
    Access:JOBS.Clearkey(job:ref_Number_Key)
    job:ref_Number    = p_web.GSV('locJobNumber')
    if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
      ! Found
        p_web.FileToSessionQueue(JOBS)
  
  
        Access:JOBNOTES.Clearkey(jbn:refNumberKey)
        jbn:refNumber    = job:Ref_Number
        if (Access:JOBNOTES.TryFetch(jbn:refNumberKey) = Level:Benign)
          ! Found
            p_web.FileToSessionQueue(JOBNOTES)
        else ! if (Access:JOBNOTES.TryFetch(jbn:refNumberKey) = Level:Benign)
          ! Error
        end ! if (Access:JOBNOTES.TryFetch(jbn:refNumberKey) = Level:Benign)
  
  
        Access:WEBJOB.Clearkey(wob:refNumberKey)
        wob:refNumber    = job:Ref_Number
        if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
          ! Found
            p_web.FileToSessionQueue(WEBJOB)
        else ! if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
          ! Error
        end ! if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
  
        Access:JOBSE.Clearkey(jobe:refNumberKey)
        jobe:refNumber    = job:Ref_Number
        if (Access:JOBSE.TryFetch(jobe:refNumberKey) = Level:Benign)
          ! Found
            p_web.FileToSessionQueue(JOBSE)
        else ! if (Access:JOBSE.TryFetch(jobe:refNumberKey) = Level:Benign)
          ! Error
        end ! if (Access:JOBSE.TryFetch(jobe:refNumberKey) = Level:Benign)
        
        Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
        jobe2:RefNumber = job:Ref_Number
        IF (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
            p_web.FileToSessionQueue(JOBSE2)
        END ! IF
        
  
        if (job:Estimate = 'YES')
            p_web.SSV('Hide:PrintEstimate',0)
        end ! if (job:Estimate = 'YES')  
        
        
        credit# = 0
        Access:JOBSINV.ClearKey(jov:TypeRecordKey)
        jov:RefNumber = wob:RefNumber
        jov:Type = 'C'
        SET(jov:TypeRecordKey,jov:TypeRecordKey)
        LOOP UNTIL Access:JOBSINV.Next() <> Level:Benign
            IF (jov:RefNumber <> wob:RefNumber OR |
                jov:Type <> 'C')
                BREAK
            END ! IF
            credit# = 1
            BREAK
        END ! LOOP
        
        IF (credit# = 1)
            p_web.SSV('Hide:PrintCreditNote',0)
            p_web.SSV('CreditNoteCreated',1)
        END ! IF
        
        OBFJob# = 0
  
        LOOP 1 TIMES
            IF NOT (jobe:OBFValidated)
                Access:AUDIT.ClearKey(aud:Action_Key)
                aud:Ref_Number = job:Ref_Number
                aud:Action = 'O.B.F. VALIDATION FAILED'
                SET(aud:Action_Key,aud:Action_Key)
                LOOP UNTIL Access:AUDIT.Next() <> Level:Benign
                    IF (aud:Ref_Number <> job:Ref_Number OR |
                        aud:Action <> 'O.B.F. VALIDATION FAILED')
                        BREAK
                    END ! IF
                    OBFJob# = 1
                    BREAK
                END ! LOOP
        
                IF (OBFJob# = 1)
                    BREAK
                END ! IF
        
                Access:AUDIT.ClearKey(aud:Action_Key)
                aud:Ref_Number = job:Ref_Number
                aud:Action = 'OBF REJECTED'
                SET(aud:Action_Key,aud:Action_Key)
                LOOP UNTIL Access:AUDIT.Next() <> Level:Benign
                    IF (aud:Ref_Number <> job:Ref_Number OR |
                        aud:Action <> 'OBF REJECTED')
                        BREAK
                    END ! IF
                    OBFJob# = 1
                    BREAK
                END ! LOOP
            ELSE
                OBFJob# = 1
            END ! IF
        END ! LOOP
        
        IF (OBFJob# = 1)
            p_web.SSV('Hide:PrintOBFValidationReport',0)
        END ! IF
        
        IF (ShowHideCreateInvoice() = Level:Benign)
            p_web.SSV('Hide:PrintInvoice',0)
        END ! IF
        
        p_web.SSV('Hide:PrintButtons',0)
        IF (job:Loan_unit_Number <> '')
            p_web.SSV('Hide:PrintLoanCollectionNote',0)
        END ! IF
        
        p_web.SSV('tmp:JobNumber',job:Ref_Number)
        
    else ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
       ! Error
        do HideButtons
        loc:Alert = 'Cannot find selected Job Number'
        loc:Invalid = 'locJobNumber'
    end ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
  !endregion  Look For Job
ShowHideCreateInvoice       PROCEDURE()
    CODE
        IF (p_web.gsv('job:Chargeable_Job') <> 'YES')
            RETURN TRUE
        END
        
        SentToHub(p_web)
        IF (p_web.GSV('BookingSite') = 'ARC' AND p_web.GSV('SentToHub') <> 1)
            RETURN TRUE
        END
        
        IF (p_web.GSV('job:Bouncer') = 'X')
            RETURN TRUE
        END
        
        ! Job not completed
        IF (p_web.GSV('job:Date_Completed') = 0 AND p_web.GSV('job:Exchange_Unit_Number') = 0)
            RETURN TRUE
        END
        
        ! Job has not been priced
        IF (p_web.GSV('job:ignore_Chargeable_Charges') = 'YES')
            IF (p_web.GSV('BookingSite') = 'RRC')
                IF (p_web.GSV('jobe:RRCSubTotal') = 0)
                    RETURN TRUE
                END
            ELSE
                IF (p_web.GSV('job:Sub_Total') = 0)
                    RETURN TRUE
                END
                
            END
        END
        p_web.SSV('URL:CreateInvoice','CreateInvoice?returnURL=PrintRoutines')
        p_web.SSV('URL:CreateInvoiceTarget','_self')
        p_web.SSV('URL:CreateInvoiceText','Create Invoice')
 
!        IF (IsJobInvoiced(p_web.GSV('job:Invoice_Number'),p_web,
        
        IF (p_web.GSV('job:Invoice_Number') > 0)
            Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
            inv:Invoice_Number = p_web.GSV('job:Invoice_Number')
            IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign)
                IF (inv:Invoice_Type <> 'SIN')
                    RETURN TRUE
                END
                ! If job has been invoiced, just print the invoice, rather than asking to create one
                IF (p_web.GSV('BookingSite') = 'RRC')
                    IF (inv:RRCInvoiceDate > 0)
                        p_web.SSV('URL:CreateInvoice',p_web.GSV('Document:Invoice') & '?var=' & RANDOM(1,9999999))
                        p_web.SSV('URL:CreateInvoiceTarget','_blank')
                        p_web.SSV('URL:CreateInvoiceText','Print Invoice')
                    END
                ELSE
                    If (inv:ARCInvoiceDate > 0)
                        p_web.SSV('URL:CreateInvoice',p_web.GSV('Document:Invoice') & '?var=' & RANDOM(1,9999999))
                        p_web.SSV('URL:CreateInvoiceTarget','_blank')
                        p_web.SSV('URL:CreateInvoiceText','Print Invoice')
                    END
                END
            END
        END
        RETURN FALSE
