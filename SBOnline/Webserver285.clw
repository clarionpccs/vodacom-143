

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER285.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER061.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER079.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER189.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER251.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER260.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER264.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER268.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER273.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER274.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER278.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER282.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER287.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER324.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER339.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER344.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER361.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER374.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER406.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER413.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER474.INC'),ONCE        !Req'd for module callout resolution
                     END


PageProcess          PROCEDURE  (NetWebServerWorker p_web)
kNoRecordsAlert EQUATE('alert("There are no records that match the selected criteria.")')
loc:x          Long
packet              string(NET:MaxBinData)
packetlen           long
CRLF           String('<13,10>')
NBSP           String('&#160;')
                    MAP
AvailableExchangeExport PROCEDURE()
BuildAvailableExchangeLoanStock		PROCEDURE()
BuildJobsProgressList   PROCEDURE()
BuildStockReceiveList   PROCEDURE()
BuildWarrantyClaimsBrowse       PROCEDURE()
CallReport  PROCEDURE(STRING pReportName)
ChargeableIncomeReportEngine    PROCEDURE()
ChargeableIncomeReportExport    PROCEDURE()
GenericTagFileClear     PROCEDURE()
CountBouncers           PROCEDURE()
CreateExchangeLoanOrder PROCEDURE()!
CreditReportExport      PROCEDURE()
CreateGRN PROCEDURE()
ExchangeAuditExport     PROCEDURE()
ExchangeLoanAvailableExport     PROCEDURE()
ExchangeLoanDeviceSoldReport		PROCEDURE()
ExchangeLoanUnitsReturnedToMainStore PROCEDURE()
ExchangePhoneExport     PROCEDURE()
ExchangeStockValuationExport PROCEDURE()!
ExportWarrantyClaims    PROCEDURE()
GlobalIMEIList          PROCEDURE(STRING pIMEINumber)
HandlingFeeReportEngine PROCEDURE()
HandlingFeeReportExport PROCEDURE()
InsuranceReportExport PROCEDURE()
LoanPhoneExport     PROCEDURE()!
LoanUnitAvailable       PROCEDURE()!
OpenJob                 PROCEDURE()
OutStandingStockOrders PROCEDURE()
RapidEngineerUpdate        PROCEDURE()
ReturnExchangeUnits     PROCEDURE()!
ReturnLoanUnits	PROCEDURE()!
RRCWarrantyAccRejImport     PROCEDURE(LONG pType)
RRCWarrantyInvoice      PROCEDURE()
RRCWarrantyInvoiceImport        PROCEDURE()
SMSResponseReport        PROCEDURE()
SMSTaggedJobs           PROCEDURE()
StatusReportCriteriaGet PROCEDURE()
StatusReportCriteriaSave        PROCEDURE()
StatusReportEngine      PROCEDURE()
StatusReportExport      PROCEDURE()
StockAuditReport        PROCEDURE()
StockCheckReport        PROCEDURE()
StockReceiveProcess     PROCEDURE()
StockValueReportEngine  PROCEDURE()
StockValueExport        PROCEDURE()
GenericTagFileTagAll    PROCEDURE()
TagStockTypes           PROCEDURE()
UpdateEngineerNotes PROCEDURE()!
WarrantyIncomeReportEngine      PROCEDURE()
WarrantyIncomeReportExport  PROCEDURE()
WaybillComplete         PROCEDURE()
WaybillConfirmation     PROCEDURE()
WaybillGeneration       PROCEDURE()
WaybillProcessJob       PROCEDURE()
WaybillSundryGeneration PROCEDURE()
WIPAuditExport		PROCEDURE()
                    END ! MAP

ProgressBarWeb              CLASS
RecordCount                     LONG()
TotalRecords                    LONG()
SkipValue                       LONG(),PRIVATE
Skip                            LONG(),PRIVATE
DisplayText                     STRING(100),PRIVATE
PercentageValue                 LONG(),PRIVATE
Initialized                     LONG(),PRIVATE
Init                            PROCEDURE(LONG pTotalRecords,LONG pSkipValue=100)
Update                          PROCEDURE(<STRING pDisplayText>,LONG pForceUpdate=0)  
UpdateProgressBar                PROCEDURE(),PRIVATE
                            END ! CASE

expfil                      CLASS(SimpleExportClass)
ExportFilename                  CSTRING(255)
Init                            PROCEDURE(STRING pDescription,<STRING pFilename>),BYTE,PROC
Kill                            PROCEDURE(<BYTE pDeleteFile>)
                            END ! CLASS
expfil2                     CLASS(SimpleExportClass)
ExportFilename                  CSTRING(255)
Init                            PROCEDURE(STRING pDescription,<STRING pFilename>),BYTE,PROC
Kill                            PROCEDURE(<BYTE pDeleteFile>)
                            END ! CLASS
expfil3                     CLASS(SimpleExportClass)
ExportFilename                  CSTRING(255)
Init                            PROCEDURE(STRING pDescription,<STRING pFilename>),BYTE,PROC
Kill                            PROCEDURE(<BYTE pDeleteFile>)
                            END ! CLASS
expfil4                     CLASS(SimpleExportClass)
ExportFilename                  CSTRING(255)
Init                            PROCEDURE(STRING pDescription,<STRING pFilename>),BYTE,PROC
Kill                            PROCEDURE(<BYTE pDeleteFile>)
                            END ! CLASS
locSavePath                 CSTRING(255)
locExportFile               CSTRING(255),STATIC
ExportFile                  File,DRIVER('ASCII'),PRE(expfil),Name(locExportFile),CREATE,BINDABLE,THREAD
RECORD                          RECORD
Line                             STRING(1000)
                                END
                            END

locExportFile2              CSTRING(255),STATIC
ExportFile2                 File,DRIVER('ASCII'),PRE(expfil2),Name(locExportFile2),CREATE,BINDABLE,THREAD
RECORD                          RECORD
Line                                STRING(1000)
                                END
                            END
locExportFile3              CSTRING(255),STATIC
ExportFile3                 File,DRIVER('ASCII'),PRE(expfil3),Name(locExportFile3),CREATE,BINDABLE,THREAD
RECORD                          RECORD
Line                                STRING(1000)
                                END
                            END
locExportFile4              CSTRING(255),STATIC
ExportFile4                 File,DRIVER('ASCII'),PRE(expfil4),Name(locExportFile4),CREATE,BINDABLE,THREAD
RECORD                          RECORD
Line                                STRING(1000)
                                END
                            END
locSubmissions              LONG
locDateRejected             DATE
locRejectionReason          STRING(60)
locDaysToFinalRejection STRING(20)

  CODE
  GlobalErrors.SetProcedureName('PageProcess')
  p_web.SetValue('_parentPage','PageProcess')
  p_web.publicpage = 1
  if p_web.sessionId = 0 then p_web.NewSession().
        p_web.SSV('ProcessType','')
        p_web.SSV('ReturnURL','')
        p_web.SSV('RedirectURL','')
        p_web.SSV('AllFinishedText','Finished Processing')
        
        p_web.StoreValue('ProcessType')
        p_web.StoreValue('ReturnURL')
        ! If only ReturnURL passed, then make RedirectURL the same
        IF (p_web.IfExistsValue('RedirectURL'))
            p_web.StoreValue('RedirectURL')
        ELSE
            p_web.SSV('RedirectURL',p_web.GSV('ReturnURL'))
        END ! IF
        
        ! Clear Export File Variables
        p_web.SSV('locExportFile','')
        p_web.SSV('locExportFile2','')
        p_web.SSV('locExportFile3','')
        p_web.SSV('locExportFile4','')
        p_web.SSV('ExportFileName1','')
        p_web.SSV('ExportFileName2','')
        p_web.SSV('ExportFileName3','')    
        p_web.SSV('ExportFileName4','')    
  do Header
        do SendPacket
        Do allFinished
        do pleasewait
        do sendpacket
  packet = clip(packet) & p_web._jsBodyOnLoad(,,)
    do SendPacket
    Do redirect
    do SendPacket
  do Footer
  packet = clip(packet) & p_web.Popup()
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

BuildOutstandingOrders      ROUTINE  ! DEPRECATED
DATA
locAccountNumber    STRING(30)
bt  LONG()
locRecordcount      LONG()
locTotalRecords LONG()
kReturnURL  EQUATE('FormBrowseStock')
CODE
    p_web.SSV('locOrderType','S')
    p_web.SSV('RedirectURL','frmOutstandingOrders')

    STREAM(SBO_OutParts)
    STREAM(ORDHEAD)
    STREAM(ORDITEMS)
    STREAM(RETSTOCK)
    STREAM(RETSALES)
    STREAM(EXCHORDR)
    STREAM(LOAORDR)

    ProgressBarWeb.Init(RECORDS(SBO_OutParts) + RECORDS(ORDHEAD) + RECORDS(EXCHORDR) + RECORDS(LOAORDR))

    locTotalRecords = RECORDS(SBO_OutParts)

    ! Clear Existing Record
    Access:SBO_OutParts.ClearKey(sout:PartNumberKey)
    sout:SessionID = p_web.SessionID
    SET(sout:PartNumberKey,sout:PartNumberKey)
    LOOP UNTIL Access:SBO_OutParts.Next()
        IF (sout:SessionID <> p_web.SessionID)
            BREAK
        END

        locRecordcount += 1
        
        ProgressBarWeb.Update('Preparing...')

        Delete(SBO_OutParts)
    END

    locRecordcount = 0

    IF (p_web.GSV('BookingStoresAccount') = '')
        locAccountNumber = p_web.GSV('BookingAccount')
    ELSE
        locAccountNumber = p_web.GSV('BookingStoresAccount')
    END

    ! Loop Through Web Orders For This Account
    Access:ORDHEAD.Clearkey(orh:AccountDateKey)
    orh:Account_No = locAccountNumber
    Set(orh:AccountDateKey,orh:AccountDateKey)
    Loop
        If Access:ORDHEAD.Next()
            Break
        End ! If Access:ORDHEAD.Next()
        If orh:Account_No <> locAccountNumber
            Break
        End ! If orh:Account_No <> locAccountNumber

        locRecordcount += 1
        
        ProgressBarWeb.Update('Checking Part Orders...')


    ! Count unprocessed orders first (DBH: 06/12/2007)
        If orh:Procesed = 0
        ! Add up items on the unprocessed order (DBH: 06/12/2007)
            Access:ORDITEMS.Clearkey(ori:KeyOrdHNo)
            ori:OrdHNo = orh:Order_No
            Set(ori:KeyOrdHNo,ori:KeyOrdHNo)
            Loop
                If Access:ORDITEMS.Next()
                    Break
                End ! If Access:ORDITEMS.Next()
                If ori:OrdHNo <> orh:Order_No
                    Break
                End ! If ori:OrdHNo <> orh:Order_No
                If ori:Qty = 0
                    Cycle
                End ! If ori:Qty = 0

                Access:SBO_OutParts.ClearKey(sout:PartDescDateRaisedKey)
                sout:SessionID = p_web.SessionID
                sout:LineType = 'S'
                sout:PartNumber = ori:PartNo
                sout:Description = ori:partdiscription
                sout:DateRaised = orh:TheDate
                IF (Access:SBO_OutParts.TryFetch(sout:PartDescDateRaisedKey))
                    IF (Access:SBO_OutParts.PrimeRecord() = Level:Benign)
                        sout:SessionID = p_web.SessionID
                        sout:PartNumber = ori:PartNo
                        sout:Description = ori:partdiscription
                        sout:DateRaised = orh:TheDate
                        sout:Quantity = ori:Qty
                        sout:DateProcessed = 0
                        sout:LineType = 'S'
                        IF (Access:SBO_OutParts.TryInsert())
                        END

                    END

                ELSE
                    sout:Quantity += ori:Qty
                    Access:SBO_OutParts.TryUpdate()
                END
            End ! Loop

        Else ! If orh:Procesed = 0
            ! Find the retail sale associated with this web order (DBH: 06/12/2007)
            Access:RETSALES.Clearkey(ret:Ref_Number_Key)
            ret:Ref_Number = orh:SalesNumber
            If Access:RETSALES.TryFetch(ret:Ref_Number_Key) = Level:Benign
            ! Double check the the sale has the corresponding web order number (DBH: 06/12/2007)
                If ret:WebOrderNumber = 0
                    Cycle
                End ! If ret:WebOrderNumber = 0

            ! Count the items on the sale (DBH: 06/12/2007)
                Access:RETSTOCK.Clearkey(res:Part_Number_Key)
                res:Ref_Number = ret:Ref_Number
                Set(res:Part_Number_Key,res:Part_Number_Key)
                Loop
                    If Access:RETSTOCK.Next()
                        Break
                    End ! If Access:RETSTOCK.Next()
                    If res:Ref_Number <> ret:Ref_Number
                        Break
                    End ! If res:Ref_Number <> ret:Ref_Number
                ! Item should be on another sale, so don't count here. (DBH: 06/12/2007)
                    If res:Despatched = 'OLD'
                        Cycle
                    End ! If res:Despatched = 'OLD'

                    If res:Despatched = 'CAN'
                        CYCLE
                    END

                ! Count the part if it hasn't arrived yet (DBH: 06/12/2007)
                    Access:SBO_OutParts.ClearKey(sout:PartDescDateRaisedProcessedKey)
                    sout:SessionID = p_web.SessionID
                    sout:LineType = 'S'
                    sout:PartNumber = res:Part_Number
                    sout:Description = res:Description
                    sout:DateRaised = orh:TheDate
                    sout:DateProcessed = orh:Pro_Date
                    IF (Access:SBO_OutParts.TryFetch(sout:PartDescDateRaisedProcessedKey))
                        IF (res:Received = 0)
                            IF (Access:SBO_OutParts.PrimeRecord() = Level:Benign)
                                sout:SessionID = p_web.SessionID
                                sout:PartNumber = res:Part_Number
                                sout:Description = res:Description
                                sout:DateRaised = orh:TheDate
                                sout:DateProcessed = orh:Pro_date

                                sout:Quantity = res:Quantity


                                sout:LineType = 'S'
                                IF (Access:SBO_OutParts.TryInsert())
                                END

                            END
                        END

                    ELSE
                        IF (res:Received = 0)
                            sout:Quantity += res:Quantity
                            Access:SBO_OutParts.TryUpdate()
                        END

                    END
                End ! Loop
            End ! If Access:RETSALES.TryFetch(ret:Ref_Number_Key) = Level:Benign
        End ! If orh:Procesed = 0
    End ! Loop

    locRecordcount = 0
    
    ! Load exchange queue
    Access:EXCHORDR.ClearKey(exo:Ref_Number_Key)
    exo:Ref_Number = 1
    set(exo:Ref_Number_Key,exo:Ref_Number_Key)
    loop until Access:EXCHORDR.Next()

        locRecordcount += 1
        
        ProgressBarWeb.Update('Checking Exchange Orders...')


        if exo:Qty_Received < exo:Qty_Required              ! This check is just for safety, when exchange orders are completed the EXCHORDR record is deleted
            IF (exo:Location <> p_web.GSV('BookingSiteLocation'))
                CYCLE
            END



            IF (Access:SBO_OutParts.PrimeRecord() = Level:Benign)
                sout:SessionID = p_web.SessionID
                sout:PartNumber = exo:Model_Number
                sout:Description = exo:Manufacturer
                sout:Quantity = exo:Qty_Required - exo:qty_received
                sout:DateRaised = exo:DateCreated
                sout:LineType = 'E'
                IF (Access:SBO_OutParts.TryInsert() = Level:Benign)
                END

            END
        end
    end
    locRecordcount = 0

! Load loan queue
    Access:LOAORDR.ClearKey(lor:Ref_Number_Key)
    lor:Ref_Number = 1
    set(lor:Ref_Number_Key,lor:Ref_Number_Key)
    loop until Access:LOAORDR.Next()

        locRecordcount += 1
        
        ProgressBarWeb.Update('Checking Loan Orders...')

        
        if lor:Qty_Received < lor:Qty_Required
            IF (exo:Location <> p_web.GSV('BookingSiteLocation'))
                CYCLE
            END

            IF (Access:SBO_OutParts.PrimeRecord() = Level:Benign)
                sout:SessionID = p_web.SessionID
                sout:PartNumber = lor:Model_Number
                sout:Description = lor:Manufacturer
                sout:Quantity = lor:Qty_Required - lor:Qty_Received
                sout:DateRaised = lor:DateCreated
                sout:LineType = 'L'
                IF (Access:SBO_OutParts.TryInsert() = Level:Benign)
                END

            END

        end
    end

    FLUSH(SBO_OutParts)
    FLUSH(ORDHEAD)
    FLUSH(ORDITEMS)
    FLUSH(RETSTOCK)
    FLUSH(RETSALES)
    FLUSH(EXCHORDR)
    FLUSH(LOAORDR)
BuildPartsList      ROUTINE
DATA
locRecordCount      LONG()
kReturnURL  EQUATE('FormBrowseStock')
locTotalRecords     LONG()
bt  LONG()
CODE

    p_web.SSV('RedirectURL','FormBrowseOldPartNumber')
    IF (p_web.GSV('locOldPartNumber') = '')
        EXIT
    END
    
    ProgressBarWeb.Init(RECORDS(SBO_OutParts))

    locTotalRecords = RECORDS(SBO_OutParts)

    ! Clear Existing Record
    Access:SBO_OutParts.ClearKey(sout:PartNumberKey)
    sout:SessionID = p_web.SessionID
    SET(sout:PartNumberKey,sout:PartNumberKey)
    LOOP UNTIL Access:SBO_OutParts.Next()
        IF (sout:SessionID <> p_web.SessionID)
            BREAK
        END

        locRecordcount += 1

        ProgressBarWeb.Update('Clearing....')


        Delete(SBO_OutParts)
    END

    locRecordcount = 0
    
    ProgressBarWeb.Init(RECORDS(STOPARTS))

    Access:STOPARTS.Clearkey(spt:LocationOldPartKey)
    spt:Location = p_web.GSV('Default:SiteLocation')
    spt:OldPartNumber = p_web.GSV('locOldPartNumber')
    Set(spt:LocationOldPartKey,spt:LocationOldPartKey)
    Loop ! Begin Loop
        If Access:STOPARTS.Next()
            Break
        End ! If Access:STOPARTS.Next()

        locRecordcount += 1

        ProgressBarWeb.Update('Searching For Parts...')


        If spt:Location <> p_web.GSV('Default:SiteLocation')
            Break
        End ! If spt:Location <> f:Location
        If spt:OldPartNumber <> p_web.GSV('locOldPartNumber')
            Break
        End ! If spt:OldPartNumber <> f:PartNumber
        Access:STOCK.ClearKey(sto:Ref_Number_Key)
        sto:Ref_Number = spt:STOCKRefNumber
        If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
        !Found
        Else ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
        !Error
            Cycle
        End ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign

        IF (Access:SBO_OutParts.PrimeRecord() = Level:Benign)
            sout:SessionID = p_web.SessionID
            sout:LineType = 'S'
            sout:PartNumber = sto:Part_Number
            sout:Description = sto:Description
            sout:Quantity = sto:Ref_Number ! Use to get part number later
            IF (Access:SBO_OutParts.TryInsert())
                Access:SBO_OutParts.CancelAutoInc()
            END
        END

    End ! Loop
    Access:STOCK.ClearKey(sto:Location_Key)
    sto:Part_Number = p_web.GSV('locOldPartNumber')
    sto:Location = p_web.GSV('Default:SiteLocation')
    If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
    !Found
        IF (Access:SBO_OutParts.PrimeRecord() = Level:Benign)
            sout:SessionID = p_web.SessionID
            sout:LineType = 'S'
            sout:PartNumber = sto:Part_Number
            sout:Description = sto:Description
            sout:Quantity = sto:Ref_Number ! Use to get part number later
            IF (Access:SBO_OutParts.TryInsert())
                Access:SBO_OutParts.CancelAutoInc()
            END
        END
    Else ! If Access:STOCK.TryFetch(sto:Part_Number_Key) = Level:Benign
    !Error
    End ! If Access:STOCK.TryFetch(sto:Part_Number_Key) = Level:Benign
BuildExchangeAllocationQueue           Routine
Data
local:WrongSite     Byte(0)
local:FoundHistory  Byte(0)
locRecordCount      LONG()
locTotalRecords     LONG()
bt LONG()

Code

    ProgressBarWeb.Init(RECORDS(JOBS) / 1000)

    ClearSBOGenericFile(p_web)

    Access:STATUS.ClearKey(sts:Ref_Number_Only_Key)
    sts:Ref_Number = 350
    If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
        !Found
    Else!If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign

    locTotalRecords = RECORDS(JOBS)

    Access:JOBS.ClearKey(job:ExcStatusKey)
    job:Exchange_Status = sts:Status
    Set(job:ExcStatusKey,job:ExcStatusKey)
    Loop
        If Access:JOBS.NEXT()
            Break
        End !If
        If job:Exchange_Status <> sts:Status      |
            Then Break.  ! End If

        locRecordcount += 1
        
        ProgressBarWeb.Update('Working...')
        

        !Which engineer changed the status. Is that engineer at this site? - 4000 (DBH: 04-03-2004)
        local:WrongSite = False
        local:FoundHistory = False

        Access:AUDSTATS.ClearKey(aus:DateChangedKey)
        aus:RefNumber   = job:Ref_Number
        aus:Type        = 'EXC'
        aus:DateChanged = Today()
        Set(aus:DateChangedKey,aus:DateChangedKey)
        Loop
            If Access:AUDSTATS.PREVIOUS()
                Break
            End !If
            If aus:RefNumber   <> job:Ref_Number      |
                Or aus:Type        <> 'EXC'      |
                Then Break.  ! End If
            If aus:NewStatus = sts:Status
                Access:USERS.Clearkey(use:User_Code_Key)
                use:User_Code   = aus:UserCode
                If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                    !Found
                    If use:Location <> p_web.GSV('Default:SiteLocation')
                        local:WrongSite = True
                    End !If use:Location <> p_web.GSV('Default:SiteLocation')
                Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                    !Error
                End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                ! Inserting (DBH 13/04/2007) # 8912 - Found correct status history
                local:FoundHistory = True
                ! End (DBH 13/04/2007) #8912
                Break
            End !If aus:NewStatus = tmp:Status
        End !Loop

        If local:WrongSite = True
            Cycle
        End !If WrongSite# = True

        ! Inserting (DBH 13/04/2007) # 8912 - Add double check in case there is no status history, or even audit trail to determine location
        If local:FoundHistory = False
            Access:AUDIT.Clearkey(aud:TypeActionKey)
            aud:Ref_Number = job:Ref_Number
            aud:Type = 'EXC'
            aud:Action = '48 HOUR EXCHANGE ORDER CREATED'
            aud:Date = Today()
            Set(aud:TypeActionKey,aud:TypeActionKey)
            Loop ! Begin Loop
                If Access:AUDIT.Next()
                    Break
                End ! If Access:AUDIT.Next()
                If aud:Ref_Number <> job:Ref_Number
                    Break
                End ! If aud:Ref_Number <> job:Ref_Number
                If aud:Type <> 'EXC'
                    Break
                End ! If aud:Type <> 'EXC'
                If aud:Action <> '48 HOUR EXCHANGE ORDER CREATED'
                    Break
                End ! If aud:Action <> '48 HOUR EXCHANGE ORDER CREATED'
                If aud:Date > Today()
                    Break
                End ! If aud:Date > Today()
                Access:USERS.ClearKey(use:User_Code_Key)
                use:User_Code = aud:User
                If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                    !Found
                    If use:Location <> p_web.GSV('Default:SiteLocation')
                        local:WrongSite = True
                    End ! If use:Location <> p_web.GSV('Default:SiteLocation')
                Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                    !Error
                End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                local:FoundHistory = True
                Break
            End ! Loop

            If local:WrongSite = True
                Cycle
            End ! If local:WrongSite = True
        End ! If local:FoundStatusHistory = False

        If local:FoundHistory = False
            ! Can't find status or audit history, then just compare the booking location and assume the RRC ordered the exchange (DBH: 13/04/2007)
            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = p_web.GSV('BookingAccount')
            If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Found
                If tra:SiteLocation <> p_web.GSV('Default:SiteLocation')
                    local:WrongSite = True
                End ! If tra:SiteLocation <> p_web.GSV('Default:SiteLocation')
            Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Error
            End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        End ! If local:FoundHistory = False
        If local:WrongSite = True
            Cycle
        End ! If local:WrongSite = True
        ! End (DBH 13/04/2007) #8912

        IF (Access:SBO_GenericFile.PrimeRecord() = Level:Benign)
            sbogen:SessionID = p_web.SessionID
            sbogen:Long1 = job:Ref_Number
            sbogen:String1 = job:Manufacturer
            sbogen:String2 = job:Model_Number
            ! Use this to indicate if it's 48 Hour or not - TrkBs: 6841 (DBH: 09-12-2005)
            sbogen:Long2 = 0
            Access:EXCHORDR.ClearKey(exo:By_Location_Key)
            exo:Location     = p_web.GSV('Default:SiteLocation')
            exo:Manufacturer = job:Manufacturer
            exo:Model_Number = job:Model_Number
            If Access:EXCHORDR.TryFetch(exo:By_Location_Key) = Level:Benign
                !Found
                ! Use this field to indicate the "On Order" - TrkBs: 6841 (DBH: 09-12-2005)
                sbogen:Byte2 = 1
            Else!If Access:EXCHORDR.TryFetch(exo:By_Location_Key) = Level:Benign
                !Error
                sbogen:Byte2 = 0
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:EXCHORDR.TryFetch(exo:By_Location_Key) = Level:Benign
            ! Use this field to indicate the "48 Hour" - TrkBs: 6841 (DBH: 09-12-2005)
            sbogen:Byte1 = 0

            If Access:SBO_GenericFile.TryInsert() = Level:Benign
                ! Insert Successful
            Else ! If Access:RAPENGLS.TryInsert() = Level:Benign
                ! Insert Failed
                Access:SBO_GenericFile.CancelAutoInc()
            End ! If Access:RAPENGLS.TryInsert() = Level:Benign
        End !If Access:RAPENGLS.PrimeRecord() = Level:Benign
    End !Loop

    Access:STATUS.ClearKey(sts:Ref_Number_Only_Key)
    sts:Ref_Number = 360
    If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
        !Found

    Else!If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign

    Access:JOBS.ClearKey(job:ExcStatusKey)
    job:Exchange_Status = sts:Status
    Set(job:ExcStatusKey,job:ExcStatusKey)
    Loop
        If Access:JOBS.NEXT()
            Break
        End !If
        If job:Exchange_Status <> sts:Status      |
            Then Break.  ! End If

        locRecordcount += 1
        
        ProgressBarWeb.Update('Working...')
        

        !Which engineer changed the status. Is that engineer at this site? - 4000 (DBH: 04-03-2004)
        local:FoundHistory = False
        local:WrongSite = False

        Access:AUDSTATS.ClearKey(aus:DateChangedKey)
        aus:RefNumber   = job:Ref_Number
        aus:Type        = 'EXC'
        aus:DateChanged = Today()
        Set(aus:DateChangedKey,aus:DateChangedKey)
        Loop
            If Access:AUDSTATS.PREVIOUS()
                Break
            End !If
            If aus:RefNumber   <> job:Ref_Number      |
                Or aus:Type        <> 'EXC'      |
                Then Break.  ! End If
            If aus:NewStatus = sts:Status
                Access:USERS.Clearkey(use:User_Code_Key)
                use:User_Code   = aus:UserCode
                If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                    !Found
                    If use:Location <> p_web.GSV('Default:SiteLocation')
                        local:WrongSite = True
                    End !If use:Location <> p_web.GSV('Default:SiteLocation')
                Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                    !Error
                End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                local:FoundHistory = True
                Break
            End !If aus:NewStatus = tmp:Status
        End !Loop

        If local:WrongSite = True
            Cycle
        End !If WrongSite# = True

        ! Inserting (DBH 13/04/2007) # 8912 - Add double check in case there is no status history, or even audit trail to determine location
        If local:FoundHistory = False

            Access:AUDIT.Clearkey(aud:TypeActionKey)
            aud:Ref_Number = job:Ref_Number
            aud:Type = 'EXC'
            aud:Action = '48 HOUR EXCHANGE ORDER CREATED'
            aud:Date = Today()
            Set(aud:TypeActionKey,aud:TypeActionKey)
            Loop ! Begin Loop
                If Access:AUDIT.Next()
                    Break
                End ! If Access:AUDIT.Next()
                If aud:Ref_Number <> job:Ref_Number
                    Break
                End ! If aud:Ref_Number <> job:Ref_Number
                If aud:Type <> 'EXC'
                    Break
                End ! If aud:Type <> 'EXC'
                If aud:Action <> '48 HOUR EXCHANGE ORDER CREATED'
                    Break
                End ! If aud:Action <> '48 HOUR EXCHANGE ORDER CREATED'
                If aud:Date > Today()
                    Break
                End ! If aud:Date > Today()
                Access:USERS.ClearKey(use:User_Code_Key)
                use:User_Code = aud:User
                If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                    !Found
                    If use:Location <> p_web.GSV('Default:SiteLocation')
                        local:WrongSite = True
                    End ! If use:Location <> p_web.GSV('Default:SiteLocation')
                Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                    !Error
                End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                local:FoundHistory = True
                Break
            End ! Loop

            If local:WrongSite = True
                Cycle
            End ! If local:WrongSite = True
        End ! If local:FoundStatusHistory = False

        If local:FoundHistory = False
            ! Can't find status or audit history, then just compare the booking location and assume the RRC ordered the exchange (DBH: 13/04/2007)
            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = p_web.GSV('BookingAccount')
            If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Found
                If tra:SiteLocation <> p_web.GSV('Default:SiteLocation')
                    local:WrongSite = True
                End ! If tra:SiteLocation <> p_web.GSV('Default:SiteLocation')
            Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Error
            End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        End ! If local:FoundHistory = False
        If local:WrongSite = True
            Cycle
        End ! If local:WrongSite = True
        ! End (DBH 13/04/2007) #8912

        If Access:SBO_GenericFile.PrimeRecord() = Level:Benign
            sbogen:SessionID = p_web.SessionID
            sbogen:Long1 = job:Ref_Number
            sbogen:String1 = job:Manufacturer
            sbogen:String2 = job:Model_Number
            sbogen:Byte2 = 0
            ! Use this to indicate if it's 48 Hour or not - TrkBs: 6841 (DBH: 09-12-2005)
            sbogen:Long2 = 1
            !Has the exchange order been processed? -  (DBH: 30-10-2003)
            Access:EXCHOR48.ClearKey(ex4:LocationJobKey)
            ex4:Received  = 1
            ex4:Returning = 0
            ex4:Location  = p_web.GSV('Default:SiteLocation')
            ex4:JobNumber = sbogen:Long1
            If Access:EXCHOR48.TryFetch(ex4:LocationJobKey) = Level:Benign
                sbogen:Byte1 = 2
            Else !If Access:EXCHOR48.TryFetch(ex4:LocationJobKey) = Level:Benign
                sbogen:Byte1 = 1
            End !If Access:EXCHOR48.TryFetch(ex4:LocationJobKey) = Level:Benign
            If Access:SBO_GenericFile.TryInsert() = Level:Benign
                ! Insert Successful
            Else ! If Access:RAPENGLS.TryInsert() = Level:Benign
                ! Insert Failed
                Access:SBO_GenericFile.CancelAutoInc()
            End ! If Access:RAPENGLS.TryInsert() = Level:Benign
        End !If Access:RAPENGLS.PrimeRecord() = Level:Benign

    End !Loop

CreateRRCWarrantyInvoice    ROUTINE
    DATA
locAuditNotes   STRING(255)
    CODE
        p_web.FileToSessionQueue(JOBS)
        p_web.FileToSessionQueue(WEBJOB)
        p_web.FileToSessionQueue(JOBSE)
            
        p_web.SSV('jobe:InvRRCWLabourCost',p_web.GSV('jobe:RRCWLabourCost'))
        p_web.SSV('jobe:InvRRCWPartsCost',p_web.GSV('jobe:RRCWPartsCost'))
        p_web.SSV('jobe:InvRRCWSubTotal',p_web.GSV('jobe:RRCWLabourCost') + p_web.GSV('jobe:RRCWPartsCost'))
            
        p_web.SSV('wob:EDI','PAY')
        p_web.SSV('wob:RRCWInvoiceNumber',p_web.GSV('WarrInvoiceNumber'))
        p_web.SSV('wob:ReconciledMarker',0)
            
        GetStatus(910,0,'JOB',p_web)
            
        p_web.SessionQueueToFile(JOBS)
        p_web.SessionQueueToFile(JOBSE)
        p_web.SessionQueueToFile(WEBJOB)
            
        IF (Access:WEBJOB.TryUpdate() = Level:Benign)
            p_web.SSV('WarrCountJobs',p_web.GSV('WarrCountJobs') + 1)
                
            p_web.SSV('WarrTotalLabour',p_web.GSV('WarrTotalLabour') + jobe:RRCWLabourCost)
            p_web.SSV('WarrTotalParts',p_web.GSV('WarrTotalPats') + jobe:RRCWPartsCost)
                
            Access:JOBSE.TryUpdate()

            jow:RRCStatus = 'PAY'
            jow:RRCDateReconciled = TODAY()
            Access:JOBSWARR.TryUpdate()

            ! Update all occurances of this job in the temp file
            Access:SBO_WarrantyClaims.ClearKey(sbojow:JobNumberOnlyKey)
            sbojow:JobNumber = jow:RefNumber
            SET(sbojow:JobNumberOnlyKey,sbojow:JobNumberOnlyKey)
            LOOP UNTIL Access:SBO_WarrantyClaims.Next() <> Level:Benign
                IF (sbojow:JobNumber <> jow:RefNumber)
                    BREAK
                END ! IF
                sbojow:RRCStatus = 'PAY'
                Access:SBO_WarrantyClaims.TryUpdate()
            END ! LOOP

                
            IF (Access:JOBS.TryUpdate() = Level:Benign)
                locAuditNotes         = 'BATCH NO: ' & Clip(job:EDI_Batch_Number) & |
                    '<13,10>WARRANTY CLAIM PAYMENT ACKNOWLEDGED<13,10>EXCH/HANDLING: '
                If job:Exchange_Unit_Number <> ''
                    locAuditNotes   = Clip(locAuditNotes) & Format(jobe:ExchangeRate,@n10.2)
                Else !If job:Exchange_Unit_Number <> ''
                    locAuditNotes   = Clip(locAuditNotes) & Format(jobe:HandlingFee,@n10.2)
                End !If job:Exchange_Unit_Number <> ''
                locAuditNotes  = Clip(locAuditNotes) & '<13,10>LABOUR: ' & Format(jobe:RRCWLabourCost,@n10.2) & |
                    '<13,10>PARTS: ' & Format(jobe:RRCWPartsCost,@n10.2)
                    
                AddToAudit(p_web,job:Ref_Number,'JOB','WARRANTY CLAIM MARKED PAID (RRC) INV NO: ' & p_web.GSV('WarrInvoiceNumber'),locAuditNotes)
                    
            END ! IF
                
        END ! IF            

GetRejectionReason_AAJ      Routine
    locRejectionReason = ''
    Access:AUDIT.Clearkey(aud:TypeActionKey)
    aud:Ref_Number = jow:RefNumber
    aud:Type = 'JOB'
    aud:Action = 'WARRANTY CLAIM REJECTION ACKNOWLEDGED'
    Set(aud:TypeActionKey,aud:TypeActionKey)
    Loop ! Begin Loop
        If Access:AUDIT.Next()
            Break
        End ! If Access:AUDIT.Next()
        If aud:Ref_Number <> jow:RefNumber
            Break
        End ! If aud:Ref_Number <> jow:RefNumber
        If aud:Type <> 'JOB'
            Break
        End ! If aud:Type = 'JOB'
        If aud:Action <> 'WARRANTY CLAIM REJECTION ACKNOWLEDGED'
            Break
        End ! If aud:Action <> 'WARRANTY CLAIM REJECTED'
        
        Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
        aud2:AUDRecordNumber = aud:Record_Number
        IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)

        END ! IF
        locRejectionReason = Clip(Sub(aud2:Notes,9,60))
        if (jow:DateFinalRejection = 0)
            jow:DateFinalRejection = aud:Date
        end
        break
    End ! Loop
GetRejectionReason_EXC      Routine
    locDaysToFinalRejection = ''
    Access:MANUFACT.ClearKey(man:Manufacturer_Key)
    man:Manufacturer = jow:Manufacturer
    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
    !Found
        If man:UseResubmissionLimit
            locDaysToFinalRejection = jow:DateRejected + man:ResubmissionLimit - Today()
        End ! If man:UseResubmissionLimit
    End

    

    Access:AUDIT.Clearkey(aud:TypeActionKey)
    aud:Ref_Number = jow:RefNumber
    aud:Type = 'JOB'
    aud:Action = 'WARRANTY CLAIM REJECTED'
    Set(aud:TypeActionKey,aud:TypeActionKey)
    Loop ! Begin Loop
        If Access:AUDIT.Next()
            Break
        End ! If Access:AUDIT.Next()
        If aud:Ref_Number <> jow:RefNumber
            Break
        End ! If aud:Ref_Number <> jow:RefNumber
        If aud:Type <> 'JOB'
            Break
        End ! If aud:Type = 'JOB'
        If aud:Action <> 'WARRANTY CLAIM REJECTED'
            Break
        End ! If aud:Action <> 'WARRANTY CLAIM REJECTED'
        
        Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
        aud2:AUDRecordNumber = aud:Record_Number
        IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)

        END ! IF
        
        locRejectionReason = Clip(Sub(aud2:Notes,9,60))
        if (jow:DateRejected = 0)
            jow:DateRejected = aud:Date
            If man:UseResubmissionLimit
                locDaysToFinalRejection = jow:DateRejected + man:ResubmissionLimit - Today()
            End ! If man:UseResubmissionLimit
        end

        break
    End ! Loop

    Count# = 1
    Access:AUDIT.Clearkey(aud:TypeActionKey)
    aud:Ref_Number = jow:RefNumber
    aud:Type = 'JOB'
    aud:Action = 'WARRANTY CLAIM RESUBMITTED'
    Set(aud:TypeActionKey,aud:TypeActionKey)
    Loop ! Begin Loop
        If Access:AUDIT.Next()
            Break
        End ! If Access:AUDIT.Next()
        If aud:Ref_Number <> jow:RefNumber
            Break
        End ! If aud:Ref_Number <> jow:RefNumber
        If aud:Type <> 'JOB'
            Break
        End ! If aud:Type = 'JOB'
        If aud:Action <> 'WARRANTY CLAIM RESUBMITTED'
            Break
        End ! If aud:Action <> 'WARRANTY CLAIM REJECTED'
        Count# += 1
    End ! Loop
    locSubmissions = Count#

GetRejectionReason_REJ      Routine
    locRejectionReason = ''
    Access:AUDIT.Clearkey(aud:TypeActionKey)
    aud:Ref_Number = jow:RefNumber
    aud:Type = 'JOB'
    aud:Action = 'WARRANTY CLAIM FINAL REJECTION'
    Set(aud:TypeActionKey,aud:TypeActionKey)
    Loop ! Begin Loop
        If Access:AUDIT.Next()
            Break
        End ! If Access:AUDIT.Next()
        If aud:Ref_Number <> jow:RefNumber
            Break
        End ! If aud:Ref_Number <> jow:RefNumber
        If aud:Type <> 'JOB'
            Break
        End ! If aud:Type = 'JOB'
        If aud:Action <> 'WARRANTY CLAIM FINAL REJECTION'
            Break
        End ! If aud:Action <> 'WARRANTY CLAIM REJECTED'
        Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
        aud2:AUDRecordNumber = aud:Record_Number
        IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)

        END ! IF
        locRejectionReason = Clip(Sub(aud2:Notes,9,60))
        if (jow:DateFinalRejection = 0)
            jow:DateFinalRejection = aud:Date
        end
        break
    End ! Loop
SendPacket  Routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,Net:NoHeader)
    packet = ''
  end
Header Routine
  packet = p_web.w3Header()
  packet = clip(packet) & '<head>'&|
      '<title>'&clip(p_web.site.PageTitle)&'</title>'&|
      '<meta http-equiv="Content-Type" content="text/html; charset='&clip(p_web.site.HtmlCharset)&'" /><13,10>'
  packet =  clip(packet) & p_web.IncludeStyles()
  packet =  clip(packet) & p_web.IncludeScripts()
  packet = clip(packet) & p_web.AddScript('progressbar.js')
    Do heading
  packet = clip(packet) & '</head><13,10>'
  p_web.ParseHTML(packet,1,0,Net:SendHeader+Net:DontCache)
  packet = ''
Footer Routine
  packet = clip(packet) & '<!-- Net:SelectField --><13,10>' &|
                          '<script>bodyOnLoad();</script><13,10>' &|
                         '</body><13,10></html><13,10>'
heading  Routine
  packet = clip(packet) & |
    '<13,10>'&|
    ''
pleasewait  Routine
  packet = clip(packet) & |
    '<13,10>'&|
    ''
    DO progressBar
    DO SendPacket
    
    VodacomClass.AddToFileLog(p_web.SessionID,'PageProcess: ' & p_web.GSV('ProcessType'),'Start') ! Debug
    
  CASE p_web.GSV('ProcessType')
    OF 'AvailableExchangeExport'
        AvailableExchangeExport()
    OF  'BuildAvailableExchangeLoanStock'
        BuildAvailableExchangeLoanStock()
    OF 'BuildJobsProgressList'  
        IF (p_web.IfExistsValue('searchtype'))
            p_web.StoreValue('searchtype')
        END ! IF
        BuildJobsProgressList()  
    OF 'BuildStockReceiveList'
        BuildStockReceiveList()  
    OF 'BuildWarrantyClaimsBrowse'
    BuildWarrantyClaimsBrowse()
    OF 'ChargeableIncomeReportEngine'
        ChargeableIncomeReportEngine()
    OF 'ChargeableIncomeReportExport'
        ChargeableIncomeReportExport()
    OF 'CreateExchangeLoanOrder'
        CreateExchangeLoanOrder()
    OF 'CreateGRN'
        CreateGRN()
    OF 'CreditReportExport'
        CreditReportExport()
  	OF 'CountBouncers'
  		CountBouncers()
  OF 'ExchangeAllocation'
      DO BuildExchangeAllocationQueue
    OF 'ExchangeAuditExport'  
        ExchangeAuditExport()
    OF 'ExchangeLoanAvailableExport'
        ExchangeLoanAvailableExport()
    OF 'ExchangeLoanDeviceSoldReport'  
        ExchangeLoanDeviceSoldReport()
    OF 'ExchangeLoanUnitsReturnedToMainStore'
        ExchangeLoanUnitsReturnedToMainStore()
    OF 'ExchangePhoneExport'
        ExchangePhoneExport()
    OF 'ExchangeStockValuationExport'
        ExchangeStockValuationExport()
    OF 'ExportWarrantyClaims'
        ExportWarrantyClaims()
    OF 'HandlingFeeReportEngine'
        HandlingFeeReportEngine()
    OF 'GenericTagFileClear'
        GenericTagFileClear()
    OF 'GlobalIMEISearch'
        GlobalIMEIList(p_web.GSV('GlobalIMEINumber'))
    OF 'GenericTagFileTagAll'
        GenericTagFileTagAll()
    OF 'InsuranceReportExport'
        InsuranceReportExport()
    OF 'LoanPhoneExport'
        LoanPhoneExport()
    OF 'LoanUnitAvailable'
        LoanUnitAvailable()
    OF 'OpenJob'   
        OpenJob()
  OF 'OldPartNumber'
      DO BuildPartsList
    OF 'OutstandingStockOrders'
        OutStandingStockOrders()
      !DO BuildOutstandingOrders
    OF 'RapidEngineerUpdate'
        RapidEngineerUpdate()
    OF 'ReturnExchangeUnits'
        ReturnExchangeUnits()
    OF 'ReturnLoanUnits'
    ReturnLoanUnits()
    OF 'RRCWarrantyInvoice'
        RRCWarrantyInvoice()  
    OF 'RRCWarrantyInvoiceImport'
        RRCWarrantyInvoiceImport()
    OF 'RRCWarrantyAccRejImport'  
        RRCWarrantyAccRejImport(0)
    OF 'RRCWarrantyResubmission'
        RRCWarrantyAccRejImport(1)
    OF 'SMSResponseReport'
        SMSResponseReport()
    OF 'SMSTaggedJobs'
        SMSTaggedJobs()
    OF 'StatusReportCriteriaGet'
        StatusReportCriteriaGet()
    OF 'StatusReportCriteriaSave'
        StatusReportCriteriaSave()
    OF 'StockAuditReport'
        StockAuditReport()
    OF 'StockCheckReport'  
        StockCheckReport()
    OF 'StockReceiveProcess'
        StockReceivePRocess()
    OF 'StatusReportEngine'
        StatusReportEngine()
  !          StatusReportEngine()
  !          StatusReportEngine()
  !        StatusReportEngine()
  !        StatusReportEngine()
  !        StatusReportEngine()
  !        StatusReportEngine()
  !        StatusReportEngine()
  !        StatusReportEngine()
  !        StatusReportEngine()
    OF 'StatusReportExport'
        StatusReportExport()
    OF 'StockValueReportEngine'
        StockValueReportEngine()
    OF 'UpdateEngineerNotes'
        UpdateEngineerNotes()
    OF 'TagStockTypes'
        TagStockTypes()
    OF 'WarrantyIncomeReportEngine'
        WarrantyIncomeReportEngine()
    OF 'WaybillComplete'
        WaybillComplete()
    OF 'WaybillConfirmation'
        WaybillConfirmation()
    OF 'WaybillGeneration'
        WaybillGeneration()
    OF 'WaybillProcessJob'   
        WaybillProcessJob()
    OF 'WaybillSundryGeneration'
        WaybillSundryGeneration()
    OF 'WIPAuditExport'
        WIPAuditExport()
    ELSE
        DO ReturnFail
    END ! Case
redirect  Routine
    IF (p_web.GetValue('debug') = '1')
        ! So I can test this page
        EXIT
    END ! IF
  
    VodacomClass.AddToFileLog(p_web.SessionID,'PageProcess: ' & p_web.GSV('ProcessType'),'End - Redirect') ! Debug  
  ! No redirect URL, assume going to FileDownload
    IF (p_web.GSV('RedirectURL') <> '')
        CreateScript(packet,'window.open("' & p_web.GSV('RedirectURL') & '","_self")')
    ELSE
        CreateScript(packet,'window.open("FileDownload","_self")')
    END ! IF
  packet = clip(packet) & |
    '<13,10>'&|
    ''
ReturnFail  Routine
    IF (p_web.GetValue('debug') = '1')
        ! So I can test this page
        EXIT
    END ! IF
    
    VodacomClass.AddToFileLog(p_web.SessionID,'PageProcess: ' & p_web.GSV('ProcessType'),'End - Fail') ! Debug    
    ! No Return URL, so assume closing the window
    IF (p_web.GSV('ReturnURL') <> '')
        CreateScript(packet,'window.open("' & p_web.GSV('ReturnURL') & '","_self")')
    ELSE
        CreateScript(packet,'window.open("DeadEnd","_self")')
    END ! IF
  packet = clip(packet) & |
    '<13,10>'&|
    ''
progressBar  Routine
  packet = clip(packet) & |
    '<<div id="progressframe" class="nt-process"><13,10>'&|
    '<<p class="nt-process-text">Please wait while ServiceBase searches for matching records<<br/><<br/>This may take a few minutes...<</br><<br/><</p><13,10>'&|
    '<<p id="progtitle" class="nt-process-text"><<br/><<br/><13,10>'&|
    '<<div id="progbarwrapper" class="smallish-progress-wrapper"><13,10>'&|
    '<<div id="progbar" class="smallish-progress-bar"><</div><13,10>'&|
    '<<div id="progtext" class="smallish-progress-text"><</div><13,10>'&|
    '<</div><13,10>'&|
    '<<br/><<a href="javascript:window.close();">Cancel Process<</a><13,10>'&|
    '<</p><13,10>'&|
    '<</div><13,10>'&|
    ''
allFinished  Routine
  packet = clip(packet) & |
    '<<div id="allfinished" class="nt-process" style="display:none"><13,10>'&|
    '<<h2><<span id="allfinished_title">Process Finished<</span><</h2><13,10>'&|
    '<<button id="close" onclick="window.close();" class="MessageBoxButton">Close Tab<</button><13,10>'&|
    '<</div><13,10>'&|
    ''
! Available Exchange Export (Summary/Detailed)
AvailableExchangeExport     PROCEDURE()
countTagged	LONG
taggedStockType STRING(30)
dateReceived DATE()
qSummary	QUEUE,PRE(qSummary)
Location	STRING(30)
Manufacturer STRING(30)
ModelNumber STRING(30)
Quantity	LONG()
			END ! QUEUE
recordCount                 LONG()
i                           LONG()
                    MAP
Export                  PROCEDURE(STRING pStockType)
                END ! MAP
	CODE
		Relate:EXCHANGE.Open()
		Relate:TagFile.Open()
        Relate:EXCHHIST.Open()
        
        IF (p_web.GSV('locAllStockTypes') <> 1)
		
            Access:TagFile.ClearKey(tag:KeyTagged)
            tag:SessionID = p_web.SessionID
            SET(tag:KeyTagged,tag:KeyTagged)
            LOOP UNTIL Access:TagFile.Next() <> Level:Benign
                IF (tag:SessionID <> p_web.SessionID)
                    BREAK
                END ! IF
                IF (tag:tagged = 0)
                    CYCLE
                END ! IF
                countTagged += 1
                taggedStockType = tag:TaggedValue
            END ! LOOP
		
            IF (countTagged = 0)
                packet = CLIP(packet) & '<script>alert("You have not tagged any stock types.");</script>'
                Do ReturnFail
            END ! IF
            ProgressBarWeb.Init(countTagged)
        ELSE
            ProgressBarWeb.Init(RECORDS(STOCKTYP))
        END ! IF
        
		
        IF (p_web.GSV('locExportType') = 1)
            rtn# = expfil.Init('Available Exchange Units Export (Summary)')
        ELSE ! IF
            rtn# = expfil.Init('Available Exchange Units Export (Detailed)')
		END ! IF
		
		
		expfil.AddField('Available Exchange Units Report',1)
        expfil.AddField('Location',1)
        expfil.AddField(p_web.GSV('BookingSiteLocation'),,1)
        expfil.AddField('Stock Type',1)
		IF (countTagged = 1)
			expfil.AddField(tag:TaggedValue,,1)
		ELSE
			expfil.AddField('Multiple',,1)
		END ! IF
        expfil.AddField('Date Created',1)
        expfil.AddField(FORMAT(TODAY(),@d06),,1)
		expfil.AddField(,1,1)

		
		IF (p_web.GSV('locExportType') = 1)
            expfil.AddField('Report Type',1)
            expfil.AddField('Summary',,1)
            expfil.AddField(,1,1) ! Blank Line
            expfil.AddField('Location',1)
            expfil.AddField('Manufacturer')
            expfil.AddField('Model Number')
            expfil.AddField('Quantity',,1)
		ELSE
            expfil.AddField('Report Type',1)
            expfil.AddField('Detailed',,1)
            expfil.AddField(,1,1) ! Blank Line
            expfil.AddField('Location',1)
            expfil.AddField('Stock Type')
            expfil.AddField('Manufacturer')
            expfil.AddField('Model Number')
            expfil.AddField('Description')
            expfil.AddField('Date Received')
            expfil.AddField('Days Available',,1)
		END ! IF
		
		FREE(qSummary)
		
        IF (p_web.GSV('locAllStockTypes') <> 1)
            Access:TagFile.ClearKey(tag:KeyTagged)
            tag:SessionID = p_web.SessionID
            SET(tag:KeyTagged,tag:KeyTagged)
            LOOP UNTIL Access:TagFile.Next() <> Level:Benign
                IF (tag:SessionID <> p_web.SessionID)
                    BREAK
                END ! IF
                IF (tag:tagged = 0)
                    CYCLE
                END ! IF
			
                Export(tag:taggedValue)
			
            END ! LOOP
        ELSE ! IF
            Access:STOCKTYP.ClearKey(stp:Use_Exchange_Key)
            stp:Use_Exchange = 'YES'
            SET(stp:Use_Exchange_Key,stp:Use_Exchange_Key)
            LOOP UNTIL Access:STOCKTYP.Next() <> Level:Benign
                IF (stp:Use_Exchange <> 'YES')
                    BREAK
                END ! IF
                Export(stp:Stock_Type)
            END ! LOOP
        END ! IF
        
		
		IF (p_web.GSV('locExportType') = 1)
			! Export Summary
            ProgressBarWeb.Init(RECORDS(qSummary))
			LOOP i = 1 TO RECORDS(qSummary)
                GET(qSummary,i)
                ProgressBarWeb.Update('Exporting Summary...')
                
                expfil.AddField(qSummary.Location,1)
                expfil.AddField(qSummary.Manufacturer)
                expfil.AddField(qSummary.ModelNumber)
                expfil.AddField(qSummary.Quantity,,1)

			END ! LOOP
            expfil.AddField('Totals',1)
            expfil.AddField()
            expfil.AddField(RECORDS(qSummary))
            expfil.AddField(recordCount,,1)
        ELSE
            expfil.AddField('Total Records',1)
            expfil.AddField(recordCount,,1)
		END ! IF

        expfil.Kill()
		
		Relate:EXCHANGE.Close()
		Relate:TagFile.Close()
		Relate:EXCHHIST.Open()
		
        IF (recordCount = 0)
            CreateScript(packet,kNoREcordsAlert)
            REMOVE(ExportFile)
            DO ReturnFail
        END 
Export              PROCEDURE(STRING pStockType)
    CODE
        Access:EXCHANGE.ClearKey(xch:LocStockAvailModelKey)
        xch:Location = p_web.GSV('BookingSiteLocation')
        xch:Stock_Type = pStockType
        xch:Available = 'AVL'
        SET(xch:LocStockAvailModelKey,xch:LocStockAvailModelKey)
        LOOP UNTIL Access:EXCHANGE.Next() <> Level:Benign
            IF (xch:Location <> p_web.GSV('BookingSiteLocation') OR |
                xch:Stock_Type <> pStockType OR |
                xch:Available <> 'AVL')
                BREAK
            END ! IF
                
            ProgressBarWeb.Update('Exporting: ' & CLIP(tag:taggedValue) & '...')
				
            IF (p_web.GSV('locExportType') = 0)
					! Summary, so export the data
                expfil.AddField(xch:Location,1)
                expfil.AddField(xch:Stock_Type)
                expfil.AddField(xch:Manufacturer)
                expfil.AddField(xch:Model_Number)
                expfil.AddField('`' & xch:ESN)
					
                dateReceived = 0
                Access:EXCHHIST.ClearKey(exh:Ref_Number_Key)
                exh:Ref_Number = xch:Ref_Number
                exh:Date = TODAY()
                SET(exh:Ref_Number_Key,exh:Ref_Number_Key)
                LOOP UNTIL Access:EXCHHIST.Next() <> Level:Benign
                    IF (exh:Ref_Number <> xch:Ref_Number)
                        BREAK
                    END ! IF
                    IF (exh:Status = 'UNIT RECEIVED')
                        dateReceived = exh:Date
                        BREAK
                    END ! IF
                END ! LOOP
					
                IF (dateReceived > 0)
                    expfil.AddField(FORMAT(dateReceived,@d06))
                    expfil.AddField(TODAY() - dateReceived,,1)
                ELSE
                    expfil.AddField()
                    expfil.AddField(,,1)
                END ! IF
            END
            recordCount += 1
					
            qSummary.Location = xch:Location
            qSummary.Manufacturer = xch:Manufacturer
            qSummary.ModelNumber = xch:Model_Number
            GET(qSummary,qSummary.Location,qSummary.Manufacturer,qSummary.ModelNumber)
            IF (ERROR())
                qSummary.Quantity = 1
                ADD(qSummary)
            ELSE
                qSummary.Quantity += 1
                PUT(qSummary)
            END ! IF
				
        END ! LOOP
BuildAvailableExchangeLoanStock		PROCEDURE()
countRecords	LONG()
fail			LONG(0)	
	MAP
Process		PROCEDURE(STRING pManName)
	END!  MAP
	CODE
		IF (p_web.GSV('locStockType') = '')
			CreateScript(packet,'alert("You must select a Stock Type.")')
			Do ReturnFail
		END ! IF
		
		Relate:SBO_GenericFile.Open()
		Relate:MANUFACT.Open()
		Relate:MODELNUM.Open()
        Relate:EXCHANGE.Open()
        Relate:LOAN.Open()
        Relate:TagFile.Open()
        Relate:SBO_GenericTagFile.Open()
		
		ClearSBOGenericFile(p_web)
		
		LOOP 1 TIMES
			countRecords = 0
			IF (p_web.GSV('locAllManufacturers') = 1)
                countRecords = RECORDS(MANUFACT)
                p_web.SSV('locManufacturer','Multiple')
            ELSE
                Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
                tagf:SessionID = p_web.SessionID
                tagf:TagType = 'M'
                SET(tagf:KeyTagged,tagf:KeyTagged)
                LOOP UNTIL Access:SBO_GenericTagFile.Next() <> Level:Benign
                    IF (tagf:SessionID <> p_web.SessionID OR |
                        tagf:TagType <> 'M')
                        BREAK
                    END ! IF
                    IF (tagf:Tagged = 0)
                        CYCLE
                    END ! I F
                    countRecords += 1
                    p_web.SSV('locManufacturer',tagf:TaggedValue)
				END ! LOOP
			
			END ! IF
			IF (countRecords = 0)
				CreateScript(packet,'alert("You must tag at least one Manufacturer.")')
				fail = 1
                BREAK
            ELSIF (countRecords > 1)
                p_web.SSV('locManufacturer','Multiple')
			END ! IF
			
			ProgressBarWeb.Init(countRecords)
			
			IF (p_web.GSV('locAllManufacturers') = 1)
				Access:MANUFACT.ClearKey(man:Manufacturer_Key)
				man:Manufacturer = ''
				SET(man:Manufacturer_Key,man:Manufacturer_Key)
				LOOP UNTIL Access:MANUFACT.Next() <> Level:Benign
					!IF (man:Notes[1:8] = 'INACTIVE')
                    IF (man:inactive = 1)
						CYCLE
					END ! IF
					
					Process(man:Manufacturer)
				END ! LOOP
            ELSE
                Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
                tagf:SessionID = p_web.SessionID
                tagf:TagType = 'M'
                SET(tagf:KeyTagged,tagf:KeyTagged)
                LOOP UNTIL Access:SBO_GenericTagFile.Next() <> Level:Benign
                    IF (tagf:SessionID <> p_web.SessionID OR |
                        tagf:TagType <> 'M')
                        BREAK
                    END ! IF
                    IF (tagf:Tagged = 0)
                        CYCLE
                    END ! I F
                    Process(tagf:TaggedValue)
              
                END ! LOOP                

			END ! IF			
			
		END ! LOOP
		
        Relate:SBO_GenericFile.Close()
        Relate:SBO_GenericTagFile.Close()
		Relate:EXCHANGE.Close()
		Relate:MODELNUM.Close()
        Relate:MANUFACT.Close()
        Relate:LOAN.Close()
		Relate:TagFile.Close()
		
		IF (fail)
			DO ReturnFail
		END ! IF
		
		
Process		PROCEDURE(STRING pManName)
	CODE
		ProgressBarWeb.Update('Building List..')
	
		Access:MODELNUM.ClearKey(mod:Manufacturer_Key)
		mod:Manufacturer = pManName
		SET(mod:Manufacturer_Key,mod:Manufacturer_Key)
		LOOP UNTIL Access:MODELNUM.Next() <> Level:Benign
            IF (mod:Manufacturer <> pManName)
                BREAK
            END ! IF
            
            CASE p_web.GSV('OrderType')
            OF 'E'
			
			Access:EXCHANGE.ClearKey(xch:LocStockAvailModelKey)
			xch:Available = 'AVL'
			xch:Location = p_web.GSV('BookingSiteLocation')
			xch:Stock_Type = p_web.GSV('locStockType')
			xch:Model_Number = mod:Model_Number
			SET(xch:LocStockAvailModelKey,xch:LocStockAvailModelKey)
                LOOP UNTIL Access:EXCHANGE.Next() <> Level:Benign
                    IF (xch:Available <> 'AVL' OR|
                        xch:Location <> p_web.GSV('BookingSiteLocation') OR |
                        xch:Stock_Type <> p_web.GSV('locStockType') OR |
                        xch:Model_Number <> mod:Model_Number)
                        BREAK
                    END ! IF
				
                    Access:SBO_GenericFile.ClearKey(sbogen:String1Key)
                    sbogen:SessionID = p_web.SessionID
                    sbogen:String1 = mod:Model_Number	
                    IF (Access:SBO_GenericFile.TryFetch(sbogen:String1Key) = Level:Benign)
                        sbogen:Long1 += 1
                        Access:SBO_GenericFile.TryUpdate()
                    ELSE ! IF
                        IF (Access:SBO_GenericFile.PrimeRecord() = Level:Benign)
                            sbogen:SessionID = p_web.SessionID
                            sbogen:String1 = mod:Model_Number
                            sbogen:String2 = mod:Manufacturer
                            sbogen:Long1 = 1
                            IF (Access:SBO_GenericFile.TryInsert())
                                Access:SBO_GenericFile.CancelAutoInc()
                            END ! IF
                        END ! IF
                    END ! IF
                END ! LOOP
            OF 'L'
                Access:LOAN.ClearKey(loa:LocStockAvailModelKey)
                loa:Available = 'AVL'
                loa:Location = p_web.GSV('BookingSiteLocation')
                loa:Stock_Type = p_web.GSV('locStockType')
                loa:Model_Number = mod:Model_Number
                SET(loa:LocStockAvailModelKey,loa:LocStockAvailModelKey)
                LOOP UNTIL Access:LOAN.Next() <> Level:Benign
                    IF (loa:Available <> 'AVL' OR|
                        loa:Location <> p_web.GSV('BookingSiteLocation') OR |
                        loa:Stock_Type <> p_web.GSV('locStockType') OR |
                        loa:Model_Number <> mod:Model_Number)
                        BREAK
                    END ! IF
				
                    Access:SBO_GenericFile.ClearKey(sbogen:String1Key)
                    sbogen:SessionID = p_web.SessionID
                    sbogen:String1 = mod:Model_Number	
                    IF (Access:SBO_GenericFile.TryFetch(sbogen:String1Key) = Level:Benign)
                        sbogen:Long1 += 1
                        Access:SBO_GenericFile.TryUpdate()
                    ELSE ! IF
                        IF (Access:SBO_GenericFile.PrimeRecord() = Level:Benign)
                            sbogen:SessionID = p_web.SessionID
                            sbogen:String1 = mod:Model_Number
                            sbogen:String2 = mod:Manufacturer
                            sbogen:Long1 = 1
                            IF (Access:SBO_GenericFile.TryInsert())
                                Access:SBO_GenericFile.CancelAutoInc()
                            END ! IF
                        END ! IF
                    END ! IF
                END ! LOOP
            END ! CASE
		END ! LOOP
! BuildJobProgressList
BuildJobsProgressList        PROCEDURE()
qJobsList                       QUEUE(),PRE(qJobsList)
RecordNumber                        LONG()
JobNumber                           LONG()
                                END !
i                                   LONG() 
recs                                LONG()
keepAlive                           LONG()
    CODE
        
        IF (p_web.GSV('BookingAccount') = '')
            packet = CLIP(packet) & '<script>alert("An error has occurred\n\nPlease log back in.");</script>'
            Do Redirect
        END ! IF
        
        Relate:WEBJOB.Open()
        Relate:JOBS.Open()
        Relate:SBO_GenericFile.Open()
        Relate:EXCHANGE.Open()
        Relate:JOBTHIRD.Open()
        
        STREAM(SBO_GenericFile)
        STREAM(WEBJOB)
        STREAM(JOBS)
        STREAM(EXCHANGE)
        STREAM(JOBTHIRD)
        
        
        !region CountRecords
        recs = 0
        
        CASE p_web.GSV('searchtype')
        OF 'jobs'
            p_web.SSV('GlobalIMEINumber','')
            p_web.SSV('GlobalMobileNumber','')
            p_web.SSV('GlobalJobNumber','')
            p_web.SSV('BrowseJobsTitle','Browse Jobs Booked Within Date Range')
            
            Access:WEBJOB.ClearKey(wob:DateBookedKey)
            wob:HeadAccountNumber = p_web.GSV('BookingAccount')
            wob:DateBooked = p_web.GSV('locJobProgressStartDate')
            SET(wob:DateBookedKey,wob:DateBookedKey)
            LOOP UNTIL Access:WEBJOB.Next() <> Level:Benign
                IF (wob:HeadAccountNumber <> p_web.GSV('BookingAccount') OR |
                    wob:DateBooked > p_web.GSV('locJobProgressEndDate'))
                    BREAK
                END ! IF
                recs += 1
                keepAlive += 1; IF keepAlive > 50 THEN p_web.Noop().;keepAlive = 0
                
            END ! IF
        OF 'imei'
            p_web.SSV('BrowseJobsTitle','Browse Jobs Matching IMEI No ' & p_web.GSV('GlobalIMEINumber'))
            !p_web.SSV('GlobalIMEINumber','')
            p_web.SSV('GlobalMobileNumber','')
            p_web.SSV('GlobalJobNumber','')
            Access:JOBS.ClearKey(job:ESN_Key)
            job:ESN = p_web.GSV('GlobalIMEINumber')
            SET(job:ESN_Key,job:ESN_Key)
            LOOP UNTIL Access:JOBS.TryNext() <> Level:Benign
                IF (job:ESN <> p_web.GSV('GlobalIMEINumber'))
                    BREAK
                END ! IF
                
                recs += 1
                keepAlive += 1; IF keepAlive > 50 THEN p_web.Noop().;keepAlive = 0
            END ! LOOP
            
            Access:JOBTHIRD.ClearKey(jot:OriginalIMEIKey)
            jot:OriginalIMEI = p_web.GSV('GlobalIMEINumber')
            SET(jot:OriginalIMEIKey,jot:OriginalIMEIKey)
            LOOP UNTIL Access:JOBTHIRD.TryNext() <> Level:Benign
                IF (jot:OriginalIMEI <> p_web.GSV('GlobalIMEINumber'))
                    BREAK
                END ! IF
                
                recs += 1
                keepAlive += 1; IF keepAlive > 50 THEN p_web.Noop().;keepAlive = 0
            END ! LOOP
            
            Access:EXCHANGE.ClearKey(xch:ESN_Only_Key)
            xch:ESN = p_web.GSV('GlobalIMEINumber')
            SET(xch:ESN_Only_Key,xch:ESN_Only_Key)
            LOOP UNTIL Access:EXCHANGE.TryNext() <> Level:Benign
                IF (xch:ESN <> p_web.GSV('GlobalIMEINumber'))
                    BREAK
                END ! IF
                
                recs += 1
                keepAlive += 1; IF keepAlive > 50 THEN p_web.Noop().;keepAlive = 0
            END ! LOOP
            
        OF 'mobileno'
            p_web.SSV('BrowseJobsTitle','Browse Jobs Matching Mobile No ' & p_web.GSV('GlobalMobileNumber'))
            p_web.SSV('GlobalIMEINumber','')
            !p_web.SSV('GlobalMobileNumber','')
            p_web.SSV('GlobalJobNumber','')
            Access:WEBJOB.ClearKey(wob:HeadMobileNumberKey)
            wob:HeadAccountNumber = p_web.GSV('BookingAccount')
            wob:MobileNumber = p_web.GSV('GlobalMobileNumber')
            SET(wob:HeadMobileNumberKey,wob:HeadMobileNumberKey)
            LOOP UNTIL Access:WEBJOB.Next() <> Level:Benign
                IF (wob:HeadAccountNumber <> p_web.GSV('BookingAccount') OR |
                    wob:MobileNumber <> p_web.GSV('GlobalMobileNumber'))
                    BREAK
                END ! IF
                recs += 1
            
            END ! LOOP
        OF 'jobno'
            p_web.SSV('GlobalIMEINumber','')
            p_web.SSV('GlobalMobileNumber','')
            !p_web.SSV('GlobalJobNumber','')
            p_web.SSV('BrowseJobsTitle','Matching Job')
        END ! CASE
        
        !endregion
        
        ProgressBarWeb.Init(recs,250)
        
        FREE(qJobsList)
        CLEAR(qJobsList)
        
        CASE p_web.GSV('searchtype')
        OF 'jobs'
            Access:WEBJOB.ClearKey(wob:DateBookedKey)
            wob:HeadAccountNumber = p_web.GSV('BookingAccount')
            wob:DateBooked = p_web.GSV('locJobProgressStartDate')
            SET(wob:DateBookedKey,wob:DateBookedKey)
            LOOP UNTIL Access:WEBJOB.Next() <> Level:Benign
                IF (wob:HeadAccountNumber <> p_web.GSV('BookingAccount') OR |
                    wob:DateBooked > p_web.GSV('locJobProgressEndDate'))
                    BREAK
                END ! I
                            
                ProgressBarWeb.Update('Finding Jobs That Match Criteria...')
            
                IF (wob:RefNumber = 0)
                    CYCLE
                END ! IF
            
                Access:JOBS.ClearKey(job:Ref_Number_Key)
                job:Ref_Number = wob:RefNumber
                IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
                    CYCLE
                END ! IF
                
                DO AddToExport
            END ! LOOP
        OF 'imei'
            Access:JOBS.ClearKey(job:ESN_Key)
            job:ESN = p_web.GSV('GlobalIMEINumber')
            SET(job:ESN_Key,job:ESN_Key)
            LOOP UNTIL Access:JOBS.TryNext() <> Level:Benign
                IF (job:ESN <> p_web.GSV('GlobalIMEINumber'))
                    BREAK
                END ! IF
                ProgressBarWeb.Update('Finding Jobs That Match Criteria...')
                
                Access:WEBJOB.ClearKey(wob:RefNumberKey)
                wob:RefNumber = job:Ref_Number
                IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
                    CYCLE
                END ! IF
                
                DO AddToExport
                
                
            END ! LOOP
            
            Access:JOBTHIRD.ClearKey(jot:OriginalIMEIKey)
            jot:OriginalIMEI = p_web.GSV('GlobalIMEINumber')
            SET(jot:OriginalIMEIKey,jot:OriginalIMEIKey)
            LOOP UNTIL Access:JOBTHIRD.TryNext() <> Level:Benign
                IF (jot:OriginalIMEI <> p_web.GSV('GlobalIMEINumber'))
                    BREAK
                END ! IF
                ProgressBarWeb.Update('Finding Jobs That Match Criteria...')
                
                Access:WEBJOB.ClearKey(wob:RefNumberKey)
                wob:RefNumber = jot:RefNumber
                IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
                    CYCLE
                END ! IF
                DO AddToExport
            END ! LOOP
            
            Access:EXCHANGE.ClearKey(xch:ESN_Only_Key)
            xch:ESN = p_web.GSV('GlobalIMEINumber')
            SET(xch:ESN_Only_Key,xch:ESN_Only_Key)
            LOOP UNTIL Access:EXCHANGE.TryNext() <> Level:Benign
                IF (xch:ESN <> p_web.GSV('GlobalIMEINumber'))
                    BREAK
                END ! IF
                ProgressBarWeb.Update('Finding Jobs That Match Criteria...')
                
                IF (xch:Job_Number = 0)
                    CYCLE
                END ! IF
                
                Access:WEBJOB.ClearKey(wob:RefNumberKey)
                wob:RefNumber = xch:Job_Number
                IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
                    CYCLE
                END ! IF
                DO AddToExport
            END ! LOOP
            
        OF 'mobileno'
            Access:WEBJOB.ClearKey(wob:HeadMobileNumberKey)
            wob:HeadAccountNumber = p_web.GSV('BookingAccount')
            wob:MobileNumber = p_web.GSV('GlobalMobileNumber')
            SET(wob:HeadMobileNumberKey,wob:HeadMobileNumberKey)
            LOOP UNTIL Access:WEBJOB.Next() <> Level:Benign
                IF (wob:HeadAccountNumber <> p_web.GSV('BookingAccount') OR |
                    wob:MobileNumber <> p_web.GSV('GlobalMobileNumber'))
                    BREAK
                END ! IF
                            
                ProgressBarWeb.Update('Finding Jobs That Match Criteria...')
            
                IF (wob:RefNumber = 0)
                    CYCLE
                END ! IF
            
                Access:JOBS.ClearKey(job:Ref_Number_Key)
                job:Ref_Number = wob:RefNumber
                IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
                    CYCLE
                END ! IF
                
                DO AddToExport
            END ! LOOP
        OF 'jobno'
            Access:WEBJOB.ClearKey(wob:HeadRefNumberKey)
            wob:HeadAccountNumber = p_web.GSV('BookingAccount')
            wob:RefNumber = p_web.GSV('GlobalJobNumber')
            IF (Access:WEBJOB.TryFetch(wob:HeadRefNumberKey) = Level:Benign)
                
                DO AddToExport
            END ! IF
        END ! CASE

        
        ! Clear any records that don't match
        ProgressBarWeb.Init(RECORDS(SBO_GenericFile))      
        
        Access:SBO_GenericFile.ClearKey(sbogen:Long1Key)
        sbogen:SessionID = p_web.SessionID
        SET(sbogen:Long1Key,sbogen:Long1Key)
        LOOP UNTIL Access:SBO_GenericFile.Next() <> Level:Benign
            IF (sbogen:SessionID <> p_web.SessionID)
                BREAK
            END ! IF
            ProgressBarWeb.Update('Clearing Jobs List...')
            
            qJobsList.RecordNumber = sbogen:Long2
            GET(qJobsList,qJobsList.RecordNumber)
            IF (ERROR())
                Access:SBO_GenericFile.DeleteRecord(0)
            END ! IF
        END ! LOOP
        
        
        FLUSH(SBO_GenericFile)
        FLUSH(WEBJOB)
        FLUSH(JOBS)
        FLUSH(EXCHANGE)
        FLUSH(JOBTHIRD)
        
        Relate:SBO_GenericFile.Close()
        Relate:WEBJOB.Close()
        Relate:JOBS.Close()
        Relate:EXCHANGE.Close()
        Relate:JOBTHIRD.Close()

        p_web.SSV('TotalAvailableJobs',RECORDS(qJobsList))
        
        IF (RECORDS(qJobsList) = 0)
            CreateScript(packet,kNoREcordsAlert)
        END ! IF
        Do Redirect        
        
AddToExport         ROUTINE
    qJobsList.RecordNumber = wob:RecordNumber
    qJobsList.JobNumber = wob:RefNumber
    GET(qJobsList,qJobsList.RecordNumber)
    IF (NOT ERRORCODE())
        EXIT
    END ! IF
    
    ADD(qJobsList)
            
    Access:SBO_GenericFile.ClearKey(sbogen:Long2Key)
    sbogen:SessionID = p_web.SessionID
    sbogen:Long2 = wob:RecordNumber
    IF (Access:SBO_GenericFile.TryFetch(sbogen:Long2Key) = Level:Benign)
                
    ELSE ! IF
        IF (Access:SBO_GenericFile.PrimeRecord() = Level:Benign)
            sbogen:SessionID = p_web.SessionID
            sbogen:Long1 = wob:RefNumber
            sbogen:Long2 = wob:RecordNumber
            IF (Access:SBO_GenericFile.TryInsert())
                Access:SBO_GenericFile.CancelAutoInc()
            END ! IF
        END ! IF
    END ! IF
!BuildStockReceiveList
BuildStockReceiveList       PROCEDURE()!
addNewPart LONG(0)
err LONG(0)
    CODE
        p_web.SSV('InvoiceFound','')
        
        IF (p_web.GSV('locInvoiceNumber') = 0)
            DO ReturnFail
        END ! IF
        
        ClearStockReceiveList(p_web)
        
        Relate:RETSALES.Open()
        Relate:RETSTOCK.Open()
        Relate:STOCKRECEIVETMP.Open()
        
        STREAM(STOCKRECEIVETMP)
        
        LOOP 1 TIMES
            Access:RETSALES.ClearKey(ret:Invoice_Number_Key)
            ret:Invoice_Number = p_web.GSV('locInvoiceNumber')
            IF (Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign)
                IF (ret:Account_Number <> p_web.GSV('BookingStoresAccount'))
                    CreateScript(packet,'alert("The selected invoice is not for you site")')
                    err = 1
                    BREAK
                ELSE ! IF
                    p_web.SSV('InvoiceFound',1)
                END ! IF
            ELSE ! IF
                CreateScript(packet,'alert("Unable to find the selected Invoice Number")')
                err = 1
                BREAK
            END ! IF
            
            ProgressBarWeb.Init(50,5)
            
            Access:RETSTOCK.ClearKey(res:Despatched_Key)
            res:Ref_Number = ret:Ref_Number
            res:Despatched = 'YES'
            SET(res:Despatched_Key,res:Despatched_Key)
            LOOP UNTIL Access:RETSTOCK.Next() <> Level:Benign
                IF (res:Ref_Number <> ret:Ref_Number OR |
                    res:Despatched <> 'YES')
                    BREAK
                END ! IF
                
                ProgressBarWeb.Update('Please Wait...')
                
                addNewPart = 1
                IF (ret:ExchangeOrder = 1 OR ret:LoanOrder = 1)
                    ! #12127 Group Parts If Exchange Order. Keep Received Seperate (Bryan: 05/07/2011)
                    Access:STOCKRECEIVETMP.Clearkey(stotmp:ReceivedPartNumberKey)
                    stotmp:SessionID = p_web.SessionID
                    stotmp:Received = res:Received
                    stotmp:PartNumber = res:Part_Number
                    IF (Access:STOCKRECEIVETMP.TryFetch(stotmp:ReceivedPartNumberKey) = Level:Benign)
                        stotmp:Quantity += 1
                        stotmp:QuantityReceived += res:QuantityReceived
                        Access:STOCKRECEIVETMP.TryUpdate()
                        addNewPart = 0
                    END                
                END ! IF
                
                IF (addNewPart = 1)
                    IF (Access:STOCKRECEIVETMP.PrimeRecord() = Level:Benign)
                        stotmp:SessionID = p_web.SessionID
                        stotmp:PartNumber = res:Part_Number
                        stotmp:Description = res:Description
                        stotmp:ItemCost = res:Item_Cost
                        stotmp:Quantity = res:Quantity
                        stotmp:QuantityReceived = res:QuantityReceived
                        stotmp:RESRecordNumber = res:Record_Number
                        stotmp:Received = res:Received
                        stotmp:ExchangeOrder = ret:ExchangeOrder
                        stotmp:LoanOrder = ret:LoanOrder
                        !IF (Access:STOCKRECEIVETMP.TryInsert())
                        if Access:STOCKRECEIVETMP.TryUpdate()
                            Access:STOCKRECEIVETMP.CancelAutoInc()
                        END ! IF
                    END ! IF
                END ! IF        
            END ! LOOP
        END ! LOOP
        
        FLUSH(STOCKRECEIVETMP)
        Relate:RETSTOCK.Close()
        Relate:STOCKRECEIVETMP.Close()
        Relate:RETSALES.Close()
        
        IF (err = 1)
            DO ReturnFail
        END ! IF
! BuildWarrantyClaimsBrowse
BuildWarrantyClaimsBrowse   PROCEDURE()
jobCount                        LONG()
recs LONG()
    CODE
        ! Save the tick box selection
        IF (p_web.GSV('locSelectAll') = 1)
            p_web.SSV('locPendingSelected',1)
            p_web.SSV('locApprovedSelected',1)
            p_web.SSV('locRejectedSelected',1)
            p_web.SSV('locAcceptedRejectedSelected',1)
            p_web.SSV('locFinalRejectionSelected',1)
            p_web.SSV('locPaidSelected',1)
        ELSE
            p_web.SSV('locPendingSelected',p_web.GSV('locPending'))
            p_web.SSV('locApprovedSelected',p_web.GSV('locApproved'))
            p_web.SSV('locRejectedSelected',p_web.GSV('locRejected'))
            p_web.SSV('locAcceptedRejectedSelected',p_web.GSV('locAcceptedRejected'))
            p_web.SSV('locFinalRejectionSelected',p_web.GSV('locFinalRejection'))
            p_web.SSV('locPaidSelected',p_web.GSV('locPaid'))
        END ! IF
        
        p_web.SSV('locPendingCount',0)
        p_web.SSV('locApprovedCount',0)
        p_web.SSV('locRejectedCount',0)
        p_web.SSV('locAcceptedRejectedCount',0)
        p_web.SSV('locFinalRejectionCount',0)
        p_web.SSV('locPaidCount',0)
        
        ClearSBOWarrantyClaims(p_web)
        
        Relate:JOBSWARR.Open()
        Relate:SBO_WarrantyClaims.Open()
        Relate:JOBS.Open()
        STREAM(SBO_WarrantyClaims)
        
        !region Quick Record Count
        recs = 0

        IF (p_web.GSV('locSelectAll') = 1 OR p_web.GSV('locPending') = 1)
            Access:JOBSWARR.ClearKey(jow:SubmittedRepairedBranchKey)
            jow:BranchID = p_web.GSV('BookingBranchID')
            jow:RepairedAt = 'RRC'
            jow:ClaimSubmitted = p_web.GSV('locWarrantyStartDate')
            SET(jow:SubmittedRepairedBranchKey,jow:SubmittedRepairedBranchKey)
            LOOP UNTIL Access:JOBSWARR.Next() <> Level:Benign
                IF (jow:BranchID <> p_web.GSV('BookingBranchID') OR |
                    jow:RepairedAt <> 'RRC' OR |
                    jow:ClaimSubmitted > p_web.GSV('locWarrantyEndDate'))
                    BREAK
                END ! IF
        
                recs += 1        
            END ! LOOP
        END ! IF

        p_web.Noop()
! Approved (APP)
        IF (p_web.GSV('locSelectAll') = 1 OR p_web.GSV('locApproved') = 1)
            Access:JOBSWARR.ClearKey(jow:AcceptedBranchKey)
            jow:BranchID = p_web.GSV('BookingBranchID')
            jow:DateAccepted = p_web.GSV('locWarrantyStartDate')
            SET(jow:AcceptedBranchKey,jow:AcceptedBranchKey)
            LOOP UNTIL Access:JOBSWARR.Next() <> Level:Benign
                IF (jow:BranchID <> p_web.GSV('BookingBranchID') OR |
                    jow:DateAccepted > p_web.GSV('locWarrantyEndDate'))
                    BREAK
                END ! IF
        
                recs += 1
            END ! LOOP
        END ! IF
        p_web.Noop()
        ! Rejected (EXC)
        IF (p_web.GSV('locSelectAll') = 1 OR p_web.GSV('locRejected') = 1)
            Access:JOBSWARR.ClearKey(jow:RejectedBranchKey)
            jow:BranchID = p_web.GSV('BookingBranchID')
            jow:DateRejected = p_web.GSV('locWarrantyStartDate')
            SET(jow:RejectedBranchKey,jow:RejectedBranchKey)
            LOOP UNTIL Access:JOBSWARR.Next() <> Level:Benign
                IF (jow:BranchID <> p_web.GSV('BookingBranchID') OR |
                    jow:DateRejected > p_web.GSV('locWarrantyEndDate'))
                    BREAK
                END ! IF
                
                recs += 1
            END ! LOOP
        END ! IF
        p_web.Noop()
        ! Accepted Rejected / Final Rejection (AAJ / REJ)
        IF (p_web.GSV('locSelectAll') = 1 OR p_web.GSV('locAcceptedRejected') = 1 OR p_web.GSV('locFinalRejection') = 1)
            Access:JOBSWARR.ClearKey(jow:FinalRejectionBranchKey)
            jow:BranchID = p_web.GSV('BookingBranchID')
            jow:DateFinalRejection = p_web.GSV('locWarrantyStartDate')
            SET(jow:FinalRejectionBranchKey,jow:FinalRejectionBranchKey)
            LOOP UNTIL Access:JOBSWARR.Next() <> Level:Benign
                IF (jow:BranchID <> p_web.GSV('BookingBranchID') OR |
                    jow:DateFinalRejection > p_web.GSV('locWarrantyEndDate'))
                    BREAK
                END ! IF
                
                recs += 1
            END ! LOOP
        END ! IF        
        p_web.Noop()
                ! Paid (PAY)
        IF (p_web.GSV('locSelectAll') = 1 OR p_web.GSV('locPaid') = 1)
            Access:JOBSWARR.ClearKey(jow:RRCReconciledKey)
            jow:BranchID = p_web.GSV('BookingBranchID')
            jow:RepairedAT = 'RRC'
            jow:RRCStatus = 'PAY'
            jow:RRCDateReconciled = p_web.GSV('locWarrantyStartDate')
            SET(jow:RRCReconciledKey,jow:RRCReconciledKey)
            LOOP UNTIL Access:JOBSWARR.Next() <> Level:Benign
                IF (jow:BranchID <> p_web.GSV('BookingBranchID') OR |
                    jow:RepairedAT <> 'RRC' OR |
                    jow:RRCStatus <> 'PAY' OR |
                    jow:RRCDateReconciled > p_web.GSV('locWarrantyEndDate'))
                    BREAK
                END ! IF
                recs += 1
            END ! LOOP
        END ! IF
! endregion
        
        
        ProgressBarWeb.Init(recs)

        ! Pending (NO)
        IF (p_web.GSV('locSelectAll') = 1 OR p_web.GSV('locPending') = 1)
            Access:JOBSWARR.ClearKey(jow:SubmittedRepairedBranchKey)
            jow:BranchID = p_web.GSV('BookingBranchID')
            jow:RepairedAt = 'RRC'
            jow:ClaimSubmitted = p_web.GSV('locWarrantyStartDate')
            SET(jow:SubmittedRepairedBranchKey,jow:SubmittedRepairedBranchKey)
            LOOP UNTIL Access:JOBSWARR.Next() <> Level:Benign
                IF (jow:BranchID <> p_web.GSV('BookingBranchID') OR |
                    jow:RepairedAt <> 'RRC' OR |
                    jow:ClaimSubmitted > p_web.GSV('locWarrantyEndDate'))
                    BREAK
                END ! IF
                
                ProgressBarWeb.Update('Building List...')
                
                IF (jow:RRCStatus <> 'NO')
                    CYCLE
                END ! IF
                
                
                DO AddToList
            END ! LOOP
        END ! IF
        
        ! Approved (APP)
        IF (p_web.GSV('locSelectAll') = 1 OR p_web.GSV('locApproved') = 1)
            Access:JOBSWARR.ClearKey(jow:AcceptedBranchKey)
            jow:BranchID = p_web.GSV('BookingBranchID')
            jow:DateAccepted = p_web.GSV('locWarrantyStartDate')
            SET(jow:AcceptedBranchKey,jow:AcceptedBranchKey)
            LOOP UNTIL Access:JOBSWARR.Next() <> Level:Benign
                IF (jow:BranchID <> p_web.GSV('BookingBranchID') OR |
                    jow:DateAccepted > p_web.GSV('locWarrantyEndDate'))
                    BREAK
                END ! IF
                
                ProgressBarWeb.Update('Building List...')
                
                IF (jow:RepairedAt <> 'RRC')
                    CYCLE
                END ! IF
                IF (jow:RRCStatus <> 'APP')
                    CYCLE
                END ! IF

                
                DO AddToList
            END ! LOOP
        END ! IF
        
        ! Rejected (EXC)
        IF (p_web.GSV('locSelectAll') = 1 OR p_web.GSV('locRejected') = 1)
            Access:JOBSWARR.ClearKey(jow:RejectedBranchKey)
            jow:BranchID = p_web.GSV('BookingBranchID')
            jow:DateRejected = p_web.GSV('locWarrantyStartDate')
            SET(jow:RejectedBranchKey,jow:RejectedBranchKey)
            LOOP UNTIL Access:JOBSWARR.Next() <> Level:Benign
                IF (jow:BranchID <> p_web.GSV('BookingBranchID') OR |
                    jow:DateRejected > p_web.GSV('locWarrantyEndDate'))
                    BREAK
                END ! IF
                
                ProgressBarWeb.Update('Building List...')
                
                IF (jow:RepairedAt <> 'RRC')
                    CYCLE
                END ! IF
                IF (jow:RRCStatus <> 'EXC')
                    CYCLE
                END ! IF

                
                DO AddToList
            END ! LOOP
        END ! IF

        ! Accepted Rejected / Final Rejection (AAJ / REJ)
        IF (p_web.GSV('locSelectAll') = 1 OR p_web.GSV('locAcceptedRejected') = 1 OR p_web.GSV('locFinalRejection') = 1)
            Access:JOBSWARR.ClearKey(jow:FinalRejectionBranchKey)
            jow:BranchID = p_web.GSV('BookingBranchID')
            jow:DateFinalRejection = p_web.GSV('locWarrantyStartDate')
            SET(jow:FinalRejectionBranchKey,jow:FinalRejectionBranchKey)
            LOOP UNTIL Access:JOBSWARR.Next() <> Level:Benign
                IF (jow:BranchID <> p_web.GSV('BookingBranchID') OR |
                    jow:DateFinalRejection > p_web.GSV('locWarrantyEndDate'))
                    BREAK
                END ! IF
                
                ProgressBarWeb.Update('Building List...')
                
                IF (jow:RepairedAt <> 'RRC')
                    CYCLE
                END ! IF
                IF (p_web.GSV('locSelectAll') = 1)
                    IF (jow:RRCStatus <> 'AAJ' AND jow:RRCStatus <> 'REJ')
                        CYCLE
                    END ! IF
                ELSE
                    IF (p_web.GSV('locAcceptedRejected') = 1)
                        IF (jow:RRCStatus <> 'AAJ')
                            CYCLE
                        END ! IF
                    ELSIF (p_web.GSV('locFinalRejection') = 1)
                        IF (jow:RRCStatus <> 'REJ')
                            CYCLE
                        END ! IF
                    
                    END ! IF
                END ! IF
                

                
                DO AddToList
            END ! LOOP
        END ! IF

        ! Paid (PAY)
        IF (p_web.GSV('locSelectAll') = 1 OR p_web.GSV('locPaid') = 1)
            Access:JOBSWARR.ClearKey(jow:RRCReconciledKey)
            jow:BranchID = p_web.GSV('BookingBranchID')
            jow:RepairedAT = 'RRC'
            jow:RRCStatus = 'PAY'
            jow:RRCDateReconciled = p_web.GSV('locWarrantyStartDate')
            SET(jow:RRCReconciledKey,jow:RRCReconciledKey)
            LOOP UNTIL Access:JOBSWARR.Next() <> Level:Benign
                IF (jow:BranchID <> p_web.GSV('BookingBranchID') OR |
                    jow:RepairedAT <> 'RRC' OR |
                    jow:RRCStatus <> 'PAY' OR |
                    jow:RRCDateReconciled > p_web.GSV('locWarrantyEndDate'))
                    BREAK
                END ! IF
                ProgressBarWeb.Update('Building List...')
                DO AddToList
            END ! LOOP
        END ! IF

        FLUSH(SBO_WarrantyClaims)
        Relate:JOBSWARR.Close()
        Relate:SBO_WarrantyClaims.Close()
        Relate:JOBS.Close()
        
        p_web.SSV('locTotalAvailableJobs',jobCount)
        
        IF (jobCount = 0)
            CreateScript(packet,kNoREcordsAlert)
            DO ReturnFail
            RETURN
        END ! IF
        
        ! Removed the total counts from the radio buttons
        ! As it may be a pain to keep them in sync
        p_web.SSV('locPendingCount','Pending')! (' & p_web.GSV('locPendingCount') & ')')
        p_web.SSV('locApprovedCount','Approved')! (' & p_web.GSV('locApprovedCount') & ')')
        p_web.SSV('locRejectedCount','Rejected')! (' & p_web.GSV('locRejectedCount') & ')')
        p_web.SSV('locAcceptedRejectedCount','Accepted Rejected')! (' & p_web.GSV('locAcceptedRejectedCount') & ')')
        p_web.SSV('locFinalRejectionCount','Final Rejection')! (' & p_web.GSV('locFinalRejectionCount') & ')')
        p_web.SSV('locPaidCount','Paid')! (' & p_web.GSV('locPaidCount') & ')')


AddToList           ROUTINE
    
    Access:JOBS.ClearKey(job:Ref_Number_Key)
    job:Ref_Number = jow:RefNumber
    IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
        EXIT
    END ! IF
    

    Access:SBO_WarrantyClaims.ClearKey(sbojow:JobNumberKey)
    sbojow:SessionID = p_web.SessionID
    sbojow:JobNumber = jow:RefNumber
    IF (Access:SBO_WarrantyClaims.TryFetch(sbojow:JobNumberKey) = Level:Benign)
    
    ELSE ! IF
        IF (Access:SBO_WarrantyClaims.PrimeRecord() = Level:Benign)
            sbojow:SessionID    = p_web.SessionID
            sbojow:JobNumber    = jow:RefNumber
            sbojow:RRCStatus    = jow:RRCStatus
            sbojow:Manufacturer = jow:Manufacturer
            sbojow:DateReconciled = jow:RRCDateReconciled
            IF (Access:SBO_WarrantyClaims.TryInsert())
                Access:SBO_WarrantyClaims.CancelAutoInc()
            ELSE
                CASE jow:RRCStatus
                OF 'NO'
                    p_web.SSV('locPendingCount',p_web.GSV('locPendingCount') + 1)
                OF 'APP'
                    p_web.SSV('locApprovedCount',p_web.GSV('locApprovedCount') + 1)
                OF 'EXC'
                    p_web.SSV('locRejectedCount',p_web.GSV('locRejectedCount') + 1)
                OF 'AAJ'
                    p_web.SSV('locAcceptedRejectedCount',p_web.GSV('locAcceptedRejectedCount') + 1)            
                OF 'REJ'
                    p_web.SSV('locFinalRejectionCount',p_web.GSV('locFinalRejectionCount') + 1)
                OF 'PAY'
                    p_web.SSV('locPaidCount',p_web.GSV('locPaidCount') + 1)
                END ! END
            END ! IF
        END ! IF
    END ! IF
    jobCount += 1
!region ChargeableIncomeReportEngine
ChargeableIncomeReportEngine        PROCEDURE()!
	MAP
qARCCharge PROCEDURE(*DECIMAL pField)!
qARCMarkup PROCEDURE(REAL pPartsSelling,*DECIMAL pField)!
qCashCreditUser PROCEDURE(REAL pTotal,*DECIMAL pCash,*DECIMAL pCredit,*STRING pUser,*DECIMAL pPaid,*DECIMAL pOutstanding)!
qExchanged PROCEDURE(*STRING pField)!
qHandlingFee PROCEDURE(*DECIMAL pField)!
qLabour PROCEDURE(*DECIMAL pField)!
qLoan PROCEDURE(*STRING pField)!
qLoanLoanCharge PROCEDURE(*DECIMAL pField)!
qPartsCost PROCEDURE(*DECIMAL pField)!
qPartsSelling PROCEDURE(*DECIMAL pField)!
qRepair PROCEDURE(*STRING pField)!
qTotal PROCEDURE(REAL pVAT,*DECIMAL pField)!
qVAT PROCEDURE(*DECIMAL pField)!
qWaybillNumber  PROCEDURE(*STRING pName,*STRING pNumber,*STRING pWaybill)
RunProcess PROCEDURE(),LONG
ValidateAccountNumber   PROCEDURE(STRING pAccountNumber),LONG
ValidateChargeTypes	 PROCEDURE(STRING pChargeType),LONG
    END ! MAP
i                                   LONG
sentToARC                           LONG
reportStyle                         STRING(1)

recordCount                         LONG()
recs                                LONG()
    CODE
        ProgressBarWeb.Init(500)
        
		! Clear Q
        reportStyle = p_web.GSV('locReportStyle')
		
        recs = RECORDS(qIncomeReport)
        
		LOOP i = recs TO 1 BY -1
			GET(qIncomeReport,i)
            IF (qIncomeReport.SessionID = p_web.SessionID)
                p_web.Noop()
				DELETE(qIncomeReport)
			END ! IF
		END ! LOOP
	
		Relate:INVOICE.Open()
		Relate:JOBS.Open()
		Relate:WEBJOB.Open()
		Relate:JOBSE.Open()
        Relate:TagFile.Open()
        Relate:PARTS.Open()
        Relate:JOBPAYMT.Open()
        Relate:SUBTRACC_ALIAS.Open()
        Relate:TRADEACC_ALIAS.Open()
        Relate:TRADEACC.Open()
        Relate:SUBTRACC.Open()
        Relate:SBO_GenericTagFile.Open()
        
        ProgressBarWeb.Init(1000)
	
		CASE p_web.GSV('locReportStyle')
		OF 'I'
			Access:INVOICE.ClearKey(inv:RRCInvoiceDateKey)
			inv:RRCInvoiceDate = p_web.GSV('locStartDate')
			SET(inv:RRCInvoiceDateKey,inv:RRCInvoiceDateKey)
			LOOP UNTIL Access:INVOICE.Next() <> Level:Benign
                IF (inv:RRCInvoiceDate > p_web.GSV('locEndDate'))
                    BREAK
                END ! IF
                
                ProgressBarWeb.Update('Building Jobs List...')
				
				IF NOT (inv:ExportedRRCOracle)
					CYCLE
				END ! IF
				
				IF (ValidateAccountNumber(inv:Account_Number))
					CYCLE
				END ! IF
				
				Access:JOBS.ClearKey(job:InvoiceNumberKey)
				job:Invoice_Number = inv:Invoice_Number
                IF (Access:JOBS.TryFetch(job:InvoiceNumberKey) = Level:Benign)
					
                ELSE ! IF
                    CYCLE
                END ! IF
                
                !TB13343 - J - 04/08/14 - don't show the invoice if the job is no longer chargeable
                if job:Chargeable_Job <> 'YES' then cycle.
                !END TB13343
                
                Access:WEBJOB.ClearKey(wob:RefNumberKey)
                wob:RefNumber = job:Ref_Number
                IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
            
                ELSE ! IF
                END ! IF      
                CLEAR(qIncomeReport)
                
                Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                tra:Account_Number  = wob:HeadAccountNumber
                If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    !Found
                    Case p_web.GSV('locReportOrder')
                    Of 0
                        qIncomeReport.InvoiceNumber = Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification)
                        qIncomeReport.InvoiceDate   = Format(inv:RRCInvoiceDate,@d06)
                        qIncomeReport.JobNumber     = Clip(job:Ref_Number) & '-' & Clip(tra:BranchIdentification) & Clip(wob:JobNumber)
                    Of 1
                        qIncomeReport.InvoiceNumber     = Clip(job:Ref_Number)! & '-' & Clip(tra:BranchIdentification) & Clip(wob:JobNumber)
                        qIncomeReport.InvoiceDate = Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification)
                        qIncomeReport.JobNumber   = Format(inv:RRCInvoiceDate,@d06)
                    End !Case tmp:ReportOrder
                Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    !Error
                End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign                
                
                qIncomeReport.SessionID = p_web.SessionID
                qIncomeReport.RefNumber = job:Ref_Number
				
                IF (RunProcess())
                    CYCLE
                END ! IF
                
                ADD(qIncomeReport)
                recordCount += 1
			END ! LOOP
		OF 'C'
			Access:JOBS.ClearKey(job:DateCompletedKey)
			job:Date_Completed = p_web.GSV('locStartDate')
			SET(job:DateCompletedKey,job:DateCompletedKey)
			LOOP UNTIL Access:JOBS.Next() <> Level:Benign
                IF (job:Date_Completed > p_web.GSV('locEndDate'))
                    BREAK
                END ! IF
                
                ProgressBarWeb.Update('Building Jobs List...')
				
				IF (job:Invoice_Number > 0)
					CYCLE
				END ! IF
				
				IF (job:Chargeable_Job <> 'YES')
					CYCLE
				END ! IF
				
                IF (ValidateAccountNumber(job:Account_Number))
                    CYCLE
                END ! IF		
                
                Access:WEBJOB.ClearKey(wob:RefNumberKey)
                wob:RefNumber = job:Ref_Number
                IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
            
                END ! IF      
                
                CLEAR(qIncomeReport)
                qIncomeReport.SessionID = p_web.SessionID
                qIncomeReport.RefNumber = job:Ref_Number
                
                Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                tra:Account_Number  = wob:HeadAccountNumber
                If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    !Found
                    qIncomeReport.InvoiceNumber = Clip(job:Ref_Number) !& '-' & Clip(tra:BranchIdentification) & Clip(wob:JobNumber)
                    qIncomeReport.InvoiceDate   = ''
                    qIncomeReport.JobNumber     = ''

                Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    !Error
                End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
				                
                IF (RunProcess())
                    CYCLE
                END ! IF
				
                ADD(qIncomeReport)		
                recordCount += 1
			END ! LOOP
		END ! CASE
		
		Relate:JOBSE.Close()
		Relate:WEBJOB.Close()
		Relate:JOBS.Close()
		Relate:INVOICE.Close()
        Relate:TagFile.Close()
        Relate:SBO_GenericTagFile.Close()
        
        Relate:JOBPAYMT.Close()
        Relate:PARTS.Close()       
        Relate:SUBTRACC_ALIAS.Close()
        Relate:TRADEACC_ALIAS.Close()
        Relate:TRADEACC.Close()
        Relate:SUBTRACC.Close()       
        
        IF (recordCount = 0)
            CreateScript(packet,kNoRecordsAlert)
            DO ReturnFail
        END ! IF
        
    IF (p_web.GetValue('ReportType') = 'E')
        ChargeableIncomeReportExport()
    ELSE
        CallReport('JobIncomeReport')
!        CreateScript(packet,'window.open("JobIncomeReport","_self")')
!        DO SendPacket
    END ! IF
        
ValidateAccountNumber PROCEDURE(STRING pAccountNumber)!,LONG
RetValue LONG(Level:Benign)
	CODE
        IF (p_web.GSV('locAllAccounts') <> 1)
            Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
            tagf:SessionID = p_web.SessionID
            IF (p_web.GSV('locGenericAccounts') = 0)
                tagf:TagType = kSubAccount
            ELSE
                tagf:TagType = kGenericAccount
            END ! IF
            tagf:TaggedValue = pAccountNumber
            IF (Access:SBO_GenericTagFile.TryFetch(tagf:KeyTagged) = Level:Benign)
                IF (tagf:Tagged = 0)
                    RetValue = Level:Fatal
                END ! IF
            ELSE ! IF
                RetValue = Level:Fatal
            END ! IF
		END ! IF	
		
		RETURN RetValue
		
ValidateChargeTypes	PROCEDURE(STRING pChargeType)!,LONG
RetValue	LONG(Level:Benign)
	CODE
        IF (p_web.GSV('locAllChargeTypes') <> 1)
            Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
            tagf:SessionID = p_web.SessionID
            tagf:TagType = 'C'
            tagf:TaggedValue = pChargeType
            IF (Access:SBO_GenericTagFile.TryFetch(tagf:KeyTagged))
				RetValue = Level:Fatal
			ELSE
				IF (tagf:Tagged = 0)
					RetValue = Level:Fatal
				END ! IF
			END ! IF
		END ! IF
		
		RETURN RetValue
	
RunProcess          PROCEDURE()!LONG
    CODE

        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = job:Ref_Number
        IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
            
        ELSE ! IF
        END ! IF     
        
        sentToARC = JobSentToARC()
        
        IF (ValidateChargeTypes(job:Charge_Type))
            RETURN Level:Fatal
        END ! IF  
        
        IF (wob:HeadAccountNumber <> p_web.GSV('BookingAccount'))
            RETURN Level:Fatal
        END ! IF       
        
        IF NOT (jobe:WebJob = 1)
            RETURN Level:Fatal
        END ! IF      

        qRepair(qIncomeReport.Repair)
        qLoan(qIncomeReport.Loan)
        qExchanged(qIncomeReport.Exchanged)
        qWaybillNumber(qIncomeReport.AccountNumberGeneric,qIncomeReport.AccountNameGeneric,qIncomeReport.WaybillNumber)
        qHandlingFee(qIncomeReport.HandlingFee)
        qARCCharge(qIncomeReport.ARCCharge)
        qLoanLoanCharge(qIncomeReport.LostLoanCharge)
        qPartsCost(qIncomeReport.PartsCost)
        qPartsSelling(qIncomeReport.PartsSelling)
        qLabour(qIncomeReport.Labour)
        qARCMarkup(qIncomeReport.PartsSelling,qIncomeReport.ARCMarkup)
        qVAT(qIncomeReport.VAT)
        qTotal(qIncomeReport.VAT,qIncomeReport.Total)
        qCashCreditUser(qIncomeReport.Total,qIncomeReport.CashAmount,qIncomeReport.CreditAmount,qIncomeReport.CollectionBy,qIncomeReport.AmountPaid,qIncomeReport.Outstanding)
        
        IF (p_web.GSV('locZeroSupression') = 'Y')
            ! #13411 Exclude zeros (DBH: 30/10/2014)
            IF (qIncomeReport.Total = 0)
                RETURN Level:Fatal
            END ! IF
        END ! IF
        
        
        RETURN Level:Benign

qRepair PROCEDURE(*STRING pField)!
    CODE
        pField = CHOOSE(sentToARC,'ARC','RRC')
		
qExchanged PROCEDURE(*STRING pField)!
    CODE
        IF (job:Exchange_Unit_Number = 0)
			pField = 'NO'
		ELSE ! IF
			pField = CHOOSE(jobe:ExchangedATRRC,'RRC','ARC')
		END ! IF

qLoan PROCEDURE(*STRING pField)!
    CODE
        pField = CHOOSE(VodacomClass.LoanUnitAttachedToJob(),'YES','NO')
        
qWaybillNumber      PROCEDURE(*STRING pName,*STRING pNumber,*STRING pWaybill)
    CODE
        pName = ''
        pNumber = ''
        pWaybill = ''
        
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = job:Account_Number
        IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
                
        END ! IF
            
        IF (sub:Generic_Account)
            Access:TRADEACC_ALIAS.ClearKey(tra_ali:Account_Number_Key)
            tra_ali:Account_Number = sub:Main_Account_Number
            IF (Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign)
                    
            ELSE ! IF
            END ! IF
            pNumber = tra_ali:Account_Number
                
            IF (SUB(tra_ali:Account_Number,1,7) = 'AA20-85')
                Found# = 0
                Access:AUDIT.ClearKey(aud:TypeActionKey)
                aud:Ref_Number = job:Ref_Number
                aud:Type = CHOOSE(job:Exchange_Unit_Number,'EXC','JOB')
                aud:Action = 'DESPATCHED FROM RRC'
                SET(aud:TypeActionKey,aud:TypeActionKey)
                LOOP UNTIL Access:AUDIT.Next() <> Level:Benign
                    IF (aud:Ref_Number <> job:Ref_Number OR |
                        aud:Type <> CHOOSE(job:Exchange_Unit_Number,'EXC','JOB') OR |
                        aud:Action <> 'DESPATCHED FROM RRC')
                        BREAK
                    END ! IF
                    Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
                    aud2:AUDRecordNumber = aud:Record_Number
                    IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
                        i# = INSTRING('WAYBILL NO:',aud2:Notes,1,1)
                        IF (i# > 0)
                            Found# = 1
                            LOOP j# = i# TO LEN(CLIP(aud2:Notes))
                                IF (SUB(aud2:Notes,j#,1) = '<13>')
                                    BREAK
                                END ! IF (SUB(aud2:Notes,j#,1) = '<13>')
                            END ! LOOP
                            pWaybill = SUB(aud2:Notes,i# + 12, j# - (i# + 12))
                            Found# = 1
                            BREAK
                        END ! IF (i# > 0)
                    END ! IF
                END ! LOOP
            END ! IF
            pName = tra_ali:Company_Name
        END ! IF

qHandlingFee PROCEDURE(*DECIMAL pField)!
    CODE
		pField = 0
        CASE reportStyle
        OF 'I'
            IF (sentToARC = TRUE)
                pField = jobe:InvoiceHandlingFee
                IF (job:Exchange_Unit_Number > 0)
                    pField += jobe:InvoiceExchangeRate
                END ! IF
            END
        OF 'C'
            IF (sentToARC = TRUE)
                pField = jobe:HandlingFee
                IF (job:Exchange_Unit_Number > 0)
                    pField += jobe:ExchangeRate
                END ! IF
            END
        END ! CASE
        
qARCCharge PROCEDURE(*DECIMAL pField)!
    CODE
        pField = 0
        CASE reportStyle
        OF 'I'
            IF (sentToARC = TRUE)
                IF (jobe:Engineer48HourOption = 1)
                    pField = job:Invoice_Parts_Cost + job:Invoice_Labour_Cost
                    
                ELSE !If jobe:Engineer48HourOption = 1
                    pField =  job:Invoice_Courier_Cost + job:Invoice_Parts_Cost + job:Invoice_Labour_Cost
                    
                END !If jobe:Engineer48HourOption = 1	
            END
        OF 'C'
            IF (sentToARC = TRUE)
                IF (jobe:Engineer48HourOption = 1)
                    pField = job:Courier_Cost + job:Parts_Cost
                    
                ELSE !If jobe:Engineer48HourOption = 1
                    pField = job:Courier_Cost + job:Parts_Cost + job:Labour_Cost
                    
                END !If jobe:Engineer48HourOption = 1    
            END
        END ! CASE

        
qLoanLoanCharge PROCEDURE(*DECIMAL pField)!
    CODE
        pField = 0
        CASE reportStyle
        OF 'I'
            IF (VodacomClass.LoanUnitAttachedToJob())
                pField = job:Invoice_Courier_Cost
            END ! IF
        OF 'C'
            IF (VodacomClass.LoanUnitAttachedToJob())
                pField = job:Courier_Cost
            END ! IF        
        END ! IF
                
qPartsCost PROCEDURE(*DECIMAL pField)!
    CODE
        Access:PARTS.ClearKey(par:Part_Number_Key)
        par:Ref_Number = job:Ref_Number
        SET(par:Part_Number_Key,par:Part_Number_Key)
        LOOP UNTIL Access:PARTS.Next() <> Level:Benign
            IF (par:Ref_Number <> job:Ref_Number)
                BREAK
            END ! IF
            
            Access:STOCK.ClearKey(sto:Ref_Number_Key)
            sto:Ref_Number = par:Part_Ref_Number
            IF (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
                IF (sto:Location <> p_web.GSV('Default:ARCLocation'))
                    ! These are ARC Parts
                    pField += par:RRCAveragePurchaseCost
                END!  IF
            ELSE ! IF
            END ! IF
        END ! LOOP         

qPartsSelling PROCEDURE(*DECIMAL pField)!
    CODE
        
        CASE reportStyle
        OF 'I'
            IF (sentToARC = TRUE)
                pField = jobe:InvRRCCPartsCost
                IF (jobe:Engineer48HourOption = 1)
                    pField += job:Invoice_Courier_Cost
                END ! IF
            ELSE
                pField = jobe:InvRRCCPartsCost
            END 
        OF 'C'
            IF (sentToARC = TRUE)
                pField = jobe:RRCCPartsCost
                IF (jobe:Engineer48HourOption = 1)
                    pField += job:Courier_Cost
                END ! IF
            ELSE 
                pField = jobe:RRCCPartsCost
            END
        END ! CASE
        
qLabour PROCEDURE(*DECIMAL pField)!
    CODE
        CASE reportStyle
        OF 'I'
            IF (sentToARC = TRUE)
                pField = jobe:InvRRCCLabourCost - job:Invoice_Labour_Cost
            ELSE ! IF 
                pField = jobe:InvRRCCLabourCost
            END ! IF
        OF 'C'
            IF (sentToARC = TRUE)
                pField = jobe:RRCCLabourCost - job:Labour_Cost
            ELSE ! IF
                pField = jobe:RRCCLabourCost
            END ! IF
        END ! CASE

qARCMarkup PROCEDURE(REAL pPartsSelling,*DECIMAL pField)!
    CODE
        pField = 0
        CASE reportStyle
        OF 'I'
            IF (sentToARC = TRUE)
                pField = pPartsSelling - job:Invoice_Parts_Cost
  
            END ! IF
        OF 'C'
            IF (sentToARC = TRUE)
                pField = pPartsSelling - job:Parts_Cost
            END ! IF
        END ! CASE
        
        IF (pField < 0)
            pField = 0
        END ! IF        
        
qVAT PROCEDURE(*DECIMAL pField)!
    CODE
        CASE reportStyle
        OF 'I'
            pField = (jobe:InvRRCCLabourCost * inv:Vat_Rate_Labour/100) + |
                        (jobe:InvRRCCPartsCost * inv:Vat_Rate_Parts/100) + |
                        (job:Invoice_Courier_Cost * inv:Vat_Rate_Labour/100)            
        OF 'C'
            pField = (jobe:RRCCLabourCost * VodacomClass.VatRate(job:Account_Number,'L')/100) + |
                        (jobe:RRCCPartsCost * VodacomClass.VatRate(job:Account_Number,'P')/100) + |
                        (job:Courier_Cost * VodacomClass.VatRate(job:Account_Number,'L')/100)
        END ! CASE
        
qTotal PROCEDURE(REAL pVAT,*DECIMAL pField)!
    CODE
        CASE reportStyle
        OF 'I'
            pField = jobe:InvRRCCLabourCost + jobe:InvRRCCPartsCost + job:Invoice_Courier_Cost + pVAT
        OF 'C'
            pField = jobe:RRCCLabourCost + jobe:RRCCPartsCost + job:Courier_Cost + pVAT
        END ! CASE
        
qCashCreditUser PROCEDURE(REAL pTotal,*DECIMAL pCash,*DECIMAL pCredit,*STRING pUser,*DECIMAL pPaid,*DECIMAL pOutstanding)!
    CODE
        pCash = 0
        pCredit = 0
        pUser = ''
        pPaid = 0
        pOutstanding = 0
        
        Access:JOBPAYMT.ClearKey(jpt:All_Date_Key)
        jpt:Ref_Number = job:Ref_Number 
        SET(jpt:All_Date_Key,jpt:All_Date_Key)
        LOOP UNTIL Access:JOBPAYMT.Next() <> Level:Benign
            IF (jpt:Ref_Number <> job:Ref_Number)
                BREAK
            END ! IF
            Access:PAYTYPES.ClearKey(pay:Payment_Type_Key)
            pay:Payment_Type = jpt:Payment_Type
            IF (Access:PAYTYPES.TryFetch(pay:Payment_Type_Key) = Level:Benign)
                IF (pay:Credit_Card = 'YES')
                    pCredit += jpt:Amount
                ELSE
                    pCash += jpt:Amount
                END ! IF
            ELSE ! IF
                pCash += jpt:Amount
            END ! IF
            pUser = jpt:User_Code
        END ! LOOP

        pPaid = pCredit + pCash
        
        pOutstanding = pTotal - pPaid
        
        IF (pOutstanding < 0)
            pOutStanding = 0
        END ! IF    

!endregion
!region ChargeableIncomeReportExport
ChargeableIncomeReportExport        PROCEDURE()!
i                                       LONG()
TotalGroup                              GROUP,PRE(total)
Lines                                       LONG
HandlingFee                                 REAL
LostLoanCharge                              REAL
ARCCharge                                   REAL
Labour                                      REAL
Parts                                       REAL
PartsSelling                                REAL
ARCMarkup                                   REAL
VAT                                         REAL
Total                                       REAL
CashAmount                                  REAL
CreditAmount                                REAL
AmountPaid                                  REAL
AmountOutstanding                           REAL
                                        END
recs                                    LONG
    CODE
        Relate:USERS.Open()
        Relate:JOBS.Open()
        Relate:WEBJOB.Open()
        Relate:TRADEACC.Open()
        
        ProgressBarWeb.Init(RECORDS(qIncomeReport))
        
        expfil.Init('Job Income Report')
		
        CASE p_web.GSV('locReportStyle')
        OF 'I'
            expfil.AddField('Chargeable Income Report - Invoiced',1,1)
			
        OF 'C'
            expfil.AddField('Chargeable Income Report - Non-Invoiced',1,1)
			
        END ! CASE
		
        expfil.AddField('Date Range',1)
        expfil.AddField(FORMAT(p_web.GSV('locStartDate'),@d06) & ' to ' & FORMAT(p_web.GSV('locEndDate'),@d06),,1)
		
        expfil.AddField('Printed By',1)
        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = p_web.GSV('BookingUserCode')
        IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
			
        ELSE ! IF
        END ! IF
        expfil.AddField(CLIP(use:Forename) & ' ' & CLIP(use:Surname),,1)
		
        expfil.AddField('Date Printed',1)
        expfil.AddField(FORMAT(TODAY(),@d06),,1)
		
        expfil.AddField('Job No,IMEI Number,Branch ID,Franchise Job No',1,,,1)
        
        IF (p_web.GSV('locReportStyle') = 'I')
            expfil.AddField('Inv No,Inv Date',,,,1)
			
        END ! IF
		
        expfil.AddField('Enginer Option,Head Account No,Head Account Name,Account Number,Waybill Number,Account Name,Sub Account No,Sub Account Name,Chargeable Charge Type,Completed Date,Chargeable Repair Type,Current Status,Days In Status,Total Days,Engineer,Repair,Exch,Loan,Handling/Exchange Fee,ARC Charge,RRC Lost Loan,RRC Part Cost,RRC Part Sell,RRC Labour,'&|
            'ARC M/Up,RRC VAT,RRC Total,Cash/Chq,Credit Card,Paid,Outstanding,Coll By',,1,,1)
		
        IF (p_web.GSV('locReportStyle') = 'C')
            SORT(qIncomeReport,qIncomeReport.SessionID,qIncomeReport.JobNumber)
			
        ELSE
            SORT(qIncomeReport,qIncomeReport.SessionID,qIncomeReport.InvoiceNumber)
        END ! IF
        
        CLEAR(TotalGroup)
		
        LOOP i = 1 TO RECORDS(qIncomeReport)
            GET(qIncomeReport,i)
            
            ProgressBarWeb.Update('Exporting...')
            
            IF (qIncomeReport.SessionID <> p_web.SessionID)
                CYCLE
            END ! IF
            
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = qIncomeReport.RefNumber
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
            END
            
            Access:WEBJOB.ClearKey(wob:RefNumberKey)
            wob:RefNumber = job:Ref_Number
            IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
            END
				
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number  = wob:HeadAccountNumber
            IF Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            END  ! IF
            
            ! Job Number
            expfil.AddField(job:Ref_Number,1)
            ! IMEI Number
            expfil.AddField('`' & job:ESN)
            ! Branch ID
            expfil.AddField(tra:BranchIdentification)
            ! Branch Job Number
            expfil.AddField(wob:JobNumber)
            ! Invoice Number / Date
            IF (p_web.GSV('locReportStyle') = 'I')
                CASE p_web.GSV('locReportOrder')
                OF 0
                    expfil.AddField(qIncomeReport.InvoiceNumber)
                    expfil.AddField(qIncomeReport.InvoiceDate)
                OF 1
                    expfil.AddField(qIncomeReport.InvoiceDate)
                    expfil.AddField(qIncomeReport.JobNumber)
                END ! CASE		
            END ! IF
            ! Engineer Option
            expfil.AddField(CHOOSE(jobe:Engineer48HourOption,'48H','ARC','7DT','STD',''))
            ! Head Account Number
            expfil.AddField(tra:Account_Number)
            ! Head Account Name
            expfil.AddField(tra:Company_Name)
            ! Account Number (Generic)
            expfil.AddField(qIncomeReport.AccountNumberGeneric)
            ! Account Name (Generic)
            expfil.AddField(qIncomeReport.AccountNameGeneric)
            ! Waybill Number
            expfil.AddField(qIncomeReport.WaybillNumber)

            ! Sub Account Number
            expfil.AddField(job:Account_Number)
            ! Sub Accont Name
            expfil.AddField(job:Company_Name)
            ! Chargeable Charge Type
            expfil.AddField(job:Charge_Type)
            ! Completed Date
            expfil.AddField(FORMAT(job:Date_Completed,@d06))
            ! Chargeable Repair Type
            expfil.AddField(job:Repair_Type)
            ! Current Status
            expfil.AddField(job:Current_Status)
            ! Days In Status
            expfil.AddField(TODAY() - wob:Current_Status_Date)
            ! Total Days
            expfil.AddField(TODAY() - job:Date_Booked)
            ! Engineer
            Access:USERS.ClearKey(use:User_Code_Key)
            use:User_Code = job:Engineer
            IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
                expfil.AddField(CLIP(use:Forename) & ' ' & CLIP(use:Surname))
                
            ELSE ! IF
                expfil.AddField()
                
            END ! IF
            ! Repair
            expfil.AddField(qIncomeReport.Repair)
            ! Exchanged
            expfil.AddField(qIncomeReport.Exchanged)
            ! Loan
            expfil.AddField(qIncomeReport.Loan)
            ! Handling Fee
            expfil.AddField(qIncomeReport.HandlingFee)
            ! ARC Charge
            expfil.AddField(qIncomeReport.ARCCharge)
            ! Loan Loan
            expfil.AddField(qIncomeReport.LostLoanCharge)
            ! Parts Cost
            expfil.AddField(qIncomeReport.PartsCost)
            ! Parts Selling
            expfil.AddField(qIncomeReport.PartsSelling)
            ! Labour
            expfil.AddField(qIncomeReport.Labour)
            ! ARC Markup
            expfil.AddField(qIncomeReport.ARCMarkup)
            ! VAT
            expfil.AddField(qIncomeReport.VAT)
            ! Total
            expfil.AddField(qIncomeReport.Total)
            ! Cash Amount
            expfil.AddField(qIncomeReport.CashAmount)
            ! Credit Card
            expfil.AddField(qIncomeReport.CreditAmount)
            ! Paid
            expfil.AddField(qIncomeReport.AmountPaid)
            ! Outstanding
            expfil.AddField(qIncomeReport.Outstanding)
            ! Collected By
            expfil.AddField(qIncomeReport.CollectionBy,,1)
            
            total:Lines             += 1
            total:ARCCharge         += qIncomeReport.ARCCharge
            total:HandlingFee       += qIncomeReport.HandlingFee
            total:LostLoanCharge    += qIncomeReport.LostLoanCharge
            total:Labour            += qIncomeReport.Labour
            total:Parts             += qIncomeReport.PartsCost
            total:PartsSelling      += qIncomeReport.PartsSelling
            total:ARCMarkup         += qIncomeReport.ARCMarkup
            total:VAT               += qIncomeReport.Vat
            total:Total             += qIncomeReport.Total
            total:CashAmount        += qIncomeReport.CashAmount
            total:CreditAmount      += qIncomeReport.CreditAmount
            total:AmountPaid        += qIncomeReport.AmountPaid
            total:AmountOutstanding += qIncomeReport.Outstanding            
        END ! LOOP
        
        expfil.AddField('TOTALS',1,1)
        
        expfil.AddField('Jobs Counted',1)
        expfil.AddField(total:Lines)
        CASE p_web.GSV('locReportStyle')
        OF 'I'
            expfil.AddField(,,,22)
        OF 'C'
            expfil.AddField(,,,20)
        END ! ICASE
        expfil.AddField(FORMAT(total:HandlingFee,@N10.2))
        expfil.AddField(FORMAT(total:ARCCharge,@N10.2))
        expfil.AddField(FORMAT(total:LostLoanCharge,@N10.2))
        expfil.AddField(FORMAT(total:Parts,@N10.2))
        expfil.AddField(FORMAT(total:PartsSelling,@N10.2))
        expfil.AddField(FORMAT(total:Labour,@N10.2))
        expfil.AddField(FORMAT(total:ARCMarkup,@N10.2))
        expfil.AddField(FORMAT(total:VAT,@N10.2))
        expfil.AddField(FORMAT(total:Total,@N10.2))
        expfil.AddField(FORMAT(total:CashAmount,@N10.2))
        expfil.AddField(FORMAT(total:CreditAmount,@N10.2))
        expfil.AddField(FORMAT(total:AmountPaid,@N10.2))
        expfil.AddField(FORMAT(total:AmountOutstanding,@N10.2),,1)
        
        Relate:USERS.Close()
        Relate:TRADEACC.Close()
        Relate:JOBS.Close()
        Relate:WEBJOB.Close()
        
        expfil.Kill()
        
        LOOP i = recs TO 1 BY -1
            GET(qIncomeReport,i)
            IF (qIncomeReport.SessionID = p_web.SessionID)
                DELETE(qIncomeReport)
            END ! IF
        END ! LOOP
!endregion
CreateExchangeLoanOrder PROCEDURE()!
recordCount	LONG()
errorFound LONG(0)
    CODE
		Relate:SBO_GenericFile.Open()
		Relate:EXCHORNO.Open()
		Relate:EXCHORDR.Open()
        Relate:LOANORNO.Open()
        Relate:LOAORDR.Open()
	
		LOOP 1 TIMES
			Access:SBO_GenericFile.ClearKey(sbogen:String1Key)
			sbogen:SessionID = p_web.SessionID
			SET(sbogen:String1Key,sbogen:String1Key)
			LOOP UNTIL Access:SBO_GenericFile.Next() <> Level:Benign
				IF (sbogen:SessionID <> p_web.SessionID)
					BREAK
				END ! IF
				recordCount += 1
			END ! LOOP
			
			IF (recordCount = 0)
				CreateScript(packet,'alert("You have not added any units to order.")')
				errorFound = 1
				BREAK
				
			END ! IF
			
			ProgressBarWeb.Init(recordCount)

			CASE p_web.GSV('OrderType')
			OF 'E'
				IF (Access:EXCHORNO.PrimeRecord() = Level:Benign)
					eno:Location = p_web.GSV('BookingSiteLocation')
					eno:UserCode = p_web.GSV('BookingUserCode')
                    IF (Access:EXCHORNO.TryInsert())
                        Access:EXCHORNO.CancelAutoInc()
                    ELSE ! IF
                        Access:SBO_GenericFile.ClearKey(sbogen:String1Key)
                        sbogen:SessionID = p_web.SessionID
                        SET(sbogen:String1Key,sbogen:String1Key)
                        LOOP UNTIL Access:SBO_GenericFile.Next() <> Level:Benign
                            IF (sbogen:SessionID <> p_web.SessionID)
                                BREAK
                            END ! IF
							
                            ProgressBarWeb.Update('Creating Order...')
							
                            IF (Access:EXCHORDR.PrimeRecord() = Level:Benign)
                                exo:Location = p_web.GSV('BookingSiteLocation')
                                exo:Manufacturer = sbogen:String2
                                exo:Model_Number = sbogen:String1
                                exo:Qty_Required = sbogen:Long1
                                exo:Qty_Received = 0
                                exo:Notes = UPPER(sbogen:Notes)
                                exo:OrderNumber = eno:RecordNumber
                                exo:DateCreated = TODAY()
                                exo:TimeCreated = CLOCK()
                                IF (Access:EXCHORDR.TryInsert())
                                    Access:EXCHORDR.CancelAutoInc()
                                END ! IF
                                
                            END ! IF
                        END ! LOOP	
                    END ! IF
                    p_web.FileToSessionQueue(EXCHORNO)
				END ! IF
				
			OF 'L'
                IF (Access:LOANORNO.PrimeRecord() = Level:Benign)
                    lno:Location = p_web.GSV('BookingSiteLocation')
                    lno:UserCode = p_web.GSV('BookingUserCode')
                    IF (Access:LOANORNO.TryInsert())
                        Access:LOANORNO.CancelAutoInc()
                    ELSE
                        Access:SBO_GenericFile.ClearKey(sbogen:String1Key)
                        sbogen:SessionID = p_web.SessionID
                        SET(sbogen:String1Key,sbogen:String1Key)
                        LOOP UNTIL Access:SBO_GenericFile.Next() <> Level:Benign
                            IF (sbogen:SessionID <> p_web.SessionID)
                                BREAK
                            END ! IF
							
                            ProgressBarWeb.Update('Creating Order...')
							
                            IF (Access:LOAORDR.PrimeRecord() = Level:Benign)
                                lor:Location = p_web.GSV('BookingSiteLocation')
                                lor:Manufacturer = sbogen:String2
                                lor:Model_Number = sbogen:String1
                                lor:Qty_Required = sbogen:Long1
                                lor:Qty_Received = 0
                                lor:Notes = UPPER(sbogen:Notes)
                                lor:OrderNumber = lno:RecordNumber
                                IF (Access:LOAORDR.TryInsert())
                                    Access:LOAORDR.CancelAutoInc()
                                END
                            END ! IF (Access:LOAODRD.PrimeRecord() = Level:Benign)
							
                        END ! LOOP ll# = 1 TO RECORDS(ModelQueue)

                    END ! IF (Access:LOANORNO.TryInsert())
                END ! IF (Access:LOANORNO.PrimeRecord() = Level:Benign)		
                p_web.FileToSessionQueue(LOANORNO)
			END ! CASE
		END ! LOOP
		
		Relate:LOANORNO.Close()
		Relate:EXCHORDR.Close()
        Relate:EXCHORNO.Close()
        Relate:LOAORDR.Close()
		Relate:SBO_GenericFile.Close()
		
        IF (errorFound = 1)
            DO ReturnFail
        END ! IF

        CreateScript(packet,'window.open("PrintGenericDocument?PrintType=ExchangeOrderReport","_self")')
!region CreditReportExport
CreditReportExport      PROCEDURE()
count LONG()

TotalsGroup          GROUP,PRE(total)
HandlingFee           REAL
ARCCharge            REAL
RRCLostLoan          REAL
RRCPartsCost         REAL
RRCPartsSelling      REAL
RRCLabour            REAL
ARCMarkup            REAL
RRCVAT               REAL
RRCTotal             REAL
Paid                 REAL
Outstanding          REAL
CNAmount             REAL
NewRRCTotal          REAL
Refund               REAL
                     END
    CODE
        Relate:JOBSINV.Open()
        Relate:JOBS.Open()
        Relate:JOBSE.Open()
        Relate:WEBJOB.Open()
        Relate:INVOICE.Open()
        Relate:TRADEACC.Open()
        Relate:SUBTRACC.Open()
        
        count = 0
        CLEAR(TotalsGroup)
        
        ProgressBarWeb.Init(2000)
        
        expfil.Init('Credit Report')
        
        expfil.AddField('Job No,Branch ID,Franchise Job No,Original Invoice No,Original Invoice Date,' & |
                    'CN Number,CN Date,Passed By,New Inv No,New Inv Date,Head Account No,Head Account Name,' & |
                    'Sub Account No,Sub Account Name,Chargeable Charge Type,Completed Date,' & |
                    'Chargeable Repair Type,Repair,Handling/Exchange Fee,ARC Charge,RRC Lost Loan,' & |
                    'RRC Parts Cost,RRC Parts Sell,RRC Labour,ARC M/Up,RRC Vat,RRC Total,Paid,Outstanding,' & |
                    'CN Amount,New RRC Total,Refund',1,1,,1)
        
        Access:JOBSINV.ClearKey(jov:BookingDateTypeKey)
        jov:BookingAccount = p_web.GSV('BookingAccount')
        jov:Type = 'C'
        jov:DateCreated = p_web.GSV('locStartDate')
        SET(jov:BookingDateTypeKey,jov:BookingDateTypeKey)
        LOOP UNTIL Access:JOBSINV.Next() <> Level:Benign
            IF (jov:BookingAccount <> p_web.GSV('BookingAccount') OR |
                jov:Type <> 'C' OR |
                jov:DateCreated > p_web.GSV('locEndDate'))
                BREAK
            END ! IF
            
            ProgressBarWeb.Update('Exporting...')
            
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = jov:RefNumber
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                
            ELSE ! IF
            END ! IF
            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = job:Ref_Number
            IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                
            ELSE ! IF
            END ! IF
            Access:WEBJOB.ClearKey(wob:RefNumberKey)
            wob:RefNumber = job:Ref_Number
            IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                
            ELSE ! IF
            END ! IF
            Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
            inv:Invoice_Number = job:Invoice_Number
            IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign)
                
            ELSE ! IF
            END ! IF
            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = wob:HeadAccountNumber
            IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                
            ELSE ! IF
            END ! IF
            Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
            sub:Account_Number = job:Account_Number
            IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
                
            ELSE ! IF
            END ! IF
            
            ! Job Number
            expfil.AddField(job:Ref_Number,1)
            ! Branch ID
            expfil.AddField(tra:BranchIdentification)
            ! Franchise Job No
            expfil.AddField(wob:JobNumber)
            ! Original Invoice No
            expfil.AddField(job:Invoice_Number)
            ! Original Invoice Date
            expfil.AddField(FORMAT(inv:RRCInvoiceDate,@d06b))
            ! CN Number
            expfil.AddField('CN' & CLIP(jov:InvoiceNumber) & jov:Suffix)
            ! CN Date
            expfil.AddField(FORMAT(jov:DateCreated,@d06b))
            ! Passed By
            expfil.AddField(jov:UserCode)
            ! New Invoice No
            expfil.AddField(jov:NewInvoiceNumber)
            ! New Invoice Date
            expfil.AddField(FORMAT(jov:DateCreated,@d06b))
            ! Head Account No
            expfil.AddField(wob:HeadAccountNumber)
            ! Head Account Name
            expfil.AddField(tra:Company_Name)
            ! Sub Account No
            expfil.AddField(job:Account_Number)
            ! Sub Account Name
            expfil.AddField(sub:Company_Name)
            ! Chargeable Charge Type
            expfil.AddField(job:Charge_Type)
            ! Completed Date
            expfil.AddField(FORMAT(job:Date_Completed,@d06b))
            ! Chargeable Repair Type
            expfil.AddField(job:Repair_Type)
            ! Repair
            IF (JobSentToARC())
                expfil.AddField('ARC')
            ELSE ! IF
                expfil.AddField('RRC')
            END ! IF
            ! Handling / Exchange Fee
            IF (jov:HandlingFee > 0)
                expfil.AddField(FORMAT(jov:HandlingFee,@n~R~_14.2))
                total:HandlingFee += jov:HandlingFee
            ELSE ! IF
                expfil.AddField(FORMAT(jov:ExchangeRate,@n~R~_14.2))
                total:HandlingFee += jov:ExchangeRate
            END ! IF
            IF (JobSentToARC())
                expfil.AddField(FORMAT(jov:ARCCharge,@n~R~_14.2))
                total:ARCCharge += jov:ARCCharge
            ELSE ! IF
                expfil.AddField()
            END ! IF
            ! RRC Lost Loan
            expfil.AddField(FORMAT(jov:RRCLostLoanCost,@n~R~_14.2))
            total:RRCLostLoan += jov:RRCLostLoanCost
            ! RRC Parts Cost
            expfil.AddField(FORMAT(jov:RRCPartsCost,@n~R~_14.2))
            total:RRCPartsCost += jov:RRCPartsCost
            ! RRC Parts Selling
            expfil.AddField(FORMAT(jov:RRCPartsSelling,@n~R~_14.2))
            total:RRCPartsSelling += jov:RRCPartsSelling
            ! RRC Labour
            expfil.AddField(FORMAT(jov:RRCLabour,@n~R~_14.2))
            total:RRCLabour += jov:RRCLabour
            ! ARC Markup
            expfil.AddField(FORMAT(jov:ARCMarkup,@n~R~_14.2))
            total:ARCMarkup += jov:ARCMarkup
            ! RRC VAT
            expfil.AddField(FORMAT(jov:RRCVAT,@n~R~_14.2))
            total:RRCVAT += jov:RRCVAT
            ! RRC Total
            expfil.AddField(FORMAT(jov:RRCLabour + jov:RRCPartsSelling + jov:RRCLostLoanCost + jov:RRCVAT,@n~R~_14.2))
            total:RRCTotal += jov:RRCLabour + jov:RRCPartsSelling + jov:RRCLostLoanCost + jov:RRCVAT
            ! Paid
            expfil.AddField(FORMAT(jov:Paid,@n~R~_14.2))
            total:Paid += jov:Paid
            ! Outstanding
            expfil.AddField(FORMAT(jov:Outstanding,@n~R~_14.2))
            total:Outstanding += jov:Outstanding
            ! CN Amount
            expfil.AddField(FORMAT(jov:CreditAmount,@n~R~_14.2))
            total:CNAmount += jov:CreditAmount
            ! New RRC Total
            expfil.AddField(FORMAT(jov:NewTotalCost,@n~R~_14.2))
            total:NewRRCTotal += jov:NewTotalCost
            ! Refund
            expfil.AddField(FORMAT(jov:Refund,@n~R~_14.2),,1)
            total:Refund += jov:Refund
            
            count += 1
        END ! LOOP
        
        expfil.AddField('Totals,,,,,,,,,,,,,,,,,,' & |
                    Format(total:HandlingFee,@n~R~-_14.2) & ',' &|
                    Format(total:ARCCharge,@n~R~-_14.2) & ',' &|
                    Format(total:RRCLostLoan,@n~R~-_14.2) & ',' &|
                    Format(total:RRCPartsCost,@n~R~-_14.2) & ',' &|
                    Format(total:RRCPartsSelling,@n~R~-_14.2) & ',' &|
                    Format(total:RRCLabour,@n~R~-_14.2) & ',' &|
                    Format(total:ARCMarkup,@n~R~-_14.2) & ',' &|
                    Format(total:RRCVat,@n~R~-_14.2) & ',' &|
                    Format(total:RRCTotal,@n~R~-_14.2) & ',' &|
                    Format(total:Paid,@n~R~-_14.2) & ',' &|
                    Format(total:Outstanding,@n~R~-_14.2) & ',' &|
                    Format(total:CNAmount,@n~R~-_14.2) & ',' &|
                    Format(total:NewRRCTotal,@n~R~-_14.2) & ',' &|
                    Format(total:Refund,@n~R~-_14.2),1,1,,1)
        
        expfil.AddField('Jobs Counted',1)
        expfil.AddField(count,,1)
        
        
        Relate:JOBSINV.Close()
        Relate:SUBTRACC.Close()
        Relate:TRADEACC.Close()
        Relate:INVOICE.Close()
        Relate:WEBJOB.Close()
        Relate:JOBSE.Close()
        Relate:JOBS.Close()        

        IF (count = 0)
            CreateScript(packet,kNoREcordsAlert)
            expfil.Kill(1)
            Do ReturnFail
        ELSE
            expfil.Kill()
        END ! IF

!endregion
!Create GRN
CreateGRN           PROCEDURE()
goodsReceived   LONG(0)
    CODE
        p_web.SSV('locGRNNumber',0)
        goodsReceived = 0
        
        Relate:RETSALES.Open()
        Relate:RETSTOCK.Open()
        Relate:GRNOTESR.Open()

        Access:RETSALES.ClearKey(ret:Invoice_Number_Key)
        ret:Invoice_Number = p_web.GSV('locInvoiceNumber')
        IF (Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign)
            Access:RETSTOCK.ClearKey(res:Part_Number_Key)
            res:Ref_Number = ret:Ref_Number
            SET(res:Part_Number_Key,res:Part_Number_Key)
            LOOP UNTIL Access:RETSTOCK.Next() <> Level:Benign
                IF (res:Ref_Number <> ret:Ref_Number)
                    BREAK
                END ! IF
                IF NOT (res:Received)
                    CYCLE
                END ! IF
                IF (res:DateReceived = p_web.GSV('locGRN'))
                    goodsReceived = 1
                    BREAK
                END ! IF
            END ! LOOP
        END ! IF
        
        IF (goodsReceived = 1)
            IF (Access:GRNOTESR.PrimeRecord() = Level:Benign)
                grr:Order_Number = p_web.GSV('locInvoiceNumber')
                grr:Goods_Received_Date = p_web.GSV('locGRN')
                IF (Access:GRNOTESR.TryInsert())
                    Access:GRNOTESR.CancelAutoInc()
                ELSE
                    Access:RETSALES.ClearKey(ret:Invoice_Number_Key)
                    ret:Invoice_Number = p_web.GSV('locInvoiceNumber')
                    IF (Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign)
                        Access:RETSTOCK.ClearKey(res:Part_Number_Key)
                        res:Ref_Number = ret:Ref_Number
                        SET(res:Part_Number_Key,res:Part_Number_Key)
                        LOOP UNTIL Access:RETSTOCK.Next() <> Level:Benign
                            IF (res:Ref_Number <> ret:Ref_Number)
                                BREAK
                            END ! IF
                            IF NOT (res:Received)
                                CYCLE
                            END ! IF
                            IF (res:DateReceived = p_web.GSV('locGRN'))
                                IF (res:GRNNumber = 0)
                                    res:GRNNumber = grr:Goods_Received_Number
                                    Access:RETSTOCK.TryUpdate()
                                END ! IF
                            END ! IF
                        END ! LOOP
                    ELSE ! IF
                    END ! IF
                    p_web.SSV('locGRNNumber',grr:Goods_Received_Number)
                END ! IF
            END ! IF
        END
        
        Relate:GRNOTESR.Close()
        Relate:RETSTOCK.Close()
        Relate:RETSALES.Close()
        
        IF (goodsReceived = 0)
            CreateScript(packet,'alert("No items were received on the selected date")')
            DO ReturnFail
        ELSE
            p_web.SSV('RedirectURL','PrintGenericDocument?PrintType=GRN')
        
        END ! IF (goodsReceived = 1)
CountBouncers		PROCEDURE()!
count	LONG(0)
    CODE
        Relate:JOBS.Open()
		Relate:WEBJOB.Open()
		
		
		ProgressBarWeb.Init(1000)
		
		Access:JOBS.ClearKey(job:Bouncer_Key)
		job:Bouncer = 'X'
		SET(job:Bouncer_Key,job:Bouncer_Key)
		LOOP UNTIL Access:JOBS.Next() <> Level:Benign
			IF (job:Bouncer <> 'X')
				BREAK
			END ! IF
			
			ProgressBarWeb.Update('Counting...')
			
			Access:WEBJOB.ClearKey(wob:RefNumberKey)
			wob:RefNumber = job:Ref_Number
			IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
				IF (wob:HeadAccountNumber <> p_web.GSV('BookingAccount'))
					CYCLE
				END ! IF
			ELSE ! IF
				CYCLE
			END ! IF
			
			count += 1
			
		END ! LOOP
		
		Relate:WEBJOB.Close()
		Relate:JOBS.Close()
		
        CreateScript(packet,'alert("There are ' & count & ' jobs in this browse.");window.close()')
        
! ExchangeAuditExport
ExchangeAuditExport PROCEDURE()
totalLines              LONG()
recordCount             LONG()
i                       LONG
    CODE
        ProgressBarWeb.Init(200)
		
        expfil.Init('Exchange Audit Report')
		
        Relate:EXCHAUI.Open()
        Relate:EXCHAMF.Open()
        Relate:EXCHANGE.Open()
		
        Access:EXCHAMF.ClearKey(emf:Audit_Number_Key)
        emf:Audit_Number = p_web.GetValue('emf:Audit_Number')
        IF (Access:EXCHAMF.TryFetch(emf:Audit_Number_Key) = Level:Benign)
			
        END ! IF
		
        expfil.AddField('EXCHANGE AUDIT REPORT',1)
        expfil.AddField(p_web.GSV('BookingSiteLocation'),,1)
		
        expfil.AddField('AUDIT NUMBER',1)
        expfil.AddField(emf:Audit_Number,,1)
		
        expfil.AddField(FORMAT(TODAY(),@d7),1,1)
        expfil.AddField(,1,1)
		
        LOOP i = 1 TO 2
            totalLines = 0
            IF (i = 1)
                expfil.AddField('SHORTAGES',1,1)
				
            ELSE
                expfil.AddField('EXCESSES',1,1)
				
            END ! IF
			
            IF (emf:Complete_Flag = 0)
                expfil.AddField('This Audit is not fully completed',1,1)
                expfil.AddField(,1,1)
            END ! IF
            expfil.AddField('Stock Type',1)
            expfil.AddField('Make')
            expfil.AddField('Model')
            expfil.AddField('IMEI',,1)
			
            Access:EXCHAUI.ClearKey(eau:Audit_Number_Key)
            eau:Audit_Number = emf:Audit_Number
            SET(eau:Audit_Number_Key,eau:Audit_Number_Key)
            LOOP UNTIL Access:EXCHAUI.Next() <> Level:Benign
                IF (eau:Audit_Number <> emf:Audit_Number)
                    BREAK
                END ! IF
				
                IF (i = 1)
					! Shortages
                    IF (eau:Confirmed <> 0)
                        CYCLE
                    END ! IF
				
                ELSE
					! Excesses
                    IF (eau:New_IMEI <> 1)
                        CYCLE
                    END ! IF
                END!  IF			
				
                totalLines += 1
				
                Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                xch:Ref_Number = eau:Ref_Number
                IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
					
                ELSE ! IF
                    CYCLE
                END ! IF
				
                expfil.AddField(xch:Stock_Type,1)
                expfil.AddField(xch:Manufacturer)
                expfil.AddField(xch:Model_Number)
                expfil.AddField('`' & xch:ESN,,1)
				
                recordCount += 1
            END ! LOOP
		
            IF (totalLines = 0)
                expfil.AddField(,1)
                expfil.AddField('NO ITEMS TO REPORT',,1)
                
            ELSE
                expfil.AddField('Total number of lines',1)
                expfil.AddField(totalLines,,1)

            END ! IF
			
            expfil.AddField(,1,1)
        END ! LOOP
        Relate:EXCHAUI.Close()
        Relate:EXCHAMF.Close()
        Relate:EXCHANGE.Close()
		
		
        IF (recordCount = 0)
            CreateScript(packet,kNoRecordsAlert)
            expfil.Kill(1)
            DO ReturnFail
        ELSE
            expfil.Kill()
        END ! IF
! ExchangeLoanAvailableExport
ExchangeLoanAvailableExport PROCEDURE()!
    CODE
        Relate:SBO_GenericFile.Open()
		
        ProgressBarWeb.Init(RECORDS(SBO_GenericFile))
        
        IF (p_web.GSV('OrderType') = 'E')
            expfil.Init('Exchange Available Report')
        END ! IF
        IF (p_web.GSV('OrderType') = 'L')
            expfil.Init('Loan Available Report')
        END ! IF
		
        IF (p_web.GSV('OrderType') = 'E')
            expfil.AddField('Exchange Stock',1,1)
        END ! IF
        IF (p_web.GSV('OrderType') = 'L')
            expfil.AddField('Loan Stock',1,1)
        END ! IF
		
        expfil.AddField('Current Date',1)
        expfil.AddField(FORMAT(TODAY(),@d06),,1)
		
        expfil.AddField('Location',1)
        expfil.AddField('Stock')
        expfil.AddField('Manufacturer')
        expfil.AddField('Model')
        expfil.AddField('Qty',,1)
		
        Access:SBO_GenericFile.ClearKey(sbogen:String2Key)
        sbogen:SessionID = p_web.SessionID
        sbogen:String2 = ''
        SET(sbogen:String2Key,sbogen:String2Key)
        LOOP UNTIL Access:SBO_GenericFile.Next() <> Level:Benign
            IF (sbogen:SessionID <> p_web.SessionID)
                BREAK
            END ! IF
			
            expfil.AddField(p_web.GSV('BookingSiteLocation'),1)
            expfil.AddField(p_web.GSV('locStockType'))
            expfil.AddField(sbogen:String2)
            expfil.AddField(sbogen:String1)
            expfil.AddField(sbogen:Long1,,1)
			
        END ! LOOP

        expfil.Kill()
		
        Relate:SBO_GenericFile.Close()
! ExchangeLoanDeviceSoldReport
ExchangeLoanDeviceSoldReport        PROCEDURE()
ReportDescription                       CSTRING(20)
ReportType                              CSTRING(20)
locRecordCount                          LONG()
locTotalRecords                         LONG()
locTotalPrice                           REAL()

locOrderDate                            DATE()
locOrderNumber                          LONG()
locInvoiceNumber                        LONG()
locPrice                                REAL

qSummary                                QUEUE(),PRE(qSummary)
InvoiceNumber                               LONG()
Location                                    STRING(30)
Manufacturer                                STRING(30)
ModelNumber                                 STRING(30)
Quantity                                    LONG()
DefaultPrice                                REAL()
Price                                       REAL()
                                        END ! IF

i                                       LONG()
    CODE
	
        ReportDescription = CHOOSE(p_web.GSV('Type') = 'E','Exchange','Loan')
        ReportType = CHOOSE(p_web.GSV('Kind') = 'D','Detailed','Summary')
		
        expfil.Init(ReportDescription & ' Device Sold Report (' & ReportType & ')')
		
		! Title
        expfil.AddField(ReportDescription & ' Device Sold Report',1,1)
        
        expfil.AddField('Start Date',1)
        expfil.AddField(FORMAT(p_web.GSV('locStartDate'),@d06),,1)
        
        expfil.AddField('End Date',1)
        expfil.AddField(FORMAT(p_web.GSV('locEndDate'),@d06),,1)
        
        expfil.AddField('Location',1)
        expfil.AddField(p_web.GSV('BookingSiteLocation'),,1)
        
        expfil.AddField('Date Created',1)
        expfil.AddField(FORMAT(TODAY(),@d06),,1)
        
        expfil.AddField(,1,1)
  
        expfil.AddField('Report Type',1)
        expfil.AddField(ReportType,,1)
        
        expfil.AddField(,1,1)
		
        IF (p_web.GSV('Kind') = 'D')
			! Detailed Title
            expfil.AddField('Invoice Number,Original Order Number,Date,Location,Description,Manufacturer,Model Number,Unit Price',1,1,,1)
        ELSE
			! Summary Title
            expfil.AddField('Invoice Number,Location,Manufacturer,Model Number,Quantity,Unit Price,Total Price',1,1,,1)
        END ! IF
        
        CASE p_web.GSV('Type')
        OF 'E' ! Exchange
            Relate:EXCHANGE.Open()
            Relate:EXCHHIST.Open()
            Relate:EXCHORNO.Open()
            Relate:MODELNUM.Open()
		
            ProgressBarWeb.Init(RECORDS(EXCHANGE))
            
            Access:EXCHANGE.ClearKey(xch:LocIMEIKey)
            xch:Location = p_web.GSV('BookingSiteLocation')
            SET(xch:LocIMEIKey,xch:LocIMEIKey)
            LOOP UNTIL Access:EXCHANGE.Next() <> Level:Benign
                IF (xch:Location <> p_web.GSV('BookingSiteLocation'))
                    BREAK
                END ! IF

                ProgressBarWeb.Update('Reading Exchange Units....')
                locOrderNumber = 0
                locOrderDate = 0
                locInvoiceNumber = 0
                locPrice = 0
				
                Access:EXCHHIST.ClearKey(exh:Ref_Number_Key)
                exh:Ref_Number = xch:Ref_Number
                exh:Date = TODAY()
                SET(exh:Ref_Number_Key,exh:Ref_Number_Key)
                LOOP UNTIL Access:EXCHHIST.Next() <> Level:Benign
                    IF (exh:Ref_Number <> xch:Ref_Number)
                        BREAK
                    END ! IF
                    IF (exh:Status = 'IN TRANSIT')
                        locOrderDate = exh:Date
                        invStart# = INSTRING('RETAIL INVOICE NUMBER',exh:Notes,1,1) + 23
                        priceStart# = INSTRING('PRICE',exh:Notes,1,1)
                        locOrderNumber = CLIP(SUB(exh:Notes,15,invStart# - 2))
                        locInvoiceNumber = SUB(exh:Notes,invStart#,priceStart# - invStart# - 2)
                        locPrice = DEFORMAT(SUB(exh:Notes,priceStart# + 8,30))	
                        BREAK							
                    END ! IF
					
                END ! LOOP
				
                IF (locOrderDate < p_web.GSV('locStartDate') OR locOrderDate > p_web.GSV('locEndDate'))
                    CYCLE
                END ! IF		
				
                IF (p_web.GSV('Kind') = 'D')
					! Export Detail
					
					! Invoice Number
                    expfil.AddField(locInvoiceNumber,1)
					! Original Order Number
                    expfil.AddField(locOrderNumber)
					! Date Franchise Placed The Order
                    Access:EXCHORNO.ClearKey(eno:LocationRecordKey)
                    eno:Location = p_web.GSV('BookingSiteLocation')
                    eno:RecordNumber = locOrderNumber
                    IF (Access:EXCHORNO.TryFetch(eno:LocationRecordKey) = Level:Benign)
                        expfil.AddField(FORMAT(eno:DateCreated,@d06))
						
                    ELSE ! IF
                        expfil.AddField()
						
                    END ! IF
					! Location
                    expfil.AddField(p_web.GSV('BookingSiteLocation'))
					! Description
                    expfil.AddField('''' & xch:ESN)
					! Manufacture
                    expfil.AddField(xch:Manufacturer)
					! Model Number
                    expfil.AddField(xch:Model_Number)
					! Unit Price
                    expfil.AddField(FORMAT(locPrice,@n_14.2),,1)
                END ! IF
				
                locRecordCount += 1
                locTotalRecords += 1
                locTotalPrice += locPrice
				
                Access:MODELNUM.ClearKey(mod:Manufacturer_Key)
                mod:Manufacturer = xch:Manufacturer
                mod:Model_Number = xch:Model_Number
                IF (Access:MODELNUM.TryFetch(mod:Manufacturer_Key) = Level:Benign)
					
                ELSE ! IF
                END ! IF
				
                IF (p_web.GSV('Kind') = 'S')
					! Build Summary
                    qSummary.InvoiceNumber = locInvoiceNumber
                    qSummary.Location = p_web.GSV('BookingSiteLocation')
                    qSummary.Manufacturer = xch:Manufacturer
                    qSummary.ModelNumber = xch:Model_Number
                    GET(qSummary,qSummary.InvoiceNumber,qSummary.Location,qSummary.Manufacturer,qSummary.ModelNumber)
                    IF (ERROR())
                        qSummary.Quantity = 1
                        qSummary.Price = locPrice
                        qSummary.DefaultPrice = mod:ExchReplaceValue
                        ADD(qSummary)
                    ELSE
                        qSummary.Quantity += 1
                        qSummary.Price += locPrice
                        PUT(qSummary)
                    END ! IF
                END ! IF
				
            END ! LOOP
            Relate:MODELNUM.Close()
            Relate:EXCHHIST.Close()
            Relate:EXCHANGE.Close()
            Relate:EXCHORNO.Close()
        OF 'L' ! Loan
            Relate:LOAN.Open()
            Relate:LOANHIST.Open()
            Relate:LOANORNO.Open()
            Relate:MODELNUM.Open()
			
            ProgressBarWeb.Init(RECORDS(LOAN))	
            
            Access:LOAN.ClearKey(loa:LocIMEIKey)
            loa:Location = p_web.GSV('BookingSiteLocation')
            SET(loa:LocIMEIKey,loa:LocIMEIKey)
            LOOP UNTIL Access:LOAN.Next() <> Level:Benign
                IF (loa:Location <> p_web.GSV('BookingSiteLocation'))
                    BREAK
                END ! IF
				
                ProgressBarWeb.Update('Reading Loan Units....')
				locOrderNumber = 0
                Access:LOANHIST.ClearKey(loh:Ref_Number_Key)
                loh:Ref_Number = loa:Ref_Number
                loh:Date = TODAY()
                SET(loh:Ref_Number_Key,loh:Ref_Number_Key)
                LOOP UNTIL Access:LOANHIST.Next() <> Level:Benign
                    IF (loh:Ref_Number <> loa:Ref_Number)
                        BREAK
                    END ! IF
                    IF (loh:Status = 'IN TRANSIT' AND locOrderNumber = 0)
                        locOrderDate = loh:Date
                        invStart# = INSTRING('RETAIL INVOICE NUMBER',loh:Notes,1,1) + 23
                        priceStart# = INSTRING('PRICE',loh:Notes,1,1)
                        locOrderNumber = CLIP(SUB(loh:Notes,15,invStart# - 2))
                        locInvoiceNumber = SUB(loh:Notes,invStart#,priceStart# - invStart# - 2)
                        locPrice = DEFORMAT(SUB(loh:Notes,priceStart# + 8,30))	
                        BREAK							
                    END ! IF					
                END ! LOOP
				
                IF (locOrderDate < p_web.GSV('locStartDate') OR locOrderDate > p_web.GSV('locEndDate'))
                    CYCLE
                END ! IF	

                IF (p_web.GSV('Kind') = 'D')
					! Export Detail
					
					! Invoice Number
                    expfil.AddField(locInvoiceNumber,1)
					! Original Order Number
                    expfil.AddField(locOrderNumber)
					! Date Franchise Placed The Order
                    Access:LOANORNO.ClearKey(lno:LocationRecordKey)
                    lno:Location = p_web.GSV('BookingSiteLocation')
                    lno:RecordNumber = locOrderNumber
                    IF (Access:LOANORNO.TryFetch(lno:LocationRecordKey) = Level:Benign)
                        expfil.AddField(FORMAT(lno:DateCreated,@d06))
						
                    ELSE ! IF
                        expfil.AddField()
						
                    END ! IF
					! Location
                    expfil.AddField(p_web.GSV('BookingSiteLocation'))
					! Description
                    expfil.AddField('''' & loa:ESN)
					! Manufacture
                    expfil.AddField(loa:Manufacturer)
					! Model Number
                    expfil.AddField(loa:Model_Number)
					! Unit Price
                    expfil.AddField(FORMAT(locPrice,@n_14.2),,1)
                END ! IF
				
                locRecordCount += 1
                locTotalRecords += 1
                locTotalPrice += locPrice
				
                IF (p_web.GSV('Kind') = 'S')
					! Build Summary
                    Access:MODELNUM.ClearKey(mod:Manufacturer_Key)
                    mod:Manufacturer = loa:Manufacturer
                    mod:Model_Number = loa:Model_Number
                    IF (Access:MODELNUM.TryFetch(mod:Manufacturer_Key) = Level:Benign)
						
                    ELSE ! IF
                    END ! IF
					
                    qSummary.InvoiceNumber = locInvoiceNumber
                    qSummary.Location = p_web.GSV('BookingSiteLocation')
                    qSummary.Manufacturer = loa:Manufacturer
                    qSummary.ModelNumber = loa:Model_Number
                    GET(qSummary,qSummary.InvoiceNumber,qSummary.Location,qSummary.Manufacturer,qSummary.ModelNumber)
                    IF (ERROR())
                        qSummary.Quantity = 1
                        qSummary.Price = locPrice
                        qSummary.DefaultPrice = mod:ExchReplaceValue
                        ADD(qSummary)
                    ELSE
                        qSummary.Quantity += 1
                        qSummary.Price += locPrice
                        PUT(qSummary)
                    END ! IF
                END ! IF
            END ! LOOP
            Relate:LOANORNO.Close()
            Relate:LOANHIST.Close()
            Relate:LOAN.Close()
            Relate:MODELNUM.Close()
        END ! CASE
		
        IF (p_web.GSV('Kind') = 'S')
            LOOP i = 1 TO RECORDS(qSummary)
                GET(qSummary,i)
                ProgressBarWeb.Update('Exporting....')
				! Invoice Number
                expfil.AddField(qSummary.InvoiceNumber,1)
				! Location
                expfil.AddField(qSummary.Location)
				! Manufacturer
                expfil.AddField(qSummary.Manufacturer)
				! Model Number
                expfil.AddField(qSummary.ModelNumber)
				! Quantity
                expfil.AddField(qSummary.Quantity)
				! Default Price
                expfil.AddField(FORMAT(qSummary.DefaultPrice,@n_14.2))
				! Total Price
                expfil.AddField(FORMAT(qSummary.Price,@n_14.2),,1)
				
            END ! LOOP
			
            expfil.AddField('Totals:,' & RECORDS(qSummary) & ',,,' & locTotalRecords & ',,' & FORMAT(locTotalPrice,@n_14.2),1,1,,1)
            
        ELSE
            expfil.AddField('Total Records,' & locTotalRecords,1,1,,1)
        END ! IF
		
		
        IF (locRecordCount = 0)
            expfil.Kill(1)
            CreateScript(packet,kNoRecordsAlert)
            DO ReturnFail
        ELSE
            expfil.Kill()
        END ! IF		
     
! ExchangeLoanUnitsReturnedToMainStore
ExchangeLoanUnitsReturnedToMainStore        PROCEDURE()	
reportFileName	CSTRING(60)
reportDescription CSTRING(60)
recordCount LONG()
locReceivedDate     DATE()
locWhoReceived      STRING(3)
locPRice            REAL()
	CODE
		CASE p_web.GSV('Kind')
		OF 'E'
			reportDescription = 'Exchange'
		OF 'L'
			reportDescription = 'Loan'
		END ! CASE
		
		CASE p_web.GSV('ReportType')
		OF '1'
			reportFileName = reportDescription & ' Received'
		OF '0'
			reportFileName = reportDescription & ' In Transit'
		END ! CASE
		
		reportDescription = CLIP(reportDescription) & ' Units Returned To Main Store'
		expfil.Init(reportDescription,reportFileName)
		
		expfil.AddField(reportDescription,1,1)
		
		expfil.AddField('Start Date',1)
		expfil.AddField(FORMAT(p_web.GSV('locStartDate'),@d06),,1)
		
		expfil.AddField('End Date',1)
		expfil.AddField(FORMAT(p_web.GSV('locEndDate'),@d06),,1)
		
		expfil.AddField('Location',1)
		expfil.AddField(p_web.GSV('BookingSiteLocation'),,1)
		
		expfil.AddField('Date Created',1)
		expfil.AddField(FORMAT(TODAY(),@d06),,1)
		
		expfil.AddField(,1,1)
		
		expfil.AddField('Report Type',1)
		
		CASE p_web.GSV('ReportType')
		OF '1'
			expfil.AddField('Received',,1)
		OF '0'
			expfil.AddField('In Transit',,1)
		END ! CASE
		
		expfil.AddField(,1,1)
		
		! Detail Title
		CASE p_web.GSV('Kind')
		OF 'L'!
			expfil.AddField('Model Number',1)
		OF 'E'
			expfil.AddField('Part Number',1)
		END ! CASE
		expfil.AddField('Description')
		expfil.AddField('Location')
		expfil.AddField('Credit Note Request No')
		expfil.AddField('Invoice Number')
		expfil.AddField('Original Order No')
		expfil.AddField('Received Date')
		expfil.AddField('Received By')
		expfil.AddField('Manufacturer')
		CASE p_web.GSV('Kind')
		OF 'L'!
			expfil.AddField('Model')
			expfil.AddField('Price',,1)
			
		OF 'E'
			CASE p_web.GSV('ReportType')
			OF '0'
				expfil.AddField('Model Number',,1)
				
			OF '1'
				expfil.AddField('Model Number')
				expfil.AddField('Accepted / Rejected',,1)
				
			END ! CASE
		END ! CASE		
		
		Relate:RTNORDER.Open()
		Relate:LOAN.Open()
		Relate:EXCHANGE.Open()
		Relate:LOANHIST.Open()
		Relate:USERS.Open()
		Relate:STOCK.Open()
		Relate:EXCHHIST.Open()
		Relate:RTNAWAIT.Open()
		
		recordCount = 0
		Access:RTNORDER.ClearKey(rtn:DateOrderedReceivedKey)
		rtn:Location = p_web.GSV('BookingSiteLocation')
		rtn:Received = p_web.GSV('ReportType')
		rtn:DateOrdered = p_web.GSV('locStartDate')
		SET(rtn:DateOrderedReceivedKey,rtn:DateOrderedReceivedKey)
		LOOP UNTIL Access:RTNORDER.Next() <> Level:Benign
			IF (rtn:Location <> p_web.GSV('BookingSiteLocation') OR |
				rtn:Received <> p_web.GSV('ReportType') OR |
				rtn:DateOrdered > p_web.GSV('locEndDate'))
				BREAK
			END ! IF
			
			CASE p_web.GSV('Kind')
			OF 'L'
				IF (rtn:ExchangeOrder <> 2)
					CYCLE
				END ! IF
				Access:LOAN.ClearKey(loa:Ref_Number_Key)
				loa:Ref_Number = rtn:RefNumber
				IF (Access:LOAN.TryFetch(loa:Ref_Number_Key))
					CYCLE
				END ! IF
				
				! Export
				! Model Number
				expfil.AddField(loa:Model_Number,1)
				! Description
				expfil.AddField('`' & loa:ESN)
				! Location
				expfil.AddField(p_web.GSV('BookingSiteLocation'))
				! Create Note Request Number
				expfil.AddField(rtn:CreditNoteRequestNumber)
				! Invoice Number
				expfil.AddField(rtn:InvoiceNumber)
				! Original Order Number
				expfil.AddField(rtn:OrderNumber)
				! Received Date
				locReceivedDate = ''
				locWhoReceived = ''
				
				Access:LOANHIST.ClearKey(loh:Ref_Number_Key)
				loh:Ref_Number = loa:Ref_Number
				loh:Date = TODAY()
				SET(loh:Ref_Number_Key,loh:Ref_Number_Key)
				LOOP UNTIL Access:LOANHIST.Next() <> Level:Benign
					IF (loh:Ref_Number <> loa:Ref_Number OR |
						loh:Date > TODAY())
						BREAK
					END ! IF
					IF (loh:Status = 'UNIT RECEIVED')
						IF (INSTRING('RETAIL INVOICE NUMBER: ' & CLIP(rtn:InvoiceNumber),UPPER(loh:Notes),1,1))
							locReceivedDate = loh:Date
							locWhoReceived = loh:USer
							BREAK
						END ! IF
					END ! IF
				END ! LOOP
				
				expfil.AddField(FORMAT(locReceivedDate,@d06))
				! Received By
				Access:USERS.ClearKey(use:User_Code_Key)
				use:User_Code = locWhoReceived
				IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
					expfil.AddField(CLIP(use:Forename) & ' ' & CLIP(use:Surname))
					
				ELSE ! IF
					expfil.AddField()
					
				END ! IF
				! Manufacturer
				expfil.AddField(loa:Manufacturer)
				! Model
				expfil.AddField(loa:Model_Number)
				! Price
				locPrice = 0
				Access:LOANHIST.Clearkey(loh:Ref_Number_Key)
				loh:Ref_Number = loa:Ref_Number
				loh:Date        = TODAY()
				SET(loh:Ref_Number_Key,loh:Ref_Number_Key)
				LOOP UNTIL Access:LOANHIST.Next()
					IF (loh:Ref_Number <> loa:Ref_Number)
						BREAK
					END
					IF (loh:Status = 'IN TRANSIT')
						invStart# = INSTRING('RETAIL INVOICE NUMBER',loh:Notes,1,1) + 23
						priceStart# = INSTRING('PRICE',loh:Notes,1,1)
						locPrice = DEFORMAT(SUB(loh:Notes,priceStart# + 8,30))
					END
				END		
				expfil.AddField(FORMAT(locPrice,@n14.2),,1)
				
				
			OF 'E'
				IF (rtn:ExchangeOrder <> 1)
					CYCLE
				END ! IF
				Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
				xch:Ref_Number = rtn:RefNumber
                IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key))
					CYCLE
                END ! IF
                
                BHAddToDebugLog('Report Type: ' & p_web.GSV('ReportType'))
				
				! Export
				! Part Number
				Access:STOCK.ClearKey(sto:ExchangeModelKey)
				sto:Location = p_web.GSV('BookingSiteLocation')
				sto:Manufacturer = xch:Manufacturer
				sto:ExchangeModelNumber = xch:Model_Number
				IF (Access:STOCK.TryFetch(sto:ExchangeModelKey) = Level:Benign)
					expfil.AddField(sto:Part_Number,1)
					
				ELSE ! IF
					expfil.AddField(,1)
					
				END ! IF
				! Description
				expfil.AddField('`' & xch:ESN)
				! Location
				expfil.AddField(p_web.GSV('BookingSiteLocation'))
				! Credit Note Request No
				expfil.AddField(rtn:CreditNoteRequestNumber)
				! Invoice Number
				expfil.AddField(rtn:InvoiceNumber)
				! Order Number
				expfil.AddField(rtn:OrderNumber)
				! Received Date
				locReceivedDate = 0
				locWhoReceived = ''
				Access:EXCHHIST.Clearkey(exh:Ref_Number_Key)
				exh:Ref_Number = xch:Ref_Number
				exh:Date = TODAY()
				SET(exh:Ref_Number_Key,exh:Ref_Number_Key)
				LOOP UNTIL Access:EXCHHIST.Next()
					IF (exh:Ref_Number <> xch:ref_Number OR |
						exh:Date > TODAY())
						BREAK
					END
					IF (exh:Status = 'UNIT RECEIVED')
						IF (INSTRING('RETAIL INVOICE NUMBER: ' & CLIP(rtn:InvoiceNumber),UPPER(exh:Notes),1,1))
							locReceivedDate = exh:Date
							locWhoReceived = exh:User
							BREAK
						END
					END
				END			
				expfil.AddField(FORMAT(locReceivedDate,@d06))
				! Received By
				Access:USERS.ClearKey(use:User_Code_Key)
				use:User_Code = locWhoReceived
				IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
					expfil.AddField(CLIP(use:Forename) & ' ' & CLIP(use:Surname))
					
				ELSE ! IF
					expfil.AddField()
					
				END ! IF
				! Manufacturer
				expfil.AddField(xch:Manufacturer)
				CASE p_web.GSV('ReportType')
				OF '0'
					! Model Number
					expfil.AddField(xch:Model_Number,,1)
					
				OF '1'
					! Model Number
					expfil.AddField(xch:Model_Number)
					! Accept / Reject
					Access:RTNAWAIT.ClearKey(rta:RTNORDERKey)
					rta:RTNRecordNumber = rtn:RecordNumber
					IF (Access:RTNAWAIT.TryFetch(rta:RTNORDERKey) = Level:Benign)
						expfil.AddField(rta:AcceptReject,,1)
						
					ELSE ! IF
						expfil.AddField(,,1)
						
					END ! IF
				END ! CASE
				recordCount += 1
			END ! CASE
			
		END ! LOOP
		
		Relate:EXCHHIST.Close()
		Relate:RTNORDER.Close()
		Relate:EXCHANGE.Close()
		Relate:LOAN.Close()
		Relate:LOANHIST.Close()
		Relate:USERS.Close()
		Relate:STOCK.Close()
		Relate:RTNAWAIT.Close()
		
		IF (recordCount = 0)
			CreateScript(packet,kNoRecordsAlert)
			expfil.Kill(1)
			DO ReturnFail
		ELSE
			expfil.Kill()
		END ! IF
! ExchangePhoneExport
ExchangePhoneExport PROCEDURE()!
countTagged LONG()
countRecords LONG()
taggedStockType STRING(30)
    CODE
		rtn# = expfil.Init('Exchange Phone Export')
		
		expfil.AddField('Exchange Phone Export',1,1)
		
		expfil.AddField('Location',1)
		expfil.AddField(p_web.GSV('BookingSiteLocation'),,1)
		
		expfil.AddField('Stock Type',1)
		
        Relate:TagFile.Open()	
        
		IF (p_web.GSV('locAllStockTypes') <> 1)
			Access:TagFile.ClearKey(tag:KeyTagged)
			tag:SessionID = p_web.SessionID
			SET(tag:KeyTagged,tag:KeyTagged)
			LOOP UNTIL Access:TagFile.Next() <> Level:Benign
				IF (tag:SessionID <> p_web.SessionID)
					BREAK
				END ! IF
				IF (tag:Tagged = 0)
					CYCLE
				END ! IF
				countTagged += 1
				taggedStockType = tag:TaggedValue
			END ! LOOP
			
			IF (countTagged = 0)
				CreateScript(packet,'alert("You have not tagged any stock types.")')
                expfil.Kill(1)
                Relate:TagFile.Close()	
				DO ReturnFail
			END ! IF
			
			ProgressBarWeb.Init(countTagged)
		ELSE ! IF
			ProgressBarWeb.Init(RECORDS(STOCKTYP))
		END ! IF
		
		IF (countTagged = 1)
			expfil.AddField(tag:TaggedValue,,1)
		ELSE ! IF
			expfil.AddField('Multiple',,1)
		END ! IF
		
		expfil.AddField(,1,1)
		
		expfil.AddField('Start Date',1)
		expfil.AddField(FORMAT(p_web.GSV('locStartDate'),@d06),,1)
		expfil.AddField('End Date',1)
		expfil.AddField(FORMAT(p_web.GSV('locEndDate'),@d06),,1)
		
		expfil.AddField('Date Created',1)
		expfil.AddField(FORMAT(TODAY(),@d06),,1)
		
		expfil.AddField(,1,1)
		
		expfil.AddField('Location',1)
		expfil.AddField('I.M.E.I. Number')
		expfil.AddField('M.S.N.')
		expfil.AddField('Manufacturer')
		expfil.AddField('Model Number')
		expfil.AddField('Stock Type')
		expfil.AddField('Unit Number')
		expfil.AddField('Unit Status')
		expfil.AddField('Status Date')
		expfil.AddField('Reason Not Available')
		expfil.AddField('Job Number',,1)
		
		Relate:EXCHANGE.Open()
		
		Relate:EXCHHIST.Open()
		Relate:JOBS.Open()
				
		Access:EXCHANGE.ClearKey(xch:LocStatusChangeDateKey)
		xch:Location = p_web.GSV('BookingSiteLocation')
		xch:StatusChangeDate = p_web.GSV('locStartDate')
		SET(xch:LocStatusChangeDateKey,xch:LocStatusChangeDateKey)
		LOOP UNTIL Access:EXCHANGE.Next() <> Level:Benign
			IF (xch:Location <> p_web.GSV('BookingSiteLocation') OR |
				xch:StatusChangeDate > p_web.GSV('locEndDate'))
				BREAK
			END ! IF
			
			ProgressBarWeb.Update('Exporting....')
			
			IF (p_web.GSV('locAllStockTypes') <> 1)
				Access:TagFile.ClearKey(tag:KeyTagged)
				tag:SessionID = p_web.SessionID
				tag:TaggedValue	= xch:Stock_Type
				IF (Access:TagFile.TryFetch(tag:KeyTagged) = Level:Benign)
					IF (tag:Tagged <> 1)
						CYCLE
					END ! IF
				ELSE ! IF
					CYCLE
				END ! IF
			END ! IF
			
			IF (p_web.GSV('locAllStatus') <> 1)
				IF (p_web.GSV('loc' & CLIP(xch:Available)) <> 1)
					CYCLE
				END ! IF
			END ! IF
			
			! Location
			expfil.AddField(p_web.GSV('BookingSiteLocation'),1)
			! IMEI Number
			expfil.AddField('`' & xch:ESN)
			! MSN
			IF (xch:MSN <> '' OR xch:MSN = 'N/A')
				expfil.AddField('`' & xch:MSN)
			ELSE
				expfil.AddField()
			END ! IF
			! Manufacturer
			expfil.AddField(xch:Manufacturer)
			! Model Number
			expfil.AddField(xch:Model_Number)
			! Stock Type
			expfil.AddField(xch:Stock_Type)
			! Unit Number
			expfil.AddField(xch:Ref_Number)
			! Unit Status
			expfil.AddField(VodacomClass.GetExchangeStatus(xch:Available,xch:Job_Number))
			! Status Date
			expfil.AddField(FORMAT(xch:StatusChangeDate,@d06))
			! Reason Not Available
			IF (xch:Available = 'NOA')
				found# = 0
				Access:EXCHHIST.ClearKey(exh:Ref_Number_Key)
				exh:Ref_Number = xch:Ref_Number
				exh:Date = TODAY()
				SET(exh:Ref_Number_Key,exh:Ref_Number_Key)
				LOOP UNTIL Access:EXCHHIST.Next() <> Level:Benign
					IF (exh:Ref_Number <> xch:Ref_Number OR |
						exh:Date > TODAY())
						BREAK
					END ! IF
					found# = 1
					expfil.AddField(exh:Status)
					BREAK
				END ! LOOP
				IF (found# = 0)
					expfil.AddField()
				END ! IF
			ELSE
				expfil.AddField()
			END ! IF
			! Job Number
			Access:JOBS.ClearKey(job:Ref_Number_Key)
			job:Ref_Number = xch:Job_Number
			IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
				IF (job:Exchange_Unit_Number = xch:Ref_Number)
					expfil.AddField(xch:Job_Number,,1)
					
				ELSE ! IF
					expfil.AddField(,,1)
					
				END!  IF
			ELSE ! IF
				expfil.AddField(,,1)
				
			END ! IF
			
			countRecords += 1
		END ! LOOP
		
		expfil.AddField('Totals',1)
		expfil.AddField(countRecords,,1)
		
		Relate:TagFile.Close()
		Relate:EXCHANGE.Close()
		Relate:EXCHHIST.Close()		
		Relate:JOBS.Close()
		
		expfil.Kill()		
! ExchangeStockValuationExport
ExchangeStockValuationExport        PROCEDURE()!
countTagged LONG()
totalRecords LONG()
totalQuantity LONG()
taggedStockType STRING(30)
totalValue REAL
totalTotal REAL
	MAP
Export	PROCEDURE(STRING pStockType)
	END ! MAP
    CODE
        expfil.Init('Exchange Stock Valuation')
		
        Relate:TagFile.Open()	
        
		IF (p_web.GSV('locAllStockTypes') <> 1)
			Access:TagFile.ClearKey(tag:KeyTagged)
			tag:SessionID = p_web.SessionID
			SET(tag:KeyTagged,tag:KeyTagged)
			LOOP UNTIL Access:TagFile.Next() <> Level:Benign
				IF (tag:SessionID <> p_web.SessionID)
					BREAK
				END ! IF
				IF (tag:Tagged = 0)
					CYCLE
				END ! IF
				countTagged += 1
				taggedStockType = tag:TaggedValue
			END ! LOOP
			
			IF (countTagged = 0)
				CreateScript(packet,'alert("You have not tagged any stock types.")')
                expfil.Kill(1)
                Relate:TagFile.Close()	
				DO ReturnFail
			END ! IF
			
			ProgressBarWeb.Init(countTagged)
		ELSE ! IF
			ProgressBarWeb.Init(RECORDS(STOCKTYP))
		END ! IF
		
		expfil.AddField('Exchange Stock Valuation Report',1,1)
		
		expfil.AddField('Location',1)
		expfil.AddField(p_web.GSV('BookingSiteLocation'),,1)
		
		expfil.AddField('Stock Type',1)
		IF (countTagged = 1)
			expfil.AddField(tag:TaggedValue,,1)
		ELSE ! IF
			expfil.AddField('Multiple',,1)
		END ! IF

		expfil.AddField('Date Created',1)
		expfil.AddField(FORMAT(TODAY(),@d06),,1)
		
		expfil.AddField(,1,1)
		
		expfil.AddField('Location',1)
		expfil.AddField('Stock Type')
		expfil.AddField('Part Number')
		expfil.AddField('Manufacturer')
		expfil.AddField('Model Number')
		expfil.AddField('Quantity')
		expfil.AddField('Value')
		expfil.AddField('Total',,1)
		
		Relate:STOCKTYP.Open()
		Relate:EXCHANGE.Open()
		Relate:MODELNUM.Open()
		Relate:STOCK.Open()
		
		
        IF (p_web.GSV('locAllStockTypes') <> 1)
            Access:TagFile.ClearKey(tag:KeyTagged)
            tag:SessionID = p_web.SessionID
            SET(tag:KeyTagged,tag:KeyTagged)
            LOOP UNTIL Access:TagFile.Next() <> Level:Benign
                IF (tag:SessionID <> p_web.SessionID)
                    BREAK
                END ! IF
                IF (tag:tagged = 0)
                    CYCLE
                END ! IF
			
                Export(tag:taggedValue)
			
            END ! LOOP
        ELSE ! IF
            Access:STOCKTYP.ClearKey(stp:Use_Exchange_Key)
            stp:Use_Exchange = 'YES'
            SET(stp:Use_Exchange_Key,stp:Use_Exchange_Key)
            LOOP UNTIL Access:STOCKTYP.Next() <> Level:Benign
                IF (stp:Use_Exchange <> 'YES')
                    BREAK
                END ! IF
                Export(stp:Stock_Type)
            END ! LOOP
        END ! IF		
		
		expfil.AddField('Totals',1)
		expfil.AddField(totalRecords)
		expfil.AddField()
		expfil.AddField()
		expfil.AddField()
		expfil.AddField(totalQuantity)
		expfil.AddField(FORMAT(totalValue,@n_14.2))
		expfil.AddField(FORMAT(totalTotal,@n_14.2),,1)
		
		Relate:TagFile.Close()
		Relate:STOCKTYP.Close()
		Relate:EXCHANGE.Close()
		Relate:MODELNUM.Close()
        Relate:STOCK.Close()
        IF (totalRecords = 0)
            expfil.Kill(1)
            CreateScript(packet,kNoREcordsAlert)
            DO ReturnFail
        ELSE
            expfil.Kill()
        END ! IF
		
Export	PROCEDURE(STRING pStockType)
qSummary		QUEUE(),PRE(qSummary)
Manufacturer STRING(30)
ModelNumber STRING(30)
ForP STRING(30)
Quantity LONG()
				END ! QUEUE
i LONG()				
	CODE
		Access:EXCHANGE.ClearKey(xch:LocStockAvailModelKey)
		xch:Location = p_web.GSV('BookingSiteLocation')
		xch:Stock_Type = pStockType
		xch:Available = 'AVL'
		SET(xch:LocStockAvailModelKey,xch:LocStockAvailModelKey)
		LOOP UNTIL Access:EXCHANGE.Next() <> Level:Benign
			IF (xch:Location <> p_web.GSV('BookingSiteLocation') OR |
				xch:Stock_Type <> pStockType OR |
				xch:Available <> 'AVL')
				BREAK
			END ! IF
			
			ProgressBarWeb.Update('Exporting....')
			
			qSummary.Manufacturer = xch:Manufacturer
			qSummary.ModelNumber = xch:Model_Number
			qSummary.ForP = xch:FreeStockPurchased
			GET(qSummary,qSummary.Manufacturer,qSummary.ModelNumber,qSummary.ForP)
			IF NOT (ERROR())
				qSummary.Quantity += 1
				PUT(qSummary)
			ELSE ! IF
				qSummary.Quantity = 1
				ADD(qSummary)
			END ! IF
			
		END ! LOOP
		
		LOOP i = 1 TO RECORDS(qSummary)
			GET(qSummary,i)
			
			ProgressBarWeb.Update('Exporting....')
			
			! Location
			expfil.AddField(p_web.GSV('BookingSiteLocation'),1)
			! Stock Type
			expfil.AddField(pStockType)
			! Part Number
			Access:STOCK.ClearKey(sto:ExchangeModelKey)
			sto:Location = 'MAIN STORE'
			sto:Manufacturer = qSummary.Manufacturer
			sto:ExchangeModelNumber = qSummary.ModelNumber
			IF (Access:STOCK.TryFetch(sto:ExchangeModelKey) = Level:Benign)
				expfil.AddField()
				
			ELSE ! IF
				expfil.AddField(sto:Part_Number)
				
			END ! IF
			! Manufacturer
			expfil.AddField(qSummary.Manufacturer)
			! Model Number
			expfil.AddField(qSummary.ModelNumber)
			! Quantity
			expfil.AddField(qSummary.Quantity)
			! Value
			! Total
			Access:MODELNUM.ClearKey(mod:Manufacturer_Key)
			mod:Manufacturer = qSummary.Manufacturer
			mod:Model_Number = qSummary.ModelNumber
			IF (Access:MODELNUM.TryFetch(mod:Manufacturer_Key) = Level:Benign)
				expfil.AddField(FORMAT(mod:ExchReplaceValue,@n_14.2))
				expfil.AddField(FORMAT(mod:ExchReplaceValue * qSummary.Quantity,@n_14.2),,1)
				totalValue += mod:ExchReplaceValue
				totalTotal += mod:ExchReplaceValue * qSummary.Quantity
			ELSE ! IF
				expfil.AddField()
				expfil.AddField(,,1)
				
			END ! IF
			
			totalRecords += 1
			totalQuantity += qSummary.Quantity
		
		END ! LOOP
! ExportWarrantyClaims
ExportWarrantyClaims      PROCEDURE()
firstLine               LONG(0)
recordCount             LONG(0)

    CODE
        Relate:JOBSWARR.Open()
        Relate:JOBS.Open()
        Relate:WEBJOB.Open()
        Relate:SBO_WarrantyClaims.Open()

        ProgressBarWeb.Init(p_web.GSV('locTotalJobsAvailable'),50)
		
        CASE p_web.GSV('locEDIType')
        OF 'NO'
            expfil.Init('Pending Claims Export')            
        OF 'APP'
            expfil.Init('Approved Claims Export')
        OF 'EXC'
            expfil.Init('Rejected Claims Export')
        OF 'AAJ'
            expfil.Init('Accepted Rejected Claims Export')
        OF 'REJ'
            expfil.Init('Final Rejection Claims Export')
        OF 'PAY'            
            expfil.Init('Paid Claims Export')
        END ! CASE
        
        Access:SBO_WarrantyClaims.ClearKey(sbojow:RRCStatusKey)
        sbojow:SessionID = p_web.SessionID
        sbojow:RRCStatus = p_web.GSV('locEDIType')
        IF (p_web.GSV('locWarrantyManufacturerFilter') = 1)
            sbojow:Manufacturer = p_web.GSV('locWarrantyManufacturer')
            SET(sbojow:RRCStatusManufacturerKey,sbojow:RRCStatusManufacturerKey)
        ELSE ! IF
            SET(sbojow:RRCStatusKey,sbojow:RRCStatusKey)
        END ! IF
        LOOP UNTIL Access:SBO_WarrantyClaims.Next() <> Level:Benign
            IF (sbojow:SessionID <> p_web.SessionID OR |
                sbojow:RRCStatus <> p_web.GSV('locEDIType'))
                BREAK
            END ! IF
            IF (p_web.GSV('locWarrantyManufacturerFilter') = 1)
                IF (sbojow:Manufacturer <> p_web.GSV('locWarrantyManufacturer'))
                    BREAK
                END ! IF
            END ! IF
            
            Access:JOBSWARR.ClearKey(jow:RefNumberKey)
            jow:RefNumber = sbojow:JobNumber
            IF (Access:JOBSWARR.TryFetch(jow:RefNumberKey))
                ! Shouldn't fail
                CYCLE
            END ! IF
            
!            CASE p_web.GSV('locEDIType')
!            OF 'NO'
!                IF (jow:ClaimSubmitted < p_web.GSV('locWarrantyStartDate') Or jow:ClaimSubmitted > p_web.GSV('locWarrantyEndDate'))
!                    CYCLE
!                END
!            OF 'APP'
!                IF (jow:DateAccepted < p_web.GSV('locWarrantyStartDate') Or jow:DateAccepted > p_web.GSV('locWarrantyEndDate'))
!                    CYCLE
!                END
!            OF 'EXC'
!                IF (jow:DateRejected < p_web.GSV('locWarrantyStartDate') Or jow:DateRejected > p_web.GSV('locWarrantyEndDate'))
!                    CYCLE
!                END
!            OF 'AAJ' OROF 'REJ'
!                IF (jow:DateFinalRejection < p_web.GSV('locWarrantyStartDate') Or jow:DateFinalRejection > p_web.GSV('locWarrantyEndDate'))
!                    CYCLE
!                END
!            OF 'PAY'
!                IF (jow:RRCDateReconciled < p_web.GSV('locWarrantyStartDate') Or jow:RRCDateReconciled > p_web.GSV('locWarrantyEndDate'))
!                    CYCLE
!                END
!            END ! CASE

            ProgressBarWeb.Update('Exporting...')

            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = jow:RefNumber
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
            END ! IF
            Access:WEBJOB.ClearKey(wob:RefNumberKey)
            wob:RefNumber = job:Ref_Number
            IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
            END ! IF
            
            IF (firstLine = 0)
                expfil.AddField('Job Number,Account Number,Company Name,IMEI Number,Manufacturer,Current Status',1,,,1)
                Case p_web.GSV('locEDIType')
                Of 'NO' ! Pending
                    expfil.AddField('Location,Submitted',,1,,1)
                of 'APP' ! Approved
                    expfil.AddField('Location,Approved',,1,,1)
                of 'EXC' ! Rejection
                    expfil.AddField('Rejected,Submissions,Days To Final Rejection,Rejection Reason,Location',,1,,1)
                of 'AAJ' orof 'REJ' ! Accepted / Final Rejection
                    expfil.AddField('Rejection,Rejection Reason,Location',,1,,1)
                of 'PAY' ! Paid
                    expfil.AddField('Location,Reconciled',,1,,1)
                End
                
                firstLine = 1
            END ! IF
            
            ! Job Number
            expfil.AddField(jow:RefNumber,1)
            ! Account Number
            expfil.AddField(job:Account_Number)
            ! Company Name
            expfil.AddField(job:Company_Name)
            ! IMEI Number
            expfil.AddField('`' & clip(job:ESN))
            ! Manufacturer
            expfil.AddField(jow:Manufacturer)
            ! Current Status
            if (job:Exchange_Unit_Number > 0)
                expfil.AddField('E ' & clip(wob:Exchange_Status))
            else ! if (job:Exchange_Unit_Number > 0)
                expfil.AddField('J ' & clip(wob:Current_Status))
            end ! if (job:Exchange_Unit_Number > 0)        
            
            Case p_web.GSV('locEDIType')
            Of 'NO' ! Pending
                ! Location
                expfil.AddField(job:Location)
                ! Submitted
                expfil.AddField(FORMAT(jow:ClaimSubmitted,@d06b))
            Of 'APP' ! Accepted
                ! Location
                expfil.AddField(job:Location)
                ! Accepted
                expfil.AddField(format(jow:DateAccepted,@d06b))
            of 'EXC' !Rejected
                locRejectionReason = ''

                Do GetRejectionReason_EXC
                ! Rejected
                expfil.AddField(format(jow:DateRejected,@d06b))
                ! Submissions
                expfil.AddField(locSubmissions)
                ! Days To Final Rejection
                expfil.AddField(locDaysToFinalRejection)
                ! Rejection Reason
                expfil.AddField(BHStripAlphaNumOnly(locRejectionReason))
                ! Location
                expfil.AddField(job:Location)
            Of 'AAJ' ! Rejection Aknowledged
                Do GetRejectionReason_AAJ
                ! Final Rejection
                expfil.AddField(format(jow:DateFinalRejection,@d06b))
                ! Rejection Reason
                expfil.AddField(BHStripAlphaNumOnly(locRejectionReason))
                ! Location
                expfil.AddField(job:Location)
            of 'REJ' ! Final Reject
            
                Do GetRejectionReason_REJ

                ! Final Rejection
                expfil.AddField(format(jow:DateFinalRejection,@d06b))
                ! Rejection Reason
                expfil.AddField(BHStripAlphaNumOnly(locRejectionReason))
                ! Location
                expfil.AddField(job:Location)
            Of 'PAY' ! Pay
                ! Location
                expfil.AddField(job:Location)
                ! Submitted
                expfil.AddField(format(jow:RRCDateReconciled,@d06b))

            End ! Case tmp:EDI
            
            expfil.AddField(,,1)
            
            recordCount += 1
            
        END ! LOOP
        
        Relate:JOBSWARR.Close()
        Relate:JOBS.Close()
        Relate:WEBJOB.Close()
        Relate:SBO_WarrantyClaims.Close()
        
        
        IF (recordCount = 0)
            expfil.Kill(1)
            CreateScript(packet,kNoREcordsAlert)
            DO ReturnFail
        END ! IF
        
        expfil.Kill()
!region GenericTagFileClear
GenericTagFileClear    PROCEDURE()
tagType STRING(30)
    CODE
        tagType = p_web.GetValue('tagtype')
        
        IF (tagType = '')
            RETURN
        END ! IF
        
        
        Relate:SBO_GenericTagFile.Open()
        
        STREAM(SBO_GenericTagFile)
        
        ProgressBarWeb.Init(RECORDS(SBO_GenericTagFile))
        Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
        tagf:SessionID = p_web.SessionID
        tagf:TagType = tagType
        SET(tagf:KeyTagged,tagf:KeyTagged)
        LOOP UNTIL Access:SBO_GenericTagFile.Next() <> Level:Benign
            IF (tagf:SessionID <> p_web.SessionID OR |
                tagf:TagType <> tagType)
                BREAK
            END ! IF
            
            BHAddToDebugLog('GenericTagFileClear = ' & CLIP(tagType) & ' / ' & CLIP(tagf:TaggedValue))
            
            tagf:Tagged = 0
            IF (Access:SBO_GenericTagFile.TryUpdate())
                BHAddToDebugLog('GenericTagFileClear = ' & CLIP(tagType) & ' / ' & CLIP(tagf:TaggedValue) & '. Error: ' & ERROR())
            END  ! IF
            
            
            ProgressBarWeb.Update('UnTagging...')
        END ! LOOP
        
        FLUSH(SBO_GenericTagFile)
        
        Relate:SBO_GenericTagFile.Close()
        
        ! Pass the tab number back on the return
        IF (p_web.IfExistsValue('tab'))
            p_web.SSV('RedirectURL',p_web.GSV('RedirectURL') & '?tab=' & p_web.GetValue('tab'))
        END ! IF
        
!endregion
!region GenericTagFileTagAll
GenericTagFileTagAll      PROCEDURE()
tagType                 STRING(30)
statusType              LONG
accountType             LONG
    MAP
AddTag  PROCEDURE(STRING pTagValue)
    END 
    CODE
    
        tagType = p_web.GetValue('tagtype')
        
        IF (tagType = '')
            RETURN
        END ! IF

        ProgressBarWeb.Init(300)
        Relate:SBO_GenericTagFile.Open()
        
        STREAM(SBO_GenericTagFile)

        CASE tagType
        OF kSubAccount
        
            Relate:SUBTRACC.Open()

            Access:SUBTRACC.ClearKey(sub:Main_Account_Key)
            sub:Main_Account_Number = p_web.GSV('BookingAccount')
            SET(sub:Main_Account_Key,sub:Main_Account_Key)
            LOOP UNTIL Access:SUBTRACC.Next() <> Level:Benign
                IF (sub:Main_Account_Number <> p_web.GSV('BookingAccount'))
                    BREAK
                END ! IF
                
                AddTag(sub:Account_Number)
                
            END ! LOOP
            
            Relate:SUBTRACC.Close()
        OF kGenericAccount
        
            Relate:SUBTRACC.Open()
            
            Access:SUBTRACC.ClearKey(sub:GenericAccountKey)
            sub:Generic_Account = 1
            SET(sub:GenericAccountKey,sub:GenericAccountKey)
            LOOP UNTIL Access:SUBTRACC.Next() <> Level:Benign
                IF (sub:Generic_Account <> 1)
                    BREAK
                END ! IF
                
                AddTag(sub:Account_Number)
            END ! LOOP
            
            Relate:SUBTRACC.Close()
            
        OF kStatusTypes
            Relate:STATUS.Open()
            
            statusType = p_web.GetValue('statustype')
            
            CASE statusType
            OF 0 ! JOB
                Access:STATUS.ClearKey(sts:JobKey)
                sts:Job = 'YES'
                SET(sts:JobKey,sts:JobKey)
            OF 1 ! EXCHANGE
                Access:STATUS.ClearKey(sts:ExchangeKey)
                sts:Exchange = 'YES'
                SET(sts:ExchangeKey,sts:ExchangeKey)
            OF 2 ! LOAN
                Access:STATUS.ClearKey(sts:LoanKey)
                sts:Loan = 'YES'
                SET(sts:LoanKey,sts:LoanKey)
            END ! CASE
            LOOP UNTIL Access:STATUS.Next() <> Level:Benign
                CASE statusType
                OF 0 ! JOB
                    IF (sts:Job <> 'YES')
                        BREAK
                    END ! IF
                OF 1 ! EXCHANGE
                    IF (sts:Exchange <> 'YES')
                        BREAK
                    END ! IF
                OF 2 ! LOAN
                    IF (sts:Loan <> 'YES')
                        BREAK
                    END ! IF
                END ! CASE
                
                AddTag(sts:Status)
            
            END ! LOOP
            
            
            Relate:STATUS.Close()
            
        
        OF kInternalLocation
            
            Relate:LOCINTER.Open()

            Access:LOCINTER.ClearKey(loi:Location_Available_Key)
            loi:Location_Available = 'YES'
            SET(loi:Location_Available_Key,loi:Location_Available_Key)
            LOOP UNTIL Access:LOCINTER.Next() <> Level:Benign
                IF (loi:Location_Available <> 'YES')
                    BREAK
                END ! IF
    
                AddTag(loi:Location)
    
            END ! LOOP

            Relate:LOCINTER.Close()
        END ! CASE
        
        FLUSH(SBO_GenericTagFile)
        
        Relate:SBO_GenericTagFile.Close()    
        
        
        ! Pass the tab number back on the return
        IF (p_web.IfExistsValue('tab'))
            p_web.SSV('RedirectURL',p_web.GSV('RedirectURL') & '?tab=' & p_web.GetValue('tab'))
        END ! IF

        
AddTag              PROCEDURE(STRING pTagValue)
    CODE
        ProgressBarWeb.Update('Tagging...')
        
        Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
        tagf:SessionID = p_web.SessionID
        tagf:TagType = tagType
        tagf:TaggedValue = pTagValue
        IF (Access:SBO_GenericTagFile.TryFetch(tagf:KeyTagged) = Level:Benign)
            tagf:Tagged = 1
            Access:SBO_GenericTagFile.TryUpdate()
            BHAddToDebugLog('GenericTagFileTagAll (PUT) = ' & CLIP(tagType) & ' / ' & CLIP(tagf:TaggedValue))
        ELSE ! IF
            IF (Access:SBO_GenericTagFile.PrimeRecord() = Level:Benign)
                tagf:SessionID      = p_web.SessionID
                tagf:TagType        = tagType
                tagf:TaggedValue    = pTagValue            
                tagf:Tagged = 1
                IF (Access:SBO_GenericTagFile.TryInsert())
                    Access:SBO_GenericTagFile.CancelAutoInc()
                END ! IF
            END ! IF

            BHAddToDebugLog('GenericTagFileTagAll (ADD) = ' & CLIP(tagType) & ' / ' & CLIP(tagf:TaggedValue))
        END ! IF  
            
    !endregion
! GlobalIMEISearch
GlobalIMEIList      PROCEDURE(STRING pIMEINumber)!
recordCount             LONG(0)
Q                       QUEUE,PRE(Q)
JobNumber                   LONG()
IMEINumber                  STRING(30)
                        END ! QUEUE
i                       LONG()
locIMEINumber           STRING(30)
CODE
        IF (pIMEINumber = '')
            CreateScript(packet,'alert("You have not entered an IMEI Number.")')
            DO ReturnFail
            RETURN
        END ! IF
    
    
        ClearTagFile(p_web) ! Sharing the tag file with the Jobs Progress Browse
        locIMEINumber = pIMEINumber
    
        ProgressBarWeb.Init(200)
        
        Relate:JOBS.Open()
        Relate:JOBTHIRD.Open()
        Relate:EXCHANGE.Open()
        Relate:SBO_OutParts.Open()
        
        LOOP 1 TIMES
            IF (p_web.IfExistsValue('recno'))
                p_web.SetValue('found',0)
                Access:SBO_OutParts.ClearKey(sout:RecordNumberKey)
                sout:RecordNumber = p_web.GetValue('recno')
                IF (Access:SBO_OutParts.TryFetch(sout:RecordNumberKey) = Level:Benign)
                    
                    Access:JOBS.ClearKey(job:Ref_Number_Key)
                    job:Ref_Number = sout:PartNumber
                    IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                        IF (job:Exchange_Unit_Number > 0)
                            Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                            xch:Ref_Number = job:Exchange_Unit_Number
                            IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                                p_web.SetValue('found',1)
                                locIMEINumber = xch:ESN
                                p_web.SSV('GlobalIMEINumber',xch:ESN)
                            END ! IF
                        END ! IF
                    END ! IF
                END ! IF
                IF (p_web.GetValue('found') = 0)
                    packet = CLIP(packet) & '<script>alert("Unable to find an exchange unit for the selected job.");</script>'    
                    packet = CLIP(packet) & '<script>window.location("' & p_web.GSV('ReturnURL') & '","_self");</script>'
                    Do Sendpacket
                    recordCount = 1 ! So we don't get the other message at the end
                    BREAK
                END ! IF                  
            END ! IF
        
            
        
            STREAM(JOBS)
            STREAM(JOBTHIRD)
            STREAM(EXCHANGE)
            STREAM(SBO_OutParts)
        
            Access:SBO_OutParts.ClearKey(sout:PartNumberKey)
            sout:SessionID = p_web.SessionID
            sout:LineType = ''
            SET(sout:PartNumberKey,sout:PartNumberKey)
            LOOP UNTIL Access:SBO_OutParts.Next() <> Level:Benign
                IF (sout:SessionID <> p_web.SessionID OR |
                    sout:LineType <> '')
                    BREAK
                END ! IF
                Access:SBO_OutParts.DeleteRecord(0)
            END ! LOOP
    
            Access:JOBS.ClearKey(job:ESN_Key)
            job:ESN = locIMEINumber
            SET(job:ESN_Key,job:ESN_Key)
            LOOP UNTIL Access:JOBS.Next() <> Level:Benign
                IF (job:ESN <> locIMEINumber)
                    BREAK
                END ! IF
            
                ProgressBarWeb.Update('Finding IMEI Numbers...')
            
                recordCount += 1
            
                Q.JobNumber = job:Ref_Number
                Q.IMEINumber = job:ESN
                ADD(Q)
            
            !InsertRecord(job:Ref_Number,job:ESN)
            END ! LOOP
        
            Access:JOBTHIRD.ClearKey(jot:OriginalIMEIKey)
            jot:OriginalIMEI = locIMEINumber
            SET(jot:OriginalIMEIKey,jot:OriginalIMEIKey)
            LOOP UNTIL Access:JOBTHIRD.Next() <> Level:Benign
                IF (jot:OriginalIMEI <> locIMEINumber)
                    BREAK
                END ! IF
                ProgressBarWeb.Update('Finding IMEI Numbers...')
            
                recordCount += 1
            
                Q.JobNumber = jot:RefNumber
                GET(Q,Q.JobNumber)
                IF (ERROR())
                    Q.JobNumber = jot:RefNumber
                    Q.IMEINumber = jot:OriginalIMEI
                    ADD(Q)
                ELSE
                    Q.JobNumber = jot:RefNumber
                    Q.IMEINumber = jot:OriginalIMEI
                    PUT(Q)
                END ! IF
            !InsertRecord(jot:RefNumber,jot:OriginalIMEI)
            END ! LOOP
        
            Access:EXCHANGE.ClearKey(xch:ESN_Only_Key)
            xch:ESN = locIMEINumber
            SET(xch:ESN_Only_Key,xch:ESN_Only_Key)
            LOOP UNTIL Access:EXCHANGE.Next() <> Level:Benign
                IF (xch:ESN <> locIMEINumber)
                    BREAK
                END ! IF
                IF (xch:Job_Number = 0)
                    CYCLE
                END ! IF
                ProgressBarWeb.Update('Finding IMEI Numbers...')
            
                recordCount += 1
            
                Q.JobNumber = xch:Job_Number
                GET(Q,Q.JobNumber)
                IF (ERROR())
                    Q.JobNumber = xch:Job_Number
                    Q.IMEINumber = xch:ESN
                    ADD(Q)
                ELSE
                    Q.JobNumber = xch:Job_Number
                    Q.IMEINumber = xch:ESN
                    PUT(Q)
                END
            
            !InsertRecord(xch:Job_Number,xch:ESN)
            END ! LOOP
        
            ProgressBarWeb.Init(RECORDS(Q))
        
            LOOP i = 1 TO RECORDS(Q)
                GET(Q,i)
                
                ProgressBarWeb.Update('Building Job List..')
            
                Access:SBO_OutParts.ClearKey(sout:PartNumberKey)
                sout:SessionID = p_web.SessionID
                sout:LineType = ''
                sout:PartNumber = Q.JobNumber
                IF (Access:SBO_OutParts.TryFetch(sout:PartNumberKey) = Level:Benign)
                    sout:Description = Q.IMEINumber
                    Access:SBO_OutParts.TryUpdate()
                ELSE ! IF
                    IF (Access:SBO_OutParts.PrimeRecord() = Level:Benign)
                        sout:SessionID   = p_web.SessionID
                        sout:PartNumber = Q.JobNumber
                        sout:LineType    = ''
                        sout:Description = Q.IMEINumber
                        IF (Access:SBO_OutParts.TryInsert())
                            Access:SBO_OutParts.CancelAutoInc()
                        END ! IF
                    END ! IF
                END ! IF             
            END ! LOOP
        
            FLUSH(JOBS)
            FLUSH(JOBTHIRD)
            FLUSH(EXCHANGE)
            FLUSH(SBO_OutParts)
            
        END ! LOOP
        
        Relate:JOBS.Close()
        Relate:JOBTHIRD.Close()
        Relate:EXCHANGE.Close()
        Relate:SBO_OutParts.Close()
        
        
        IF (recordCount = 0)
            packet = CLIP(packet) & '<script>alert("There are no jobs that match the selected IMEI Number.");</script>'
            DO ReturnFail
        ELSE
            Do Redirect  
        END ! IF


!region HandlingFeeReportEngine
HandlingFeeReportEngine PROCEDURE()
    MAP
Criteria      PROCEDURE(),BYTE    
    END ! MAP
recs                        LONG()
i                           LONG()
count                       LONG()
    CODE
        ProgressBarWeb.Init(2000)
        
        ! Clear Q
        recs = RECORDS(qHandlingFeeReport)
        LOOP i = recs TO 1 BY -1
            GET(qHandlingFeeReport,i)
            IF (qHandlingFeeReport.SessionID = p_web.SessionID)
                DELETE(qHandlingFeeReport)
            END ! IF
        END ! LOOP
        
        FREE(qHandlingFeeReport)
        
        Relate:JOBS.Open()
        Relate:WEBJOB.Open()
        Relate:JOBSE.Open()
        Relate:LOCATLOG.Open()
        Relate:AUDIT.Open()
        Relate:AUDIT2.Open()
        Relate:JOBSCONS.Open()
        Relate:JOBTHIRD.Open()
        Relate:EXCHANGE.Open()
        Relate:Audit_Alias.Open()
        Relate:SBO_GenericTagFile.Open()
        
        Access:AUDIT.ClearKey(aud:ActionOnlyKey)
        aud:Action = 'O.B.F. VALIDATION ACCEPTED'
        aud:Date = p_web.GSV('locStartDate')
        SET(aud:ActionOnlyKey,aud:ActionOnlyKey)
        LOOP UNTIL Access:AUDIT.Next() <> Level:Benign
            IF (aud:Action <> 'O.B.F. VALIDATION ACCEPTED' OR |
                aud:Date > p_web.GSV('locEndDate'))
                BREAK
            END ! IF
            
            BHAddToDebugLog('HandlingFreeReport: OBF = ' & aud:Ref_Number)
            
            IF (Criteria())
                BHAddToDebugLog('HandlingFreeReport: OBF = ' & aud:Ref_Number & ' - FAILED')
                CYCLE
            END ! IF
            CLEAR(qHandlingFeeReport)
            qHandlingFeeReport.RepairType = 'OBF'
            DO AddToQ
        END ! LOOP
        
        Access:AUDIT.ClearKey(aud:ActionOnlyKey)
        aud:Action = 'EXCHANGE UNIT ATTACHED TO JOB'
        aud:Date = p_web.GSV('locStartDate')
        SET(aud:ActionOnlyKey,aud:ActionOnlyKey)
        LOOP UNTIL Access:AUDIT.Next() <> Level:Benign
            IF (aud:Action <> 'EXCHANGE UNIT ATTACHED TO JOB' OR |
                aud:Date > p_web.GSV('locEndDate'))
                BREAK
            END ! IF
            
            BHAddToDebugLog('HandlingFreeReport: EXCH = ' & aud:Ref_Number)
            
            IF (Criteria())
                BHAddToDebugLog('HandlingFreeReport: EXCH = ' & aud:Ref_Number & ' - FAILED')
                CYCLE
            END ! IF
            
            Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
            aud2:AUDRecordNumber = aud:Record_Number
            IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
                IF ~(INSTRING('UNIT NUMBER: ' & job:Exchange_Unit_Number,aud2:Notes,1,1))
                    CYCLE
                END
            ELSE ! IF
            END ! IF

            IF (jobe:ExchangedAtRRC <> TRUE)
                CYCLE
            END ! IF
            
            Access:LOCATLOG.ClearKey(lot:NewLocationKey)
            lot:RefNumber = aud:Ref_Number
            lot:NewLocation = GETINI('RRC','ARCLocation',,PATH() & '\SB2KDEF.INI')
            IF (Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign)
                
            ELSE ! IF
                CYCLE
            END ! IF
            CLEAR(qHandlingFeeReport)
            qHandlingFeeReport.RepairType = 'EXCH'
            DO AddToQ
                    
        END ! LOOP
        
        Access:AUDIT.ClearKey(aud:ActionOnlyKey)
        aud:Action = 'JOB UPDATED'
        aud:Date = p_web.GSV('locStartDate')
        SET(aud:ActionOnlyKey,aud:ActionOnlyKey)
        LOOP UNTIL Access:AUDIT.Next() <> Level:Benign
            IF (aud:Action <> 'JOB UPDATED' OR |
                aud:Date > p_web.GSV('locEndDate'))
                BREAK
            END ! IF
            BHAddToDebugLog('HandlingFreeReport: JOB = ' & aud:Ref_Number)
            
            IF (Criteria())
                BHAddToDebugLog('HandlingFreeReport: JOB = ' & aud:Ref_Number & ' - FAILED')
                CYCLE
            END ! IF
            
            IF (jobe:HubRepairDate = '')
                CYCLE
            END ! IF            
            
            Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
            aud2:AUDRecordNumber = aud:Record_Number
            IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
                IF ~(INSTRING('HUB REPAIR SELECTED',aud2:Notes,1,1))
                    CYCLE
                END
            ELSE ! IF
            END ! IF     
            
            Access:LOCATLOG.ClearKey(lot:NewLocationKey)
            lot:RefNumber = aud:Ref_Number
            lot:NewLocation = GETINI('RRC','ARCLocation',,PATH() & '\SB2KDEF.INI')
            IF (Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign)
                
            ELSE ! IF
                CYCLE
            END ! IF            
            CLEAR(qHandlingFeeReport)
            qHandlingFeeReport.RepairType = 'REPAIR'
            DO AddToQ
        END ! LOOP        

        Relate:AUDIT.Close()
        Relate:AUDIT2.Close()
        Relate:LOCATLOG.Close()
        Relate:JOBSE.Close()
        Relate:WEBJOB.Close()
        Relate:JOBS.Close()   
        Relate:JOBSCONS.Close()    
        Relate:JOBTHIRD.Close()
        Relate:EXCHANGE.Close()
        Relate:Audit_Alias.Close()
        Relate:SBO_GenericTagFile.Close()
        
        
        IF (count = 0)
            CreateScript(packet,kNoREcordsAlert)
            DO ReturnFail
        ELSE
            
            CASE p_web.GSV('locReportOrder')
            OF 0
                SORT(qHandlingFeeReport,qHandlingFeeReport.RefNumber)
            OF 1
                SORT(qHandlingFeeReport,qHandlingFeeReport.ActionDate)
            OF 2
                SORT(qHandlingFeeReport,qHandlingFeeReport.Manufacturer)
                
            END ! CASE
            
            
            IF (p_web.GetValue('ReportType') = 'E')
                HandlingFeeReportExport()
            ELSE
!                CreateScript(packet,'window.open("HandlingFeeReport","_self")')
!                DO SendPacket
                CallReport('HandlingFeeReport')
            END ! IF        
        END ! IF
        
AddToQ              ROUTINE
    qHandlingFeeReport.SessionID = p_web.SessionID
    qHandlingFeeReport.RefNumber = job:Ref_Number
    qHandlingFeeReport.Manufacturer = job:Manufacturer
    qHandlingFeeReport.ModelNumber = job:Model_Number
    qHandlingFeeReport.ActionDate = aud:Date
    qHandlingFeeReport.CompanyName = job:Company_Name
    qHandlingFeeReport.AccountNumber = job:Account_Number
    IF (job:Warranty_Job = 'YES' AND job:Chargeable_Job = 'YES')
        qHandlingFeeReport.ChargeType  = CLIP(job:Warranty_Charge_Type) & '/' & CLIP(job:Charge_Type)
    ELSE ! IF
        IF (job:Warranty_Job = 'YES')
            qHandlingFeeReport.ChargeType = job:Warranty_Charge_Type
        END ! IF
        IF (job:Chargeable_Job = 'YES')
            qHandlingFeeReport.ChargeType = job:Charge_Type
        END ! IF
    END ! IF
    qHandlingFeeReport.RRCJobNumber = wob:JobNumber
    qHandlingFeeReport.JobNumber = job:Ref_Number & '-' & CLIP(tra:BranchIdentification) & CLIP(wob:JobNumber)
    qHandlingFeeReport.HandlingFee = jobe:HandlingFee + jobe:ExchangeRate
    
    CASE qHandlingFeeReport.RepairType
    OF 'REPAIR'
        IF (job:Exchange_Unit_Number > 0)
            qHandlingFeeReport.Repair = 'ARC'
        ELSE ! IF
            IF (JobSentToARC())
                qHandlingFeeReport.Repair = 'ARC'
            ELSE ! IF
                qHandlingFeeReport.Repair = 'RRC'
            END ! IF
        END ! IF
    OF 'EXCH'
        qHandlingFeeReport.Repair = 'RRC'
    END ! CASE
    CASE jobe:Engineer48HourOption
    OF 1
        qHandlingFeeReport.EngineerOption = '48H'
    Of 2
        qHandlingFeeReport.EngineerOption = 'ARC'
    Of 3
        qHandlingFeeReport.EngineerOption = '7DT'
    OF 4
        qHandlingFeeReport.EngineerOption = 'STD' 
    ELSE
        qHandlingFeeReport.EngineerOption = ''
    END ! CASE
    qHandlingFeeReport.WaybillNo = ''
    qhandlingFeeReport.WaybillDate = ''
    Access:JOBSCONS.ClearKey(joc:DateKey)
    joc:RefNumber = job:Ref_Number
    SET(joc:DateKey,joc:DateKey)
    LOOP UNTIL Access:JOBSCONS.Next() <> Level:Benign
        IF (joc:RefNumber <> job:Ref_Number)
            BREAK
        END ! IF
        IF (joc:DespatchFrom = 'RRC' AND joc:DespatchTo = 'ARC')
            qHandlingFeeReport.WaybillNo = joc:ConsignmentNumber
            qhandlingFeeReport.WaybillDate = joc:TheDate
            BREAK
        END ! IF
    END ! LOOP
    Access:LOCATLOG.ClearKey(lot:NewLocationKey)
    lot:RefNumber = job:Ref_Number
    lot:NewLocation = GETINI('RRC','ARCLocation',,PATH() & '\SB2KDEF.INI')
    IF (Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign)
        qHandlingFeeReport.ARCWaybillDate = lot:TheDate
    END ! IF
    
    qHandlingFeeReport.OriginalIMEI = job:ESN
    Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
    jot:RefNumber = job:Ref_Number
    SET(jot:RefNumberKey,jot:RefNumberKey)
    IF (Access:JOBTHIRD.Next() = Level:Benign)
        IF (jot:RefNumber = job:Ref_Number)
            qHandlingFeeReport.OriginalIMEI = jot:OriginalIMEI
        END ! IF
    END ! LOOP
    
    qHandlingFeeReport.JobBookedDate = job:date_booked

    qHandlingFeeReport.ExchangeIMEI = ''
    qHandlingFeeReport.ExchangeIMEIDate = ''
    
    IF (job:Exchange_Unit_Number > 0)
        Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
        xch:Ref_Number = job:Exchange_Unit_Number
        IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
            qHandlingFeeReport.ExchangeIMEI = xch:ESN
            
            ! Get Allocation Date From Audit
            Access:AUDIT_ALIAS.ClearKey(aud1:Ref_Number_Key)
            aud1:Ref_Number = job:Ref_Number
            aud1:Date = TODAY()
            SET(aud1:Ref_Number_Key,aud1:Ref_Number_Key)
            LOOP UNTIL Access:AUDIT_ALIAS.Next() <> Level:Benign
                IF (aud1:Ref_Number <> job:Ref_Number)
                    BREAK
                END ! IF
                IF (aud1:Action = 'EXCHANGE UNIT ATTACHED TO JOB')
                    qHandlingFeeReport.ExchangeIMEIDate = aud1:Date
                    BREAK
                END ! IF
            END ! LOOP
        END ! IF
    END ! IF
    
    
    ADD(qHandlingFeeReport)        
    count += 1
        
Criteria      PROCEDURE()!,BYTE
    CODE
        ProgressBarWeb.Update('Building Data....')
        
        qHandlingFeeReport.SessionID = p_web.SessionID
        qHandlingFeeReport.RefNumber = aud:Ref_Number
        GET(qHandlingFeeReport,qHandlingFeeReport.SessionID,qHandlingFeeReport.RefNumber)
        IF NOT (ERROR())
            RETURN Level:Fatal
        END ! IF
        
        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = aud:Ref_Number
        IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
            
        ELSE ! IF
            RETURN Level:Fatal
        END ! IF
        
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
            
        ELSE ! IF
            RETURN Level:Fatal
        END ! IF
        
        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = job:Ref_Number
        IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
            
        ELSE ! IF
            RETURN Level:Fatal
        END ! IF
        
        IF (wob:HeadAccountNumber <> p_web.GSV('BookingAccount'))
            RETURN Level:Fatal
        END ! IF
        
        IF (p_web.GSV('locAllAccounts') <> 1)
            Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
            tagf:SessionID = p_web.SessionID
            IF (p_web.GSV('locGenericAccounts') = 0)
                tagf:TagType = kSubAccount
            ELSE
                tagf:TagType = kGenericAccount
            END ! IF
            
            tagf:TaggedValue = inv:Account_Number
            IF (Access:SBO_GenericTagFile.TryFetch(tagf:KeyTagged) = Level:Benign)
                IF (tagf:Tagged = 0)
                    RETURN Level:Fatal
                END ! IF
            ELSE ! IF
                RETURN Level:Fatal
            END ! IF       
        END ! IF
        IF (p_web.GSV('locAllChargeTypes') <> 1)
            Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
            tagf:SessionID = p_web.SessionID
            tagf:TagType = 'C'
            tagf:TaggedValue = job:Warranty_Charge_Type
            IF (Access:SBO_GenericTagFile.TryFetch(tagf:KeyTagged) = Level:Benign)
                IF (tagf:Tagged = 0)
                    RETURN Level:Fatal
                END ! IF
            ELSE ! IF
                RETURN Level:Fatal
            END ! IF            
        END ! IF
        IF (p_web.GSV('locAllManufacturers') <> 1)
            Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
            tagf:SessionID = p_web.SessionID
            tagf:TagType = 'M'
            tagf:TaggedValue = job:Manufacturer
            IF (Access:SBO_GenericTagFile.TryFetch(tagf:KeyTagged) = Level:Benign)
                IF (tagf:Tagged = 0)
                    RETURN Level:Fatal
                END ! IF
            ELSE ! IF
                RETURN Level:Fatal
            END ! IF        
        END ! IF
        
        RETURN Level:Benign    

!endregion
!region HandlingFeeReportExport
HandlingFeeReportExport     PROCEDURE()
TotalGroup           GROUP,PRE(total)
Lines                LONG
HandlingFee          REAL
                     END
i                               LONG()            
countRecords                    LONG()
    CODE
        Relate:USERS.Open()
        
        ProgressBarWeb.Init(RECORDS(qHandlingFeeReport))
        
        expfil.Init('Exchange Handling Fee Report')
        expfil.AddField('HANDLING AND EXCHANGE FEE REPORT',1,1)
        
        expfil.AddField('Date Range',1)
        expfil.AddField(FORMAT(p_web.GSV('locStartDate'),@d06) & ' to ' & FORMAT(p_web.GSV('locEndDate'),@d06b))
        
        expfil.AddField('Printed By',1)
        
        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = p_web.GSV('BookingUserCode')
        IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
            
        ELSE ! IF
        END ! IF
        expfil.AddField(CLIP(use:Forename) & ' ' & CLIP(Use:Surname),,1)
        
        expfil.AddField('Date Printed',1)
        expfil.AddField(FORMAT(TODAY(),@d06),,1)
        
        expfil.AddField('Job No,Engineer Option,Branch ID,Franchise Job No,Action Date,Account No,Account Name,Manufacturer,Model Number,Charge Type,RRC Waybill No,RRC Waybill Date,ARC Waybill Receipt Date,Type,Location,Hand/Exch,Original IMEI,Booking Date,Exchange IMEI,Exchange Date',1,1,,1)
        
        CLEAR(TotalGroup)
        countRecords = 0
        LOOP i = 1 TO RECORDS(qHandlingFeeReport)
            GET(qHandlingFeeReport,i)
            
            ProgressBarWeb.Update('Exporting...')
            
            IF (qHandlingFeeReport.SessionID <> p_web.SessionID)
                CYCLE
            END ! IF
            
            
            ! Job Number
            expfil.AddField(qHandlingFeeReport.RefNumber,1)
            ! Add Engineer Option
            expfil.AddField(qHandlingFeeReport.EngineerOption)
            ! Trade Branch ID
            expfil.AddField(p_web.GSV('BookingBranchID'))
            ! WebJob Job Number
            expfil.AddField(qHandlingFeeReport.RRCJobNumber)
            ! Action Date
            expfil.AddField(FORMAT(qHandlingFeeReport.ActionDate,@d06b))
            ! Account Number
            expfil.AddField(qHandlingFeeReport.AccountNumber)
            ! Company Name
            expfil.AddField(qHandlingFeeReport.CompanyName)
            ! Manufacturer
            expfil.AddField(qHandlingFeeReport.Manufacturer)
            ! Model Numnber
            expfil.AddField(qHandlingFeeReport.ModelNumber)
            ! Charge Type
            expfil.AddField(qHandlingFeeReport.ChargeType)
            ! RRC Waybill No
            expfil.AddField(qHandlingFeeReport.WaybillNo)
            ! RRC Waybill Date
            expfil.AddField(FORMAT(qHandlingFeeReport.WaybillDate,@d06b))
            ! ARC Waybill Date
            expfil.AddField(FORMAT(qHandlingFeeReport.ARCWaybillDate,@d06b))
            ! Type
            expfil.AddField(qHandlingFeeReport.RepairType)
            ! Repair
            expfil.AddField(qHandlingFeeReport.Repair)
            ! Handling Fee
            expfil.AddField(FORMAT(qHandlingFeeReport.HandlingFee,@n_14.2))
            ! Original IMEI
            IF (qHandlingFeeReport.OriginalIMEI <> '')
                expfil.AddField('''' & qHandlingFeeReport.OriginalIMEI)
            ELSE ! IF
                expfil.AddField()
            END ! IF
            ! Date Booked
            expfil.AddField(FORMAT(qHandlingFeeReport.JobBookedDate,@d06b))
            ! Exchange IMEI
            IF (qHandlingFeeReport.ExchangeIMEI <> '')
                expfil.AddField('''' & qHandlingFeeReport.ExchangeIMEI)
            ELSE
                expfil.AddField()
            END ! IF
            ! Exchange IMEI Date
            expfil.AddField(FORMAT(qHandlingFeeReport.ExchangeIMEIDate,@d06b),,1)
            
            total:HandlingFee += qHandlingFeeReport.HandlingFee
            total:Lines += 1
        END ! LOOP
        expfil.AddField('TOTALS',1,1)
        expfil.AddField('Jobs Counted',1)
        expfil.AddField(total:Lines)
        expfil.AddField(,,,13)
        expfil.AddField(FORMAT(total:HandlingFee,@n_14.2),,1)
        
        
        expfil.Kill()
        
        Relate:USERS.Close()

!endregion
!region InsuranceReportExport
InsuranceReportExport       PROCEDURE() 
vat DECIMAL(10,2)
total DECIMAL(10,2)
paid    DECIMAL(10,2)
count LONG()
    CODE
        ProgressBarWeb.Init(1000)
        
        expfil.Init('Insurance Claim Export','Insurance Claim Export ' & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Format(Year(Today()),@n04) & Format(Clock(),@t2))
        
        expfil.AddField('Authority Number Report',1,1)
        expfil.AddField('Job Number,Charge Type,Authorisation Number,Trade Account Number,Trade Account Name,' &|
            'Customer,Date Booked,Date Completed,Manufacturer,Model Number,Repair Type,' &|
            'Parts Selling,Labour,V.A.T.,Total,Excess Paid,Outstanding Amount',1,1,,1)

        Relate:JOBS.Open()
        Relate:WEBJOB.Open()
        Relate:SUBTRACC.Open()
        Relate:INVOICE.Open()
        Relate:JOBPAYMT.Open()
        Relate:JOBSE.Open()
        
        count = 0
        
        Access:WEBJOB.ClearKey(wob:DateCompletedKey)
        wob:HeadAccountNumber = p_web.GSV('BookingAccount')
        wob:DateCompleted = p_web.GSV('locStartDate')
        SET(wob:DateCompletedKey,wob:DateCompletedKey)
        LOOP UNTIL Access:WEBJOB.Next() <> Level:Benign
            IF (wob:HeadAccountNumber <> p_web.GSV('BookingAccount') OR |
                wob:DateCompleted > p_web.GSV('locEndDate'))
                BREAK
            END ! IF

            ProgressBarWeb.Update('Exporting.....')
            
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = wob:RefNumber
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
                CYCLE
            END ! IF
            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = job:Ref_Number
            IF (Access:JOBSE.TryFetch(jobe:RefNumberKey))
                CYCLE
            END ! IF
            
            IF (job:Chargeable_Job <> 'YES')
                CYCLE
            END ! IF
            IF (job:Invoice_Number = 0)
                CYCLE
            END ! IF
            
            Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
            inv:Invoice_Number = job:Invoice_Number
            IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign)
                IF NOT (inv:ExportedRRCOracle)
                    CYCLE
                END ! IF
            ELSE ! IF
            END ! I            
            
            expfil.AddField(job:Ref_Number,1)
            expfil.AddField(job:Charge_Type)
            expfil.AddField(job:Authority_Number)
            expfil.AddField(job:Account_Number)
            
            Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
            sub:Account_Number = job:Account_Number
            IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
                expfil.AddField(sub:Company_Name)
            ELSE ! IF
                expfil.AddField()
            END ! IF
            expfil.AddField(job:Company_Name)
            expfil.AddField(FORMAT(job:Date_Booked,@d06))
            expfil.AddField(FORMAT(job:Date_Completed,@d06))
            expfil.AddField(job:Manufacturer)
            expfil.AddField(job:Model_Number)
            expfil.AddField(job:Repair_Type)
            expfil.AddField(FORMAT(jobe:InvRRCCPartsCost,@n_14.2))
            expfil.AddField(FORMAT(jobe:InvRRCCLabourCost,@n_14.2))
                
            vat = jobe:InvRRCCPartsCost * inv:RRCVatRateLabour/100 + |
                                                jobe:InvRRCCLabourCost * inv:RRCVatRateParts/100 + |
                                                job:Invoice_Courier_Cost * inv:RRCVatRateLabour/100
            
            expfil.AddField(FORMAT(vat,@n_14.2))
                          
            total = vat + jobe:InvRRCCLabourCost + jobe:InvRRCCPartsCost + |
                                                job:Invoice_Courier_Cost
                          
            expfil.AddField(FORMAT(total,@n_14.2))
            
            paid = 0
            Access:JOBPAYMT.ClearKey(jpt:All_Date_Key)
            jpt:Ref_Number = job:Ref_Number
            SET(jpt:All_Date_Key,jpt:All_Date_Key)
            LOOP UNTIL Access:JOBPAYMT.Next() <> Level:Benign
                IF (jpt:Ref_Number <> job:Ref_Number)
                    BREAK
                END ! IF
                paid += jpt:Amount
            END ! LOOP
            
            expfil.AddField(FORMAT(paid,@n_14.2))
            
            IF (paid < total)
                expfil.AddField(FORMAT(total - paid,@n_14.2),,1)
            ELSE ! IF
                expfil.AddField(,,1)
            END ! IF
            
            count += 1
            
        END ! LOOP
        
        Relate:INVOICE.Close()
        Relate:SUBTRACC.Close()
        Relate:JOBS.Close()
        Relate:WEBJOB.Close()
        Relate:JOBPAYMT.Close()
        Relate:JOBSE.Close()
        
        IF (count = 0)
            expfil.Kill(1)
            CreateScript(packet,kNoREcordsAlert)
            DO ReturnFail
        ELSE ! IF 
            expfil.Kill()
        END ! IF
!endregion
! LoanPhoneExport
LoanPhoneExport     PROCEDURE()!
countRecords LONG(0)
    CODE
        expfil.Init('Loan Phone Export')
		
        ProgressBarWeb.Init(RECORDS(LOAN))
        
        expfil.AddField('LOAN UNIT REPORT',1,1)
		
		expfil.AddField('I.M.E.I. Number,M.S.N.,Manufacturer,Model Number,' & |
						'Stock Type,Unit Number,Unit Status,Status Date,' & |
						'Reason Not Available,Job Number,Date Job Booked,' & |
						'Date Loan Issued,Contact Name,Contact Number,' & |
						'Current Job Status,Current Exchange Status,' & |
						'Current Loan Status,Delivery Address Line 1,' & |
						'Delivery Address Line 2,Delivery Address Line 3,' & |
						'Delivery Address Line 4',1,1,,1)
						
		Relate:LOAN.Open()
		Relate:LOANHIST.Open()
		Relate:JOBS.Open()
		Relate:AUDIT.Open()
		
		Access:LOAN.ClearKey(loa:LocIMEIKey)
		loa:Location = p_web.GSV('BookingSiteLocation')
		SET(loa:LocIMEIKey,loa:LocIMEIKey)
		LOOP UNTIL Access:LOAN.Next() <> Level:Benign
			IF (loa:Location <> p_web.GSV('BookingSiteLocation'))
				BREAK
			END ! IF
			
			ProgressBarWeb.Update('Exporting.....')
			
            BHAddToDebugLog('LoanPhoneExport: BEFORE loa:Ref_Number = ' & loa:Ref_Number)
			IF (p_web.GSV('locLoanStatus') <> '')
				IF (loa:Available <> p_web.GSV('locLoanStatus'))
					CYCLE
				END ! IF
			END ! IF
			
			IF (p_web.GSV('locStockType') <> '')
				IF (loa:Stock_Type <> p_web.GSV('locStockType'))	
					CYCLE
				END ! IF
			END ! IF
			
			IF (p_web.GSV('locStartDate') > 0)
				IF (loa:StatusChangeDate < p_web.GSV('locStartDate'))
					CYCLE
				END ! IF
			END ! IF
			IF (p_web.GSV('locEndDate') > 0)
				IF (loa:StatusChangeDate > p_web.GSV('locEndDate'))
					CYCLE
				END ! IF
			END ! IF
			
            BHAddToDebugLog('LoanPhoneExport: AFTER loa:Ref_Number = ' & loa:Ref_Number)
			! IMEI
			expfil.AddField('`' & loa:ESN,1)
			! MSN
            IF (loa:MSN <> '' AND loa:MSN <> 'N/A')
                expfil.AddField('`' & loa:MSN)
            ELSE
                expfil.AddField()
            END ! IF
			! Manufacturer
			expfil.AddField(loa:Manufacturer)
			! Model Number
			expfil.AddField(loa:Model_Number)
			! Stock Type
			expfil.AddField(loa:Stock_Type)
			! Ref Number
			expfil.AddField(loa:Ref_Number)
			! Status
			expfil.AddField(VodacomClass.GetLoanStatus(loa:Available,loa:Job_Number))
			! Status Date
			expfil.AddField(FORMAT(loa:StatusChangeDate,@d06))
			! Loan Reason
			found# = 0
			IF (loa:Available = 'NOA')
				Access:LOANHIST.ClearKey(loh:Ref_Number_Key)
				loh:Ref_Number = loa:Ref_Number
				SET(loh:Ref_Number_Key,loh:Ref_Number_Key)
				LOOP UNTIL Access:LOANHIST.Next() <> Level:Benign
					IF (loh:Ref_Number <> loa:Ref_Number)
						BREAK
					END ! IF
					If Instring('UNIT AVAILABLE',loh:Status,1,1)
						Cycle
					End ! If Instring('UNIT AVAILABLE',loh:Status,1,1)
					If Instring('UNIT DESPATCHED',loh:Status,1,1)
						Cycle
					End ! If Instring('UNIT DESPATCHED',loh:Status,1,1)
					If Instring('UNIT RETURNED',loh:Status,1,1)
						Cycle
					End ! If Instring('UNIT RETURNED',log:Status,1,1)
					If Instring('UNIT LOANED',loh:Status,1,1)
						Cycle
					End ! If Instring('UNIT LOANED',loh:Status,1,1)
					If Instring('UNIT MOVED FROM',loh:Status,1,1)
						Cycle
					End ! If Instring('UNIT MOVED FROM',loh:Status,1,1)
					If Instring('UNIT RE-STOCKED',loh:Status,1,1)
						Cycle
					End ! If Instring('UNIT RE-STOCKED',loh:Status,1,1)
					If Instring('INITIAL ENTRY',loh:Status,1,1)
						Cycle
					End ! If Instring('INITIAL ENTRY',loh:Status,1,1)
					If Instring('REPLACE LOAN',loh:Status,1,1)
						Cycle
					End ! If Instring('REPLACE LOAN',loh:Status,1,1)
					If Instring('AWAITING QA',loh:Status,1,1)
						Cycle
					End ! If Instring('AWAITING QA',loh:Status,1,1)
					If Instring('RAPID QA UPDATE',loh:Status,1,1)
						Cycle
					End ! If Instring('RAPID QA UPDATE',loh:Status,1,1)
					If Instring('QA PASS ON JOB',loh:Status,1,1)
						Cycle
					End ! If Instring('QA PASS ON JOB',loh:Status,1,1)
					If Instring('AUDIT SHORTAGE',loh:Status,1,1)
						Cycle
					End ! If Instring('AUDIT SHORTAGE',loh:Status,1,1)					
					expfil.AddField(BHStripAlphaNumOnly(loh:Status))
					found# = 1
					BREAK
				END ! LOOP
				
			END ! IF
			IF (Found# = 0)
				expfil.AddField()
			END ! IF

			IF (loa:Job_Number > 0)
				! Job Number
				expfil.AddField(loa:Job_Number)
				! Date Job Booked
				Access:JOBS.ClearKey(job:Ref_Number_Key)
				job:Ref_Number = loa:Job_Number	
                IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                    expfil.AddField(FORMAT(job:Date_Booked,@d06))
					! Date Attached
                    Found# = 0
                    Access:AUDIT.ClearKey(aud:Action_Key)
                    aud:Ref_Number = job:Ref_Number
                    aud:Action = 'LOAN UNIT ATTACHED TO JOB'
                    SET(aud:Action_Key,aud:Action_Key)
                    LOOP UNTIL Access:AUDIT.Next() <> Level:Benign
                        IF (aud:Ref_Number <> job:Ref_Number OR|
                            aud:Action <> 'LOAN UNIT ATTACHED TO JOB')
                            BREAK
                        END ! IF
                        expfil.AddField(FORMAT(aud:Date,@d06))
                        Found# = 1
                        BREAK
                    END ! LOOP
                    IF (Found# = 0)
                        expfil.AddField(FORMAT(job:Date_Booked,@d06))
                    END ! IF
					
					! Customer
                    expfil.AddField(CLIP(job:Title) & ' ' & CLIP(job:Initial) & CLIP(job:Surname))
                    expfil.AddField(job:Telephone_Delivery)
                    expfil.AddField(job:Current_Status)
                    expfil.AddField(job:Exchange_Status)
                    expfil.AddField(job:Loan_Status)
                    expfil.AddField(job:Address_Line1_Delivery)
                    expfil.AddField(job:Address_Line2_Delivery)
                    expfil.AddField(job:Address_Line3_Delivery)
                    expfil.AddField(job:PostCode_Delivery,,1)
					
                ELSE ! IF
                    expfil.AddField(,,1) ! Not filling in blanks. Should it matter?
                END ! IF
            ELSE
                expfil.AddField(,,1)
			END ! IF
			
			countRecords += 1
		END ! LOOP
		
		Relate:LOANHIST.Close()
		Relate:LOAN.Close()
		Relate:AUDIT.Close()
		Relate:JOBS.Close()

		IF (countRecords = 0)
			CreateScript(packet,kNoRecordsAlert)
			expfil.Kill(1)
			DO ReturnFail
        ELSE
			expfil.Kill()
		END ! IF
LoanUnitAvailable PROCEDURE()!
fail	LONG(0)
locAuditNotes STRING(255)
    CODE
        Relate:LOAN.Open()
		Relate:EXCHANGE.Open()
		Relate:JOBS.Open()
		Relate:JOBSE.Open()
		Relate:WEBJOB.Open()
		Relate:LOANHIST.Open()
		
		
		LOOP 1 TIMES
			Access:LOAN.ClearKey(loa:Ref_Number_Key)
			loa:Ref_Number = p_web.GetValue('loa:Ref_Number')
			IF (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
				IF (loa:Available = 'ITF')
					! Unit in transit. Need access level
					IF (DoesUserHaveAccess(p_web,'AMEND LOAN IN TRANSIT') = 0)
						CreateScript(packet,'alert("Error! Unit is in Transit. You do have not access to make it available.")')
						fail = 1
						BREAK
					END ! IF
				END ! IF
			ELSE ! IF
				CreateScript(packet,'alert("Cannot find Loan Unit.")')
				fail = 1
				BREAK
			END ! IF
			
			Access:EXCHANGE.ClearKey(xch:ESN_Only_Key)
			xch:ESN = loa:ESN	
			IF (Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign)
				IF (xch:Available <> 'NOA')
					CreateScript(packet,'alert("Cannot make this unit available. It is already "Available" in Exchange Stock.")')
					fail = 1
					BREAK				
				END ! IF
			ELSE ! IF
			END ! IF
			
			IF (loa:Job_Number > 0)
				Access:JOBS.ClearKey(job:Ref_Number_Key)
				job:Ref_Number = loa:Job_Number
				IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
					Access:JOBSE.ClearKey(jobe:RefNumberKey)
					jobe:RefNumber = job:Ref_Number
					IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
					END ! IF
					Access:WEBJOB.ClearKey(wob:RefNumberKey)
					wob:RefNumber = job:Ref_Number
					IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
					END ! IF
				
					p_web.FileToSessionQueue(JOBS)
					p_web.FileToSessionQueue(WEBJOB)
					p_web.FileToSessionQueue(JOBSE)
					
					GetStatus(816,1,'LOA',p_web)
					
					If Sub(job:Current_Status,1,3) = '811'
						GetStatus(810,1,'JOB',p_web)
					End !If Sub(job:Current_Status,1,3) = '811'					
					
					p_web.SessionQueueToFile(JOBS)
					p_web.SessionQueueToFile(WEBJOB)
					p_web.SessionQueueToFile(JOBSE)
					
					IF (Access:JOBS.TryUpdate() = Level:Benign)
						Access:WEBJOB.TryUpdate()
						Access:JOBSE.TryUpdate()
						AddToAudit(p_web,job:Ref_Number,'LOA','RETURN LOAN: RE-STOCKED - ACCESSORIES UNCHECKED','UNIT HAD BEEN DESPATCHED: ' & |
                        '<13,10>UNIT NUMBER: ' & Clip(job:Loan_unit_number) & |
                        '<13,10>COURIER: ' & CLip(job:loan_Courier) & |
                        '<13,10>CONSIGNMENT NUMBER: ' & CLip(job:loan_consignment_number) & |
                        '<13,10>DATE DESPATCHED: ' & Clip(Format(job:loan_despatched,@d6)) &|
                        '<13,10>DESPATCH USER: ' & CLip(job:loan_despatched_user) &|
                        '<13,10>DESPATCH NUMBER: ' & CLip(job:loan_despatch_number))
						
						!Remove the loan from the job table.
						job:loan_unit_number = ''
						job:loan_accessory = ''
						job:loan_consignment_number =''
						job:loan_despatched         =''
						job:loan_despatched_user    =''
						job:loan_despatch_number    =''
						job:Loaservice              =''
						job:loan_issued_date        = ''
						job:loan_user               = ''	

						IF (job:Despatch_Type = 'LOA')
							job:Despatched = 'NO'
							job:Despatch_Type = ''
						ELSE ! IF
							IF (jobe:DespatchType = 'LOA')
								jobe:DespatchType = ''
								wob:ReadyToDespatch = 0
							END ! IF
						END ! IF
						IF (Access:JOBS.TryUpdate() = Level:Benign)
							Access:WEBJOB.TryUpdate()
							Access:JOBSE.TryUpdate()						
						END ! IF
					END ! IF
					
				ELSE ! IF
					CreateScript(packet,'alert("Cannot find the job details for this loan.")')
					fail = 1
					BREAK
				END ! IF
				loa:Job_Number = ''
				loa:Available = 'AVL'
				loa:StatusChangeDate = TODAY()
				IF (Access:LOAN.TryUpdate() = Level:Benign)
					IF (Access:LOANHIST.PrimeRecord() = Level:Benign)
						loh:Ref_Number = loa:Ref_Number
						loh:Date = TODAY()
						loh:Time = CLOCK()
						loh:User = p_web.GSV('BookingUserCode')
						loh:Status = 'UNIT RE-STOCKED FROM JOB NO: ' & clip(format(job:ref_number,@p<<<<<<<#p))
						IF (Access:LOANHIST.TryInsert())
							Access:LOANHIST.CancelAutoInc()
						END ! IF
					END ! IF
				END ! IF
				
			ELSE ! IF
				loa:Job_Number = ''
				loa:Available = 'AVL'
				loa:StatusChangeDate = TODAY()
				IF (Access:LOAN.TryUpdate() = Level:Benign)
					IF (Access:LOANHIST.PrimeRecord() = Level:Benign)
						loh:Ref_Number = loa:Ref_Number
						loh:Date = TODAY()
						loh:Time = CLOCK()
						loh:User = p_web.GSV('BookingUserCode')
						loh:Status = 'UNIT AVAILABLE'
						IF (Access:LOANHIST.TryInsert())
							Access:LOANHIST.CancelAutoInc()
						END ! IF
					END ! IF
				END ! IF
			END ! IF
			
			
		END ! LOOP
		
		Relate:JOBS.Close()
		Relate:EXCHANGE.Close()
		Relate:LOAN.Close()
		Relate:WEBJOB.Close()
		Relate:JOBSE.Close()
		Relate:LOANHIST.Close()
		
		IF (fail)
			Do ReturnFail
		END ! IF
OpenJob             PROCEDURE()!
fail                    LONG(0)
    CODE
        Relate:JOBS.Open()
        Relate:WEBJOB.Open()
        Relate:JOBSE.Open()
		
        LOOP 1 TIMES
            IF (p_web.IfExistsValue('wobjobno'))
                Access:WEBJOB.ClearKey(wob:RecordNumberKey)
                wob:RecordNumber = p_web.GetValue('wobjobno')
                IF (Access:WEBJOB.TryFetch(wob:RecordNumberKey) = Level:Benign)
				
                ELSE ! IF
                    fail = 1
                    BREAK
                END ! IF
                
                Access:JOBS.ClearKey(job:Ref_Number_Key)
                job:Ref_Number = wob:RefNumber
                IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
				
                ELSE ! IF
                    fail = 1
                    BREAK
                END ! IF
            ELSIF (p_web.IfExistsValue('jobno'))
                Access:JOBS.ClearKey(job:Ref_Number_Key)
                job:Ref_Number = p_web.GetValue('jobno')
                IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
				
                ELSE ! IF
                    fail = 1
                    BREAK
                END ! IF
                Access:WEBJOB.ClearKey(wob:RefNumberKey)
                wob:RefNumber = job:Ref_Number
                IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
				
                ELSE ! IF
                    fail = 1
                    BREAK
                END ! IF
            ELSE
                fail = 1
                BREAK
            END ! IF
		
            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = job:Ref_Number
            IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
			
            ELSE ! IF
            END ! IF
		
            p_web.FileToSessionQueue(JOBS)
            p_web.FileToSessionQueue(WEBJOB)
            p_web.FileToSessionQueue(JOBSE)
            p_web.SSV('RedirectURL','ViewJob?wob__RecordNumber=' & wob:RecordNumber & | 
                '&Change_btn=Change&ChainTo=' & p_web.GSV('ReturnURL'))

        END ! IF
		
        Relate:JOBSE.Close()
        Relate:WEBJOB.Close()
        Relate:JOBS.Close()
		
        IF (fail = 1)
            CreateScript(packet,'alert("Unable to find job details.")')
            DO ReturnFail
        END ! IF

! Outstanding Stock Orders
OutstandingStockOrders      PROCEDURE()!
accountNumber                   STRING(30)
    CODE
        Relate:SBO_OutParts.Open()
        Relate:RETSTOCK.Open()
        Relate:RETSALES.Open()
        Relate:ORDHEAD.Open()
        Relate:ORDITEMS.Open()
        Relate:EXCHORDR.Open()
        Relate:LOAORDR.Open()
        
        STREAM(SBO_OutParts)
    
        ! Clear Existing Records
        Access:SBO_OutParts.ClearKey(sout:PartNumberKey)
        sout:SessionID = p_web.SessionID
        SET(sout:PartNumberKey,sout:PartNumberKey)
        LOOP UNTIL Access:SBO_OutParts.Next() <> Level:Benign
            IF (sout:SessionID <> p_web.SessionID)
                BREAK
            END ! IF
            Access:SBO_OutParts.DeleteRecord(0)
        END ! LOOP
        
        IF (p_web.GSV('BookingStoresAccount') = '')
            accountNumber = p_web.GSV('BookingAccount')
        ELSE
            accountNumber = p_web.GSV('BookingStoresAccount')
        END ! IF
        
        p_web.StoreValue('ordType')
        
        IF (p_web.GSV('ordType') = 'A' OR p_web.GSV('ordType') = 'S')
            ProgressBarWeb.Init(RECORDS(ORDHEAD),20)
            Access:ORDHEAD.ClearKey(orh:AccountDateKey)
            orh:Account_No = accountNumber
            SET(orh:AccountDateKey,orh:AccountDateKey)
            LOOP UNTIL Access:ORDHEAD.Next() <> Level:Benign
                IF (orh:Account_No <> accountNumber)
                    BREAK
                END ! IF
                
                ProgressBarWeb.Update('Building Stock List...')
                
                ! Count unprocessed orders first
                IF (orh:Procesed = 0)
                    ! Add up items on unprocessed order
                    Access:ORDITEMS.ClearKey(ori:KeyOrdHNo)
                    ori:OrdHNo = orh:Order_No
                    SET(ori:KeyOrdHNo,ori:KeyOrdHNo)
                    LOOP UNTIL Access:ORDITEMS.Next() <> Level:Benign
                        IF (ori:OrdHNo <> orh:Order_No)
                            BREAK
                        END ! IF
                        IF (ori:Qty = 0)
                            CYCLE
                        END ! IF
                        
                        Access:SBO_OutParts.ClearKey(sout:PartDescDateRaisedKey)
                        sout:SessioNID = p_web.SessionID
                        sout:LineType = 'S'
                        sout:PartNumber = ori:PartNo
                        sout:Description = ori:PartDiscription
                        sout:DateRaised = orh:TheDate
                        IF (Access:SBO_OutParts.TryFetch(sout:PartDescDateRaisedKey) = Level:Benign)
                            sout:Quantity += ori:Qty
                            Access:SBO_OutParts.TryUpdate()
                        ELSE ! IF
                            IF (Access:SBO_OutParts.PrimeRecord() = Level:Benign)
                                sout:SessionID = p_web.SessionID
                                sout:PartNumber = ori:PartNo
                                sout:Description = ori:PartDiscription
                                sout:DateRaised = orh:TheDate
                                sout:Quantity = ori:Qty
                                sout:DateProcessed = 0
                                sout:LineType = 'S'
                                IF (Access:SBO_OutParts.TryInsert())
                                    Access:SBO_OutParts.CancelAutoInc()
                                END ! IF
                            END ! IF
                        END ! IF
                    END ! LOOP
                ELSE ! IF (orh:Procesed = 0)
                    ! Find the retail sale associated with this web order
                    Access:RETSALES.ClearKey(ret:Ref_Number_Key)
                    ret:Ref_Number = orh:SalesNumber
                    IF (Access:RETSALES.TryFetch(ret:Ref_Number_Key) = Level:Benign)
                        ! Double check the sale has a corresponding web order number
                        IF (ret:WebOrderNumber = 0)
                            CYCLE
                        END ! IF
                        
                        ! Count the items on the sale
                        Access:RETSTOCK.ClearKey(res:Part_Number_Key)
                        res:Ref_Number = ret:Ref_Number
                        SET(res:Part_Number_Key,res:Part_Number_Key)
                        LOOP UNTIL Access:RETSTOCK.Next() <> Level:Benign
                            IF (res:Ref_Number <> ret:Ref_Number)
                                BREAK
                            END ! IF
                            ! Item should be on another sale, so don't count here
                            IF (res:Despatched = 'OLD')
                                CYCLE
                            END ! IF
                            IF (res:Despatched = 'CAN')
                                CYCLE
                            END ! IF
                            IF (res:Received <> 0)
                                CYCLE
                            END ! IF
                            Access:SBO_OutParts.ClearKey(sout:PartDescDateRaisedProcessedKey)
                            sout:SessionID = p_web.SessionID
                            sout:LineType = 'S'
                            sout:PartNumber = res:Part_Number
                            sout:Description = res:Description
                            sout:DateRaised = orh:TheDate
                            sout:DateProcessed = orh:Pro_Date
                            IF (Access:SBO_OutParts.TryFetch(sout:PartDescDateRaisedProcessedKey) = Level:Benign)
                                sout:Quantity += res:Quantity
                                Access:SBO_OutParts.TryUpdate()
                            ELSE ! IF
                                IF (Access:SBO_OutParts.PrimeRecord() = Level:Benign)
                                    sout:SessionID = p_web.SessionID
                                    sout:PartNumber = res:Part_Number
                                    sout:Description = res:Description
                                    sout:DateRaised = orh:TheDate
                                    sout:DateProcessed = orh:Pro_Date
                                    sout:Quantity = res:Quantity
                                    sout:LineType = 'S'
                                    IF (Access:SBO_OutParts.TryInsert())
                                        Access:SBO_OutParts.CancelAutoInc()
                                    END ! IF
                                END ! IF
                            END ! IF
                        END ! LOOP
                    ELSE ! IF
                    END ! IF
                END ! IF (orh:Procesed = 0)
            END ! LOOP
        
        END ! IF (p_web.GetValue('type') = 'A' OR p_web.GetValue('type') = 'S')
        
        IF (p_web.GSV('ordType') = 'A' OR p_web.GSV('ordType') = 'E')
            ProgressBarWeb.Init(RECORDS(EXCHORDR),20)
        
            Access:EXCHORDR.ClearKey(exo:OrderNumberKey)
            exo:Location = p_web.GSV('BookingSiteLocation')
            SET(exo:OrderNumberKey,exo:OrderNumberKey)
            LOOP UNTIL Access:EXCHORDR.Next() <> Level:Benign
                IF (exo:Location <> p_web.GSV('BookingSiteLocation'))
                    BREAK
                END ! IF
                
                ProgressBarWeb.Update('Building Exchange Orders...')
                
                IF (exo:Qty_Received < exo:Qty_Required)
                    IF (Access:SBO_OutParts.PrimeRecord() = Level:Benign)
                        sout:SessionID = p_web.SessionID
                        sout:PartNumber = exo:Model_Number
                        sout:Description = exo:Manufacturer
                        sout:Quantity = exo:Qty_Required - exo:Qty_Received
                        sout:DateRaised = exo:DateCreated
                        sout:LineType = 'E'
                        IF (Access:SBO_OutParts.TryInsert())
                            Access:SBO_OutParts.CancelAutoInc()
                        END ! IF
                    END ! IF
                END ! IF
            END ! LOOP
        END ! IF (p_web.GSV('ordType') = 'A' OR p_web.GSV('ordType') = 'E')
        
        IF (p_web.GSV('ordType') = 'A' OR p_web.GSV('ordType') = 'L')
            
            ProgressBarWeb.Init(RECORDS(LOAORDR),20)
            
            Access:LOAORDR.ClearKey(lor:OrderNumberKey)
            lor:Location = p_web.GSV('BookingSiteLocation')
            SET(lor:OrderNumberKey,lor:OrderNumberKey)
            LOOP UNTIL Access:LOAORDR.Next() <> Level:Benign
                IF (lor:Location <> p_web.GSV('BookingSiteLocation'))
                    BREAK
                END ! IF
                ProgressBarWeb.Update('Building Loan Orders....')
                
                IF (lor:Qty_Received < lor:Qty_Required)
                    IF (Access:SBO_OutParts.PrimeRecord() = Level:Benign)
                        sout:SessionID = p_web.SessionID
                        sout:PartNumber = lor:Model_Number
                        sout:Description = lor:Manufacturer
                        sout:Quantity = lor:Qty_Required - lor:Qty_Received
                        sout:DateRaised = lor:DateCreated
                        sout:LineType = 'L'
                        IF (Access:SBO_OutParts.TryInsert())
                            Access:SBO_OutParts.CancelAutoInc()
                        END ! IF
                    END ! IF
                END ! IF
            END ! LOOP
        END ! IF (p_web.GSV('ordType') = 'A' OR p_web.GSV('ordType') = 'L'
        
        FLUSH(SBO_OutParts)
        Relate:SBO_OutParts.Close()
        Relate:RETSALES.Close()
        Relate:RETSTOCK.Close()
        Relate:LOAORDR.Close()
        Relate:EXCHORDR.Close()
        Relate:ORDITEMS.Close()
        Relate:ORDHEAD.Close()
        

RapidEngineerUpdate        PROCEDURE()
    CODE
        ClearSBOGenericFile(p_web)
        
		Relate:SBO_GenericFile.Open()
		Relate:JOBS.Open()
		Relate:STATUS.Open()
		
		ProgressBarWeb.Init(1000)
        
        Access:JOBS.ClearKey(job:EngDateCompKey)
        job:Engineer = p_web.GSV('BookingUserCode')
        job:Date_Completed = 0
        SET(job:EngDateCompKey,job:EngDateCompKey)
        LOOP UNTIL Access:JOBS.Next() <> Level:Benign
            IF (job:Engineer <> p_web.GSV('BookingUserCode') OR | 
                job:Date_Completed <> 0)
                BREAK
            END ! IF
			ProgressBarWeb.Update('Building List Of Jobs....')
			
            Access:STATUS.ClearKey(sts:Ref_Number_Only_Key)
            sts:Ref_Number = job:Current_Status[1:3]
            IF (Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign)
                IF (sts:EngineerStatus)
                    IF (Access:SBO_GenericFile.PrimeRecord() = Level:Benign)
                        sbogen:SessionID = p_web.SessionID
                        sbogen:Long1 = job:Ref_Number
                        IF (Access:SBO_GenericFile.TryInsert())
                            Access:SBO_GenericFile.CancelAutoInc()
                        END! IF
                    END ! IF
                END ! IF
            END ! IF
        END ! LOOp
		
		Relate:STATUS.Close()
		Relate:JOBS.Close()
		Relate:SBO_GenericFile.Close()
ReturnExchangeUnits	PROCEDURE()!
recordCount	LONG(0)
    CODE
		
		Relate:SBO_GenericFile.Open()
		Relate:RTNORDER.Open()
		Relate:EXCHANGE.Open()
		Relate:EXCHHIST.Open()
		
		Access:SBO_GenericFile.ClearKey(sbogen:String1Key)
		sbogen:SessionID = p_web.SessionID
		SET(sbogen:String1Key,sbogen:String1Key)
		LOOP UNTIL Access:SBO_GenericFile.Next() <> Level:Benign
			IF (sbogen:SessionID <> p_web.SessionID)
				BREAK
			END ! IF
			recordCount += 1
			
		END ! LOOP
		
		IF (recordCount = 0)
			CreateScript(packet,'alert("You have not selected any I.M.E.I.s to return.")')
			DO ReturnFail
		END ! IF
		
		ProgressBarWeb.Init(recordCount)
		
		Access:SBO_GenericFile.ClearKey(sbogen:String1Key)
		sbogen:SessionID = p_web.SessionID
		SET(sbogen:String1Key,sbogen:String1Key)
		LOOP UNTIL Access:SBO_GenericFile.Next() <> Level:Benign
			IF (sbogen:SessionID <> p_web.SessionID)
				BREAK
			END ! IF
			
			ProgressBarWeb.Update('Returning I.M.E.I.s...')
			
			IF (Access:RTNORDER.PrimeRecord() = Level:Benign)
				Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
				xch:Ref_Number = sbogen:Long3
				IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key))
					CYCLE
				END ! IF
				
				xch:Available = 'RTM'
				IF (Access:EXCHANGE.TryUpdate())
					CYCLE
				END ! IF
				
				IF (Access:EXCHHIST.PrimeRecord() = Level:Benign)
					exh:Ref_Number = xch:Ref_Number
					exh:Date = TODAY()
					exh:Time = CLOCK()
					exh:User = p_web.GSV('BookingUserCode')
					exh:Status = 'RETURN ORDER GENERATED'
					exh:Notes = ''
					IF (Access:EXCHHIST.TryInsert())
						Access:EXCHHIST.CancelAutoInc()
					END ! IF
				END ! IF
				
				rtn:Location = p_web.GSV('BookingSiteLocation')
				rtn:UserCode = p_web.GSV('BookingUserCode')
				rtn:RefNumber = sbogen:Long3
				rtn:OrderNumber = sbogen:Long2
				rtn:InvoiceNumber = sbogen:Long1
				rtn:PartNumber = xch:Model_Number
				rtn:Description = xch:ESN
				rtn:QuantityReturned = 1
				rtn:ExchangeOrder = 1
				rtn:Status = 'NEW'
				rtn:Notes = ''
				rtn:ReturnType = sbogen:String2
				rtn:ExchangePrice = sbogen:Real1
				
				IF (Access:RTNORDER.TryInsert())
					Access:RTNORDER.CancelAutoInc()
				END ! IF
			END ! IF
		
		END ! LOOP
		
		Relate:EXCHHIST.Close()
		Relate:EXCHANGE.Close()
		Relate:RTNORDER.Close()
		Relate:SBO_GenericFile.Close()

ReturnLoanUnits	PROCEDURE()!
recordCount	LONG(0)
    CODE
		
		Relate:SBO_GenericFile.Open()
		Relate:RTNORDER.Open()
		Relate:LOAN.Open()
		Relate:LOANHIST.Open()
		
		Access:SBO_GenericFile.ClearKey(sbogen:String1Key)
		sbogen:SessionID = p_web.SessionID
		SET(sbogen:String1Key,sbogen:String1Key)
		LOOP UNTIL Access:SBO_GenericFile.Next() <> Level:Benign
			IF (sbogen:SessionID <> p_web.SessionID)
				BREAK
			END ! IF
			recordCount += 1
			
		END ! LOOP
		
		IF (recordCount = 0)
			CreateScript(packet,'alert("You have not selected any I.M.E.I.s to return.")')
			DO ReturnFail
		END ! IF
		
		ProgressBarWeb.Init(recordCount)
		
		Access:SBO_GenericFile.ClearKey(sbogen:String1Key)
		sbogen:SessionID = p_web.SessionID
		SET(sbogen:String1Key,sbogen:String1Key)
		LOOP UNTIL Access:SBO_GenericFile.Next() <> Level:Benign
			IF (sbogen:SessionID <> p_web.SessionID)
				BREAK
			END ! IF
			
			ProgressBarWeb.Update('Returning I.M.E.I.s...')
			
			IF (Access:RTNORDER.PrimeRecord() = Level:Benign)
				Access:LOAN.ClearKey(loa:Ref_Number_Key)
				loa:Ref_Number = sbogen:Long3
				IF (Access:LOAN.TryFetch(loa:Ref_Number_Key))
					CYCLE
				END ! IF
				
				loa:Available = 'RTM'
				IF (Access:LOAN.TryUpdate())
					CYCLE
				END ! IF
				
				IF (Access:LOANHIST.PrimeRecord() = Level:Benign)
					loh:Ref_Number = loa:Ref_Number
					loh:Date = TODAY()
					loh:Time = CLOCK()
					loh:User = p_web.GSV('BookingUserCode')
					loh:Status = 'RETURN ORDER GENERATED'
					loh:Notes = ''
					IF (Access:EXCHHIST.TryInsert())
						Access:EXCHHIST.CancelAutoInc()
					END ! IF
				END ! IF
				
				rtn:Location = p_web.GSV('BookingSiteLocation')
				rtn:UserCode = p_web.GSV('BookingUserCode')
				rtn:RefNumber = sbogen:Long3
				rtn:OrderNumber = sbogen:Long2
				rtn:InvoiceNumber = sbogen:Long1
				rtn:PartNumber = loa:Model_Number
				rtn:Description = loa:ESN
				rtn:QuantityReturned = 1
				rtn:ExchangeOrder = 2
				rtn:Status = 'NEW'
				rtn:Notes = ''
				rtn:ExchangePrice = sbogen:Real1
				
				IF (Access:RTNORDER.TryInsert())
					Access:RTNORDER.CancelAutoInc()
				END ! IF
			END ! IF
		
		END ! LOOP
		
		Relate:LOANHIST.Close()
		Relate:LOAN.Close()
		Relate:RTNORDER.Close()
		Relate:SBO_GenericFile.Close()

! RRCWarrantyInvoiceImport
RRCWarrantyInvoiceImport    PROCEDURE()
locImportFile                   Cstring(255),STATIC
ImportFile                      File,Driver('BASIC'),Pre(impfil),Name(locImportFile),Create,Bindable,Thread
Record                              Record
JobNumber                               String(30)
                                    End
                                End
recordCount                     LONG(0)
    CODE
        locImportFile = CLIP(p_web.site.webFolderPath) & '\uploads\' & CLIP(p_web.GSV('ImportFile'))
        
        ! Add the import file to the tag files, so can reuse the existing RRCWarrantyInvoice routine
        
        ClearTagFile(p_web)
        
        Relate:JOBSWARR.Open()
        Relate:TagFile.Open()
        
        OPEN(ImportFile)
        
        SET(ImportFile,0)
        LOOP
            NEXT(ImportFile)
            IF (ERROR())
                BREAK
            END ! IF
            recordCount += 1
        END ! LOOP
        
        ProgressBarWeb.Init(recordCount)
        
        SET(ImportFile,0)
        LOOP
            NEXT(ImportFile)
            IF (ERROR())
                BREAK
            END ! IF
            
            ProgressBarWeb.Update('Importing...')
            
            Access:JOBSWARR.ClearKey(jow:RefNumberKey)
            jow:RefNumber = impfil:JobNumber
            IF (Access:JOBSWARR.TryFetch(jow:RefNumberKey))
                CYCLE
            END ! IF
            
            Access:TagFile.ClearKey(tag:keyTagged)
            tag:sessionID = p_web.SessionID
            tag:taggedValue = jow:RecordNumber
            IF (Access:TagFile.TryFetch(tag:keyTagged) = Level:Benign)
                tag:tagged = 1
                Access:TagFile.TryUpdate()
            ELSE
                IF (Access:TagFile.PrimeRecord() = Level:Benign)
                    tag:sessionID = p_web.SessionID
                    tag:tagged = 1
                    tag:taggedValue = jow:RecordNumber
                    IF (Access:TagFile.TryInsert())
                        Access:TagFile.CancelAutoInc()
                    END ! IF
                    
                END ! IF
            END !IF
        END ! LOOP
        
        CLOSE(ImportFile)
        
        REMOVE(ImportFile)
        
        Relate:JOBSWARR.Close()
        Relate:TagFile.Close()
        
        RRCWarrantyInvoice()

        
! RRCWarrantyAccRejImport
RRCWarrantyAccRejImport     PROCEDURE(LONG pType)
locImportFile                   Cstring(255),STATIC
ImportFile                      File,Driver('BASIC'),Pre(impfil),Name(locImportFile),Create,Bindable,Thread
Record                              Record
JobNumber                               String(30)
                                    End
                                End
totalRecords                    LONG(0)
recordCount                     LONG(0)
    CODE
        locImportFile = CLIP(p_web.site.webFolderPath) & '\uploads\' & CLIP(p_web.GSV('ImportFile'))
        
    totalRecords = 0        
    OPEN(ImportFile)
    SET(ImportFile,0)
        LOOP
            NEXT(ImportFile)
            IF (ERROR())
                BREAK
            END ! IF
            totalRecords += 1
        END ! LOOP
        
        ProgressBarWeb.Init(totalRecords)
        
        Relate:JOBS.Open()
        Relate:JOBSWARR.Open()
        Relate:WEBJOB.Open()
        Relate:JOBSE.Open()
        Relate:SBO_WarrantyClaims.Open()
        
        SET(ImportFile,0)
        LOOP
            NEXT(ImportFile)
            IF (ERROR())
                BREAK
            END ! IF
            ProgressBarWeb.Update('Exporting...')

            Access:JOBS.Clearkey(job:Ref_Number_Key)
            job:Ref_Number = impfil:JobNumber
            If (Access:JOBS.TryFetch(job:Ref_Number_Key))
                Cycle
            End

            If (JobInUse(job:Ref_Number))
                Cycle
            End

            Access:JOBSWARR.Clearkey(jow:RefNumberKey)
            jow:RefNumber = job:Ref_Number
            If (Access:JOBSWARR.TryFetcH(jow:RefNumberKey))
                Cycle
            End
            
            IF (p_web.GSV('BookingSite') = 'RRC' AND jow:BranchID <> p_web.GSV('locBranchID'))
                CYCLE
            END ! IF


            Access:WEBJOB.Clearkey(wob:RefNumberKey)
            wob:RefNumber = job:Ref_Number
            If (Access:WEBJOB.TryFetch(wob:RefNumberKey))
                Cycle
            End

            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber = job:Ref_Number
            If (Access:JOBSE.TryFetch(jobe:RefNumberKey))
                Cycle
            End

            If (jow:RRCStatus <> 'EXC')
                Cycle
            End
            
            IF (pType = 0)
                ! AccRej
    
                if (jobe:OBFProcessed = 3)
                    jobe:OBFProcessed = 4
                else
                    job:EDI = 'REJ'
                    jow:Status = 'REJ'
                end

                wob:EDI = 'AAJ'
                jow:RRCStatus = 'AAJ'
                jow:DateFinalRejection = Today()
                jobe:WarrantyClaimStatus = 'FINAL REJECTION'
                jobe:WarrantyStatusDate = Today()
                If (Access:JOBS.TryUpdate() = Level:Benign)
                    If (Access:JOBSE.TryUpdate() = Level:Benign)
                        IF (Access:WEBJOB.TryUpdate() = Level:Benign)
                            If (Access:JOBSWARR.TryUpdate() = Level:Benign)
                                AddToAudit(p_web,job:Ref_Number,'JOB','WARRANTY CLAIM REJECTION ACKNOWLEDGED','')
                                recordCount += 1
                            End ! If (Access:JOBSWARR.TryUpdate() = Level:Benign)
                        End ! IF (Access:WEBJOB.Update() = Level:Benign)
                    End ! If (Access:JOBSE.Update() = Level:Benign)
                End
            END ! IF
            IF (pType = 1)
                ! REsubmissinon
                if (jobe:OBFProcessed = 3)
                    jobe:OBFProcessed = 0
                else
                    job:EDI = 'NO'
                    jow:Status = 'NO'
                    job:EDI_Batch_Number = ''
                    jow:Submitted += 1
                    jow:ClaimSubmitted = TODAY()
                end

                wob:EDI = 'NO'
                jow:RRCStatus = 'NO'
                jow:DateFinalRejection = Today()
                jobe:WarrantyClaimStatus = 'RESUBMITTED'
                jobe:WarrantyStatusDate = Today()
                If (Access:JOBS.TryUpdate() = Level:Benign)
                    IF (job:EDI = 'NO')
                        Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
                        jobe2:RefNumber = job:Ref_Number
                        IF (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
                            jobe2:InPendingDate = TODAY()
                            Access:JOBSE2.TryUpdate()
                        ELSE ! IF
                            If (Access:JOBSE2.PrimeRecord() = Level:Benign)
                                jobe2:RefNumber = job:Ref_Number
                                jobe2:InPendingDate = Today()
                                If (Access:JOBSE2.TryINsert())
                                    Access:JOBSE2.CancelAutoInc()
                                end
                            End ! If (Access:JOBSE2.PrimeRecord() = Level:Benign)
                        END ! IF
                        
                    END ! IF
                    
                    If (Access:JOBSE.TryUpdate() = Level:Benign)
                        IF (Access:WEBJOB.TryUpdate() = Level:Benign)
                            If (Access:JOBSWARR.TryUpdate() = Level:Benign)
                                AddToAudit(p_web,job:Ref_Number,'JOB','WARRANTY CLAIM RESUBMITTED','')
                                recordCount += 1
                            End ! If (Access:JOBSWARR.TryUpdate() = Level:Benign)
                        End ! IF (Access:WEBJOB.Update() = Level:Benign)
                    End ! If (Access:JOBSE.Update() = Level:Benign)
                End
            END ! IF
            
            ! Update Browse
            Access:SBO_WarrantyClaims.ClearKey(sbojow:JobNumberOnlyKey)
            sbojow:JobNumber = jow:RefNumber
            SET(sbojow:JobNumberOnlyKey,sbojow:JobNumberOnlyKey)
            LOOP UNTIL Access:SBO_WarrantyClaims.Next() <> Level:Benign
                IF (sbojow:JobNumber <> jow:RefNumber)
                    BREAK
                END ! IF
                IF (sbojow:RRCStatus <> jow:RRCStatus)
                    sbojow:RRCStatus = jow:RRCStatus
                    Access:SBO_WarrantyClaims.TryUpdate()
                END ! IF
            END ! LOOP
            
        END ! LOOP
        
        Relate:JOBS.Close()
        Relate:JOBSWARR.Close()
        Relate:WEBJOB.Close()
        Relate:JOBSE.Close()
        Relate:SBO_WarrantyClaims.Close()

        CLOSE(ImportFile)
        IF (recordCount = 0)
            packet = CLIP(packet) & |
                '<script>alert("No records found to import.");window.location.href="' & p_web.GSV('ReturnURL') & '";</script>'
        ELSE
            packet = CLIP(packet) & |
                '<script>alert("Record(s) Updated: "' & recordCount & ');window.location.href="' & p_web.GSV('ReturnURL') & '";</script>'
        END ! IF
        DO redirect
        
! RRCWarrantyInvoice
RRCWarrantyInvoice  PROCEDURE()
locAuditNotes           STRING(255)
    CODE        
        
        Relate:TagFile.Open()
        
        ProgressBarWeb.Init(RECORDS(TagFile))
        
        Relate:JOBSWARR.Open()
        Relate:JOBS.Open()
        Relate:WEBJOB.Open()
        Relate:INVOICE.Open()
        Relate:JOBSE.Open()
        Relate:TRADEACC.Open()
        Relate:VATCODE.Open()
        Relate:DEFAULTS.Open()
        Relate:SBO_WarrantyClaims.Open()
        
        p_web.SSV('WarrInvoiceNumber',0)
        p_web.SSV('WarrCountJobs',0)
        p_web.SSV('WarrTotalLabour',0)
        p_web.SSV('WarrTotalParts',0)
        p_web.SSV('FirstJobFound',0)
        
        Access:TagFile.ClearKey(tag:keyTagged)
        tag:sessionID = p_web.SessionID
        SET(tag:keyTagged,tag:keyTagged)
        LOOP UNTIL Access:TagFile.Next() <> Level:Benign
            IF (tag:sessionID <> p_web.SessionID)
                BREAK
            END ! IF
            
            
            ProgressBarWeb.Update('Processing...')
            
            IF (tag:tagged = 0)
                CYCLE
            END ! IF
            
            Access:JOBSWARR.ClearKey(jow:RecordNumberKey)
            jow:RecordNumber = tag:taggedValue
            IF (Access:JOBSWARR.TryFetch(jow:RecordNumberKey))
                CYCLE
            END ! IF
            
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = jow:RefNumber
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
                CYCLE
            END ! IF
            
            IF (JobInUse(job:Ref_Number))
                packet = CLIP(packet) & '<script>alert("Error! Job ' & job:Ref_Number & ' Currently In Use And Cannot Included In The Invoice");</script>'
                DO SendPacket
                CYCLE
            END ! IF
            
            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = jow:RefNumber
            IF (Access:JOBSE.TryFetch(jobe:RefNumberKey))
                CYCLE
            END ! IF
            
            Access:WEBJOB.ClearKey(wob:RefNumberKey)
            wob:RefNumber = jow:RefNumber
            IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
                CYCLE
            END !IF
            
            IF (jobe:HubRepairDate <> '')
                CYCLE
            END ! IF
            
            IF (p_web.GSV('FirstJobFound') = 0)
                ! First Job Found, create the Invoice Record
                IF (Access:INVOICE.PrimeRecord() = Level:Benign)
                    IF (Access:INVOICE.TryInsert())
                        Access:INVOICE.CancelAutoInc()
                        BREAK
                    END ! IF
                    p_web.SSV('WarrInvoiceNumber',inv:Invoice_Number)
                END ! IF
                p_web.SSV('FirstJobFound',1)
            END ! IF
            
            DO CreateRRCWarrantyInvoice          
                
        END ! LOOP
        
        SET(DEFAULTS,0)
        Access:DEFAULTS.Next()
        
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = p_web.GSV('BookingAccount')
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
            Access:VATCODE.ClearKey(vat:Vat_code_Key)
            vat:VAT_Code = tra:Labour_VAT_Code
            IF (Access:VATCODE.TryFetch(vat:Vat_code_Key) = Level:Benign)
            END ! IF            
        END ! IF
        
        IF (p_web.GSV('FirstJobFound') = 0)
            ClearTagFile(p_web)
            packet = CLIP(packet) & |
                '<script>alert("No records found to invoice.");window.location.href="' & p_web.GSV('ReturnURL') & '";</script>'
            DO redirect
            RETURN
        END ! IF
        
        
        Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
        inv:Invoice_Number = p_web.GSV('WarrInvoiceNumber')
        IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign)
            inv:Date_Created = TODAY()
            inv:Account_Number = p_web.GSV('BookingAccount')
            inv:AccountType = 'MAI'
            inv:Total = p_web.GSV('WarrTotalLabour') + p_web.GSV('WarrTotalParts')
            
            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = p_web.GSV('BookingAccount')
            IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                Access:VATCODE.ClearKey(vat:Vat_code_Key)
                vat:VAT_Code = tra:Labour_VAT_Code
                IF (Access:VATCODE.TryFetch(vat:Vat_code_Key) = Level:Benign)
                    inv:Vat_Rate_Labour = vat:VAT_Rate
                END ! IF
                Access:VATCODE.ClearKey(vat:Vat_code_Key)
                vat:VAT_Code = tra:Parts_VAT_Code
                IF (Access:VATCODE.TryFetch(vat:Vat_code_Key) = Level:Benign)
                    inv:Vat_Rate_Parts = vat:VAT_Rate
                END ! IF
            END ! IF
           
            inv:Vat_Number = def:VAT_Number
            inv:Batch_Number = 0
            inv:Manufacturer = p_web.GSV('WarrantyManufacturer')
            inv:Total_Claimed = p_web.GSV('WarrTotalLabour') + p_web.GSV('WarrTotalParts')
            inv:Labour_Paid = p_web.GSV('WarrTotalLabour')
            inv:Parts_Paid = p_web.GSV('WarrTotalParts')
            inv:Reconciled_Date = TODAY()
            inv:Invoice_Type = 'WRC'
            inv:jobs_count = p_web.GSV('WarrCountJobs')
            Access:INVOICE.TryUpdate()
            
        END ! IF
        
        Relate:DEFAULTS.Open()
        Relate:TRADEACC.Close()
        Relate:VATCODE.Close()
        Relate:JOBSE.Close()
        Relate:INVOICE.Close()
        Relate:WEBJOB.Close()
        Relate:JOBS.Close()
        Relate:JOBSWARR.Close()
        Relate:TagFile.Close()
        Relate:SBO_WarrantyClaims.Close()
        
        ClearTagFile(p_web)
!region SMSProcessReport
SMSResponseReport    PROCEDURE() 
count       LONG()
inFault STRING(30)
inFaultDescription STRING(255)
outFault STRING(30)
outFaultDescription STRING(255)

Grp   GROUP(gGetOutFaults),PRE(Grp)
    END ! IF

    CODE
        Relate:TRADEACC.Open()
        Relate:SUBTRACC.Open()
        Relate:SMSRECVD.Open()
        Relate:JOBS.Open()
        Relate:JOBNOTES.Open()
        Relate:MANFAULT.Open()
        Relate:MANFAULO.Open()
    
        count = 0
        expfil.Init('SMS Response Report','SMS Response Report ' & FORMAT(TODAY(),@d012))
        
        expfil.AddField('Date Time',1)
        expfil.AddField('MSISDN')
        expfil.AddField('Manufacturer')
        expfil.AddField('Model Number')
        expfil.AddField('Text Received')
        expfil.AddField('Job Number')
        expfil.AddField('In Fault')
        expfil.AddField('Out Fault')
        expfil.AddField('Notes')
        expfil.AddField('Franchise',,1)
        
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = p_web.GSV('BookingAccount')
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
        END ! IF

        Access:SUBTRACC.ClearKey(sub:Main_Account_Key)
        sub:Main_Account_Number = tra:Account_Number
        SET(sub:Main_Account_Key,sub:Main_Account_Key)
        LOOP UNTIL Access:SUBTRACC.Next() <> Level:Benign
            IF (sub:Main_Account_Number <> tra:Account_Number)
                BREAK
            END ! IF
            
            Access:SMSRECVD.ClearKey(smr:KeyAccount_Date_Time)
            smr:AccountNumber = sub:Account_Number
            smr:DateReceived = p_web.GSV('locStartDate')
            smr:TimeReceived = 0
            SET(smr:KeyAccount_Date_Time,smr:KeyAccount_Date_Time)
            LOOP UNTIL Access:SMSRECVD.Next() <> Level:Benign
                IF (smr:AccountNumber <> sub:Account_Number OR |
                    smr:DateReceived > p_web.GSV('locEndDate'))
                    BREAK
                END ! IF
                
                Access:JOBS.ClearKey(job:Ref_Number_Key)
                job:Ref_Number = smr:Job_Ref_Number
                IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
                    CYCLE
                END ! IF
                
                Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
                jbn:RefNumber = smr:Job_Ref_Number
                IF (Access:JOBNOTES.TryFetch(jbn:RefNumberKey))
                    jbn:Fault_Description = ''
                END ! IF
                
                inFault = ''
                inFaultDescription = ''
                Access:MANFAULT.ClearKey(maf:InFaultKey)
                maf:Manufacturer = job:Manufacturer
                maf:InFault  = 1
                IF (Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign)
                    CASE maf:Field_Number
                    OF 1
                        inFault = job:Fault_Code1
                    OF 2
                        inFault = job:Fault_Code2
                    OF 3
                        inFault = job:Fault_Code3
                    OF 4
                        inFault = job:Fault_Code4
                    OF 5
                        inFault = job:Fault_Code5
                    OF 6
                        inFault = job:Fault_Code6
                    OF 7
                        inFault = job:Fault_Code7
                    OF 8
                        inFault = job:Fault_Code8
                    OF 9
                        inFault = job:Fault_Code9
                    OF 10
                        inFault = job:Fault_Code10
                    OF 11
                        inFault = job:Fault_Code11
                    OF 12
                        inFault = job:Fault_Code12
                    END ! CASE
                    
                    Access:MANFAULO.ClearKey(mfo:Field_Key)
                    mfo:Manufacturer = job:Manufacturer
                    mfo:Field_Number = maf:Field_Number
                    mfo:Field = inFault
                    IF (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                        inFaultDescription = mfo:Description
                    ELSE ! IF
                    END ! IF
                ELSE ! IF
                END ! IF
                
                CLEAR(Grp)
                GetOutFaults(Grp,job:Record)
                
                IF (Grp.wFaultCode = '')
                    outFault = Grp.cFaultCode
                    outFaultDescription = Grp.cFaultDescription
                ELSE
                    outFault = Grp.wFaultCode
                    outFaultDescription = Grp.wFaultDescription
                END ! IF
                
                expfil.AddField(FORMAT(smr:DateReceived,@d06) & ' ' & FORMAT(smr:TimeReceived,@t1),1)
                expfil.AddField(smr:MSISDN)
                expfil.AddField(job:Manufacturer)
                expfil.AddField(job:Model_Number)
                expfil.AddField(BHStripNonAlphaNum(smr:TextReceived,' '))
                expfil.AddField(job:Ref_Number)
                expfil.AddField(CLIP(inFault) & '-' & CLIP(inFaultDescription))
                expfil.AddField(CLIP(outFault) & '-' & CLIP(outFaultDescription))
                expfil.AddField(BHStripNonAlphaNum(jbn:Fault_Description,' '))
                expfil.AddField(tra:Company_Name,,1)
                count += 1
            END ! LOOP
        END ! LOOP
        
        Relate:SUBTRACC.Close()
        Relate:TRADEACC.Close()
        Relate:SMSRECVD.Close()
        Relate:JOBNOTES.Close()
        Relate:JOBS.Close()
        Relate:MANFAULO.Close()
        Relate:MANFAULT.Close()
        
        
        IF (count = 0)
            expfil.Kill(1)
            CreateScript(packet,kNoREcordsAlert)
            
            DO ReturnFail
        ELSE ! IF
            expfil.Kill()
        END ! IF
    
!endregion    
    

SMSTaggedJobs       PROCEDURE()
countRecords            LONG(0)
    CODE
        Relate:TAGFILE.Open()
        Relate:JOBS.Open()
        Relate:JOBSE.Open()
        Relate:JOBSE2.Open()
        Relate:STATUS.Open()
        
        ProgressBarWeb.Init(RECORDS(TAGFILE))
    
        Access:TAGFILE.ClearKey(tag:KeyTagged)
        tag:SessionID = p_web.SessionID
        SET(tag:KeyTagged,tag:KeyTagged)
        LOOP UNTIL Access:TAGFILE.Next() <> Level:Benign
            IF (tag:SessionID <> p_web.SessionID)
                BREAK
            END ! IF
            countRecords += 1
            
            ProgressBarWeb.Update('Sending...')
            
            IF (tag:Tagged = 0)
                CYCLE
            END ! IF
            
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = tag:TaggedValue
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
                CYCLE
            END ! IF
            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = job:Ref_Number
            IF (Access:JOBSE.TryFetch(jobe:RefNumberKey))
                CYCLE
            END ! IF
            Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
            jobe2:RefNumber = job:Ref_Number
            IF (Access:JOBSE2.TryFetch(jobe2:RefNumberKey))
                CYCLE
            END ! IF
            Access:WEBJOB.ClearKey(wob:RefNumberKey)
            wob:RefNumber = job:Ref_Number
            IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
                CYCLE
                ! Wob is used in SMSText, but not the sessionqueue
                ! so should be save to load here, for later (Status change)
            END! I F

            p_web.FileToSessionQueue(JOBS)
            p_web.FileToSessionQueue(JOBSE)
            p_web.FileToSessionQueue(JOBSE2)
            
            CASE p_web.GSV('locJobProgressStatusFilter')
            OF 1 ! Job Status
                
                SendSMSText('J','N','N',p_web)
                
                IF (p_web.GSV('job:Current_Status') = '510 ESTIMATE READY')
                    GetStatus(520,0,'JOB',p_web)
                    Access:JOBS.ClearKey(job:Ref_Number_Key)
                    job:Ref_Number = p_web.GSV('job:Ref_Number')
                    IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
                    END ! IF
                    p_web.SessionQueueToFile(JOBS)
                    IF (Access:JOBS.TryUpdate() = Level:Benign)
                        Access:WEBJOB.ClearKey(wob:RefNumberKey)
                        wob:RefNumber = job:Ref_Number
                        IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
                        END ! IF
                        p_web.SessionQueueToFile(WEBJOB)
                        Access:WEBJOB.TryUpdate()
                        AddToAudit(p_web,job:Ref_Number,'JOB','AUTOMATED STATUS CHANGE TO: 520 ESTIMATE SENT','PREVIOUS STATUS: 510 ESTIMATE READY<13,10>NEW STATUS: 520 ESTIMATE SENT ' &|
                            '<13,10,13,10>REASON:<13,10>AUTOMATED SMS SENT')
                    END ! IF
                END ! IF
            OF 2 ! Exchange Status
                SendSMSText('E','N','N',p_web)
            OF 3 ! Loan Status
                SendSMSText('L','N','N',p_web)
            END ! CASE
            
            
            
        END ! LOOP
        
        IF (countRecords = 0)
            packet = CLIP(packet) & '<script>alert("You have not tagged any jobs.");</script>'
        END ! IF
        
        Relate:TAGFILE.Open()
        Relate:JOBS.Open()
        Relate:JOBSE.Open()
        Relate:JOBSE2.Open()
        Relate:STATUS.Open()
        
        Do Redirect
!region StatusReportCriteriaGet
StatusReportCriteriaGet        PROCEDURE()
    CODE
        ClearGenericTagFile(p_web)
        Relate:STATCRIT.Open()
        Relate:SBO_GenericTagFile.Open()
        STREAM(SBO_GenericTagFile)
        
        ProgressBarWeb.Init(RECORDS(STATCRIT))
        
        p_web.SSV('locSavedCriteria',p_web.GetValue('Criteria'))
    
        Access:STATCRIT.ClearKey(stac:LocationDescriptionKey)
        stac:Description = p_web.GSV('locSavedCriteria')
        stac:Location = p_web.GSV('BookingAccount')
        SET(stac:LocationDescriptionKey,stac:LocationDescriptionKey)
        LOOP UNTIL Access:STATCRIT.Next() <> Level:Benign
            IF (stac:Description <> p_web.GSV('locSavedCriteria') OR |
                stac:Location <> p_web.GSV('BookingAccount'))
                BREAK
            END ! IF
            
            ProgressBarWeb.Update('Applying Criteria...')
            
            IF (INLIST(stac:OptionType,kSubAccount,kGenericAccount,kStatusTypes,kInternalLocation))
                ! Fill Tag Lists
                
                Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
                tagf:SessionID      = p_web.SessionID
                tagf:TagType        = stac:OptionType
!                IF (stac:OptionType = kGenericAccount)
!                    tagf:TagType = kSubAccount
!                END ! IF
                tagf:TaggedValue    = stac:FieldValue
                IF (Access:SBO_GenericTagFile.TryFetch(tagf:KeyTagged) = Level:Benign)
                    tagf:Tagged = 1
                    Access:SBO_GenericTagFile.TryUpdate()
                ELSE ! IF
                    tagf:Tagged = 1
                    Access:SBO_GenericTagFile.TryInsert()
                END ! IF 
            ELSE
                CASE stac:OptionType
                Of 'PAPER REPORT TYPE'
                    p_web.SSV('locPaperReportType',stac:FieldValue)
                Of 'EXPORT JOBS'
                    p_web.SSV('locExportJobs',stac:FieldValue)
                Of 'EXPORT AUDIT'
                    p_web.SSV('locExportAudit',stac:FieldValue)
                Of 'EXPORT PARTS'
                    p_web.SSV('locExportCParts',stac:FieldValue)
                Of 'EXPORT WARPARTS'
                    p_web.SSV('locExportWParts',stac:FieldValue)
                Of 'STATUS TYPE'
                    p_web.SSV('locStatusType',stac:FieldValue)
                Of 'REPORT ORDER'
                    p_web.SSV('locReportOrder',stac:FieldValue)
                Of 'DATE RANGE TYPE'
                    p_web.SSV('locDateRangeType',stac:FieldValue)              
                END ! IF
            END ! 
        END ! LOOP
        FLUSH(SBO_GenericTagFile)
        Relate:STATCRIT.Close()
        Relate:SBO_GenericTagFile.Close()
!endregion
!region StatusReportCriteriaSave
StatusReportCriteriaSave    PROCEDURE()
error   LONG()
    CODE
        Relate:STATREP.Open()
        Relate:STATCRIT.Open()
        Relate:SUBTRACC.Open()
        Relate:SBO_GenericTagFile.Open()
        
        STREAM(SBO_GenericTagFile)
        
        ProgressBarWeb.Init(RECORDS(SBO_GenericTagFile))
        
        Access:STATREP.ClearKey(star:LocationDescriptionKey)
        star:Location = p_web.GSV('BookingAccount')
        star:Description = UPPER(p_web.GetValue('Criteria'))
        IF (Access:STATREP.TryFetch(star:LocationDescriptionKey) = Level:Benign)
            error = 1
        ELSE
            IF (Access:STATREP.PrimeRecord() = Level:Benign)
                star:Location = p_web.GSV('BookingAccount')
                star:Description = UPPER(p_web.GetValue('Criteria'))
                IF (Access:STATREP.TryInsert())
                    Access:STATREP.CancelAutoInc()
                ELSE
                    Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
                    tagf:SessionID = p_web.SessionID
                    SET(tagf:KeyTagged,tagf:KeyTagged)
                    LOOP UNTIL Access:SBO_GenericTagFile.Next() <> Level:Benign
                        IF (tagf:SessionID <> p_web.SessionID)
                            BREAK
                        END ! IF
                        
                        ProgressBarWeb.Update('Saving Criteria...')
                        
                        IF (tagf:Tagged <> 1)
                            CYCLE
                        END ! IF
                        
                        IF (Access:STATCRIT.PrimeRecord() = Level:Benign)
                            stac:Description = star:Description
                            stac:Location = star:Location
                            stac:OptionType = tagf:TagType
                            stac:FieldValue = tagf:TaggedValue
                            ! Had to store Generic And Sub Accounts under the same TagType
                            ! So double check the account type so that they are stored 
                            ! correctly in the criteria file.
!                            IF (tagf:TagType = kSubAccount)
!                                Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
!                                sub:Account_Number = tagf:TaggedValue
!                                IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
!                                    IF (sub:Generic_Account = 1)
!                                        stac:OptionType = kGenericAccount
!                                    END ! IF
!                                
!                                END ! IF
!                            END !I F
                            IF (Access:STATCRIT.TryInsert())
                                Access:STATCRIT.CancelAutoInc()
                            END ! IF
                        END ! IF
                        
                    END ! LOOP
                    
                    IF (Access:STATCRIT.PrimeRecord() = Level:Benign)
                        stac:Description = star:Description
                        stac:Location = star:Location
                        stac:OptionType = 'PAPER REPORT TYPE'
                        stac:FieldValue = p_web.GSV('locPaperReportType')
                        IF (Access:STATCRIT.TryInsert())
                            Access:STATCRIT.CancelAutoInc()
                        END ! IF
                    END ! IF                        
                        
                    IF (Access:STATCRIT.PrimeRecord() = Level:Benign)
                        stac:Description = star:Description
                        stac:Location = star:Location
                        stac:OptionType = 'EXPORT JOBS'
                        stac:FieldValue = p_web.GSV('locExportJobs')
                        IF (Access:STATCRIT.TryInsert())
                            Access:STATCRIT.CancelAutoInc()
                        END ! IF
                    END ! IF                        
                    IF (Access:STATCRIT.PrimeRecord() = Level:Benign)
                        stac:Description = star:Description
                        stac:Location = star:Location
                        stac:OptionType = 'EXPORT AUDIT'
                        stac:FieldValue = p_web.GSV('locExportAudit')
                        IF (Access:STATCRIT.TryInsert())
                            Access:STATCRIT.CancelAutoInc()
                        END ! IF
                    END ! IF                        
                    IF (Access:STATCRIT.PrimeRecord() = Level:Benign)
                        stac:Description = star:Description
                        stac:Location = star:Location
                        stac:OptionType = 'EXPORT PARTS'
                        stac:FieldValue = p_web.GSV('locExportCParts')
                        IF (Access:STATCRIT.TryInsert())
                            Access:STATCRIT.CancelAutoInc()
                        END ! IF
                    END ! IF                        
                    IF (Access:STATCRIT.PrimeRecord() = Level:Benign)
                        stac:Description = star:Description
                        stac:Location = star:Location
                        stac:OptionType = 'EXPORT WARPARTS'
                        stac:FieldValue = p_web.GSV('locExportWParts')
                        IF (Access:STATCRIT.TryInsert())
                            Access:STATCRIT.CancelAutoInc()
                        END ! IF
                    END ! IF                        
                    IF (Access:STATCRIT.PrimeRecord() = Level:Benign)
                        stac:Description = star:Description
                        stac:Location = star:Location
                        stac:OptionType = 'STATUS TYPE'
                        stac:FieldValue = p_web.GSV('locStatusType')
                        IF (Access:STATCRIT.TryInsert())
                            Access:STATCRIT.CancelAutoInc()
                        END ! IF
                    END ! IF                                            
                    IF (Access:STATCRIT.PrimeRecord() = Level:Benign)
                        stac:Description = star:Description
                        stac:Location = star:Location
                        stac:OptionType = 'REPORT ORDER'
                        stac:FieldValue = p_web.GSV('locReportOrder')
                        IF (Access:STATCRIT.TryInsert())
                            Access:STATCRIT.CancelAutoInc()
                        END ! IF
                    END ! IF                                        
                    IF (Access:STATCRIT.PrimeRecord() = Level:Benign)
                        stac:Description = star:Description
                        stac:Location = star:Location
                        stac:OptionType = 'DATE RANGE TYPE'
                        stac:FieldValue = p_web.GSV('locDateRangeType')
                        IF (Access:STATCRIT.TryInsert())
                            Access:STATCRIT.CancelAutoInc()
                        END ! IF
                    END ! IF                                            
                END ! IF
                
            END ! IF
        END ! IF
        Relate:STATCRIT.Close()
        Relate:STATREP.Close()
        Relate:SUBTRACC.Close()
        FLUSH(SBO_GenericTagFile)
        Relate:SBO_GenericTagFile.Close()
        
        IF (error = 1)
            CreateScript(packet,'alert("Error! The selected description has already been used.");window.open("FormStatusReportCriteria?tab=6&ShowCriteria=1","_self")')    
        ELSE
            CreateScript(packet,'alert("Criteria Saved.");window.open("FormStatusReportCriteria?tab=6","_self")')    
        END ! IF
 !endregion
!region StatusReportEngine
StatusReportEngine  PROCEDURE()
                    MAP
ProcessJobs             PROCEDURE(KEY pKeyName,<LONG pDateField>)   
BuildStatusQueue        PROCEDURE(STRING pStatus,LONG pStatusType)    
GetDays                 PROCEDURE(STRING pSat,STRING pSun,DATE pStart),LONG     
                    END
recs                    LONG()
i                       LONG()    
countRecords            LONG()
    CODE
        ProgressbarWeb.Init(1000)
        
        ! Clear Report Q
        recs = RECORDS(qStatusReport)
        LOOP i = recs TO 1 BY -1
            GET(qStatusReport,i)
            IF (qStatusReport.SessionID = p_web.SessionID)
                DELETE(qStatusReport)
            END ! IF 
        END ! LOOP
        recs = RECORDS(qStatusReportAudit)
        LOOP i = recs TO 1 BY -1
            GET(qStatusReportAudit,i)
            IF (qStatusReportAudit.SessionID = p_web.SessionID)
                Delete(qStatusReportAudit)
            END ! IF
        END ! LOOP
        recs = RECORDS(qStatusReportCParts)
        LOOP i = recs TO 1 BY -1
            GET(qStatusReportCParts,i)
            IF (qStatusReportCParts.SessionID = p_web.SessionID)
                DELETE(qStatusReportCParts)
            END ! IF
        END ! LOOP
        recs = RECORDS(qStatusReportWParts)
        LOOP i = recs TO 1 BY -1
            GET(qStatusReportWParts,i)
            IF (qStatusReportWParts.SessionID = p_web.SessionID)
                DELETE(qStatusReportWParts)
            END ! IF
        END ! LOOP
    
        Relate:JOBS.Open()
        Relate:WEBJOB.Open()
        Relate:SUBTRACC.Open()
        Relate:AUDIT.Open()
        Relate:PARTS.Open()
        Relate:WARPARTS.Open()
        Relate:JOBSE.Open()
        Relate:TRADEACC.Open()
        Relate:SBO_GenericTagFile.Open()

        countRecords = 0

        CASE p_web.GSV('locReportOrder')
        OF 'JOB NUMBER'
            ProcessJobs(job:Ref_Number_Key)
        OF 'I.M.E.I. NUMBER'
            ProcessJobs(job:ESN_Key)
        OF 'ACCOUNT NUMBER'
            ProcessJobs(job:AccountNumberKey)
        OF 'MODEL NUMBER'
            ProcessJobs(job:Model_Number_Key)
        OF 'STATUS'
            CASE p_web.GSV('locStatusType')
            OF 0 ! Job Status
                ProcessJobs(job:By_Status)
            OF 1 ! Exchange
                ProcessJobs(job:ExcStatusKey)
            OF 2 ! Loan Status
                ProcessJobs(job:LoanStatusKey)
            END ! CASE
        OF 'MSN'
            ProcessJobs(job:MSN_Key)
        OF 'SURNAME'
            ProcessJobs(job:Surname_Key)
        OF 'ENGINEER'
            ProcessJobs(job:Engineer_Key)
        OF 'MOBILE NUMBER'
            ProcessJobs(job:MobileNumberKey)
        OF 'DATE BOOKED'
            IF (p_web.GSV('locDateRangeType') = 0)
                ProcessJobs(job:Date_Booked_Key,1)
            ELSE ! IF
                ProcessJobs(job:Date_Booked_Key)
            END ! IF
        OF 'DATE COMPLETED'
            IF (p_web.GSV('locDateRangeType') = 1)
                ProcessJobs(job:DateCompletedKey,2)
            ELSE ! IF
                ProcessJobs(job:DateCompletedKey)
            END ! IF
        END ! CASE

        Relate:WEBJOB.Close()
        Relate:JOBS.Close()
        Relate:SUBTRACC.Close()
        Relate:AUDIT.Close()
        Relate:WARPARTS.Close()
        Relate:PARTS.Close()
        Relate:JOBSE.Close()
        Relate:TRADEACC.Close()
        Relate:SBO_GenericTagFile.Close()
        
        IF (countRecords = 0)
            CreateScript(packet,kNoREcordsAlert)
            DO ReturnFail
        ELSE ! IF
            CASE p_web.GetValue('type')
            OF 'E'
                StatusReportExport()
            OF 'P'
                ! Print Report
                IF (p_web.GSV('locPaperReportType') = 1)
                    !CreateScript(packet,'window.open("SummaryStatusReport","_self")')  
                    CallReport('SummaryStatusReport')
                ELSE !
                    !CreateScript(packet,'window.open("StatusReport","_self")')
                    p_web.SSV('RedirectURL','StatusReport')
                    CallReport('StatusReport')
                END ! 
            END ! CASE
        END ! IF

        
ProcessJobs         PROCEDURE(KEY pKeyName,<LONG pDateField>)    
qGenericAccounts        QUEUE(),PRE(qGenericAccounts)    
AccountNumber               STRING(30)
                        END ! QUEUE
qSubAccounts            QUEUE(),PRE(qSubAccounts)
AccountNumber               STRING(30)
                        END
qLocations              QUEUE(),PRE(qLocations)            
Location                    STRING(30)
                        END  
qStatusTypes            QUEUE(),PRE(qStatusTypes)
Status                      STRING(30)
                        END
    CODE
        Access:JOBS.ClearKey(pKeyName)
        CASE pDateField
        OF 1
            job:Date_Booked = p_web.GSV('locStartDate')
        OF 2
            job:Date_Completed = p_web.GSV('locStartDate')
        END ! IF        
        SET(pKeyName,pKeyName)
        LOOP UNTIL Access:JOBS.Next() <> Level:Benign
            ProgressBarWeb.Update('Building Report...')
            CASE pDateField
            OF 1
                IF (job:Date_Booked > p_web.GSV('locEndDate'))
                    BREAK
                END ! IF
            OF 2
                IF (job:Date_Completed > p_web.GSV('locEndDate'))
                    BREAK
                END ! IF
            END ! CASE
            
            ! Date Criteria
            CASE p_web.GSV('locDateRangeType')
            OF 0 ! Booked
                IF ~(INRANGE(job:Date_Booked,p_web.GSV('locStartDate'),p_web.GSV('locEndDate')))
                    CYCLE
                END ! IF
            OF 1 ! Completed
                IF ~(INRANGE(job:Date_Completed,p_web.GSV('locStartDate'),p_web.GSV('locEndDate')))
                    CYCLE
                END ! IF
            END ! CASE
            
            Access:WEBJOB.ClearKey(wob:RefNumbeRKey)
            wob:RefNumber = job:Ref_Number
            IF (Access:WEBJOB.TryFetch(wob:RefNumbeRKey))
                CYCLE
           
            END ! IF
            
            ! Only For This RRC
            IF (wob:HeadAccountNumber <> p_web.GSV('BookingAccount'))
                CYCLE
            END ! IF
            
            !Build some temp Qs of what is tagged
            Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
            tagf:SessionID = p_web.SessionID
            SET(tagf:KeyTagged,tagf:KeyTagged)
            LOOP UNTIL Access:SBO_GenericTagFile.Next() <> Level:Benign
                IF (tagf:SessionID <> p_web.SessionID)
                    BREAK
                END ! IF
                IF (tagf:Tagged <> 1)
                    CYCLE
                END ! IF
                CASE tagf:TagType
                OF kSubAccount
                    qSubAccounts.AccountNumber = tagf:TaggedValue
                    ADD(qSubAccounts)
                OF kGenericAccount
                    qGenericAccounts.AccountNumber = tagf:TaggedValue
                    ADD(qGenericAccounts)
!                    Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
!                    sub:Account_Number = tagf:TaggedValue
!                    IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
!                        IF (sub:Generic_Account = 1)
!                            qGenericAccounts.AccountNumber = sub:Account_Number
!                            ADD(qGenericAccounts)
!                        ELSE
!                            qSubAccounts.AccountNumber = sub:Account_Number
!                            ADD(qSubAccounts)
!                        END !I F
!                    END ! IF
                OF kInternalLocation
                    qLocations.Location = tagf:TaggedValue
                    ADD(qLocations)
                OF kStatusTypes
                    qStatusTypes.Status = tagf:TaggedValue
                    ADD(qStatusTypes)
                END ! CASE
            END ! LOOP
            
            IF (RECORDS(qGenericAccounts))
                qGenericAccounts.AccountNumber = job:Account_Number
                GET(qGenericAccounts,qGenericAccounts.AccountNumber)
                IF (ERROR())
                    CYCLE
                END ! IF
            END ! IF
            
            IF (RECORDS(qSubAccounts))
                qSubAccounts.AccountNumber = job:Account_Number
                GET(qSubAccounts,qSubAccounts.AccountNumber)
                IF (ERROR())
                    CYCLE
                END ! IF
            END ! IF
            
            IF (RECORDS(qStatusTypes))
                CASE p_web.GSV('locStatusType')
                OF 0 ! Job
                    qStatusTypes.Status = job:Current_Status
                OF 1 ! EXchange
                    qStatusTypes.Status = job:Exchange_Status
                OF 2 ! Loan
                    qStatusTypes.Status = job:Loan_Status
                END ! CASE
                GET(qStatusTypes,qStatusTypes.Status)
                IF (ERROR())
                    CYCLE
                END ! IF
            END ! IF
            
            IF (RECORDS(qLocations))
                qLocations.Location = job:Location
                GET(qLocations,qLocations.Location)
                IF (ERROR())
                    CYCLE
                END ! IF
            END ! IF
            
            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = p_web.GSV('BookingAccount')
            IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
                ! Fail
                
            END ! IF
            
            ! PASSED CRITERIA
            qStatusReport.SessionID = p_web.SessionID
            qStatusReport.JobNumber = job:Ref_Number
            qStatusReport.FullJobNumber = job:Ref_Number & '-' & p_web.GSV('BookingBranchID') & wob:JobNumber
            qStatusReport.OSDays = GetDays(tra:IncludeSaturday,tra:IncludeSunday,job:Date_Booked)
            qStatusReport.JDaysInStatus = GetDays(tra:IncludeSaturday,tra:IncludeSunday,wob:Current_Status_Date)
            qStatusReport.EDaysInStatus = GetDays(tra:IncludeSaturday,tra:IncludeSunday,wob:Exchange_Status_Date)
            qStatusReport.LDaysInStatus = GetDays(tra:IncludeSaturday,tra:IncludeSunday,wob:Loan_Status_Date)
            
            ADD(qStatusReport)
            
            IF (p_web.GetValue('type') = 'P')
                IF (p_web.GSV('locPaperReportType') = 1)
                    ! Status Summary Report
                    BuildStatusQueue(job:Current_Status,0)
                    BuildStatusQueue(job:Exchange_Status,1)
                    BuildStatusQueue(job:Loan_Status,2)
                END ! IF
            END ! IF            
            
            IF (p_web.GSV('locExportAudit') = 1)
                Access:AUDIT.ClearKey(aud:TypeRefKey)
                aud:Ref_Number = job:Ref_Number
                SET(aud:TypeRefKey,aud:TypeRefKey)
                LOOP UNTIL Access:AUDIT.Next() <> Level:Benign
                    IF (aud:Ref_Number <> job:Ref_Number)
                        BREAK
                    END ! IF                
                    qStatusReportAudit.SessionID = p_web.SessionID
                    qStatusReportAudit.RecordNumber = aud:Record_Number
                    ADD(qStatusReportAudit)
                END ! LOOP
            END ! IF
            
            IF (p_web.GSV('locExportCParts') = 1)
                Access:PARTS.ClearKey(par:Part_Number_Key)
                par:Ref_Number = job:Ref_Number
                SET(par:Part_Number_Key,par:Part_Number_Key)
                LOOP UNTIL Access:PARTS.Next() <> Level:Benign
                    IF (par:Ref_Number <> job:Ref_Number)
                        BREAK
                    END ! IF
                    qStatusReportCParts.SessionID = p_web.SessionID
                    qStatusReportCPArts.RecordNumber = par:Record_Number
                    ADD(qStatusReportCParts)
                END ! LOOP
            END ! IF
            IF (p_web.GSV('locExportWParts') = 1)
                Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
                wpr:Ref_Number = job:Ref_Number
                SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
                LOOP UNTIL Access:WARPARTS.Next() <> Level:Benign
                    IF (wpr:Ref_Number <> job:Ref_Number)
                        BREAK
                    END ! IF
                    qStatusReportWParts.SessionID = p_web.SessionID
                    qStatusReportWPArts.RecordNumber = wpr:Record_Number
                    ADD(qStatusReportWParts)
                END ! LOOP            
            END ! IF
            countRecords += 1
        END ! LOOP
        
BuildStatusQueue    PROCEDURE(STRING pStatus,LONG pStatusType)        
    CODE
        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = job:Ref_Number
        IF (Access:JOBSE.TryFetch(jobe:RefNumberKey))
            ! Fail
            RETURN
        END ! IF
        
        SORT(qStatusReportStatus,qStatusReportStatus.SessionID,qStatusReportStatus.StatusType,qStatusReportStatus.Status)
        qStatusReportStatus.SessionID = p_web.SessionID
        qStatusReportStatus.StatusType = pStatusType
        qStatusReportStatus.Status = pStatus
        GET(qStatusReportStatus,qStatusReportStatus.SessionID,qStatusReportStatus.StatusType,qStatusReportStatus.Status)
        IF (ERROR())
            IF (pStatusType = 0)
                IF ~(jobe:HubRepair)
                    qStatusReportStatus.RRCNumber = 1
                    qStatusReportStatus.Number = 0
                ELSE ! IF
                    qStatusReportStatus.RRCNumber = 0
                    qStatusReportStatus.Number = 1
                END ! IF
            ELSE ! IF
                qStatusReportStatus.Number = 1
            END ! IF
            ADD(qStatusReportStatus)
        ELSE ! IF
            IF (pStatusType = 0)
                IF ~(jobe:HubRepair)
                    qStatusReportStatus.RRCNumber += 1
                ELSE ! IF
                    qStatusReportStatus.Number += 1
                END ! IF
            ELSE ! IF
                qStatusReportStatus.Number += 1
            END ! IF
            PUT(qStatusReportStatus)                    
        END ! IF    
        
GetDays             PROCEDURE(STRING pSat,STRING pSun,DATE pStart)!,LONG
locCount                LONG
locDays                 LONG
    CODE
        locDays = 0
        locCount = 0

        LOOP
            locCount += 1
            IF (pStart + locCount) %7 = 0 AND pSun <> 'YES'
                CYCLE
            END
            IF (pStart + locCount) %7 = 6 AND pSat <> 'YES'
                CYCLE
            END
            locDays += 1
            IF pStart + locCount >= TODAY()
                BREAK
            END 
        END !Loop

        RETURN locDays           

!endregion        
!region StatusReportExport
StatusReportExport  PROCEDURE()
                    MAP
DateReceivedFromPUP     PROCEDURE(),DATE            
ExportJobsTitle         PROCEDURE(LONG pType)  
ExportJobs              PROCEDURE(LONG pType)  
ExportAuditTitle        PROCEDURE()
ExportAudit             PROCEDURE()
ExportCPartsTitle       PROCEDURE()
ExportCParts            PROCEDURE(LONG pFirstPart)
ExportWPartsTitle       PROCEDURE()
ExportWParts            PROCEDURE(LONG pFirstPart)
RepairLocation          PROCEDURE(),STRING
GetWOBEDIStatus         PROCEDURE(STRING pStatus),STRING 
WhoBooked               PROCEDURE(),STRING
WhoDespatched           PROCEDURE(),STRING
                    END
recs                    LONG()
i                       LONG()    
sentToHub               LONG()      
firstPart               LONG()  
    CODE
!region Processing    
        Relate:JOBS.Open()
        Relate:JOBSE.Open()
        Relate:JOBSE2.Open()
        Relate:WEBJOB.Open()
        Relate:TRADEACC.Open()
        Relate:LOCATLOG.Open()
        Relate:USERS.Open()
        Relate:JOBSCONS.Open()
        Relate:CHARTYPE.Open()
        Relate:REPTYDEF.Open()
        Relate:INVOICE.Open()
        Relate:JOBTHIRD.Open()
        Relate:EXCHANGE.Open()
        Relate:PARTS.Open()
        Relate:WARPARTS.Open()
        Relate:JOBNOTES.Open()
        Relate:MANFAULT.Open()
        Relate:AUDIT.Open()
        
        
        IF (p_web.GSV('locExportJobs') = 1)
            ProgressBarWeb.Init(RECORDS(qStatusReport))
            
            expfil.Init('Jobs Data','JOBSDATA.CSV')
            ExportJobsTitle(p_web.GSV('locExportType'))
            LOOP i = 1 TO RECORDS(qStatusReport)
                GET(qStatusReport,i)
                
                ProgressBarWeb.Update('Exporting Jobs...')
                
                IF (qStatusReport.SessionID <> p_web.SessionID)
                    CYCLE
                END ! IF
                
                ExportJobs(p_web.GSV('locExportType'))
            END ! LOOP
            expfil.Kill()
        END ! IF    

        IF (p_web.GSV('locExportAudit') = 1)
            ProgressBarWeb.Init(RECORDS(qStatusReportAudit))
            expfil2.Init('Audit Data','AUDITDATA.CSV')
            ExportAuditTitle()
            LOOP i = 1 TO RECORDS(qStatusReportAudit)
                GET(qStatusReportAudit,i)
                ProgressBarWeb.Update('Exporting Audit...')
                
                IF (qStatusReportAudit.SessionID <> p_web.SessionID)
                    CYCLE
                END ! IF
                
             
                ExportAudit()
            END ! LOOP
            
            expfil2.Kill()
        END ! IF
        IF (p_web.GSV('locExportCParts') = 1)
            ProgressBarWeb.Init(RECORDS(qStatusReportCParts))
            expfil3.Init('Parts Data','PARTSDATA.CSV')
            ExportCPartsTitle()
            firstPart = 1
            LOOP i = 1 TO RECORDS(qStatusReportCParts)
                GET(qStatusReportCParts,i)
                
                ProgressBarWeb.Update('Exporting Parts...')
                
                IF (qStatusReportCParts.SessionID <> p_web.SessionID)
                    CYCLE
                END ! IF
                
                ExportCParts(firstPart)
                firstPart = 0
            END!  LOOP
            expfil3.Kill()
        END ! IF
        IF (p_web.GSV('locExportWParts') = 1)
            ProgressBarWeb.Init(RECORDS(qStatusReportWParts))
            expfil4.Init('Warranty Parts Data','WARPARTSDATA.CSV')
            ExportWPartsTitle()
            firstPart = 1
            LOOP i = 1 TO RECORDS(qStatusReportWParts)
                GET(qStatusReportWParts,i)
                
                ProgressBarWeb.Update('Exporting Warranty Parts...')
                
                IF (qStatusReportWParts.SessionID <> p_web.SessionID)
                    CYCLE
                END ! IF
                
                ExportWParts(firstPart)
                firstPart = 0
            END!  LOOP        
            expfil4.Kill()
        END ! IF
        
        
        Relate:LOCATLOG.Close()
        Relate:TRADEACC.Close()
        Relate:WEBJOB.Close()
        Relate:JOBSE2.Close()
        Relate:JOBSE.Close()
        Relate:JOBS.Close()
        Relate:USERS.Close()
        Relate:JOBSCONS.Close()
        Relate:REPTYDEF.Close()
        Relate:CHARTYPE.Close()
        Relate:INVOICE.Close()
        Relate:EXCHANGE.Close()
        Relate:JOBTHIRD.Close()
        Relate:WARPARTS.Close()
        Relate:PARTS.Close()
        Relate:JOBNOTES.Close()
        Relate:MANFAULT.Close()
        Relate:AUDIT.Close()
!endregion        
        
!region Titles        
ExportJobsTitle     PROCEDURE(LONG pType)        
    CODE
        CASE pType
        OF 1 ! Short Version
            expfil.AddField('Job No',1)
            expfil.AddField('Date Booked')
            expfil.AddField('Date Received From PUP')
            expfil.AddField('Account Number')
            expfil.AddField('Account Name')
            expfil.AddField('Account Telephone')
            expfil.AddField('Customer')
            expfil.AddField('Phone')
            expfil.AddField('Model Number')
            expfil.AddField('I.M.E.I. Number')
            expfil.AddField('Repair Location')
            expfil.AddField('Job/Exchange')
            expfil.AddField('Status')
            expfil.AddField('Days in Status')
            expfil.AddField('Total Days')
            expfil.AddField('Location')
            expfil.AddField('Type')
            expfil.AddField('Loan')
            expfil.AddField('Eng')
            expfil.AddField('Booked By') 
            expfil.AddField('Despatched By',,1)
        OF 0 ! Long Version
            expfil.AddField('Job No',1)
            expfil.AddField('Franchise Branch Code')
            expfil.AddField('Franchise Job Number')
            expfil.AddField('Unit Type')
            expfil.AddField('Manufacturer')
            expfil.AddField('Model Number')
            expfil.AddField('Repair Location')
            expfil.AddField('Date Booked')
            expfil.AddField('Date Received From PUP')
            expfil.AddField('Current Job Status')
            expfil.AddField('Current Exch Status')
            expfil.AddField('Current Loan Status')
            expfil.AddField('Job Completed')
            expfil.AddField('Date Completed')
            expfil.AddField('Date Of Purchase')
            expfil.AddField('EDI Batch Number')
            expfil.AddField('H/O Claim Status')
            expfil.AddField('RRC Claim Status')
            expfil.AddField('Warranty Job')
            expfil.AddField('Warranty Charge Type')
            expfil.AddField('Exclude From EDI')
            expfil.AddField('Warranty Repair Type')
            expfil.AddField('Exclude From EDI')
            expfil.AddField('Warranty Invoice Number')
            expfil.AddField('Warranty Invoice Date')
            expfil.AddField('Chargeable Job')
            expfil.AddField('Chargeable Charge Type')
            expfil.AddField('Chargeable Repair Type')
            expfil.AddField('Chargeable Invoice Number')
            expfil.AddField('Chargeable Invoice Date')
            expfil.AddField('Incoming I.M.E.I. Number')
            expfil.AddField('Outgoing I.M.E.I. Number')
            expfil.AddField('Incoming M.S.N.')
            expfil.AddField('Outgoing M.S.N.')
            expfil.AddField('Unit Exchanged')
            expfil.AddField('Exchange Location')
            expfil.AddField('Exchange I.M.E.I. Number')
            expfil.AddField('Exchange M.S.N.')
            expfil.AddField('Third Party Site')
            expfil.AddField('3rd Party Charge')
            expfil.AddField('3rd Party V.A.T.')
            expfil.AddField('3rd Party Total')
            expfil.AddField('ARC Chargeable Spares Cost')
            expfil.AddField('ARC Chargeable Spares Selling')
            expfil.AddField('ARC Chargeable Labour')
            expfil.AddField('ARC Chargeable V.A.T.')
            expfil.AddField('ARC Chargeable Total Value')
            expfil.AddField('ARC Exchange Fee')
            expfil.AddField('ARC Warranty Spares Cost')
            expfil.AddField('ARC Warranty Spares Selling')
            expfil.AddField('ARC Warranty Labour')
            expfil.AddField('ARC Warranty V.A.T.')
            expfil.AddField('ARC Warranty Total Value')
            expfil.AddField('RRC Handling Fee Type')
            expfil.AddField('RRC Handling Fee Location')
            expfil.AddField('RRC Handling Fee')
            expfil.AddField('RRC Exchange Fee')
            expfil.AddField('RRC Chargeable Spares Cost')
            expfil.AddField('RRC Chargeable Spares Selling')
            expfil.AddField('RRC Chargeable Labour')
            expfil.AddField('RRC Chargeable Vat')
            expfil.AddField('RRC Chargeable Total Value')
            expfil.AddField('Lost Loan Charge')
            expfil.AddField('RRC Warranty Spares Cost')
            expfil.AddField('RRC Warranty Spares Selling')
            expfil.AddField('RRC Warranty Labour')
            expfil.AddField('RRC Warranty Vat')
            expfil.AddField('RRC Warranty Total Value')
            expfil.AddField('ARC Charge')
            expfil.AddField('ARC MarkUp')
            expfil.AddField('Account Number')
            expfil.AddField('Order Number')
            expfil.AddField('Authority Number')
            expfil.AddField('Incoming Fault Description')
            expfil.AddField('Main Out Fault Description')
            expfil.AddField('Fault Code 1')
            expfil.AddField('Fault Code 2')
            expfil.AddField('Fault Code 3')
            expfil.AddField('Fault Code 4')
            expfil.AddField('Fault Code 5')
            expfil.AddField('Fault Code 6')
            expfil.AddField('Fault Code 7')
            expfil.AddField('Fault Code 8')
            expfil.AddField('Fault Code 9')
            expfil.AddField('Fault Code 10')
            expfil.AddField('Fault Code 11')
            expfil.AddField('Fault Code 12')
            expfil.AddField('Fault Code 13')
            expfil.AddField('Fault Code 14')
            expfil.AddField('Fault Code 15')
            expfil.AddField('Fault Code 16')
            expfil.AddField('Fault Code 17')
            expfil.AddField('Fault Code 18')
            expfil.AddField('Fault Code 19')
            expfil.AddField('Fault Code 20')
            expfil.AddField('Engineers Notes')
            expfil.AddField('Invoice Text')
            expfil.AddField('Part Number')
            expfil.AddField('Description')
            expfil.AddField('Quantity')
            expfil.AddField('Cost Price')
            expfil.AddField('Cost Selling')
            expfil.AddField('Part Number')
            expfil.AddField('Description')
            expfil.AddField('Quantity')
            expfil.AddField('Cost Price')
            expfil.AddField('Cost Selling')
            expfil.AddField('Part Number')
            expfil.AddField('Description')
            expfil.AddField('Quantity')
            expfil.AddField('Cost Price')
            expfil.AddField('Cost Selling')
            expfil.AddField('Part Number')
            expfil.AddField('Description')
            expfil.AddField('Quantity')
            expfil.AddField('Cost Price')
            expfil.AddField('Cost Selling')
            expfil.AddField('Part Number')
            expfil.AddField('Description')
            expfil.AddField('Quantity')
            expfil.AddField('Cost Price')
            expfil.AddField('Cost Selling')
            expfil.AddField('Part Number')
            expfil.AddField('Description')
            expfil.AddField('Quantity')
            expfil.AddField('Cost Price')
            expfil.AddField('Cost Selling')
            expfil.AddField('Part Number')
            expfil.AddField('Description')
            expfil.AddField('Quantity')
            expfil.AddField('Cost Price')
            expfil.AddField('Cost Selling')
            expfil.AddField('Part Number')
            expfil.AddField('Description')
            expfil.AddField('Quantity')
            expfil.AddField('Cost Price')
            expfil.AddField('Cost Selling')
            expfil.AddField('Part Number')
            expfil.AddField('Description')
            expfil.AddField('Quantity')
            expfil.AddField('Cost Price')
            expfil.AddField('Cost Selling')
            expfil.AddField('Part Number')
            expfil.AddField('Description')
            expfil.AddField('Quantity')
            expfil.AddField('Cost Price')
            expfil.AddField('Cost Selling')
            expfil.AddField('Booked By')
            expfil.AddField('Despatched By',,1)        
        END ! CASE        
        
ExportAuditTitle    PROCEDURE()
    CODE
        expfil2.AddField('Job Number',1)
        expfil2.AddField('Date')
        expfil2.AddField('Time')
        expfil2.AddField('User')
        expfil2.AddField('Action')
        expfil2.AddField('Notes')
        expfil2.AddField('Type',,1)

ExportCPartsTitle   PROCEDURE()
    CODE
        expfil3.AddField('Job Number',1)
        expfil3.AddField('Part Number')
        expfil3.AddField('Description')
        expfil3.AddField('Supplier')
        expfil3.AddField('Quantity')
        expfil3.AddField('Exclude From Order')
        expfil3.AddField('Despatch Note Number')
        expfil3.AddField('Order Number')
        expfil3.AddField('Date Received')
        expfil3.AddField('Fault Codes Checked')
        expfil3.AddField('Fault Code 1')
        expfil3.AddField('Fault Code 2')
        expfil3.AddField('Fault Code 3')
        expfil3.AddField('Fault Code 4')
        expfil3.AddField('Fault Code 5')
        expfil3.AddField('Fault Code 6')
        expfil3.AddField('Fault Code 7')
        expfil3.AddField('Fault Code 8')
        expfil3.AddField('Fault Code 9')
        expfil3.AddField('Fault Code 10')
        expfil3.AddField('Fault Code 11')
        expfil3.AddField('Fault Code 12')
        expfil3.AddField('First Part',,1)
        
ExportWPartsTitle   PROCEDURE()
    CODE
        expfil4.AddField('Job Number',1)
        expfil4.AddField('Part Number')
        expfil4.AddField('Description')
        expfil4.AddField('Supplier')
        expfil4.AddField('Quantity')
        expfil4.AddField('Exclude From Order')
        expfil4.AddField('Despatch Note Number')
        expfil4.AddField('Order Number')
        expfil4.AddField('Date Received')
        expfil4.AddField('Fault Codes Checked')
        expfil4.AddField('Fault Code 1')
        expfil4.AddField('Fault Code 2')
        expfil4.AddField('Fault Code 3')
        expfil4.AddField('Fault Code 4')
        expfil4.AddField('Fault Code 5')
        expfil4.AddField('Fault Code 6')
        expfil4.AddField('Fault Code 7')
        expfil4.AddField('Fault Code 8')
        expfil4.AddField('Fault Code 9')
        expfil4.AddField('Fault Code 10')
        expfil4.AddField('Fault Code 11')
        expfil4.AddField('Fault Code 12')
        expfil4.AddField('First Part',,1)       
!endregion
!region Job Export        
ExportJobs          PROCEDURE(LONG pType)   
foundThirdParty         LONG()
cCostPriceARC           REAL
cCostPriceRRC           REAL
wCostPriceARC           REAL
wCostPriceRRC           REAL
costCourier             REAL
costParts               REAL
costLabour              REAL
vatRateLabour           REAL
vatRateParts            REAL
vat                     REAL

handlingFeeType         STRING(10)
handlingFeeLocation     STRING(3)
countParts              LONG()
    CODE
        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = qStatusReport.JobNumber
        IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
            ! Fail
            RETURN
        END ! IF            
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
            ! Fail
            RETURN
        END ! IF            
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = p_web.GSV('BookingAccount')
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
            ! Fail
        END ! IF  
        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = job:Ref_Number
        IF (Access:JOBSE.TryFetch(jobe:RefNumberKEy))
            ! Fail
            RETURN
        END ! IF

        Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
        jobe2:RefNumber = job:Ref_Number
        IF (Access:JOBSE2.TryFetch(jobe2:RefNumberKey))
            ! Fail
        END ! IF

        Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
        jbn:RefNumber = job:Ref_Number
        IF (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)
            
        ELSE ! IF
        END ! IF
        
        sentToHub = VodacomClass.JobSentToHub(job:Ref_Number)
    
        CASE pType
        OF 1 ! Short Version
            ! Job Number
            expfil.AddField(qStatusReport.FullJobNumber,1)
            ! Date Booked
            expfil.AddField(FORMAT(job:Date_Booked,@d06b))
            ! Date Received From PUP
            expfil.AddField(FORMAT(DateReceivedFromPUP(),@d06b))
            ! Account No
            expfil.AddField(job:Account_Number)
            ! Company Name
            expfil.AddField(job:Company_Name)
            ! Telephone
            expfil.AddField(job:Telephone_Number)
            ! Customer
            expfil.AddField(CLIP(job:Title) & ' ' & CLIP(job:Initial) & ' ' & CLIP(job:Surname))
            ! End User Tel
            expfil.AddField(jobe:EndUserTelNo)
            ! Model Number
            expfil.AddField(job:Model_Number)
            ! IMEI
            expfil.AddField('''' & job:ESN)
            ! Repair Location
            expfil.AddField(RepairLocation())
            ! Status & Days In Status
            IF (job:Exchange_Unit_Number > 0)
                expfil.AddField('E')
                expfil.AddField(wob:Exchange_Status)
                IF (wob:Exchange_Status_Date = 0)
                    expfil.AddField()
                ELSE ! IF   
                    expfil.AddField(qStatusReport.EDaysInStatus)
                END ! IF                
            ELSE ! IF
                expfil.AddField('J')
                expfil.AddField(wob:Current_Status)
                ! Days In Status?
                IF (wob:Current_Status_Date = 0)
                    expfil.AddField()
                ELSE ! IF   
                    expfil.AddField(qStatusReport.JDaysInStatus)
                END ! IF
            END ! IF
            expfil.AddField(qStatusReport.OSDays)
            ! Location
            expfil.AddField(job:Location)
            ! Job Type
            IF (job:Warranty_Job = 'YES')
                IF (job:Chargeable_Job = 'YES')
                    expfil.AddField('S')
                ELSE ! IF
                    expfil.AddField('W')
                END! I F
            ELSE ! IF
                IF (job:Chargeable_Job = 'YES')
                    expfil.AddField('C')
                ELSE ! IF
                    expfil.AddField('N')
                END ! IF
            END ! IF
            ! Loan Unit
            IF (job:Loan_Unit_Number > 0)
                expfil.AddField('YES')
            ELSE
                expfil.AddField('NO')
            END ! IF
            ! Engineer
            expfil.AddField(job:Engineer)
            ! Who Booked
            expfil.AddField(WhoBooked())
            ! Who Despatched To Customer
            expfil.AddField(WhoDespatched(),,1)
        OF 0 ! Long Version
            ! Job Number
            expfil.AddField(job:Ref_Number,1)
            ! ID    
            expfil.AddField(tra:BranchIdentification)
            ! Branch Job
            expfil.AddField(wob:JobNumber)
            ! Unit Type
            expfil.AddField(job:Unit_Type)
            ! Manufacturer
            expfil.AddField(job:Manufacturer)
            ! Model Number
            expfil.AddField(job:Model_Number)
            ! Repair Location
            expfil.AddField(RepairLocation())
            ! Date Booked
            expfil.AddField(FORMAT(job:Date_Booked,@d06b))
            ! Date Received From PUP
            expfil.AddField(FORMAT(DateReceivedFromPUP(),@d06b))
            ! Status
            expfil.AddField(job:Current_Status)
            expfil.AddField(job:Exchange_Status)
            expfil.AddField(job:Loan_Status)
            ! Job Completed?
            IF (job:Date_Completed > 0)
                expfil.AddField('YES')
            ELSE
                expfil.AddField('NO')
            END ! IF
            expfil.AddField(FORMAT(job:Date_Completed,@d06b))
            ! DOP
            expfil.AddField(FORMAT(job:DOP,@d06b))
            ! Batch Number (NOT USED)
            expfil.AddField(job:EDI_Batch_Number)
            ! Warranty Claim Status
            expfil.AddField(jobe:WarrantyClaimStatus)
            ! WOB
            expfil.AddField(GetWOBEDIStatus(wob:EDI))
            ! Warranty Details
            IF (job:Warranty_Job = 'YES')
                expfil.AddField('YES')
                expfil.AddField(job:Warranty_Charge_Type)
                Access:CHARTYPE.ClearKey(cha:Warranty_Key)
                cha:Warranty = 'YES'
                cha:Charge_Type= job:Warranty_Charge_Type
                IF (Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign)
                    expfil.AddField(cha:Exclude_EDI)
                ELSE ! IF
                    expfil.AddField('NO')
                END ! IF
                expfil.AddField(job:Repair_Type_Warranty)
                
                Access:REPTYDEF.ClearKey(rtd:Repair_Type_Key)
                rtd:Repair_Type = job:Repair_Type_Warranty
                IF (Access:REPTYDEF.TryFetch(rtd:Repair_Type_Key) = Level:Benign)
                    IF (rtd:ExcludeFromEDI)
                        expfil.AddField('YES')
                    ELSE
                        expfil.AddField('NO')
                    END ! IF
                ELSE ! IF
                    expfil.AddField()
                END ! IF
            ELSE ! IF
                expfil.AddField('NO')
                expfil.AddField(,,,4)
            END ! IF
            ! Warranty Invoice Number
            expfil.AddField(job:Invoice_Number_Warranty)
            IF (job:Invoice_Number_Warranty > 0)
                Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
                inv:Invoice_NUmber = job:Invoice_Number_Warranty
                IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign)
                    expfil.AddField(FORMAT(inv:Date_Created,@d06b))
                ELSE ! IF
                    expfil.AddField()
                END ! IF
            ELSE ! IF
                expfil.AddField()
            END ! IF
            ! Chargeable Invoice Number
            IF (job:Chargeable_Job = 'YES')
                expfil.AddField('YES')
                expfil.AddField(job:Charge_Type)
                expfil.AddField(job:Repair_Type)
            ELSE ! IF
                expfil.AddField('NO')
                expfil.AddField(,,,2)
            END ! IF
            expfil.AddField(job:Invoice_Number)
            IF (job:Invoice_Number > 0)
                Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
                inv:Invoice_NUmber = job:Invoice_Number
                IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign)
                    expfil.AddField(FORMAT(inv:Date_Created,@d06b))
                ELSE ! IF
                    expfil.AddField()
                END ! IF
            ELSE ! IF
                expfil.AddField()
            END ! IF            
            ! IMEI & MSN
            foundThirdParty = FALSE
            Access:JOBTHIRD.ClearKey(jot:RefNUmberKey)
            jot:RefNumber = job:Ref_Number
            SET(jot:RefNUmberKey,jot:RefNUmberKey)
            LOOP UNTIL Access:JOBTHIRD.Next() <> Level:Benign
                IF (jot:RefNumber <> job:Ref_Number)
                    BREAK
                END ! IF
                expfil.AddField('''' & jot:OriginalIMEI)
                expfil.AddField('''' & job:ESN)
                expfil.AddField('''' & jot:OriginalMSN)
                expfil.AddField('''' & job:MSN)
                foundThirdParty = TRUE
                BREAK
            END ! LOOP
            IF (foundThirdParty = FALSE)
                expfil.AddField('''' & job:ESN)
                expfil.AddField('''' & job:ESN)
                expfil.AddField('''' & job:MSN)
                expfil.AddField('''' & job:MSN)
            END ! IF
            ! Exchange Unit
            IF (job:Exchange_Unit_Number > 0)
                expfil.AddField('YES')
                Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                xch:Ref_NUmber = job:Exchange_Unit_Number
                IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                    expfil.AddField(xch:Location)
                    expfil.AddField('''' & xch:ESN)
                    expfil.AddField('''' & xch:MSN)
                ELSE ! IF
                    expfil.AddField(,,,3)
                END ! IF
            ELSE ! IF
                expfil.AddField('NO')
                expfil.AddField(,,,3)
            END ! IF
            ! Third Party
            IF (job:Third_Party_Site <> '')
                expfil.AddField(job:Third_Party_Site)
                expfil.AddField(jobe:ARC3rdPartyCost)
                expfil.AddField(jobe:ARC3rdPartyVAT)
                expfil.AddField(Round(jobe:ARC3rdPartyCost,.01) + Round(jobe:ARC3rdPartyVAT,.01))
            ELSE ! IF
                expfil.AddField(,,,4)
            END ! IF
            ! Parts
            cCostPriceARC = 0
            cCostPriceARC = 0
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number = job:Ref_Number
            SET(par:Part_Number_Key,par:Part_Number_Key)
            LOOP UNTIL Access:PARTS.Next() <> Level:Benign
                IF (par:Ref_Number <> job:Ref_Number)
                    BREAK
                END ! IF
                cCostPriceARC += par:AveragePurchaseCost
                cCostPriceRRC += par:RRCAveragePurchaseCost
            END ! LOOP
            
            wCostPriceARC = 0
            wCostPriceRRC = 0
            Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
            wpr:Ref_Number = job:Ref_Number
            SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
            LOOP UNTIL Access:WARPARTS.Next() <> Level:Benign
                IF (wpr:Ref_Number <> job:Ref_Number)
                    BREAK
                END ! IF
                wCostPriceARC += wpr:AveragePurchaseCost
                wCostPriceRRC += wpr:RRCAveragePurchaseCost
            END ! LOOP@            
            ! ARC Costs
            IF (job:Chargeable_Job = 'YES')
                IF (sentToHub)
                    costCourier = job:Courier_Cost
                    costParts = job:Parts_Cost
                    costLabour = job:Labour_Cost
                    
                    IF (job:Estimate = 'YES' AND job:Invoice_Number = 0 AND ~jobe:WebJob)
                        costCourier = job:Courier_Cost_Estimate
                        costParts = job:Parts_Cost_Estimate
                        costLabour = job:Labour_Cost_Estimate
                    END ! IF
                    
                    vatRateLabour = VodacomClass.VatRate(job:Account_Number,'L')
                    vatRateParts = VodacomClass.VatRate(job:Account_Number,'P')
                    
                    IF (job:Invoice_NUmber > 0)
                        Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
                        inv:Invoice_NUmber = job:Invoice_Number
                        IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign)
                            costCourier = job:Invoice_Courier_Cost
                            costParts = job:Invoice_Parts_Cost
                            costLabour = job:Invoice_Labour_Cost        
                            vatRateLabour = inv:Vat_Rate_Labour
                            vatRateParts = inv:Vat_Rate_Parts
                        ELSE ! IF
                        END ! IF
                    END ! IF
                    
                    expfil.AddField(FORMAT(cCostPriceARC,@n_14.2))
                    expfil.AddField(FORMAT(costParts,@n_14.2))
                    expfil.AddField(FORMAT(costLabour,@n_14.2))
                    vat = (costLabour * vatRateLabour/100) + |
                        (costParts * vatRateParts/100) + |
                        (costCourier * vatRateLabour/100)
                    expfil.AddField(FORMAT(vat,@n_14.2))
                    expfil.AddField(FORMAT(costParts + costLabour + costCourier + vat,@n_14.2))
                ELSE ! IF (sentToHub)
                    expfil.AddField(,,,5)
                END ! IF
            ELSE ! IF
                expfil.AddField(,,,5)
            END ! IF
            ! ARC Exchange Fee
            IF (job:Exchange_Unit_Number > 0 AND ~jobe:ExchangedAtRRC)
                expfil.AddField(FORMAT(job:Courier_Cost_Warranty,@n_14.2))
            ELSE ! IF
                expfil.AddField()
            END ! IF
            
            IF (job:Warranty_Job = 'YES')
                IF (sentToHub)
                    costCourier = job:Courier_Cost
                    costParts = jobe:ClaimPartsCost
                    costLabour = jobe:ClaimValue
                    vatRateLabour = VodacomClass.VatRate(job:Account_Number,'L')
                    vatRateParts = VodacomClass.VatRate(job:Account_Number,'P')
                    
                    IF (job:Invoice_Number_Warranty > 0)
                        Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
                        inv:Invoice_NUmber = job:Invoice_Number_Warranty
                        IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign)
                            costCourier = job:WInvoice_Courier_Cost
                            costParts = jobe:InvClaimPartsCost
                            costLabour = jobe:InvoiceClaimValue        
                            vatRateLabour = inv:Vat_Rate_Labour
                            vatRateParts = inv:Vat_Rate_Parts
                        ELSE ! IF
                        END ! IF
                    END ! IF
                    
                    expfil.AddField(FORMAT(wCostPriceARC,@n_14.2))
                    expfil.AddField(FORMAT(costParts,@n_14.2))
                    expfil.AddField(FORMAT(costLabour,@n_14.2))
                    vat = (costLabour * vatRateLabour/100) + |
                        (costParts * vatRateParts/100) + |
                        (costCourier * vatRateLabour/100)
                    expfil.AddField(FORMAT(vat,@n_14.2))
                    expfil.AddField(FORMAT(costParts + costLabour + costCourier + vat,@n_14.2))
                ELSE ! IF (sentToHub)
                    expfil.AddField(,,,5)
                END ! IF
            ELSE ! IF
                expfil.AddField(,,,5)
            END ! IF     
            ! Handling Fee Type
            handlingFeeType = 'REPAIR'
            IF (sentToHub)
                handlingFeeLocation = 'ARC'
            ELSE
                handlingFeeLocation = 'RRC'
            END ! IF
            IF (job:Exchange_Unit_Number > 0)
                IF (jobe:ExchangedAtRRC)
                    handlingFeeLocation = 'RRC'
                ELSE 
                    handlingFeeLocation = 'ARC'
                END ! IF
            END ! IF
            IF (jobe:OBFValidateDate > 0)
                handlingFeeType = 'OBF'
            END ! IF
            expfil.AddField(handlingFeeType)
            expfil.AddField(handlingFeeLocation)
            ! Exchange Rate/Handling Fee
            IF (jobe:WebJob)
                IF (sentToHub)
                    IF (job:Exchange_Unit_Number > 0)
                        IF (jobe:ExchangedATRRC)
                            expfil.AddField()
                            expfil.AddField(jobe:ExchangeRate)
                        ELSE ! IF
                            expfil.AddField(jobe:HandlingFee)
                            expfil.AddField()
                        END ! IF
                    ELSE ! IF
                        expfil.AddField(jobe:HandlingFee)
                        expfil.AddField()
                    END ! IF
                ELSE ! IF
                    expfil.AddField(,,,2)
                END!  IF
            ELSE ! IF
                expfil.AddField(,,,2)
            END ! IF
            ! RRC Costs
            IF (job:Chargeable_Job = 'YES')
                IF (jobe:WebJob)
                    costCourier = job:Courier_Cost
                    costParts = jobe:RRCCPartsCost
                    costLabour = jobe:RRCCLabourCost
                    
                    IF (job:Estimate = 'YES' AND job:Invoice_Number = 0 AND ~jobe:WebJob)
                        costCourier = job:Courier_Cost_Estimate
                        costParts = jobe:RRCEPartsCost
                        costLabour = jobe:RRCELabourCost
                    END ! IF
                    
                    vatRateLabour = VodacomClass.VatRate(job:Account_Number,'L')
                    vatRateParts = VodacomClass.VatRate(job:Account_Number,'P')
                    
                    IF (job:Invoice_NUmber > 0)
                        Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
                        inv:Invoice_NUmber = job:Invoice_Number
                        IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign)
                            IF (inv:RRCInvoiceDate <> '')
                                costCourier = job:Invoice_Courier_Cost
                                costParts = jobe:InvRRCCPartsCost
                                costLabour = jobe:InvRRCCLabourCost        
                                vatRateLabour = inv:Vat_Rate_Labour
                                vatRateParts = inv:Vat_Rate_Parts
                            END ! IF
                        ELSE ! IF
                        END ! IF
                    END ! IF
                    
                    expfil.AddField(FORMAT(cCostPriceRRC,@n_14.2))
                    expfil.AddField(FORMAT(costParts,@n_14.2))
                    expfil.AddField(FORMAT(costLabour,@n_14.2))
                    vat = (costLabour * vatRateLabour/100) + |
                        (costParts * vatRateParts/100) + |
                        (costCourier * vatRateLabour/100)
                    expfil.AddField(FORMAT(vat,@n_14.2))
                    expfil.AddField(FORMAT(costParts + costLabour + costCourier + vat,@n_14.2))
                    expfil.AddField(FORMAT(costCourier,@n_14.2))
                ELSE ! IF (sentToHub)
                    expfil.AddField(,,,6)
                END ! IF
            ELSE ! IF
                expfil.AddField(,,,6)
            END ! IF 
            IF (job:Warranty_Job = 'YES')
                IF ~(sentToHub)
                    costParts = jobe:RRCWPartsCost
                    costLabour = jobe:RRCWLabourCost
                    vatRateLabour = VodacomClass.VatRate(job:Account_Number,'L')
                    vatRateParts = VodacomClass.VatRate(job:Account_Number,'P')
                    
                    IF (wob:RRCWInvoiceNumber  > 0)
                        Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
                        inv:Invoice_NUmber = wob:RRCWInvoiceNumber 
                        IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign)
                            costParts = jobe:InvRRCWPartsCost
                            costLabour = jobe:InvRRCWLabourCost       
                            vatRateLabour = inv:Vat_Rate_Labour
                            vatRateParts = inv:Vat_Rate_Parts
                        ELSE ! IF
                        END ! IF
                    END ! IF
                    
                    expfil.AddField(FORMAT(wCostPriceRRC,@n_14.2))
                    expfil.AddField(FORMAT(costParts,@n_14.2))
                    expfil.AddField(FORMAT(costLabour,@n_14.2))
                    vat = (costLabour * vatRateLabour/100) + |
                        (costParts * vatRateParts/100) + |
                        (costCourier * vatRateLabour/100)
                    expfil.AddField(FORMAT(vat,@n_14.2))
                    expfil.AddField(FORMAT(costParts + costLabour + costCourier + vat,@n_14.2))
                ELSE ! IF (sentToHub)
                    expfil.AddField(,,,5)
                END ! IF
            ELSE ! IF
                expfil.AddField(,,,5)
            END ! IF
            ! ARC Charge & ARC Markup
            IF (job:Chargeable_Job = 'YES')
                IF (jobe:WebJob AND sentToHub)
                    expfil.AddField(FORMAT(job:Courier_Cost + job:Parts_Cost + job:Labour_Cost + jobe:ARC3rdPartyCost,@n_14.2))
                    expfil.AddField(FORMAT(jobe:RRCCLabourCost + jobe:RRCCPartsCost + job:Courier_Cost,@n_14.2)) ! This doesn't make sense?
                ELSE
                    expfil.AddField(,,,2)
                END ! IF
            ELSE ! IF
                expfil.AddField(,,,2)
            END ! IF
            ! Account Number
            expfil.AddField(job:Account_Number)
            ! Order Number
            expfil.AddField(job:Order_Number)
            ! Authority Number
            expfil.AddField(job:Authority_Number)
            ! Fault Description
            expfil.AddField(BHStripNonAlphaNum(jbn:Fault_Description,' '))
            !Lets hope that the main out fault is written in the fault code field.
            Access:MANFAULT.ClearKey(maf:MainFaultKey)
            maf:Manufacturer = job:Manufacturer
            maf:MainFault    = 1
            If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                !Found
                Access:MANFAULO.ClearKey(mfo:Field_Key)
                mfo:Manufacturer = job:Manufacturer
                mfo:Field_Number = maf:Field_Number
                Case maf:Field_Number
                Of 1
                    mfo:Field        = job:Fault_Code1
                Of 2
                    mfo:Field        = job:Fault_Code2
                Of 3
                    mfo:Field        = job:Fault_Code3
                Of 4
                    mfo:Field        = job:Fault_Code4
                Of 5
                    mfo:Field        = job:Fault_Code5
                Of 6
                    mfo:Field        = job:Fault_Code6
                Of 7
                    mfo:Field        = job:Fault_Code7
                Of 8
                    mfo:Field        = job:Fault_Code8
                Of 9
                    mfo:Field        = job:Fault_Code9
                Of 10
                    mfo:Field        = job:Fault_Code10
                Of 11
                    mfo:Field        = job:Fault_Code11
                Of 12
                    mfo:Field        = job:Fault_Code12
                End !Case maf:Field_Number
                If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                    !Found
                    expfil.AddField(BHStripNonAlphaNum(mfo:Description,' '))
                Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                    !Error
                    expfil.AddField()
                End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
            Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                !Error
                expfil.AddField()
            End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign            
            ! Fault Codes
            expfil.AddField(job:Fault_Code1)
            expfil.AddField(job:Fault_Code2)
            expfil.AddField(job:Fault_Code3)
            expfil.AddField(job:Fault_Code4)
            expfil.AddField(job:Fault_Code5)
            expfil.AddField(job:Fault_Code6)
            expfil.AddField(job:Fault_Code7)
            expfil.AddField(job:Fault_Code8)
            expfil.AddField(job:Fault_Code9)
            expfil.AddField(job:Fault_Code10)
            expfil.AddField(job:Fault_Code11)
            expfil.AddField(job:Fault_Code12)
            expfil.AddField(wob:FaultCode13)
            expfil.AddField(wob:FaultCode14)
            expfil.AddField(wob:FaultCode15)
            expfil.AddField(wob:FaultCode16)
            expfil.AddField(wob:FaultCode17)
            expfil.AddField(wob:FaultCode18)
            expfil.AddField(wob:FaultCode19)
            expfil.AddField(wob:FaultCode20)
            ! Engineers Notes
            expfil.AddField(BHStripNonAlphaNum(jbn:Engineers_Notes,' '))
            ! Invoice Text
            expfil.AddField(BHStripNonAlphaNum(jbn:Invoice_Text,' '))
            ! Parts
            countParts = 0
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number = job:Ref_Number
            SET(par:Part_Number_Key,par:Part_Number_Key)
            LOOP UNTIL Access:PARTS.Next() <> Level:Benign
                IF (par:Ref_Number <> job:Ref_Number)
                    BREAK
                END ! IF
                countParts += 1
                IF (countParts > 5)
                    BREAK
                END ! IF
                expfil.AddField(par:Part_Number)
                expfil.AddField(par:Description)
                expfil.AddField(par:Quantity)
                IF (sentToHub)
                    expfil.AddField(FORMAT(par:AveragePurchaseCost,@n_14.2))
                    expfil.AddField(FORMAT(par:Sale_Cost,@n_14.2))
                ELSE ! IF
                    expfil.AddField(FORMAT(par:RRCAveragePurchaseCost,@n_14.2))
                    expfil.AddField(FORMAT(par:RRCSaleCost,@n_14.2))                
                END ! IF
            END ! LOOP
            IF (countParts < 5)
                LOOP
                    countParts += 1
                    IF (countParts > 5)
                        BREAK
                    END ! IF
                    expfil.AddField(,,,5)
                END !  LOOP
            END ! IF
            countParts = 0
            Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
            wpr:Ref_Number = job:Ref_Number
            SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
            LOOP UNTIL Access:WARPARTS.Next() <> Level:Benign
                IF (wpr:Ref_Number <> job:Ref_Number)
                    BREAK
                END ! IF
                countParts += 1
                IF (countParts > 5)
                    BREAK
                END ! IF
                expfil.AddField(wpr:Part_Number)
                expfil.AddField(wpr:Description)
                expfil.AddField(wpr:Quantity)
                IF (sentToHub)
                    expfil.AddField(FORMAT(wpr:AveragePurchaseCost,@n_14.2))
                    expfil.AddField(FORMAT(wpr:Purchase_Cost,@n_14.2))
                ELSE ! IF
                    expfil.AddField(FORMAT(wpr:RRCAveragePurchaseCost,@n_14.2))
                    expfil.AddField(FORMAT(wpr:RRCPurchaseCost,@n_14.2))                
                END ! IF
            END ! LOOP
            IF (countParts < 5)
                LOOP
                    countParts += 1
                    IF (countParts > 5)
                        BREAK
                    END ! IF
                    expfil.AddField(,,,5)
                END !  LOOP
            END ! IF      
            ! Who Booked
            expfil.AddField(WhoBooked())
            ! Who Despatched
            expfil.AddField(WhoDespatched(),,1)
        END ! CASE
        
        

        
RepairLocation      PROCEDURE()!,STRING
    CODE
        IF (job:Exchange_Unit_Number > 0 AND jobe:ExchangedAtRRC)
            RETURN 'RRC'
        ELSE
            IF (sentToHub)
                RETURN 'ARC'
            ELSE ! IF
                RETURN 'RRC'
            END ! IF
        END ! IF
    
DateReceivedFromPUP PROCEDURE()!,DATE
locDateReceived         DATE()
locPUPFlag              LONG()
    CODE
        locDateReceived = 0
        locPUPFlag = 0
        Access:LOCATLOG.ClearKey(lot:DateKey)
        lot:RefNumber = job:Ref_Number
        lot:TheDate = 0
        SET(lot:DateKey,lot:DateKey)
        LOOP UNTIL Access:LOCATLOG.Next() <> Level:Benign
            IF (lot:RefNumber <> job:Ref_Number)
                BREAK
            END ! IF
            IF (INSTRING('PUP',lot:PreviousLocation,1,1))
                locPUPFlag = 1
                ! Unit was at the PUP
            END ! IF
            IF (lot:NewLocation <> p_web.GSV('Default:RRCLocation'))
                CYCLE
            END ! IF
            IF (lot:NewLocation = p_web.GSV('Default:ARCLocation'))
                ! Unit send to ARC. Too late for the date now.
                BREAK
            END ! IF
            IF (locPUPFlag = 1)
                locDateReceived = lot:TheDate
            END ! IF
            BREAK
        END ! LOOP    
        
        RETURN locDateReceived
        
GetWOBEDIStatus     PROCEDURE(STRING pStatus)!,STRING        
    CODE
        Case pStatus
        Of 'NO'
            Return 'PENDING'
        Of 'YES'
            Return 'SUBMITTED'
        Of 'APP'
            Return 'APPROVED'
        Of 'PAY'
            Return 'PAID'
        Of 'EXC'
            Return 'REJECTED'
        Of 'EXM'
            Return 'REJECTED'
        Of 'AAJ'
            Return 'ACCEPTED REJECTION'
        Of 'REJ'
            Return 'FINAL REJECTION'
        End !func:Status
        Return ''

WhoBooked           PROCEDURE()!,STRING
RetValue                STRING(100)
    CODE
        RetValue = ''
        IF (job:Who_Booked = 'WEB')
            RetValue = jobe2:SIDBookingName
        ELSE ! IF
            Access:USERS.ClearKey(use:User_Code_Key)
            use:User_Code = job:Who_Booked
            IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
                RetValue = CLIP(use:Forename) & ' ' & CLIP(use:Surname)
                
            END ! IF
        END ! IF    
        RETURN RetValue
    
WhoDespatched       PROCEDURE()!,STRING
RetValue                STRING(100)    
    CODE
        RetValue = ''
        IF (job:Date_Completed > 0)
            Access:JOBSCONS.ClearKey(joc:DateKey)
            joc:RefNumber = job:Ref_Number
            joc:TheDate = 0
            SET(joc:DateKey,joc:DateKey)
            LOOP UNTIL Access:JOBSCONS.Next() <> Level:Benign
                IF (joc:RefNumber <> job:Ref_Number)
                    BREAK
                END ! IF
                IF (joc:DespatchFrom = 'RRC' AND joc:DespatchTo = 'CUSTOMER')
                    Access:USERS.Clearkey(use:User_Code_Key)
                    use:User_Code = joc:UserCode
                    IF (Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign)
                        RetValue = Clip(use:Forename) & ' ' & Clip(use:Surname)
                        BREAK
                    END
                END ! IF                    
            END ! LOOP
        END ! IF
        RETURN RetValue
!endregion
!region Audit Export
ExportAudit         PROCEDURE()
    CODE
        Access:AUDIT.ClearKey(aud:Record_Number_Key)
        aud:Record_Number = qStatusReportAudit.RecordNumber
        IF (Access:AUDIT.TryFetch(aud:Record_Number_Key) = Level:Benign)
            
        ELSE ! IF
            RETURN
        END ! IF
        
        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = aud:Ref_Number
        IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
            ! Fail
            RETURN
        END ! IF            
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
            ! Fail
            RETURN
        END ! IF            
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = p_web.GSV('BookingAccount')
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
            ! Fail
        END ! IF      
        Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
        aud2:AUDRecordNumber = aud:Record_Number
        IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey))
            ! Fail
            
        END ! IF
        expfil2.AddField(job:Ref_Number & '-' & tra:BranchIdentification & wob:JobNumber,1)
        expfil2.AddField(FORMAT(aud:Date,@d06b))
        expfil2.AddField(FORMAT(aud:Time,@t01b))
        expfil2.AddField(aud:User)
        expfil2.AddField(aud:Action)
        expfil2.AddField(BHStripNonAlphaNum(aud2:Notes,' '))
        expfil2.AddField(aud:Type,,1)            

!endregion
!region CParts Export
ExportCParts        PROCEDURE(LONG pFirstPart)
    CODE
        Access:PARTS.ClearKey(par:RecordNumberKey)
        par:Record_Number = qStatusReportCParts.RecordNumber
        IF (Access:PARTS.TryFetch(par:RecordNumberKey) = Level:Benign)
            
        ELSE ! IF
            RETURN
        END ! IF
        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = par:Ref_Number
        IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
            ! Fail
            RETURN
        END ! IF            
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
            ! Fail
            RETURN
        END ! IF            
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = p_web.GSV('BookingAccount')
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
            ! Fail
        END ! IF           
        expfil3.AddField(job:Ref_Number & '-' & tra:BranchIdentification & wob:JobNumber,1)
        expfil3.AddField(par:Part_Number)
        expfil3.AddField(par:Description)
        expfil3.AddField(par:Supplier)
        expfil3.AddField(par:Quantity)
        expfil3.AddField(par:Exclude_From_Order)
        expfil3.AddField(par:Despatch_Note_Number)
        expfil3.AddField(par:Order_Number)
        expfil3.AddField(Format(par:Date_Received,@d06))
        expfil3.AddField(par:Fault_Codes_Checked)
        expfil3.AddField(par:Fault_Code1)
        expfil3.AddField(par:Fault_Code2)
        expfil3.AddField(par:Fault_Code3)
        expfil3.AddField(par:Fault_Code4)
        expfil3.AddField(par:Fault_Code5)
        expfil3.AddField(par:Fault_Code6)
        expfil3.AddField(par:Fault_Code7)
        expfil3.AddField(par:Fault_Code8)
        expfil3.AddField(par:Fault_Code9)
        expfil3.AddField(par:Fault_Code10)
        expfil3.AddField(par:Fault_Code11)
        expfil3.AddField(par:Fault_Code12)
        IF (pFirstPart)
            expfil3.AddField(1,,1)
        Else !If tmp:FirstPart = 0
            expfil3.AddField(0,,1)
        End !If tmp:FirstPart = 0        
!endregion        
!region WParts Export
ExportWParts        PROCEDURE(LONG pFirstPart)
    CODE
        Access:WARPARTS.ClearKey(wpr:RecordNumberKey)
        wpr:Record_Number = qStatusReportWParts.RecordNumber
        IF (Access:WARPARTS.TryFetch(wpr:RecordNumberKey) = Level:Benign)
            
        ELSE ! IF
            RETURN
        END ! IF     
        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = wpr:Ref_Number
        IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
            ! Fail
            RETURN
        END ! IF            
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
            ! Fail
            RETURN
        END ! IF            
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = p_web.GSV('BookingAccount')
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
            ! Fail
        END ! IF         
        expfil4.AddField(job:Ref_Number & '-' & tra:BranchIdentification & wob:JobNumber,1)
        expfil4.AddField(wpr:Part_Number)
        expfil4.AddField(wpr:Description)
        expfil4.AddField(wpr:Supplier)
        expfil4.AddField(wpr:Quantity)
        expfil4.AddField(wpr:Exclude_From_Order)
        expfil4.AddField(wpr:Despatch_Note_Number)
        expfil4.AddField(wpr:Order_Number)
        expfil4.AddField(Format(wpr:Date_Received,@d06))
        expfil4.AddField(wpr:Fault_Codes_Checked)
        expfil4.AddField(wpr:Fault_Code1)
        expfil4.AddField(wpr:Fault_Code2)
        expfil4.AddField(wpr:Fault_Code3)
        expfil4.AddField(wpr:Fault_Code4)
        expfil4.AddField(wpr:Fault_Code5)
        expfil4.AddField(wpr:Fault_Code6)
        expfil4.AddField(wpr:Fault_Code7)
        expfil4.AddField(wpr:Fault_Code8)
        expfil4.AddField(wpr:Fault_Code9)
        expfil4.AddField(wpr:Fault_Code10)
        expfil4.AddField(wpr:Fault_Code11)
        expfil4.AddField(wpr:Fault_Code12)
        IF (pFirstPart)
            expfil4.AddField(1,,1)
        Else !If tmp:FirstPart = 0
            expfil4.AddField(0,,1)
        End !If tmp:FirstPart = 0  
   
!endregion
! StockAuditReport
StockAuditReport    PROCEDURE()
totalLines              LONG()
totalNumberOfLines      LONG()
totalAudited            LONG()
totalQty                LONG()
totalLineCost           REAL()
i                       LONG()
    CODE
        expfil.Init('Stock Audit Export')
	
        BuildStockAuditShortages(p_web,1,p_web.GSV('stom:Audit_No'),totalLines,totalAudited)
		
        IF (totalLines = 0)
            expfil.Kill(1)
            CreateScript(packet,kNoRecordsAlert)
            DO ReturnFail
        END ! IF
		
        expfil.AddField('STOCK AUDIT REPORT',1)
        expfil.AddField(p_web.GSV('BookingSiteLocation'),,1)
		
        expfil.AddField(FORMAT(TODAY(),@d06),1,1)
		
        expfil.AddField('SHORTAGE',1,1)
		
        IF (p_web.GSV('stom:Complete') = 'N')
            expfil.AddField('This Audit is not fully completed',1,1)
			
        END ! IF
        expfil.AddField('Stock Code,Description,Shelf Location,Orig Qty,Qty,Unit Cost,Line Cost',1,1,,1)
		
        ProgressBarWeb.Init(RECORDS(qStockAudit))
		
        Relate:STOAUDIT.Open()
        Relate:STOCK.Open()
		! Shortages
        totalQty = 0
        totalLineCost = 0
        totalNumberOfLines = 0
		
        SORT(qStockAudit,qStockAudit.SessionID,qStockAudit.ShortagesExcess)
        LOOP i = 1 TO RECORDS(qStockAudit)
            GET(qStockAudit,i)
			
            ProgressBarWeb.Update('Exporting Shortages...')
			
            IF (qStockAudit.SessionID <> p_web.SessionID OR qStockAudit.ShortagesExcess <> 'S')
                CYCLE
            END ! IF
			
            DO ExportLine
            
			
        END ! LOOP
        
        DO Totals
        
        expfil.AddField(,1,1)

        expfil.AddField('EXCESSES',1,1)
        
        expfil.AddField(,1,1)
		!Excesses
        totalQty = 0
        totalLineCost = 0
        totalNumberOfLines = 0
		
        SORT(qStockAudit,qStockAudit.SessionID,qStockAudit.ShortagesExcess)
        LOOP i = 1 TO RECORDS(qStockAudit)
            GET(qStockAudit,i)
			
            ProgressBarWeb.Update('Exporting Excesses...')
			
            IF (qStockAudit.SessionID <> p_web.SessionID OR qStockAudit.ShortagesExcess <> 'E')
                CYCLE
            END ! IF
			
            DO ExportLine
            
			
        END ! LOOP		
		
        DO Totals
		
        Relate:STOCK.Close()
        Relate:STOAUDIT.Close()
        
        expfil.Kill()
		
ExportLine          ROUTINE
    Access:STOAUDIT.ClearKey(stoa:Internal_AutoNumber_Key)
    stoa:Internal_AutoNumber = qStockAudit.StoAuditRecordNumber
    IF (Access:STOAUDIT.TryFetch(stoa:Internal_AutoNumber_Key) = Level:Benign)
			
    END ! IF
		
    Access:STOCK.ClearKey(sto:Ref_Number_Key)
    sto:Ref_Number = qStockAudit.StockRecordNumber
    IF (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
			
    END ! IF
		
    expfil.AddField(sto:Part_Number,1)
    expfil.AddField(sto:Description)
    expfil.AddField(sto:Shelf_Location)
    expfil.AddField(FORMAT(stoa:Original_Level,@n9))
    expfil.AddField(FORMAT(-1 * qStockAudit.StockQty,@n9))
    expfil.AddField(FORMAT(sto:Purchase_Cost,@n-10.2))
    expfil.AddField(FORMAT(sto:Purchase_Cost * (-1 * qStockAudit.StockQty),@n10.2),,1)
    

    totalNumberOfLines += 1
    totalQty += -1 * qStockAudit.StockQty
    totalLineCost += (sto:Purchase_Cost * (-1 * qStockAudit.StockQty))	
		
		
Totals              ROUTINE		
		
    IF (totalNumberOfLines = 0)
        expfil.AddField(,1,1,2)
        expfil.AddField(',,NO ITEMS ON REPORT',1,1,,1)
			
    ELSE ! IF
        expfil.AddField(,1,1)
        IF (qStockAudit.ShortagesExcess = 'S')
            expfil.AddField('Total Shortage Lines',1)
        ELSE
            expfil.AddField('Total Excess Lines',1)
        END ! IF
        expfil.AddField(FORMAT(totalNumberOfLines,@n9))
        expfil.AddField()
        expfil.AddField('Totals')
        expfil.AddField(FORMAT(totalQty,@n9))
        expfil.AddField()
        expfil.AddField(FORMAT(totalLineCost,@n9.2),,1)
			
        expfil.AddField('Stock Lines',1)
        expfil.AddField(FORMAT(totalLines,@n9),,1)
			
        expfil.AddField('Lines Audited',1)
        expfil.AddField(FORMAT(totalAudited,@n9),,1)
			
        expfil.AddField('Lines Not Audited',1)
        expfil.AddField(FORMAT(totalAudited - totalLines,@n9),,1)
			
        expfil.AddField('Percentage Audited',1)
        expfil.AddField(FORMAT((totalAudited/totalLines) * 100,@n9.2) & '%',,1)
			
    END ! IF
		
		
		
		

! StockCheckReport
StockCheckReport    PROCEDURE()
locUsed                 LONG()	
locFirstModel           STRING(30)	
recordCount             LONG()
i                       LONG()
    CODE
		
        expfil.Init('Stock Check Export')
		
        Relate:STOCK.Open()
        Relate:PRIBAND.Open()
        Relate:TRADEACC.Open()
        Relate:USERS.Open()
        Relate:STOMODEL.Open()
		
        Access:PRIBAND.ClearKey(prb:keyBandName)
        prb:BandName = p_web.GSV('locPriceBand')
        IF (Access:PRIBAND.TryFetch(prb:keyBandName) = Level:Benign)
            p_web.FileToSessionQueue(PRIBAND)
        ELSE ! IF
        END ! IF
        
        p_web.SSV('pBuildForAudit',0)
        p_web.SSV('pUpdateProgress',1)
        BuildStockAuditQueue(p_web)
		
        !recordCount = BuildStockAuditQueue(p_web,0,0)
        recordCount = p_web.GSV('recordCount')

        ProgressBarWeb.Init(recordCount)
		
        IF (recordCount = 0)
            expfil.Kill(1)
            CreateScript(packet,kNoRecordsAlert)
            Do ReturnFail
        END ! IF
		
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = p_web.GSV('BookingAccount')
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
			
        ELSE ! IF
        END ! IF
		
        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = p_web.GSV('BookingUserCode')
        IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
			
        ELSE ! IF
        END ! IF
		
        expfil.AddField('"' & CLIP(tra:Company_Name) & '",,,,Site Location,"' & p_web.GSV('BookingSiteLocation') & '"',1,1,,1)
		
        expfil.AddField('"' & CLIP(tra:Address_Line1) & '",,,,Manufacturer,"' & CHOOSE(p_web.GSV('locAllManufacturers'),'',p_web.GSV('locManufacturer')) & '"',1,1,,1)
		
        expfil.AddField('"' & CLIP(tra:Address_Line2) & '",,,,Inc Accessories,' & CHOOSE(p_web.GSV('locIncludeAccessores'),'YES','NO'),1,1,,1)
		
        expfil.AddField('"' & CLIP(tra:Address_Line3) & '",,,,Supress Zeros,' & CHOOSE(p_web.GSV('locSuppressZerosOnReport'),'YES','NO'),1,1,,1)
		
        expfil.AddField('"' & CLIP(tra:Postcode) & '",,,,Price Band,"' & CHOOSE(p_web.GSV('locAllPriceBands'),'',p_web.GSV('locPriceBand')) & '"',1,1,,1)
		
        expfil.AddField('"TEL: ' & CLIP(tra:Telephone_Number) & '",,,,Printed By,"' & CLIP(use:Forename) & ' ' & CLIP(use:Surname) & '"',1,1,,1)
		
        expfil.AddField('"FAX: ' & CLIP(tra:Fax_Number) & '",,,,Report Date,' & FORMAT(TODAY(),@d06b),1,1,,1)
		
        expfil.AddField('"EMAIL: ' & CLIP(tra:EmailAddress) & '"',1,1,,1)
		
        expfil.AddField(,1,1)
		
        expfil.AddField('Shelf Location',1)
        expfil.AddField('2nd Location')
        expfil.AddField('Part Number')
        expfil.AddField('1st Model')
        expfil.AddField('Description')
        expfil.AddField('Purch Cost')
		
        IF (p_web.GSV('locShowUsageDate') = 1)
            expfil.AddField('Usage',,1)
        END ! IF
        IF (p_web.GSV('locShowStockQuantityOnReport') = 1)
            expfil.AddField('In Stock',,1)
        END ! IF
        
        IF (p_web.GSV('locReportOrder') = 1)
            SORT(qStockCheck,qStockCheck.SessionID,qStockCheck.Description)
        ELSE ! IF
            SORT(qStockCheck,qStockCheck.SessionID)
        END ! IF
        
        CLEAR(qStockCheck)
        LOOP i = 1 TO RECORDS(qStockCheck)
            GET(qStockCheck,i)
            IF (qStockCheck.SessionID <> p_web.SessionID)
                CYCLE
            END ! IF

            ProgressBarWeb.Update('Exporting..')
            
            Access:STOCK.ClearKey(sto:Ref_Number_Key)
            sto:Ref_Number = qStockCheck.RefNumber	
            IF (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
				
            ELSE ! IF
                CYCLE
            END ! IF
			
            locFirstModel = ''
            Access:STOMODEL.ClearKey(stm:Model_Number_Key)
            stm:Ref_Number = sto:Ref_Number
            stm:Manufacturer = sto:Manufacturer
            SET(stm:Model_Number_Key,stm:Model_Number_Key)
            LOOP UNTIL Access:STOMODEL.Next() <> Level:Benign
                IF (stm:Ref_Number <> sto:Ref_Number OR |
                    stm:Manufacturer <> sto:Manufacturer)
                    BREAK
                END ! IF
                locFirstModel = stm:Model_Number
                BREAK
            END ! LOOP
			
            expfil.AddField(sto:Shelf_Location,1)
            expfil.AddField(sto:Second_Location)
            expfil.AddField(sto:Part_Number)
            expfil.AddField(locFirstModel)
            expfil.AddField(sto:Description)
            expfil.AddField(LEFT(FORMAT(sto:Purchase_Cost,@n10.2)))
            IF (p_web.GSV('locShowUsageDate') = 1)
                expfil.AddField(qStockCheck.Usage,,1)
            END ! IF
            IF (p_web.GSV('locShowStockQuantityOnReport') = 1)
                expfil.AddField(sto:Quantity_stock,,1)
            END ! IF	
			
        END ! LOOP
		
        expfil.Kill()
			
        Relate:STOCK.Close()
        Relate:PRIBAND.Close()
        Relate:TRADEACC.Close()
        Relate:USERS.Close()
        Relate:STOMODEL.Close()
!StockReceiveProcess
StockReceiveProcess PROCEDURE()
processed               LONG(0)
locMessage              STRING(255)
    CODE
        IF (CheckIfTagged(p_web) = 0)
            CreateScript(packet,'alert("You have not tagged any parts")')
            DO ReturnFail
        END ! IF
        
        Relate:TagFile.Open()
        Relate:STOCKRECEIVETMP.Open()
        Relate:RETSTOCK.Open()
        Relate:STOCK.Open()
        Relate:RETSALES.Open()
        
        processed = 0
        
        Access:TagFile.ClearKey(tag:KeyTagged)
        tag:SessionID = p_web.SessionID
        SET(tag:KeyTagged,tag:KeyTagged)
        LOOP UNTIL Access:TagFile.Next() <> Level:Benign
            IF (tag:SessionID <> p_web.SessionID)
                BREAK
            END ! IF
            IF (tag:Tagged <> 1)
                CYCLE
            END ! IF
            
            Access:STOCKRECEIVETMP.ClearKey(stotmp:RecordNumberKey)
            stotmp:RecordNumber = tag:TaggedValue
            IF (Access:STOCKRECEIVETMP.TryFetch(stotmp:RecordNumberKey) = Level:Benign)
                
            ELSE ! IF
            END ! IF

            Access:RETSTOCK.ClearKey(res:Record_Number_Key)
            res:Record_Number = stotmp:RESRecordNumber
            IF (Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign)
                Access:RETSALES.ClearKey(ret:Ref_Number_Key)
                ret:Ref_Number = res:Ref_Number
                IF (Access:RETSALES.TryFetch(ret:Ref_Number_Key) = Level:Benign)
                    IF (ret:ExchangeOrder)
                        CYCLE
                    END ! IF
                END ! IF
                
                IF NOT (res:Received)
                    Access:STOCK.ClearKey(sto:Location_Key)
                    sto:Location =  p_web.GSV('Default:SiteLocation')
                    sto:Part_Number = res:Part_Number
                    IF (Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign)
                        sto:Quantity_stock += res:QuantityReceived
                        IF (Access:STOCK.TryUpdate() = Level:Benign)
                            processed = 1
                            If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                'ADD', | ! Transaction_Type
                                '', | ! Depatch_Note_Number
                                0, | ! Job_Number
                                0, | ! Sales_Number
                                res:Quantity, | ! Quantity
                                res:Item_Cost, | ! Purchase_Cost
                                res:Sale_Cost, | ! Sale_Cost
                                res:Retail_Cost, | ! Retail_Cost
                                'STOCK ADDED FROM ORDER', | ! Notes
                                'RAPID STOCK RECEIVED <13,10>INVOICE NO: ' & p_web.GSV('locInvoiceNumber'), |
                                p_web.GSV('BookingUserCode'), |
                                sto:Quantity_Stock) ! Information
                            END ! IF 
                            ! #10396 Now has stock. Part unsuspended. (DBH: 16/07/2010)
                            if (sto:Suspend)
                                sto:Suspend = 0
                                If (Access:STOCK.TryUpdate() = Level:Benign)
                                    
                                    If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                        'ADD', | ! Transaction_Type
                                        '', | ! Depatch_Note_Number
                                        0, | ! Job_Number
                                        0, | ! Sales_Number
                                        0, | ! Quantity
                                        res:Item_Cost, | ! Purchase_Cost
                                        res:Sale_Cost, | ! Sale_Cost
                                        res:Retail_Cost, | ! Retail_Cost
                                        'PART UNSUSPENDED', | ! Notes
                                        '', |
                                        p_web.GSV('BookingUserCode'), |
                                        sto:Quantity_Stock) ! Information
                                    End ! AddToStockHistory
                                End ! If (Access:STOCK.TryUpdate() = Level:Benign)
                            end
                            If res:QuantityReceived < res:Quantity
                                If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                    'DEC', | ! Transaction_Type
                                    '', | ! Depatch_Note_Number
                                    0, | ! Job_Number
                                    0, | ! Sales_Number
                                    res:Quantity - res:QuantityReceived, | ! Quantity
                                    res:Item_Cost, | ! Purchase_Cost
                                    res:Sale_Cost, | ! Sale_Cost
                                    res:Retail_Cost, | ! Retail_Cost
                                    'RAPID STOCK RECEIVED ADJUSTMENT', | ! Notes
                                    'INVOICE NO: ' & p_web.GSV('locInvoiceNumber'), |
                                    p_web.GSV('BookingUserCode'), |
                                    sto:Quantity_Stock) ! Information
                                    ! Added OK
                                Else ! AddToStockHistory
                                    ! Error
                                End ! AddToStockHistory
                            End !If res:QuantityReceived < res:Quantity
                            res:Received = 1
                            res:DateReceived = Today()
                            Access:RETSTOCK.Update()
  
                              !Only work out average if more than 0 is received
                            If res:QuantityReceived > 0
                                AvPurchaseCost$ = sto:AveragePurchaseCost
                                PurchaseCost$ = sto:Purchase_Cost
                                SaleCost$ = sto:Sale_Cost
                                RetailCost$ = sto:Retail_Cost
  
                                sto:AveragePurchaseCost = AveragePurchaseCost(sto:Ref_Number,sto:Quantity_stock,locMessage)
                                sto:Purchase_Cost   = VodacomClass.Markup(sto:Purchase_Cost,sto:AveragePurchaseCost,sto:PurchaseMarkup)
                                sto:Sale_Cost       = VodacomClass.Markup(sto:Sale_Cost,sto:AveragePurchaseCost,sto:Percentage_Mark_Up)
                                BHAddToDebugLog('Av Cost: ' & AvPurchaseCost$ & ' - ' & sto:AveragePurchaseCost &  ' - ' & sto:Sale_Cost)
                                  !Write record of information!
                                If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                    'ADD', | ! Transaction_Type
                                    '', | ! Depatch_Note_Number
                                    0, | ! Job_Number
                                    0, | ! Sales_Number
                                    0, | ! Quantity
                                    sto:Purchase_Cost, | ! Purchase_Cost
                                    sto:Sale_Cost, | ! Sale_Cost
                                    sto:Retail_Cost, | ! Retail_Cost
                                    'AVERAGE COST CALCULATION', | ! Notes
                                    CLIP(locMessage),|
                                    p_web.GSV('BookingUserCode'), |
                                    sto:Quantity_Stock,|
                                    AvPurchaseCost$,|
                                    PurchaseCost$,|
                                    SaleCost$,|
                                    RetailCost$)
                                    ! Added OK
                                Else ! AddToStockHistory
                                    ! Error
                                End ! AddToStockHistory
                            End !If res:QuantityReceived > 0
  
                            Access:STOCK.Update()       
                        END
                        
                    ELSE ! IF
                    END ! IF
                END ! IF
            ELSE ! IF
            END ! IF
        END ! LOOP
        
        Relate:RETSTOCK.Close()
        Relate:STOCKRECEIVETMP.Close()
        Relate:TagFile.Close()
        Relate:STOCK.Close()
        Relate:RETSALES.Close()
        
        IF (processed = 1)
            ClearTagFile(p_web)
            !ClearStockReceiveList(p_web)
            BuildStockReceiveList()
            CreateScript(packet,'alert("The tagged items have been processed.")')
        ELSE
            CreateScript(packet,'alert("No items were processed.")')
            DO ReturnFail
        END ! IF

!region StockValueReportEngine
StockValueReportEngine      PROCEDURE()
recs                            LONG()
i                               LONG()
count                           LONG()
    CODE
    
        Relate:STOCK.Open()
        Relate:STOMODEL.Open()
    
        recs = RECORDS(qStockValueReport)
        LOOP i = recs TO 1 BY -1
            GET(qStockValueReport,i)
            IF (qStockValueReport.SessionID = p_web.SessionID)
                DELETE(qStockValueReport)
            END ! IF
        END ! LOOP
    
        ProgressBarWeb.Init(RECORDS(STOCK)/10)
    
        count = 0
        
        Access:LOCSHELF.ClearKey(los:Shelf_Location_Key)
        los:Site_Location = p_web.GSV('BookingSiteLocation')
        SET(los:Shelf_Location_Key,los:Shelf_Location_Key)
        LOOP UNTIL Access:LOCSHELF.Next() <> Level:Benign
            IF (los:Site_Location <> p_web.GSV('BookingSiteLocation'))
                BREAK
            END ! IF
            If (p_web.GSV('locAllShelfLocations') <> 1)
                Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
                tagf:SessionID = p_web.SessionID
                tagf:TagType = kShelfLocation
                tagf:TaggedValue = los:Shelf_Location
                IF (Access:SBO_GenericTagFile.TryFetch(tagf:KeyTagged))
                    CYCLE
                ELSE
                    IF (tagf:Tagged <> 1)
                        CYCLE
                    END ! IF
                END ! IF
            END ! IF

            Access:STOCK.ClearKey(sto:Shelf_Location_Key)
            sto:Location = p_web.GSV('BookingSiteLocation')
            sto:Shelf_Location = los:Shelf_Location
            SET(sto:Shelf_Location_Key,sto:Shelf_Location_Key)
            LOOP UNTIL Access:STOCK.Next() <> Level:Benign
                IF (sto:Location <> p_web.GSV('BookingSiteLocation') OR |
                    sto:Shelf_Location <> los:Shelf_Location)
                    BREAK
                END ! IF
        
                ProgressBarWeb.Update('Building Data....')
        
                IF (sto:Suspend) THEN CYCLE; END
        
                IF (p_web.GSV('locIncludeAccessories') <> 1)
                    IF (sto:Accessory = 'YES') THEN CYCLE; END
                END ! IF
        
                IF (p_web.GSV('locSuppressZeros') = 1)
                    IF (sto:Quantity_Stock = 0) THEN CYCLE; END
                END ! IF
        
                qStockValueReport.SessionID = p_web.SessionID
                qStockValueReport.SiteLocation = sto:Location
                qStockValueReport.Manufacturer = sto:Manufacturer
                qStockValueReport.Quantity = sto:Quantity_Stock
                qStockValueReport.PurchaseValue = sto:Purchase_Cost
                qStockValueReport.AveragePurchaseCost = sto:AveragePurchaseCost
                qStockValueReport.SaleValue = sto:Sale_Cost
                qStockValueReport.PartNumber = sto:Part_Number
                qStockValueReport.Description = sto:Description
                qStockValueReport.FirstModel = ''
        
                Access:STOMODEL.ClearKey(stm:Model_Number_Key)
                stm:Ref_Number = sto:Ref_Number
                stm:Manufacturer = sto:Manufacturer
                SET(stm:Model_Number_Key,stm:Model_Number_Key)
                LOOP UNTIL Access:STOMODEL.Next() <> Level:Benign
                    IF (stm:Ref_Number <> sto:Ref_Number OR |
                        stm:Manufacturer <> sto:Manufacturer)
                        BREAK
                    END ! IF
                    qStockValueReport.FirstModel = stm:Model_Number
                    BREAK
                END ! LOOP
        
                qStockValueReport.MinimumLevel = sto:Minimum_Level
                ADD(qStockValueReport)
                count += 1
        
            END ! LOOP
        END ! LOOP
    
        Relate:STOMODEL.Close()
        Relate:STOCK.Close()
    
        IF (count = 0)
            CreateScript(packet,kNoREcordsAlert)
            DO ReturnFail
        END ! IF
        
        CASE p_web.GSV('locReportType')
        OF 0
            SORT(qStockValueReport,qStockValueReport.SiteLocation,qStockValueReport.Manufacturer)
        OF 1
            SORT(qStockValueReport,qStockValueReport.PartNumber,qStockValueReport.Description)
        END !I F
        
    
        IF (p_web.GetValue('ReportType') = 'E')
            StockValueExport()
        ELSE
            ProgressbarWeb.Update('Building Report....')
            CallReport('StockValueReport')
            !CreateScript(packet,'window.open("StockValueReport","_self")')
        END ! IF
        
!endregion
!region StockValueExport
StockValueExport        PROCEDURE() 
gSummary                    GROUP,PRE()
currentManufacturer             STRING(30)    
totalStock                      LONG()
purchaseValue                   REAL!DECIMAL(10,2)
averagePurchaseCost             REAL!DECIMAL(10,2)
saleValue                       REAL!DECIMAL(10,2)
                            END
gTotals                     GROUP,PRE()
totalLines                      LONG()
totalStockItems                 LONG()
totalPurchaseValue              DECIMAL(10,2)
totalAveragePurchaseCost        DECIMAL(10,2)
totalSaleValue                  DECIMAL(10,2)
                            END
i                           LONG()
firstManufacturer LONG()
    MAP
AddSummaryLine  PROCEDURE()
ExportHeader    PROCEDURE()
    END ! MAP
    CODE
        ProgressBarWeb.Init(RECORDS(qStockValueReport))
        
        Relate:USERS.Open()
        
    
        CASE p_web.GSV('locReportType')
        OF 0 ! Summary  
            expfil.Init('Stock Value Report (Summary)','Stock Value Report (Summary)' & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Format(Year(today()),@n04) & Format(Clock(),@t2) & '.csv')
            
            expfil.AddField('Stock Value Report (Summary)',1,1)

            ExportHeader()
            
            expfil.AddField('Site Location',1)
            expfil.AddField('Manufacturer')
            expfil.AddField('Total Stock Items')
            expfil.AddField('In Warranty Value')
            expfil.AddField('Average Purchase Cost')
            expfil.AddField('Out Warranty Value',,1)
            
            
            
            CLEAR(gSummary)
            CLEAR(gTotals)
            firstManufacturer = 1
            currentManufacturer = '!!!'
            LOOP i = 1 TO RECORDS(qStockValueReport)
                GET(qSTockValueReport,i)
                ProgressBarWeb.Update('Exporting....')
                
                IF (qStockValueReport.SessionID <> p_web.SessionID)
                    CYCLE
                END ! IF
                IF (qStockValueReport.Manufacturer <> currentManufacturer AND currentManufacturer <> '!!!')
                    AddSummaryLine()
                    CLEAR(gSummary)
                    
                END ! IF
                
                currentManufacturer = qStockValueReport.Manufacturer
                
                totalStock += qStockValueReport.Quantity
                purchaseValue += ROUND(qStockValueReport.PurchaseValue * qStockValueReport.Quantity,.01)
                averagePurchaseCost += ROUND(qStockValueReport.AveragePurchaseCost * qStockValueReport.Quantity,.01)
                saleValue += ROUND(qStockValueReport.SaleValue * qStockValueReport.Quantity,.01)

            END ! IF
            AddSummaryLine()
            
            expfil.AddField('Total Number Of Lines',1)
            expfil.AddField(totalLines)
            expfil.AddField(totalStockItems)
            expfil.AddField(FORMAT(totalPurchaseValue,@n_14.2))
            expfil.AddField(FORMAT(totalAveragePurchaseCost,@n_14.2))
            expfil.AddField(FORMAT(totalSaleValue,@n_14.2),,1)
            
        OF 1 ! Detailed
            expfil.Init('Stock Value Report (Detailed)','Stock Value Report (Summary)' & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Format(Year(today()),@n04) & Format(Clock(),@t2) & '.csv')
            expfil.AddField('Stock Value Report (Summary)',1,1)

            ExportHeader()
            
            expfil.AddField('Part Number',1)
            expfil.AddField('Description')
            expfil.AddField('First Model Number')
            expfil.AddField('Order If Below')
            expfil.AddField('Out Warranty Value')
            expfil.AddField('Qty In Stock')
            expfil.AddField('In Warranty Value')
            expfil.AddField('Average Purchase Cost')
            expfil.AddField('Line Cost',,1)
            
            
            
            CLEAR(gTotals)
            LOOP i = 1 TO RECORDS(qStockValueReport)
                GET(qStockValueReport,i)
                ProgressBarWeb.Update('Exporting....')
                
                IF (qStockValueReport.SessionID <> p_web.SessionID)
                    CYCLE
                END ! IF
                
                expfil.AddField('''' & qStockValueReport.PartNumber,1)
                expfil.AddField(qStockValueReport.Description)
                expfil.AddField('''' & qStockValueReport.FirstModel)
                expfil.AddField(qStockValueReport.MinimumLevel)
                expfil.AddField(FORMAT(qStockValueReport.SaleValue,@n_14.2))
                expfil.AddField(qStockValueReport.Quantity)
                expfil.AddField(FORMAT(qStockValueReport.PurchaseValue,@n_14.2))
                expfil.AddField(FORMAT(qStockValueReport.AveragePurchaseCost,@n_14.2))
                expfil.AddField(Round(qStockValueReport.AveragePurchaseCost * qStockValueReport.Quantity,.01),,1)
                
                totalLines += qStockValueReport.Quantity
                totalSaleValue += Round(qStockValueReport.AveragePurchaseCost * qStockValueReport.Quantity,.01)
                   
            END! LOOP
            
            expfil.AddField('Total Number Of Lines',1)
            expfil.AddField(,,,4)
            expfil.AddField(totalLines)
            expfil.AddField(,,,2)
            expfil.AddField(FORMAT(totalSaleValue,@n_14.2),,1)
            
        END ! IF
        
        expfil.Kill()
        
        Relate:USERS.Close()
        
AddSummaryLine  PROCEDURE()
    CODE
        IF (firstManufacturer = 1)
            expfil.AddField(p_web.GSV('BookingSiteLocation'),1)
        ELSE
            expfil.AddField(,1)
        END ! IF
        firstManufacturer = ''
        expfil.AddField(currentManufacturer)
        expfil.AddField(totalStock)
        expfil.AddField(FORMAT(purchaseValue,@n_14.2))
        expfil.AddField(FORMAT(averagePurchaseCost,@n_14.2))
        expfil.AddField(FORMAT(SaleValue,@n_14.2),,1)
        totalLines += 1
        totalStockItems += totalStock
        totalPurchaseValue += purchaseValue
        totalAveragePurchaseCost += averagePurchaseCost
        totalSaleValue += SaleValue
        
ExportHeader        PROCEDURE()
    CODE
        expfil.AddField('Site Location',1)
        expfil.AddField(p_web.GSV('BookingSiteLocation'),,1)
            
        expfil.AddField('Include Accessories',1)
        IF (p_web.GSV('locIncludeAccessories') = 1)
            expfil.AddField('YES',,1)
        ELSE
            expfil.AddField('NO',,1)
        END ! IF
            
        expfil.AddField('Suppress Zeros',1)
        IF (p_web.GSV('locSuppressZeros') = 1)
            expfil.AddField('YES',,1)
        ELSE
            expfil.AddField('NO',,1)
        END ! IF
            
        expfil.AddField('Printed By',1)
        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = p_web.GSV('BookingUserCode')
        IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
                
        ELSE ! IF
        END ! IF
        expfil.AddField(CLIP(use:Forename) & ' ' & CLIP(use:Surname),,1)
        expfil.AddField('Date Printed',1)
        expfil.AddField(FORMAT(TODAY(),@d06),,1)

!endregion
TagStockTypes       PROCEDURE()
    CODE
        
        
        Relate:TagFile.Open()
        Relate:STOCKTYP.Open()
        
        ProgressBarWeb.Init(RECORDS(STOCKTYP))
        ClearTagFile(p_web)
        STREAM(TagFile)
        Access:STOCKTYP.ClearKey(stp:Use_Exchange_Key)
        stp:Use_Exchange = 'YES'
        stp:Stock_Type = ''
        SET(stp:Use_Exchange_Key,stp:Use_Exchange_Key)
        LOOP UNTIL Access:STOCKTYP.Next() <> Level:Benign
            IF (stp:Use_Exchange <> 'YES')
                BREAK
            END ! IF
            ProgressBarWeb.Update('Tagging All..')
        
            IF (Access:TagFile.PrimeRecord() = Level:Benign)
                tag:SessionID = p_web.SessionID	
                tag:Tagged = 1
                tag:TaggedValue = stp:Stock_Type
                IF (Access:TagFile.TryInsert())
                    Access:TagFile.CancelAutoInc()
                END ! IF
            END ! IF
        END ! LOOP
        FLUSH(TagFile)
	
        Relate:STOCKTYP.Close()
        Relate:TagFile.Close()
! UpdateEngineerNotes
UpdateEngineerNotes PROCEDURE()
locFinalListField       CSTRING(10000)
locFinalList             CSTRING(10000)
i                       LONG()
pos                     LONG()
    CODE
        
        CASE p_web.GetValue('type')
        OF 'add'
            BHAddToDebugLog('PickList: ' & p_web.GSV('locPickList'))
            
            p_web.SSV('locFinalListField',p_web.GSV('locFinalListField') & |
                p_web.GSV('locPickList'))
            
            p_web.SSV('locPickList','')
            
            
            BHAddToDebugLog('FinalList: ' & p_web.GSV('locFinalListField'))
            
        OF 'del'
            BHAddToDebugLog('UpdateEngineersNotes: DEL')
            
            locFinalListField = p_web.GSV('locFinalListField') & ';'
            locFinalList = '|;' & p_web.GSV('locFinalList') & ';'
            LOOP i = 1 TO 10000
                pos = INSTRING(locFinalList,locFinalListField,1,1)
                IF (pos > 0)
                    locFinalListField = locFinalListField[ 1 : pos -1 ] & |
                        locFinalListField[pos + LEN(locFinalList) : 10000]
                    BREAK
                END ! IF
            END !  LOOP
            
            p_web.SSV('locFinalListField',locFinalListField[ 1 : LEN(locFinalListField) - 1])
            p_web.SSV('locFinalList','')
        END ! CASE
        
        CreateScript(packet,'window.open("PickEngineersNotes","_self")')
        Do SendPacket
        
        RETURN
WaybillComplete     PROCEDURE()
    CODE
        ProgressBarWeb.Init(10)
        
        Relate:WAYBILLS.Open()
        Relate:SBO_GenericFile.Open()
        Relate:JOBS.Open()
        Relate:JOBSE.Open()
        Relate:WEBJOB.Open()
        Relate:LOCATLOG.Open()
        
        LOOP 1 TIMES
        
            Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
            way:WayBillNumber = p_web.GSV('locWaybillNumber')
            IF (Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign)
                IF (way:Received = 1)
                    IF (DoesUserHaveAccess(p_web,'RECOMPLETE RECEIVED WAYBILL') = 0)
                        packet = CLIP(packet) & |
                            '<script>alert("You do not have sufficient access to re-complete a received waybill");window.location.href="' & p_web.GSV('ReturnURL') & '";</script>'
                        DO redirect
                        BREAK
                    END ! IF
                ELSE
                    way:Received = TRUE
                    Access:WAYBILLS.TryUpdate()              
                
                END ! IF
            
                Access:SBO_GenericFile.ClearKey(sbogen:Long2Key)
                sbogen:SessionID = p_web.SessionID
                SET(sbogen:Long2Key,sbogen:Long2Key)
                LOOP UNTIL Access:SBO_GenericFile.Next() <> Level:Benign
                    IF (sbogen:SessionID <> p_web.SessionID)
                        BREAK
                    END ! IF
                    ProgressBarWeb.Update('Processing...')

                    Access:JOBS.ClearKey(job:Ref_Number_Key)
                    job:Ref_Number = sbogen:Long2
                    IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)

                        Access:JOBSE.ClearKey(jobe:RefNumberKey)
                        jobe:RefNumber = job:Ref_Number
                        IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                        END ! IF
                        
                        Access:WEBJOB.ClearKey(wob:RefNumberKey)
                        wob:RefNumber = job:Ref_Number
                        IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                        END!  IF

                        
                        IF (sbogen:Byte2 = 0)
                            IF (job:Consignment_Number <> p_web.GSV('locWaybillNumber'))
                        ! Double Check We are in the right waybill
                                CYCLE
                            END ! IF
                        
                            IF (job:Current_Status = p_web.GSV('Default:StatusReceivedAtRRC'))
                                CYCLE
                            END ! IF
                    
                            IF (job:Location = p_web.GSV('Default:RRCLocation'))
                                CYCLE
                            END!  IF
                        
                            Access:LOCATLOG.ClearKey(lot:NewLocationKey)
                            lot:RefNumber = job:Ref_Number
                            lot:NewLocation = 'DespatchToCustomer'
                            IF (Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign)
                                CYCLE
                            END ! IF
                        

                        ELSE
                            IF (wob:ExcWayBillNumber <> p_web.GSV('locWaybillNumber'))
                                CYCLE
                            END ! IF
                            
                            ! Exchange
                            IF (job:Exchange_Status = p_web.GSV('ExchangeStatusReceivedAtRRC'))
                                CYCLE
                            END ! IF
                            
                        END ! IF
                        
                        
                        p_web.FileToSessionQueue(JOBS)
                        p_web.FileToSessionQueue(WEBJOB)
                        p_web.FileToSessionQUEUE(JOBSE)
                        
                        IF (sbogen:Byte2 = 0)
                            GetStatus(SUB(p_web.GSV('Default:StatusRRCReceivedQuery'),1,3),0,'JOB',p_web)
                        ELSE
                            GetStatus(SUB(p_web.GSV('Default:StatusRRCReceivedQuery'),1,3),0,'EXC',p_web)
                        END  
                        
                        Access:JOBS.ClearKey(job:Ref_Number_Key)
                        job:Ref_Number = p_web.GSV('job:Ref_Number')
                        IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                        END ! IF
                        
                        Access:JOBSE.ClearKey(jobe:RefNumberKey)
                        jobe:RefNumber = job:Ref_Number
                        IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                        END ! IF
                        
                        Access:WEBJOB.ClearKey(wob:RefNumberKey)
                        wob:RefNumber = job:Ref_Number
                        IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                        END!  IF
                        
                        p_web.SessionQueueToFile(JOBS)
                        p_web.SessionQueueToFile(WEBJOB)
                        p_web.SessionQueueToFile(JOBSE)
                        
                        job:Workshop = 'YES'
                        IF (Access:JOBS.TryUpdate() = Level:Benign)
                            Access:WEBJOB.TryUpdate()
                            Access:JOBSE.TryUpdate()
                        END ! IF
                        
                    END ! IF
                END ! LOOP
            
                p_web.SSV('locWaybillFound',0)
                p_web.SSV('locWaybillNumber','') ! Reset Screen
                p_web.SSV('locJobFound',0)
                
                packet = CLIP(packet) & | 
                    '<script>alert("Waybill Is Complete");</script>'
                
            END ! IF
        
            
        END ! LOOP
        
        Relate:SBO_GenericFile.Close()
        Relate:WAYBILLS.Close()
        Relate:JOBS.Close()
        Relate:JOBSE.Close()
        Relate:WEBJOB.Close()

        Relate:LOCATLOG.Close()
!region WarrantyIncomeReportEngine
WarrantyIncomeReportEngine  PROCEDURE()!
                    MAP
BuildQ                  PROCEDURE(),BYTE
                    END ! MAP
recs                            LONG(0)
i                               LONG()
reportOrder                     LONG()
dateRange                       LONG()
totalJobs                       LONG()
    CODE
        ProgressBarWeb.Init(500)
        
        dateRange = p_web.GSV('locDateRangeType')
        reportOrder = p_web.GSV('locReportOrder')
        
        recs = RECORDS(qWIncomeReport)
        LOOP i = recs TO 1 BY -1
            GET(qWIncomeReport,i)
            IF (qWIncomeReport.SessionID = p_web.SessionID)
                p_web.Noop()
                DELETE(qWIncomeReport)
            END ! IF
        END ! LOOP
        
        Relate:TRADEACC.Open()
        Relate:JOBSWARR.Open()
        Relate:JOBS.Open()
        Relate:WEBJOB.Open()
        Relate:JOBSE.Open()
        Relate:REPTYDEF.Open()
        Relate:JOBSE.Open()
        Relate:WARPARTS.Open()
        Relate:SUBTRACC.Open()
        Relate:AUDIT.Open()
        Relate:AUDIT2.Open()
        Relate:Audit_Alias.Open()

        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = p_web.GSV('BookingAccount')
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
            
        END ! IF
        
        totalJobs = 0
        
        IF (dateRange = 0)
            Access:JOBS.ClearKey(job:DateCompletedKey)
            job:Date_Completed = p_web.GSV('locStartDate')
            SET(job:DateCompletedKey,job:DateCompletedKey)
            LOOP UNTIL Access:JOBS.Next() <> Level:Benign
                IF (job:Date_Completed > p_web.GSV('locEndDate'))
                    BREAK
                END ! IF
                
                ProgressBarWeb.Update('Building Report...')
               
                Access:JOBSWARR.ClearKey(jow:RefNumberKey)
                jow:RefNumber = job:Ref_Number
                IF (Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign)
                    
                ELSE ! IF
                    ! #13413 Allow excluded jobs through as well. (DBH: 03/11/2014)
                END ! IF
                
                IF (BuildQ())
                    CYCLE
                END ! IF
                totalJobs += 1
            END ! LOOP
        ELSE ! IF (reportOrder = 0)
            CASE dateRange
            OF 1 ! Submitted Date
                Access:JOBSWARR.ClearKey(jow:SubmittedBranchKey)
                jow:BranchID = tra:BranchIdentification
                jow:ClaimSubmitted = p_web.GSV('locStartDate')
                SET(jow:SubmittedBranchKey,jow:SubmittedBranchKey)
            OF 2 ! Date Accepted    
                Access:JOBSWARR.ClearKey(jow:AcceptedBranchKey)
                jow:BranchID = tra:BranchIdentification
                jow:DateAccepted = p_web.GSV('locStartDate')
                SET(jow:AcceptedBranchKey,jow:AcceptedBranchKey)
            END ! CASE

            LOOP UNTIL Access:JOBSWARR.Next() <> Level:Benign
                IF (jow:BranchID <> tra:BranchIdentification)
                    BREAK
                END ! IF
                
                CASE dateRange
                OF 1 ! Submitted
                    IF (jow:ClaimSubmitted > p_web.GSV('locEndDate'))
                        BREAK
                    END ! IF
                OF 2 ! Date Accepted
                    IF (jow:DateAccepted > p_web.GSV('locEndDate'))
                        BREAK
                    END ! IF
                END ! CASE
                
                Access:JOBS.ClearKey(job:Ref_Number_Key)
                job:Ref_Number = jow:RefNumber
                IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                    
                ELSE ! IF
                    ProgressBarWeb.Update('Building Report...')
                    CYCLE
                END ! IF                
                
                IF (BuildQ())
                    CYCLE
                END ! IF
                
                totalJobs += 1
            END ! LOOP
        END ! IF (reportOrder = 0)
        
        IF (dateRange <> 2 AND p_web.GSV('locExcludedOnly') <> 1)
            BHAddToDebugLog('WarrantyIncomeReportExport: Check Audit Trail')
            Access:Audit_Alias.ClearKey(aud1:ActionOnlyKey)
            aud1:Action = 'WARRANTY CLAIM RESUBMITTED'
            aud1:Date = p_web.GSV('locStartDate')
            SET(aud1:ActionOnlyKey,aud1:ActionOnlyKey)
            LOOP UNTIL Access:Audit_Alias.Next() <> Level:Benign
                IF (aud1:Action <> 'WARRANTY CLAIM RESUBMITTED' OR |
                    aud1:Date > p_web.GSV('locEndDate'))
                    BREAK
                END ! IF
                BHAddToDebugLog('WarrantyIncomeReportExport: Audit Check = ' & job:Ref_Number & ' / ' & p_web.SessionID)
                
                Access:JOBS.ClearKey(job:Ref_Number_Key)
                job:Ref_Number = aud1:Ref_Number
                IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                    
                ELSE ! IF
                    CYCLE
                END ! IF 
                
                BHAddToDebugLog('WarrantyIncomeReportExport: Audit Check (After Job) = ' & job:Ref_Number & ' / ' & p_web.SessionID)

                Access:JOBSWARR.ClearKey(jow:RefNumberKey)
                jow:RefNumber = job:Ref_Number
                IF (Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign)
                    
                ELSE ! IF
                    ProgressBarWeb.Update('Building Report...')
                    CYCLE
                END ! IF
                
                BHAddToDebugLog('WarrantyIncomeReportExport: Audit Check (After Jobswarr) = ' & job:Ref_Number & ' / ' & p_web.SessionID)
                
                IF (BuildQ())
                    CYCLE
                END ! IF
                
                totalJobs += 1
            END ! LOOP
        END ! IF
        
        Relate:JOBSE.Close()
        Relate:WEBJOB.Close()
        Relate:JOBS.Close()
        Relate:JOBSWARR.Close()       
        Relate:TRADEACC.Close()
        Relate:REPTYDEF.Close()
        Relate:JOBSE.Close()
        Relate:WARPARTS.Close()
        Relate:SUBTRACC.Close()
        Relate:AUDIT2.Close()
        Relate:AUDIT.Close()
        Relate:Audit_Alias.Close()
        
        IF (totalJobs = 0)
            CreateScript(packet,kNoREcordsAlert)
            DO ReturnFail
        END ! IF
        
        CASE reportOrder
        OF 0
            SORT(qWIncomeReport,qWIncomeReport.SessionID,qWIncomeReport.Manufacturer,qWIncomeReport.ModelNumber,qWIncomeReport.RefNumber)
        OF 1
            SORT(qWIncomeReport,qWIncomeReport.SessionID,qWIncomeReport.RefNumber)
        OF 2
            SORT(qWIncomeReport,qWIncomeReport.SessionID,qWIncomeReport.CompletedDate,qWIncomeReport.RefNumber)
        END ! CASE
        
        IF (p_web.GetValue('ReportType') = 'E')
            WarrantyIncomeReportExport()
        ELSE
            CallReport('WarrantyIncomeReport')
        END ! IF
        
        
BuildQ              PROCEDURE()!,BYTE
sentTOARC               LONG(0)

    CODE
        
        ! Check for duplicates
        BHAddToDebugLog('WarrantyIncomeReportExport: Lookup Q = ' & job:Ref_Number & ' / ' & p_web.SessionID)
        qWIncomeReport.RefNumber = job:Ref_Number
        qWIncomeReport.SessionID = p_web.SessionID
        GET(qWIncomeReport,qWIncomeReport.SessionID,qWIncomeReport.RefNumber)
        IF ~(ERROR())
            BHAddToDebugLog('WarrantyIncomeReportExport: Found Duplicate = ' & job:Ref_Number & ' / ' & p_web.SessionID)
            RETURN Level:Fatal
        END ! IF        
        
        !BHAddToDebugLog('WarrantyIncomeReportExport: BuildQ = ' & job:Ref_Number)
    
        ProgressBarWeb.Update('Building Report...')
        
        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = job:Ref_Number
        IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
            
        ELSE ! IF
            RETURN Level:Fatal
        END ! IF
        
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
            IF (wob:HeadAccountNumber <> p_web.GSV('BookingAccount'))
                ! Don't think this check is necessary
                RETURN Level:Fatal
            END ! IF            
        ELSE ! IF
        END ! IF
        
        IF (job:Warranty_Job <> 'YES')
            RETURN Level:Fatal
        END ! IF
        IF (job:Cancelled = 'YES')
            RETURN Level:Fatal
        END ! IF
        
        BHAddToDebugLog('WarrantyIncomeReportExport: Found Warranty Check = ' & job:Ref_Number)
        
        IF (p_web.GSV('locAllAccounts') <> 1)
            Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
            tagf:SessionID = p_web.SessionID
            IF (p_web.GSV('locGenericAccounts') = 0)
                tagf:TagType = kSubAccount
            ELSE
                tagf:TagType = kGenericAccount
            END ! IF
            tagf:TaggedValue = job:Account_Number
            IF (Access:SBO_GenericTagFile.TryFetch(tagf:KeyTagged) = Level:Benign)
                IF (tagf:Tagged = 0)
                    RETURN Level:Fatal
                END ! IF
            ELSE ! IF
                RETURN Level:Fatal
            END ! IF
        END ! IF        

        IF (p_web.GSV('locAllChargeTypes') <> 1)
            Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
            tagf:SessionID = p_web.SessionID
            tagf:TagType = 'C'
            tagf:TaggedValue = job:Warranty_Charge_Type
            IF (Access:SBO_GenericTagFile.TryFetch(tagf:KeyTagged) = Level:Benign)
                IF (tagf:Tagged = 0)
                    RETURN Level:Fatal
                END ! IF
            ELSE ! IF
                RETURN Level:Fatal
            END ! IF            
        END ! IF
        IF (p_web.GSV('locAllManufacturers') <> 1)
            Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
            tagf:SessionID = p_web.SessionID
            tagf:TagType = 'M'
            tagf:TaggedValue = job:Manufacturer
            IF (Access:SBO_GenericTagFile.TryFetch(tagf:KeyTagged) = Level:Benign)
                IF (tagf:Tagged = 0)
                    RETURN Level:Fatal
                END ! IF
            ELSE ! IF
                RETURN Level:Fatal
            END ! IF
        END ! IF
        
        BHAddToDebugLog('WarrantyIncomeReportExport: Before Status Check = ' & job:Ref_Number)
        
        IF (p_web.GSV('locAllWarrantyStatus') <> 1)
            CASE jobe:WarrantyClaimStatus
            OF 'SUBMITTED'
                IF (p_web.GSV('locSubmitted') <> 1)
                    RETURN Level:Fatal
                END ! IF
            OF 'RESUBMITTED'
                IF (p_web.GSV('locResubmitted') <> 1)
                    RETURN Level:Fatal
                END ! IF
            OF 'REJECTED'
                IF (p_web.GSV('locRejected') <> 1)
                    RETURN Level:Fatal
                END ! IF
            OF 'REJECTION ACKNOWLEDGED' OROF 'FINAL REJECTION'
                IF (p_web.GSV('locRejectionAcknowledged') <> 1)
                    RETURN Level:Fatal
                END ! IF
            OF 'RECONCILED'
                IF (p_web.GSV('locReconciled') <> 1)
                    RETURN Level:Fatal
                END ! IF
            END ! CASE
        END ! IF
        BHAddToDebugLog('WarrantyIncomeReportExport: Before Excluded Only = ' & job:Ref_Number)
        
        IF (p_web.GSV('locExcludedOnly') = 1)
            IF (job:Bouncer <> 'X')
                Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
                rtd:Manufacturer = job:Manufacturer
                rtd:Warranty = 'YES'
                rtd:Repair_Type = job:Repair_Type_Warranty
                IF (Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign)
                    IF (rtd:BER <> 7)
                        RETURN Level:Fatal
                    END ! IF
                ELSE ! IF
                    RETURN Level:Fatal
                END ! IF
            END ! IF
        END ! IF
        
        ! Write To Q
        CLEAR(qWIncomeReport)
        qWIncomeReport.RefNumber = job:Ref_Number
        qWIncomeReport.SessionID = p_web.SessionID
        
        IF (job:Exchange_Unit_Number > 0 AND jobe:ExchangedAtRRC)
            qWIncomeReport.HandlingFee = jobe:ExchangeRate
        END ! IF
        
        sentToARC = JobSentToARC()
        
        BHAddToDebugLog('WarrantyIncomeReportExport: Before Include ARC Repaired Jobs = ' & job:Ref_Number)
        
        IF (p_web.GSV('locIncludeARCRepairedJobs') <> 1)
            IF (sentToARC = TRUE)
                RETURN Level:Fatal
            END ! IF
        END ! IF        
        
        IF (sentToARC)
            qWIncomeReport.HandlingFee += jobe:HandlingFee
        ELSE ! IF
            qWIncomeReport.PartsSelling = jobe:RRCWPartsCost
            qWIncomeReport.Labour = jobe:RRCWLabourCost
            qWIncomeReport.SubTotal = jobe:RRCWLabourCost + jobe:RRCWPartsCost
            qWIncomeReport.VAT = jobe:RRCWPartsCost * VodacomClass.VatRate(job:Account_Number,'L')/100 + |
                jobe:RRCWLabourCost * VodacomClass.VatRate(job:Account_Number,'P')/100 + |
                job:Courier_Cost_Warranty * VodacomClass.VatRate(job:Account_Number,'L')/100
            qWIncomeReport.Total = qWIncomeReport.PartsSelling + qWIncomeReport.Labour + qWIncomeReport.VAT + job:Courier_Cost_Warranty
            
        END ! IF
        
        BHAddToDebugLog('WarrantyIncomeReportExport: Before Zero Supression = ' & job:Ref_Number)
        
        IF (p_web.GSV('locZeroSupression') = 'Y')
            IF (qWIncomeReport.Total = 0)
                RETURN Level:Fatal
            END ! IF
        END ! IF

        Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
        wpr:Ref_Number = job:Ref_Number
        SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
        LOOP UNTIL Access:WARPARTS.Next() <> Level:Benign
            IF (wpr:Ref_Number <> job:Ref_Number)
                BREAK
            END ! IF
            IF (wpr:Correction)
                CYCLE
            END ! IF
            IF (sentToARC = FALSE)  ! #13413 Only show costs when job NOT sent to ARC (DBH: 31/10/2014)
                qWIncomeReport.PartsCost += wpr:RRCAveragePurchaseCost * wpr:Quantity
            END ! IF
            
        END ! LOOP
        
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = wob:HeadAccountNumber
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
        END ! IF
        
        qWIncomeReport.BranchID = tra:BranchIdentification
        qWIncomeReport.JobNumber = wob:JobNumber
        qWIncomeReport.FranchiseAccountNumber = tra:Account_Number
        qWIncomeReport.FranchiseAccountName = tra:Company_Name
        qWIncomeReport.FullJobNumber = job:Ref_Number & '-' & CLIP(tra:BranchIdentification) & wob:JobNumber
        
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = job:Account_Number
        IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
            qWIncomeReport.CompanyName = sub:Company_Name
        ELSE ! IF
        END ! IF
        
        qWIncomeReport.JobAccountNumber = job:Account_Number
        
        IF (sentToARC)
            qWIncomeReport.Repair = 'ARC'
        ELSE
            qWIncomeReport.Repair = 'RRC'
        END ! IF
        
        IF (job:Exchange_Unit_Number = 0)
            qWIncomeReport.Exchanged = ''
        ELSE ! IF
            IF (jobe:ExchangedATRRC)
                qWIncomeReport.Exchanged = 'RRC'
            ELSE ! IF
                qWIncomeReport.Exchanged = 'ARC'
            END ! IF
        END ! IF
        
        qWIncomeReport.WarrantyStatus = ''
        qWIncomeReport.RejectionBy = ''
        
        CASE wob:EDI
        OF 'XXX'
            qWIncomeReport.WarrantyStatus = 'EXCLUDED'
        OF 'NO'
            qWIncomeReport.WarrantyStatus = 'PENDING'
        OF 'YES'
            qWIncomeReport.WarrantyStatus = 'IN PROGRESS'
        OF 'EXC'
            qWIncomeReport.WarrantyStatus = 'REJECTED'
        OF 'PAY'
            qWIncomeReport.WarrantyStatus = 'PAID'            
        OF 'APP'
            qWIncomeReport.WarrantyStatus = 'APPROVED'
        OF 'AAJ'
            qWIncomeReport.WarrantyStatus = 'ACCEPTED-REJECTION'    
            Access:AUDIT.ClearKey(aud:TypeActionKey)
            aud:Ref_Number = job:Ref_Number
            aud:Type = 'JOB'
            aud:Action = 'WARRANTY CLAIM REJECTION ACKNOWLEDGED'
            SET(aud:TypeActionKey,aud:TypeActionKey)
            LOOP UNTIL Access:AUDIT.Next() <> Level:Benign
                IF (aud:Ref_Number <> job:Ref_Number OR |
                    aud:Type <> 'JOB' OR |
                    aud:Action <> 'WARRANTY CLAIM REJECTION ACKNOWLEDGED')
                    BREAK
                END ! IF
                Access:USERS.ClearKey(use:User_Code_Key)
                use:User_Code = aud:User
                IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
                    qWIncomeReport.RejectionBy = CLIP(use:Forename) & ' ' & CLIP(use:Surname)
                ELSE ! IF
                END ! IF
            END ! LOOP
        OF 'REJ'    
            qWIncomeReport.WarrantyStatus = 'FINAL REJECTION'
        END ! CASE
        
        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = job:Engineer
        IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
            qWIncomeReport.Engineer = CLIP(use:Forename) & ' ' & CLIP(use:Surname)
        ELSE ! IF
            qWIncomeReport.Engineer = ''
        END ! IF
        
        qWIncomeReport.Manufacturer = job:Manufacturer
        qWIncomeReport.ModelNumber = job:Model_Number
        qWIncomeReport.ChargeType = job:Warranty_Charge_Type
        qWIncomeReport.RepairType = job:Repair_Type_Warranty
        qWIncomeReport.CompletedDate = job:Date_Completed
        qWIncomeReport.ClaimSubmitted = FORMAT(jow:ClaimSubmitted,@d06)
        
        BHAddToDebugLog('WarrantyIncomeReportExport: Added To Q = ' & job:Ref_Number & ' / ' & p_web.SessionID)
        
        ADD(qWIncomeReport)
        
        RETURN Level:Benign
!endregion        
!region WarrantyIncomeReportExport
WarrantyIncomeReportExport  PROCEDURE()
TotalGroup                      GROUP,PRE(total)
Lines                               LONG()
HandlingFee                         DECIMAL(10,2)
Labour                              DECIMAL(10,2)
Parts                               DECIMAL(10,2)
PartsSelling                        DECIMAL(10,2)
SubTotal                            DECIMAL(10,2)
VAT                                 DECIMAL(10,2)
Total                               DECIMAL(10,2)
                                END ! GROUP
i                               LONG()      
recs                            LONG()
countReasons                    LONG()
rejectionReason                 STRING(255)
    CODE
        Relate:USERS.Open()
        Relate:AUDIT.Open()
        Relate:AUDIT2.Open()
    
        ProgressBarWeb.Init(RECORDS(qWIncomeReport))
        
        expfil.Init('Warranty Income Report')
        expfil.AddField('RRC Warranty Income Report',1,1)
        
        CASE p_web.GSV('locReportOrder')
        OF 0
            expfil.AddField('Completed Date Range',1)
        OF 1
            expfil.AddField('Submitted Date Range',1)
        OF 2
            expfil.AddField('Accepted Date Range',1)
        END ! CASE
        expfil.AddField(FORMAT(p_web.GSV('locStartDate'),@d06) & ' to ' & FORMAT(p_web.GSV('locEndDate'),@d06),,1)
        
        expfil.AddField('Printed By',1)
        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = p_web.GSV('BookingUserCode')
        IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
            expfil.AddField(CLIP(use:Forename) & ' ' & CLIP(use:Surname),,1)
        ELSE ! IF
            expfil.AddField(,,1)
        END ! IF
        
        expfil.AddField('Date Printed',1)
        expfil.AddField(FORMAT(TODAY(),@d06),,1)
        
        expfil.AddField('SB Job No',1)
        expfil.AddField('Franchise Branch ID')
        expfil.AddField('Franchise Job No')
        expfil.AddField('Completed')
        expfil.AddField('Head Account No')
        expfil.AddField('Head Account Name')
        expfil.AddField('Account No')
        expfil.AddField('Account Name')
        expfil.AddField('Manufacturer')
        expfil.AddField('Model Number')
        expfil.AddField('Warranty Charge Type')
        expfil.AddField('Warranty Repair Type')
        expfil.AddField('Engineer')
        expfil.AddField('Repair')
        expfil.AddField('Exchange')
        expfil.AddField('Handling/Swap Fee')
        expfil.AddField('Parts Cost')
        expfil.AddField('Parts Sale')
        expfil.AddField('Labour')
        expfil.AddField('Sub Total')
        expfil.AddField('VAT')
        expfil.AddField('Total')
        expfil.AddField('Warranty Status')
        expfil.AddField('Rejection Reason 1')
        expfil.AddField('Rejection Reason 2')
        expfil.AddField('Rejection Reason 3')
        expfil.AddField('Rejection Accepted By')
        expfil.AddField('Claim Submitted',,1)
        
        CLEAR(TotalGroup)
        
        BHAddToDebugLog('WarrantyIncomeReportExport: Records = ' & RECORDS(qWIncomeReport))
        
        LOOP i = 1 TO RECORDS(qWIncomeReport)
            GET(qWIncomeReport,i)
            
            ProgressBarWeb.Update('Exporting.....')
            
            IF (qWIncomeReport.SessionID <> p_web.SessionID)
                CYCLE
            END ! IF
            
            ! Job Number
            expfil.AddField(qWIncomeReport.RefNumber,1)
            ! Branch ID
            expfil.AddField(qWIncomeReport.BranchID)
            ! Web Job Number
            expfil.AddField(qWIncomeReport.JobNumber)
            ! Completed
            expfil.AddField(FORMAT(qWIncomeReport.CompletedDate,@d06))
            ! Account Number
            expfil.AddField(qWIncomeReport.FranchiseAccountNumber)
            ! Account Name
            expfil.AddField(qWIncomeReport.FranchiseAccountName)
            ! Job Account No
            expfil.AddField(qWIncomeReport.JobAccountNumber)
            ! Company Name
            expfil.AddField(qWIncomeReport.CompanyName)
            ! Manufacturer
            expfil.AddField(qWIncomeReport.Manufacturer)
            ! Model Number
            expfil.AddField(qWIncomeReport.ModelNumber)!
            ! Charge Type
            expfil.AddField(qWIncomeReport.ChargeType)
            ! Repair Type
            expfil.AddField(qWIncomeReport.RepairType)
            ! Engineer
            expfil.AddField(qWIncomeReport.Engineer)
            ! Repair
            expfil.AddField(qWIncomeReport.Repair)
            ! Exchanged
            expfil.AddField(qWIncomeReport.Exchanged)
            ! Handling Fee
            expfil.AddField(qWIncomeReport.HandlingFee)
            ! Parts Cost
            expfil.AddField(qWIncomeReport.PartsCost)
            ! Parts Selling Cost
            expfil.AddField(qWIncomeReport.PartsSelling)
            ! Labour Cost
            expfil.AddField(qWIncomeReport.Labour)
            ! Sub Total
            expfil.AddField(qWIncomeReport.SubTotal)
            ! VAT
            expfil.AddField(qWIncomeReport.VAT)
            ! Total
            expfil.AddField(qWIncomeReport.Total)
            ! Warranty Status
            expfil.AddField(qWIncomeReport.WarrantyStatus)
            ! Rejection Reasons
            qWIncomeReport.RejectionReason1 = ''
            qWIncomeReport.RejectionReason2 = ''
            qWIncomeReport.RejectionReason3 = ''
            
            countReasons = 1
            Access:AUDIT.ClearKey(aud:TypeActionKey)
            aud:Ref_Number = qWIncomeReport.RefNumber
            aud:Type = 'JOB'
            aud:Action = 'WARRANTY CLAIM REJECTED'
            SET(aud:TypeActionKey,aud:TypeActionKey)
            LOOP UNTIL Access:AUDIT.Next() <> Level:Benign
                IF (aud:Ref_Number <> qWIncomeReport.RefNumber OR |
                    aud:Type <> 'JOB' OR |
                    aud:Action <> 'WARRANTY CLAIM REJECTED')
                    BREAK
                END ! IF
                Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
                aud2:AUDRecordNumber = aud:Record_Number
                IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
                    rejectionReason = Clip(BHStripNonAlphaNum(BHStripReplace(Sub(aud2:Notes,9,255),',',''),' '))
                    CASE countReasons
                    OF 1
                        qWIncomeReport.RejectionReason1 = rejectionReason
                    OF 2
                        qWIncomeReport.RejectionReason2 = rejectionReason
                    OF 3
                        qWIncomeReport.RejectionReason3 = rejectionReason
                    ELSE
                        BREAK
                    END ! CASE
                    countReasons += 1
                ELSE ! IF
                END ! IF
            END ! LOOP            
            
            expfil.AddField(qWIncomeReport.RejectionReason1)
            expfil.AddField(qWIncomeReport.RejectionReason2)
            expfil.AddField(qWIncomeReport.RejectionReason3)
            ! Rejection Now
            expfil.AddField(qWIncomeReport.RejectionBy)
            ! Claim Submitted
            expfil.AddField(qWIncomeReport.ClaimSubmitted,,1)
            
            total:Lines             += 1
            total:HandlingFee       += qWIncomeReport.HandlingFee
            total:Labour            += qWIncomeReport.Labour
            total:Parts             += qWIncomeReport.PartsCost
            total:PartsSelling      += qWIncomeReport.PartsSelling
            total:SubTotal          += (qWIncomeReport.PartsSelling + qWIncomeReport.Labour)
            total:VAT               += qWIncomeReport.Vat
            total:Total             += qWIncomeReport.Total    
            
        END ! LOOP
        
        ! Totals
        expfil.AddField('TOTALS',1,1)
        expfil.AddField('Jobs Counted',1)
        expfil.AddField(total:Lines)
        expfil.AddField(,,,13)
        expfil.AddField(total:HandlingFee)
        expfil.AddField(total:Parts)
        expfil.AddField(total:PartsSelling)
        expfil.AddField(total:Labour)
        expfil.AddField(total:Subtotal)
        expfil.AddField(total:VAT)
        expfil.AddField(total:Total,,1)
    
        Relate:USERS.Close()
        Relate:AUDIT2.Close()
        Relate:AUDIT.Close()
        
        
        expfil.Kill()
        
        LOOP i = recs TO 1 BY -1
            GET(qIncomeReport,i)
            IF (qIncomeReport.SessionID = p_web.SessionID)
                DELETE(qIncomeReport)
            END ! IF
        END ! LOOP
!endregion
! WaybillConfirmation
WaybillConfirmation PROCEDURE()
wayError    LONG(0)
    CODE
        p_web.SSV('txtWaybillReceived','')
        ClearSBOGenericFile(p_web)
    
        p_web.SSV('locWaybillFound',0)
        p_web.SSV('locSundryWaybillFound',0)    
    
        Relate:WAYBILLS.Open()
        Relate:WAYBILLJ.Open()
        Relate:JOBS.Open()
        Relate:EXCHANGE.Open()
        Relate:SBO_GenericFile.Open()
        
        p_web.StoreValue('locWaybillNumber')
        
        Access:WAYBILLS.ClearKey(way:WaybillNumberKey)
        way:WaybillNumber = p_web.GSV('locWaybillNumber')
        IF (Access:WAYBILLS.TryFetch(way:WaybillNumberKey))
            CreateScript(packet,'alert("Waybill Number Not Found")')
            DO ReturnFail
            RETURN
        END ! IF
        
        p_web.FileToSessionQueue(WAYBILLS)
        
        wayError = 0
        IF (p_web.GSV('BookingSite') = 'RRC')
            IF (way:WayBillType <> 1)
                ! ERROR 
                wayError = 1
            ELSE
                IF (way:AccountNumber <> p_web.GSV('BookingAccount'))
                    wayError = 1
                END ! IF
            END ! IF
        ELSE
            IF (way:WayBillType <> 0)
                ! ERROR 
                wayError = 1
            END ! IF
        END !IF   

        IF (wayError = 1)
            CreateScript(packet,'alert("Waybill Number Not Found For This Site")')
            DO ReturnFail
            RETURN        
        END ! IF
        IF (way:Received)
            p_web.SSV('txtWaybillReceived','Waybill Received')
        END ! IF
        
        IF (way:WaybillID = 300)
            p_web.SSV('locSundryWaybillFound',1)
        ELSE
            p_web.SSV('locWaybillFound',1) 
            
            ProgressBarWeb.Init(30,1)
            
            Access:WAYBILLJ.ClearKey(waj:JobNumberKey)
            waj:WayBillNumber = way:WayBillNumber
            SET(waj:JobNumberKey,waj:JobNumberKey)
            LOOP UNTIL Access:WAYBILLJ.Next() <> Level:Benign
                IF (waj:WayBillNumber <> way:WayBillNumber)
                    BREAK
                END ! IF
                
                ProgressBarWeb.Update('Finding Jobs...')
                
                Access:JOBS.ClearKey(job:Ref_Number_Key)
                job:Ref_Number = waj:JobNumber
                IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                END ! IF
                
                IF (Access:SBO_GenericFile.PrimeRecord() = Level:Benign)
                    sbogen:SessionID = p_web.SessionID
                    sbogen:Long2 = waj:JobNumber
                    sbogen:String1 = waj:IMEINumber
                    sbogen:String3 = job:date_booked
                    
                    CASE waj:JobType
                    OF 'JOB'
                        sbogen:Byte1 = 0                            
                        sbogen:String2 = job:Model_Number
                        IF (p_web.GSV('BookingSite') = 'RRC')
                            IF (job:Location = p_web.GSV('Default:InTransitRRC'))
                                sbogen:Long1 = 0
                            ELSE
                                sbogen:Long1 = 1
                            END ! IF
                        ELSE ! IF
                            IF (job:Location = p_web.GSV('Default:InTransitARC'))
                                sbogen:Long1 = 0
                            ELSE
                                sbogen:Long1 = 1
                            END ! IF
                        END ! IF
                        
                        
                    OF 'EXC'
                        sbogen:Byte1 = 1
                        Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                        xch:Ref_Number = job:Exchange_Unit_Number
                        IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                        END !I F
                        sbogen:String2 = xch:Model_Number
                        
                        If Sub(job:Exchange_Status,1,3) = Sub(GETINI('RRC','ExchangeStatusDespatchToRRC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3)
                            sbogen:Long1 = 0
                        ELSE ! IF
                            sbogen:Long1 = 1
                        END !IF
                    END ! CASE
                    IF (Access:SBO_GenericFile.TryInsert())
                        Access:SBO_GenericFile.CancelAutoInc()
                    END ! IF
                END ! IF
            END!  LOOp    
        END ! IF
        
        Relate:WAYBILLJ.Close()
        Relate:JOBS.Close()
        Relate:EXCHANGE.Close()
        Relate:SBO_GenericFile.Close()
        Relate:WAYBILLS.Close()
! WaybillGeneration
WaybillGeneration   PROCEDURE()
recordCount             LONG()
    CODE
        
        IF (p_web.IFExistsValue('wayType'))
            p_web.StoreValue('wayType')
        END ! IF
        
        IF (p_web.GetValue('force') <> 1)
            ! Only check access level, if not being "forced", i.e. have reached 10
            CASE p_web.GSV('wayType')
            OF 'OBF'
                IF (DoesUserHaveAccess(p_web,'GENERATE OBF WAYBILL') = 0)
                    CreateScript(packet,'alert("You do not have access to this option.")')
                    DO ReturnFail
                    RETURN
                END! IF
            
            OF 'NR'
                IF (DoesUserHaveAccess(p_web,'GENERATE NR/48E WAYBILL') = 0)
                    CreateScript(packet,'alert("You do not have access to this option.")')
                    DO ReturnFail
                    RETURN
                END! IF
            END ! CASE
        END ! IF
        
        
        ProgressBarWeb.Init(20)
        
        Relate:WAYBPRO.Open()
        Relate:JOBS.Open()
        Relate:JOBSE.Open()
        Relate:TRADEACC.Open()
        Relate:COURIER.Open()
        Relate:WAYBILLS.Open()
        Relate:WAYBILLJ.Open()
        Relate:WEBJOB.Open()
        
        
        p_web.SSV('locWaybillNumber',0)
        
        !TB13195 - need to check if there are less than ten being processed
        recordCount = 0
        
        Access:WAYBPRO.ClearKey(wyp:AccountJobNumberKey)
        wyp:AccountNumber = p_web.GSV('BookingAccount')
        SET(wyp:AccountJobNumberKey,wyp:AccountJobNumberKey)
        LOOP UNTIL Access:WAYBPRO.Next() <> Level:Benign
            
            IF (wyp:AccountNumber <> p_web.GSV('BookingAccount'))
                BREAK
            END ! IF
            
            

            !will need jobse
            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = wyp:JobNumber
            IF (Access:JOBSE.TryFetch(jobe:RefNumberKey))
                CYCLE
            END  ! IF            
            
            !now we can see if this is the right type
            CASE p_web.GSV('wayType')
            OF 'OBF'
                IF (jobe:OBFvalidated <> 1)
                    CYCLE
                END ! IF
            OF 'NR'
                IF (jobe:OBFvalidated <> 0)
                    CYCLE
                END ! IF
            END ! CASE
            
            !this matches - count it
            recordCount += 1

        END !preloop
        
        
        If RecordCount > 0!9 then
            
            !OK to go ahead
            !end TB13195
            
            p_web.SSV('Waybill:ToAccount','')
            p_web.SSV('Waybill:ToType','')
            p_web.SSV('Waybill:FromAccount','')
            p_web.SSV('Waybill:FromType','')
            p_web.SSV('Waybill:Courier','')
                
            p_web.SSV('FirstTime',1)
            
            
            Access:WAYBPRO.ClearKey(wyp:AccountJobNumberKey)
            wyp:AccountNumber = p_web.GSV('BookingAccount')
            SET(wyp:AccountJobNumberKey,wyp:AccountJobNumberKey)
            LOOP UNTIL Access:WAYBPRO.Next() <> Level:Benign
                IF (wyp:AccountNumber <> p_web.GSV('BookingAccount'))
                    BREAK
                END ! IF
                
                
                
                Access:JOBS.ClearKey(job:Ref_Number_Key)
                job:Ref_Number = wyp:JobNumber
                IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
                    CYCLE
                END ! IF
                p_web.FileToSessionQueue(JOBS)
                
                
                Access:JOBSE.ClearKey(jobe:RefNumberKey)
                jobe:RefNumber = job:Ref_Number
                IF (Access:JOBSE.TryFetch(jobe:RefNumberKey))
                    CYCLE
                END  ! IF
                p_web.FileToSessionQueue(JOBSE)
                
                
                Access:WEBJOB.ClearKey(wob:RefNumberKey)
                wob:RefNumber = job:Ref_Number
                IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
                    CYCLE
                END ! IF
                p_web.FileToSessionQueue(WEBJOB)
                
                            
                IF (JobInUse(job:Ref_Number))
                    
                    CYCLE
                END ! IF
                
                
                
                CASE p_web.GSV('wayType')
                OF 'OBF'
                    IF (jobe:OBFvalidated <> 1)
                        CYCLE
                    END ! IF
                OF 'NR'
                    IF (jobe:OBFvalidated <> 0)
                        CYCLE
                    END ! IF
                END ! CASE
                
                ProgressBarWeb.Update('Printing...')

                IF (p_web.GSV('FirstTime') = 1)

                    p_web.SSV('FirstTime',0)
                    
                    Access:TRADEACC.ClearKey(tra:Account_Number_Key)
                    tra:Account_Number = p_web.GSV('BookingAccount')
                    IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                        IF (tra:Courier_Outgoing <> '')
                            Access:COURIER.ClearKey(cou:Courier_Key)
                            cou:Courier = tra:Courier_Outgoing
                            IF (Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign)
                                IF (cou:PrintWaybill = 1)
                                    p_web.SSV('locWaybillNumber',NextWaybillNumber())
                                ELSE
                                    IF (cou:AutoConsignmentNo)
                                        cou:LastConsignmentNo += 1
                                        Access:COURIER.TryUpdate()
                                        p_web.SSV('locWaybillNumber',cou:LastConsignmentNo)
                                    END ! IF
                                END ! IF
                                
                            END ! IF
                            
                        END ! IF
                        
                    END ! IF
                    
                    
                    
                    IF (p_web.GSV('locWaybillNumber') = 0)
                        BREAK
                    END ! IF
                    
                    Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
                    way:WayBillNumber = p_web.GSV('locWaybillNumber')
                    IF (Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign)
                        way:AccountNumber = p_web.GSV('BookingAccount')
                        way:WaybillID = 1
                        way:FromAccount = p_web.GSV('BookingAccount')
                        way:ToAccount = p_web.GSV('ARC:AccountNumber')
                        IF (Access:WAYBILLS.TryUpdate())
                        END ! IF
                    END ! IF
                        
                    p_web.SSV('Waybill:ToAccount',way:ToAccount)
                    p_web.SSV('Waybill:ToType','TRA')
                    p_web.SSV('Waybill:FromAccount',way:FromAccount)
                    p_web.SSV('Waybill:FromType','TRA')
                    p_web.SSV('Waybill:Courier',tra:Courier_Outgoing)
                    
                END ! IF
                

                
                LocationChange(p_web,p_web.GSV('Default:InTransitARC'))
                
                GetStatus(Sub(GETINI('RRC','StatusDespatchedToARC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3),1,'JOB',p_web)
                
                p_web.SSV('job:Incoming_Consignment_Number',p_web.GSV('locWaybillNumber'))
                
                p_web.SSV('jobe:InSecurityPackNo',wyp:SecurityPackNumber)
                
                
                Access:JOBS.ClearKey(job:Ref_Number_Key)
                job:Ref_Number = p_web.GSV('job:Ref_Number')
                IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
                    CYCLE
                END ! IF
                p_web.SessionQueueToFile(JOBS)
                
                
                Access:JOBSE.ClearKey(jobe:RefNumberKey)
                jobe:RefNumber = p_web.GSV('job:Ref_Number')
                IF (Access:JOBSE.TryFetch(jobe:RecordNumberKey))
                    CYCLE
                END  ! IF
                p_web.SessionQueueToFile(JOBSE)
                
                
                Access:WEBJOB.ClearKey(wob:RefNumberKey)
                wob:RefNumber = p_web.GSV('job:Ref_Number')
                IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
                    CYCLE
                END ! IF
                p_web.SessionQueueToFile(WEBJOB)
                
                IF (Access:JOBS.TryUpdate() = Level:Benign)
                    
                    Access:JOBSE.TryUpdate()
                    
                    !Save Engineer then remove from Job
                    wob:RRCEngineer = job:Engineer
                    Access:WEBJOB.TryUpdate()
                    
                    job:Engineer = ''
                    Access:JOBS.TryUpdate()
                    
                    AddToConsignmentHistory(p_web,job:Ref_Number,'RRC','ARC','RAM',p_web.GSV('locWaybillNumber'),'JOB')
                    
                    IF (Access:WAYBILLJ.PrimeRecord() = Level:Benign)
                        waj:WayBillNumber = p_web.GSV('locWaybillNumber')
                        waj:JobNumber = job:Ref_Number
                        waj:IMEINumber = wyp:IMEINumber
                        waj:OrderNumber = job:Order_Number
                        waj:SecurityPackNumber = wyp:SecurityPackNumber
                        waj:JobType = 'JOB'
                        IF (Access:WAYBILLJ.TryInsert())
                            Access:WAYBILLJ.CancelAutoInc()
                        END ! IF
                    END ! IF
                    
                    AddToAudit(p_web,job:Ref_Number,'JOB','WAYBILL GENERATED: ' & p_web.GSV('locWaybillNumber'),'SECURITY PACK NO: ' & wyp:SecurityPackNumber)
                END ! IF
                Access:WAYBPRO.DeleteRecord(0)
            END ! LOOP
        END !if recordcount > 9
        
        Relate:WAYBPRO.Close()
        Relate:JOBS.Close()
        Relate:JOBSE.Close()
        Relate:TRADEACC.Close()  
        Relate:COURIER.Close()
        Relate:WAYBILLS.Close()
        Relate:WAYBILLJ.Close()
        Relate:WEBJOB.Close()
        
        IF (p_web.GSV('locWaybillNumber') = 0)
            IF (recordCount > 0)                                !was = 0)
                REMOVE(ExportFile)
            END !
            
                packet = CLIP(packet) & |
                    '<script>alert("Unable To Print Any Records. Check there are jobs to print and that they are not in use.");window.location.href="' & p_web.GSV('ReturnURL') & '";</script>'
                DO redirect
!            ELSE
!                !did not count up to ten records - this is not permitted
!                packet = CLIP(packet) & |
!                    '<script>alert("Unable To Print Any Records. You are not authorised to print less then ten jobs.");window.location.href="' & p_web.GSV('ReturnURL') & '";</script>'
!                DO redirect
!            END ! IF
        END ! IF
        
WaybillProcessJob   PROCEDURE()
    CODE
        Relate:JOBS.Open()
        Relate:JOBSE.Open()
        Relate:WEBJOB.Open()
        Relate:SBO_GenericFile.Open()
        Relate:SUBTRACC.Open()
        Relate:TRADEACC.Open()
        Relate:JOBSENG.Open()
        Relate:USERS.Open()
        Relate:JOBACC.Open()
        
        IF (p_web.GSV('locWaybillNumber') > 0 AND p_web.GSV('locJobNumber') > 0)
            Access:SBO_GenericFile.ClearKey(sbogen:Long2Key)
            sbogen:SessionID = p_web.SessionID
            sbogen:Long2 = p_web.GSV('locJobNumber')
            IF (Access:SBO_GenericFile.TryFetch(sbogen:Long2Key) = Level:Benign)
                Access:JOBS.ClearKey(job:Ref_Number_Key)
                job:Ref_Number = sbogen:Long2
                IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                    Access:JOBSE.ClearKey(jobe:RefNumberKey)
                    jobe:RefNumber = job:Ref_Number
                    IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                            
                        Access:WEBJOB.ClearKey(wob:RefNumberKey)
                        wob:RefNumber = job:Ref_Number
                        IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                        END ! IF


                        IF (sbogen:Byte2 = 1)
                            ! Exchange Job
    
                            jobe:DespatchType = 'EXC'
                            Access:JOBSE.TryUpdate()
                            

                            
                            job:Workshop = 'YES'
                            
                             !Reset the Courier back to the Trade Account default.. as per booking
                            Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                            sub:Account_Number  = job:Account_Number
                            If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                            !Found
                                Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                                tra:Account_Number  = sub:Main_Account_Number
                                If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                !Found
                                    If tra:Use_Sub_Accounts = 'YES'
                                        job:Courier = sub:Courier_Outgoing
                                    Else !If tra:Use_Sub_Accounts = 'YES'
                                        job:Courier = tra:Courier_Outgoing
                                    End !If tra:Use_Sub_Accounts = 'YES'
                                Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                !Error
                                End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                            Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                            !Error
                            End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                            
                            p_web.FileToSessionQueue(JOBS)
                            p_web.FileToSessionQueue(JOBSE)
                            p_web.FileToSessionQueue(WEBJOB)

                            
                            GetStatus(SUB(p_web.GSV('Default:ExchangeStatusReceivedAtRRC'),1,3),1,'EXC',p_web)
                            
                            p_web.SessionQueueToFile(JOBS)
                            p_web.SessionQueueToFile(JOBSE)
                            p_web.SessionQueueToFile(WEBJOB)
                            
                            IF (Access:JOBS.TryUpdate() = Level:Benign)
                                Access:WEBJOB.TryUpdate()
                                Access:JOBSE.TryUpdate()
                            END ! IF
                            ! Reset Confirmation Screen
                            p_web.SSV('locJobNumber','')
                            p_web.SSV('locIMEINumber','')
                            p_web.SSV('locJobFound',0)
                        ELSE
                            ! Job
                            jobe:Despatched = 'JOB'
                            jobe:HubRepair = 0
                            Access:JOBSE.TryUpdate()
                            
                            job:Workshop = 'YES'    ! L256 - Added to allow the unit to be QA'ed.
                            
                            p_web.FileToSessionQueue(JOBS)
                            p_web.FileToSessionQueue(JOBSE)
                            p_web.FileToSessionQueue(WEBJOB)
                            
                            LocationChange(p_web,p_web.GSV('Default:RRCLocation'))
                            
                            
                            !Reset the Courier back to the Trade Account default.. as per booking
                            Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                            sub:Account_Number  = job:Account_Number
                            If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                                Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                                tra:Account_Number  = sub:Main_Account_Number
                                If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                    If tra:Use_Sub_Accounts = 'YES'
                                        job:Courier = sub:Courier_Outgoing
                                    Else !If tra:Use_Sub_Accounts = 'YES'
                                        job:Courier = tra:Courier_Outgoing
                                    End !If tra:Use_Sub_Accounts = 'YES'
                                End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                            End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                            
                                
                            GetStatus(SUB(p_web.GSV('Default:StatusReceivedAtRRC'),1,3),1,'JOB',p_web)
                            
                            p_web.SessionQueueToFile(JOBS)
                            p_web.SessionQueueToFile(JOBSE)
                            p_web.SessionQueueToFile(WEBJOB)
                            
                            IF (Access:JOBS.TryUpdate() = Level:Benign)
                                IF wob:RRCEngineer <> '' And wob:RRCEngineer <> job:Engineer
                                    job:Engineer = wob:RRCEngineer
                                    Access:JOBS.TryUpdate()

                                    AddToAudit(p_web,job:ref_number,'JOB','ENGINEER ALLOCATED: ' & CLip(job:engineer),Clip(use:Forename) & ' ' & Clip(use:Surname) & ' - ' & Clip(use:Location) & ' - LEVEL ' & Clip(use:SkillLevel))

                                    If Access:JOBSENG.PrimeRecord() = Level:Benign
                                        joe:JobNumber     = job:Ref_Number
                                        joe:UserCode      = job:Engineer
                                        joe:DateAllocated = Today()
                                        joe:TimeAllocated = Clock()
                                        joe:AllocatedBy   = p_web.GSV('BookingUserCode')
                                        access:users.clearkey(use:User_Code_Key)
                                        use:User_Code   = job:Engineer
                                        access:users.fetch(use:User_Code_Key)
                                        joe:Status = 'ENGINEER QA'
                                        joe:StatusDate  = Today()
                                        joe:StatusTime  = Clock()

                                        joe:EngSkillLevel = use:SkillLevel
                                        joe:JobSkillLevel = jobe:SkillLevel

                                        If Access:JOBSENG.TryInsert() = Level:Benign
                                !Insert Successful
                                        Else !If Access:JOBSENG.TryInsert() = Level:Benign
                                !Insert Failed
                                        End !If Access:JOBSENG.TryInsert() = Level:Benign
                                    End !If Access:JOBSENG.PrimeRecord() = Level:Benign
                                End !IF wob:RRCEngineer <> ''

                                
                                
                                Access:WEBJOB.TryUpdate()
                                Access:JOBSE.TryUpdate()
                                
                                Access:JOBACC.Clearkey(jac:Ref_Number_Key)
                                jac:Ref_Number = job:Ref_Number
                                Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
                                Loop ! Begin Loop
                                    If Access:JOBACC.Next()
                                        Break
                                    End ! If Access:JOBACC.Next()
                                    If jac:Ref_Number <> job:Ref_Number
                                        Break
                                    End ! If jac:Ref_Number <> job:Ref_Number
                                    If jac:Attached = 0
                                        packet = CLIP(packet) & | 
                                            '<script>alert("The selected job has accessories that have been retained by the RRC");</script>'
                                        DO SendPacket
                                        Break
                                    End ! If jac:Attached = ''
                                End ! Loop
                                
                                ! Make as processed in browse
                                sbogen:Long1 = 1
                                Access:SBO_GenericFile.TryUpdate()
                                
                                ! Reset Confirmation Screen
                                p_web.SSV('locJobNumber','')
                                p_web.SSV('locIMEINumber','')
                                p_web.SSV('locJobFound',0)
                                
                                
                            END ! IF
                        END ! IF
                    END ! IF
                END ! IF
            END !IF            
            
            unProc# = 0
            Access:SBO_GenericFile.ClearKey(sbogen:Long2Key)
            sbogen:SessionID = p_web.SessionID
            SET(sbogen:Long2Key,sbogen:Long2Key)
            LOOP UNTIL Access:SBO_GenericFile.Next() <> Level:Benign
                IF (sbogen:SessionID <> p_web.SessionID)
                    BREAK
                END ! IF
                IF (sbogen:Long1 = 0)
            ! Unprocesed!
                    unProc# = 1
                    BREAK
                END ! IF
            END ! LOOP
            
            IF (unProc# = 0)
                WaybillComplete()
            END ! IF
            
            
        END ! IF
        
        Relate:JOBS.Close()
        Relate:JOBSE.Close()
        Relate:WEBJOB.Close()
        Relate:SBO_GenericFile.Close()
        Relate:SUBTRACC.Close()
        Relate:TRADEACC.Close()
        Relate:JOBSENG.Close()
        Relate:USERS.Close()
        Relate:JOBACC.Close()
WaybillSundryGeneration     PROCEDURE()
    CODE
        
        IF (p_web.IFExistsValue('wayType'))
            p_web.StoreValue('wayType')
        END ! IF
        
        Relate:WAYBILLS.Open()
        Relate:SBO_GenericFile.Open()
        Relate:WAYSUND.Open()
        Relate:WAYAUDIT.Open()
        
        p_web.SSV('locWaybillNumber',NextWaybillNumber())
        
        IF (p_web.GSV('locWaybillNumber') >  0)
            Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
            way:WayBillNumber = p_web.GSV('locWaybillNumber')
            IF (Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign)
                IF (p_web.GSV('BookingSite') = 'RRC')
                    way:FromAccount = p_web.GSV('BookingAccount')
                ELSE
                    ! I think this will be RRC only, but will include for now
                    way:FromAccount = p_web.GSV('ARC:AccountNumber') ! Head ACcount
                END ! IF
                
                IF (p_web.GSV('locDestination') = p_web.GSV('ARC:AccountNumber'))
                    way:WayBillType = 0
                ELSE
                    way:WayBillType = 1
                END ! IF
                
                way:AccountNumber = p_web.GSV('locDestination')
                way:WaybillID = 300
                way:ToAccount = p_web.GSV('locDestination')
                way:SecurityPackNumber = p_web.GSV('locSecurityPackNumber')
                way:UserNotes = p_web.GSV('locUserNotes')
                way:Courier = p_web.GSV('locTransport')
                way:TheDate = TODAY()
                way:TheTime = CLOCK()
                IF (p_web.GSV('locSendToOtherAddress') = 1)
                    way:WayBillType = 2
                    ! Item being sent to "Other" customer
                    way:OtherCompanyName   = p_web.GSV('locCompanyName')
                    way:OtherAccountNumber =  p_web.GSV('locAccountNumber')
                    way:OtherAddress1      =  p_web.GSV('locAddress1')
                    way:OtherAddress2      =  p_web.GSV('locAddress2')
                    way:OtherAddress3      =  p_web.GSV('locAddress3')
                    way:OtherPostcode      =  p_web.GSV('locPostcode')
                    way:OtherContactName   =  p_web.GSV('locContactName')
                    way:OtherTelephoneNO   =  p_web.GSV('locTelephoneNumber')
                    way:OtherEmailAddress  =  p_web.GSV('locEmailAddress')
                    way:OtherHub           =  p_web.GSV('locHub')
                END ! IF
                IF (Access:WAYBILLS.TryUpdate() = Level:Benign)
                    Access:SBO_GenericFile.ClearKey(sbogen:String1Key)
                    sbogen:SessionID = p_web.SessionID
                    SET(sbogen:String1Key,sbogen:String1Key)
                    LOOP UNTIL Access:SBO_GenericFile.Next() <> Level:Benign
                        IF (sbogen:SessionID <> p_web.SessionID)
                            BREAK
                        END ! IF
                        IF (Access:WAYSUND.PrimeRecord() = Level:Benign)
                            was:WAYBILLSRecordNumber = way:RecordNumber
                            was:Description = sbogen:String1
                            was:Quantity = sbogen:Long1
                            IF (Access:WAYSUND.TryInsert())
                                Access:WAYSUND.CancelAutoInc()
                            END !IF
                        END ! IF
                        
                    END ! LOOp
                    If Access:WAYAUDIT.PrimeRecord() = Level:Benign
                        waa:WAYBILLSRecordNumber = way:RecordNumber
                        waa:UserCode = p_web.GSV('BookingUserCode')
                        waa:Action   = 'WAYBILL CREATED'
                        waa:TheDate  = Today()
                        waa:TheTime  = Clock()
                        If Access:WAYAUDIT.TryInsert() = Level:Benign
                                ! Insert
                                ! Waybill created (DBH: 04/09/2006)
                            !WayBillDespatch(way:FromAccount, 'TRA', way:ToAccount, 'TRA', way:WaybillNumber, way:Courier)
                            !Post(Event:CloseWindow)
                        Else ! If Access:WAYAUDIT.TryInsert() = Level:Benign
                            Access:WAYAUDIT.CancelAutoInc()
                        End ! If Access:WAYAUDIT.TryInsert() = Level:Benign
                    End ! If Access.WAYAUDIT.PrimeRecord() = Level:Benign
                    
                    
                    p_web.SSV('Waybill:ConsignmentNumber',way:WayBillNumber)
                    p_web.SSV('Waybill:Courier',way:Courier)
                    p_web.SSV('Waybill:FromAccount',way:FromAccount)
                    p_web.SSV('Waybill:ToAccount',way:ToAccount)
                    p_web.SSV('Waybill:FromType','TRA')
                    p_web.SSV('Waybill:ToType','TRA')

                END ! IF
            END ! IF
        END ! IF
        
        
        Relate:WAYSUND.Close()
        Relate:SBO_GenericFile.Close()
        Relate:WAYBILLS.Close()
        Relate:WAYAUDIT.Close()
        
        IF (p_web.GSV('locWaybillNumber') = 0)
            packet = CLIP(packet) & |
                '<script>alert("An error occurred create the waybill. Please try again.");window.location.href="' & p_web.GSV('ReturnURL') & '";</script>'
            DO redirect
        END ! IF
! WIPAuditExport
WIPAuditExport      PROCEDURE()
recordCount1            LONG(0)
recordCount2            LONG(0)
recordCount3            LONG(0)
qJobs                   QUEUE,PRE(qJobs)
JobNumber                   LONG
IMEI                        STRING(20)
Manufacturer                STRING(30)
ModelNumber                 STRING(30)
StatusType                  STRING(30)
                        END
firstStatus             LONG(0)
lastStatus1             STRING(30)
lastStatus2             STRING(30)
StatusUsedQ             Queue,PRE(SUQ)
Status                      STRING(30)
                        END
StatusDays              Long(0)
StartingRef             Long(0)
    CODE
	
        ProgressBarWeb.Init(200)
		
        IF (p_web.GSV('locWIPSerialsScanned') = 1)
            locExportFile = CLIP(p_web.site.webFolderPath) & '\temp\SerialsScanned_' & p_web.GSV('wim:Audit_Number') & ' ' & Format(Today(),@d11) & ' ' & FORMAT(CLOCK(),@t05) & '.csv'
            p_web.SSV('ExportFileName1','Serials Scanned Export')
            REMOVE(locExportFile)
		
            CREATE(ExportFile)
            OPEN(ExportFile)            
        END ! IF
        IF (p_web.GSV('locWIPSerialsScannedLocation') = 1)
            locExportFile2 = CLIP(p_web.site.webFolderPath) & '\temp\SerialsScannedLocation_' & p_web.GSV('wim:Audit_Number') & ' ' & Format(Today(),@d11) & ' ' & FORMAT(CLOCK(),@t05) & '.csv'
            p_web.SSV('ExportFileName2','Serials Scanned Not In Location Export')
            REMOVE(locExportFile2)
		
            CREATE(ExportFile2)
            OPEN(ExportFile2)
        END ! IF
        IF (p_web.GSV('locWIPSerialsNotScanned') = 1)
            locExportFile3 = CLIP(p_web.site.webFolderPath) & '\temp\SerialsNotScanned' & p_web.GSV('wim:Audit_Number') & ' ' & Format(Today(),@d11) & ' ' & FORMAT(CLOCK(),@t05) & '.csv'
            p_web.SSV('ExportFileName3','Serials Not Scanned Export')
            REMOVE(locExportFile3)
		
            CREATE(ExportFile3)
            OPEN(ExportFile3)            
        END ! IF
        
		
        Relate:WIPAUI.Open()
        Relate:JOBSE.Open()
        Relate:JOBS.Open()
        Relate:EXCHANGE.Open()
        Relate:WEBJOB.Open()
        Relate:STATUS.Open()
        
        IF (p_web.GSV('locWIPSerialsScanned') = 1 OR p_web.GSV('locWIPSerialsScannedLocation') = 1)
		
            IF (p_web.GSV('locWIPSerialsScannedLocation') = 1)
                    
                expfil2:Line = 'WIP Audit,' & p_web.GSV('BookingAccount') & ' ' & p_web.GSV('BookingName')
                ADD(ExportFile2)
                expfil2:Line = 'Date Completed,' & FORMAT(p_web.GSV('wim:Date_Completed'),@d06) & ',' & FORMAT(p_web.GSV('wim:Time_Completed'),@t01)
                ADD(ExportFile2)
                expfil2:Line = 'WIP Audit Number,' & p_web.GSV('wim:Audit_Number')
                ADD(ExportFile2)
            END 
            IF (p_web.GSV('locWIPSerialsScanned') = 1)
                expfil:Line = 'WIP Audit,' & p_web.GSV('BookingAccount') & ' ' & p_web.GSV('BookingName')
                ADD(ExportFile)
                expfil:Line = 'Date Completed,' & FORMAT(p_web.GSV('wim:Date_Completed'),@d06) & ',' & FORMAT(p_web.GSV('wim:Time_Completed'),@t01)
                ADD(ExportFile)
                expfil:Line = 'WIP Audit Number,' & p_web.GSV('wim:Audit_Number')
                ADD(ExportFile)    
            END!  IF
            
            lastStatus1 = ''
            lastStatus2 = ''
            Access:WIPAUI.ClearKey(wia:AuditStatusRefNoKey)
            wia:Audit_Number = p_web.GSV('wim:Audit_Number')
            wia:Status = ''
            SET(wia:AuditStatusRefNoKey,wia:AuditStatusRefNoKey)
            LOOP UNTIL Access:WIPAUI.Next() <> Level:Benign
                IF (wia:Audit_Number <> p_web.GSV('wim:Audit_Number'))
                    BREAK
                END ! IF
			
                ProgressBarWeb.Update('Exporting...')
                
                Access:JOBS.ClearKey(job:Ref_Number_Key)
                job:Ref_Number = wia:Ref_Number
                IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
                    CYCLE
                END ! IF
			
                Access:JOBSE.ClearKey(jobe:RefNumberKey)
                jobe:RefNumber = wia:Ref_Number
                IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
				
                ELSE ! IF
                    CYCLE
                END ! IF
            
                IF (p_web.GSV('locWIPSerialsScannedLocation') = 1)
                    LOOP 1 TIMES
                        IF (p_web.GSV('BookingSite') = 'RRC')
                            IF (jobe:HubRepair <> 1)
                                BREAK
                            END ! IF
                        ELSE ! IF
                            IF (jobe:HubRepair = 1)
                                BREAK
                            END ! IF
                        END ! IF
                        
                        IF (lastStatus2 <> wia:Status)
                            expfil2:Line = wia:Status
                            ADD(ExportFile2)
                            lastStatus2 = wia:Status
                        END ! IF
                    
                        expfil2:Line = '"' & wia:Ref_Number
			
                        IF (wia:IsExchange)
                            Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                            xch:Ref_Number = job:Exchange_Unit_Number
                            IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                                expfil2:Line = CLIP(expfil2:Line) & '","' & xch:ESN
                                expfil2:Line = CLIP(expfil2:Line) & '","' & xch:Manufacturer
                                expfil2:Line = CLIP(expfil2:Line) & '","' & xch:Model_Number
                            ELSE ! IF
                                BREAK
                            END ! IF
                        ELSE  ! IF
                            expfil2:Line = CLIP(expfil2:Line) & '","' & job:ESN
                            expfil2:Line = CLIP(expfil2:Line) & '","' & job:Manufacturer
                            expfil2:Line = CLIP(expfil2:Line) & '","' & job:Model_Number
                        END ! IF
			
                        expfil2:Line = CLIP(expfil2:Line) & '","' & wia:Status
                        expfil2:Line = CLIP(expfil2:Line) & '"'
                        ADD(ExportFile2) 
                        recordCount2 += 1
                    END ! LOOP
                    
                END
                IF (p_web.GSV('locWIPSerialsScanned') = 1)
                    LOOP 1 TIMES
                        
                        IF (lastStatus1 <> wia:Status)
                            expfil:Line = wia:Status
                            ADD(ExportFile)
                            lastStatus1 = wia:Status
                        END ! IF
                        
                        expfil:Line = '"' & wia:Ref_Number
			
                        IF (wia:IsExchange)
                            Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                            xch:Ref_Number = job:Exchange_Unit_Number
                            IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                                expfil:Line = CLIP(expfil:Line) & '","`' & xch:ESN
                                expfil:Line = CLIP(expfil:Line) & '","' & xch:Manufacturer
                                expfil:Line = CLIP(expfil:Line) & '","' & xch:Model_Number
                            ELSE ! IF
                                BREAK
                            END ! IF
                        ELSE  ! IF
                            expfil:Line = CLIP(expfil:Line) & '","`' & job:ESN
                            expfil:Line = CLIP(expfil:Line) & '","' & job:Manufacturer
                            expfil:Line = CLIP(expfil:Line) & '","' & job:Model_Number
                        END ! IF
			
                        expfil:Line = CLIP(expfil:Line) & '","' & wia:Status
                        expfil:Line = CLIP(expfil:Line) & '"'
                        ADD(ExportFile)
                        recordCount1 += 1
                    END ! LOOP
                    
                END ! IF

			
            END ! LOOP
        END ! IF
        
        IF (p_web.GSV('locWIPSerialsNotScanned') = 1)

            !TB12740 - need a starting point of so many days ago - J - 24/07/14
            StatusDays = getini(p_web.GSV('BookingSiteLocation'),'StatusDays',0,clip(path())&'\WIPAudit.ini')
            If StatusDays = 0 then
                StartingRef = 0
            ELSE
                Access:jobs.clearkey(job:Date_Booked_Key)
                job:date_booked =  today() - StatusDays
                set(job:Date_Booked_Key,job:Date_Booked_Key)
                if Access:jobs.previous() then             
                    !No jobs in system?
                    StartingRef = 0
                ELSE
                    StartingRef = job:Ref_Number
                END
            END
            !TB12740 - end of find a starting refnumber               
            
            
            
            Access:WIPAUI.ClearKey(wia:Audit_Number_Key)
            wia:Audit_Number = p_web.GSV('wim:Audit_Number')
            SET(wia:Audit_Number_Key,wia:Audit_Number_Key)
            LOOP UNTIL Access:WIPAUI.Next() <> Level:Benign
                IF (wia:Audit_Number<> p_web.GSV('wim:Audit_Number'))
                    BREAK
                END ! IF
    
                ProgressBarWeb.Update('Working...')
                
                qjobs.JobNumber = wia:Ref_Number
                GET(qJobs,qJobs.JobNumber)
                IF (ERROR())
                    qJobs.JobNumber = wia:Ref_Number
                    ADD(qJobs)
                END ! IF
                
                !TB12740 - J - 24/07/14 - Will need a list of status in audit so can include status even if excluded
                StatusUsedQ.SUQ:Status = wia:Status
                get(StatusUsedQ,SUQ:Status)
                if error() then
                    !not found add it
                    StatusUsedQ.SUQ:Status = wia:Status
                    add(StatusUsedQ)
                END !if error()           
                !END TB12740                 
                
            END ! LOOP

	
            ProgressBarWeb.Init(RECORDS(STATUS))
            
            expfil3:Line = 'WIP Audit,' & p_web.GSV('BookingAccount') & ' ' & p_web.GSV('BookingName')
            ADD(ExportFile3)
            expfil3:Line = 'Date Completed,' & FORMAT(p_web.GSV('wim:Date_Completed'),@d06) & ',' & FORMAT(p_web.GSV('wim:Time_Completed'),@t01)
            ADD(ExportFile3)
            expfil3:Line = 'WIP Audit Number,' & p_web.GSV('wim:Audit_Number')
            ADD(ExportFile3)
            
            
            
            Access:STATUS.ClearKey(sts:Ref_Number_Only_Key)
            sts:Ref_Number = 0
            SET(sts:Ref_Number_Only_Key,sts:Ref_Number_Only_Key)
            LOOP UNTIL Access:STATUS.Next() <> Level:Benign
                IF (sts:Loan = 'YES')
                    CYCLE
                END ! IF
                

                !TB12740 - J - 24/07/14
                !for the status to be used in this report it must NOT be in the excluded list
                Access:WIPEXC.clearkey(wix:StatusTypeKey)
                wix:Location = p_web.GSV('BookingSiteLocation') 
                wix:Status = sts:Status
                if NOT access:WIPEXC.fetch(wix:StatusTypeKey)
                    !found in exclusions don't use it - Unless a job in that status has been scanned in this audit.
                    SUQ:Status = wix:Status
                    get(StatusUsedQ,SUQ:Status)
                    if error() then cycle.      !not found in the used list either
                END !if found in the exclusions 
                !END TB12740 - J - 23/07/14                                                        
		
                ProgressBarWeb.Update('Exporting...')
                
                firstStatus = 1
                
                IF (p_web.GSV('BookingSite') = 'RRC')
                    IF (sts:Exchange = 'YES')
                        Access:WEBJOB.ClearKey(wob:HeadExchangeStatus)     !note this key has ref_number as the final part - can use this to do the date()
                        wob:HeadAccountNumber = p_web.GSV('BookingAccount')
                        wob:Exchange_Status    = sts:Status
                        wob:RefNumber         = StartingRef       !TB12740 - J - 23/07/14 - added to limit search - this is part of existing key                        
                        SET(wob:HeadExchangeStatus, wob:HeadExchangeStatus)				
                    ELSE ! IF
                        Access:WEBJOB.ClearKey(wob:HeadCurrentStatusKey)   !note this key has ref_number as the final part - can use this to do the date()
                        wob:HeadAccountNumber = p_web.GSV('BookingAccount')
                        wob:Current_Status    = sts:Status
                        wob:RefNumber         = StartingRef       !TB12740 - J - 23/07/14 - added to limit search - this is part of existing key                        
                        SET(wob:HeadCurrentStatusKey, wob:HeadCurrentStatusKey)			
                    END ! IF
                    LOOP UNTIL Access:WEBJOB.Next() <> Level:Benign
                        IF (wob:HeadAccountNumber <> p_web.GSV('BookingAccount'))
                            BREAK
                        END ! IF
                        IF (sts:Exchange = 'YES')
                            IF (wob:Exchange_Status <> sts:Status)
                                BREAK
                            END ! IF
                        ELSE ! IF
                            IF (wob:Current_Status <> sts:Status)
                                BREAK
                            END ! IF
                        END ! IF
                        Access:JOBSE.ClearKey(jobe:RefNumberKey)
                        jobe:RefNumber = wob:RefNumber
                        IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                            IF (jobe:HubRepair = 1)
                                CYCLE
                            END ! IF
                        ELSE ! IF
                        END ! IF
				
                        Access:JOBS.ClearKey(job:Ref_Number_Key)
                        job:Ref_Number = wob:RefNumber
                        IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                        ELSE ! IF
                            CYCLE
                        END ! IF
				
                        qJobs.JobNumber = job:Ref_Number
                        GET(qJobs,qJobs.JobNumber)
                        IF NOT ERROR()
                            CYCLE
                        END ! IF
                        
                        IF (firstStatus = 1)
                            expfil3:Line = sts:Status
                            ADD(ExportFile3)
                            firstStatus = 0
                        END ! 
                        
                        IF (sts:Exchange = 'YES')
                            Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                            xch:Ref_Number = job:Exchange_Unit_Number
                            IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                                expfil3:Line = '"' & job:Ref_Number
                                expfil3:Line = CLIP(expfil3:Line) & '","`' & xch:ESN
                                expfil3:Line = CLIP(expfil3:Line) & '","' & xch:Manufacturer
                                expfil3:Line = CLIP(expfil3:Line) & '","' & xch:Model_Number
                            ELSE ! IF
                                CYCLE
                            END ! IF
                        ELSE ! IF
                            IF (job:ESN = '')
                                CYCLE
                            END ! IF
                            expfil3:Line = '"' & job:Ref_Number
                            expfil3:Line = CLIP(expfil3:Line) & '","`' & job:ESN
                            expfil3:Line = CLIP(expfil3:Line) & '","' & job:Manufacturer
                            expfil3:Line = CLIP(expfil3:Line) & '","' & job:Model_Number			
                        END ! IF
				
                        expfil3:Line = CLIP(expfil3:Line) & '","' & 'NOT SCANNED'
                        expfil3:Line = CLIP(expfil3:Line) & '"'
                        ADD(ExportFile3)
                        recordCount3 += 1
                    END ! LOOP
                ELSE ! IF
                    ! Not designed for FAT CLIENT
                END ! IF
            END ! LOOP
        END ! IF
        
		
        Relate:JOBSE.Close()
        Relate:WIPAUI.Close()
        Relate:JOBS.Close()
        Relate:EXCHANGE.Close()
        Relate:WEBJOB.Close()
        Relate:STATUS.Close()
        
        IF (p_web.GSV('locWIPSerialsScanned') = 1)
            CLOSE(ExportFile)   
            IF (recordCount1 = 0)
                packet = CLIP(packet) & |
                    '<script>alert("Serials Scanned Export: No Matching Records Found.");</script>'
                
            ELSE ! 
                p_web.SSV('locExportFile','/temp/' & BHGetFileFromPath(locExportFile))	
            END ! IF
        END ! IF
        IF (p_web.GSV('locWIPSerialsScannedLocation') = 1)
            CLOSE(ExportFile2)
            IF (recordCount2 = 0)
                packet = CLIP(packet) & |
                    '<script>alert("Serials Scanned Not In Location Export: No Matching Records Found.");</script>'
            ELSE !
                p_web.SSV('locExportFile2','/temp/' & BHGetFileFromPath(locExportFile2))	
            END ! IF
            
        END ! IF
        IF (p_web.GSV('locWIPSerialsNotScanned') = 1)
            CLOSE(ExportFile3)  
            IF (recordCount3 = 0)
                packet = CLIP(packet) & |
                    '<script>alert("Serials Not Scanned Export: No Matching Records Found.");</script>'
            ELSE !            
                p_web.SSV('locExportFile3','/temp/' & BHGetFileFromPath(locExportFile3))	
            END ! IF
            
        END ! IF		
		
        IF (recordCount1 = 0 AND recordCount2 = 0 AND recordCount3 = 0)
            REMOVE(ExportFile)
            DO ReturnFail
        END ! IF
		
! Define ProgressBarWeb
ProgressBarWeb.Init PROCEDURE(LONG pTotalRecords,LONG pSkipValue=100)
    CODE
        SELF.TotalRecords = pTotalRecords
        SELF.RecordCount = 0
        SELF.Skip = 0
        SELF.SkipValue = pSkipValue
        CreateScript(packet,'document.getElementById("allfinished").display.style:"none"')
        DO SendPacket
!!        IF (SELF.Initialized = 0)
!            packet = CLIP(packet) & '<script>drawProgressBar("' & p_web.GSV('ReturnURL') & '");</script>'
!            DO SendPacket
!            SELF.Initialized = 1
!!        END ! IF
        
ProgressBarWeb.Update   PROCEDURE(<STRING pDisplayText>,LONG pForceUpdate=0)  
    CODE
        SELF.RecordCount += 1
        IF (pDisplayText <> '')
            SELF.DisplayText = pDisplayText
        ELSE ! IF
            SELF.DisplayText = ''
        END ! IF
        
        
        IF (SELF.Skip > SELF.SkipValue OR SELF.Skip = 0)
            SELF.Skip = 0

            SELF.PercentageValue = INT((SELF.RecordCount / SELF.TotalRecords) * 100)
        
            IF (SELF.PercentageValue => 100)
                SELF.RecordCount = 0
                SELF.PercentageValue = 0
            END ! IF

            SELF.UpdateProgressBar()
        END ! IF
        IF (pForceUpdate)
            SELF.UpdateProgressBar()
        END !I F
        
        SELF.Skip += 1
        
        YIELD()
ProgressBarWeb.UpdateProgressBar    PROCEDURE()
    CODE
        packet = CLIP(packet) & '<script>updateProgressBar("' & CLIP(SELF.DisplayText) & '",' & SELF.PercentageValue & ');</script>'
        DO SendPacket 
! Subclassing SimpleExportClass
expfil.Init         PROCEDURE(STRING pDescription,<STRING pFilename>)!,BYTE
retValue        BYTE(0)
	CODE
        p_web.SSV('ExportFileName1',pDescription)
        
        IF (pFilename <> '')
            SELF.ExportFilename = CLIP(p_web.site.webFolderPath) & '\temp\' & pFilename & ' ' & Format(Today(),@d012) & Format(Clock(),@t02) & ' (' & kVersionNumber & '.' & kWebserverCurrentVersion & ').csv'
        ELSE
            SELF.ExportFilename = CLIP(p_web.site.webFolderPath) & '\temp\' & pDescription & ' ' & Format(Today(),@d012) & Format(Clock(),@t02) & ' (' & kVersionNumber & '.' & kWebserverCurrentVersion & ').csv'
        END ! IF
        
		
		retValue = PARENT.Init(SELF.ExportFilename,',',1)
		
        RETURN retValue
        
expfil.Kill            PROCEDURE(<BYTE pDeleteFile>)
    CODE
        
        PARENT.Kill()
        
        IF (pDeleteFile)
            REMOVE(SELF.ExportFilename)
        ELSE
            p_web.SSV('locExportFile','/temp/' & BHGetFileFromPath(SELF.ExportFilename))
        END ! IF
expfil2.Init         PROCEDURE(STRING pDescription,<STRING pFilename>)!,BYTE
retValue                BYTE(0)
    CODE
        p_web.SSV('ExportFileName2',pDescription)
        
        IF (pFilename <> '')
            SELF.ExportFilename = CLIP(p_web.site.webFolderPath) & '\temp\' & pFilename & ' ' & Format(Today(),@d012) & Format(Clock(),@t02) & ' (' & kVersionNumber & '.' & kWebserverCurrentVersion & ').csv'
        ELSE
            SELF.ExportFilename = CLIP(p_web.site.webFolderPath) & '\temp\' & pDescription & ' ' & Format(Today(),@d012) & Format(Clock(),@t02) & ' (' & kVersionNumber & '.' & kWebserverCurrentVersion & ').csv'
        END ! IF
        
        retValue = PARENT.Init(SELF.ExportFilename,',',1)
		
        RETURN retValue
        
expfil2.Kill         PROCEDURE(<BYTE pDeleteFile>)
    CODE
        
        PARENT.Kill()
        
        IF (pDeleteFile)
            REMOVE(SELF.ExportFilename)
        ELSE
            p_web.SSV('locExportFile2','/temp/' & BHGetFileFromPath(SELF.ExportFilename))
        END ! IF
expfil3.Init         PROCEDURE(STRING pDescription,<STRING pFilename>)!,BYTE
retValue                BYTE(0)
    CODE
        p_web.SSV('ExportFileName3',pDescription)
        
        IF (pFilename <> '')
            SELF.ExportFilename = CLIP(p_web.site.webFolderPath) & '\temp\' & pFilename & ' ' & Format(Today(),@d012) & Format(Clock(),@t02) & ' (' & kVersionNumber & '.' & kWebserverCurrentVersion & ').csv'
        ELSE
            SELF.ExportFilename = CLIP(p_web.site.webFolderPath) & '\temp\' & pDescription & ' ' & Format(Today(),@d012) & Format(Clock(),@t02) & ' (' & kVersionNumber & '.' & kWebserverCurrentVersion & ').csv'
        END ! IF
        
        retValue = PARENT.Init(SELF.ExportFilename,',',1)
		
        RETURN retValue
        
expfil3.Kill         PROCEDURE(<BYTE pDeleteFile>)
    CODE
        
        PARENT.Kill()
        
        IF (pDeleteFile)
            REMOVE(SELF.ExportFilename)
        ELSE
            p_web.SSV('locExportFile3','/temp/' & BHGetFileFromPath(SELF.ExportFilename))
        END ! IF
expfil4.Init        PROCEDURE(STRING pDescription,<STRING pFilename>)!,BYTE
retValue                BYTE(0)
    CODE
        p_web.SSV('ExportFileName4',pDescription)
        
        IF (pFilename <> '')
            SELF.ExportFilename = CLIP(p_web.site.webFolderPath) & '\temp\' & pFilename & ' ' & Format(Today(),@d012) & Format(Clock(),@t02) & ' (' & kVersionNumber & '.' & kWebserverCurrentVersion & ').csv'
        ELSE
            SELF.ExportFilename = CLIP(p_web.site.webFolderPath) & '\temp\' & pDescription & ' ' & Format(Today(),@d012) & Format(Clock(),@t02) & ' (' & kVersionNumber & '.' & kWebserverCurrentVersion & ').csv'
        END ! IF
        
        retValue = PARENT.Init(SELF.ExportFilename,',',1)
		
        RETURN retValue
        
expfil4.Kill        PROCEDURE(<BYTE pDeleteFile>)
    CODE
        
        PARENT.Kill()
        
        IF (pDeleteFile)
            REMOVE(SELF.ExportFilename)
        ELSE
            p_web.SSV('locExportFile4','/temp/' & BHGetFileFromPath(SELF.ExportFilename))
        END ! IF        
CallReport          PROCEDURE(STRING pReportName)
    CODE
        p_web.SSV('RedirectURL',pReportName)
        CreateScript(packet,'document.getElementById("progressframe").style.display="none";' & | 
            'document.getElementById("allfinished").style.display="block";' & | 
            'document.getElementById("allfinished_title").innerHTML="Creating Report..."')
        DO SendPacket

