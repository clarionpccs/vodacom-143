

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER204.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER059.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER103.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER155.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER205.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER206.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER208.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER296.INC'),ONCE        !Req'd for module callout resolution
                     END


DisplayBrowsePayments PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locOutstanding       REAL                                  !
FilesOpened     Long
WEBJOB::State  USHORT
INVOICE::State  USHORT
JOBSE2::State  USHORT
TRADEACC::State  USHORT
SUBTRACC::State  USHORT
JOBSE::State  USHORT
JOBS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
                    MAP
GetOutstanding  PROCEDURE(),REAL
showHideCreateInvoice       PROCEDURE(),BYTE
    END
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('DisplayBrowsePayments')
  loc:formname = 'DisplayBrowsePayments_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('DisplayBrowsePayments','')
    p_web._DivHeader('DisplayBrowsePayments',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferDisplayBrowsePayments',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDisplayBrowsePayments',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDisplayBrowsePayments',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_DisplayBrowsePayments',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDisplayBrowsePayments',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_DisplayBrowsePayments',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
DeleteSessionvalues  ROUTINE
    p_web.DeleteSessionValue('IgnoreMessage')
    p_web.DeleteSessionValue('Job:ViewOnly')
    p_web.DeleteSessionValue('DisplayBrowsePaymentsReturnURL')
OpenFiles  ROUTINE
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(INVOICE)
  p_web._OpenFile(JOBSE2)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(JOBS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(INVOICE)
  p_Web._CloseFile(JOBSE2)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('DisplayBrowsePayments_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('locOutstanding')
    p_web.SetPicture('locOutstanding','@n_14.2')
  End
  p_web.SetSessionPicture('locOutstanding','@n_14.2')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locOutstanding',locOutstanding)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locOutstanding')
    locOutstanding = p_web.dformat(clip(p_web.GetValue('locOutstanding')),'@n_14.2')
    p_web.SetSessionValue('locOutstanding',locOutstanding)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('DisplayBrowsePayments_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  ! Return URL
  IF (p_web.IfExistsValue('DisplayBrowsePaymentsReturnURL'))
      p_web.StoreValue('DisplayBrowsePaymentsReturnURL')
  END
  ! Open Files
  ! Assume JOBS is in a session queue
  
  Access:JOBSE.ClearKey(jobe:RefNumberKey)
  jobe:RefNumber = p_web.GSV('job:Ref_Number')
  IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
      p_web.FileToSessionQueue(JOBSE)
  END
  
  Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
  jobe2:RefNumber = p_web.GSV('job:Ref_Number')
  IF (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
      p_web.FileToSessionQueue(JOBSE2)
  END
  
  Access:WEBJOB.ClearKey(wob:RefNumberKey)
  wob:RefNumber = p_web.GSV('job:Ref_Number')
  IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
      p_web.FileToSessionQueue(WEBJOB)
  END
  
  
  IF (p_web.IfExistsValue('IgnoreMessage'))
      p_web.StoreValue('IgnoreMessage')
  END
  
  ! Force the cost screen to be view only 
  p_web.SSV('Job:ViewOnly',1)
  
  p_web.SSV('locOutStanding',FORMAT(GetOutstanding(),@n_14.2))
  p_web.site.SaveButton.TextValue = 'Close'
  
  
    
  IF (p_web.GSV('job:Date_Completed') = 0)
      IF (p_web.GSV('IgnoreMessage') <> 1)
            p_web.SSV('Message:Text','Warning! This job has not been completed!\n\nClick ''OK'' To Continue.')
            p_web.SSV('Message:PassURL','DisplayBrowsePayments?IgnoreMessage=1')
            p_web.SSV('Message:FailURL',p_web.GSV('DisplayBrowsePaymentsReturnURL'))
            MessageQuestion(p_web)
            Exit
      END
  END
  
  p_web.SSV('Hide:CreateCreditNoteButton',1)
  
      
  IF (p_web.GSV('BookingSite') = 'RRC')
      ! Only credit if job has been invoiced
      IF (p_web.GSV('job:Invoice_Number') = 0)
      ELSE
          Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
          inv:Invoice_Number = p_web.GSV('job:Invoice_Number')
          IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign)
              IF (inv:RRCInvoiceDate = 0)
              ELSE
                  creditAllowed# = 1
                  Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
                  sub:Account_Number = p_web.GSV('job:Account_Number')
                  IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
                      IF (sub:Generic_Account <> 1)
                          Access:TRADEACC.clearkey(tra:Account_Number_Key)
                          tra:Account_Number = p_web.GSV('BookingAccount')
                          IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                              IF (tra:CompanyOwned = 1)
                                  creditAllowed# = 0
                              END
                          END
                      ELSE
                          creditAllowed# = 0
                      END
          
                  END
      
                  IF (creditAllowed# = 1)
                      Access:TRADEACC.ClearKey(tra:Account_Number_Key)
                      tra:Account_Number = p_web.GSV('BookingAccount')
                      IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                          IF (tra:AllowCreditNotes)
                              p_web.SSV('Hide:CreateCreditNoteButton',0)
                          END
                      END
                  END
                  
              END
              
          END
      END
        
          
  ELSE ! IF (p_web.GSV('BookingSite') = 'RRC')
  END ! IF (p_web.GSV('BookingSite') = 'RRC')
  
      
  
  p_web.SSV('Hide:IssueRefundButton',1)
  IF (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'ISSUE REFUND') = 0)
      p_web.SSV('Hide:IssueRefundButton',0)
  END
  
  
  p_web.SSV('Hide:CreateInvoiceButton',showHideCreateInvoice())
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locOutstanding = p_web.RestoreValue('locOutstanding')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('DisplayBrowsePaymentsReturnURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('DisplayBrowsePayments_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('DisplayBrowsePayments_ChainTo')
    loc:formaction = p_web.GetSessionValue('DisplayBrowsePayments_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('DisplayBrowsePaymentsReturnURL')
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="DisplayBrowsePayments" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="DisplayBrowsePayments" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="DisplayBrowsePayments" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Amend Payments') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Amend Payments',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_DisplayBrowsePayments">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_DisplayBrowsePayments" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_DisplayBrowsePayments')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Browse Payments') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Actions') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_DisplayBrowsePayments')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_DisplayBrowsePayments'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('DisplayBrowsePayments_BrowsePayments_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
    Else
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('DisplayBrowsePayments_BrowsePayments_embedded_div')&''');'
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_DisplayBrowsePayments')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Browse Payments') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_DisplayBrowsePayments_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Payments')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Payments')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Payments')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Payments')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::BrowsePayments
      do Comment::BrowsePayments
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locOutstanding
      do Value::locOutstanding
      do Comment::locOutstanding
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Actions') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_DisplayBrowsePayments_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonViewCosts
      do Comment::buttonViewCosts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonIssueRefund
      do Comment::buttonIssueRefund
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonCreateCreditNote
      do Comment::buttonCreateCreditNote
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonCreateInvoice
      do Comment::buttonCreateInvoice
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::BrowsePayments  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('BrowsePayments',p_web.GetValue('NewValue'))
    do Value::BrowsePayments
  Else
    p_web.StoreValue('jpt:Record_Number')
  End

Value::BrowsePayments  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowsePayments --
  p_web.SetValue('BrowsePayments:NoForm',1)
  p_web.SetValue('BrowsePayments:FormName',loc:formname)
  p_web.SetValue('BrowsePayments:parentIs','Form')
  p_web.SetValue('_parentProc','DisplayBrowsePayments')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('DisplayBrowsePayments_BrowsePayments_embedded_div')&'"><!-- Net:BrowsePayments --></div><13,10>'
    p_web._DivHeader('DisplayBrowsePayments_' & lower('BrowsePayments') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('DisplayBrowsePayments_' & lower('BrowsePayments') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowsePayments --><13,10>'
  end
  do SendPacket

Comment::BrowsePayments  Routine
    loc:comment = ''
  p_web._DivHeader('DisplayBrowsePayments_' & p_web._nocolon('BrowsePayments') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locOutstanding  Routine
  p_web._DivHeader('DisplayBrowsePayments_' & p_web._nocolon('locOutstanding') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Outstanding:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locOutstanding  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locOutstanding',p_web.GetValue('NewValue'))
    locOutstanding = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locOutstanding
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locOutstanding',p_web.dFormat(p_web.GetValue('Value'),'@n_14.2'))
    locOutstanding = p_web.Dformat(p_web.GetValue('Value'),'@n_14.2') !
  End

Value::locOutstanding  Routine
  p_web._DivHeader('DisplayBrowsePayments_' & p_web._nocolon('locOutstanding') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locOutstanding
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(format(p_web.GetSessionValue('locOutstanding'),'@n_14.2')) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locOutstanding  Routine
    loc:comment = ''
  p_web._DivHeader('DisplayBrowsePayments_' & p_web._nocolon('locOutstanding') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonViewCosts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonViewCosts',p_web.GetValue('NewValue'))
    do Value::buttonViewCosts
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonViewCosts  Routine
  p_web._DivHeader('DisplayBrowsePayments_' & p_web._nocolon('buttonViewCosts') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ViewCosts','Job Costs','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('ViewCosts?ViewCostsReturnURL=DisplayBrowsePayments')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/money.png',,,,)

  do SendPacket
  p_web._DivFooter()

Comment::buttonViewCosts  Routine
    loc:comment = ''
  p_web._DivHeader('DisplayBrowsePayments_' & p_web._nocolon('buttonViewCosts') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonIssueRefund  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonIssueRefund',p_web.GetValue('NewValue'))
    do Value::buttonIssueRefund
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonIssueRefund  Routine
  p_web._DivHeader('DisplayBrowsePayments_' & p_web._nocolon('buttonIssueRefund') & '_value',Choose(p_web.GSV('Hide:IssueRefundButton') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:IssueRefundButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','IssueRefund','Issue Refund','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormPayments?Insert_btn=Insert&PassedType=REFUND')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/money_delete.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::buttonIssueRefund  Routine
    loc:comment = ''
  p_web._DivHeader('DisplayBrowsePayments_' & p_web._nocolon('buttonIssueRefund') & '_comment',Choose(p_web.GSV('Hide:IssueRefundButton') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:IssueRefundButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonCreateCreditNote  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonCreateCreditNote',p_web.GetValue('NewValue'))
    do Value::buttonCreateCreditNote
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonCreateCreditNote  Routine
  p_web._DivHeader('DisplayBrowsePayments_' & p_web._nocolon('buttonCreateCreditNote') & '_value',Choose(p_web.GSV('Hide:CreateCreditNoteButton') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CreateCreditNoteButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','CreateCreditNote','Create Credit Note','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('CreateCreditNote')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/money_add.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::buttonCreateCreditNote  Routine
    loc:comment = ''
  p_web._DivHeader('DisplayBrowsePayments_' & p_web._nocolon('buttonCreateCreditNote') & '_comment',Choose(p_web.GSV('Hide:CreateCreditNoteButton') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:CreateCreditNoteButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonCreateInvoice  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonCreateInvoice',p_web.GetValue('NewValue'))
    do Value::buttonCreateInvoice
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonCreateInvoice  Routine
  p_web._DivHeader('DisplayBrowsePayments_' & p_web._nocolon('buttonCreateInvoice') & '_value',Choose(p_web.GSV('Hide:CreateInvoiceButton') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CreateInvoiceButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','CreateInvoice',p_web.GSV('URL:CreateInvoiceText'),'button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip(p_web.GSV('URL:CreateInvoice'))) & ''','''&clip(p_web.GSV('URL:CreateInvoiceTarget'))&''')',loc:javascript,0,'images/money.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::buttonCreateInvoice  Routine
    loc:comment = ''
  p_web._DivHeader('DisplayBrowsePayments_' & p_web._nocolon('buttonCreateInvoice') & '_comment',Choose(p_web.GSV('Hide:CreateInvoiceButton') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:CreateInvoiceButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('DisplayBrowsePayments_form:ready_',1)
  p_web.SetSessionValue('DisplayBrowsePayments_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_DisplayBrowsePayments',0)

PreCopy  Routine
  p_web.SetValue('DisplayBrowsePayments_form:ready_',1)
  p_web.SetSessionValue('DisplayBrowsePayments_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_DisplayBrowsePayments',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('DisplayBrowsePayments_form:ready_',1)
  p_web.SetSessionValue('DisplayBrowsePayments_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('DisplayBrowsePayments:Primed',0)

PreDelete       Routine
  p_web.SetValue('DisplayBrowsePayments_form:ready_',1)
  p_web.SetSessionValue('DisplayBrowsePayments_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('DisplayBrowsePayments:Primed',0)
  p_web.setsessionvalue('showtab_DisplayBrowsePayments',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('DisplayBrowsePayments_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
    CheckPaid(p_web)  
    
    ! Update the relevant data files
    Access:JOBS.ClearKey(job:Ref_Number_Key)
    job:Ref_Number  = p_web.GSV('job:Ref_Number')
    IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
        p_web.SessionQueueToFile(JOBS)
        IF (Access:JOBS.TryUpdate() = Level:Benign)
            Access:WEBJOB.Clearkey(wob:RefNumberKey)
            wob:RefNumber = job:Ref_Number
            IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                p_web.SessionQueueToFile(WEBJOB)
                IF (Access:WEBJOB.TryUpdate() = Level:Benign)
                    Access:JOBSE.ClearKey(jobe:RefNumberKey)
                    jobe:RefNumber = job:Ref_Number
                    IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                        p_web.SessionQueueToFile(JOBSE)
                        Access:JOBSE.TryUpdate()
                    END ! IF
                END !IF
            END !IF
            
        END ! IF
        
    END
    
  p_web.DeleteSessionValue('DisplayBrowsePayments_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
  ! tab = 3
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
  DO DeleteSessionValues
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('DisplayBrowsePayments:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('locOutstanding')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
GetOutstanding      PROCEDURE()!,REAL
paid                    REAL()
vat                     REAL()
rrcTotal                REAL()
    CODE
        Relate:JOBPAYMT.Open()
        Relate:USERS.Open()
        
        Access:JOBPAYMT.ClearKey(jpt:All_Date_Key)
        jpt:Ref_Number = p_web.GSV('job:Ref_Number')
        SET(jpt:All_Date_Key,jpt:All_Date_Key)
        LOOP UNTIL Access:JOBPAYMT.Next() <> Level:Benign
            IF (jpt:Ref_Number <> p_web.GSV('job:Ref_Number'))
                BREAK
            END ! IF
            Access:USERS.ClearKey(use:User_Code_Key)
            use:User_Code = jpt:User_Code
            IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
                IF (use:Location <> p_web.GSV('ARC:SiteLocation'))
                    paid += jpt:Amount
                END ! IF
            ELSE ! IF
            END ! IF
        END ! LOOP
        
        vat = (p_web.GSV('job:Courier_Cost') * (GetVATRate(p_web.GSV('job:Account_Number'),'L') /100)) + |
            (p_web.GSV('jobe:RRCCPartsCost') * (GetVATRate(p_web.GSV('job:Account_Number'),'P') /100)) +  |
            (p_web.GSV('jobe:RRCCLabourCost') * (GetVATRate(p_web.GSV('job:Account_Number'),'L')/100))
                            
        rrcTotal = vat + p_web.GSV('jobe:RRCCSubTotal')
        
        
        Relate:USERS.Close()
        Relate:JOBPAYMT.Close()
        
        RETURN rrcTOtal - paid
showHideCreateInvoice       PROCEDURE()
CODE
    IF (p_web.gsv('job:Chargeable_Job') <> 'YES')
        RETURN TRUE
    END
        
    SentToHub(p_web)
    IF (p_web.GSV('BookingSite') = 'ARC' AND p_web.GSV('SendToHub') <> 1)
        RETURN TRUE
    END
        
    IF (p_web.GSV('job:Bouncer') = 'X')
        RETURN TRUE
    END
        
    ! Job not completed
    IF (p_web.GSV('job:Date_Completed') = 0 AND p_web.GSV('job:Exchange_Unit_Number') = 0)
        RETURN TRUE
    END
        
    ! Job has not been priced
    IF (p_web.GSV('job:ignore_Chargeable_Charges') = 'YES')
        IF (p_web.GSV('BookingSite') = 'RRC')
            IF (p_web.GSV('jobe:RRCSubTotal') = 0)
                RETURN TRUE
            END
        ELSE
            IF (p_web.GSV('job:Sub_Total') = 0)
                RETURN TRUE
            END
                
        END
    END
    p_web.SSV('URL:CreateInvoice','CreateInvoice?returnURL=DisplayBrowsePayments')
    p_web.SSV('URL:CreateInvoiceTarget','_self')
    p_web.SSV('URL:CreateInvoiceText','Create Invoice')
 
    IF (p_web.GSV('job:Invoice_Number') > 0)
        Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
        inv:Invoice_Number = p_web.GSV('job:Invoice_Number')
        IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign)
            IF (inv:Invoice_Type <> 'SIN')
                RETURN TRUE
            END
            ! If job has been invoiced, just print the invoice, rather than asking to create one
            IF (p_web.GSV('BookingSite') = 'RRC')
                IF (inv:RRCInvoiceDate > 0)
                    p_web.SSV('URL:CreateInvoice',p_web.GSV('Document:Invoice') & '?var=' & RANDOM(1,9999999))
                    p_web.SSV('URL:CreateInvoiceTarget','_blank')
                    p_web.SSV('URL:CreateInvoiceText','Print Invoice')
                END
            ELSE
                If (inv:ARCInvoiceDate > 0)
                    p_web.SSV('URL:CreateInvoice',p_web.GSV('Document:Invoice') & '?var=' & RANDOM(1,9999999))
                    p_web.SSV('URL:CreateInvoiceTarget','_blank')
                    p_web.SSV('URL:CreateInvoiceText','Print Invoice')
                END
            END
        END
            
    END
    RETURN FALSE
    
        
        
