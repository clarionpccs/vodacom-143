

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER139.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
UseReplenishmentProcess PROCEDURE  (fModelNumber,fAccountNumber) ! Declare Procedure
MANUFACT::State  USHORT
MODELNUM::State  USHORT
TRADEACC::State  USHORT
FilesOpened     BYTE(0)

  CODE
    do openFiles
    do saveFiles


    Return# = 0
    ! Inserting (DBH 21/02/2008) # 9717 - If model/manufacturer is used in Replenishment, then it can't be 48 Hour
    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number = fAccountNumber
    If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        If tra:IgnoreReplenishmentProcess = 0
            ! This account CAN use the replenishment process (DBH: 21/02/2008)
            Access:MODELNUM.Clearkey(mod:Model_Number_Key)
            mod:Model_Number = fModelNumber
            If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
                If mod:UseReplenishmentProcess = 1
                    Return# = 1
                Else ! If mod:UseReplenishmentProcess = 1
                    Access:MANUFACT.Clearkey(man:Manufacturer_Key)
                    man:Manufacturer = mod:Manufacturer
                    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                        If man:UseReplenishmentProcess = 1
                            Return# = 1
                        End ! If man:UseReplenishmentProcess = 1
                    End ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                End ! If mod:UseReplenishmentProcess = 1
            End ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
        End ! If tra:IgnoreReplenishmentProcess = 0
    End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
    ! End (DBH 21/02/2008) #9717

    do restoreFiles
    do closeFiles

    Return Return#
SaveFiles  ROUTINE
  MANUFACT::State = Access:MANUFACT.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  MODELNUM::State = Access:MODELNUM.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  TRADEACC::State = Access:TRADEACC.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF MANUFACT::State <> 0
    Access:MANUFACT.RestoreFile(MANUFACT::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF MODELNUM::State <> 0
    Access:MODELNUM.RestoreFile(MODELNUM::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF TRADEACC::State <> 0
    Access:TRADEACC.RestoreFile(TRADEACC::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:MANUFACT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MODELNUM.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MODELNUM.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:MANUFACT.Close
     Access:MODELNUM.Close
     Access:TRADEACC.Close
     FilesOpened = False
  END
