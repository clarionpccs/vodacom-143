

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER194.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
InWarrantyMarkup     PROCEDURE  (func:Manufacturer,func:SiteLocation) ! Declare Procedure
returnValue          LONG                                  !
MANUFACT::State  USHORT
MANMARK::State  USHORT
FilesOpened     BYTE(0)

  CODE
    do OpenFiles
    do SaveFiles

    returnValue = 0

    Access:MANUFACT.Clearkey(man:Manufacturer_Key)
    man:Manufacturer    = func:Manufacturer
    If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        !Found
        Access:MANMARK.Clearkey(mak:SiteLocationKey)
        mak:RefNumber   = man:RecordNumber
        mak:SiteLocation    = func:SiteLocation
        If Access:MANMARK.Tryfetch(mak:SiteLocationKey) = Level:Benign
            !Found
            returnValue = mak:InWarrantyMarkup
        Else ! If Access:MANMARK.Tryfetch(mak:SiteLocationKey) = Level:Benign
            !Error
        End !If Access:MANMARK.Tryfetch(mak:SiteLocationKey) = Level:Benign
    Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        !Error
    End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign

    do RestoreFiles
    do CloseFiles

    return returnValue
SaveFiles  ROUTINE
  MANUFACT::State = Access:MANUFACT.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  MANMARK::State = Access:MANMARK.SaveFile()               ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF MANUFACT::State <> 0
    Access:MANUFACT.RestoreFile(MANUFACT::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF MANMARK::State <> 0
    Access:MANMARK.RestoreFile(MANMARK::State)             ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:MANUFACT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANMARK.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANMARK.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:MANUFACT.Close
     Access:MANMARK.Close
     FilesOpened = False
  END
