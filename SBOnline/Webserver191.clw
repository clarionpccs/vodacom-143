

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER191.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
virtualSite          PROCEDURE  (fLocation)                ! Declare Procedure
LOCATION_ALIAS::State  USHORT
FilesOpened     BYTE(0)

  CODE
    do openFiles
    do saveFiles

    returnValue# = 0
    Access:LOCATION_ALIAS.ClearKey(loc_ali:Location_Key)
    loc_ali:Location = fLocation
    If Access:LOCATION_ALIAS.TryFetch(loc_ali:Location_Key) = Level:Benign
        !Found
        If loc_ali:VirtualSite
            returnValue# = 1
        End !If loc_ali:VirtualSite
    Else!If Access:LOCATION_ALIAS.TryFetch(loc_ali:Location_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:LOCATION_ALIAS.TryFetch(loc_ali:Location_Key) = Level:Benign

    do restoreFiles
    do closeFiles
    
    return returnValue#
SaveFiles  ROUTINE
  LOCATION_ALIAS::State = Access:LOCATION_ALIAS.SaveFile() ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF LOCATION_ALIAS::State <> 0
    Access:LOCATION_ALIAS.RestoreFile(LOCATION_ALIAS::State) ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:LOCATION_ALIAS.Open                               ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:LOCATION_ALIAS.UseFile                            ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:LOCATION_ALIAS.Close
     FilesOpened = False
  END
