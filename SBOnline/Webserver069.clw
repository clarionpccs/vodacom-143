

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER069.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER061.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER063.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER064.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER065.INC'),ONCE        !Req'd for module callout resolution
                     END


FinishBatch          PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locWaybillNumber     STRING(30)                            !
LocDespatchNumber    REAL                                  !
FilesOpened     Long
DESBATCH::State  USHORT
MULDESPJ_ALIAS::State  USHORT
MULDESP_ALIAS::State  USHORT
WAYBILLS::State  USHORT
WAYBILLJ::State  USHORT
LOAN::State  USHORT
EXCHANGE::State  USHORT
SUBTRACC::State  USHORT
TRADEACC::State  USHORT
COURIER::State  USHORT
JOBSE::State  USHORT
WEBJOB::State  USHORT
JOBS::State  USHORT
MULDESPJ::State  USHORT
MULDESP::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FinishBatch')
  loc:formname = 'FinishBatch_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FinishBatch','')
    p_web._DivHeader('FinishBatch',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFinishBatch',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFinishBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFinishBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FinishBatch',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFinishBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FinishBatch',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
Despatch            ROUTINE
    PUTINI('MULTIPLEDESPATCH',p_web.GSV('muld:RecordNumber'),'InUse',CLIP(PATH()) & '\DESPATCH.LOG')
    LocDespatchNumber = 0 
    
    Access:MULDESP.ClearKey(muld:RecordNumberKey)
    muld:RecordNumber = p_web.GSV('muld:RecordNumber')
    IF (Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign)
        p_web.SetValue('AllOK',1)
        Access:MULDESPJ.ClearKey(mulj:JobNumberKey)
        mulj:RefNumber = muld:RecordNumber
        SET(mulj:JobNumberKey,mulj:JobNumberKey)
        LOOP UNTIL Access:MULDESPJ.Next()
            IF (mulj:RefNumber <> muld:RecordNumber)
                BREAK
            END

            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = mulj:JobNumber
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
                CYCLE
            END
            
            
            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = job:Ref_Number
            IF (Access:JOBSE.TryFetch(jobe:RefNumberKey))
                CYCLE
            END
            
            Access:WEBJOB.ClearKey(wob:RefNumberKey)
            wob:RefNumber = job:Ref_Number
            IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
                CYCLE
            END
            
            p_web.FileToSessionQueue(JOBS)
            p_web.FileToSessionQueue(JOBSE)
            p_web.FileToSessionQueue(WEBJOB)
            
            ! Add to waybill job list
            ! SB does this regards of waybill. So I will too
            IF (Access:WAYBILLJ.PrimeRecord() = Level:Benign)
                waj:WayBillNumber = p_web.GSV('locWaybillNumber')
                waj:JobNumber = job:Ref_Number
                IF (p_web.GSV('BookingSite') = 'RRC')
                    CASE (jobe:DespatchType)
                    OF 'JOB'
                        waj:IMEINumber = job:ESN
                    OF 'EXC'
                        Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                        xch:Ref_Number = job:Exchange_Unit_Number
                        IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                           
                        END
                        waj:IMEINumber = xch:ESN
                    OF 'LOA'
                        Access:LOAN.ClearKey(loa:Ref_Number_Key)
                        loa:Ref_Number = job:Loan_Unit_Number
                        IF (Access:LOAN.TryFetch(loa:Ref_Number_Key))
                        END
                        
                        waj:IMEINumber = loa:ESN
                    END
                    waj:JobType = jobe:DespatchType
                    p_web.SSV('DespatchType',jobe:DespatchType)
                ELSE
                    CASE (job:Despatch_Type)
                    OF 'JOB'
                        waj:IMEINumber = job:ESN
                    OF 'EXC'
                        Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                        xch:Ref_Number = job:Exchange_Unit_Number
                        IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                           
                        END
                        waj:IMEINumber = xch:ESN
                    OF 'LOA'
                        Access:LOAN.ClearKey(loa:Ref_Number_Key)
                        loa:Ref_Number = job:Loan_Unit_Number
                        IF (Access:LOAN.TryFetch(loa:Ref_Number_Key))
                        END
                        
                        waj:IMEINumber = loa:ESN
                    END
                    waj:JobType = job:Despatch_Type
                    p_web.SSV('DespatchType',job:Despatch_Type)
                END
                waj:OrderNumber = job:Order_Number
                waj:SecurityPackNumber = mulj:SecurityPackNumber
                IF (Access:WAYBILLJ.TryInsert())
                    Access:WAYBILLJ.CancelAutoInc()
                    p_web.SetValue('AllOK',0)
                    CYCLE                    
                END
                    
                CASE p_web.GSV('DespatchType')
                OF 'JOB'
                    Despatch:Job(p_web)
                OF 'EXC'
                    Despatch:Exc(p_web)
                OF 'LOA'
                    Despatch:Loa(p_web)
                END
    
                !despatch note number needed for jobs file
                if LocDespatchNumber = 0 THEN
                    !this has not been set up yet - must be the first time through
                    If access:desbatch.primerecord() = Level:Benign
                        If access:desbatch.insert()
                            access:desbatch.cancelautoinc()
                        End  !If access:desbatch.insert()
                    End  !If access:desbatch.primerecord() = Level:Benign
                    LocDespatchNumber  = dbt:Batch_Number                        
                END
                !p_web.ssv('job:Despatch_Number',left(format(LocDespatchNumber,@n10)))  This did not work - only gave first 3 characters of number
                            
                Access:JOBS.ClearKey(job:Ref_Number_Key)
                job:Ref_Number = p_web.GSV('job:Ref_Number')
                IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                    p_web.SessionQueueToFile(JOBS)
                    job:Despatch_Number = LocDespatchNumber !update the despatch number
                    Access:JOBS.TryUpdate()
                END
            
            
                Access:JOBSE.ClearKey(jobe:RefNumberKey)
                jobe:RefNumber = p_web.GSV('jobe:RefNumber')
                IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                    p_web.SessionQueueToFile(JOBSE)
                    Access:JOBSE.TryUpdate()
                END
            
                Access:WEBJOB.ClearKey(wob:RefNumberKey)
                wob:RefNumber = p_web.GSV('wob:RefNumber')
                IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                    p_web.SessionQueueToFile(WEBJOB)
                    Access:WEBJOB.TryUpdate()
                END
            END
        END    
        IF (p_web.GetValue('AllOK') = 1)
            Access:MULDESP.ClearKey(muld:RecordNumberKey)
            muld:RecordNumber = p_web.GSV('muld:RecordNumber')
            IF (Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign)
                IF (Relate:MULDESP.Delete(0) = Level:Benign)
                    PUTINI('MULTIPLEDESPATCH',p_web.GSV('muld:RecordNumber'),'Free',CLIP(PATH()) & '\DESPATCH.LOG')
                END
            END
        END
    END
    
    
    
DeleteSessionVariables      ROUTINE
    PUTINI('MULTIPLEDESPATCH',p_web.GSV('muld:RecordNumber'),'Free',CLIP(PATH()) & '\DESPATCH.LOG')
    p_web.DeleteSessionValue('IgnoreInUse')    
    p_web.DeleteSessionValue('locWaybillNumber')
    p_web.DeleteSessionValue('Hide:PrintWaybillButton')
    p_web.DeleteSessionValue('Hide:DespatchNoteButton')
OpenFiles  ROUTINE
  p_web._OpenFile(DESBATCH)
  p_web._OpenFile(MULDESPJ_ALIAS)
  p_web._OpenFile(MULDESP_ALIAS)
  p_web._OpenFile(WAYBILLS)
  p_web._OpenFile(WAYBILLJ)
  p_web._OpenFile(LOAN)
  p_web._OpenFile(EXCHANGE)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(COURIER)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(MULDESPJ)
  p_web._OpenFile(MULDESP)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(DESBATCH)
  p_Web._CloseFile(MULDESPJ_ALIAS)
  p_Web._CloseFile(MULDESP_ALIAS)
  p_Web._CloseFile(WAYBILLS)
  p_Web._CloseFile(WAYBILLJ)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(MULDESPJ)
  p_Web._CloseFile(MULDESP)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FinishBatch_form:inited_',1)
  do RestoreMem

CancelForm  Routine
  DO DeleteSessionVariables

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  If p_web.GSV('locWaybillNumber') = ''
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locWaybillNumber',locWaybillNumber)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locWaybillNumber')
    locWaybillNumber = p_web.GetValue('locWaybillNumber')
    p_web.SetSessionValue('locWaybillNumber',locWaybillNumber)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FinishBatch_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  ! Batch already in use?
  IF (p_web.IfExistsValue('muld:RecordNumber'))
      p_web.StoreValue('muld:RecordNumber')
  END
  
  If GETINI('MULTIPLEDESPATCH',p_web.GSV('muld:RecordNumber'),,CLIP(PATH())&'\DESPATCH.LOG') = 'InUse'
      IF (p_web.IfExistsValue('IgnoreInUse'))
          p_web.StoreValue('IgnoreInUse')
      END
      
      IF (p_web.GSV('IgnoreInUse') = 1)
      ELSE
          p_web.SSV('Message:Text','The selected batch appears to be in use by another station. If you continue despatching, corruption may occur.'&|
              '\n\nIf you are SURE this batch is not in use, click ''OK''. Otherwise click ''Cancel'' and try again.')
          p_web.SSV('Message:PassURL','FinishBatch?IgnoreInUse=1')
          p_web.SSV('Message:FailURL','MultipleBatchDespatch')
          MessageQuestion(p_web)
          EXIT
      END
      
  End !If Sub(Format(Clock(),@t01),4,2) = GETINI('MULTIPLEDESPATCH',muld:BatchNUmber,,CLIP(PATH())&'\DESPATCH.LOG')
      ! Despatch now, or cons number required?
      p_web.SSV('BatchDespatched',0)
      p_web.SSV('Hide:DespatchNoteButton',1)
      p_web.SSV('Hide:PrintWaybillButton',1)
      locWaybillNumber = ''
      LocDespatchNumber = 0
      
      p_web.SSV('locWaybillNumber',locWaybillNumber)
      Access:MULDESP.ClearKey(muld:RecordNumberKey)
      muld:RecordNumber = p_web.GSV('muld:RecordNumber')
      IF (Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign)
        
          p_web.FileToSessionQueue(MULDESP)
          
          p_web.SSV('MultipleDespatch:Type',p_web.GSV('muld:BatchType'))
          p_web.SSV('MultipleDespatch:AccountNumber',p_web.GSV('muld:AccountNumber'))
          p_web.SSV('MultipleDespatch:Courier',p_web.GSV('muld:Courier'))
          
          Access:COURIER.ClearKey(cou:Courier_Key)
          cou:Courier = muld:Courier
          IF (Access:COURIER.TryFetch(cou:Courier_Key))
          END
  
          IF (cou:AutoConsignmentNo)
              cou:LastConsignmentNo += 1
              Access:COURIER.TryUpdate()
              locWaybillNumber = cou:LastConsignmentNo
              p_web.SSV('locWaybillNumber',locWaybillNumber)
              p_web.SSV('BatchDespatched',1)
          ELSE
              locWaybillNumber = ''
          END
          
          !do we need to set up Waybill stuff and show waybill number?
          IF (cou:PrintWaybill)
              locWaybillNumber = NextWaybillNumber()
            
              Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
              way:WayBillNumber = locWaybillNumber
              IF (Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign)
                  way:WayBillType = 1 ! Created From RRC
                  way:AccountNumber = muld:AccountNumber
                  way:FromAccount = p_web.GSV('BookingAccount')
                  IF (p_web.GSV('BookingSite') = 'RRC')
                      way:ToAccount = muld:AccountNumber
                      way:WaybillID = 10 ! RRC TO Customer (Multi)
                      ! Allow for mult desp back to PUP
                      Access:MULDESPJ.ClearKey(mulj:JobNumberKey)
                      mulj:RefNumber = muld:RecordNumber
                      SET(mulj:JobNumberKey,mulj:JobNumberKey)
                      LOOP UNTIL Access:MULDESPJ.Next()
                          IF (mulj:RefNumber <> muld:RecordNumber)
                              BREAK
                          END
                        
                          Access:JOBS.ClearKey(job:Ref_Number_Key)
                          job:Ref_Number = mulj:JobNumber
                          IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
                              CYCLE
                          END
                        
                          IF (job:who_booked = 'WEB')
                              way:WayBillType = 9
                              way:WaybillID = 22
                          END
                          BREAK
                      END
                    
                  ELSE ! IF (p_web.GSV('BookingSite') = 'RRC')
                      way:ToAccount = muld:AccountNumber
                      ! Send To Customer or RRC?
                      Access:MULDESPJ.ClearKey(mulj:JobNumberKey)
                      mulj:RefNumber = muld:RecordNumber
                      SET(mulj:JobNumberKey,mulj:JobNumberKey)
                      LOOP UNTIL Access:MULDESPJ.Next()
                          IF (mulj:RefNumber <> muld:RecordNumber)
                              BREAK
                          END
                        
                          Access:JOBS.ClearKey(job:Ref_Number_Key)
                          job:Ref_Number = mulj:JobNumber
                          IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
                              CYCLE
                          END
                          Access:JOBSE.ClearKey(jobe:RefNumberKey)
                          jobe:RefNumber = job:Ref_Number
                          IF (Access:JOBSE.TryFetch(jobe:RefNumberKey))
                              CYCLE
                          END
                          Access:WEBJOB.ClearKey(wob:RefNumberKey)
                          wob:RefNumber = job:Ref_Number
                          IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
                              CYCLE
                          END
                        
                          IF (jobe:WebJob)
                              way:WaybillID = 12 ! ARC TO RRC (Multi)
                              way:AccountNumber = wob:HeadAccountNumber
                              way:ToAccount = wob:HeadAccountNumber
                          ELSE
                              way:WaybillID = 11 ! ARC TO Customer
                              way:WayBillType = 2 ! Don't show in confirmation
                          END
                        
                          BREAK
                      END
                  END ! IF (p_web.GSV('BookingSite') = 'RRC')
                  Access:WAYBILLS.TryUpdate()
                  p_web.SSV('Hide:PrintWaybillButton',0)
                  ! Waybill Variables
                  ! locWaybillNumber
                  p_web.SSV('Waybill:Courier',muld:Courier)
                  ! Waybill:Courier
                  IF (p_web.GSV('BookingSite') = 'RRC')
                      p_web.SSV('Waybill:FromType','TRA')
                  ELSE
                      p_web.SSV('Waybill:FromType','DEF')
                  END
                  ! Waybill:FromType
                  p_web.SSV('Waybill:FromAccount',p_web.GSV('BookingAccount'))
                  ! Waybill:FromAccount
                  p_web.SSV('Waybill:ToType',muld:BatchType)
                  ! Waybill:ToType
                  p_web.SSV('Waybill:ToAccount',muld:AccountNumber)
                  ! Waybill:ToAccount
                  
                  p_web.SSV('BatchDespatched',1)
                  
              END     !if waybils.fetch
            
              p_web.SSV('locWaybillNumber',locWaybillNumber)
              DO Despatch
              
              p_web.SSV('BatchDespatched',1)
              
              !Set Values For Multiple Despatch Note, if necessary
              p_web.SSV('MultipleDespatch:ConsignmentNo',p_web.GSV('locWaybillNumber'))
              
          END     !IF (cou:PrintWaybill)
          
          !Do we need to display the despatch note button? (This used to be part of an ELSE on cou:PrintWaybill - TB13096 - JC23/09/13
          p_web.SSV('Hide:DespatchNoteButton',1)
          
          !changed back later - always hide this button - this is a multiple despatch note. Instead when added jobs to a
          !batch allow user to print the individual despatch note there.
  !        
  !        IF (p_web.GSV('muld:BatchType') = 'TRA')
  !            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
  !            tra:Account_Number = p_web.GSV('muld:AccountNumber')
  !            IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
  !                IF (tra:Print_Despatch_Despatch = 'YES')
  !                    !IF (tra:Summary_Despatch_Notes = 'YES')
  !                        ! Don't think this is used. - it isn't and should not be considered
  !                        p_web.SSV('Hide:DespatchNoteButton',0)
  !                    !END
  !                END
  !            END
  !    
  !        ELSE ! IF (muld:BatchType = 'TRA')
  !            Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
  !            sub:Account_Number = p_web.GSV('muld:AccountNumber')
  !            IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
  !                Access:TRADEACC.ClearKey(tra:Account_Number_Key)
  !                tra:Account_Number = sub:Main_Account_Number
  !                IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
  !                    IF (tra:Use_Sub_Accounts = 'YES')
  !                        IF (sub:Print_Despatch_Despatch = 'YES')
  !                            !IF (sub:Summary_Despatch_Notes = 'YES') - should not be used
  !                                p_web.SSV('Hide:DespatchNoteButton',0)
  !                            !END
  !                        END 
  !                
  !                    ELSE
  !                        IF (tra:Print_Despatch_Despatch = 'YES')
  !                            !IF (tra:Summary_Despatch_Notes = 'YES') - should not be used
  !                                p_web.SSV('Hide:DespatchNoteButton',0)
  !                            !END
  !                        END
  !                    END
  !                END
  !            END
  !        END ! IF (muld:BatchType = 'TRA')
  
      END     !IF (Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign)
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locWaybillNumber = p_web.RestoreValue('locWaybillNumber')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'MultipleBatchDespatch'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FinishBatch_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FinishBatch_ChainTo')
    loc:formaction = p_web.GetSessionValue('FinishBatch_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'MultipleBatchDespatch'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FinishBatch" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FinishBatch" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FinishBatch" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Multiple Despatch') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Multiple Despatch',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FinishBatch">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FinishBatch" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FinishBatch')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
        If p_web.GSV('locWaybillNumber') = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Insert Consignment Number') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Finish Batch') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FinishBatch')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FinishBatch'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    ElsIf p_web.GSV('locWaybillNumber') = ''
        p_web.SetValue('SelectField',clip(loc:formname) & '.locWaybillNumber')
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
          If p_web.GSV('locWaybillNumber') = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FinishBatch')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
    if p_web.GSV('locWaybillNumber') = ''
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
  If p_web.GSV('locWaybillNumber') = ''
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Insert Consignment Number') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FinishBatch_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Insert Consignment Number')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Insert Consignment Number')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Insert Consignment Number')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Insert Consignment Number')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locWaybillNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locWaybillNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locWaybillNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonFinishBatch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonFinishBatch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FinishBatch_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonPrintWaybill
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonPrintWaybill
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonPrintDespatchNote
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonPrintDespatchNote
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Finish Batch') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FinishBatch_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Finish Batch')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Finish Batch')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Finish Batch')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Finish Batch')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::textBatchFinished
      do Comment::textBatchFinished
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locWaybillNumber  Routine
  p_web._DivHeader('FinishBatch_' & p_web._nocolon('locWaybillNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Consignment Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FinishBatch_' & p_web._nocolon('locWaybillNumber') & '_prompt')

Validate::locWaybillNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locWaybillNumber',p_web.GetValue('NewValue'))
    locWaybillNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locWaybillNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locWaybillNumber',p_web.GetValue('Value'))
    locWaybillNumber = p_web.GetValue('Value')
  End
    locWaybillNumber = Upper(locWaybillNumber)
    p_web.SetSessionValue('locWaybillNumber',locWaybillNumber)
  do Value::locWaybillNumber
  do SendAlert

Value::locWaybillNumber  Routine
  p_web._DivHeader('FinishBatch_' & p_web._nocolon('locWaybillNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locWaybillNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('BatchDespatched') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('BatchDespatched') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('locWaybillNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locWaybillNumber'',''finishbatch_locwaybillnumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locWaybillNumber',p_web.GetSessionValueFormat('locWaybillNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FinishBatch_' & p_web._nocolon('locWaybillNumber') & '_value')

Comment::locWaybillNumber  Routine
      loc:comment = ''
  p_web._DivHeader('FinishBatch_' & p_web._nocolon('locWaybillNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonFinishBatch  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonFinishBatch',p_web.GetValue('NewValue'))
    do Value::buttonFinishBatch
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    IF (p_web.GSV('locWaybillNumber') <> '')
        p_web.SSV('MultipleDespatch:ConsignmentNo',p_web.GSV('locWaybillNumber'))
        p_web.SSV('BatchDespatched',1)
    END
  do Value::buttonFinishBatch
  do SendAlert
  do Value::buttonPrintDespatchNote  !1
  do Value::textBatchFinished  !1
  do Prompt::locWaybillNumber
  do Value::locWaybillNumber  !1

Value::buttonFinishBatch  Routine
  p_web._DivHeader('FinishBatch_' & p_web._nocolon('buttonFinishBatch') & '_value',Choose(p_web.GSV('BatchDespatched') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('BatchDespatched') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonFinishBatch'',''finishbatch_buttonfinishbatch_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','FinishBatch','Finish Batch','button-entryfield',loc:formname,,,,loc:javascript,0,'images/package.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FinishBatch_' & p_web._nocolon('buttonFinishBatch') & '_value')

Comment::buttonFinishBatch  Routine
    loc:comment = ''
  p_web._DivHeader('FinishBatch_' & p_web._nocolon('buttonFinishBatch') & '_comment',Choose(p_web.GSV('BatchDespatched') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('BatchDespatched') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonPrintWaybill  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonPrintWaybill',p_web.GetValue('NewValue'))
    do Value::buttonPrintWaybill
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do Value::buttonPrintWaybill
  do SendAlert

Value::buttonPrintWaybill  Routine
  p_web._DivHeader('FinishBatch_' & p_web._nocolon('buttonPrintWaybill') & '_value',Choose(p_web.GSV('Hide:PrintWaybillButton') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:PrintWaybillButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonPrintWaybill'',''finishbatch_buttonprintwaybill_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PrintWaybill','Waybill','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('Waybill?' &'rnd=' & RANDOM(1,1000))) & ''','''&clip('_blank')&''')',loc:javascript,0,'images/printer.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FinishBatch_' & p_web._nocolon('buttonPrintWaybill') & '_value')

Comment::buttonPrintWaybill  Routine
    loc:comment = ''
  p_web._DivHeader('FinishBatch_' & p_web._nocolon('buttonPrintWaybill') & '_comment',Choose(p_web.GSV('Hide:PrintWaybillButton') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:PrintWaybillButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonPrintDespatchNote  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonPrintDespatchNote',p_web.GetValue('NewValue'))
    do Value::buttonPrintDespatchNote
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do Value::buttonPrintDespatchNote
  do SendAlert

Value::buttonPrintDespatchNote  Routine
  p_web._DivHeader('FinishBatch_' & p_web._nocolon('buttonPrintDespatchNote') & '_value',Choose(p_web.GSV('Hide:DespatchNoteButton') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:DespatchNoteButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonPrintDespatchNote'',''finishbatch_buttonprintdespatchnote_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','DespatchNote','Despatch Note','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('MultipleDespatchNote?' &'rnd=' & RANDOM(1,1000))) & ''','''&clip('_blank')&''')',loc:javascript,0,'images/printer.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FinishBatch_' & p_web._nocolon('buttonPrintDespatchNote') & '_value')

Comment::buttonPrintDespatchNote  Routine
    loc:comment = ''
  p_web._DivHeader('FinishBatch_' & p_web._nocolon('buttonPrintDespatchNote') & '_comment',Choose(p_web.GSV('Hide:DespatchNoteButton') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:DespatchNoteButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::textBatchFinished  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('textBatchFinished',p_web.GetValue('NewValue'))
    do Value::textBatchFinished
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::textBatchFinished  Routine
  p_web._DivHeader('FinishBatch_' & p_web._nocolon('textBatchFinished') & '_value',Choose(p_web.GSV('BatchDespatched') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('BatchDespatched') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBoldLarge')&'">' & p_web.Translate('The Batch Will Be Despatched When You Click ''Save''',) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FinishBatch_' & p_web._nocolon('textBatchFinished') & '_value')

Comment::textBatchFinished  Routine
    loc:comment = ''
  p_web._DivHeader('FinishBatch_' & p_web._nocolon('textBatchFinished') & '_comment',Choose(p_web.GSV('BatchDespatched') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('BatchDespatched') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FinishBatch_locWaybillNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locWaybillNumber
      else
        do Value::locWaybillNumber
      end
  of lower('FinishBatch_buttonFinishBatch_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonFinishBatch
      else
        do Value::buttonFinishBatch
      end
  of lower('FinishBatch_buttonPrintWaybill_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonPrintWaybill
      else
        do Value::buttonPrintWaybill
      end
  of lower('FinishBatch_buttonPrintDespatchNote_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonPrintDespatchNote
      else
        do Value::buttonPrintDespatchNote
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FinishBatch_form:ready_',1)
  p_web.SetSessionValue('FinishBatch_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FinishBatch',0)

PreCopy  Routine
  p_web.SetValue('FinishBatch_form:ready_',1)
  p_web.SetSessionValue('FinishBatch_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FinishBatch',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FinishBatch_form:ready_',1)
  p_web.SetSessionValue('FinishBatch_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FinishBatch:Primed',0)

PreDelete       Routine
  p_web.SetValue('FinishBatch_form:ready_',1)
  p_web.SetSessionValue('FinishBatch_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FinishBatch:Primed',0)
  p_web.setsessionvalue('showtab_FinishBatch',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If p_web.GSV('locWaybillNumber') = ''
  End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FinishBatch_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
    IF (p_web.GSV('BatchDespatched') = 1)
        Do Despatch
    END
    
    DO DeleteSessionVariables
  p_web.DeleteSessionValue('FinishBatch_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
  If p_web.GSV('locWaybillNumber') = ''
    loc:InvalidTab += 1
          locWaybillNumber = Upper(locWaybillNumber)
          p_web.SetSessionValue('locWaybillNumber',locWaybillNumber)
        If loc:Invalid <> '' then exit.
  End
  ! tab = 3
    loc:InvalidTab += 1
  ! tab = 1
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FinishBatch:Primed',0)
  p_web.StoreValue('locWaybillNumber')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
alertmessage  Routine
  packet = clip(packet) & |
    '<<SCRIPT LANGUAGE="javascript"><13,10>'&|
    '<<!--<13,10>'&|
    'function CONFIRM(){{if (!confirm<13,10>'&|
    '("Change this to your message"))<13,10>'&|
    'window.location = "MultipleBatchDespatch";return " "}<13,10>'&|
    'document.writeln(CONFIRM())<13,10>'&|
    '<<!-- END --><13,10>'&|
    '<</SCRIPT><13,10>'&|
    ''
