

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER093.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER089.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER095.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER344.INC'),ONCE        !Req'd for module callout resolution
                     END


PickExchangeUnit     PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:ExchangeUnitNumber LONG                                !
tmp:UnitDetails      STRING(100)                           !
tmp:ExchangeIMEINumber STRING(30)                          !
tmp:exchangeModelNumber STRING(30)                         !
tmp:MSN              STRING(30)                            !
tmp:ExchangeUnitDetails STRING(60)                         !
tmp:ExchangeAccessories STRING(100)                        !
tmp:ExchangeLocation STRING(100)                           !
tmp:ReplacementValue REAL                                  !
locExchangeAlertMessage STRING(255)                        !
tmp:RemovalReason    BYTE                                  !
locRemovalAlertMessage STRING(255)                         !
tmp:HandsetPartNumber STRING(30)                           !
tmp:HandsetReplacementValue REAL                           !
tmp:NoUnitAvailable  BYTE                                  !
tmp:ExchangeDate     DATE                                  !
locKeepParts         LONG                                  !
FilesOpened     Long
AUDIT2::State  USHORT
STOCKALL::State  USHORT
JOBNOTES::State  USHORT
JOBSE2::State  USHORT
SMSRECVD::State  USHORT
COURIER::State  USHORT
JOBS::State  USHORT
EXCHANGE::State  USHORT
JOBEXACC::State  USHORT
EXCHANGE_ALIAS::State  USHORT
EXCHOR48::State  USHORT
MANUFACT::State  USHORT
PARTS::State  USHORT
STOCK::State  USHORT
WARPARTS::State  USHORT
STOCKTYP::State  USHORT
EXCHHIST::State  USHORT
PRODCODE::State  USHORT
AUDIT::State  USHORT
JOBSE::State  USHORT
WEBJOB::State  USHORT
TRDBATCH::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
tmp:HandsetPartNumber_OptionView   View(PRODCODE)
                          Project(prd:ProductCode)
                        End
job:Exchange_Courier_OptionView   View(COURIER)
                          Project(cou:Courier)
                        End
                    MAP
AddExchangeUnit PROCEDURE()
AllocateExchangePart    Procedure(String func:Status,Byte func:SecondUnit)
ChargeableParts         PROCEDURE(),BOOL
WarrantyParts           PROCEDURE(),BOOL
GetExchangeDate         PROCEDURE()
GetExchangeDetails      PROCEDURE()
    END ! MAP
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('PickExchangeUnit')
  loc:formname = 'PickExchangeUnit_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('PickExchangeUnit','')
    p_web._DivHeader('PickExchangeUnit',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferPickExchangeUnit',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickExchangeUnit',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickExchangeUnit',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_PickExchangeUnit',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickExchangeUnit',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_PickExchangeUnit',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
clearExchangeDetails        routine
    p_web.SSV('tmp:MSN','')
    p_web.SSV('tmp:ExchangeUnitDetails','')
    p_web.SSV('tmp:ExchangeLocation','')
    p_web.SSV('tmp:ReplacementValue','')
    p_web.SSV('tmp:ExchangeIMEINumber','')
    p_web.SSV('tmp:ExchangeUnitNumber','')
    p_web.SSV('tmp:ExchangeModelNumber','')
    p_web.SSV('tmp:ExchangeAccessories','')
    p_web.SSV('tmp:ExchangeLocation','')
    p_web.SSV('tmp:HandsetPartNumber','')
    p_web.SSV('tmp:HandsetReplacementValue','')


lookupExchangeDetails       Routine
    p_web.SSV('Hide:HandsetPartNumber',1)
    p_web.SSV('tmp:HandsetPartNumber','')
    p_web.SSV('tmp:HandsetReplacementValue','')

    Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
    xch:Ref_Number    = p_web.GSV('tmp:ExchangeUnitNumber')
    if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign and xch:Ref_Number > 0)
        ! Found
        p_web.SSV('tmp:ExchangeIMEINumber',xch:ESN)
        p_web.SSV('tmp:MSN',xch:MSN)
        p_web.SSV('tmp:ExchangeUnitDetails',Clip(xch:Ref_Number) & ': ' & Clip(xch:Manufacturer) & ' ' & Clip(xch:Model_Number))
        p_web.SSV('tmp:ExchangeLocation',Clip(xch:Location) & ' / ' & Clip(xch:Stock_Type))
        p_web.SSV('tmp:exchangeModelNumber',xch:Model_Number)

        if (p_web.GSV('BookingSite') = 'ARC')
            Access:MANUFACT.Clearkey(man:Manufacturer_Key)
            man:Manufacturer    = xch:Manufacturer
            if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
                ! Found
            else ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
                ! Error
            end ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
            if (man:UseProdCodesForEXC = 1)
                p_web.SSV('Hide:HandsetPartNumber',0)
                p_web.SSV('tmp:HandsetPartNumber',p_web.GSV('jobe:ExchangeProductCode'))
                p_web.SSV('tmp:HandsetReplacementValue',p_web.GSV('jobe:HandsetReplacmentValue'))
            end ! if (man:UseProdCodesForEXC = 1)
        end ! if (p_web.GSV('job:Exchange_unit_Number') > 0 and p_web.GSV('BookingSite') = 'ARC')

        
        
    else ! if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
        ! Error
        p_web.SSV('tmp:ExchangeIMEINumber','')
        p_web.SSV('tmp:MSN','')
        p_web.SSV('tmp:ExchangeUnitDetails','')
        p_web.SSV('tmp:ExchangeLocation','')
    end ! if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
    
    GetExchangeDate()
clearVariables      ROUTINE
    p_web.DeleteSessionValue('ReadOnly:ExchangeDespatchDetails')
OpenFiles  ROUTINE
  p_web._OpenFile(AUDIT2)
  p_web._OpenFile(STOCKALL)
  p_web._OpenFile(JOBNOTES)
  p_web._OpenFile(JOBSE2)
  p_web._OpenFile(SMSRECVD)
  p_web._OpenFile(COURIER)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(EXCHANGE)
  p_web._OpenFile(JOBEXACC)
  p_web._OpenFile(EXCHANGE_ALIAS)
  p_web._OpenFile(EXCHOR48)
  p_web._OpenFile(MANUFACT)
  p_web._OpenFile(PARTS)
  p_web._OpenFile(STOCK)
  p_web._OpenFile(WARPARTS)
  p_web._OpenFile(STOCKTYP)
  p_web._OpenFile(EXCHHIST)
  p_web._OpenFile(PRODCODE)
  p_web._OpenFile(AUDIT)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(TRDBATCH)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(AUDIT2)
  p_Web._CloseFile(STOCKALL)
  p_Web._CloseFile(JOBNOTES)
  p_Web._CloseFile(JOBSE2)
  p_Web._CloseFile(SMSRECVD)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(JOBEXACC)
  p_Web._CloseFile(EXCHANGE_ALIAS)
  p_Web._CloseFile(EXCHOR48)
  p_Web._CloseFile(MANUFACT)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(STOCKTYP)
  p_Web._CloseFile(EXCHHIST)
  p_Web._CloseFile(PRODCODE)
  p_Web._CloseFile(AUDIT)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(TRDBATCH)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  IF (p_web.IfExistsValue('ReturnURL'))
      p_web.StoreValue('ReturnURL')
  END
  
  IF (p_web.IfExistsValue('FromURL'))
      p_web.StoreValue('FromURL')
  END
  
  IF (p_web.IfExistsValue('Warning'))
      p_web.StoreValue('Warning')
      IF p_web.GSV('Warning') <> 'EXCH'
          loc:Alert = 'Warning! The selected part is NOT an Exchange Part.'
      END
  END
  
  
  ! Passed From Exchange Allocation
  IF (p_web.IfExistsValue('sbogen:RecordNumber') AND p_web.GSV('FromURL') = 'ExchangeAllocation')
      p_web.StoreValue('sbogen:RecordNumber')
      Access:SBO_GenericFile.ClearKey(sbogen:RecordNumberKey)
      sbogen:RecordNumber = p_web.GSV('sbogen:RecordNumber')
      IF (Access:SBO_GenericFile.TryFetch(sbogen:RecordNumberKey) = Level:Benign)
          Access:JOBS.ClearKey(job:Ref_Number_Key)
          job:Ref_Number = sbogen:Long1
          IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
              p_web.FileToSessionQueue(JOBS)
          END
  
          Access:JOBSE.ClearKey(jobe:RefNumberKey)
          jobe:RefNumber = job:Ref_Number
          IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
              p_web.FileToSessionQueue(JOBSE)
          END
  
          Access:WEBJOB.ClearKey(wob:RefNumberKey)
          wob:RefNumber = job:Ref_Number
          IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
              p_web.FileToSessionQueue(WEBJOB)
          END
  
      END
  END
  
  ! Passed From Stock Allocation
  IF (p_web.IfExistsSessionValue('stl:RecordNumber') AND p_web.GSV('FromURL') = 'StockAllocation')
      Access:STOCKALL.ClearKey(stl:RecordNumberKey)
      stl:RecordNumber = p_web.GSV('stl:RecordNumber')
      IF (Access:STOCKALL.TryFetch(stl:RecordNumberKey) = Level:Benign)
  
          Access:JOBS.ClearKey(job:Ref_Number_Key)
          job:Ref_Number = stl:JobNumber
          IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
              p_web.FileToSessionQueue(JOBS)
          ELSE
              p_web.site.SaveButton.Class = 'NoShow'
              loc:alert = 'Unable To Find Job Details'
          END
  
          Access:JOBSE.ClearKey(jobe:RefNumberKey)
          jobe:RefNumber = job:Ref_Number
          IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
              p_web.FileToSessionQueue(JOBSE)
  
          END
  
          Access:WEBJOB.ClearKey(wob:RefNumberKey)
          wob:RefNumber = job:Ref_Number
          IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
              p_web.FileToSessionQueue(WEBJOB)
          END
  
      END
  
  END
  
  ! Security Checks
        p_web.SSV('ReadOnly:ExchangeIMEINumber',0)
  
        IF (p_web.GSV('Job:ViewOnly') = 1)
            p_web.SSV('ReadOnly:ExchangeIMEINumber',1)
        ELSE ! IF
            IF (p_web.GSV('job:Exchange_Unit_Number')> 0)
                IF (~DoesUserHaveAccess(p_web,'JOBS - AMEND EXCHANGE UNIT'))
                    ! #13418 Don't allow user to CHANGE the exchange unit (DBH: 05/11/2014)
                    p_web.SSV('ReadOnly:ExchangeIMEINumber',1)
                END ! IF
            ELSE ! IF
                IF (~DoesUserHaveAccess(p_web,'JOBS - ADD EXCHANGE UNIT'))
                    ! #13418 Don't allow user to ADD an exchange unit (DBH: 05/11/2014)
                    p_web.SSV('ReadOnly:ExchangeIMEINumber',1)
                END ! IF
            END ! IF
        END ! IF
  
      p_web.SSV('tmp:ExchangeUnitNumber',p_web.GSV('job:Exchange_Unit_Number'))
  
      if (p_web.GSV('job:Exchange_Unit_Number') > 0)
          do lookupExchangeDetails
      else
          do clearExchangeDetails
  
      end
  
      p_web.SSV('locExchangeAlertMessage','')
      p_web.SSV('locRemoveAlertMessage','')
      p_web.SSV('Hide:RemovalReason',1)
      p_web.SSV('tmp:RemovalReason',0)
        p_web.SSV('tmp:NoUnitAvailable',0)
        p_web.SSV('Hide:KeepParts',1)
        p_web.SSV('locKeepParts',0)
  
  
  p_web.SetValue('PickExchangeUnit_form:inited_',1)
  do RestoreMem

CancelForm  Routine
      do clearVariables

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('tmp:ExchangeDate')
    p_web.SetPicture('tmp:ExchangeDate','@d06')
  End
  p_web.SetSessionPicture('tmp:ExchangeDate','@d06')
  If p_web.IfExistsValue('tmp:ReplacementValue')
    p_web.SetPicture('tmp:ReplacementValue','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ReplacementValue','@n14.2')
  If p_web.IfExistsValue('tmp:HandsetReplacementValue')
    p_web.SetPicture('tmp:HandsetReplacementValue','@n14.2')
  End
  p_web.SetSessionPicture('tmp:HandsetReplacementValue','@n14.2')
  If p_web.IfExistsValue('job:Exchange_Despatched')
    p_web.SetPicture('job:Exchange_Despatched','@d06b')
  End
  p_web.SetSessionPicture('job:Exchange_Despatched','@d06b')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'tmp:ExchangeIMEINumber'
    p_web.setsessionvalue('showtab_PickExchangeUnit',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(EXCHANGE)
        p_web.setsessionvalue('tmp:ExchangeUnitNumber',xch:Ref_Number)
        GetExchangeDetails()
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locExchangeAlertMessage')
  End
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'job:Exchange_Courier'
    p_web.setsessionvalue('showtab_PickExchangeUnit',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(COURIER)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.job:Exchange_Consignment_Number')
  End
  If p_web.GSV('job:Exchange_Unit_Number') > 0 AND p_web.GSV('ReadOnly:ExchangeIMEINumber') = 0
    loc:TabNumber += 1
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('job:ESN',job:ESN)
  p_web.SetSessionValue('job:MSN',job:MSN)
  p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
  p_web.SetSessionValue('tmp:ExchangeIMEINumber',tmp:ExchangeIMEINumber)
  p_web.SetSessionValue('locExchangeAlertMessage',locExchangeAlertMessage)
  p_web.SetSessionValue('tmp:ExchangeDate',tmp:ExchangeDate)
  p_web.SetSessionValue('tmp:MSN',tmp:MSN)
  p_web.SetSessionValue('tmp:ExchangeUnitDetails',tmp:ExchangeUnitDetails)
  p_web.SetSessionValue('tmp:ExchangeAccessories',tmp:ExchangeAccessories)
  p_web.SetSessionValue('tmp:ExchangeLocation',tmp:ExchangeLocation)
  p_web.SetSessionValue('tmp:HandsetPartNumber',tmp:HandsetPartNumber)
  p_web.SetSessionValue('tmp:ReplacementValue',tmp:ReplacementValue)
  p_web.SetSessionValue('tmp:HandsetReplacementValue',tmp:HandsetReplacementValue)
  p_web.SetSessionValue('tmp:NoUnitAvailable',tmp:NoUnitAvailable)
  p_web.SetSessionValue('locKeepParts',locKeepParts)
  p_web.SetSessionValue('job:Exchange_Courier',job:Exchange_Courier)
  p_web.SetSessionValue('job:Exchange_Consignment_Number',job:Exchange_Consignment_Number)
  p_web.SetSessionValue('job:Exchange_Despatched',job:Exchange_Despatched)
  p_web.SetSessionValue('job:Exchange_Despatched_User',job:Exchange_Despatched_User)
  p_web.SetSessionValue('job:Exchange_Despatch_Number',job:Exchange_Despatch_Number)
  p_web.SetSessionValue('locRemovalAlertMessage',locRemovalAlertMessage)
  p_web.SetSessionValue('tmp:RemovalReason',tmp:RemovalReason)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('job:ESN')
    job:ESN = p_web.GetValue('job:ESN')
    p_web.SetSessionValue('job:ESN',job:ESN)
  End
  if p_web.IfExistsValue('job:MSN')
    job:MSN = p_web.GetValue('job:MSN')
    p_web.SetSessionValue('job:MSN',job:MSN)
  End
  if p_web.IfExistsValue('job:Charge_Type')
    job:Charge_Type = p_web.GetValue('job:Charge_Type')
    p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  End
  if p_web.IfExistsValue('job:Warranty_Charge_Type')
    job:Warranty_Charge_Type = p_web.GetValue('job:Warranty_Charge_Type')
    p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
  End
  if p_web.IfExistsValue('tmp:ExchangeIMEINumber')
    tmp:ExchangeIMEINumber = p_web.GetValue('tmp:ExchangeIMEINumber')
    p_web.SetSessionValue('tmp:ExchangeIMEINumber',tmp:ExchangeIMEINumber)
  End
  if p_web.IfExistsValue('locExchangeAlertMessage')
    locExchangeAlertMessage = p_web.GetValue('locExchangeAlertMessage')
    p_web.SetSessionValue('locExchangeAlertMessage',locExchangeAlertMessage)
  End
  if p_web.IfExistsValue('tmp:ExchangeDate')
    tmp:ExchangeDate = p_web.dformat(clip(p_web.GetValue('tmp:ExchangeDate')),'@d06')
    p_web.SetSessionValue('tmp:ExchangeDate',tmp:ExchangeDate)
  End
  if p_web.IfExistsValue('tmp:MSN')
    tmp:MSN = p_web.GetValue('tmp:MSN')
    p_web.SetSessionValue('tmp:MSN',tmp:MSN)
  End
  if p_web.IfExistsValue('tmp:ExchangeUnitDetails')
    tmp:ExchangeUnitDetails = p_web.GetValue('tmp:ExchangeUnitDetails')
    p_web.SetSessionValue('tmp:ExchangeUnitDetails',tmp:ExchangeUnitDetails)
  End
  if p_web.IfExistsValue('tmp:ExchangeAccessories')
    tmp:ExchangeAccessories = p_web.GetValue('tmp:ExchangeAccessories')
    p_web.SetSessionValue('tmp:ExchangeAccessories',tmp:ExchangeAccessories)
  End
  if p_web.IfExistsValue('tmp:ExchangeLocation')
    tmp:ExchangeLocation = p_web.GetValue('tmp:ExchangeLocation')
    p_web.SetSessionValue('tmp:ExchangeLocation',tmp:ExchangeLocation)
  End
  if p_web.IfExistsValue('tmp:HandsetPartNumber')
    tmp:HandsetPartNumber = p_web.GetValue('tmp:HandsetPartNumber')
    p_web.SetSessionValue('tmp:HandsetPartNumber',tmp:HandsetPartNumber)
  End
  if p_web.IfExistsValue('tmp:ReplacementValue')
    tmp:ReplacementValue = p_web.dformat(clip(p_web.GetValue('tmp:ReplacementValue')),'@n14.2')
    p_web.SetSessionValue('tmp:ReplacementValue',tmp:ReplacementValue)
  End
  if p_web.IfExistsValue('tmp:HandsetReplacementValue')
    tmp:HandsetReplacementValue = p_web.dformat(clip(p_web.GetValue('tmp:HandsetReplacementValue')),'@n14.2')
    p_web.SetSessionValue('tmp:HandsetReplacementValue',tmp:HandsetReplacementValue)
  End
  if p_web.IfExistsValue('tmp:NoUnitAvailable')
    tmp:NoUnitAvailable = p_web.GetValue('tmp:NoUnitAvailable')
    p_web.SetSessionValue('tmp:NoUnitAvailable',tmp:NoUnitAvailable)
  End
  if p_web.IfExistsValue('locKeepParts')
    locKeepParts = p_web.GetValue('locKeepParts')
    p_web.SetSessionValue('locKeepParts',locKeepParts)
  End
  if p_web.IfExistsValue('job:Exchange_Courier')
    job:Exchange_Courier = p_web.GetValue('job:Exchange_Courier')
    p_web.SetSessionValue('job:Exchange_Courier',job:Exchange_Courier)
  End
  if p_web.IfExistsValue('job:Exchange_Consignment_Number')
    job:Exchange_Consignment_Number = p_web.GetValue('job:Exchange_Consignment_Number')
    p_web.SetSessionValue('job:Exchange_Consignment_Number',job:Exchange_Consignment_Number)
  End
  if p_web.IfExistsValue('job:Exchange_Despatched')
    job:Exchange_Despatched = p_web.dformat(clip(p_web.GetValue('job:Exchange_Despatched')),'@d06b')
    p_web.SetSessionValue('job:Exchange_Despatched',job:Exchange_Despatched)
  End
  if p_web.IfExistsValue('job:Exchange_Despatched_User')
    job:Exchange_Despatched_User = p_web.GetValue('job:Exchange_Despatched_User')
    p_web.SetSessionValue('job:Exchange_Despatched_User',job:Exchange_Despatched_User)
  End
  if p_web.IfExistsValue('job:Exchange_Despatch_Number')
    job:Exchange_Despatch_Number = p_web.GetValue('job:Exchange_Despatch_Number')
    p_web.SetSessionValue('job:Exchange_Despatch_Number',job:Exchange_Despatch_Number)
  End
  if p_web.IfExistsValue('locRemovalAlertMessage')
    locRemovalAlertMessage = p_web.GetValue('locRemovalAlertMessage')
    p_web.SetSessionValue('locRemovalAlertMessage',locRemovalAlertMessage)
  End
  if p_web.IfExistsValue('tmp:RemovalReason')
    tmp:RemovalReason = p_web.GetValue('tmp:RemovalReason')
    p_web.SetSessionValue('tmp:RemovalReason',tmp:RemovalReason)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('PickExchangeUnit_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
      if (p_web.GSV('job:Loan_Unit_Number') > 0 And ((p_web.GSV('BookingSite') = 'RRC' And p_web.GSV('jobe:Despatched') = 'REA' And p_web.GSV('jobe:DespatchedType') = 'LOA') Or |
          (p_web.GSV('BookingSite') = 'ARC' And p_web.GSV('job:Despatched') = 'REA' And p_web.GSV('job:Despatch_Type') = 'LOA')))
          packet = clip(packet) & '<script language="JavaScript" type="text/javascript">alert("You cannot attached an Exchange Unit until the Loan Unit has been despatched.")</script>'
          do sendPacket
          p_web.SSV('ReadOnly:ExchangeIMEINumber',1)
      End !
  
      p_web.SSV('ReadOnly:ExchangeDespatchDetails',1)
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 tmp:ExchangeIMEINumber = p_web.RestoreValue('tmp:ExchangeIMEINumber')
 locExchangeAlertMessage = p_web.RestoreValue('locExchangeAlertMessage')
 tmp:ExchangeDate = p_web.RestoreValue('tmp:ExchangeDate')
 tmp:MSN = p_web.RestoreValue('tmp:MSN')
 tmp:ExchangeUnitDetails = p_web.RestoreValue('tmp:ExchangeUnitDetails')
 tmp:ExchangeAccessories = p_web.RestoreValue('tmp:ExchangeAccessories')
 tmp:ExchangeLocation = p_web.RestoreValue('tmp:ExchangeLocation')
 tmp:HandsetPartNumber = p_web.RestoreValue('tmp:HandsetPartNumber')
 tmp:ReplacementValue = p_web.RestoreValue('tmp:ReplacementValue')
 tmp:HandsetReplacementValue = p_web.RestoreValue('tmp:HandsetReplacementValue')
 tmp:NoUnitAvailable = p_web.RestoreValue('tmp:NoUnitAvailable')
 locKeepParts = p_web.RestoreValue('locKeepParts')
 locRemovalAlertMessage = p_web.RestoreValue('locRemovalAlertMessage')
 tmp:RemovalReason = p_web.RestoreValue('tmp:RemovalReason')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('ReturnURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('PickExchangeUnit_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('PickExchangeUnit_ChainTo')
    loc:formaction = p_web.GetSessionValue('PickExchangeUnit_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('ReturnURL')
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="PickExchangeUnit" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="PickExchangeUnit" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="PickExchangeUnit" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Pick Exchange Unit') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Pick Exchange Unit',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_PickExchangeUnit">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_PickExchangeUnit" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_PickExchangeUnit')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Job Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Exchange Unit Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Despatch Details') & ''''
        If p_web.GSV('job:Exchange_Unit_Number') > 0 AND p_web.GSV('ReadOnly:ExchangeIMEINumber') = 0
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Remove Exchange Unit') & ''''
        End
        Loc:Tabnumber = p_web.getSessionValue('showtab_PickExchangeUnit')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_PickExchangeUnit'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='EXCHANGE'
            p_web.SetValue('SelectField',clip(loc:formname) & '.job:Exchange_Consignment_Number')
    End
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab5'''
          If p_web.GSV('job:Exchange_Unit_Number') > 0 AND p_web.GSV('ReadOnly:ExchangeIMEINumber') = 0
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          End
          Loc:Tabnumber = p_web.getSessionValue('showtab_PickExchangeUnit')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab5'');'&CRLF
    if p_web.GSV('job:Exchange_Unit_Number') > 0 AND p_web.GSV('ReadOnly:ExchangeIMEINumber') = 0
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
    end
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Job Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PickExchangeUnit_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Job Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:ESN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:ESN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:ESN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:MSN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:MSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:MSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locUnitDetails
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locUnitDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locUnitDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Charge_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Warranty_Charge_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Warranty_Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Warranty_Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Exchange Unit Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PickExchangeUnit_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Exchange Unit Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Exchange Unit Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Exchange Unit Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Exchange Unit Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ExchangeIMEINumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ExchangeIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ExchangeIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& ' rowspan="4">'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locExchangeAlertMessage
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&' rowspan="4">'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locExchangeAlertMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&' rowspan="4">'
      loc:columncounter += 1
      do SendPacket
      do Comment::locExchangeAlertMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GSV('ReadOnly:ExchangeIMEINumber') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonPickExchangeUnit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonPickExchangeUnit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('tmp:ExchangeDate') > 0
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ExchangeDate
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ExchangeDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ExchangeDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('tmp:MSN') <> ''
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:MSN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:MSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:MSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ExchangeUnitDetails
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ExchangeUnitDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ExchangeUnitDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ExchangeAccessories
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ExchangeAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ExchangeAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ExchangeLocation
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ExchangeLocation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ExchangeLocation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:HandsetPartNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:HandsetPartNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:HandsetPartNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ReplacementValue
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ReplacementValue
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ReplacementValue
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:HandsetReplacementValue
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:HandsetReplacementValue
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:HandsetReplacementValue
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:NoUnitAvailable
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:NoUnitAvailable
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:NoUnitAvailable
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::txtKeepParts
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&' colspan="4">'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::txtKeepParts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&' colspan="4">'
      loc:columncounter += 1
      do SendPacket
      do Comment::txtKeepParts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locKeepParts
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locKeepParts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locKeepParts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel5">'&CRLF &|
                                    '  <div id="panel5Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Despatch Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel5Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PickExchangeUnit_5">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Despatch Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab5" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab5">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Despatch Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab5">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Despatch Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab5">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Despatch Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Exchange_Courier
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Exchange_Courier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Exchange_Courier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Exchange_Consignment_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Exchange_Consignment_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Exchange_Consignment_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Exchange_Despatched
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Exchange_Despatched
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Exchange_Despatched
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Exchange_Despatched_User
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Exchange_Despatched_User
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Exchange_Despatched_User
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Exchange_Despatch_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Exchange_Despatch_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Exchange_Despatch_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::button:AmendDespatchDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::button:AmendDespatchDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab3  Routine
  If p_web.GSV('job:Exchange_Unit_Number') > 0 AND p_web.GSV('ReadOnly:ExchangeIMEINumber') = 0
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Remove Exchange Unit') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PickExchangeUnit_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Remove Exchange Unit')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Remove Exchange Unit')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Remove Exchange Unit')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Remove Exchange Unit')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::locRemoveText
      do Comment::locRemoveText
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::buttonRemoveAttachedUnit
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonRemoveAttachedUnit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonRemoveAttachedUnit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& ' rowspan="5">'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locRemovalAlertMessage
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&' valign="top" rowspan="5">'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locRemovalAlertMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&' rowspan="5">'
      loc:columncounter += 1
      do SendPacket
      do Comment::locRemovalAlertMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:RemovalReason
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:RemovalReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:RemovalReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::buttonConfirmExchangeRemoval
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonConfirmExchangeRemoval
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonConfirmExchangeRemoval
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end


Prompt::job:ESN  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:ESN') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('IMEI Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:ESN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:ESN',p_web.GetValue('NewValue'))
    job:ESN = p_web.GetValue('NewValue') !FieldType= STRING Field = job:ESN
    do Value::job:ESN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:ESN',p_web.dFormat(p_web.GetValue('Value'),'@s20'))
    job:ESN = p_web.GetValue('Value')
  End

Value::job:ESN  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:ESN') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:ESN
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:ESN'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::job:ESN  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:ESN') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:MSN  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:MSN') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('M.S.N.')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:MSN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:MSN',p_web.GetValue('NewValue'))
    job:MSN = p_web.GetValue('NewValue') !FieldType= STRING Field = job:MSN
    do Value::job:MSN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:MSN',p_web.dFormat(p_web.GetValue('Value'),'@s20'))
    job:MSN = p_web.GetValue('Value')
  End

Value::job:MSN  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:MSN') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:MSN
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:MSN'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::job:MSN  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:MSN') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locUnitDetails  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('locUnitDetails') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Unit Details')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locUnitDetails  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locUnitDetails',p_web.GetValue('NewValue'))
    do Value::locUnitDetails
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::locUnitDetails  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('locUnitDetails') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web.Translate(p_web.GSV('job:Manufacturer') & ' ' & p_web.GSV('job:Model_Number') & ' - ' & p_web.GSV('job:Unit_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locUnitDetails  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('locUnitDetails') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Charge_Type  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Charge_Type') & '_prompt',Choose(p_web.GSV('job:Chargeable_Job') <> 'YES','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Charge Type')
  If p_web.GSV('job:Chargeable_Job') <> 'YES'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Charge_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Charge_Type',p_web.GetValue('NewValue'))
    job:Charge_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Charge_Type
    do Value::job:Charge_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Charge_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Charge_Type = p_web.GetValue('Value')
  End

Value::job:Charge_Type  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Charge_Type') & '_value',Choose(p_web.GSV('job:Chargeable_Job') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Chargeable_Job') <> 'YES')
  ! --- DISPLAY --- job:Charge_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Charge_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::job:Charge_Type  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Charge_Type') & '_comment',Choose(p_web.GSV('job:Chargeable_Job') <> 'YES','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Chargeable_Job') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Warranty_Charge_Type  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Warranty_Charge_Type') & '_prompt',Choose(p_web.GSV('job:Warranty_Job') <> 'YES','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Warr Charge Type')
  If p_web.GSV('job:Warranty_Job') <> 'YES'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Warranty_Charge_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Warranty_Charge_Type',p_web.GetValue('NewValue'))
    job:Warranty_Charge_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Warranty_Charge_Type
    do Value::job:Warranty_Charge_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Warranty_Charge_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Warranty_Charge_Type = p_web.GetValue('Value')
  End

Value::job:Warranty_Charge_Type  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Warranty_Charge_Type') & '_value',Choose(p_web.GSV('job:Warranty_Job') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Warranty_Job') <> 'YES')
  ! --- DISPLAY --- job:Warranty_Charge_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Warranty_Charge_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::job:Warranty_Charge_Type  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Warranty_Charge_Type') & '_comment',Choose(p_web.GSV('job:Warranty_Job') <> 'YES','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Warranty_Job') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ExchangeIMEINumber  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeIMEINumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Exchange IMEI No')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:ExchangeIMEINumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ExchangeIMEINumber',p_web.GetValue('NewValue'))
    tmp:ExchangeIMEINumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ExchangeIMEINumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ExchangeIMEINumber',p_web.GetValue('Value'))
    tmp:ExchangeIMEINumber = p_web.GetValue('Value')
  End
    tmp:ExchangeIMEINumber = Upper(tmp:ExchangeIMEINumber)
    p_web.SetSessionValue('tmp:ExchangeIMEINumber',tmp:ExchangeIMEINumber)
      ! Validate IMEI
      p_web.SSV('locExchangeAlertMessage','')
      Access:EXCHANGE.Clearkey(xch:ESN_Only_Key)
      xch:ESN    = p_web.GSV('tmp:ExchangeIMEINumber')
      if (Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign)
          ! Found
          if (xch:Location <> p_web.GSV('BookingSiteLocation'))
              p_web.SSV('locExchangeAlertMessage','Selected IMEI is from a different location.')
          else !  !if (xch:Location <> p_web.GSV('BookingSiteLocation'))
              if (xch:Available = 'AVL')
                  Access:STOCKTYP.Clearkey(stp:Stock_Type_Key)
                  stp:Stock_Type    = xch:Stock_Type
                  if (Access:STOCKTYP.TryFetch(stp:Stock_Type_Key) = Level:Benign)
                      ! Found
                      if (stp:Available <> 1)
                          p_web.SSV('locExchangeAlertMessage','Selected Stock Type is not available')
                      else ! if (stp:Available <> 1)
                          p_web.SSV('tmp:ExchangeUnitNumber',xch:Ref_Number)
                      end ! if (stp:Available <> 1)
                  else ! if (Access:STOCKTYP.TryFetch(stp:Stock_Type_Key) = Level:Benign)
                      ! Error
                  end ! if (Access:STOCKTYP.TryFetch(stp:Stock_Type_Key) = Level:Benign)
              else ! if (xch:Available = 'AVL')
                  p_web.SSV('locExchangeAlertMessage','Selected IMEI is not available')
              end ! if (xch:Available = 'AVL')
          end !if (xch:Location <> p_web.GSV('BookingSiteLocation'))
      else ! if (Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign)
          ! Error
          p_web.SSV('locExchangeAlertMessage','Cannot find the selected IMEI Number')
      end ! if (Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign)
  
      if (p_web.GSV('locExchangeAlertMessage') = '')
          GetExchangeDetails()
      else
          do clearExchangeDetails
      end ! if (p_web.GSV('locExchangeAlertMessage') = '')
  p_Web.SetValue('lookupfield','tmp:ExchangeIMEINumber')
  do AfterLookup
  do Value::tmp:ExchangeIMEINumber
  do SendAlert
  do Comment::tmp:ExchangeIMEINumber
  do Value::tmp:ExchangeLocation  !1
  do Value::tmp:ExchangeUnitDetails  !1
  do Value::tmp:ReplacementValue  !1
  do Prompt::tmp:HandsetPartNumber
  do Value::tmp:HandsetPartNumber  !1
  do Prompt::tmp:HandsetReplacementValue
  do Value::tmp:HandsetReplacementValue  !1
  do Prompt::tmp:ReplacementValue
  do Value::tmp:ReplacementValue  !1
  do Prompt::tmp:NoUnitAvailable
  do Value::tmp:NoUnitAvailable  !1
  do Prompt::locExchangeAlertMessage
  do Value::locExchangeAlertMessage  !1

Value::tmp:ExchangeIMEINumber  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeIMEINumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:ExchangeIMEINumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:ExchangeIMEINumber') = 1 OR p_web.GSV('job:Exchange_Unit_Number') > 0,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:ExchangeIMEINumber') = 1 OR p_web.GSV('job:Exchange_Unit_Number') > 0
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('tmp:ExchangeIMEINumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ExchangeIMEINumber'',''pickexchangeunit_tmp:exchangeimeinumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ExchangeIMEINumber')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','tmp:ExchangeIMEINumber',p_web.GetSessionValueFormat('tmp:ExchangeIMEINumber'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('FormExchangeUnitFilter')&'?LookupField=tmp:ExchangeIMEINumber&Tab=3&ForeignField=xch:ESN&_sort=xch:ESN&Refresh=sort&LookupFrom=PickExchangeUnit&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeIMEINumber') & '_value')

Comment::tmp:ExchangeIMEINumber  Routine
      loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeIMEINumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeIMEINumber') & '_comment')

Prompt::locExchangeAlertMessage  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('locExchangeAlertMessage') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('locExchangeAlertMessage') & '_prompt')

Validate::locExchangeAlertMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locExchangeAlertMessage',p_web.GetValue('NewValue'))
    locExchangeAlertMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locExchangeAlertMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locExchangeAlertMessage',p_web.GetValue('Value'))
    locExchangeAlertMessage = p_web.GetValue('Value')
  End

Value::locExchangeAlertMessage  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('locExchangeAlertMessage') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locExchangeAlertMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('red bold')&'">' & p_web.Translate(p_web.GSV('locExchangeAlertMessage'),1) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('locExchangeAlertMessage') & '_value')

Comment::locExchangeAlertMessage  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('locExchangeAlertMessage') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonPickExchangeUnit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonPickExchangeUnit',p_web.GetValue('NewValue'))
    do Value::buttonPickExchangeUnit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do Value::buttonPickExchangeUnit
  do SendAlert
  do Value::tmp:HandsetPartNumber  !1

Value::buttonPickExchangeUnit  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('buttonPickExchangeUnit') & '_value',Choose(1 or p_web.GSV('job:Exchange_Unit_Number') > 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (1 or p_web.GSV('job:Exchange_Unit_Number') > 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonPickExchangeUnit'',''pickexchangeunit_buttonpickexchangeunit_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PickExchangeUnit','Pick Exchange Unit','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormExchangeUnitFilter?LookupField=tmp:ExchangeIMEINumber&Tab=2&ForeignField=xch:ESN&_sort=xch:ESN&Refresh=sort&LookupFrom=PickExchangeUnit&')) & ''','''&clip('_self')&''')',loc:javascript,0,'images\packinsert.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('buttonPickExchangeUnit') & '_value')

Comment::buttonPickExchangeUnit  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('buttonPickExchangeUnit') & '_comment',Choose(1 or p_web.GSV('job:Exchange_Unit_Number') > 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If 1 or p_web.GSV('job:Exchange_Unit_Number') > 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ExchangeDate  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeDate') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Exchange Date:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:ExchangeDate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ExchangeDate',p_web.GetValue('NewValue'))
    tmp:ExchangeDate = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ExchangeDate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ExchangeDate',p_web.dFormat(p_web.GetValue('Value'),'@d06'))
    tmp:ExchangeDate = p_web.Dformat(p_web.GetValue('Value'),'@d06') !
  End

Value::tmp:ExchangeDate  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeDate') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- tmp:ExchangeDate
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(format(p_web.GetSessionValue('tmp:ExchangeDate'),'@d06')) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeDate') & '_value')

Comment::tmp:ExchangeDate  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeDate') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:MSN  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:MSN') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('M.S.N.')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:MSN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:MSN',p_web.GetValue('NewValue'))
    tmp:MSN = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:MSN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:MSN',p_web.GetValue('Value'))
    tmp:MSN = p_web.GetValue('Value')
  End

Value::tmp:MSN  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:MSN') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- tmp:MSN
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('tmp:MSN'),) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('tmp:MSN') & '_value')

Comment::tmp:MSN  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:MSN') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ExchangeUnitDetails  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeUnitDetails') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Unit Details')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:ExchangeUnitDetails  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ExchangeUnitDetails',p_web.GetValue('NewValue'))
    tmp:ExchangeUnitDetails = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ExchangeUnitDetails
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ExchangeUnitDetails',p_web.GetValue('Value'))
    tmp:ExchangeUnitDetails = p_web.GetValue('Value')
  End

Value::tmp:ExchangeUnitDetails  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeUnitDetails') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- tmp:ExchangeUnitDetails
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('tmp:ExchangeUnitDetails'),) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeUnitDetails') & '_value')

Comment::tmp:ExchangeUnitDetails  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeUnitDetails') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ExchangeAccessories  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeAccessories') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Accessories')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:ExchangeAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ExchangeAccessories',p_web.GetValue('NewValue'))
    tmp:ExchangeAccessories = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ExchangeAccessories
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ExchangeAccessories',p_web.GetValue('Value'))
    tmp:ExchangeAccessories = p_web.GetValue('Value')
  End

Value::tmp:ExchangeAccessories  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeAccessories') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- tmp:ExchangeAccessories
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('tmp:ExchangeAccessories'),) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeAccessories') & '_value')

Comment::tmp:ExchangeAccessories  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeAccessories') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ExchangeLocation  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeLocation') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Location / Stock Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:ExchangeLocation  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ExchangeLocation',p_web.GetValue('NewValue'))
    tmp:ExchangeLocation = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ExchangeLocation
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ExchangeLocation',p_web.GetValue('Value'))
    tmp:ExchangeLocation = p_web.GetValue('Value')
  End

Value::tmp:ExchangeLocation  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeLocation') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- tmp:ExchangeLocation
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('tmp:ExchangeLocation'),) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeLocation') & '_value')

Comment::tmp:ExchangeLocation  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeLocation') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:HandsetPartNumber  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:HandsetPartNumber') & '_prompt',Choose(p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Handset Part No')
  If p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('tmp:HandsetPartNumber') & '_prompt')

Validate::tmp:HandsetPartNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:HandsetPartNumber',p_web.GetValue('NewValue'))
    tmp:HandsetPartNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:HandsetPartNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:HandsetPartNumber',p_web.GetValue('Value'))
    tmp:HandsetPartNumber = p_web.GetValue('Value')
  End
  Access:PRODCODE.Clearkey(prd:ModelProductKey)
  prd:ModelNumber    = p_web.GSV('tmp:ExchangeModelNumber')
  prd:ProductCode    = p_web.GSV('tmp:HandsetPartNumber')
  if (Access:PRODCODE.TryFetch(prd:ModelProductKey) = Level:Benign)
      ! Found
      p_web.SSV('tmp:HandsetReplacementValue',prd:HandsetReplacementValue)
  else ! if (Access:PRODCODE.TryFetch(prd:ModelProductKey) = Level:Benign)
      ! Error
      p_web.SSV('tmp:HandsetReplacementValue','')
  end ! if (Access:PRODCODE.TryFetch(prd:ModelProductKey) = Level:Benign)
  do Value::tmp:HandsetPartNumber
  do SendAlert
  do Value::tmp:HandsetReplacementValue  !1

Value::tmp:HandsetPartNumber  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:HandsetPartNumber') & '_value',Choose(p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1)
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:HandsetPartNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:HandsetPartNumber'',''pickexchangeunit_tmp:handsetpartnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:HandsetPartNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('ReadOnly:ExchangeIMEINumber') = 1,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:HandsetPartNumber',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:HandsetPartNumber') = 0
    p_web.SetSessionValue('tmp:HandsetPartNumber','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('tmp:HandsetPartNumber')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(AUDIT2)
  bind(aud2:Record)
  p_web._OpenFile(STOCKALL)
  bind(stl:Record)
  p_web._OpenFile(JOBNOTES)
  bind(jbn:Record)
  p_web._OpenFile(JOBSE2)
  bind(jobe2:Record)
  p_web._OpenFile(SMSRECVD)
  bind(SMR:Record)
  p_web._OpenFile(COURIER)
  bind(cou:Record)
  p_web._OpenFile(JOBS)
  bind(job:Record)
  p_web._OpenFile(EXCHANGE)
  bind(xch:Record)
  p_web._OpenFile(JOBEXACC)
  bind(jea:Record)
  p_web._OpenFile(EXCHANGE_ALIAS)
  bind(xch_ali:Record)
  p_web._OpenFile(EXCHOR48)
  bind(ex4:Record)
  p_web._OpenFile(MANUFACT)
  bind(man:Record)
  p_web._OpenFile(PARTS)
  bind(par:Record)
  p_web._OpenFile(STOCK)
  bind(sto:Record)
  p_web._OpenFile(WARPARTS)
  bind(wpr:Record)
  p_web._OpenFile(STOCKTYP)
  bind(stp:Record)
  p_web._OpenFile(EXCHHIST)
  bind(exh:Record)
  p_web._OpenFile(PRODCODE)
  bind(prd:Record)
  p_web._OpenFile(AUDIT)
  bind(aud:Record)
  p_web._OpenFile(JOBSE)
  bind(jobe:Record)
  p_web._OpenFile(WEBJOB)
  bind(wob:Record)
  p_web._OpenFile(TRDBATCH)
  bind(trb:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(tmp:HandsetPartNumber_OptionView)
  tmp:HandsetPartNumber_OptionView{prop:filter} = 'Upper(prd:ModelNumber) = Upper('''  & p_web.GSV('tmp:ExchangeModelNumber') & ''')'
  tmp:HandsetPartNumber_OptionView{prop:order} = 'UPPER(prd:ProductCode)'
  Set(tmp:HandsetPartNumber_OptionView)
  Loop
    Next(tmp:HandsetPartNumber_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('tmp:HandsetPartNumber') = 0
      p_web.SetSessionValue('tmp:HandsetPartNumber',prd:ProductCode)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(clip(prd:ProductCode) & '  (' & format(prd:HandsetReplacementValue,@n_14.2) & '  )',prd:ProductCode,choose(prd:ProductCode = p_web.getsessionvalue('tmp:HandsetPartNumber')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(tmp:HandsetPartNumber_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(AUDIT2)
  p_Web._CloseFile(STOCKALL)
  p_Web._CloseFile(JOBNOTES)
  p_Web._CloseFile(JOBSE2)
  p_Web._CloseFile(SMSRECVD)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(JOBEXACC)
  p_Web._CloseFile(EXCHANGE_ALIAS)
  p_Web._CloseFile(EXCHOR48)
  p_Web._CloseFile(MANUFACT)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(STOCKTYP)
  p_Web._CloseFile(EXCHHIST)
  p_Web._CloseFile(PRODCODE)
  p_Web._CloseFile(AUDIT)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(TRDBATCH)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('tmp:HandsetPartNumber') & '_value')

Comment::tmp:HandsetPartNumber  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:HandsetPartNumber') & '_comment',Choose(p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ReplacementValue  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ReplacementValue') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Replacement Value')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('tmp:ReplacementValue') & '_prompt')

Validate::tmp:ReplacementValue  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ReplacementValue',p_web.GetValue('NewValue'))
    tmp:ReplacementValue = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ReplacementValue
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ReplacementValue',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:ReplacementValue = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End

Value::tmp:ReplacementValue  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ReplacementValue') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- tmp:ReplacementValue
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(format(p_web.GetSessionValue('tmp:ReplacementValue'),'@n14.2')) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('tmp:ReplacementValue') & '_value')

Comment::tmp:ReplacementValue  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ReplacementValue') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:HandsetReplacementValue  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:HandsetReplacementValue') & '_prompt',Choose(p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Replacement Value')
  If p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('tmp:HandsetReplacementValue') & '_prompt')

Validate::tmp:HandsetReplacementValue  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:HandsetReplacementValue',p_web.GetValue('NewValue'))
    tmp:HandsetReplacementValue = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:HandsetReplacementValue
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:HandsetReplacementValue',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:HandsetReplacementValue = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:HandsetReplacementValue
  do SendAlert

Value::tmp:HandsetReplacementValue  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:HandsetReplacementValue') & '_value',Choose(p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1)
  ! --- STRING --- tmp:HandsetReplacementValue
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tmp:HandsetReplacementValue')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:HandsetReplacementValue'',''pickexchangeunit_tmp:handsetreplacementvalue_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:HandsetReplacementValue',p_web.GetSessionValue('tmp:HandsetReplacementValue'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('tmp:HandsetReplacementValue') & '_value')

Comment::tmp:HandsetReplacementValue  Routine
      loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:HandsetReplacementValue') & '_comment',Choose(p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:NoUnitAvailable  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:NoUnitAvailable') & '_prompt',Choose(p_web.GSV('tmp:ExchangeUnitNumber') > 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('No Unit Available')
  If p_web.GSV('tmp:ExchangeUnitNumber') > 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('tmp:NoUnitAvailable') & '_prompt')

Validate::tmp:NoUnitAvailable  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:NoUnitAvailable',p_web.GetValue('NewValue'))
    tmp:NoUnitAvailable = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:NoUnitAvailable
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('tmp:NoUnitAvailable',p_web.GetValue('Value'))
    tmp:NoUnitAvailable = p_web.GetValue('Value')
  End

Value::tmp:NoUnitAvailable  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:NoUnitAvailable') & '_value',Choose(p_web.GSV('tmp:ExchangeUnitNumber') > 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('tmp:ExchangeUnitNumber') > 0)
  ! --- CHECKBOX --- tmp:NoUnitAvailable
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('ReadOnly:ExchangeIMEINumber') = 1 OR p_web.GSV('job:Exchange_Unit_Number') > 0,'disabled','')
  If p_web.GetSessionValue('tmp:NoUnitAvailable') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:NoUnitAvailable',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('tmp:NoUnitAvailable') & '_value')

Comment::tmp:NoUnitAvailable  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:NoUnitAvailable') & '_comment',Choose(p_web.GSV('tmp:ExchangeUnitNumber') > 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:ExchangeUnitNumber') > 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::txtKeepParts  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('txtKeepParts') & '_prompt',Choose(p_web.GSV('Hide:KeepParts') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('Hide:KeepParts') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::txtKeepParts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtKeepParts',p_web.GetValue('NewValue'))
    do Value::txtKeepParts
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtKeepParts  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('txtKeepParts') & '_value',Choose(p_web.GSV('Hide:KeepParts') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:KeepParts') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('red bold')&'">' & p_web.Translate('This job has Warranty parts attached.<br/>Do you wish to keep the parts attached to the job when it is sent to the ARC?',1) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::txtKeepParts  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('txtKeepParts') & '_comment',Choose(p_web.GSV('Hide:KeepParts') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:KeepParts') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locKeepParts  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('locKeepParts') & '_prompt',Choose(p_web.GSV('Hide:KeepParts') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Keep Parts')
  If p_web.GSV('Hide:KeepParts') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locKeepParts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locKeepParts',p_web.GetValue('NewValue'))
    locKeepParts = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locKeepParts
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locKeepParts',p_web.GetValue('Value'))
    locKeepParts = p_web.GetValue('Value')
  End
  do Value::locKeepParts
  do SendAlert

Value::locKeepParts  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('locKeepParts') & '_value',Choose(p_web.GSV('Hide:KeepParts') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:KeepParts') = 1)
  ! --- RADIO --- locKeepParts
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locKeepParts')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locKeepParts') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locKeepParts'',''pickexchangeunit_lockeepparts_value'',1,'''&clip(1)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locKeepParts',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locKeepParts_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Yes') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locKeepParts') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locKeepParts'',''pickexchangeunit_lockeepparts_value'',1,'''&clip(2)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locKeepParts',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locKeepParts_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('No') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('locKeepParts') & '_value')

Comment::locKeepParts  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('locKeepParts') & '_comment',Choose(p_web.GSV('Hide:KeepParts') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:KeepParts') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Exchange_Courier  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Courier') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Courier')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Exchange_Courier  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Exchange_Courier',p_web.GetValue('NewValue'))
    job:Exchange_Courier = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Exchange_Courier
    do Value::job:Exchange_Courier
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Exchange_Courier',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Exchange_Courier = p_web.GetValue('Value')
  End
    job:Exchange_Courier = Upper(job:Exchange_Courier)
    p_web.SetSessionValue('job:Exchange_Courier',job:Exchange_Courier)
  do Value::job:Exchange_Courier
  do SendAlert

Value::job:Exchange_Courier  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Courier') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('job:Exchange_Courier')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Exchange_Courier'',''pickexchangeunit_job:exchange_courier_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('job:Exchange_Courier',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('job:Exchange_Courier') = 0
    p_web.SetSessionValue('job:Exchange_Courier','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('job:Exchange_Courier')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(AUDIT2)
  bind(aud2:Record)
  p_web._OpenFile(STOCKALL)
  bind(stl:Record)
  p_web._OpenFile(JOBNOTES)
  bind(jbn:Record)
  p_web._OpenFile(JOBSE2)
  bind(jobe2:Record)
  p_web._OpenFile(SMSRECVD)
  bind(SMR:Record)
  p_web._OpenFile(COURIER)
  bind(cou:Record)
  p_web._OpenFile(JOBS)
  bind(job:Record)
  p_web._OpenFile(EXCHANGE)
  bind(xch:Record)
  p_web._OpenFile(JOBEXACC)
  bind(jea:Record)
  p_web._OpenFile(EXCHANGE_ALIAS)
  bind(xch_ali:Record)
  p_web._OpenFile(EXCHOR48)
  bind(ex4:Record)
  p_web._OpenFile(MANUFACT)
  bind(man:Record)
  p_web._OpenFile(PARTS)
  bind(par:Record)
  p_web._OpenFile(STOCK)
  bind(sto:Record)
  p_web._OpenFile(WARPARTS)
  bind(wpr:Record)
  p_web._OpenFile(STOCKTYP)
  bind(stp:Record)
  p_web._OpenFile(EXCHHIST)
  bind(exh:Record)
  p_web._OpenFile(PRODCODE)
  bind(prd:Record)
  p_web._OpenFile(AUDIT)
  bind(aud:Record)
  p_web._OpenFile(JOBSE)
  bind(jobe:Record)
  p_web._OpenFile(WEBJOB)
  bind(wob:Record)
  p_web._OpenFile(TRDBATCH)
  bind(trb:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(job:Exchange_Courier_OptionView)
  job:Exchange_Courier_OptionView{prop:order} = 'UPPER(cou:Courier)'
  Set(job:Exchange_Courier_OptionView)
  Loop
    Next(job:Exchange_Courier_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('job:Exchange_Courier') = 0
      p_web.SetSessionValue('job:Exchange_Courier',cou:Courier)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,cou:Courier,choose(cou:Courier = p_web.getsessionvalue('job:Exchange_Courier')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(job:Exchange_Courier_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(AUDIT2)
  p_Web._CloseFile(STOCKALL)
  p_Web._CloseFile(JOBNOTES)
  p_Web._CloseFile(JOBSE2)
  p_Web._CloseFile(SMSRECVD)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(JOBEXACC)
  p_Web._CloseFile(EXCHANGE_ALIAS)
  p_Web._CloseFile(EXCHOR48)
  p_Web._CloseFile(MANUFACT)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(STOCKTYP)
  p_Web._CloseFile(EXCHHIST)
  p_Web._CloseFile(PRODCODE)
  p_Web._CloseFile(AUDIT)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(TRDBATCH)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Courier') & '_value')

Comment::job:Exchange_Courier  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Courier') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Exchange_Consignment_Number  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Consignment_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Consignment Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Exchange_Consignment_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Exchange_Consignment_Number',p_web.GetValue('NewValue'))
    job:Exchange_Consignment_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Exchange_Consignment_Number
    do Value::job:Exchange_Consignment_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Exchange_Consignment_Number',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Exchange_Consignment_Number = p_web.GetValue('Value')
  End

Value::job:Exchange_Consignment_Number  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Consignment_Number') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Exchange_Consignment_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:ExchangeDespatchDetails') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:ExchangeDespatchDetails') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Exchange_Consignment_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Exchange_Consignment_Number',p_web.GetSessionValueFormat('job:Exchange_Consignment_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Consignment_Number') & '_value')

Comment::job:Exchange_Consignment_Number  Routine
      loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Consignment_Number') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Exchange_Despatched  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Despatched') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Date')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Exchange_Despatched  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Exchange_Despatched',p_web.GetValue('NewValue'))
    job:Exchange_Despatched = p_web.GetValue('NewValue') !FieldType= DATE Field = job:Exchange_Despatched
    do Value::job:Exchange_Despatched
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Exchange_Despatched',p_web.dFormat(p_web.GetValue('Value'),'@d06b'))
    job:Exchange_Despatched = p_web.Dformat(p_web.GetValue('Value'),'@d06b') !
  End

Value::job:Exchange_Despatched  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Despatched') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Exchange_Despatched
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:ExchangeDespatchDetails') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:ExchangeDespatchDetails') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Exchange_Despatched')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Exchange_Despatched',p_web.GetSessionValue('job:Exchange_Despatched'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06b',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Despatched') & '_value')

Comment::job:Exchange_Despatched  Routine
      loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Despatched') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Exchange_Despatched_User  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Despatched_User') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('User')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Exchange_Despatched_User  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Exchange_Despatched_User',p_web.GetValue('NewValue'))
    job:Exchange_Despatched_User = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Exchange_Despatched_User
    do Value::job:Exchange_Despatched_User
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Exchange_Despatched_User',p_web.dFormat(p_web.GetValue('Value'),'@s3'))
    job:Exchange_Despatched_User = p_web.GetValue('Value')
  End

Value::job:Exchange_Despatched_User  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Despatched_User') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Exchange_Despatched_User
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:ExchangeDespatchDetails') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:ExchangeDespatchDetails') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Exchange_Despatched_User')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Exchange_Despatched_User',p_web.GetSessionValueFormat('job:Exchange_Despatched_User'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s3'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Despatched_User') & '_value')

Comment::job:Exchange_Despatched_User  Routine
      loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Despatched_User') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Exchange_Despatch_Number  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Despatch_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Despatch Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Exchange_Despatch_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Exchange_Despatch_Number',p_web.GetValue('NewValue'))
    job:Exchange_Despatch_Number = p_web.GetValue('NewValue') !FieldType= LONG Field = job:Exchange_Despatch_Number
    do Value::job:Exchange_Despatch_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Exchange_Despatch_Number',p_web.dFormat(p_web.GetValue('Value'),'@s8'))
    job:Exchange_Despatch_Number = p_web.GetValue('Value')
  End

Value::job:Exchange_Despatch_Number  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Despatch_Number') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Exchange_Despatch_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:ExchangeDespatchDetails') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:ExchangeDespatchDetails') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Exchange_Despatch_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Exchange_Despatch_Number',p_web.GetSessionValueFormat('job:Exchange_Despatch_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s8'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Despatch_Number') & '_value')

Comment::job:Exchange_Despatch_Number  Routine
      loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Despatch_Number') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::button:AmendDespatchDetails  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('button:AmendDespatchDetails',p_web.GetValue('NewValue'))
    do Value::button:AmendDespatchDetails
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
      if (p_web.GSV('ReadOnly:ExchangeDespatchDetails') = 1)
          p_web.SSV('ReadOnly:ExchangeDespatchDetails',0)
      ELSE
          p_web.SSV('ReadOnly:ExchangeDespatchDetails',1)
      END
  do Value::button:AmendDespatchDetails
  do SendAlert
  do Value::job:Exchange_Consignment_Number  !1
  do Value::job:Exchange_Despatch_Number  !1
  do Value::job:Exchange_Despatched  !1
  do Value::job:Exchange_Despatched_User  !1

Value::button:AmendDespatchDetails  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('button:AmendDespatchDetails') & '_value',Choose(p_web.GSV('job:Exchange_Consignment_Number') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Exchange_Consignment_Number') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''button:AmendDespatchDetails'',''pickexchangeunit_button:amenddespatchdetails_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','AmendDespatchDetails','Amend Despatch Details','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('button:AmendDespatchDetails') & '_value')

Comment::button:AmendDespatchDetails  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('button:AmendDespatchDetails') & '_comment',Choose(p_web.GSV('job:Exchange_Consignment_Number') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Exchange_Consignment_Number') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::locRemoveText  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locRemoveText',p_web.GetValue('NewValue'))
    do Value::locRemoveText
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::locRemoveText  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('locRemoveText') & '_value',Choose(p_web.GSV('job:exchange_unit_Number') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:exchange_unit_Number') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('red bold')&'">' & p_web.Translate('You must remove the existing exchange before you can add a new one',) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('locRemoveText') & '_value')

Comment::locRemoveText  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('locRemoveText') & '_comment',Choose(p_web.GSV('job:exchange_unit_Number') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:exchange_unit_Number') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::buttonRemoveAttachedUnit  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('buttonRemoveAttachedUnit') & '_prompt',Choose(p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Exchange_Unit_Number') > 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Exchange_Unit_Number') > 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::buttonRemoveAttachedUnit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonRemoveAttachedUnit',p_web.GetValue('NewValue'))
    do Value::buttonRemoveAttachedUnit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  p_web.SSV('Hide:RemovalReason',0)
  
  if (p_web.GSV('job:exchange_Consignment_Number') <> '')
      p_web.SSV('locRemoveAlertMessage','Warning!<br>The Exchange Unit has already been despatched. '&|
                          'If the continue the unit will be removed and returned to Exchange Stock.')
  end ! if (p_web.GSV('job:exchange_Consignment_Number') <> '')
  do SendAlert
  do Prompt::tmp:RemovalReason
  do Value::tmp:RemovalReason  !1
  do Value::buttonConfirmExchangeRemoval  !1
  do Value::buttonRemoveAttachedUnit  !1
  do Prompt::locRemovalAlertMessage
  do Value::locRemovalAlertMessage  !1

Value::buttonRemoveAttachedUnit  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('buttonRemoveAttachedUnit') & '_value',Choose(p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Exchange_Unit_Number') > 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Exchange_Unit_Number') > 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonRemoveAttachedUnit'',''pickexchangeunit_buttonremoveattachedunit_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','RemoveExchangeUnit','Remove Exchange Unit','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('buttonRemoveAttachedUnit') & '_value')

Comment::buttonRemoveAttachedUnit  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('buttonRemoveAttachedUnit') & '_comment',Choose(p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Exchange_Unit_Number') > 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Exchange_Unit_Number') > 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locRemovalAlertMessage  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('locRemovalAlertMessage') & '_prompt',Choose(p_web.GSV('locRemovalAlertMessage') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Alert')
  If p_web.GSV('locRemovalAlertMessage') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('locRemovalAlertMessage') & '_prompt')

Validate::locRemovalAlertMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locRemovalAlertMessage',p_web.GetValue('NewValue'))
    locRemovalAlertMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locRemovalAlertMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locRemovalAlertMessage',p_web.GetValue('Value'))
    locRemovalAlertMessage = p_web.GetValue('Value')
  End
  do Value::locRemovalAlertMessage
  do SendAlert

Value::locRemovalAlertMessage  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('locRemovalAlertMessage') & '_value',Choose(p_web.GSV('locRemovalAlertMessage') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locRemovalAlertMessage') = '')
  ! --- TEXT --- locRemovalAlertMessage
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locRemovalAlertMessage')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locRemovalAlertMessage'',''pickexchangeunit_locremovalalertmessage_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'readonly'
  packet = clip(packet) & p_web.CreateTextArea('locRemovalAlertMessage',p_web.GetSessionValue('locRemovalAlertMessage'),5,25,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(locRemovalAlertMessage),,Net:Web:Frame) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('locRemovalAlertMessage') & '_value')

Comment::locRemovalAlertMessage  Routine
      loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('locRemovalAlertMessage') & '_comment',Choose(p_web.GSV('locRemovalAlertMessage') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locRemovalAlertMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:RemovalReason  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:RemovalReason') & '_prompt',Choose(p_web.GSV('Hide:RemovalReason') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Removal Reason')
  If p_web.GSV('Hide:RemovalReason') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('tmp:RemovalReason') & '_prompt')

Validate::tmp:RemovalReason  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:RemovalReason',p_web.GetValue('NewValue'))
    tmp:RemovalReason = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:RemovalReason
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:RemovalReason',p_web.GetValue('Value'))
    tmp:RemovalReason = p_web.GetValue('Value')
  End
  do Value::tmp:RemovalReason
  do SendAlert
  do Value::buttonConfirmExchangeRemoval  !1

Value::tmp:RemovalReason  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:RemovalReason') & '_value',Choose(p_web.GSV('Hide:RemovalReason') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:RemovalReason') = 1)
  ! --- RADIO --- tmp:RemovalReason
  loc:fieldclass = Choose(sub(' noButton',1,1) = ' ',clip('FormEntry') & ' noButton',' noButton')
  If lower(loc:invalid) = lower('tmp:RemovalReason')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If loc:readonly = '' and (p_web.GSV('jobe:Engineer48HourOption') = 1) then loc:readonly = 'disabled'.
    if p_web.GetSessionValue('tmp:RemovalReason') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:RemovalReason'',''pickexchangeunit_tmp:removalreason_value'',1,'''&clip(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:RemovalReason')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:RemovalReason',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:RemovalReason_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Replace Faulty Unit') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If loc:readonly = '' and (p_web.GSV('jobe:Engineer48HourOption') = 1) then loc:readonly = 'disabled'.
    if p_web.GetSessionValue('tmp:RemovalReason') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:RemovalReason'',''pickexchangeunit_tmp:removalreason_value'',1,'''&clip(2)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:RemovalReason')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:RemovalReason',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:RemovalReason_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Alternative Model Required') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:RemovalReason') = 3
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:RemovalReason'',''pickexchangeunit_tmp:removalreason_value'',1,'''&clip(3)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:RemovalReason')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:RemovalReason',clip(3),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:RemovalReason_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Restock Unit') & '<13,10>'
    packet = clip(packet) & p_web.br
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('tmp:RemovalReason') & '_value')

Comment::tmp:RemovalReason  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:RemovalReason') & '_comment',Choose(p_web.GSV('Hide:RemovalReason') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:RemovalReason') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::buttonConfirmExchangeRemoval  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('buttonConfirmExchangeRemoval') & '_prompt',Choose(p_web.GSV('Hide:RemovalReason') = 1 Or p_web.GSV('tmp:RemovalReason') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('Hide:RemovalReason') = 1 Or p_web.GSV('tmp:RemovalReason') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::buttonConfirmExchangeRemoval  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonConfirmExchangeRemoval',p_web.GetValue('NewValue'))
    do Value::buttonConfirmExchangeRemoval
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
      ! Remove Exchange Unit
      p_web.SSV('AddToAudit:Type','EXC')
      if p_web.GSV('job:Exchange_Consignemnt_Number') > 0
          p_web.SSV('AddToAudit:Notes','UNIT HAD BEEN DESPATCHED: ' & |
              '<13,10>UNIT NUMBER: ' & p_web.GSV('job:Exchange_unit_number') & |
              '<13,10>COURIER: ' & p_web.GSV('job:exchange_Courier') & |
              '<13,10>CONSIGNMENT NUMBER: ' & p_web.GSV('job:exchange_consignment_number') & |
              '<13,10>DATE DESPATCHED: ' & Format(p_web.GSV('job:Exchange_despatched'),@d6) &|
              '<13,10>DESPATCH USER: ' & p_web.GSV('job:Exchange_despatched_user') &|
              '<13,10>DESPATCH NUMBER: ' & p_web.GSV('job:exchange_despatch_number'))
      else ! if p_web.GSV('job:Exchange_Consignemnt_Number') > 0
          p_web.SSV('AddToAudit:Notes','UNIT NUMBER: ' & p_web.GSV('job:exchange_unit_Number'))
      end !if p_web.GSV('job:Exchange_Consignemnt_Number') > 0
      case p_web.GSV('tmp:RemovalReason')
      of 1
          p_web.SSV('AddToAudit:Action','REPLACED FAULTY EXCHANGE UNIT')
      of 2
          p_web.SSV('AddToAudit:Action','ALTERNATIVE EXCHANGE MODEL REQUIRED')
      of 3
          p_web.SSV('AddToAudit:Action','EXCHANGE UNIT NOT REQUIRED: RE-STOCKED')
      end ! case p_web.GSV('tmp:RemovalReason')
  
      addToAudit(p_web,p_web.GSV('job:Ref_Number'),'EXC',p_web.GSV('AddToAudit:Action'),p_web.GSV('AddToAudit:Notes'))
  
      !p_web.SSV('job:Exchange_Unit_Number','')
      p_web.SSV('job:Exchange_Accessory','')
      p_web.SSV('job:Exchange_Consignment_Number','')
      p_web.SSV('job:Exchange_Despatched','')
      p_web.SSV('job:exchange_Despatched_User','')
      p_web.SSV('job:Exchange_Issued_Date','')
      p_web.SSV('job:Exchange_User','')
      if (p_web.GSV('job:Despatch_Type') = 'EXC')
          p_web.SSV('job:Despatched','NO')
          p_web.SSV('job:Despatch_type','')
      else ! if (p_web.GSV('job:Despatch_Type') = 'EXC')
          if (p_web.GSV('jobe:DespatchType') = 'EXC')
              p_web.SSV('jobe:DespatchType','')
              p_web.SSV('wob:ReadyToDespatch',0)
          end ! if (p_web.GSV('jobe:DespatchType') = 'EXC')
      end !
  
  
      getStatus(101,0,'EXC',p_web)
  
      restock# = 0
      Access:PARTS.Clearkey(par:Part_Number_Key)
      par:Ref_Number    = p_web.GSV('job:Ref_Number')
      par:Part_Number    = 'EXCH'
      SET(par:Part_Number_Key,par:part_Number_key)
      loop
          if (Access:parts.Next())
              break
          end
          if (par:Ref_Number <> p_web.GSV('job:Ref_Number'))
              break
          end
          if (par:Part_Number <> 'EXCH')
              break
          end
          ! Found
          removeFromStockAllocation(par:Record_Number,'CHA')
          access:PARTS.deleterecord(0)
          restock# = 1
      end ! if (Access:PARTS.TryFetch(par:Part_Number_Key) = Level:Benign)
  
      Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
      wpr:Ref_Number = p_web.GSV('job:Ref_Number')
      wpr:Part_Number  = 'EXCH'
      SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
      loop
          if (Access:WARPARTS.Next())
              break
          end
          if (wpr:Ref_Number <> p_web.GSV('job:Ref_Number'))
              break
          end
          if (wpr:Part_Number <> 'EXCH')
              break
          end
  
          RemoveFromStockAllocation(wpr:Record_Number,'WAR')
          Access:WARPARTS.Deleterecord(0)
          restock# = 1
      end
  
      ! Remove incoming unit
      Access:EXCHANGE.Clearkey(xch:ESN_Only_Key)
      xch:ESN    = p_web.GSV('job:ESN')
      set(xch:ESN_Only_Key,xch:ESN_Only_Key)
      loop
          if (Access:EXCHANGE.Next())
              Break
          end ! if (Access:EXCHANGE.Next())
          if (xch:ESN    <> p_web.GSV('job:ESN'))
              Break
          end ! if (xch:ESN    <> p_web.GSV('job:ESN'))
          if (xch:Job_Number = p_web.GSV('job:Ref_Number'))
              relate:EXCHANGE.delete(0)
          end ! if (xch:Job_Number = p_web.GSV('job:Ref_Number'))
      end ! loop
  
  !    ! Dont think this is necessary?!
  !    if (restock# = 1)
  !        AllocateExchangePart('RTS',0)
  !    end ! if (restock# = 1)
  
  
      Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
      xch:Ref_Number    = p_web.GSV('job:Exchange_Unit_Number')
      if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
          ! Found
          if (p_web.GSV('tmp:RemovalReason') = 1)
                xch:Available = 'INR'
                xch:StatusChangeDate = TODAY() ! #12127 Record status change. (Bryan: 13/07/2011)
              p_web.SSV('locExchangeAlertMessage','Please book the replaced Exchange Unit as a new job')
          else ! if (p_web.GSV('tmp:RemovalReason') = 1)
              if (xch:Available <> 'QAF')
                    xch:available = 'AVL'
                    xch:StatusChangeDate = TODAY() ! #12127 Record status change. (Bryan: 13/07/2011)
              end ! if (xch:Available <> 'QAF')
          end ! if (p_web.GSV('tmp:RemovalReason') = 1)
          xch:Job_Number = ''
          access:EXCHANGE.tryUpdate()
          if (Access:EXCHHIST.PrimeRecord() = Level:Benign)
              exh:Ref_Number    = p_web.GSV('job:Exchange_Unit_Number')
              exh:Date = today()
              exh:Time = clock()
              exh:User = p_web.GSV('BookingUserCode')
              if (p_web.GSV('tmp:RemovalReason') = 1)
                  exh:Status = 'FAULTY UNIT RE-STOCKED FROM JOB NO: ' & clip(format(p_web.GSV('job:Ref_Number'),@p<<<<<<<#p))
              else ! if (p_web.GSV('tmp:RemovalReason') = 1)
                  exh:Status = 'UNIT RE-STOCKED FROM JOB NO: ' & clip(format(p_web.GSV('job:Ref_Number'),@p<<<<<<<#p))
              end !if (p_web.GSV('tmp:RemovalReason') = 1)
              if (Access:EXCHHIST.TryInsert() = Level:Benign)
                  ! Inserted
              else ! if (Access:EXCHHIST.TryInsert() = Level:Benign)
                  ! Error
                  Access:EXCHHIST.CancelAutoInc()
              end ! if (Access:EXCHHIST.TryInsert() = Level:Benign)
          end ! if (Access:EXCHHIST.PrimeRecord() = Level:Benign)
      else ! if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
          ! Error
      end ! if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
  
      p_web.SSV('job:Exchange_Unit_Number','')
  
  
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number    = p_web.GSV('job:Ref_Number')
      if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
          ! Found
          p_web.SessionQueueToFile(JOBS)
          access:JOBS.tryUpdate()
      else ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
          ! Error
      end ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
  
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber    = p_web.GSV('job:Ref_Number')
      if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
          ! Found
          p_web.SessionQueueToFile(JOBSE)
          access:JOBSE.tryUpdate()
      else ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
          ! Error
      end ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
  
      p_web.SessionQueueToFile(WEBJOB)
      access:WEBJOB.TryUpdate()
  
      p_web.SSV('tmp:ExchangeUnitNumber',0)
  
      do clearExchangeDetails
  
      p_web.SSV('Hide:RemovalReason',1)
  do Value::buttonConfirmExchangeRemoval
  do SendAlert
  do Value::buttonPickExchangeUnit  !1
  do Value::locExchangeAlertMessage  !1
  do Value::locRemovalAlertMessage  !1
  do Value::tmp:ExchangeAccessories  !1
  do Value::tmp:ExchangeIMEINumber  !1
  do Value::tmp:ExchangeLocation  !1
  do Value::tmp:ExchangeUnitDetails  !1
  do Value::tmp:MSN  !1
  do Prompt::tmp:RemovalReason
  do Value::tmp:RemovalReason  !1
  do Value::locRemoveText  !1
  do Value::tmp:ExchangeDate  !1

Value::buttonConfirmExchangeRemoval  Routine
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('buttonConfirmExchangeRemoval') & '_value',Choose(p_web.GSV('Hide:RemovalReason') = 1 Or p_web.GSV('tmp:RemovalReason') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:RemovalReason') = 1 Or p_web.GSV('tmp:RemovalReason') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonConfirmExchangeRemoval'',''pickexchangeunit_buttonconfirmexchangeremoval_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ConfirmExchangeRemoval','Confirm Exch Removal','button-entryfield',loc:formname,,,,loc:javascript,0,'images\packdelete.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickExchangeUnit_' & p_web._nocolon('buttonConfirmExchangeRemoval') & '_value')

Comment::buttonConfirmExchangeRemoval  Routine
    loc:comment = ''
  p_web._DivHeader('PickExchangeUnit_' & p_web._nocolon('buttonConfirmExchangeRemoval') & '_comment',Choose(p_web.GSV('Hide:RemovalReason') = 1 Or p_web.GSV('tmp:RemovalReason') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:RemovalReason') = 1 Or p_web.GSV('tmp:RemovalReason') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('PickExchangeUnit_tmp:ExchangeIMEINumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ExchangeIMEINumber
      else
        do Value::tmp:ExchangeIMEINumber
      end
  of lower('PickExchangeUnit_buttonPickExchangeUnit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonPickExchangeUnit
      else
        do Value::buttonPickExchangeUnit
      end
  of lower('PickExchangeUnit_tmp:HandsetPartNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:HandsetPartNumber
      else
        do Value::tmp:HandsetPartNumber
      end
  of lower('PickExchangeUnit_tmp:HandsetReplacementValue_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:HandsetReplacementValue
      else
        do Value::tmp:HandsetReplacementValue
      end
  of lower('PickExchangeUnit_locKeepParts_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locKeepParts
      else
        do Value::locKeepParts
      end
  of lower('PickExchangeUnit_job:Exchange_Courier_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Exchange_Courier
      else
        do Value::job:Exchange_Courier
      end
  of lower('PickExchangeUnit_button:AmendDespatchDetails_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::button:AmendDespatchDetails
      else
        do Value::button:AmendDespatchDetails
      end
  of lower('PickExchangeUnit_buttonRemoveAttachedUnit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonRemoveAttachedUnit
      else
        do Value::buttonRemoveAttachedUnit
      end
  of lower('PickExchangeUnit_locRemovalAlertMessage_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locRemovalAlertMessage
      else
        do Value::locRemovalAlertMessage
      end
  of lower('PickExchangeUnit_tmp:RemovalReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RemovalReason
      else
        do Value::tmp:RemovalReason
      end
  of lower('PickExchangeUnit_buttonConfirmExchangeRemoval_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonConfirmExchangeRemoval
      else
        do Value::buttonConfirmExchangeRemoval
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('PickExchangeUnit_form:ready_',1)
  p_web.SetSessionValue('PickExchangeUnit_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_PickExchangeUnit',0)

PreCopy  Routine
  p_web.SetValue('PickExchangeUnit_form:ready_',1)
  p_web.SetSessionValue('PickExchangeUnit_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_PickExchangeUnit',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('PickExchangeUnit_form:ready_',1)
  p_web.SetSessionValue('PickExchangeUnit_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('PickExchangeUnit:Primed',0)

PreDelete       Routine
  p_web.SetValue('PickExchangeUnit_form:ready_',1)
  p_web.SetSessionValue('PickExchangeUnit_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('PickExchangeUnit:Primed',0)
  p_web.setsessionvalue('showtab_PickExchangeUnit',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
      If(p_web.GSV('tmp:ExchangeUnitNumber') > 0)
        If (p_web.GSV('ReadOnly:ExchangeIMEINumber') = 1 OR p_web.GSV('job:Exchange_Unit_Number') > 0)
          If p_web.IfExistsValue('tmp:NoUnitAvailable') = 0
            p_web.SetValue('tmp:NoUnitAvailable',0)
            tmp:NoUnitAvailable = 0
          End
        End
      End
  If p_web.GSV('job:Exchange_Unit_Number') > 0 AND p_web.GSV('ReadOnly:ExchangeIMEINumber') = 0
  End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('PickExchangeUnit_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
    if (p_web.GSV('tmp:ExchangeUnitNumber') > 0)
        IF (p_web.GSV('Hide:KeepParts') = 0)
            IF (p_web.GSV('locKeepParts') = 0)
                loc:alert = 'You must specify if you want to keep the attached warranty parts.'
                loc:invalid = 'locKeepParts'
                EXIT
            END !I F
        END!  F
        
        
          if (p_web.GSV('tmp:ExchangeUnitNumber') <> p_web.GSV('job:Exchange_Unit_Number'))
              AddExchangeUnit()
          end ! if (p_web.GSV('tmp:ExchangeUnitNumber') <> p_web.GSV('job:Exchange_Unit_Number'))
      else ! if (p_web.GSV('tmp:ExchangeUnitNumber') > 0)
          if (p_web.GSV('tmp:NoUnitAvailable') = 1)
  
              getStatus(350,0,'EXC',p_web)
  
              getStatus(355,0,'JOB',p_web)
  
              AllocateExchangePart('ORD',0)
          end ! if (p_web.GSV('tmp:NoUnitAvailable') = 1)
      end ! if (p_web.GSV('tmp:ExchangeUnitNumber') > 0)
      do clearVariables
  p_web.DeleteSessionValue('PickExchangeUnit_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
          tmp:ExchangeIMEINumber = Upper(tmp:ExchangeIMEINumber)
          p_web.SetSessionValue('tmp:ExchangeIMEINumber',tmp:ExchangeIMEINumber)
        If loc:Invalid <> '' then exit.
  ! tab = 5
    loc:InvalidTab += 1
          job:Exchange_Courier = Upper(job:Exchange_Courier)
          p_web.SetSessionValue('job:Exchange_Courier',job:Exchange_Courier)
        If loc:Invalid <> '' then exit.
  ! tab = 3
  If p_web.GSV('job:Exchange_Unit_Number') > 0 AND p_web.GSV('ReadOnly:ExchangeIMEINumber') = 0
    loc:InvalidTab += 1
  End
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('PickExchangeUnit:Primed',0)
  p_web.StoreValue('job:ESN')
  p_web.StoreValue('job:MSN')
  p_web.StoreValue('')
  p_web.StoreValue('job:Charge_Type')
  p_web.StoreValue('job:Warranty_Charge_Type')
  p_web.StoreValue('tmp:ExchangeIMEINumber')
  p_web.StoreValue('locExchangeAlertMessage')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:ExchangeDate')
  p_web.StoreValue('tmp:MSN')
  p_web.StoreValue('tmp:ExchangeUnitDetails')
  p_web.StoreValue('tmp:ExchangeAccessories')
  p_web.StoreValue('tmp:ExchangeLocation')
  p_web.StoreValue('tmp:HandsetPartNumber')
  p_web.StoreValue('tmp:ReplacementValue')
  p_web.StoreValue('tmp:HandsetReplacementValue')
  p_web.StoreValue('tmp:NoUnitAvailable')
  p_web.StoreValue('')
  p_web.StoreValue('locKeepParts')
  p_web.StoreValue('job:Exchange_Courier')
  p_web.StoreValue('job:Exchange_Consignment_Number')
  p_web.StoreValue('job:Exchange_Despatched')
  p_web.StoreValue('job:Exchange_Despatched_User')
  p_web.StoreValue('job:Exchange_Despatch_Number')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locRemovalAlertMessage')
  p_web.StoreValue('tmp:RemovalReason')
  p_web.StoreValue('')
AddExchangeUnit     PROCEDURE()
    
locAuditNotes   String(255)
    CODE
        Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
        xch:Ref_Number    = p_web.GSV('tmp:ExchangeUnitNumber')
        if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
            ! Found
            Access:MANUFACT.Clearkey(man:Manufacturer_Key)
            man:Manufacturer    = p_web.GSV('job:Manufacturer')
            if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
                ! Found
                if (man:QALoanExchange)
                    xch:Available = 'QA1'

                else ! if (man:QALoanExchange)
                    xch:Available = 'EXC'
                end !if (man:QALoanExchange)
                xch:StatusChangeDate = TODAY() ! #12127 Record status change. (Bryan: 13/07/2011)
                xch:Job_Number = p_web.GSV('job:Ref_Number')
                access:EXCHANGE.tryUpdate()

                if (Access:EXCHHIST.PrimeRecord() = Level:Benign)
                    exh:Ref_Number    = tmp:ExchangeUnitNumber
                    exh:Date    = Today()
                    exh:Time    = Clock()
                    exh:User    = p_web.GSV('BookingUserCode')
                    if (man:QALoanExchange)
                        exh:Status    = 'AWAITING QA. EXCHANGED ON JOB NO: ' & p_web.GSV('job:ref_number')
                    else ! if (man:QALoanExchange)
                        exh:status = 'UNIT EXCHANGED ON JOB NO: ' & p_web.GSV('job:Ref_number')
                    end !

                    if (Access:EXCHHIST.TryInsert() = Level:Benign)
                        ! Inserted
                    else ! if (Access:EXCHHIST.TryInsert() = Level:Benign)
                        ! Error
                        Access:EXCHHIST.CancelAutoInc()
                    end ! if (Access:EXCHHIST.TryInsert() = Level:Benign)
                end ! if (Access:EXCHHIST.PrimeRecord() = Level:Benign)

                locAuditNotes = 'UNIT NUMBER: ' & CLip(xch:ref_number) & |
                    '<13,10>MODEL NUMBER: ' & CLip(xch:model_number) & |
                    '<13,10>I.M.E.I.: ' & CLip(xch:esn)

                if (MSNRequired(xch:Manufacturer))
                    locAuditNotes = Clip(locAuditNotes) & '<13,10>M.S.N.: ' & Clip(xch:MSN)
                end ! if (MSNRequired(xch:Manufacturer))

                locAuditNotes = clip(locAuditNotes) & '<13,10>STOCK TYPE: ' & Clip(xch:Stock_Type)

!                p_web.SSV('AddToAudit:Type','EXC')
!                p_web.SSV('AddToAudit:Action','EXCHANGE UNIT ATTACHED TO JOB')
!                p_web.SSV('AddToAudit:Notes',clip(locAuditNotes))
                addToAudit(p_web,p_web.GSV('job:Ref_Number'),'EXC','EXCHANGE UNIT ATTACHED TO JOB',clip(locAuditNotes))

                if (p_web.GSV('BookingSite') = 'RRC')
                    p_web.SSV('jobe:ExchangeATRRC',1)
                else ! if (p_web.GSV('BookingSite') = 'RRC')
                    p_web.SSV('jobe:ExchangeATRRC',0)
                end !if (p_web.GSV('BookingSite') = 'RRC')

                AllocateExchangePart('PIK',0)

                ! Create a new exchange unit for incoming unit

                Access:EXCHANGE_ALIAS.Clearkey(xch_ali:ESN_Only_Key)
                xch_ali:ESN    = xch:ESN
                if (Access:EXCHANGE_ALIAS.TryFetch(xch_ali:ESN_Only_Key) = Level:Benign)
                    ! Found
                    ! Unit already exists, use that one
                    if (p_web.GSV('job:Date_Completed') <> '')
                        xch_ali:Available = 'RTS'
                    else ! if (p_web.GSV('job:Date_Completed') <> '')
                        if (p_web.GSV('job:Workshop') = 'YES')
                            xch_ali:Available = 'REP'
                        else ! if (p_web.GSV('job:Workshop') = 'YES')
                            xch_ali:Available = 'INC'
                        end !if (p_web.GSV('job:Workshop') = 'YES')
                    end !if (p_web.GSV('job:Date_Completed') <> '')
                    xch_ali:StatusChangeDate = TODAY() ! #12127 Record status change. (Bryan: 13/07/2011)
                    xch_ali:Job_Number = p_web.GSV('job:Ref_Number')
                    access:EXCHANGE_ALIAS.tryUpdate()
                else ! if (Access:EXCHANGE_ALIAS.TryFetch(xch_ali:ESN_Only_Key) = Level:Benign)
                    ! Error
                    ! IMEI doesn't exist. Create a new one
                    if (Access:EXCHANGE_ALIAS.PrimeRecord() = Level:Benign)
                        refNo# = xch_ali:Ref_Number
                        xch_ali:Record :=: xch:Record
                        xch_ali:Ref_Number = refNo#

                        if (p_web.GSV('job:Date_Completed') <> '')
                            xch_ali:Available = 'RTS'
                        else ! if (p_web.GSV('job:Date_Completed') <> '')
                            if (p_web.GSV('job:Workshop') = 'YES')
                                xch_ali:Available = 'REP'
                            else ! if (p_web.GSV('job:Workshop') = 'YES')
                                xch_ali:Available = 'INC'
                            end !if (p_web.GSV('job:Workshop') = 'YES')
                        end !if (p_web.GSV('job:Date_Completed') <> '')
                        xch_ali:Job_Number = p_web.GSV('job:Ref_Number')
                        xch_ali:ESN = p_web.GSV('job:ESN')
                        xch_ali:MSN = p_web.GSV('job:MSN')
                        xch_ali:Model_Number = p_web.GSV('job:Model_Number')
                        xch_ali:Manufacturer = p_web.GSV('job:Manufacturer')

                        if (Access:EXCHANGE_ALIAS.TryInsert() = Level:Benign)
                            ! Inserted
                        else ! if (Access:EXCHANGE_ALIAS.TryInsert() = Level:Benign)
                            ! Error
                            Access:EXCHANGE_ALIAS.CancelAutoInc()
                        end ! if (Access:EXCHANGE_ALIAS.TryInsert() = Level:Benign)
                    end ! if (Access:EXCHANGE_ALIAS.PrimeRecord() = Level:Benign)
                End

                ! Mark exchange order as fulfilled
                if (p_web.GSV('jobe:Engineer48HourOption') = 1)

                    Access:EXCHOR48.Clearkey(ex4:AttachedToJobKey)
                    ex4:AttachedToJob    = 0
                    ex4:Location    = p_web.GSV('BookingSiteLocation')
                    ex4:JobNumber    = p_web.GSV('job:Ref_Number')
                    if (Access:EXCHOR48.TryFetch(ex4:AttachedToJobKey) = Level:Benign)
                        ! Found
                        ex4:attachedToJob = 1
                        access:EXCHOR48.tryUpdate()
                    else ! if (Access:EXCHOR48.TryFetch(ex4:AttachedToJobKey) = Level:Benign)
                        ! Error
                    end ! if (Access:EXCHOR48.TryFetch(ex4:AttachedToJobKey) = Level:Benign)
                end ! if (p_web.GSV('jobe:Engineer48HourOption') = 1)

                if (man:QALoanExchange)
                    p_web.SSV('job:Despatched','')
                    p_web.SSV('job:DespatchType','')
                    p_web.SSV('job:Exchange_Status','QA REQUIRED')
                    p_web.SSV('job:Exchange_Unit',p_web.GSV('BookingUserCode'))

                    getStatus(605,0,'EXC',p_web)
                else ! if (man:QALoanExchange)

                    if (p_web.GSV('BookingSite') = 'RRC')
                        p_web.SSV('jobe:DespatchType','EXC')
                        p_web.SSV('jobe:Despatched','REA')
                        p_web.SSV('wob:ReadyToDespatch',1)
                        p_web.SSV('wob:DespatchCourier',p_web.GSV('job:exchange_Courier'))
                        p_web.SSV('jobe:Despatched','REA')
                        p_web.SSV('jobe:DespatchType','EXC')
                        p_web.SSV('wob:DespatchCourier',p_web.GSV('job:Exchange_Courier'))

                    else ! if (p_web.GSV('BookingSite') = 'RRC')
                        p_web.SSV('job:Exchange_Status','AWAITING DESPATCH')
                        p_web.SSV('job:Exchange_User',p_web.GSV('BookingUserCode'))
                        p_web.SSV('job:Exchange_Despatched','')

                        p_web.SSV('job:Despatched','REA')
                        p_web.SSV('job:Despatch_Type','EXC')
                    end ! if (p_web.GSV('BookingSite') = 'RRC')

                    getStatus(110,0,'EXC',p_web)

                end !if (man:QALoanExchange)

                if (p_web.GSV('job:Third_Party_Site') <> '')
                    Access:TRDBATCH.Clearkey(trb:ESN_Only_Key)
                    trb:ESN    = p_web.GSV('job:ESN')
                    set(trb:ESN_Only_Key,trb:ESN_Only_Key)
                    loop
                        if (Access:TRDBATCH.Next())
                            Break
                        end ! if (Access:TRDBATCH.Next())
                        if (trb:ESN    <> p_web.GSV('job:ESN'))
                            Break
                        end ! if (trb:ESN    <> p_web.GSV('job:ESN'))
                        if (trb:Ref_Number = p_web.GSV('job:Ref_Number'))
                            trb:Exchanged = 'YES'
                            access:TRDBATCH.tryUpdate()
                        end ! if (trb:Ref_Number = p_web.GSV('job:Ref_Number'))
                    end ! loop
                end ! if (p_web.GSV('job:Third_Party_Site'))

                if (p_web.GSV('BookingSite') = 'RRC')
                    ! #13088 Only mark as Hub Repair if there are no Chargeable Parts,
                    ! Or they have selected to keep the Warranty Parts (DBH: 14/03/2014)
                    hub# = 1
                    IF (ChargeableParts() = 1)
                        ! Don't mark as hub
                        hub# = 0
                    END !
                    IF (WarrantyParts() = 1 AND p_web.GSV('locKeepParts') <> 1)
                        ! Don't mark as hub
                        hub# = 0
                    END ! IF
                    IF (hub# = 1)                  
                        p_web.SSV('jobe:HubRepair',1)
                        p_web.SSV('jobe:HubRepairDate',Today())
                        p_web.SSV('jobe:HubRepairTime',Clock())
                        p_web.SSV('GetStatus:StatusNumber',Sub(GETINI('RRC','StatusSendToARC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3))
                        p_web.SSV('GetStatus:Type','JOB')
                        GetStatus(p_web.GSV('GetStatus:StatusNumber'),0,p_web.GSV('GetStatus:Type'),p_web)

                        Access:REPTYDEF.Clearkey(rtd:ManRepairTypeKey)
                        rtd:Manufacturer    = p_web.GSV('job:Manufacturer')
                        set(rtd:ManRepairTypeKey,rtd:ManRepairTypeKey)
                        loop
                            if (Access:REPTYDEF.Next())
                                Break
                            end ! if (Access:REPTYDEF.Next())
                            if (rtd:Manufacturer    <> p_web.GSV('job:Manufacturer'))
                                Break
                            end ! if (rtd:Manufacturer    <> p_web.GSV('job:Manufacturer'))
                            if (rtd:BER = 10)
                                if (p_web.GSV('job:Chargeable_Job') = 'YES' and p_web.GSV('job:Repair_Type') = '')
                                    p_web.SSV('job:Repair_Type',rtd:Repair_Type)
                                end
                                if (p_web.GSV('job:Warranty_Job') = 'YES' and p_web.GSV('job:Repair_Type_Warranty') = '')
                                    p_web.SSV('job:Repair_Type_Warranty',rtd:Repair_Type)
                                end
                                break
                            end ! if (rtd:BER = 10)
                        end ! loop

                        Access:JOBSENG.Clearkey(joe:UserCodeKey)
                        joe:JobNumber    = p_web.GSV('job:Ref_Number')
                        joe:UserCode    = p_web.GSV('job:Engineer')
                        joe:DateAllocated    = Today()
                        set(joe:UserCodeKey,joe:UserCodeKey)
                        loop
                            if (Access:JOBSENG.Previous())
                                Break
                            end ! if (Access:JOBSEND.Next())
                            if (joe:JobNumber    <> p_web.GSV('job:Ref_Number'))
                                Break
                            end ! if (joe:JobNumber    <> p_web.GSV('job:Ref_Number'))
                            if (joe:UserCode    <> p_web.GSV('job:Engineer'))
                                Break
                            end ! if (joe:UserCode    <> p_web.GSV('job:Engineer'))
                            if (joe:DateAllocated    > Today())
                                Break
                            end ! if (joe:DateAllocated    <> Today())
                            joe:Status = 'HUB'
                            joe:StatusDate = Today()
                            joe:StatusTime = Clock()
                            access:JOBSENG.tryUpdate()
                            break
                        end ! loop
                    END ! IF
                    
                end ! if (p_web.GSV('BookingSite') = 'RRC')

                p_web.SSV('jobe:ExchangePRoductCode','')
                p_web.SSV('jobe:HandsetReplacmentValue',0)
                p_web.SSV('job:Exchange_Unit_Number',p_web.GSV('tmp:ExchangeUnitNumber'))

                if (p_web.GSV('Hide:HandsetPartNumber') = 0)
                    p_web.SSV('jobe:ExchangeProductCode',p_web.GSV('tmp:HandsetPartNumber'))
                    p_web.SSV('jobe:HandsetReplacmentValue',p_web.GSV('tmp:HandsetReplacementValue'))
                end ! if (p_web.GSV('BookingSite') = 'ARC')

                Access:JOBSE.Clearkey(jobe:RefNumberKey)
                jobe:RefNumber    = p_web.GSV('job:Ref_Number')
                if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                    ! Found
                    p_web.SessionQueueToFile(JOBSE)
                    access:JOBSE.tryUpdate()
                else ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                    ! Error
                end ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)

                !Access:WEBJOB.Clearkey(wob:RefNumberKey)
                !wob:RefNumber    = p_web.GSV('job:Ref_Number')
                !if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                ! Found
                p_web.SessionQueueToFile(WEBJOB)
                access:WEBJOB.tryUpdate()
                !else ! if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                !    ! Error
                !end ! if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)

                Access:JOBS.Clearkey(job:Ref_Number_Key)
                job:Ref_Number    = p_web.GSV('job:Ref_Number')
                if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                    ! Found
                    p_web.SessionQueueToFile(JOBS)
                    access:JOBS.tryUpdate()
                else ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                    ! Error
                end ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)

                loc:Alert = 'Exchange Unit Added'

                ! Passed From Exchange Allocation
                IF (p_web.GSV('FromURL') = 'ExchangeAllocation')
                    Access:SBO_GenericFile.ClearKey(sbogen:RecordNumberKey)
                    sbogen:RecordNumber = p_web.GSV('sbogen:RecordNumber')
                    IF (Access:SBO_GenericFile.TryFetch(sbogen:RecordNumberKey) = Level:Benign)
                        Access:SBO_GenericFile.DeleteRecord(0)
                    END
                END
                
                
                ! Completely save the job, so that there is no need to return to the job screen
                Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
                jobe2:RefNumber = p_web.GSV('job:Ref_Number')
                IF (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
                    p_web.SessionQueueToFile(JOBSE2)
                    Access:JOBSE2.TryUpdate()
                END ! IF
                
                Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
                jbn:RefNumber = p_web.GSV('job:Ref_Number')
                IF (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)
                    p_web.SessionQueueToFile(JOBNOTES)
                    Access:JOBNOTES.TryUpdate()
                END ! IF
            else ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
                ! Error
            end ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
        else ! if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
            ! Error
        end ! if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
AllocateExchangePart        Procedure(String    func:Status,Byte func:SecondUnit)
local:FoundPart                 Byte(0)
Code
    If p_web.GSV('job:Warranty_Job') = 'YES'
        Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
        wpr:Ref_Number  = p_web.GSV('job:Ref_Number')
        wpr:Part_Number = 'EXCH'
        Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
        Loop
            If Access:WARPARTS.NEXT()
               Break
            End !If
            If wpr:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
            Or wpr:Part_Number <> 'EXCH'      |
                Then Break.  ! End If
            If func:SecondUnit
                If wpr:SecondExchangeUnit
                    local:FoundPart = True
                End !If wpr:SecondExchangeUnit
            Else !If func:SecondUnit
                local:FoundPart = True
            End !If func:SecondUnit
            If local:FoundPart = True
                wpr:Status = func:Status
                If func:Status = 'PIK'
                    wpr:PartAllocated = 1
                End !If func:Status = 'PIK'
                Access:WARPARTS.Update()
                !Remove the EXCH line from Stock Allocation,
                !just incase the Exchange isn't being added properly - L873 (DBH: 24-07-2003)
                RemoveFromStockAllocation(wpr:Record_Number,'WAR')
                Break
            End !If local:FoundPart = True
        End !Loop
    End !If job:Warranty_Job = 'YES'


    If local:FoundPart = False
        If p_web.GSV('job:Chargeable_Job') = 'YES'
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number  = p_web.GSV('job:Ref_Number')
            par:Part_Number = 'EXCH'
            Set(par:Part_Number_Key,par:Part_Number_Key)
            Loop
                If Access:PARTS.NEXT()
                   Break
                End !If
                If par:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
                Or par:Part_Number <> 'EXCH'      |
                    Then Break.  ! End If
                If func:SecondUnit
                    If par:SecondExchangeUnit
                        local:FoundPart = True
                    End !If wpr:SecondExchangeUnit
                Else !If func:SecondUnit
                    local:FoundPart = True
                End !If func:SecondUnit
                If local:FoundPart = True
                    par:Status = func:Status
                    If func:Status = 'PIK'
                        par:PartAllocated = 1
                    End !If func:Status = 'PIK'
                    Access:PARTS.Update()
                    !Remove the EXCH line from Stock Allocation,
                    !just incase the Exchange isn't being added properly - L873 (DBH: 24-07-2003)
                    RemoveFromStockAllocation(par:Record_Number,'CHA')
                    Break
                End !If local:FoundPart = True

            End !Loop
        End !If job:Chargeable_Job = 'YES'
    End !If local:FounPart = False

    If local:FoundPart = False
        If p_web.GSV('job:Engineer') = ''
            Access:USERS.Clearkey(use:Password_Key)
            use:Password    = p_web.GSV('BookingUserPassword')
            If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                !Found

            Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                !Error
            End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        Else !If job:Engineer = ''
            Access:USERS.Clearkey(use:User_Code_Key)
            use:User_Code   = p_web.GSV('job:Engineer')
            If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign

            End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign

        End !If job:Engineer = ''
        Access:STOCK.Clearkey(sto:Location_Key)
        sto:Location    = use:Location
        sto:Part_Number = 'EXCH'
        If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
            !Found
            Access:LOCATION.Clearkey(loc:Location_Key)
            loc:Location    = sto:Location
            If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
                !Found
!                    If ~loc:UseRapidStock
!                        Return
!                    End !If ~loc:UseRapidStock

            Else ! If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
                !Error
            End !If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
            If p_web.GSV('job:Warranty_Job') = 'YES'
               get(warparts,0)
               if access:warparts.primerecord() = level:benign
                   wpr:PArt_Ref_Number      = sto:Ref_Number
                   wpr:ref_number            = p_web.GSV('job:ref_number')
                   wpr:adjustment            = 'YES'
                   wpr:part_number           = 'EXCH'
                   wpr:description           = p_web.GSV('job:Manufacturer') & ' EXCHANGE UNIT'
                   wpr:quantity              = 1
                   wpr:warranty_part         = 'NO'
                   wpr:exclude_from_order    = 'YES'
                   wpr:PartAllocated         = 1
                   wpr:Status                = func:Status
                   wpr:ExchangeUnit          = True
                   wpr:SecondExchangeUnit    = func:SecondUnit
                   If sto:Assign_Fault_Codes = 'YES'
                       !Try and get the fault codes. This key should get the only record
                       Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                       stm:Ref_Number  = sto:Ref_Number
                       stm:Part_Number = sto:Part_Number
                       stm:Location    = sto:Location
                       stm:Description = sto:Description
                       If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                           !Found
                           wpr:Fault_Code1  = stm:FaultCode1
                           wpr:Fault_Code2  = stm:FaultCode2
                           wpr:Fault_Code3  = stm:FaultCode3
                           wpr:Fault_Code4  = stm:FaultCode4
                           wpr:Fault_Code5  = stm:FaultCode5
                           wpr:Fault_Code6  = stm:FaultCode6
                           wpr:Fault_Code7  = stm:FaultCode7
                           wpr:Fault_Code8  = stm:FaultCode8
                           wpr:Fault_Code9  = stm:FaultCode9
                           wpr:Fault_Code10 = stm:FaultCode10
                           wpr:Fault_Code11 = stm:FaultCode11
                           wpr:Fault_Code12 = stm:FaultCode12
                       Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                       !Error
                       !Assert(0,'<13,10>Fetch Error<13,10>')
                       End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                   end !If sto:Assign_Fault_Codes = 'YES'
                   if access:warparts.insert()
                       access:warparts.cancelautoinc()
                   end
                End !If Prime

             Else !If job:Warranty_Job = 'YES'
                If p_web.GSV('job:Chargeable_Job') = 'YES'
                    get(parts,0)
                    if access:parts.primerecord() = level:benign
                        !message('At break2')
                        par:PArt_Ref_Number      = sto:Ref_Number
                        par:ref_number            = p_web.GSV('job:ref_number')
                        par:adjustment            = 'YES'
                        par:part_number           = 'EXCH'
                        par:description           = p_web.GSV('job:Manufacturer') & ' EXCHANGE UNIT'
                        par:quantity              = 1
                        par:warranty_part         = 'NO'
                        par:exclude_from_order    = 'YES'
                        par:PartAllocated         = 1
                        par:Status                = func:Status
                        par:ExchangeUnit          = True
                        par:SecondExchangeUnit    = func:SecondUnit
                        If sto:Assign_Fault_Codes = 'YES'
                           !Try and get the fault codes. This key should get the only record
                           Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                           stm:Ref_Number  = sto:Ref_Number
                           stm:Part_Number = sto:Part_Number
                           stm:Location    = sto:Location
                           stm:Description = sto:Description
                           If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                               !Found
                               par:Fault_Code1  = stm:FaultCode1
                               par:Fault_Code2  = stm:FaultCode2
                               par:Fault_Code3  = stm:FaultCode3
                               par:Fault_Code4  = stm:FaultCode4
                               par:Fault_Code5  = stm:FaultCode5
                               par:Fault_Code6  = stm:FaultCode6
                               par:Fault_Code7  = stm:FaultCode7
                               par:Fault_Code8  = stm:FaultCode8
                               par:Fault_Code9  = stm:FaultCode9
                               par:Fault_Code10 = stm:FaultCode10
                               par:Fault_Code11 = stm:FaultCode11
                               par:Fault_Code12 = stm:FaultCode12
                           Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                               !Error
                               !Assert(0,'<13,10>Fetch Error<13,10>')
                           End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                        End !If sto:Assign_Fault_Codes = 'YES'
                        if access:parts.insert()
                            access:parts.cancelautoinc()
                        end
                    End !If access:Prime

                End !If job:Chargeable_Job = 'YES'
             End !If job:Warranty_Job = 'YES'

        Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
             !Error
!            Case Missive('You must have a Part setup in Stock Control with a part number of "EXCH" under the location ' & Clip(use:Location) & '.','ServiceBase 3g',|
!                           'mstop.jpg','/OK')
!                Of 1 ! OK Button
!            End ! Case Missive
        End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
    End !If local:FounPart = False
ChargeableParts  PROCEDURE()!,BOOL
RetValue    BOOL(FALSE)
    CODE
        Access:PARTS.ClearKey(par:Part_Number_Key)
        par:Ref_Number = p_web.GSV('job:Ref_Number')
        SET(par:Part_Number_Key,par:Part_Number_Key)
        LOOP UNTIL Access:PARTS.Next() <> Level:Benign
            IF (par:Ref_Number <> p_web.GSV('job:Ref_Number'))
                BREAK
            END ! IF
            IF (par:Part_Number = 'EXCH')
                CYCLE
            END ! IF
            RetValue = TRUE
            BREAK
        END ! LOOP
        
        RETURN RetValue
GetExchangeDate PROCEDURE()
    CODE
        p_web.SSV('tmp:ExchangeDate','')
        IF (p_web.GSV('tmp:ExchangeUnitNumber') > 0)
            Access:AUDIT.ClearKey(aud:TypeActionKey)
            aud:Ref_Number = p_web.GSV('job:Ref_Number')
            aud:Type = 'EXC'
            aud:Action = 'EXCHANGE UNIT ATTACHED TO JOB'
            SET(aud:TypeActionKey,aud:TypeActionKey)
            LOOP UNTIL Access:AUDIT.Next() <> Level:Benign
                IF (aud:Ref_Number <> p_web.GSV('job:Ref_Number') OR |
                    aud:Type <> 'EXC' OR |
                    aud:Action <> 'EXCHANGE UNIT ATTACHED TO JOB')
                    BREAK
                END ! IF
                
                Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
                aud2:AUDRecordNumber = aud:Record_Number
                IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
                    IF (INSTRING(p_web.GSV('tmp:ExchangeUnitNumber'),aud2:Notes,1,1))
                        p_web.SSV('tmp:ExchangeDate',aud:Date)
                        BREAK
                    END ! IF
                END ! IF
            END ! LOOP
        END ! IF
GetExchangeDetails  PROCEDURE()

    CODE
    p_web.SSV('locExchangeAlertMessage','')

    do lookupExchangeDetails

    if (p_web.GSV('jobe:Engineer48HourOption') = 1)
        Access:EXCHOR48.Clearkey(ex4:AttachedToJobKey)
        ex4:AttachedToJob    = 0
        ex4:Location    = p_web.GSV('BookingSiteLocation')
        ex4:JobNumber    = p_web.GSV('job:Ref_Number')
        if (Access:EXCHOR48.TryFetch(ex4:AttachedToJobKey) = Level:Benign)
            ! Found
            Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
            xch:Ref_Number    = ex4:orderUnitNumber
            if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                ! Found
                if (p_web.GSV('tmp:exchangeIMEINumber') <> xch:ESN)
                    p_web.SSV('locExchangeAlertMessage','Warning! An Exchange Unit has been ordered for this job under the 48 Hour Exchange Process. The I.M.E.I. Number you have selected is NOT the unit that has been ordered.')
                end ! if (p_web.GSV('tmp:exchangeIMEINumber') <> xch:ESN)
            else ! if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                ! Error
            end ! if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
        else ! if (Access:EXCHOR48.TryFetch(ex4:AttachedToJobKey) = Level:Benign)
            ! Error
        end ! if (Access:EXCHOR48.TryFetch(ex4:AttachedToJobKey) = Level:Benign)
    end ! if (p_web.GSV('jobe:Engineer48HourOption') = 1)

    Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
    xch:Ref_Number    = p_web.GSV('tmp:ExchangeUnitNumber')
    if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
        ! Found
    else ! if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
        ! Error
    end ! if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
    if (xch:Model_Number <> p_web.GSV('job:Model_Number'))
        p_web.SSV('locExchangeAlertMessage','Warning! The selected Exchange Unit has a different Model Number!')
    end ! if (xch:Model_Number <> p_web.GSV('job:Model_Number')
    

        
        IF (ChargeableParts() > 0)
            loc:alert = 'This job has parts attached. These must be removed before you can send it to the ARC.'
        END ! I F
       
        IF (WarrantyParts() > 0)
            p_web.SSV('Hide:KeepParts',0)
        ELSE ! IF
            p_web.SSV('Hide:KeepParts',1)
        END ! IF
    
WarrantyParts  PROCEDURE()!,BOOL
RetValue    BOOL(FALSE)
    CODE
        Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
        wpr:Ref_Number = p_web.GSV('job:Ref_Number')
        SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
        LOOP UNTIL Access:WARPARTS.Next() <> Level:Benign
            IF (wpr:Ref_Number <> p_web.GSV('job:Ref_Number'))
                BREAK
            END ! IF
            IF (wpr:Part_Number = 'EXCH')
                CYCLE
            END ! IF
            RetValue = TRUE
            BREAK
        END ! LOOP
        
        RETURN RetValue
