

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER306.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER116.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER159.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER217.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER260.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER307.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER315.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER316.INC'),ONCE        !Req'd for module callout resolution
                     END


QAProcedure          PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:Paid             BYTE                                  !
LocalDateBooked      STRING(10)                            !
LocalHandsetType     STRING(20)                            !
LocalModel           STRING(20)                            !
LimitModel           STRING(30)                            !
LocalIMEI            STRING(20)                            !
LocalJobNumber       LONG                                  !
QAType               STRING(10)                            !M/E
SendingResult        STRING(255)                           !
PassOrFail           STRING(4)                             !PASS or FAIL
FilterLocation       STRING(30)                            !
FailReason           STRING(30)                            !
FailNotes            STRING(255)                           !
FilesOpened     Long
JOBNOTES::State  USHORT
DESBATCH::State  USHORT
JOBACC::State  USHORT
REPTYDEF::State  USHORT
EXCAUDIT::State  USHORT
USERS::State  USHORT
EXCHANGE_ALIAS::State  USHORT
EXCHHIST::State  USHORT
MODELNUM::State  USHORT
JOBSENG::State  USHORT
JOBSOBF::State  USHORT
COURIER::State  USHORT
WEBJOB::State  USHORT
TRADEACC::State  USHORT
EXCHANGE::State  USHORT
JOBSE::State  USHORT
JOBS::State  USHORT
TagFile::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  GlobalErrors.SetProcedureName('QAProcedure')
  loc:formname = 'QAProcedure_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('QAProcedure','')
    p_web._DivHeader('QAProcedure',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferQAProcedure',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferQAProcedure',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferQAProcedure',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_QAProcedure',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferQAProcedure',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_QAProcedure',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
LookupJob       ROUTINE
    DATA
errorMessage    STRING(255)
    CODE
    
        p_web.SSV('LocalDateBooked','NOT FOUND')
        p_web.SSV('LocalHandsetType','NOT FOUND')
        p_web.SSV('LocalModel','NOT FOUND')
        errorMessage = 'ERROR: Serial number not found on job '&clip(p_web.gsv('LocalJobNumber'))&'. Please check and try again.'
        
        LOOP 1 TIMES
            IF (p_web.GSV('LocalJobNumber') = '')
                errorMessage = 'EMPTY: No Job Number'
                BREAK
            END ! IF
            
            IF (p_web.GSV('LocalIMEI') = '')
                errorMessage = 'EMPTY: No Serial Number'
                BREAK
            END ! IF
            
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = p_web.GSV('LocalJobNumber')
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
                errorMessage = 'ERROR: Job '&clip(p_web.gsv('LocalJobNumber')) & ' not found. Please check and try again.'
                BREAK
            END ! IF
            
            Access:WEBJOB.ClearKey(wob:RefNumberKey)
            wob:RefNumber = job:Ref_Number
            IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
                errorMessage = 'ERROR: Webjob Record not found by job number '&clip(p_web.gsv('LocalJobNumber')) & '. Please check and try again.'
                BREAK
            END ! IF
            
            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = wob:HeadAccountNumber
            IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
                errorMessage = 'ERROR: TradeAcc Record not found by job number '&clip(p_web.gsv('LocalJobNumber')) & '. Please check and try again.'
                BREAK
            END ! IF
            
            !will need to remember if this is an exchange account
            p_web.SSV('ExchangeAcc',tra:ExchangeAcc)
            
            if p_web.gsv('LocalIMEI') = job:ESN or  p_web.gsv('LocalIMEI') = job:MSN THEN
                !further checks apply
                if job:Workshop <> 'YES' THEN
                    errorMessage = 'ERROR -  Job '&clip(p_web.gsv('LocalJobNumber')) & '  cannot be updated as it is not in the workshop.'
                    BREAK
                END
                if tra:SiteLocation <> p_web.gsv('FilterLocation') THEN
                    errorMessage = 'ERROR: Job '&clip(p_web.gsv('LocalJobNumber')) & ' is out of RRC control.'
                    !SendingResult =  'ERROR - This job cannot be updated as it is at a different location to the user. ' & clip(tra:SiteLocation)&' : '&clip(p_web.gsv('FilterLocation'))
                    BREAK
                END 
                if job:Completed = 'YES' and job:Current_Status[1:3] <> '605' THEN
                    errorMessage = 'ERROR - Job '&clip(p_web.gsv('LocalJobNumber')) & ' cannot be updated as it is completed.'
                    BREAK
                END
                if job:Third_Party_Site = ''
                    p_web.SSV('LocalHandsetType','REPAIR')
                ELSE
                    p_web.SSV('LocalHandsetType','THIRD PARTY REPAIR')
                END

                p_web.SSV('LocalModel',clip(job:Model_Number) & ' ' & clip(job:Manufacturer))
                p_web.SSV('LimitModel',job:Model_Number)
                p_web.SSV('LocalDateBooked',FORMAT(job:Date_Booked,@d06b))
                errorMessage = ''

            ELSE ! if p_web.gsv('LocalIMEI') = job:ESN or  p_web.gsv('LocalIMEI') = job:MSN THEN
            
                !localIMEI is not the original handset
                
                !check for second exchange first - this takes priority
                if jobe:SecondExchangeNumber <> '' THEN
                    access:exchange.clearkey(xch:ref_number_key)
                    xch:ref_number = jobe:SecondExchangeNumber
                    if access:exchange.fetch(xch:ref_number_key) = Level:Benign        
                        if p_web.gsv('LocalIMEI') = xch:ESN or  p_web.gsv('LocalIMEI') = xch:MSN THEN
                            p_web.SSV('LocalModel',Clip(xch:model_number) & ' - ' & Clip(xch:manufacturer))
                            p_web.SSV('LimitModel',xch:Model_Number)
                            p_web.SSV('LocalHandsetType','2ND EXCHANGE')
                            p_web.SSV('LocalDateBooked',format(job:date_booked,@d06))    !job:date_booked
                            errorMessage = ''
                            BREAK
                        END !if imei matches
                    END !if exchange fetched
                END !if second exchange number exists
                
                !SerialNo matches Exchange IMEI	   
                If job:exchange_unit_number <> '' 
                    access:exchange.clearkey(xch:ref_number_key)
                    xch:ref_number = job:exchange_unit_number
                    if access:exchange.fetch(xch:ref_number_key) = Level:Benign        
                        if p_web.gsv('LocalIMEI') = xch:ESN or  p_web.gsv('LocalIMEI') = xch:MSN THEN
                            p_web.SSV('LocalModel',Clip(xch:model_number) & ' - ' & Clip(xch:manufacturer))
                            p_web.SSV('LimitModel',xch:Model_Number)
                            p_web.SSV('LocalHandsetType','EXCHANGE')
                            p_web.SSV('LocalDateBooked',format(job:date_booked,@d06))        !job:date_booked
                            errorMessage = ''
                            BREAK
                        END !if imei matches
                    END !if exchange fetched
                END !if job:exchangeUnitNumber exists
                
                !check for the loan last
                If job:loan_unit_number <> '' 
                    access:loan.clearkey(loa:ref_number_key)
                    loa:ref_number = job:loan_unit_number
                    if access:loan.fetch(loa:ref_number_key) = Level:Benign
                        if p_web.gsv('LocalIMEI') = loa:ESN or  p_web.gsv('LocalIMEI') = loa:MSN THEN
                            p_web.SSV('LocalModel',Clip(loa:model_number) & ' - ' & Clip(loa:manufacturer))
                            p_web.SSV('LimitModel',loa:Model_Number)
                            p_web.SSV('LocalHandsetType','LOAN')
                            p_web.SSV('LocalDateBooked',format(job:date_booked,@d06))        !job:date_booked
                            errorMessage = ''
                            BREAK        
                        End  !if imei matches
                    end !if loan.fetched
                End!If job:loan_unit_number
            END ! if p_web.gsv('LocalIMEI') = job:ESN or  p_web.gsv('LocalIMEI') = job:MSN THEN
        END ! LOOP
        
        CASE errorMessage[1:5]
        of 'EMPTY'
            !do nothing - wait for both job and serial to be filled in
            EXIT
        of 'ERROR'
            !show the error
            p_web.Message('Alert',clip(errorMessage),p_web._MessageClass,Net:Send)
            EXIT
    !    of 'SCRAP'
    !        !show the advice
    !        p_web.message('Alert','This job has an exchange attached. The repair type indicates that this unit should be scrapped. Select if you want to Scrap or Restock this.',p_web._MessageClass,Net:Send)
        ELSE
            !message needed to update screen?   NO!
            !p_web.Message('Alert','JOB FOUND: Please check on screen details and then select [PASS] or [FAIL]',p_web._MessageClass,Net:Send)
        END
        
        p_web.FileToSessionQueue(JOBS)
        p_web.FileToSessionQueue(WEBJOB)

!region LookupJob           ROUTINE ! #13425 Refactored code to try and prevent issues with job lookup not always working. (DBH: 20/11/2014)
!    
!    !does the job exist with this job number and Serial Number?
!    do LookupJobDetails     !so I can use EXIT
!    
!    CASE SendingResult[1:5]
!    of 'EMPTY'
!        !do nothing - wait for both job and serial to be filled in
!    of 'ERROR'
!        !show the error
!        p_web.Message('Alert',clip(SendingResult),p_web._MessageClass,Net:Send)
!!    of 'SCRAP'
!!        !show the advice
!!        p_web.message('Alert','This job has an exchange attached. The repair type indicates that this unit should be scrapped. Select if you want to Scrap or Restock this.',p_web._MessageClass,Net:Send)
!    ELSE
!        !message needed to update screen?   NO!
!        !p_web.Message('Alert','JOB FOUND: Please check on screen details and then select [PASS] or [FAIL]',p_web._MessageClass,Net:Send)
!    END
!    
!    !update statics
!    p_web.ssv('LocalDateBooked',LocalDateBooked)
!    p_web.ssv('LocalHandsetType',LocalHandsetType)  !REPAIR, THIRD PARTY REPAIR, '2ND EXCHANGE', 'EXCHANGE' or 'LOAN'
!    p_web.ssv('LocalModel',LocalModel)
!    p_web.ssv('LimitModel',LimitModel)  !used in call to accessories to limit choice to "limitmodel"
!    
!    EXIT
!    
!    
!LookupJobDetails    ROUTINE
!
!    !p_web.Message('Alert','In lookup job details' ,p_web._MessageClass,Net:Send)
!    
!    !defaults for the erroring
!    LocalDateBooked  = 'NOT FOUND'
!    LocalHandsetType = 'NOT FOUND'
!    LocalModel       = 'NOT FOUND'
!    SendingResult    = 'ERROR: Serial number not found on job '&clip(p_web.gsv('LocalJobNumber'))&'. Please check and try again.'
!        !was 'ERROR: This serial number does not match against any on the job. Please check your entry.'
!    
!    if p_web.gsv('LocalJobNumber') = '' THEN
!        SendingResult = 'EMPTY: No Job number'
!        EXIT
!    END
!    
!    if p_web.gsv('LocalIMEI') = '' THEN
!        SendingResult = 'EMPTY: No Serial Number'
!        EXIT
!    END
!    
!    Access:jobs.ClearKey(job:Ref_Number_Key)
!    !job:Ref_Number = DEFORMAT(p_web.gsv('LocalJobNumber'))
!    job:Ref_Number = p_web.GSV('LocalJobNumber')  ! No need to deformat (DBH: 20/11/2014)
!    if access:jobs.fetch(job:Ref_Number_Key)
!        SendingResult = 'ERROR: Job '&clip(p_web.gsv('LocalJobNumber')) & ' not found. Please check and try again.'
!            !was 'ERROR: Job Record not found by job number '&clip(p_web.gsv('LocalJobNumber') & '. Please check your entry.')
!        EXIT
!    END
!    p_web.FileToSessionQueue(JOBS)  !so it will be accessed in other procedures
!    
!    Access:jobse.clearkey(jobe:RefNumberKey)
!    jobe:RefNumber = job:Ref_Number
!    if access:jobse.fetch(jobe:RefNumberKey)
!        SendingResult = 'ERROR: Jobse Record not found by job number '&clip(p_web.gsv('LocalJobNumber')) & '. Please check and try again.'
!        EXIT
!    END
!    p_web.FileToSessionQueue(JOBSE) !so it will be accessed in other procedures
!    
!    !will need location - find from webjob.HeadAccountNumber
!    Access:Webjob.clearkey(wob:RefNumberKey)
!    wob:RefNumber = job:Ref_Number
!    if access:Webjob.fetch(wob:RefNumberKey)
!        SendingResult = 'ERROR: Webjob Record not found by job number '&clip(p_web.gsv('LocalJobNumber')) & '. Please check and try again.'
!        EXIT
!    END
!     p_web.FileToSessionQueue(WEBJOB) !so it will be accessed in other procedures
!    
!    Access:TRADEACC.clearkey(tra:Account_Number_Key)
!    tra:Account_Number = wob:HeadAccountNumber
!    if Access:TRADEACC.fetch(tra:Account_Number_Key)
!        SendingResult = 'ERROR: TradeAcc Record not found by job number '&clip(p_web.gsv('LocalJobNumber')) & '. Please check and try again.'
!        EXIT
!    END
!
!    !will need to remember if this is an exchange account
!    p_web.ssv('ExchangeAcc',tra:ExchangeAcc)
!    
!    if p_web.gsv('LocalIMEI') = job:ESN or  p_web.gsv('LocalIMEI') = job:MSN THEN
!        !further checks apply
!        if job:Workshop <> 'YES' THEN
!            SendingResult = 'ERROR -  Job '&clip(p_web.gsv('LocalJobNumber')) & '  cannot be updated as it is not in the workshop.'
!            EXIT
!        END
!        if tra:SiteLocation <> p_web.gsv('FilterLocation') THEN
!            SendingResult = 'ERROR: Job '&clip(p_web.gsv('LocalJobNumber')) & ' is out of RRC control.'
!            !SendingResult =  'ERROR - This job cannot be updated as it is at a different location to the user. ' & clip(tra:SiteLocation)&' : '&clip(p_web.gsv('FilterLocation'))
!            EXIT
!        END 
!        if job:Completed = 'YES' and job:Current_Status[1:3] <> '605' THEN
!            SendingResult = 'ERROR - Job '&clip(p_web.gsv('LocalJobNumber')) & ' cannot be updated as it is completed.'
!            EXIT
!        END
!        if job:Third_Party_Site = ''
!            LocalHandsetType = 'REPAIR'
!        ELSE
!            LocalHandsetType = 'THIRD PARTY REPAIR'
!        END
!        
!        LocalModel       = clip(job:Model_Number)&' '&clip(job:Manufacturer)
!        LimitModel       = job:Model_Number
!        LocalDateBooked  = format(job:date_booked,@d06)
!        SendingResult    = 'PROCESS OK'
!        
!        EXIT
!
!    ELSE    
!        !localIMEI is not the original handset
!        
!        !check for second exchange first - this takes priority
!        if jobe:SecondExchangeNumber <> '' THEN
!            access:exchange.clearkey(xch:ref_number_key)
!            xch:ref_number = jobe:SecondExchangeNumber
!            if access:exchange.fetch(xch:ref_number_key) = Level:Benign        
!                if p_web.gsv('LocalIMEI') = xch:ESN or  p_web.gsv('LocalIMEI') = xch:MSN THEN
!                    LocalModel        = Clip(xch:model_number) & ' - ' & Clip(xch:manufacturer)
!                    LimitModel        = xch:Model_Number
!                    LocalHandsetType  = '2ND EXCHANGE'
!                    LocalDateBooked   = format(job:date_booked,@d06)    !job:date_booked
!                    SendingResult    = 'PROCESS OK'
!                    EXIT 
!                END !if imei matches
!            END !if exchange fetched
!        END !if second exchange number exists
!        
!        !SerialNo matches Exchange IMEI	   
!        If job:exchange_unit_number <> '' 
!            access:exchange.clearkey(xch:ref_number_key)
!            xch:ref_number = job:exchange_unit_number
!            if access:exchange.fetch(xch:ref_number_key) = Level:Benign        
!                if p_web.gsv('LocalIMEI') = xch:ESN or  p_web.gsv('LocalIMEI') = xch:MSN THEN
!                    LocalModel        = Clip(xch:model_number) & ' - ' & Clip(xch:manufacturer)
!                    LimitModel        = xch:Model_Number
!                    LocalHandsetType  = 'EXCHANGE'
!                    LocalDateBooked   = format(job:date_booked,@d06)        !job:date_booked
!                    SendingResult    = 'PROCESS OK'
!                    EXIT 
!                END !if imei matches
!            END !if exchange fetched
!        END !if job:exchangeUnitNumber exists
!        
!        !check for the loan last
!        If job:loan_unit_number <> '' 
!            access:loan.clearkey(loa:ref_number_key)
!            loa:ref_number = job:loan_unit_number
!            if access:loan.fetch(loa:ref_number_key) = Level:Benign
!                if p_web.gsv('LocalIMEI') = loa:ESN or  p_web.gsv('LocalIMEI') = loa:MSN THEN
!                    LocalModel        = Clip(loa:model_number) & ' - ' & Clip(loa:manufacturer)
!                    LimitModel        = loa:Model_Number
!                    LocalHandsetType  = 'LOAN'
!                    LocalDateBooked   = format(job:date_booked,@d06)        !job:date_booked
!                    SendingResult    = 'PROCESS OK'
!                    EXIT                     
!                End  !if imei matches
!            end !if loan.fetched
!        End!If job:loan_unit_number
!    END !if serial number is original imei/ msn
!    
!    !NOT FOUND ANYWHERE - the defaults remain in place    
!    
!    EXIT
!endregion
UpdateJob           ROUTINE
    
    Do UpdateJobDetails !So I can use EXIT from there
 
    CASE SendingResult[1:5]

    of 'ERROR'
        !dont update the completed jobs list ...
        !nor reset the variables - let the user ammend them
        !just show the result
        p_web.Message('Alert',clip(SendingResult),p_web._MessageClass,Net:Send)    

    ELSE

        !show the result - this doesn't seem to work :-(
        p_web.Message('Alert',clip(SendingResult),p_web._MessageClass,Net:Send)    

!    !temp debugging - update the completed jobs list 
!    Access:Tagfile.PrimeRecord()
!    tag:sessionID = p_web.SessionID
!    tag:taggedValue = clip(p_web.gsv('LocalHandsetType'))&'/'&clip(LocalHandsetType)
!    tag:tagged = 1
!    access:Tagfile.insert()        
        
        
        !update job based on pass/fail and LocalHandsetType = Repair/Third Party Repair, LOAN, EXCHANGE, 2nd Exchange
        Case p_web.gsv('LocalHandsetType')
        of 'REPAIR' orof 'THIRD PARTY REPAIR'
            !if PassOrFail = 'PASS'
            if p_web.gsv('PassOrFail') = 'PASS'    
                do PassRepair
            ELSE
                do FailRepair
            END            
        of 'LOAN'
            IF p_web.gsv('PassOrFail') = 'PASS' THEN
                do PassLoan
            ELSE
                do FailLoan
            END 
        of 'EXCHANGE'
            if p_web.gsv('PassOrFail') = 'PASS' THEN
                do PassExchange
            ELSE
                do FailExchange
            END
        of '2ND EXCHANGE'
            if p_web.gsv('PassOrFail') = 'PASS'
                do Pass2ndExchange
            ELSE
                do Fail2ndExchange
            END
        ELSE
            SendingResult = 'ERROR - not found value of '&clip(LocalHandsetType)& ' aka '&clip(p_web.gsv('LocalHandsetType'))
        END !case LocalHandsetType
        
        
        CASE SendingResult[1:5]

        of 'ERROR'
            !dont update the completed jobs list ...
            !nor reset the varialbes - let the user ammend them
            !just show the result
            p_web.Message('Alert',clip(SendingResult),p_web._MessageClass,Net:Send)    
        of 'WARNI'
            Do WindowUpdate
            p_web.Message('Alert',clip(SendingResult),p_web._MessageClass,Net:Send)    
        ELSE
            Do WindowUpdate
        END !case sending result
    END    
    
    !reset the variables used to pass from QA_Reason/QApass - so do not do the restore mem bit again
    p_web.ssv('FailReason','')
    p_web.ssv('FailNotes','')    
    
    EXIT
    
WindowUpdate        ROUTINE
    
    !AT last can update the completed jobs list and reset variables
    Access:Tagfile.PrimeRecord()
    tag:sessionID = p_web.SessionID
    tag:taggedValue = clip(p_web.gsv('LocalJobNumber'))& '  -  ' & clip(p_web.gsv('PassOrFail'))
    tag:tagged = 1
    access:Tagfile.insert()        

    !reset variables for next use
    LocalDateBooked = ''
    LocalHandsetType= ''
    LocalModel      = '' 
    LocalIMEI       = ''
    LocalJobNumber  = ''
    p_web.ssv('LocalDateBooked' ,'')
    p_web.ssv('LocalHandsetType','')
    p_web.ssv('LocalModel      ','') 
    p_web.ssv('LocalIMEI       ','')
    p_web.ssv('LocalJobNumber  ','')

    EXIT

UpdateJobDetails    ROUTINE
    
    if p_web.gsv('LocalDateBooked') = 'NOT FOUND' or p_web.gsv('LocalDateBooked') = '' THEN
        SendingResult = 'ERROR: You must find a matching set of data before you can update the job.'
        EXIT
    END
    
!    if P_web.gsv('HIdeScrapChoice') = 'N'
!        if p_web.gsv('ScrapChoice') <> 'S' and p_web.gsv('ScrapChoice') <> 'R'
!            SendingResult = 'ERROR: You must say if you intend to Scrap or Restock the handset on this job.'
!            EXIT
!        END
!    END
    
    !refetch the jobs record, so it can be updated
    Access:jobs.ClearKey(job:Ref_Number_Key)
    job:Ref_Number = DEFORMAT(p_web.gsv('LocalJobNumber'))
    if access:jobs.fetch(job:Ref_Number_Key)
        SendingResult = 'ERROR: Job '&clip(p_web.gsv('LocalJobNumber')) & ' not found. Please check and try again.'
            !was 'ERROR: Job Record not found by job number '&clip(p_web.gsv('LocalJobNumber') & '. Please check your entry.')
        EXIT
    END
    
    p_web.FileToSessionQueue(JOBS)  !so it will be accessed in other procedures
    
    BHAddToDebugLog('QA Procedure: Job Number = ' & p_web.GSV('job:ref_Number'))

    !refetch the jobse record, so it can be updated
    Access:jobse.clearkey(jobe:RefNumberKey)
    jobe:RefNumber = job:Ref_Number
    if access:jobse.fetch(jobe:RefNumberKey)
        SendingResult = 'ERROR: Jobse Record not found by job number '&clip(p_web.gsv('LocalJobNumber')) & '. Please check and try again.'
        EXIT
    END
    p_web.FileToSessionQueue(JOBSE) !so it will be accessed in other procedures
    
    SendingResult = 'Got details: ' & p_web.gsv('FailNotes') & ' '  & p_web.gsv('FailReason')
    
    ! #13333 Load child files, needed for compulsory field checking (DBH: 17/07/2014)
    Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
    jobe2:RefNumber = job:Ref_Number
    IF (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
        p_web.FileToSessionQueue(JOBSE2)
    ELSE ! IF
    END ! IF

    Access:WEBJOB.ClearKey(wob:RefNumberKey)
    wob:RefNumber = job:Ref_Number
    IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
        p_web.FileToSessionQueue(WEBJOB)
    ELSE ! IF
    END ! IF

    Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
    jbn:RefNumber = job:Ref_Number
    IF (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)
        p_web.FileToSessionQueue(JOBNOTES)
    ELSE ! IF
    END ! IF

    EXIT
    
UpdateAllJobbyFiles ROUTINE     

!this should be called every time a change is made to one of these files
!importantly the GetStatus() call needs it
!it keeps the file record up to date with the session variables
    
    Access:JOBS.ClearKey(job:Ref_Number_Key)
    job:Ref_Number = p_web.GSV('job:Ref_Number')
    IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
        p_web.SessionQueueToFile(JOBS)
        Access:JOBS.TryUpdate()
    END
    
    Access:WEBJOB.ClearKey(wob:RefNumberKey)
    wob:RefNumber = p_web.GSV('job:Ref_Number')
    IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
        p_web.SessionQueueToFile(WEBJOB)
        Access:WEBJOB.TryUpdate()
    END    

    Access:jobse.clearkey(jobe:RefNumberKey)
    jobe:RefNumber = p_web.GSV('job:Ref_Number')
    if access:jobse.fetch(jobe:RefNumberKey) = Level:Benign
        p_web.SessionQueueToFile(JOBSE)
        Access:JOBSE.TryUpdate()
    END

    exit    
!Completing subroutines in here

CompleteJob         ROUTINE 

    !only fill in date IF it's not already filled in.
    !this should make sure the date doesn't change again when QA'd by RRC from ARC
    If job:Date_Completed = '' THEN
        
        job:date_completed = Today()
        job:time_completed = Clock()
        job:Despatched = 'REA'

        ! #13333 Save QA Dates (DBH: 16/07/2014)
        job:qa_passed      = 'YES'
        job:date_qa_passed = Today()
        job:time_qa_passed = Clock()
    End !If job:Date_Completed = ''
        
    !Write the date completed info into WEBJOBS -  (DBH: 14-10-2003)
    
!    Access:WEBJOB.Clearkey(wob:RefNumberKey)
!    wob:RefNumber = job:Ref_Number
!    If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!        !Found
!        wob:DateCompleted = job:Date_Completed
!        wob:TimeCompleted = job:Time_Completed
!        wob:Completed       = 'YES'
!        wob:ReadyToDespatch = 1
!        Access:WEBJOB.Update()
!    End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!this would be overwritten by what is held in session variables, so update the session vaiables instead
    
    p_web.ssv('wob:DateCompleted',job:Date_Completed)
    p_web.ssv('wob:TimeCompleted',job:Time_Completed)
    p_web.ssv('wob:Completed','YES')
    p_web.ssv('wob:ReadyToDespatch','1')
    
    ! Inserting (DBH 15/03/2007) # 8718 - Update the obf processed file accordingly
    Access:JOBSOBF.ClearKey(jof:RefNumberKey)
    jof:RefNumber = job:Ref_Number
    If Access:JOBSOBF.TryFetch(jof:RefNumberKey) = Level:Benign
        !Found
        jof:DateCompleted = job:Date_Completed
        jof:TimeCompleted = job:Time_Completed
        jof:HeadAccountNumber = wob:HeadAccountNumber
        jof:IMEINumber = job:ESN
        Access:JOBSOBF.Update()
    End ! If Access:JOBSOBF.TryFetch(jof:RefNumberKey) = Level:Benign

    job:completed = 'YES'
    job:Despatched = 'REA'
    Access:jobs.update()
    
    p_web.FileToSessionQueue(JOBS)  ! #13333 Put the saved fields back into the session. They must be being used later. (DBH: 17/07/2014)
    
    !Lookup current engineer and mark as escalated
    Access:JOBSENG.ClearKey(joe:UserCodeKey)
    joe:JobNumber     = job:Ref_Number
    joe:UserCode      = job:Engineer
    joe:DateAllocated = Today()
    Set(joe:UserCodeKey,joe:UserCodeKey)
    Loop
        If Access:JOBSENG.PREVIOUS()
           Break
        End !If
        If joe:JobNumber     <> job:Ref_Number       |
        Or joe:UserCode      <> job:Engineer      |
        Or joe:DateAllocated > Today()       |
            Then Break.  ! End If
        joe:Status = 'COMPLETED'
        joe:StatusDate = Today()
        joe:StatusTime = Clock()
        Access:JOBSENG.Update()
        Break
    End !Loop

    !Mark the job's exchange unit as being available
    do CompleteExchange        !SBA01app.dll
    
    !check for SMS due to BRS and liquid damage
    do BER_SMS
    
    EXIT
    
    
CompleteExchange  Routine 
    
    If job:Exchange_Unit_Number <> 0
        access:exchange_alias.clearkey(xch_ali:ref_number_key)
        xch_ali:ref_number  = job:Exchange_Unit_Number
        If access:Exchange_alias.tryfetch(xch_ali:ref_number_key) = Level:Benign
            If xch_ali:audit_number <> ''                                               !If this has an audit number
                access:excaudit.clearkey(exa:audit_number_key)                          !Mark the replacement as ready
                exa:stock_type  = xch_ali:stock_type                                    !To be returned to stock
                exa:audit_number    = xch_ali:audit_number
                If access:excaudit.tryfetch(exa:audit_number_key) = Level:Benign
                    access:exchange.clearkey(xch:ref_number_key)
                    xch:ref_number  = exa:stock_unit_number
                    If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                        get(exchhist,0)
                        if access:exchhist.primerecord() = level:benign
                            exh:ref_number   = xch:ref_number
                            exh:date          = today()
                            exh:time          = clock()
!                            access:users.clearkey(use:password_key)
!                            use:password = glo:password
!                            access:users.fetch(use:password_key)
                            exh:user = p_web.GSV('BookingUserCode')     !use:User_Code
                            exh:status        = 'REMOVED FROM AUDIT NUMBER: ' & Clip(xch:audit_number)
                            access:exchhist.insert()
                        end!if access:exchhist.primerecord() = level:benign
                        xch:audit_number    = ''
                        access:exchange.update()
                    End!If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                    access:exchange.clearkey(xch:ref_number_key)
                    xch:ref_number  = exa:replacement_unit_number
                    If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                        get(exchhist,0)
                        if access:exchhist.primerecord() = level:benign
                            exh:ref_number   = xch:ref_number
                            exh:date          = today()
                            exh:time          = clock()
!                            access:users.clearkey(use:password_key)
!                            use:password = glo:password
!                            access:users.fetch(use:password_key)
                            exh:user = p_web.GSV('BookingUserCode')     !use:User_Code
                            exh:status        = 'UNIT REPAIRED. (AUDIT:' &|
                                                Clip(exa:audit_number) & ')'
                            access:exchhist.insert()
                        end     !if access:exchhist.primerecord() = level:benign
                        xch:audit_number    = exa:audit_number
                        xch:available   = 'RTS'
                        xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                        access:exchange.update()
                    End!If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                End!If access:excaudit.tryfetch(exa:audit_number_key) = Level:Benign
            Else!If xch_ali:audit_number <> ''
                access:exchange.clearkey(xch:esn_available_key)
                xch:available  = 'REP'
                xch:stock_type = xch_ali:stock_type
                xch:esn        = job:esn
                if access:exchange.tryfetch(xch:esn_available_key) = Level:Benign
                    get(exchhist,0)
                    if access:exchhist.primerecord() = level:benign
                        exh:ref_number   = xch:ref_number
                        exh:date          = today()
                        exh:time          = clock()
!                        access:users.clearkey(use:password_key)
!                        use:password = glo:password
!                        access:users.fetch(use:password_key)
                        exh:user = p_web.GSV('BookingUserCode')     !use:User_Code
                        exh:status        = 'REPAIR COMPLETED'
                        access:exchhist.insert()
                    end!if access:exchhist.primerecord() = level:benign
                    xch:available = 'RTS'
                    xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                    access:exchange.update()
                end!if access:exchange.tryfetch(xch:esn_available_key)
            End!If xch:audit_number <> ''
        End!If access:Exchange_alias.tryfetch(xch_ali:ref_number_key) = Level:Benign

    End !If job:Exchange_Unit_Number <> 0
    
    EXIT    
    
ForceDespatch       ROUTINE
    
    !Force a job to be marked as despatched
    job:Date_Despatched     = Today()
    job:Consignment_Number  = 'N/A'
    job:Despatch_User    = p_web.GSV('BookingUserCode')     !use:User_Code

!this is multipe despatch processing and should be done on despatch not on QA.
!    If access:desbatch.primerecord() = Level:Benign
!        If access:desbatch.insert()
!            access:desbatch.cancelautoinc()
!        End  !If access:desbatch.insert()
!    End  !If access:desbatch.primerecord() = Level:Benign
!    job:Despatch_Number  = dbt:Batch_Number
    
    EXIT
        
BER_SMS           ROUTINE     !includes liquid damage
    
!used to be ExchangeScrap       ROUTINE        !always called - hijacked to also do BER setup
!now all stuff about exchange and scrap are removed...

  !reset the defaults
  p_web.ssv('BER_SMS_Send', 'N')
    
  !check for warranty part
  IF job:Warranty_Job = 'YES' THEN
     Access:REPTYDEF.CLEARKEY(rtd:WarManRepairTypeKey)
     rtd:Manufacturer = p_web.gsv('job:Manufacturer')
     rtd:Warranty = 'YES'
     rtd:Repair_Type = p_web.gsv('job:Warranty_Charge_Type')
     IF ~Access:REPTYDEF.Fetch(rtd:WarManRepairTypeKey) THEN
        if rtd:BER = 1 or rtd:BER = 4 then      !BER/Liquid Damage
            p_web.ssv('BER_SMS_Send',rtd:SMSSendType)
            !will be used later in  SendSMSText('J','Y',BER_SMS_Send) 
        END !if BER etc
     END !IF fetched
  END !IF warranty

  !check for chargeable part
  IF job:Chargeable_Job = 'YES' THEN
     Access:REPTYDEF.CLEARKEY(rtd:ChaManRepairTypeKey)
     rtd:Manufacturer = p_web.gsv('job:Manufacturer')
     rtd:Chargeable = 'YES'
     rtd:Repair_Type = p_web.gsv('job:Repair_Type')
     IF ~Access:REPTYDEF.Fetch(rtd:ChaManRepairTypeKey) THEN
        if rtd:BER = 1 or rtd:BER = 4 then      !BER/Liquid Damage
            IF p_web.gsv('BER_SMS_Send') = 'N' or p_web.gsv('BER_SMS_Send') = '' then   !only change it if it has not been set, or been set blank
                p_web.ssv('BER_SMS_Send',rtd:SMSSendType)
            ENd
            !will be used later in  SendSMSText('J','Y',BER_SMS_Send)    !beyond economic repair
        END !if BER etc
     END !IF fetch
  END !IF chargeable

  !TB012477 - SMS notification for BER/Liquid Damage  dealt with here  JC 13/04/12
  if p_web.gsv('BER_SMS_Send') <> 'N' and job:Who_Booked <> 'WEB' and jobe2:SMSNotification then
      SendSMSTEXT('J','Y',p_web.gsv('BER_SMS_Send'),P_web)
  END !Ifthe sms for ber or liquid damage needs to be sent    
  
  EXIT      
!pass and fail routines start in here

PassRepair          ROUTINE
    
    !ValidateAccessoriesPage will have been done and passed
    
    !o   If Manual QA then complete the job .
    if p_web.gsv('QAType') = 'MANUAL' THEN
            
        !Clarion Tip: The CompletionFieldCheck routine should do the validation for you             
        CompulsoryFieldCheck(p_web)
        
        !Failed Completion Validation
        !o	If the job cannot be completed because it fails completion validation, the display the QA Failure Page (as above).
        !Cannot do this - ask user to retry or fail        
        if (p_web.GSV('locCompleteRepair') <> '')
            SendingResult = 'ERROR - The repair cannot be completed because of the following reasons:<13,10,13,10>'&p_web.gSV('locCompleteRepair')&'<13,10>Please amend or fail this job.'
            CreateScript(packet,'alert("' & CLIP(BHStripReplace(SendingResult,'<13,10>','\n')) & '")')
            DO SendPacket
            exit
        end 
        
        do CompleteJob
        
    END !if manual
        
    SendingResult = 'sucesss'
    
    !(for Vodcom's turnaround report, the status HAS to change
    !to completed, first.)    
    GetStatus(705,1,'JOB',p_web)
    
    !update jobse before saving all files
    P_web.ssv('jobe:DespatchType','JOB')
    !TB13306 - job must also have date_completed set
    p_web.ssv('job:Date_Completed',today())
    
    PendingJob(p_web) ! #13346 Make sure job is prime for warranty process (DBH: 06/08/2014)
    
    
    ! #13346 Make sure job is primed for warranty process. Have to use session Q as the "jobby" routine will lose the info otherwise (DBH: 05/08/2014)
    If job:Warranty_Job = 'YES'
        If p_web.GSV('jobe:WarrantyClaimStatus') = ''
            p_web.SSV('jobe:WarrantyClaimStatus','PENDING')
        End !If jobe:WarrantyClaimStatus = ''
    End !If job:Warranty_Job = 'YES'    
    
    do UpdateAllJobbyFiles

    tmp:Paid = 0
    !final status depands on the courier and if it has been paid
    Access:COURIER.Clearkey(cou:Courier_Key)
    
    CASE p_web.gsv('jobe:DespatchType')
    OF 'JOB'
        cou:Courier = job:Courier
    OF 'EXC'
        cou:Courier = job:Exchange_Courier
    of 'LOA'
        cou:Courier = job:Loan_Courier
    end   
    !cou:Courier = job:Courier
    If Access:COURIER.Tryfetch(cou:Courier_Key) then
        !not found - remain unpaid
    ELSE
        !Found
        If job:Warranty_Job = 'YES'
            If job:EDI = 'FIN'
                tmp:Paid = 1
            End !If job:EDI = 'FIN'
        End !If job:Warranty_Job = 'YES'
        If job:Chargeable_Job = 'YES'
            !using existing pricing routine
            TotalPrice(p_web,'C',vat$,tot$,bal$)                
            IF deformat(tot$) = 0 or DEFORMAT(bal$) <= 0
               tmp:paid = 1
            END ! IF                
        End !If job:Chargeable_Job = 'YES'
    END !if courier fetched

    !!make sure all jobs are despatched
    do ForceDespatch
    

    Access:jobs.update()
    
    !have everything needed to set up final status
    if p_web.gsv('ExchangeAcc') = 'YES' THEN    !trade account exchange value was saved when found on first pass
        GetStatus(707,1,'JOB',p_web)
    ELSE
        !Has the OBF been validated?
        If jobe:OBFValidated
            GetStatus(710,1,'JOB',p_web)
        Else !If jobe:OBFValidated
            If job:Who_Booked = 'WEB' 
                !this is from a pup
                GetStatus(Sub(GETINI('RRC','StatusDespatchToPUP',,Clip(Path()) & '\SB2KDEF.INI'),1,3),0,'JOB',p_web)
            Else ! If job:Who_Booked = 'WEB'        
                If cou:CustomerCollection
                    If tmp:Paid
                        GetStatus(915,0,'JOB',p_web) !Paid Awaiting Collection
                    Else !If tmp:Paid
                        GetStatus(805,0,'JOB',p_web) !Ready To Collection
                    End !If tmp:Paid
                Else !If cou:CustomerCollection
                    If tmp:Paid
                        GetStatus(916,0,'JOB',p_web) !Paid Awaiting Despatch
                    Else !If tmp:Paid
                        GetStatus(810,0,'JOB',p_web) !Ready To Despatch
                    End !If tmp:Paid
                End !If cou:CustomerCollection
            End ! If job:Who_Booked = 'WEB'
        END !if OBF 
    END !if exchange account
    do UpdateAllJobbyFiles
        
    AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','RAPID QA UPDATE',p_web.gsv('QAType')&' QA PASSED')
    
    !Check if the job has been sent to the ARC, returned and still has accessories attached. If so display the message �The selected job has accessories that have been retained by the RRC�.
    SentToHub(p_web)    !existing procedure
    if p_web.GSV('SentToHub') = 1 then
        
        Access:JOBACC.Clearkey(jac:Ref_Number_Key)
        jac:Ref_Number = job:Ref_Number
        Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
        Loop ! Begin Loop
            If Access:JOBACC.Next() THEN BREAK.
            If jac:Ref_Number <> job:Ref_Number THEN BREAK.            
            If jac:Attached = 0
                SendingResult = 'ERROR: - The selected job has accessories that have been retained by the RRC. (Job has passed QA.)'
                Break
            End ! If jac:Attached = 0            
        End ! Loop
        
    End ! If p_web.gsv('SentToHub')
        
    EXIT
    
FailRepair          ROUTINE
    
    !Ask for QA Failure Reason will have been done stored in 'FailNotes', 'FailReason'
    
    if p_web.gsv('QAType') = 'MANUAL' THEN
        !o	IF Manual QA, then set the job�s status to 615
        GetStatus(615,1,'JOB',p_web)        
    ELSE        
        !o	If Electronic QA, then set the job�s Status to 625
        GetStatus(625,1,'JOB',p_web)
    END
    
    !update jobse before saving all files
    P_web.ssv('jobe:DespatchType','')

    do UpdateAllJobbyFiles
    
    !o	Append the above QA Failure Notes to the Engineer Notes on the job. 
    access:Jobnotes.clearkey(jbn:RefNumberKey)
    jbn:RefNumber = job:ref_number
    If access:jobnotes.fetch(jbn:RefNumberKey) = level:benign then
        !found the jobnotes
        If clip(jbn:Engineers_Notes) = '' then
            jbn:Engineers_Notes = clip(p_web.gsv('FailNotes'))
        Else
            jbn:Engineers_Notes = clip(jbn:Engineers_Notes) & '; ' & clip(p_web.gsv('FailNotes'))
        End !If clip(jbn:Engineers_Notes) = '' then
        access:Jobnotes.update()
    End !If access:jobnotes.fetch(jbn:RefNumberKey) = level:benign then
    
    !Update the job�s Audit Trail with �QA REJECTION {ELECTRONIC/MANUAL}: {REASON}�. Display the QA Failure Notes in the Audit notes.
    AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','QA REJECTION '&p_web.gsv('QAType')&': '&p_web.gsv('FailReason'),'QA FAILURE REASON: ' & clip(p_web.gsv('FailNotes')))    

   
    EXIT
    
PassLoan            ROUTINE

    !ValidateAccessoriesPage will have been done    

    if p_web.gsv('QAType') = 'MANUAL' THEN
        !o	If Manual QA set the job�s Loan Status to 110
        GetStatus(110,1,'LOA',p_web)        
    ELSE        
        !o	If Electronic QA set the job�s Loan Status to 620
        GetStatus(620,1,'LOA',p_web)
    END

    !update jobse before saving all files
    P_web.ssv('jobe:DespatchType','LOA')
    
    do UpdateAllJobbyFiles
    
!Passed Accessory Validation
!o	Add to the job�s Loan audit trail with �RAPID QA UPDATE: {ELECTRONIC/MANUAL} QA PASSED (LOA)�. Set the notes to �LOAN UNIT NUMBER: 123�
    AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'LOA','RAPID QA UPDATE: '&p_web.gsv('QAType')&': '&'QA PASSED (LOA)','LOAN UNIT NUMBER: '&P_web.gsv('job:Loan_Unit_Number'))

!o	Add to Loan unit history: �RAPID QA UPDATE: {ELECTRONIC/MANUAL} QA PASSED�
    access:loan.clearkey(loa:ref_number_key)
    loa:ref_number = job:loan_unit_number
    if access:loan.tryfetch(loa:ref_number_key) = Level:benign
        loa:available = 'LOA'
        loa:StatusChangeDate = today()
        access:loan.update()

        if access:loanhist.primerecord() = level:benign
            loh:ref_number    = job:loan_unit_number
            loh:date          = today()
            loh:time          = clock()
            loh:user          = p_web.GSV('BookingUserCode')     ! use:user_code
            loh:status        = 'RAPID QA UPDATE: '&p_web.gsv('QAType')&' QA PASSED'
            access:loanhist.insert()
        end!if access:exchhist.primerecord() = level:benign
    END !if loan fetched
    !o	Update the job so that the Loan Unit can be despatched .    
    Access:JOBSE.ClearKey(jobe:RefNumberKey)
    jobe:RefNumber = p_web.gsv('job:ref_number')
    if not Access:JOBSE.Fetch(jobe:RefNumberKey)
        jobe:Despatched = 'REA'
        jobe:DespatchType = 'LOA'
        Access:JOBSE.TryUpdate()
      
        Access:WEBJOB.Clearkey(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign

            wob:ReadyToDespatch = 1
            wob:DespatchCourier =  P_web.gsv('job:Loan_Courier')
            Access:WEBJOB.TryUpdate()

        End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        
    end
    
    EXIT
    
FailLoan            ROUTINE 
    
!o	Ask for QA Failure Reason will have been done
    if p_web.gsv('QAType') = 'MANUAL' THEN
        !o	IF Manual QA, then set the job�s status to 615
        GetStatus(615,1,'LOA',p_web)        
    ELSE        
        !o	If Electronic QA, then set the job�s Status to 625
        GetStatus(625,1,'LOA',p_web)
    END
    
    !update jobse before saving all files
    P_web.ssv('jobe:DespatchType','')
    
    do UpdateAllJobbyFiles
    
!o	Append the above QA Failure Notes to the Engineer Notes on the job.
    access:Jobnotes.clearkey(jbn:RefNumberKey)
    jbn:RefNumber = job:ref_number
    If access:jobnotes.fetch(jbn:RefNumberKey) = level:benign then
        !found the jobnotes
        If clip(jbn:Engineers_Notes) = '' then
            jbn:Engineers_Notes = clip(p_web.gsv('FailNotes'))
        Else
            jbn:Engineers_Notes = clip(jbn:Engineers_Notes) & '; ' & clip(p_web.gsv('FailNotes'))
        End !If clip(jbn:Engineers_Notes) = '' then
        access:Jobnotes.update()
    End !If access:jobnotes.fetch(jbn:RefNumberKey) = level:benign then

!    !o	Update the job�s Loan Audit Trail with �RAPID QA UPDATE: {ELECTRONIC/MANUAL} QA FAILED (LOA)�. Display �LOAN UNIT NUMBER: 123� and �QA FAILURE REASON: XXX� in the Audit notes.
    AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'LOA','RAPID QA UPDATE '&p_web.gsv('QAType')&' QA FAILED (LOA)','LOAN UNIT NUMBER: ' & p_web.gsv('job:Loan_Unit_Number')&'<13,10>QA FAILURE REASON: '&p_web.gsv('FailReason')& '; ' & clip(p_web.gsv('FailNotes')))

    !o	Update the loan unit history with �RAPID QA UPDATE: QA FAILED � {REASON}�
    access:loan.clearkey(loa:ref_number_key)
    loa:ref_number = job:loan_unit_number
    if access:loan.tryfetch(loa:ref_number_key) = Level:benign
        loa:available = 'QAF'
        loa:StatusChangeDate = today()
        access:loan.update()
        
        if access:loanhist.primerecord() = level:benign
            loh:ref_number    = loa:ref_number          !xch:ref_number
            loh:date          = today()
            loh:time          = clock()
            loh:user          = p_web.GSV('BookingUserCode')     ! use:user_code
            loh:status        = 'RAPID QA UPDATE: QA FAILED - ' & CLip(P_WEB.GSV('FailReason'))
            access:loanhist.insert()
        end!if access:exchhist.primerecord() = level:benign
    end!if access:exchange.tryfetch(xch:ref_number_key) = Level:benign
    
    EXIT
    
PassExchange        ROUTINE
    !ValidateAccessoriesPage will have been done
    
    !o	Add to the job�s exchange audit trail with �RAPID QA UPDATE: {ELECTRONIC/MANUAL} QA PASSED (EXC)�. Set the notes to �EXCHANGE UNIT NUMBER: 123�
    AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'EXC','RAPID QA UPDATE '&p_web.gsv('QAType')&' QA PASSED (EXC)','EXCHANGE UNIT NUMBER: ' & p_web.gsv('job:Exchange_Unit_Number'))

    !statuses
    if p_web.gsv('QAType') = 'MANUAL' THEN
        !o	If Manual QA set the job�s Exchange Status to 110
        GetStatus(110,1,'EXC',p_web)        
    ELSE        
        !o	If Electronic QA set the job�s Exchange Status to 620
        GetStatus(620,1,'EXC',p_web)
    END
    
    !update jobse before saving all files
    P_web.ssv('jobe:DespatchType','EXC')
    
    do UpdateAllJobbyFiles

    !o	Update the job so that the exchange unit appears in the job despatch table 
    Access:JOBSE.ClearKey(jobe:RefNumberKey)
    jobe:RefNumber = job:ref_number
    if not Access:JOBSE.Fetch(jobe:RefNumberKey)
        jobe:Despatched = 'REA'
        jobe:DespatchType = 'EXC'
        Access:JOBSE.TryUpdate()
      
        Access:WEBJOB.Clearkey(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign

            wob:ReadyToDespatch = 1
            wob:DespatchCourier = job:Exchange_Courier
            Access:WEBJOB.TryUpdate()

        End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        
    end

    !o	Add to exchange unit history: �RAPID QA UPDATE: {ELECTRONIC/MANUAL} QA PASSED�
    access:exchange.clearkey(xch:ref_number_key)
    xch:ref_number = job:exchange_unit_number
    if access:exchange.tryfetch(xch:ref_number_key) = Level:benign
        xch:available = 'EXC'    
        xch:StatusChangeDate = Today() 
        access:exchange.update()
        
        if access:exchhist.primerecord() = level:benign
            exh:ref_number    = xch:ref_number
            exh:date          = today()
            exh:time          = clock()
            exh:user          = p_web.GSV('BookingUserCode')     !use:User_Codeuse:user_code
            exh:status        = 'RAPID QA UPDATE: '&clip(p_web.gsv('QAType'))&' QA PASSED'
            access:exchhist.insert()
        end  !if access:exchhist.primerecord() = level:benign
        
    end  !if access:exchange.tryfetch(xch:ref_number_key) = Level:benign

    !FOLLOWING done on return
!o	Add an entry to the �Jobs Updated� list with type �QA PASS�. 
!o  Clear the screen and reset back to just the job number and serial number fields showing.
!o	Send the relevant SMS and Email. Loan
    
    
    EXIT
    
FailExchange        ROUTINE

!o	Ask for QA Failure Reason  will have been done

    !statuses
    if p_web.gsv('QAType') = 'MANUAL' THEN
        !o	IF Manual QA, then set the job�s Exchange Status to 615
        GetStatus(615,1,'EXC',p_web)        
    ELSE        
        !o	If Electronic QA, then set the job�s Exchange Status to 625
        GetStatus(625,1,'EXC',p_web)
    END
    
    !update jobse before saving all files
    P_web.ssv('jobe:DespatchType','')
    
    do UpdateAllJobbyFiles
    
    !o	Append the above QA Failure Notes to the Engineer Notes on the job.
    access:Jobnotes.clearkey(jbn:RefNumberKey)
    jbn:RefNumber = job:ref_number
    If access:jobnotes.fetch(jbn:RefNumberKey) = level:benign then
        !found the jobnotes
        If clip(jbn:Engineers_Notes) = '' then
            jbn:Engineers_Notes = clip(p_web.gsv('FailNotes'))
        Else
            jbn:Engineers_Notes = clip(jbn:Engineers_Notes) & '; ' & clip(p_web.gsv('FailNotes'))
        End !If clip(jbn:Engineers_Notes) = '' then
        access:Jobnotes.update()
    End !If access:jobnotes.fetch(jbn:RefNumberKey) = level:benign then
    
!o	Update the job�s Exchange Audit Trail with �RAPID QA UPDATE: {ELECTRONIC/MANUAL} QA FAILED (EXC)�. Display �EXCHANGE UNIT NUMBER: 123� and �QA FAILURE REASON: XXX� in the Audit notes.
    AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'EXC','RAPID QA UPDATE '&p_web.gsv('QAType')&' QA FAILED (EXC)' ,'EXCHANGE UNIT NUMBER: ' & p_web.gsv('job:Exchange_Unit_Number')&'<13,10>QA FAILURE REASON : '&clip(p_web.gsv('FailReason'))& '; ' & clip(p_web.gsv('FailNotes')))
    
!o	Change the Exchange Unit�s available flag  to �QAF�
    access:exchange.clearkey(xch:ref_number_key)
    xch:ref_number = job:exchange_unit_number
    if access:exchange.tryfetch(xch:ref_number_key) = Level:benign
        xch:available = 'QAF'
        xch:StatusChangeDate = Today() 
        access:exchange.update()
        
        !o	Update the exchange unit history with �RAPID QA UPDATE: QA FAILED � {REASON}�
        if access:exchhist.primerecord() = level:benign
            exh:ref_number    = xch:ref_number
            exh:date          = today()
            exh:time          = clock()
            exh:user          = p_web.GSV('BookingUserCode')     !use:User_Codeuse:user_code
            exh:status        = 'RAPID QA UPDATE: QA FAILED - '&clip(p_web.gsv('FailReason'))
            access:exchhist.insert()
        end  !if access:exchhist.primerecord() = level:benign
        
    end  !if access:exchange.tryfetch(xch:ref_number_key) = Level:benign

    !o	If the job�s Engineer Option set to �7 Day TAT� then change the exchange status to 126. Display a new page: 
    !check if this is a '7DAY TAT' as well as an exchange
    If p_web.gsv('jobe:Engineer48HourOption') = '3' THEN
        !yep - its a 7 day tat with an exchange unit so process new information
        GetStatus(126,1,'EXC',p_web)
        do UpdateAllJobbyFiles
        
        AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'EXC','EXCHANGE QA FAILED: ' & Clip(p_web.gsv('FailReason')),clip(P_web.gsv('FailNotes')))

    End !If jobe:Engineer48HourOption = 3 then
    
    
    EXIT
    

Pass2ndExchange     ROUTINE
    !ValidateAccessoriesPage will have been done    

    !statuses
    if p_web.gsv('QAType') = 'MANUAL' THEN
        !Manual QA Set the job�s 2nd Exchange Status to 708
        GetStatus(708,1,'2NE',p_web)        
    ELSE      
        !Electronic QA - Set the job�s 2nd Exchange Status to 620
        GetStatus(620,1,'2NE',p_web)
    END
    
    !update jobse before saving all files
    P_web.ssv('jobe:DespatchType','EXC')
    
    do UpdateAllJobbyFiles
    
    AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'2NE','RAPID QA UPDATE '&p_web.gsv('QAType')&' QA PASSED (EXC)','EXCHANGE UNIT NUMBER: ' & p_web.gsv('jobe:SecondExchangeNumber'))

    !o	Update the exchange unit�s history with �QA PASS ON JOB NO: XXX�
    access:exchange.clearkey(xch:ref_number_key)
    xch:ref_number = jobe:SecondExchangeNumber
    if access:exchange.tryfetch(xch:ref_number_key) = Level:benign
        xch:Available = 'EXC'
        xch:StatusChangeDate = Today() 
        access:exchange.update()
        
        if access:exchhist.primerecord() = level:benign
            exh:ref_number    = xch:ref_number
            exh:date          = today()
            exh:time          = clock()
            exh:user          = p_web.GSV('BookingUserCode')     !use:User_Codeuse:user_code
            exh:status        = 'QA PASS ON JOB NO: '&clip(p_web.gsv('job:Ref_Number'))
            access:exchhist.insert()
        end  !if access:exchhist.primerecord() = level:benign
        
    end  !if access:exchange.tryfetch(xch:ref_number_key) = Level:benign
    
    !o	Display message �The selected unit must now be restocked to national stores�
    SendingResult = upper('WARNING - The selected unit must now be restocked to national stores')
    
    EXIT
    
Fail2ndExchange     ROUTINE
    
!o	Ask for QA Failure Reason will have been done
!o	If Electronic QA, then set the job�s 2nd Exchange Status to 625
!o	If Manual QA, then set the job�s 2nd Exchange Status to 615
    !statuses
    if p_web.gsv('QAType') = 'MANUAL' THEN
        !Manual QA Set the job�s 2nd Exchange Status to 708
        GetStatus(625,1,'2NE',p_web)        
    ELSE      
        !Electronic QA - Set the job�s 2nd Exchange Status to 620
        GetStatus(615,1,'2NE',p_web)
    END
    
    !update jobse before saving all files
    P_web.ssv('jobe:DespatchType','')
    
    do UpdateAllJobbyFiles
    
    !o	Append the above QA Failure Notes to the Engineer Notes on the job.
    access:Jobnotes.clearkey(jbn:RefNumberKey)
    jbn:RefNumber = job:ref_number
    If access:jobnotes.fetch(jbn:RefNumberKey) = level:benign then
        !found the jobnotes
        If clip(jbn:Engineers_Notes) = '' then
            jbn:Engineers_Notes = clip(p_web.gsv('FailNotes'))
        Else
            jbn:Engineers_Notes = clip(jbn:Engineers_Notes) & '; ' & clip(p_web.gsv('FailNotes'))
        End !If clip(jbn:Engineers_Notes) = '' then
        access:Jobnotes.update()
    End !If access:jobnotes.fetch(jbn:RefNumberKey) = level:benign then
    
    !o	Update the job�s Exchange Audit Trail with �RAPID QA UPDATE: {ELECTRONIC/MANUAL} QA FAILED (EXC)�. Display �EXCHANGE UNIT NUMBER: 123� and �QA FAILURE REASON: XXX� in the Audit notes.    
    AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'2NE','RAPID QA UPDATE '&p_web.gsv('QAType')&' QA FAILED (EXC)','EXCHANGE UNIT NUMBER: ' & p_web.gsv('jobe:SecondExchangeNumber')&'<13,10>QA FAILURE REASON: '&p_web.gsv('FailReason')& '; ' & clip(p_web.gsv('FailNotes')))

    !o	Change the Exchange Unit�s available flag  to �QAF�
    !o	Update the exchange unit history with �RAPID QA UPDATE: QA FAILED � {REASON}�
    access:exchange.clearkey(xch:ref_number_key)
    xch:ref_number = jobe:SecondExchangeNumber
    if access:exchange.tryfetch(xch:ref_number_key) = Level:benign
        xch:available = 'QAF'
        xch:StatusChangeDate = Today() 
        access:exchange.update()
        
        if access:exchhist.primerecord() = level:benign
            exh:ref_number    = xch:ref_number
            exh:date          = today()
            exh:time          = clock()
            exh:user          = p_web.GSV('BookingUserCode')     !use:User_Codeuse:user_code
            exh:status        = 'RAPID QA UPDATE: QA FAILED � '&clip(p_web.gsv('FailReason'))
            access:exchhist.insert()
        end  !if access:exchhist.primerecord() = level:benign
        
    end  !if access:exchange.tryfetch(xch:ref_number_key) = Level:benign
    
    EXIT
OpenFiles  ROUTINE
  p_web._OpenFile(JOBNOTES)
  p_web._OpenFile(DESBATCH)
  p_web._OpenFile(JOBACC)
  p_web._OpenFile(REPTYDEF)
  p_web._OpenFile(EXCAUDIT)
  p_web._OpenFile(USERS)
  p_web._OpenFile(EXCHANGE_ALIAS)
  p_web._OpenFile(EXCHHIST)
  p_web._OpenFile(MODELNUM)
  p_web._OpenFile(JOBSENG)
  p_web._OpenFile(JOBSOBF)
  p_web._OpenFile(COURIER)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(EXCHANGE)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(TagFile)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBNOTES)
  p_Web._CloseFile(DESBATCH)
  p_Web._CloseFile(JOBACC)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(EXCAUDIT)
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(EXCHANGE_ALIAS)
  p_Web._CloseFile(EXCHHIST)
  p_Web._CloseFile(MODELNUM)
  p_Web._CloseFile(JOBSENG)
  p_Web._CloseFile(JOBSOBF)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(TagFile)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
      !set variables used by FaultCodes screen if it gets called
      p_web.ssv('ReturnURL','QAProcedure')
      p_web.SSV('MSNValidation',0)
      p_web.ssv('HideTools',1)
  
      !variables used by QA_pass if called
      p_web.ssv('HideMySave',TRUE)
      p_web.ssv('HideMyCancel',TRUE)
  p_web.SetValue('QAProcedure_form:inited_',1)
  do RestoreMem

CancelForm  Routine
  !clear all sessionvariables as we close the form
  p_web.ssv('LocalDateBooked','')
  p_web.ssv('LocalHandsetType','')
  p_web.ssv('LocalModel','')
  p_web.ssv('LimitModel','')
  p_web.ssv('LocalIMEI','')
  p_web.ssv('LocalJobNumber','')
  p_web.ssv('QAType','')
  p_web.ssv('SendingResult','')
  p_web.ssv('PassOrFail','')
  p_web.ssv('FilterLocation','')
  p_web.ssv('FailReason','')
  p_web.ssv('FailNotes','')
  
  !unset variable used by FaultCodes screen
  p_web.ssv('ReturnURL','')
  p_web.SSV('MSNValidation','')
  p_web.ssv('HideTools','')
  
  !variables used by QA_pass if called
  p_web.ssv('HideMySave','')
  p_web.ssv('HideMyCancel','')
  
  !and the local variables?
  LocalDateBooked = ''
  LocalHandsetType = ''
  LocalModel = ''
  LimitModel = ''
  LocalIMEI = ''
  LocalJobNumber = ''
  QAType = ''
  SendingResult = ''
  PassOrFail = ''
  FilterLocation = ''
  FailReason = ''
  FailNotes = '' 
  tmp:Paid = ''
  
  !clear out the tag file used to hold jobs updated
  LOOP
      Access:TagFile.clearkey(tag:keyTagged)
      tag:sessionID = p_web.SessionID
      set(tag:keyTagged,tag:keyTagged)
      if Access:TagFile.next() then break.
      if tag:sessionID <> p_web.SessionID then break.
      Access:TagFile.DeleteRecord(0)
  END !loop

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('QAType',QAType)
  p_web.SetSessionValue('LocalJobNumber',LocalJobNumber)
  p_web.SetSessionValue('LocalIMEI',LocalIMEI)
  p_web.SetSessionValue('LocalModel',LocalModel)
  p_web.SetSessionValue('LocalHandsetType',LocalHandsetType)
  p_web.SetSessionValue('LocalDateBooked',LocalDateBooked)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('QAType')
    QAType = p_web.GetValue('QAType')
    p_web.SetSessionValue('QAType',QAType)
  End
  if p_web.IfExistsValue('LocalJobNumber')
    LocalJobNumber = p_web.GetValue('LocalJobNumber')
    p_web.SetSessionValue('LocalJobNumber',LocalJobNumber)
  End
  if p_web.IfExistsValue('LocalIMEI')
    LocalIMEI = p_web.GetValue('LocalIMEI')
    p_web.SetSessionValue('LocalIMEI',LocalIMEI)
  End
  if p_web.IfExistsValue('LocalModel')
    LocalModel = p_web.GetValue('LocalModel')
    p_web.SetSessionValue('LocalModel',LocalModel)
  End
  if p_web.IfExistsValue('LocalHandsetType')
    LocalHandsetType = p_web.GetValue('LocalHandsetType')
    p_web.SetSessionValue('LocalHandsetType',LocalHandsetType)
  End
  if p_web.IfExistsValue('LocalDateBooked')
    LocalDateBooked = p_web.GetValue('LocalDateBooked')
    p_web.SetSessionValue('LocalDateBooked',LocalDateBooked)
  End
  if p_web.IfExistsValue('PassOrFail')
      PassOrFail = p_web.GetValue('PassOrFail')
      p_web.SetSessionValue('PassOrFail',PassOrFail)
  End
  !if p_web.IfExistsValue('FailReason')
  !    FailReason = p_web.GetValue('FailReason')
  !    p_web.SetSessionValue('FailReason',FailReason)
  !End
  !if p_web.IfExistsValue('FailNotes')
  !    FailNotes = p_web.GetValue('FailNotes')
  !    p_web.SetSessionValue('FailNotes',FailNotes)
  !End
  if p_web.IfExistsValue('FilterLocation')
      FilterLocation = p_web.GetValue('FilterLocation')
      p_web.SetSessionValue('FilterLocation',FilterLocation)
  End
  !  if p_web.gsv('FailReason') <> '' THEN    
  !      we have the result of QA_FailReason or QAPASS
  !      PUTINI('RETURNING','FailReason',p_web.GSV('FailReason'),CLIP(PATH()) & '\DEBUG.LOG')
  !      do UpdateJob
  !      show the result - this does not work on entry?
  !      p_web.Message('Alert2',clip(p_web.gsv('SendingResult')),p_web._MessageClass,Net:Send)    
  !  END
  

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('QAProcedure_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    IF (p_web.GetValue('firsttime') = 1)
        ! #13444 Clear the temp list if this is the first time opening this screen (DBH: 02/12/2014)
        ClearTagFile(p_web)
    END ! IF
    
    
    access:users.clearkey(use:User_Code_Key)
  use:User_Code = p_web.GSV('BookingUserCode')     !use:User_Code at logon
  access:users.fetch(use:User_Code_Key)
  
  !set up the filter so can only allocate to users at the same location
  p_web.ssv('FilterLocation',use:Location)
  FilterLocation = use:location
      p_web.site.CancelButton.TextValue = 'Close'
    if p_web.gsv('FailReason') <> '' THEN    
      !we have the result of QA_FailReason or QAPASS
        PUTINI('RETURNING','FailReason',p_web.GSV('FailReason'),CLIP(PATH()) & '\DEBUG.LOG')
        do UpdateJob
      !show the result - this does not work on entry?
        p_web.Message('Alert2',clip(p_web.gsv('SendingResult')),p_web._MessageClass,Net:Send)    
    END
    
    ! Pass a return URL because this is called from different placed
    IF (p_web.IfExistsValue('QAProcReturnURL'))
        p_web.StoreValue('QAProcReturnURL')
    END !I F
    
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 QAType = p_web.RestoreValue('QAType')
 LocalJobNumber = p_web.RestoreValue('LocalJobNumber')
 LocalIMEI = p_web.RestoreValue('LocalIMEI')
 LocalModel = p_web.RestoreValue('LocalModel')
 LocalHandsetType = p_web.RestoreValue('LocalHandsetType')
 LocalDateBooked = p_web.RestoreValue('LocalDateBooked')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('QAProcReturnURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('QAProcedure_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('QAProcedure_ChainTo')
    loc:formaction = p_web.GetSessionValue('QAProcedure_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('QAProcReturnURL')
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="QAProcedure" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="QAProcedure" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="QAProcedure" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('QA Procedure') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('QA Procedure',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_QAProcedure">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_QAProcedure" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_QAProcedure')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Select QA Type') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Enter Job Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Jobs Updated') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_QAProcedure')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_QAProcedure'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('QAProcedure_BrowseTagFile_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_QAProcedure')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Select QA Type') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_QAProcedure_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Select QA Type')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Select QA Type')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Select QA Type')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Select QA Type')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::QAType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::QAType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::QAType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Enter Job Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_QAProcedure_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Job Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Job Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Job Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Job Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::LocalJobNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::LocalJobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::LocalJobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::LocalIMEI
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::LocalIMEI
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::LocalIMEI
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::LocalModel
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::LocalModel
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::LocalModel
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::LocalHandsetType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::LocalHandsetType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::LocalHandsetType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::LocalDateBooked
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::LocalDateBooked
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::LocalDateBooked
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::ButtonFaultCodes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::ButtonFaultCodes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::ButtonQA_Pass
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::ButtonQA_Pass
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::ButtonQAFail
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::ButtonQAFail
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Jobs Updated') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_QAProcedure_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Jobs Updated')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Jobs Updated')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Jobs Updated')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Jobs Updated')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::BrowseTagFile
      do Value::BrowseTagFile
      do Comment::BrowseTagFile
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::QAType  Routine
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('QAType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('QA Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::QAType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('QAType',p_web.GetValue('NewValue'))
    QAType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::QAType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('QAType',p_web.GetValue('Value'))
    QAType = p_web.GetValue('Value')
  End
      p_web.ssv('QAType',QAType)
  do Value::QAType
  do SendAlert
  do Prompt::LocalModel
  do Value::LocalModel  !1
  do Prompt::LocalDateBooked
  do Value::LocalDateBooked  !1
  do Prompt::LocalHandsetType
  do Value::LocalHandsetType  !1
  do Prompt::LocalIMEI
  do Value::LocalIMEI  !1
  do Comment::LocalIMEI
  do Prompt::LocalJobNumber
  do Value::LocalJobNumber  !1
  do Comment::LocalJobNumber

Value::QAType  Routine
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('QAType') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- QAType
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('QAType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(p_web.gsv('QAType') <> '','disabled','')
    if p_web.GetSessionValue('QAType') = 'MANUAL'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''QAType'',''qaprocedure_qatype_value'',1,'''&clip('MANUAL')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('QAType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','QAType',clip('MANUAL'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'QAType_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Manual') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(p_web.gsv('QAType') <> '','disabled','')
    if p_web.GetSessionValue('QAType') = 'ELECTRONIC'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''QAType'',''qaprocedure_qatype_value'',1,'''&clip('ELECTRONIC')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('QAType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','QAType',clip('ELECTRONIC'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'QAType_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Electronic') & '<13,10>'
    packet = clip(packet) & p_web.br
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('QAProcedure_' & p_web._nocolon('QAType') & '_value')

Comment::QAType  Routine
    loc:comment = ''
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('QAType') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::LocalJobNumber  Routine
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('LocalJobNumber') & '_prompt',Choose(p_web.GSV('QAType') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Job Number')
  If p_web.GSV('QAType') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('QAProcedure_' & p_web._nocolon('LocalJobNumber') & '_prompt')

Validate::LocalJobNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('LocalJobNumber',p_web.GetValue('NewValue'))
    LocalJobNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::LocalJobNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('LocalJobNumber',p_web.GetValue('Value'))
    LocalJobNumber = p_web.GetValue('Value')
  End
      !p_web.ssv('LocalJobNumber',LocalJobNumber)  ! Why is this here? (DBH: 20/11/2014)
      do LookupJob
  do Value::LocalJobNumber
  do SendAlert

Value::LocalJobNumber  Routine
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('LocalJobNumber') & '_value',Choose(p_web.GSV('QAType') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('QAType') = '')
  ! --- STRING --- LocalJobNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('LocalJobNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''LocalJobNumber'',''qaprocedure_localjobnumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','LocalJobNumber',p_web.GetSessionValueFormat('LocalJobNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('QAProcedure_' & p_web._nocolon('LocalJobNumber') & '_value')

Comment::LocalJobNumber  Routine
    loc:comment = p_web.Translate('Enter Value and press [TAB]')
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('LocalJobNumber') & '_comment',Choose(p_web.GSV('QAType') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('QAType') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('QAProcedure_' & p_web._nocolon('LocalJobNumber') & '_comment')

Prompt::LocalIMEI  Routine
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('LocalIMEI') & '_prompt',Choose(p_web.GSV('QAType') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Serial Number')
  If p_web.GSV('QAType') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('QAProcedure_' & p_web._nocolon('LocalIMEI') & '_prompt')

Validate::LocalIMEI  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('LocalIMEI',p_web.GetValue('NewValue'))
    LocalIMEI = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::LocalIMEI
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('LocalIMEI',p_web.GetValue('Value'))
    LocalIMEI = p_web.GetValue('Value')
  End
    LocalIMEI = Upper(LocalIMEI)
    p_web.SetSessionValue('LocalIMEI',LocalIMEI)
  !      p_web.ssv('LocalIMEI',LocalIMEI) ! Why is this here? (DBH: 20/11/2014)
      Do LookupJob
  do Value::LocalIMEI
  do SendAlert
  do Value::ButtonFaultCodes  !1
  do Value::ButtonQAFail  !1
  do Prompt::LocalHandsetType
  do Value::LocalHandsetType  !1
  do Prompt::LocalModel
  do Value::LocalModel  !1
  do Prompt::LocalDateBooked
  do Value::LocalDateBooked  !1
  do Value::ButtonQA_Pass  !1

Value::LocalIMEI  Routine
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('LocalIMEI') & '_value',Choose(p_web.GSV('QAType') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('QAType') = '')
  ! --- STRING --- LocalIMEI
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('LocalIMEI')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''LocalIMEI'',''qaprocedure_localimei_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('LocalIMEI')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','LocalIMEI',p_web.GetSessionValueFormat('LocalIMEI'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('QAProcedure_' & p_web._nocolon('LocalIMEI') & '_value')

Comment::LocalIMEI  Routine
    loc:comment = p_web.Translate('Enter Value and press [TAB]')
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('LocalIMEI') & '_comment',Choose(p_web.GSV('QAType') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('QAType') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('QAProcedure_' & p_web._nocolon('LocalIMEI') & '_comment')

Prompt::LocalModel  Routine
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('LocalModel') & '_prompt',Choose(p_web.GSV('LocalImei') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Model')
  If p_web.GSV('LocalImei') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('QAProcedure_' & p_web._nocolon('LocalModel') & '_prompt')

Validate::LocalModel  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('LocalModel',p_web.GetValue('NewValue'))
    LocalModel = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::LocalModel
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('LocalModel',p_web.GetValue('Value'))
    LocalModel = p_web.GetValue('Value')
  End

Value::LocalModel  Routine
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('LocalModel') & '_value',Choose(p_web.GSV('LocalImei') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('LocalImei') = '')
  ! --- DISPLAY --- LocalModel
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('LocalModel'),) & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('QAProcedure_' & p_web._nocolon('LocalModel') & '_value')

Comment::LocalModel  Routine
    loc:comment = ''
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('LocalModel') & '_comment',Choose(p_web.GSV('LocalImei') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('LocalImei') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('QAProcedure_' & p_web._nocolon('LocalModel') & '_comment')

Prompt::LocalHandsetType  Routine
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('LocalHandsetType') & '_prompt',Choose(p_web.GSV('LocalImei') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Handset Type')
  If p_web.GSV('LocalImei') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('QAProcedure_' & p_web._nocolon('LocalHandsetType') & '_prompt')

Validate::LocalHandsetType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('LocalHandsetType',p_web.GetValue('NewValue'))
    LocalHandsetType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::LocalHandsetType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('LocalHandsetType',p_web.GetValue('Value'))
    LocalHandsetType = p_web.GetValue('Value')
  End

Value::LocalHandsetType  Routine
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('LocalHandsetType') & '_value',Choose(p_web.GSV('LocalImei') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('LocalImei') = '')
  ! --- DISPLAY --- LocalHandsetType
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('LocalHandsetType'),) & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('QAProcedure_' & p_web._nocolon('LocalHandsetType') & '_value')

Comment::LocalHandsetType  Routine
    loc:comment = ''
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('LocalHandsetType') & '_comment',Choose(p_web.GSV('LocalImei') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('LocalImei') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::LocalDateBooked  Routine
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('LocalDateBooked') & '_prompt',Choose(p_web.GSV('LocalImei') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Date Booked')
  If p_web.GSV('LocalImei') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('QAProcedure_' & p_web._nocolon('LocalDateBooked') & '_prompt')

Validate::LocalDateBooked  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('LocalDateBooked',p_web.GetValue('NewValue'))
    LocalDateBooked = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::LocalDateBooked
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('LocalDateBooked',p_web.GetValue('Value'))
    LocalDateBooked = p_web.GetValue('Value')
  End

Value::LocalDateBooked  Routine
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('LocalDateBooked') & '_value',Choose(p_web.GSV('LocalImei') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('LocalImei') = '')
  ! --- DISPLAY --- LocalDateBooked
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('LocalDateBooked'),) & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('QAProcedure_' & p_web._nocolon('LocalDateBooked') & '_value')

Comment::LocalDateBooked  Routine
    loc:comment = ''
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('LocalDateBooked') & '_comment',Choose(p_web.GSV('LocalImei') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('LocalImei') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::ButtonFaultCodes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('ButtonFaultCodes',p_web.GetValue('NewValue'))
    do Value::ButtonFaultCodes
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::ButtonFaultCodes  Routine
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('ButtonFaultCodes') & '_value',Choose(p_web.GSV('LocalImei') = '' OR p_web.GSV('LocalModel') = 'NOT FOUND','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('LocalImei') = '' OR p_web.GSV('LocalModel') = 'NOT FOUND')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ButtonFaultCodes','Edit Fault Codes','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('JobFaultCodes')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('QAProcedure_' & p_web._nocolon('ButtonFaultCodes') & '_value')

Comment::ButtonFaultCodes  Routine
    loc:comment = ''
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('ButtonFaultCodes') & '_comment',Choose(p_web.GSV('LocalImei') = '' OR p_web.GSV('LocalModel') = 'NOT FOUND','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('LocalImei') = '' OR p_web.GSV('LocalModel') = 'NOT FOUND'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::ButtonQA_Pass  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('ButtonQA_Pass',p_web.GetValue('NewValue'))
    do Value::ButtonQA_Pass
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
      PassOrFail = 'PASS'
    p_web.ssv('PassOrFail',PassOrFail)  
  do Value::ButtonQA_Pass
  do SendAlert

Value::ButtonQA_Pass  Routine
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('ButtonQA_Pass') & '_value',Choose(p_web.GSV('LocalImei') = ''  OR p_web.GSV('LocalModel') = 'NOT FOUND','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('LocalImei') = ''  OR p_web.GSV('LocalModel') = 'NOT FOUND')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''ButtonQA_Pass'',''qaprocedure_buttonqa_pass_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ButtonQA_Pass','QA Pass','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('QAPass')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('QAProcedure_' & p_web._nocolon('ButtonQA_Pass') & '_value')

Comment::ButtonQA_Pass  Routine
    loc:comment = ''
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('ButtonQA_Pass') & '_comment',Choose(p_web.GSV('LocalImei') = ''  OR p_web.GSV('LocalModel') = 'NOT FOUND','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('LocalImei') = ''  OR p_web.GSV('LocalModel') = 'NOT FOUND'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::ButtonQAFail  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('ButtonQAFail',p_web.GetValue('NewValue'))
    do Value::ButtonQAFail
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    PassOrFail = 'FAIL'
  
    p_web.ssv('PassOrFail',PassOrFail)
  do Value::ButtonQAFail
  do SendAlert
  do Value::BrowseTagFile  !1
  do Value::ButtonQA_Pass  !1
  do Prompt::LocalDateBooked
  do Value::LocalDateBooked  !1
  do Prompt::LocalHandsetType
  do Value::LocalHandsetType  !1
  do Prompt::LocalIMEI
  do Value::LocalIMEI  !1
  do Comment::LocalIMEI
  do Prompt::LocalJobNumber
  do Value::LocalJobNumber  !1
  do Comment::LocalJobNumber
  do Prompt::LocalModel
  do Value::LocalModel  !1
  do Comment::LocalModel
  do Value::ButtonFaultCodes  !1

Value::ButtonQAFail  Routine
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('ButtonQAFail') & '_value',Choose(p_web.GSV('LocalImei') = ''  OR p_web.GSV('LocalModel') = 'NOT FOUND','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('LocalImei') = ''  OR p_web.GSV('LocalModel') = 'NOT FOUND')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''ButtonQAFail'',''qaprocedure_buttonqafail_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ButtonQAFail','QA Fail','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('QA_FailReason')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('QAProcedure_' & p_web._nocolon('ButtonQAFail') & '_value')

Comment::ButtonQAFail  Routine
    loc:comment = ''
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('ButtonQAFail') & '_comment',Choose(p_web.GSV('LocalImei') = ''  OR p_web.GSV('LocalModel') = 'NOT FOUND','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('LocalImei') = ''  OR p_web.GSV('LocalModel') = 'NOT FOUND'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::BrowseTagFile  Routine
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('BrowseTagFile') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('This Session')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::BrowseTagFile  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('BrowseTagFile',p_web.GetValue('NewValue'))
    do Value::BrowseTagFile
  Else
    p_web.StoreValue('tag:sessionID')
  End

Value::BrowseTagFile  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseTagFile --
  p_web.SetValue('BrowseTagFile:NoForm',1)
  p_web.SetValue('BrowseTagFile:FormName',loc:formname)
  p_web.SetValue('BrowseTagFile:parentIs','Form')
  p_web.SetValue('_parentProc','QAProcedure')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('QAProcedure_BrowseTagFile_embedded_div')&'"><!-- Net:BrowseTagFile --></div><13,10>'
    p_web._DivHeader('QAProcedure_' & lower('BrowseTagFile') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('QAProcedure_' & lower('BrowseTagFile') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseTagFile --><13,10>'
  end
  do SendPacket

Comment::BrowseTagFile  Routine
    loc:comment = ''
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('BrowseTagFile') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('QAProcedure_QAType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::QAType
      else
        do Value::QAType
      end
  of lower('QAProcedure_LocalJobNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::LocalJobNumber
      else
        do Value::LocalJobNumber
      end
  of lower('QAProcedure_LocalIMEI_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::LocalIMEI
      else
        do Value::LocalIMEI
      end
  of lower('QAProcedure_ButtonQA_Pass_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::ButtonQA_Pass
      else
        do Value::ButtonQA_Pass
      end
  of lower('QAProcedure_ButtonQAFail_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::ButtonQAFail
      else
        do Value::ButtonQAFail
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('QAProcedure_form:ready_',1)
  p_web.SetSessionValue('QAProcedure_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_QAProcedure',0)

PreCopy  Routine
  p_web.SetValue('QAProcedure_form:ready_',1)
  p_web.SetSessionValue('QAProcedure_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_QAProcedure',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('QAProcedure_form:ready_',1)
  p_web.SetSessionValue('QAProcedure_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('QAProcedure:Primed',0)

PreDelete       Routine
  p_web.SetValue('QAProcedure_form:ready_',1)
  p_web.SetSessionValue('QAProcedure_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('QAProcedure:Primed',0)
  p_web.setsessionvalue('showtab_QAProcedure',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('QAProcedure_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('QAProcedure_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
      If not (p_web.GSV('QAType') = '')
          LocalIMEI = Upper(LocalIMEI)
          p_web.SetSessionValue('LocalIMEI',LocalIMEI)
        If loc:Invalid <> '' then exit.
      End
  ! tab = 3
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('QAProcedure:Primed',0)
  p_web.StoreValue('QAType')
  p_web.StoreValue('LocalJobNumber')
  p_web.StoreValue('LocalIMEI')
  p_web.StoreValue('LocalModel')
  p_web.StoreValue('LocalHandsetType')
  p_web.StoreValue('LocalDateBooked')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('tmp:Paid',tmp:Paid) ! BYTE
     p_web.SSV('LocalDateBooked',LocalDateBooked) ! STRING(10)
     p_web.SSV('LocalHandsetType',LocalHandsetType) ! STRING(20)
     p_web.SSV('LocalModel',LocalModel) ! STRING(20)
     p_web.SSV('LimitModel',LimitModel) ! STRING(30)
     p_web.SSV('LocalIMEI',LocalIMEI) ! STRING(20)
     p_web.SSV('LocalJobNumber',LocalJobNumber) ! LONG
     p_web.SSV('QAType',QAType) ! STRING(10)
     p_web.SSV('SendingResult',SendingResult) ! STRING(255)
     p_web.SSV('PassOrFail',PassOrFail) ! STRING(4)
     p_web.SSV('FilterLocation',FilterLocation) ! STRING(30)
     p_web.SSV('FailReason',FailReason) ! STRING(30)
     p_web.SSV('FailNotes',FailNotes) ! STRING(255)
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     tmp:Paid = p_web.GSV('tmp:Paid') ! BYTE
     LocalDateBooked = p_web.GSV('LocalDateBooked') ! STRING(10)
     LocalHandsetType = p_web.GSV('LocalHandsetType') ! STRING(20)
     LocalModel = p_web.GSV('LocalModel') ! STRING(20)
     LimitModel = p_web.GSV('LimitModel') ! STRING(30)
     LocalIMEI = p_web.GSV('LocalIMEI') ! STRING(20)
     LocalJobNumber = p_web.GSV('LocalJobNumber') ! LONG
     QAType = p_web.GSV('QAType') ! STRING(10)
     SendingResult = p_web.GSV('SendingResult') ! STRING(255)
     PassOrFail = p_web.GSV('PassOrFail') ! STRING(4)
     FilterLocation = p_web.GSV('FilterLocation') ! STRING(30)
     FailReason = p_web.GSV('FailReason') ! STRING(30)
     FailNotes = p_web.GSV('FailNotes') ! STRING(255)
