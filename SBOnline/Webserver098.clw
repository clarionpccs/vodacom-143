

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER098.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
LockRecord           PROCEDURE  (fJobNumber,fSessionValue,fType) ! Declare Procedure
JOBSLOCK::State  USHORT
FilesOpened     BYTE(0)

  CODE
    do openFiles

    access:JOBSLOCK.clearkey(lock:jobnumberkey)
    lock:jobNumber = fjobnumber
    if (access:jobslock.tryfetch(lock:jobnumberkey) = level:benign)
        if (fType = 0) ! Add Lock
            lock:datelocked = today()
            lock:timelocked = clock()
            lock:SessionValue = fsessionvalue
            access:JOBSLOCK.tryUpdate()
        else
            access:jobslock.deleterecord(0)
        end
        
    else
        if (fType = 0) ! Add Lock
            if (access:jobslock.primerecord() = level:benign)
                lock:jobNumber = fjobnumber
                lock:sessionValue = fsessionvalue
                if (access:jobslock.tryinsert() = level:benign)
                else
                    access:jobslock.cancelautoinc()
                end
            end
        end
    end
    do closefiles
SaveFiles  ROUTINE
  JOBSLOCK::State = Access:JOBSLOCK.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF JOBSLOCK::State <> 0
    Access:JOBSLOCK.RestoreFile(JOBSLOCK::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:JOBSLOCK.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSLOCK.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:JOBSLOCK.Close
     FilesOpened = False
  END
