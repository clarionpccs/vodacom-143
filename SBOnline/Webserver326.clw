

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER326.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER285.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER327.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER330.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER332.INC'),ONCE        !Req'd for module callout resolution
                     END


WarrantyBatchProcessing PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locBatchProcessingType LONG                                !
locFilterByDate      LONG                                  !
locFilterMonth       STRING(30)                            !
locFilterYear        STRING(30)                            !
                    MAP
SetFilterDate           PROCEDURE()
TagColour               PROCEDURE(LONG pType)
                    END ! MAP
FilesOpened     Long
SBO_WarrantyClaims::State  USHORT
WEBJOB::State  USHORT
JOBSE::State  USHORT
JOBS::State  USHORT
JOBSWARR::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  GlobalErrors.SetProcedureName('WarrantyBatchProcessing')
  loc:formname = 'WarrantyBatchProcessing_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('WarrantyBatchProcessing','')
    p_web._DivHeader('WarrantyBatchProcessing',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferWarrantyBatchProcessing',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferWarrantyBatchProcessing',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferWarrantyBatchProcessing',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_WarrantyBatchProcessing',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferWarrantyBatchProcessing',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_WarrantyBatchProcessing',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(SBO_WarrantyClaims)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(JOBSWARR)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(SBO_WarrantyClaims)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(JOBSWARR)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('WarrantyBatchProcessing_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('locFilterYear')
    p_web.SetPicture('locFilterYear','@n04')
  End
  p_web.SetSessionPicture('locFilterYear','@n04')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locBatchProcessingType',locBatchProcessingType)
  p_web.SetSessionValue('locFilterByDate',locFilterByDate)
  p_web.SetSessionValue('locFilterMonth',locFilterMonth)
  p_web.SetSessionValue('locFilterYear',locFilterYear)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locBatchProcessingType')
    locBatchProcessingType = p_web.GetValue('locBatchProcessingType')
    p_web.SetSessionValue('locBatchProcessingType',locBatchProcessingType)
  End
  if p_web.IfExistsValue('locFilterByDate')
    locFilterByDate = p_web.GetValue('locFilterByDate')
    p_web.SetSessionValue('locFilterByDate',locFilterByDate)
  End
  if p_web.IfExistsValue('locFilterMonth')
    locFilterMonth = p_web.GetValue('locFilterMonth')
    p_web.SetSessionValue('locFilterMonth',locFilterMonth)
  End
  if p_web.IfExistsValue('locFilterYear')
    locFilterYear = p_web.dformat(clip(p_web.GetValue('locFilterYear')),'@n04')
    p_web.SetSessionValue('locFilterYear',locFilterYear)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('WarrantyBatchProcessing_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
      p_web.site.CancelButton.TextValue = 'Close'
      p_web.site.CancelButton.Image = '/images/psave.png'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locBatchProcessingType = p_web.RestoreValue('locBatchProcessingType')
 locFilterByDate = p_web.RestoreValue('locFilterByDate')
 locFilterMonth = p_web.RestoreValue('locFilterMonth')
 locFilterYear = p_web.RestoreValue('locFilterYear')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'WarrantyClaims'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('WarrantyBatchProcessing_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('WarrantyBatchProcessing_ChainTo')
    loc:formaction = p_web.GetSessionValue('WarrantyBatchProcessing_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'WarrantyClaims'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="WarrantyBatchProcessing" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="WarrantyBatchProcessing" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="WarrantyBatchProcessing" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Warranty Batch Processing') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Warranty Batch Processing',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_WarrantyBatchProcessing">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_WarrantyBatchProcessing" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_WarrantyBatchProcessing')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Criteria') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Claims') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_WarrantyBatchProcessing')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_WarrantyBatchProcessing'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('WarrantyBatchProcessing_WarrantyBatchProcessingBrowse_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('WarrantyBatchProcessing_WarrantyBatchCostsPage_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_WarrantyBatchProcessing')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Criteria') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_WarrantyBatchProcessing_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locBatchProcessingType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locBatchProcessingType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locBatchProcessingType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Claims') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_WarrantyBatchProcessing_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Claims')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Claims')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Claims')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Claims')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFilterByDate
      do Value::locFilterByDate
      do Comment::locFilterByDate
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFilterMonth
      do Value::locFilterMonth
      do Comment::locFilterMonth
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFilterYear
      do Value::locFilterYear
      do Comment::locFilterYear
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwWarrantyBatch
      do Comment::brwWarrantyBatch
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::costsPage
      do Comment::costsPage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnRefreshCosts
      do Comment::btnRefreshCosts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_WarrantyBatchProcessing_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::txtMarkJob
      do Comment::txtMarkJob
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnMarkGreen
      do Comment::btnMarkGreen
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnMarkBlue
      do Comment::btnMarkBlue
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnMarkRed
      do Comment::btnMarkRed
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnTagGreen
      do Comment::btnTagGreen
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnTagBlue
      do Comment::btnTagBlue
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnTagRed
      do Comment::btnTagRed
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnTagAll
      do Comment::btnTagAll
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnUnTagAll
      do Comment::btnUnTagAll
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnInvoiceImport
      do Comment::btnInvoiceImport
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnCreateInvoice
      do Comment::btnCreateInvoice
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnReprintInvoice
      do Comment::btnReprintInvoice
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locBatchProcessingType  Routine
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('locBatchProcessingType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Batch Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locBatchProcessingType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locBatchProcessingType',p_web.GetValue('NewValue'))
    locBatchProcessingType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locBatchProcessingType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locBatchProcessingType',p_web.GetValue('Value'))
    locBatchProcessingType = p_web.GetValue('Value')
  End
  do Value::locBatchProcessingType
  do SendAlert
  do Value::brwWarrantyBatch  !1
  do Value::costsPage  !1
  do Value::btnMarkRed  !1
  do Value::btnMarkBlue  !1
  do Value::btnMarkGreen  !1
  do Value::btnTagAll  !1
  do Value::btnTagBlue  !1
  do Value::btnTagGreen  !1
  do Value::btnTagRed  !1
  do Value::btnUnTagAll  !1
  do Value::txtMarkJob  !1
  do Value::btnCreateInvoice  !1
  do Value::btnRefreshCosts  !1
  do Value::btnReprintInvoice  !1
  do Prompt::locFilterByDate
  do Value::locFilterByDate  !1
  do Prompt::locFilterMonth
  do Value::locFilterMonth  !1
  do Prompt::locFilterYear
  do Value::locFilterYear  !1
  do Value::btnInvoiceImport  !1

Value::locBatchProcessingType  Routine
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('locBatchProcessingType') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- locBatchProcessingType
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locBatchProcessingType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locBatchProcessingType') = 0
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locBatchProcessingType'',''warrantybatchprocessing_locbatchprocessingtype_value'',1,'''&clip(0)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locBatchProcessingType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locBatchProcessingType',clip(0),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locBatchProcessingType_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Awaiting Processing') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locBatchProcessingType') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locBatchProcessingType'',''warrantybatchprocessing_locbatchprocessingtype_value'',1,'''&clip(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locBatchProcessingType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locBatchProcessingType',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locBatchProcessingType_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Reconciled') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyBatchProcessing_' & p_web._nocolon('locBatchProcessingType') & '_value')

Comment::locBatchProcessingType  Routine
    loc:comment = ''
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('locBatchProcessingType') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locFilterByDate  Routine
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('locFilterByDate') & '_prompt',Choose(p_web.GSV('locBatchProcessingType') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Filter By Date')
  If p_web.GSV('locBatchProcessingType') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyBatchProcessing_' & p_web._nocolon('locFilterByDate') & '_prompt')

Validate::locFilterByDate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFilterByDate',p_web.GetValue('NewValue'))
    locFilterByDate = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFilterByDate
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locFilterByDate',p_web.GetValue('Value'))
    locFilterByDate = p_web.GetValue('Value')
  End
    SetFilterDate()  
  do Value::locFilterByDate
  do SendAlert
  do Value::locFilterMonth  !1
  do Value::locFilterYear  !1
  do Value::brwWarrantyBatch  !1

Value::locFilterByDate  Routine
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('locFilterByDate') & '_value',Choose(p_web.GSV('locBatchProcessingType') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locBatchProcessingType') = 0)
  ! --- CHECKBOX --- locFilterByDate
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locFilterByDate'',''warrantybatchprocessing_locfilterbydate_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locFilterByDate')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locFilterByDate') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locFilterByDate',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyBatchProcessing_' & p_web._nocolon('locFilterByDate') & '_value')

Comment::locFilterByDate  Routine
    loc:comment = ''
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('locFilterByDate') & '_comment',Choose(p_web.GSV('locBatchProcessingType') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locBatchProcessingType') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locFilterMonth  Routine
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('locFilterMonth') & '_prompt',Choose(p_web.GSV('locFilterByDate') = 0 OR p_web.GSV('locBatchProcessingType') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Month')
  If p_web.GSV('locFilterByDate') = 0 OR p_web.GSV('locBatchProcessingType') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyBatchProcessing_' & p_web._nocolon('locFilterMonth') & '_prompt')

Validate::locFilterMonth  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFilterMonth',p_web.GetValue('NewValue'))
    locFilterMonth = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFilterMonth
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locFilterMonth',p_web.GetValue('Value'))
    locFilterMonth = p_web.GetValue('Value')
  End
    SetFilterDate()  
  do Value::locFilterMonth
  do SendAlert
  do Value::brwWarrantyBatch  !1

Value::locFilterMonth  Routine
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('locFilterMonth') & '_value',Choose(p_web.GSV('locFilterByDate') = 0 OR p_web.GSV('locBatchProcessingType') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locFilterByDate') = 0 OR p_web.GSV('locBatchProcessingType') = 0)
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locFilterMonth')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locFilterMonth'',''warrantybatchprocessing_locfiltermonth_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locFilterMonth')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('locFilterMonth',loc:fieldclass,loc:readonly,,100,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locFilterMonth') = 0
    p_web.SetSessionValue('locFilterMonth','01 January')
  end
    packet = clip(packet) & p_web.CreateOption('01 January','01 January',choose('01 January' = p_web.getsessionvalue('locFilterMonth')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('02 February','02 February',choose('02 February' = p_web.getsessionvalue('locFilterMonth')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('03 March','03 March',choose('03 March' = p_web.getsessionvalue('locFilterMonth')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('04 April','04 April',choose('04 April' = p_web.getsessionvalue('locFilterMonth')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('05 May','05 May',choose('05 May' = p_web.getsessionvalue('locFilterMonth')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('06 June','06 June',choose('06 June' = p_web.getsessionvalue('locFilterMonth')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('07 July','07 July',choose('07 July' = p_web.getsessionvalue('locFilterMonth')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('08 August','08 August',choose('08 August' = p_web.getsessionvalue('locFilterMonth')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('09 September','09 September',choose('09 September' = p_web.getsessionvalue('locFilterMonth')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('10 October','10 October',choose('10 October' = p_web.getsessionvalue('locFilterMonth')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('11 November','11 November',choose('11 November' = p_web.getsessionvalue('locFilterMonth')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('12 December','12 December',choose('12 December' = p_web.getsessionvalue('locFilterMonth')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyBatchProcessing_' & p_web._nocolon('locFilterMonth') & '_value')

Comment::locFilterMonth  Routine
    loc:comment = ''
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('locFilterMonth') & '_comment',Choose(p_web.GSV('locFilterByDate') = 0 OR p_web.GSV('locBatchProcessingType') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locFilterByDate') = 0 OR p_web.GSV('locBatchProcessingType') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locFilterYear  Routine
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('locFilterYear') & '_prompt',Choose(p_web.GSV('locFilterByDate') = 0 OR p_web.GSV('locBatchProcessingType') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Year')
  If p_web.GSV('locFilterByDate') = 0 OR p_web.GSV('locBatchProcessingType') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyBatchProcessing_' & p_web._nocolon('locFilterYear') & '_prompt')

Validate::locFilterYear  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFilterYear',p_web.GetValue('NewValue'))
    locFilterYear = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFilterYear
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locFilterYear',p_web.dFormat(p_web.GetValue('Value'),'@n04'))
    locFilterYear = p_web.Dformat(p_web.GetValue('Value'),'@n04') !
  End
    SetFilterDate()  
  do Value::locFilterYear
  do SendAlert
  do Value::brwWarrantyBatch  !1

Value::locFilterYear  Routine
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('locFilterYear') & '_value',Choose(p_web.GSV('locFilterByDate') = 0 OR p_web.GSV('locBatchProcessingType') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locFilterByDate') = 0 OR p_web.GSV('locBatchProcessingType') = 0)
  ! --- STRING --- locFilterYear
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locFilterYear')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(4) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locFilterYear'',''warrantybatchprocessing_locfilteryear_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locFilterYear')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locFilterYear',p_web.GetSessionValue('locFilterYear'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n04',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyBatchProcessing_' & p_web._nocolon('locFilterYear') & '_value')

Comment::locFilterYear  Routine
      loc:comment = ''
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('locFilterYear') & '_comment',Choose(p_web.GSV('locFilterByDate') = 0 OR p_web.GSV('locBatchProcessingType') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locFilterByDate') = 0 OR p_web.GSV('locBatchProcessingType') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::brwWarrantyBatch  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwWarrantyBatch',p_web.GetValue('NewValue'))
    do Value::brwWarrantyBatch
  Else
    p_web.StoreValue('sbojow:RecordNumber')
  End
  do SendAlert
  do Value::btnMarkRed  !1
  do Value::btnReprintInvoice  !1
  do Value::btnMarkGreen  !1
  do Value::btnMarkRed  !1
  do Value::btnMarkBlue  !1

Value::brwWarrantyBatch  Routine
  loc:extra = ''
  ! --- BROWSE ---  WarrantyBatchProcessingBrowse --
  p_web.SetValue('WarrantyBatchProcessingBrowse:NoForm',1)
  p_web.SetValue('WarrantyBatchProcessingBrowse:FormName',loc:formname)
  p_web.SetValue('WarrantyBatchProcessingBrowse:parentIs','Form')
  p_web.SetValue('_parentProc','WarrantyBatchProcessing')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('WarrantyBatchProcessing_WarrantyBatchProcessingBrowse_embedded_div')&'"><!-- Net:WarrantyBatchProcessingBrowse --></div><13,10>'
    p_web._DivHeader('WarrantyBatchProcessing_' & lower('WarrantyBatchProcessingBrowse') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('WarrantyBatchProcessing_' & lower('WarrantyBatchProcessingBrowse') & '_value')
  else
    packet = clip(packet) & '<!-- Net:WarrantyBatchProcessingBrowse --><13,10>'
  end
  do SendPacket

Comment::brwWarrantyBatch  Routine
    loc:comment = ''
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('brwWarrantyBatch') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::costsPage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('costsPage',p_web.GetValue('NewValue'))
    do Value::costsPage
  End

Value::costsPage  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('locBatchProcessingType') = 1,1,0))
  ! --- BROWSE ---  WarrantyBatchCostsPage --
  p_web.SetValue('WarrantyBatchCostsPage:NoForm',1)
  p_web.SetValue('WarrantyBatchCostsPage:FormName',loc:formname)
  p_web.SetValue('WarrantyBatchCostsPage:parentIs','Form')
  p_web.SetValue('_parentProc','WarrantyBatchProcessing')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('WarrantyBatchProcessing_WarrantyBatchCostsPage_embedded_div')&'"><!-- Net:WarrantyBatchCostsPage --></div><13,10>'
    p_web._DivHeader('WarrantyBatchProcessing_' & lower('WarrantyBatchCostsPage') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('WarrantyBatchProcessing_' & lower('WarrantyBatchCostsPage') & '_value')
  else
    packet = clip(packet) & '<!-- Net:WarrantyBatchCostsPage --><13,10>'
  end
  do SendPacket

Comment::costsPage  Routine
    loc:comment = ''
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('costsPage') & '_comment',Choose(p_web.GSV('locBatchProcessingType') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locBatchProcessingType') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnRefreshCosts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnRefreshCosts',p_web.GetValue('NewValue'))
    do Value::btnRefreshCosts
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do SendAlert
  do Value::costsPage  !1

Value::btnRefreshCosts  Routine
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('btnRefreshCosts') & '_value',Choose(p_web.GSV('locBatchProcessingType') = 1 AND p_web.GSV('locClaims') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locBatchProcessingType') = 1 AND p_web.GSV('locClaims') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnRefreshCosts'',''warrantybatchprocessing_btnrefreshcosts_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnRefreshCosts','Refresh Values','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyBatchProcessing_' & p_web._nocolon('btnRefreshCosts') & '_value')

Comment::btnRefreshCosts  Routine
    loc:comment = ''
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('btnRefreshCosts') & '_comment',Choose(p_web.GSV('locBatchProcessingType') = 1 AND p_web.GSV('locClaims') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locBatchProcessingType') = 1 AND p_web.GSV('locClaims') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::txtMarkJob  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtMarkJob',p_web.GetValue('NewValue'))
    do Value::txtMarkJob
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtMarkJob  Routine
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('txtMarkJob') & '_value',Choose(p_web.GSV('locBatchProcessingType') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locBatchProcessingType') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.Translate('Mark Job:',) & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyBatchProcessing_' & p_web._nocolon('txtMarkJob') & '_value')

Comment::txtMarkJob  Routine
    loc:comment = ''
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('txtMarkJob') & '_comment',Choose(p_web.GSV('locBatchProcessingType') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locBatchProcessingType') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnMarkGreen  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnMarkGreen',p_web.GetValue('NewValue'))
    do Value::btnMarkGreen
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    Access:SBO_WarrantyClaims.ClearKey(sbojow:RecordNumberKey)
    sbojow:RecordNumber = p_web.GSV('sbojow:RecordNumber')
    IF (Access:SBO_WarrantyClaims.TryFetch(sbojow:RecordNumberKey) = Level:Benign)
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = sbojow:JobNumber
        IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
            wob:ReconciledMarker = CHOOSE(wob:ReconciledMarker = 1,0,1)
            Access:WEBJOB.TryUpdate()
        END ! IF
    END ! IF
    
  do SendAlert
  do Value::brwWarrantyBatch  !1

Value::btnMarkGreen  Routine
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('btnMarkGreen') & '_value',Choose(p_web.GSV('locBatchProcessingType') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locBatchProcessingType') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnMarkGreen'',''warrantybatchprocessing_btnmarkgreen_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnMarkGreen','','LookupButton',loc:formname,,,,loc:javascript,0,'/images/tag_green.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyBatchProcessing_' & p_web._nocolon('btnMarkGreen') & '_value')

Comment::btnMarkGreen  Routine
    loc:comment = ''
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('btnMarkGreen') & '_comment',Choose(p_web.GSV('locBatchProcessingType') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locBatchProcessingType') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnMarkBlue  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnMarkBlue',p_web.GetValue('NewValue'))
    do Value::btnMarkBlue
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    Access:SBO_WarrantyClaims.ClearKey(sbojow:RecordNumberKey)
    sbojow:RecordNumber = p_web.GSV('sbojow:RecordNumber')
    IF (Access:SBO_WarrantyClaims.TryFetch(sbojow:RecordNumberKey) = Level:Benign)
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = sbojow:JobNumber
        IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
            wob:ReconciledMarker = CHOOSE(wob:ReconciledMarker = 2,0,2)
            Access:WEBJOB.TryUpdate()
        END ! IF
    END ! IF
  do SendAlert
  do Value::brwWarrantyBatch  !1

Value::btnMarkBlue  Routine
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('btnMarkBlue') & '_value',Choose(p_web.GSV('locBatchProcessingType') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locBatchProcessingType') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnMarkBlue'',''warrantybatchprocessing_btnmarkblue_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnMarkBlue','','LookupButton',loc:formname,,,,loc:javascript,0,'/images/tag_blue.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyBatchProcessing_' & p_web._nocolon('btnMarkBlue') & '_value')

Comment::btnMarkBlue  Routine
    loc:comment = ''
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('btnMarkBlue') & '_comment',Choose(p_web.GSV('locBatchProcessingType') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locBatchProcessingType') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnMarkRed  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnMarkRed',p_web.GetValue('NewValue'))
    do Value::btnMarkRed
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    Access:SBO_WarrantyClaims.ClearKey(sbojow:RecordNumberKey)
    sbojow:RecordNumber = p_web.GSV('sbojow:RecordNumber')
    IF (Access:SBO_WarrantyClaims.TryFetch(sbojow:RecordNumberKey) = Level:Benign)
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = sbojow:JobNumber
        IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
            wob:ReconciledMarker = CHOOSE(wob:ReconciledMarker = 3,0,3)
            Access:WEBJOB.TryUpdate()
        END ! IF
    END ! IF
  do SendAlert
  do Value::brwWarrantyBatch  !1

Value::btnMarkRed  Routine
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('btnMarkRed') & '_value',Choose(p_web.GSV('locBatchProcessingType') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locBatchProcessingType') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnMarkRed'',''warrantybatchprocessing_btnmarkred_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnMarkRed','','LookupButton',loc:formname,,,,loc:javascript,0,'/images/tag_red.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyBatchProcessing_' & p_web._nocolon('btnMarkRed') & '_value')

Comment::btnMarkRed  Routine
    loc:comment = ''
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('btnMarkRed') & '_comment',Choose(p_web.GSV('locBatchProcessingType') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locBatchProcessingType') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnTagGreen  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnTagGreen',p_web.GetValue('NewValue'))
    do Value::btnTagGreen
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    TagColour(1)  
  do SendAlert
  do Value::brwWarrantyBatch  !1

Value::btnTagGreen  Routine
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('btnTagGreen') & '_value',Choose(p_web.GSV('locBatchProcessingType') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locBatchProcessingType') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnTagGreen'',''warrantybatchprocessing_btntaggreen_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnTagGreen','Tag Green','MainButton',loc:formname,,,,loc:javascript,0,'/images/tag_green.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyBatchProcessing_' & p_web._nocolon('btnTagGreen') & '_value')

Comment::btnTagGreen  Routine
    loc:comment = ''
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('btnTagGreen') & '_comment',Choose(p_web.GSV('locBatchProcessingType') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locBatchProcessingType') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnTagBlue  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnTagBlue',p_web.GetValue('NewValue'))
    do Value::btnTagBlue
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    TagColour(2)  
  do SendAlert
  do Value::brwWarrantyBatch  !1

Value::btnTagBlue  Routine
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('btnTagBlue') & '_value',Choose(p_web.GSV('locBatchProcessingType') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locBatchProcessingType') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnTagBlue'',''warrantybatchprocessing_btntagblue_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnTagBlue','Tag Blue','MainButton',loc:formname,,,,loc:javascript,0,'/images/tag_blue.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyBatchProcessing_' & p_web._nocolon('btnTagBlue') & '_value')

Comment::btnTagBlue  Routine
    loc:comment = ''
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('btnTagBlue') & '_comment',Choose(p_web.GSV('locBatchProcessingType') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locBatchProcessingType') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnTagRed  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnTagRed',p_web.GetValue('NewValue'))
    do Value::btnTagRed
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    TagColour(3)  
  do SendAlert
  do Value::brwWarrantyBatch  !1

Value::btnTagRed  Routine
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('btnTagRed') & '_value',Choose(p_web.GSV('locBatchProcessingType') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locBatchProcessingType') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnTagRed'',''warrantybatchprocessing_btntagred_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnTagRed','Tag Red','MainButton',loc:formname,,,,loc:javascript,0,'/images/tag_red.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyBatchProcessing_' & p_web._nocolon('btnTagRed') & '_value')

Comment::btnTagRed  Routine
    loc:comment = ''
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('btnTagRed') & '_comment',Choose(p_web.GSV('locBatchProcessingType') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locBatchProcessingType') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnTagAll  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnTagAll',p_web.GetValue('NewValue'))
    do Value::btnTagAll
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  TagColour(0)
  do SendAlert
  do Value::brwWarrantyBatch  !1

Value::btnTagAll  Routine
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('btnTagAll') & '_value',Choose(p_web.GSV('locBatchProcessingType') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locBatchProcessingType') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnTagAll'',''warrantybatchprocessing_btntagall_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnTagAll','Tag All','MainButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyBatchProcessing_' & p_web._nocolon('btnTagAll') & '_value')

Comment::btnTagAll  Routine
    loc:comment = ''
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('btnTagAll') & '_comment',Choose(p_web.GSV('locBatchProcessingType') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locBatchProcessingType') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnUnTagAll  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnUnTagAll',p_web.GetValue('NewValue'))
    do Value::btnUnTagAll
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
      ClearTagFile(p_web)
  do SendAlert
  do Value::brwWarrantyBatch  !1

Value::btnUnTagAll  Routine
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('btnUnTagAll') & '_value',Choose(p_web.GSV('locBatchProcessingType') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locBatchProcessingType') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnUnTagAll'',''warrantybatchprocessing_btnuntagall_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnUnTagAll','Untag All','MainButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyBatchProcessing_' & p_web._nocolon('btnUnTagAll') & '_value')

Comment::btnUnTagAll  Routine
    loc:comment = ''
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('btnUnTagAll') & '_comment',Choose(p_web.GSV('locBatchProcessingType') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locBatchProcessingType') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnInvoiceImport  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnInvoiceImport',p_web.GetValue('NewValue'))
    do Value::btnInvoiceImport
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnInvoiceImport  Routine
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('btnInvoiceImport') & '_value',Choose(p_web.GSV('locBatchProcessingType') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locBatchProcessingType') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnInvoiceImport','Reconcile - Inv. Import','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('ImportFile?' &'ProcessType=RRCWarrantyInvoiceImport&ReturnURL=WarrantyBatchProcessing&RedirectURL=PrintRRCWarrantyInvoice')) & ''','''&clip('_self')&''')',loc:javascript,0,'/images/money.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyBatchProcessing_' & p_web._nocolon('btnInvoiceImport') & '_value')

Comment::btnInvoiceImport  Routine
    loc:comment = ''
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('btnInvoiceImport') & '_comment',Choose(p_web.GSV('locBatchProcessingType') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locBatchProcessingType') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnCreateInvoice  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnCreateInvoice',p_web.GetValue('NewValue'))
    do Value::btnCreateInvoice
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnCreateInvoice  Routine
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('btnCreateInvoice') & '_value',Choose(p_web.GSV('locBatchProcessingType') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locBatchProcessingType') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnCreateInvoice','Create Invoice','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PageProcess?' &'ProcessType=RRCWarrantyInvoice&ReturnURL=WarrantyBatchProcessing&RedirectURL=PrintRRCWarrantyInvoice')) & ''','''&clip('_self')&''')',loc:javascript,0,'/images/money.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyBatchProcessing_' & p_web._nocolon('btnCreateInvoice') & '_value')

Comment::btnCreateInvoice  Routine
    loc:comment = ''
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('btnCreateInvoice') & '_comment',Choose(p_web.GSV('locBatchProcessingType') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locBatchProcessingType') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnReprintInvoice  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnReprintInvoice',p_web.GetValue('NewValue'))
    do Value::btnReprintInvoice
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnReprintInvoice  Routine
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('btnReprintInvoice') & '_value',Choose(p_web.GSV('locBatchProcessingType') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locBatchProcessingType') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&clip('mbWarrantyBatch_ReprintInvoice(' & p_web.GSV('jow:RecordNumber') & ')')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnReprintInvoice','Reprint Invoice','button-entryfield',loc:formname,,,,loc:javascript,0,'/images/printer.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyBatchProcessing_' & p_web._nocolon('btnReprintInvoice') & '_value')

Comment::btnReprintInvoice  Routine
    loc:comment = ''
  p_web._DivHeader('WarrantyBatchProcessing_' & p_web._nocolon('btnReprintInvoice') & '_comment',Choose(p_web.GSV('locBatchProcessingType') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locBatchProcessingType') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('WarrantyBatchProcessing_locBatchProcessingType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locBatchProcessingType
      else
        do Value::locBatchProcessingType
      end
  of lower('WarrantyBatchProcessing_locFilterByDate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locFilterByDate
      else
        do Value::locFilterByDate
      end
  of lower('WarrantyBatchProcessing_locFilterMonth_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locFilterMonth
      else
        do Value::locFilterMonth
      end
  of lower('WarrantyBatchProcessing_locFilterYear_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locFilterYear
      else
        do Value::locFilterYear
      end
  of lower('WarrantyBatchProcessing_WarrantyBatchProcessingBrowse_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::brwWarrantyBatch
      else
        do Value::brwWarrantyBatch
      end
  of lower('WarrantyBatchProcessing_btnRefreshCosts_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnRefreshCosts
      else
        do Value::btnRefreshCosts
      end
  of lower('WarrantyBatchProcessing_btnMarkGreen_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnMarkGreen
      else
        do Value::btnMarkGreen
      end
  of lower('WarrantyBatchProcessing_btnMarkBlue_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnMarkBlue
      else
        do Value::btnMarkBlue
      end
  of lower('WarrantyBatchProcessing_btnMarkRed_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnMarkRed
      else
        do Value::btnMarkRed
      end
  of lower('WarrantyBatchProcessing_btnTagGreen_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnTagGreen
      else
        do Value::btnTagGreen
      end
  of lower('WarrantyBatchProcessing_btnTagBlue_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnTagBlue
      else
        do Value::btnTagBlue
      end
  of lower('WarrantyBatchProcessing_btnTagRed_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnTagRed
      else
        do Value::btnTagRed
      end
  of lower('WarrantyBatchProcessing_btnTagAll_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnTagAll
      else
        do Value::btnTagAll
      end
  of lower('WarrantyBatchProcessing_btnUnTagAll_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnUnTagAll
      else
        do Value::btnUnTagAll
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('WarrantyBatchProcessing_form:ready_',1)
  p_web.SetSessionValue('WarrantyBatchProcessing_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_WarrantyBatchProcessing',0)

PreCopy  Routine
  p_web.SetValue('WarrantyBatchProcessing_form:ready_',1)
  p_web.SetSessionValue('WarrantyBatchProcessing_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_WarrantyBatchProcessing',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('WarrantyBatchProcessing_form:ready_',1)
  p_web.SetSessionValue('WarrantyBatchProcessing_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('WarrantyBatchProcessing:Primed',0)

PreDelete       Routine
  p_web.SetValue('WarrantyBatchProcessing_form:ready_',1)
  p_web.SetSessionValue('WarrantyBatchProcessing_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('WarrantyBatchProcessing:Primed',0)
  p_web.setsessionvalue('showtab_WarrantyBatchProcessing',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
      If(p_web.GSV('locBatchProcessingType') = 0)
          If p_web.IfExistsValue('locFilterByDate') = 0
            p_web.SetValue('locFilterByDate',0)
            locFilterByDate = 0
          End
      End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('WarrantyBatchProcessing_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('WarrantyBatchProcessing_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 3
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('WarrantyBatchProcessing:Primed',0)
  p_web.StoreValue('locBatchProcessingType')
  p_web.StoreValue('locFilterByDate')
  p_web.StoreValue('locFilterMonth')
  p_web.StoreValue('locFilterYear')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('locBatchProcessingType',locBatchProcessingType) ! LONG
     p_web.SSV('locFilterByDate',locFilterByDate) ! LONG
     p_web.SSV('locFilterMonth',locFilterMonth) ! STRING(30)
     p_web.SSV('locFilterYear',locFilterYear) ! STRING(30)
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     locBatchProcessingType = p_web.GSV('locBatchProcessingType') ! LONG
     locFilterByDate = p_web.GSV('locFilterByDate') ! LONG
     locFilterMonth = p_web.GSV('locFilterMonth') ! STRING(30)
     locFilterYear = p_web.GSV('locFilterYear') ! STRING(30)
SetFilterDate       PROCEDURE()
    CODE
        p_web.SSV('locFilterStartDate',DATE(SUB(p_web.GSV('locFilterMonth'),1,2),1,p_web.GSV('locFilterYear')))
        p_web.SSV('locFilterEndDate',DATE(SUB(p_web.GSV('locFilterMonth') + 1,1,2),1,p_web.GSV('locFilterYear')) - 1)
TagColour           PROCEDURE(LONG pType)
    CODE
        ClearTagFile(p_web)
        
        Access:JOBSWARR.ClearKey(jow:RepairedRRCAccManKey)
        jow:BranchID = p_web.GSV('locBranchID')
        jow:RepairedAt = p_web.GSV('BookingSite')
        jow:RRCStatus = 'APP'
        jow:Manufacturer = p_web.GSV('locWarrantyManufacturer')
        jow:DateAccepted = p_web.GSV('locPaidStartDate')
        SET(jow:RepairedRRCAccManKey,jow:RepairedRRCAccManKey)
        LOOP UNTIL Access:JOBSWARR.Next() <> Level:Benign
            IF (jow:BranchID <> p_web.GSV('locBranchID') OR |
                jow:RepairedAt <> p_web.GSV('BookingSite') OR |
                jow:RRCStatus <> 'APP' OR |
                jow:Manufacturer <> p_web.GSV('locWarrantyManufacturer') OR |
                jow:DateAccepted > p_web.GSV('locPaidEndDate'))
                BREAK
            END ! IF
            Access:WEBJOB.ClearKey(wob:RefNumberKey)
            wob:RefNumber = jow:RefNumber
            IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                IF (pType = 0 OR wob:ReconciledMarker = pType)
                    Access:TagFile.ClearKey(tag:keyTagged)
                    tag:sessionID = p_web.SessionID
                    tag:taggedValue = jow:RecordNumber
                    IF (Access:TagFile.TryFetch(tag:keyTagged) = Level:Benign)
                        IF (tag:tagged = 0)
                            tag:tagged = 1
                            Access:TagFile.TryUpdate()
                        END ! IF
                    ELSE
                        IF (Access:TagFile.PrimeRecord() = Level:Benign)
                            tag:sessionID = p_web.SessionID
                            tag:tagged = 1
                            tag:taggedValue = jow:RecordNumber
                            IF (Access:TagFile.TryInsert())
                                Access:TagFile.CancelAutoInc()
                            END !I F
                        
                        END ! IF
                    END ! IF
                END ! IF
               
            END ! IF
            
        END ! LOOP
