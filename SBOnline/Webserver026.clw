

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER026.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER017.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER019.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER025.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER027.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER028.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER029.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER030.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER031.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER032.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER033.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER037.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER045.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER047.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER260.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER494.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER495.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER496.INC'),ONCE        !Req'd for module callout resolution
                     END


NewJobBooking        PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locPreviousOutFault  STRING(255)                           !
locPreviousJobNumber LONG                                  !
tmp:TransitType      STRING(30)                            !tmp:TransitType
tmp:AuditNotes       STRING(255)                           !
tmp:PreviousAddress  STRING(255)                           !Previous Address
tmp:UsePreviousAddress BYTE(0)                             !Use Previous Address
filter:Manufacturer  STRING(1000)                          !filter:Manufacturer
filter:ModelNumber   STRING(1000)                          !filter:ModelNumber
tmp:POPType          STRING(30)                            !POP Type
tmp:DOP              DATE                                  !
FranchiseAccount     BYTE(0)                               !FranchiseAccount
tmp:FranchiseAccountNumber STRING(30)                      !Franchise Account Number
tmp:GenericAccountNumber STRING(30)                        !Generic Account Number
tmp:ExternalDamageChecklist BYTE(0)                        !External DamageChecklist
tmp:AmendAddress     BYTE(0)                               !tmp:AmendAddress
tmp:AmendFaultCodes  BYTE(0)                               !Amend Fault Codes
tmp:FaultCode1       STRING(255)                           !tmp:FaultCode1
tmp:FaultCode2       STRING(255)                           !Fault Code 2
tmp:FaultCode3       STRING(255)                           !tmp:FaultCode3
tmp:FaultCode4       STRING(255)                           !tmp:FaultCode4
tmp:FaultCode5       STRING(255)                           !tmp:FaultCode5
tmp:FaultCode6       STRING(255)                           !tmp:FaultCode6
tmp:ShowAccessory    STRING(1000)                          !tmp:ShowAccessory
tmp:TheAccessory     STRING(1000)                          !
tmp:TheJobAccessory  STRING(1000)                          !
tmp:FoundAccessory   STRING(30)                            !tmp:FoundAccessory
tmp:AmendAccessories BYTE(0)                               !tmp:AmendAccessories
tmp:AmendAddresses   BYTE(0)                               !Amend Addresses
locMobileLifetimeValue STRING(30)                          !
locMobileAverageSpend STRING(20)                           !
locMobileLoyaltyStatus STRING(30)                          !
locMobileUpgradeDate STRING(30)                            !
locMobileIDNumber    STRING(30)                            !
locPOPTypePassword   STRING(30)                            !
qContactHistory      QUEUE,PRE(cq)                         !
chtRecord            LIKE(cht:Record)                      !
                     END                                   !
local       Class
CheckLength         Procedure(String f:Type),Byte
ValidateMSN         Procedure(),Byte
FaultCode           Procedure(String f:FieldName,Byte f:FieldNumber)
FaultCodeDescription           Procedure(Byte f:FieldNumber)
SetStatus           Procedure(String f:Type,Long f:Number)
            End ! local       Class

FilesOpened     Long
PREJOB::State  USHORT
TRADEACC_ALIAS::State  USHORT
JOBS::State  USHORT
TRANTYPE::State  USHORT
MANUFACT::State  USHORT
MODELNUM::State  USHORT
TRAHUBAC::State  USHORT
DEFAULTS::State  USHORT
ESNMODEL::State  USHORT
MODPROD::State  USHORT
UNITTYPE::State  USHORT
MODELCOL::State  USHORT
NETWORKS::State  USHORT
ACCESSOR::State  USHORT
JOBS_ALIAS::State  USHORT
JOBSE::State  USHORT
JOBSE2::State  USHORT
CHARTYPE::State  USHORT
WEBJOB::State  USHORT
WEBJOBNO::State  USHORT
SUBTRACC::State  USHORT
CONTHIST::State  USHORT
COURIER::State  USHORT
TRADEACC::State  USHORT
JOBNOTES::State  USHORT
USERS::State  USHORT
SUBURB::State  USHORT
MANFAULO::State  USHORT
MANFAULT::State  USHORT
MANFAUEX::State  USHORT
LOCATLOG::State  USHORT
JOBACC::State  USHORT
JOBSOBF::State  USHORT
JOBSE_ALIAS::State  USHORT
AUDIT::State  USHORT
AUDSTATS::State  USHORT
STATUS::State  USHORT
STAHEAD::State  USHORT
JOBSTAGE::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('NewJobBooking')
  loc:formname = 'NewJobBooking_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('NewJobBooking','')
    p_web._DivHeader('NewJobBooking',clip('fdiv') & ' ' & clip('FormContent'))
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferNewJobBooking',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of InsertRecord + NET:WEB:StagePost
    do RestoreMem
    do PostInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferNewJobBooking',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:StagePost
    do RestoreMem
    do PostCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferNewJobBooking',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_NewJobBooking',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    If loc:act = InsertRecord
      do PostInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do PostUpdate
    End

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferNewJobBooking',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of DeleteRecord + NET:WEB:StagePost
    do RestoreMem
    do PostDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_NewJobBooking',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
DeleteVariables     Routine
    
    p_web.DeleteSessionValue('locMobileActivateDate')
    p_web.DeleteSessionValue('locMobilePhoneName')
    p_web.DeleteSessionValue('locMobileLoyaltyStatus')
    p_web.DeleteSessionValue('locMobileValidated')
    p_web.DeleteSessionValue('locMobileUpgradeDate')
    p_web.DeleteSessionValue('locMobileAverageSpend')
    p_web.DeleteSessionValue('locMobileLifetimeValue')
    p_web.DeleteSessionValue('locMobileIDNumber')
    p_web.DeleteSessionValue('MSISDNCheckRun')
    p_web.DeleteSessionValue('NewAccountAuthorised')
CalculateFaultCodes  Routine

    FaultNumber# = 1
    p_web.SSV('Hide:AmendFaultCodes',1)
    p_web.SSV('Comment:AmendFaultCodes','')
    p_web.SSV('tmp:AmendFaultCodes','')
    
    Loop x# = 1 To 20
        Access:MANFAULT.Clearkey(maf:Field_Number_Key)
        maf:Manufacturer = p_web.GSV('job:Manufacturer')
        maf:Field_Number = x#
        If Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign
            ! OK
            If maf:MainFault
                Cycle
            End ! If maf:MainFault
            If maf:NotAvailable
                Cycle
            End ! If maf:NotAvailable
            Comp# = 0
            If p_web.GSV('job:Warranty_Job') = 'YES'
                If maf:Compulsory = 'YES'
                    If maf:Compulsory_At_Booking = 'YES'
                        Comp# = 1
                    End ! If maf:Compulsory_At_Booking = 'YES'
                End ! If maf:Compulsory = 'YES'
            End ! If p_web.GSV('job:Warranty_Job') = 'YES'

            If p_web.GSV('job:Chargeable_Job') = 'YES'
                If maf:CharCompulsory
                    If maf:CharCompulsoryBooking
                        Comp# = 1
                    End ! If maf:CharCompulsoryBooking
                End ! If maf:CharCompulsory
            End ! If p_web.GSV('job:Chargeabl_Job') = 'YES'

            If Comp# = 0
                Cycle
            End ! If Comp# = 0

            p_web.SSV('Hide:FaultCode' & FaultNumber#,'')
            p_web.SSV('Filter:FaultCode' & FaultNumber#,'mfo:NotAvailable = 0 AND Upper(mfo:Manufacturer) = Upper(''' & p_web.GSV('job:Manufacturer') & ''') AND mfo:Field_Number = ' & x#)
            p_web.SSV('Comment:FaultCode' & FaultNumber#,'Required')
            p_web.SSV('Prompt:FaultCode' & FaultNumber#,Clip(maf:Field_Name))
            p_web.SSV('Hide:AmendFaultCodes','')
            p_web.SSV('Comment:AmendFaultCodes','Required')
            p_web.SSV('tmp:AmendFaultCodes',1)
            FaultNumber# += 1
            If FaultNumber# > 6
                Break
            End ! If FaultNumber# > 6
        Else ! If Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign
            ! Error
        End ! If Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign
    End ! Loop x# = 1 To 20

    Loop x# = FaultNumber# To 6
        p_web.SSV('Filter:FaultCode' & x#,0)
        p_web.SSV('Hide:FaultCode' & x#,1)
        p_web.SSV('Comment:FaultCode' & x#,'')
        p_web.SSV('Prompt:FaultCode' & x#,'Fault Code ' & x#)
    End ! Loop x# = 1 To 6
MSISDNRoutine       Routine
Data
local:Folder        String(255)
local:ININame       String(255)
local:INIFileName   String(255)
locTimeOut          Long()
Code
    DO MSISDNClearVariables

!!    Test Code
!    p_web.SSV('locMobileActivateDate',Deformat('01/01/2010',@d06))
!    p_web.SSV('locMobilePhoneName','Phone Name')
!    p_web.SSV('locMobileLoyaltyStatus','Super Duper Customer')
!    p_web.SSV('locMobileValidated',1)
!    p_web.SSV('locMobileUpgradeDate',Deformat('02/02/2010',@d6))
!    p_web.SSV('locMobileAverageSpend',12345.90)
!    p_web.SSV('locMobileLifetimeValue','Best Customer In The World')
!    p_web.SSV('locMobileIDNumber','12345678901234567890') 
!    exit
     
    If p_web.GSV('job:Mobile_Number') = ''
        Exit
    End ! If job:Mobile_Number = ''
    
    local:Folder =  GETINI('MQ','MQRequestFolder',,Clip(Path()) & '\SB2KDEF.INI')

    If local:Folder = ''
        Exit
    End ! If GETINI('MQ','MQRequestFolder',,Clip(Path()) & '\SB2KDEF.INI') = ''
    
    p_web.SSV('locMobileLifetimeValue','Checking.....')
    p_web.SSV('locMobileLoyaltyStatus','')
    p_web.SSV('locMobileAverageSpend','')
    p_web.SSV('locMobileUpgradeDate','')
    p_web.SSV('locMobileIDNumber','')   
    
    If Sub(Clip(local:Folder),-1,1) <> '\'
        local:Folder = Clip(local:Folder) & '\'
    End ! If Sub(Clip(local:Folder),-1,1) <> '\'

    local:ININame = Clip(p_web.GSV('job:Mobile_Number')) & '_' & Format(Clock(),@t05) & '.ini'

    local:INIFIleName = Clip(local:Folder) & Clip(local:ININame)

    Remove(local:INIFileName)
    Remove(Clip(local:INIFileName) & '.old')

    PUTINI('REQUEST','TYPE','QRY_SUB',local:INIFileName)
    PUTINI('REQUEST','MSISDN',Clip(p_web.GSV('job:Mobile_Number')),local:INIFileName)

    locTimeOut = GETINI('MQ','TimeOut',,CLIP(PATH()) & '\SB2DEF.INI')
    IF (locTimeOut = 0)
        locTimeOut = 20
    END
    
    !TB13095 - implies that MQRemote is to be found on the Local:Folder path
    Return# = RunDOS(Clip(local:Folder) & 'MQREMOTE.EXE ' & Clip(local:ININame),1)
    !Return# = RunDOS(Clip(Path()) & '\MQREMOTE.EXE ' & Clip(local:ININame),1)
   ! RETURN# = BHRunDOS(Clip(Path()) & '\MQREMOTE.EXE ' & Clip(local:ININame),locTimeOut)

    Error# = 0
    If Return# = False
        Error# = 1
    Else ! If Return# = False
        If GETINI('RESPONSE','REQUEST_STATUS',,local:INIFileName) <> 'SUCCESS'
            Error# = 1
        End ! If GETINI('RESPONSE','REQUEST_STATUS',,local:INIFileName) <> 'SUCCESS'
    End ! If Return# = False

    If Error# = 1
        If GETINI('MQ','MSISDNKeepFiles',,Clip(Path()) & '\SB2KDEF.INI') = 1
            ! Keep a history of processed files (DBH: 07/11/2007)
            Rename(local:INIFilename,Clip(local:INIFileName) & '.old')
        Else ! If GETINI('MQ','MSISDNKeepFiles',,Clip(Path()) & '\SB2KDEF.INI') = 1
            Remove(local:INIFilename)
        End ! If GETINI('MQ','MSISDNKeepFiles',,Clip(Path()) & '\SB2KDEF.INI') = 1
        
        p_web.SSV('locMobileLifetimeValue','Information Unavailable')
        p_web.SSV('locMobileLoyaltyStatus','Information Unavailable')
        p_web.SSV('locMobileAverageSpend','Information Unavailable')
        p_web.SSV('locMobileUpgradeDate','Information Unavailable')
        p_web.SSV('locMobileIDNumber','Information Unavailable')  
        Exit
    End ! If Error# = 1
    
    p_web.SSV('locMobileActivateDate',GETINI('RESPONSE','ACTIVATION_DATE',,local:INIFileName))
    p_web.SSV('locMobilePhoneName',GETINI('RESPONSE','PHONE_NAME',,local:INIFileName))
    p_web.SSV('locMobileLoyaltyStatus',GETINI('RESPONSE','LOYALTY_STATUS',,local:INIFileName))
    p_web.SSV('locMobileValidated',1)
    p_web.SSV('locMobileUpgradeDate',GETINI('RESPONSE','NEXT_UPGRADE_DATE',,local:INIFileName))
    p_web.SSV('locMobileAverageSpend',GETINI('RESPONSE','AVERAGE_SPEND',,local:INIFilename))
    p_web.SSV('locMobileIDNumber',GETINI('RESPONSE','ID_NUMBER',,local:INIFilename))

    If GETINI('MQ','MSISDNKeepFiles',,Clip(Path()) & '\SB2KDEF.INI') = 1
        ! Keep a history of processed files (DBH: 07/11/2007)
        Rename(local:INIFilename,Clip(local:INIFileName) & '.old')
    Else ! If GETINI('MQ','MSISDNKeepFiles',,Clip(Path()) & '\SB2KDEF.INI') = 1
        Remove(local:INIFilename)
    End ! If GETINI('MQ','MSISDNKeepFiles',,Clip(Path()) & '\SB2KDEF.INI') = 1
    
    ! DBH #11388 - Call Second INI

    local:ININame = Clip(p_web.GSV('job:Mobile_Number')) & '_2_' & Format(Clock(),@t05) & '.ini'

    local:INIFIleName = Clip(local:Folder) & Clip(local:ININame)

    Remove(local:INIFileName)
    Remove(Clip(local:INIFileName) & '.old')

    PUTINI('REQUEST','TYPE','QRY_VDTATT',local:INIFileName)
    PUTINI('REQUEST','MSISDN',Clip(p_web.GSV('job:Mobile_Number')),local:INIFileName)

    locTimeOut = GETINI('MQ','TimeOut',,CLIP(PATH()) & '\SB2DEF.INI')
    IF (locTimeOut = 0)
        locTimeOut = 20
    END
    
    Return# = RunDOS(Clip(Path()) & '\MQREMOTE.EXE ' & Clip(local:ININame),1)

    p_web.SSV('locMobileLifetimeValue',GETINI('RESPONSE','SUBSCRIBER_ATTRIBUTE_1_SAC_DESCRIPTION',,local:INIFilename))

    If GETINI('MQ','MSISDNKeepFiles',,Clip(Path()) & '\SB2KDEF.INI') = 1
        ! Keep a history of processed files (DBH: 07/11/2007)
        Rename(local:INIFilename,Clip(local:INIFileName) & '.old')
    Else ! If GETINI('MQ','MSISDNKeepFiles',,Clip(Path()) & '\SB2KDEF.INI') = 1
        Remove(local:INIFilename)
    End ! If GETINI('MQ','MSISDNKeepFiles',,Clip(Path()) & '\SB2KDEF.INI') = 1

    IF (p_web.GSV('locMobileLifetimeValue') = '')
        p_web.SSV('locMobileLifetimeValue','Information Unavailable')
    END
    IF (p_web.GSV('locMobileLoyaltyStatus') ='')
        p_web.SSV('locMobileLoyaltyStatus','Information Unavailable')
    END
    IF (p_web.GSV('locMobileAverageSpend') = '')
        p_web.SSV('locMobileAverageSpend','Information Unavailable')
    END
    IF (p_web.GSV('locMobileUpgradeDate') = '')
        p_web.SSV('locMobileUpgradeDate','Information Unavailable')
    END
    IF (p_web.GSV('locMobileIDNumber') = '')
        p_web.SSV('locMobileIDNumber','Information Unavailable')  
    END
    
    
    Display()
MSISDNClearVariables        routine
    p_web.SSV('locMobileActivateDate','')
    p_web.SSV('locMobilePhoneName','')
    p_web.SSV('locMobileLoyaltyStatus','')
    p_web.SSV('locMobileValidated','')
    p_web.SSV('locMobileUpgradeDate','')
    p_web.SSV('locMobileAverageSpend','')
    p_web.SSV('locMobileLifetimeValue','')
    p_web.SSV('locMobileIDNumber','')
    p_web.SSV('MSISDNCheckRun',1)
MultipleJobBookingSetup     ROUTINE
DATA

CODE
    if (p_web.GSV('MultipleJobBooking') = 1)
        p_web.SSV('mj:InProgress',1)
        Access:JOBS.Clearkey(job:Ref_Number_Key)
        job:Ref_Number = p_web.GSV('mj:PreviousJobNumber')
        if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
            p_web.FileToSessionQueue(JOBS)
        END
        Access:JOBSE.Clearkey(jobe:RefNumberKey)
        jobe:RefNumber = job:Ref_Number
        if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
            p_web.FileToSessionQueue(JOBSE)
        END
        Access:WEBJOB.Clearkey(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
            p_web.FileToSessionQueue(WEBJOB)
        END
        Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
        jbn:RefNumber = job:Ref_Number
        if (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)
            p_web.FileToSessionQueue(JOBNOTES)
        END

!        #13392 Don't replicate anything from jobse2 (same as webmaster) (DBH: 06/11/2014)        
!        Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
!        jobe2:RefNumber = job:Ref_Number
!        if (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
!            p_web.FileToSessionQueue(JOBSE2)
!        END
    
        job:Mobile_Number = ''
        job:Warranty_Job = 'NO'
        job:Chargeable_Job = 'NO'
        job:Charge_Type = ''
        job:Warranty_Charge_Type = ''
        job:DOP = ''
!        jobe2:WarrantyRefNo = ''
!        p_web.SSV('jobe2:WarrantyRefNo',jobe2:WarrantyRefNo)
!        jobe2:CourierWaybillNumber = ''
!        p_web.SSV('jobe2:CourierWaybillNumber',jobe2:CourierWaybillNumber)
!        jobe2:SMSAlertNumber = ''
!        p_web.SSV('jobe2:SMSAlertNumber',jobe2:SMSAlertNumber)
!        jobe2:EmailAlertAddress = ''
!        p_web.SSV('jobe2:EmailAlertAddress',jobe2:EmailAlertAddress)
!        jobe2:EmailNotification = 0
!        p_web.SSV('jobe2:EmailNotification',jobe2:EmailNotification)
        if (p_web.GSV('mj:FaultCode') = 1)
            ! If fault codes aren't being retained, then blank fault description
            jbn:Fault_Description = ''
            p_web.SSV('jbn:Fault_Description',jbn:Fault_Description)
        END
        jobe:Booking48HourOption = ''
        p_web.SSV('jobe:Booking48HourOption',jobe:Booking48HourOption)
        jobe:VSACustomer = ''
        p_web.SSV('jobe:VSACustomer',jobe:VSACustomer)
!        jobe2:Contract = ''
!        p_web.SSV('jobe2:Contract',jobe2:Contract)
!        jobe2:Prepaid = ''
!        p_web.SSV('jobe2:Prepaid',jobe2:Prepaid)
    
        if (p_web.GSV('mj:Colour') = 1)
            job:Colour = ''
        END
        IF (p_web.GSV('mj:OrderNumber') = 1)
            job:Order_Number = ''
        END
        if (p_web.GSV('mj:FaultCode') <> 1)
        
            Do CalculateFaultCodes
        
            loop xx# = 1 to 6
                Access:MANFAULT.ClearKey(maf:Field_Number_Key)
                maf:Manufacturer = job:Manufacturer
                set(maf:Field_Number_Key,maf:Field_Number_Key)
                LOOP UNTIL Access:MANFAULT.NEXT()
                    if (maf:Manufacturer <> job:Manufacturer)
                        BREAK
                    END
                    IF (maf:Field_Name = p_web.GSV('Prompt:FaultCode' & xx#))
                        EXECUTE maf:Field_Number
                            p_web.SSV('tmp:FaultCode' & xx#,job:Fault_Code1)
                            p_web.SSV('tmp:FaultCode' & xx#,job:Fault_Code2)
                            p_web.SSV('tmp:FaultCode' & xx#,job:Fault_Code3)
                            p_web.SSV('tmp:FaultCode' & xx#,job:Fault_Code4)
                            p_web.SSV('tmp:FaultCode' & xx#,job:Fault_Code5)
                            p_web.SSV('tmp:FaultCode' & xx#,job:Fault_Code6)
                            p_web.SSV('tmp:FaultCode' & xx#,job:Fault_Code7)
                            p_web.SSV('tmp:FaultCode' & xx#,job:Fault_Code8)
                            p_web.SSV('tmp:FaultCode' & xx#,job:Fault_Code9)
                            p_web.SSV('tmp:FaultCode' & xx#,job:Fault_Code10)
                            p_web.SSV('tmp:FaultCode' & xx#,job:Fault_Code11)
                            p_web.SSV('tmp:FaultCode' & xx#,job:Fault_Code12)
                            p_web.SSV('tmp:FaultCode' & xx#,wob:FaultCode13)
                            p_web.SSV('tmp:FaultCode' & xx#,wob:FaultCode14)
                            p_web.SSV('tmp:FaultCode' & xx#,wob:FaultCode15)
                            p_web.SSV('tmp:FaultCode' & xx#,wob:FaultCode16)
                            p_web.SSV('tmp:FaultCode' & xx#,wob:FaultCode17)
                            p_web.SSV('tmp:FaultCode' & xx#,wob:FaultCode18)
                            p_web.SSV('tmp:FaultCode' & xx#,wob:FaultCode19)
                            p_web.SSV('tmp:FaultCode' & xx#,wob:FaultCode20)
                        END
                    END
                
                END
            
            END
        
                
        END
        if (p_web.GSV('mj:EngineersNotes') = 1)
            jbn:Engineers_Notes = ''
            p_web.SSV('jbn:Engineers_Notes',jbn:Engineers_Notes)
        END

        ! Contact History will be added when the job is inserted
    
    ELSE
        p_web.DeleteSessionValue('mj:InProgress')
    END

PreBooking  ROUTINE
    IF (p_web.GSV('PreBookingRefNo') > 0)
        Access:PREJOB.ClearKey(PRE:keyRefNumber)
        PRE:RefNumber = p_web.GSV('PreBookingRefNo')
        IF (Access:PREJOB.TryFetch(PRE:keyRefNumber) = Level:Benign)
            
!            p_web.SSV('job:Title',PRE:Title)
!            p_web.SSV('job:Initial',PRE:Initial)
!            p_web.SSV('job:Surname',PRE:Surname)
!            p_web.SSV('job:Company_Name',clip(PRE:Title)&' '&PRE:Initial&' '&clip(PRE:Surname))
!            p_web.SSV('job:Address_Line1',PRE:Address_Line1)
!            p_web.SSV('job:Address_Line2',PRE:Address_Line2)
!            p_web.SSV('job:Address_Line3',PRE:Suburb)
!            p_web.SSV('job:Telephone_Number',PRE:Telephone_number)
!            p_web.SSV('job:Mobile_Number',PRE:Mobile_number)
!            ! = PRE:ID_number
!!            job:ESN = PRE:ESN
!!            job:Manufacturer = PRE:Manufacturer
!!            job:Model_Number = PRE:Model_number
!            p_web.SSV('job:DOP',PRE:DOP)
!            Access:modelnum.clearkey(mod:Model_Number_Key)
!            mod:Model_Number = PRE:Model_number
!            if access:Modelnum.fetch(mod:Model_Number_Key) = level:benign
!                p_web.SSV('job:Unit_Type',mod:Unit_Type)
!            END
!            !What sort of charge type is called for?
!            Access:Chartype.clearkey(cha:Charge_Type_Key)
!            cha:Charge_Type = PRE:ChargeType
!            if access:Chartype.fetch(cha:Charge_Type_Key) = level:benign
!                if cha:Warranty = 'YES' then
!                    p_web.SSV('job:Chargeable_Job','NO')
!                    p_web.SSV('Job:Warranty_job','YES')
!                    p_web.SSV('job:Warranty_Charge_Type',PRE:ChargeType)
!                ELSE
!                    p_web.SSV('job:Chargeable_Job','YES')
!                    p_web.SSV('Job:Warranty_job','NO')
!                    p_web.SSV('job:Charge_Type',Pre:ChargeType)
!                END
!  
!            END !if charge type found
            
            
            job:Title = PRE:Title
            job:Initial = PRE:Initial
            job:Surname = PRE:Surname
            job:Company_Name = clip(PRE:Title)&' '&PRE:Initial&' '&clip(PRE:Surname)
            job:Address_Line1 = PRE:Address_Line1
            job:Address_Line2 = PRE:Address_Line2
            job:Address_Line3 = PRE:Suburb
            job:Telephone_Number = PRE:Telephone_number
            job:Mobile_Number = PRE:Mobile_number
            ! = PRE:ID_number
            job:ESN = PRE:ESN
            job:Manufacturer = PRE:Manufacturer
            job:Model_Number = PRE:Model_number
            job:DOP = PRE:DOP
            Access:modelnum.clearkey(mod:Model_Number_Key)
            mod:Model_Number = PRE:Model_number
            if access:Modelnum.fetch(mod:Model_Number_Key) = level:benign
                job:Unit_Type = mod:Unit_Type
            END
            !What sort of charge type is called for?
            Access:Chartype.clearkey(cha:Charge_Type_Key)
            cha:Charge_Type = PRE:ChargeType
            if access:Chartype.fetch(cha:Charge_Type_Key) = level:benign
                if cha:Warranty = 'YES' then
                    job:Chargeable_Job = 'NO'
                    Job:Warranty_job = 'YES'
                    job:Warranty_Charge_Type  = PRE:ChargeType
                    IF (cha:SecondYearWarranty)
                        job:POP = 'S'
                    ELSE
                        job:POP = 'F'
                    END
                    
                ELSE
                    job:Chargeable_Job = 'YES'
                    Job:Warranty_job = 'NO'
                    job:Charge_Type  = Pre:ChargeType
                    job:POP = 'C'
                END
  
            END !if charge type found
            
            
            p_web.SSV('jobe2:IDNumber',PRE:ID_number)
            p_web.SSV('jobe2:SMSAlertNumber',PRE:Mobile_number)
            p_web.SSV('jbn:Fault_Description',PRE:FaultText)
            
            p_web.SSV('job:Title',job:Title)
            p_web.SSV('job:Initial',job:Initial)
            p_web.SSV('job:Surname',job:Surname)
            p_web.SSV('job:Company_Name',job:Company_Name)
            p_web.SSV('job:Address_Line1',job:Address_Line1)
            p_web.SSV('job:Address_Line2',job:Address_Line2)
            p_web.SSV('job:Address_Line3',job:Address_Line3)
            p_web.SSV('job:Telephone_Number',job:Telephone_Number)
            p_web.SSV('job:Mobile_Number',job:Mobile_Number)
            p_web.SSV('job:ESN',job:ESN)
            p_web.SSV('job:Manufacturer',job:Manufacturer)
            p_web.SSV('job:Model_Number',job:Model_Number)
            p_web.SSV('job:DOP',job:DOP)
            p_web.SSV('job:Unit_Type',job:Unit_Type)
            p_web.SSV('job:Chargeable_Job',job:Chargeable_Job)
            p_web.SSV('job:Warranty_Job',job:Warranty_Job)
            p_web.SSV('job:Charge_type',job:Charge_Type)
            p_web.SSV('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
            p_web.SSV('job:POP',job:POP)
            
            Do CalculateFaultCodes ! Call this again as more info has been filled in relevant to fault codes
        END ! IF
    END ! IF
    
    
Reset:AccountNumber     Routine
    p_web.SetSessionValue('job:Account_Number','')
    p_web.SetSessionValue('Hide:EndUserName',1)
Value:AccountNumber        Routine
Data
local:OnStop                Byte(0)
local:ForceCustomerName     Byte(0)
Code

    p_web.SSV('ShowHubWarning','')

    ForceCustomerName# = 0
    
    Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
    sub:Account_Number = p_web.GSV('job:Account_Number')
    If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Found

        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = sub:Main_Account_Number
        If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            !Found

            p_web.SSV('Hide:48Hour',1)
            If tra:Activate48Hour
                Access:ESNMODEL.Clearkey(esn:ESN_Key)
                esn:ESN = Sub(p_web.GSV('job:ESN'),1,6)
                esn:Model_Number = p_web.GSV('job:Model_Number')
                If Access:ESNMODEL.TryFetch(esn:ESN_Key) = Level:Benign
                    If esn:Include48Hour
                        p_web.SSV('Hide:48Hour','')
                    End ! If esn:Include48Hour
                End ! If Access:ESNMODEL.TryFetch(esn:ESN_Key) = Level:Benign

                Access:ESNMODEL.Clearkey(esn:ESN_Key)
                esn:ESN = Sub(p_web.GSV('job:ESN'),1,8)
                esn:Model_Number = p_web.GSV('job:Model_Number')
                If Access:ESNMODEL.TryFetch(esn:ESN_Key) = Level:Benign
                    If esn:Include48Hour
                        p_web.SSV('Hide:48Hour','')
                    End ! If esn:Include48Hour
                End ! If Access:ESNMODEL.TryFetch(esn:ESN_Key) = Level:Benign
            End ! If tra:Activate48Hour


            If tra:Invoice_Sub_Accounts = 'YES' And tra:Use_Sub_Accounts = 'YES'
                If sub:Stop_Account = 'YES'
                    local:OnStop = 1
                End ! If sub:Stop_Account = 'YES'

                If sub:ForceOrderNumber
                    p_web.SSV('Required:OrderNumber',1)
                    p_web.SSV('Comment:OrderNumber','Required')
                Else ! If sub:ForceOrderNumber
                    p_web.SSV('Required:OrderNumber','')
                    p_web.SSV('Comment:OrderNumber','')
                End ! If sub:ForceOrderNumber

                if sub:Force_Customer_name      !J - 25/06/14 - TB13307 
                    ForceCustomerName# = 2      !new value - added to allow to show user name but not force it
                END
                
                If sub:ForceEndUserName 
                    ForceCustomerName# = 1      !J - 25/06/14 - this is old code to show and force it
                End ! If sub:ForceEndUserName
                
                IF (sub:ForceEstimate)
                    p_web.SSV('job:Estimate_If_Over',sub:EstimateIfOver)
                    p_web.SSV('job:Estimate','YES')
                END
                
            Else ! If tra:Invoice_Sub_Accounts = 'YES' And tra:Use_Sub_Accounts = 'YES'
                If tra:Stop_Account = 'YES'
                    local:OnStop = 1
                End ! If tra:Stop_Accont = 'YES'
                
                If tra:ForceOrderNumber
                    p_web.SSV('Required:OrderNumber',1)
                    p_web.SSV('Comment:OrderNumber','Required')
                Else ! If tra:ForceOrderNumber
                    p_web.SSV('Required:OrderNumber','')
                    p_web.SSV('Comment:OrderNumber','')
                End ! If tra:ForceOrderNumber

                If tra:ForceEndUserName   
                    ForceCustomerName# = 1
                End ! If sub:ForceEndUserName
                
                IF (tra:Force_Estimate = 'YES')
                    p_web.SSV('job:Estimate_If_Over',tra:Estimate_If_Over)
                    p_web.SSV('job:Estimate','YES')
                END                
            End ! If tra:Invoice_Sub_Accounts = 'YES' And tra:Use_Sub_Accounts = 'YES'

            If tra:Invoice_Sub_Accounts <> 'YES'
                If tra:Use_Customer_Address <> 'YES'
                    p_web.SSV('job:Postcode',tra:Postcode)
                    p_web.SSV('job:Company_Name',tra:Company_Name)
                    p_web.SSV('job:Address_Line1',tra:Address_Line1)
                    p_web.SSV('job:Address_Line2',tra:Address_Line2)
                    p_web.SSV('job:Address_Line3',tra:Address_Line3)
                    p_web.SSV('job:Telephone_Number',tra:Telephone_Number)
                    p_web.SSV('job:Fax_Number',tra:Fax_Number)
                    p_web.SSV('jobe:EndUserEmailAddress',tra:EmailAddress)
                    p_web.SSV('jobe2:HubCustomer',tra:Hub)
                End ! If tra:Use_Customer_Address <> 'YES'
            Else ! If tra:Invoice_Sub_Accounts <> 'YES'
                If sub:Use_Customer_Address <> 'YES'
                    p_web.SSV('job:Postcode',sub:Postcode)
                    p_web.SSV('job:Company_Name',sub:Company_Name)
                    p_web.SSV('job:Address_Line1',sub:Address_Line1)
                    p_web.SSV('job:Address_Line2',sub:Address_Line2)
                    p_web.SSV('job:Address_Line3',sub:Address_Line3)
                    p_web.SSV('job:Telephone_Number',sub:Telephone_Number)
                    p_web.SSV('job:Fax_Number',sub:Fax_Number)
                    p_web.SSV('jobe:EndUserEmailAddress',sub:EmailAddress)
                    p_web.SSV('jobe2:HubCustomer',sub:Hub)
                End ! If sub:Use_Customer_Address <> 'YES'
            End ! If tra:Invoice_Sub_Accounts <> 'YES'

            If tra:Use_Sub_Accounts <> 'YES'
                If tra:Use_Delivery_ADdress = 'YES'
                    p_web.SSV('job:Postcode_Delivery',tra:Postcode)
                    p_web.SSV('job:Company_Name_Delivery',tra:Company_Name)
                    p_web.SSV('job:Address_Line1_Delivery',tra:Address_Line1)
                    p_web.SSV('job:Address_Line2_Delivery',tra:Address_Line2)
                    p_web.SSV('job:Address_Line3_Delivery',tra:Address_Line3)
                    p_web.SSV('job:Telephone_Delivery',tra:Telephone_Number)
                    p_web.SSV('jobe2:HubDelivery',tra:Hub)
                End ! If tra:Use_Delivery_ADdress = 'YES'
                If tra:Use_Collection_Address = 'YES'
                    p_web.SSV('job:Postcode_Collection',tra:Postcode)
                    p_web.SSV('job:Company_Name_Collection',tra:Company_Name)
                    p_web.SSV('job:Address_Line1_Collection',tra:Address_Line1)
                    p_web.SSV('job:Address_Line2_Collection',tra:Address_Line2)
                    p_web.SSV('job:Address_Line3_Collection',tra:Address_Line3)
                    p_web.SSV('job:Telephone_Collection',tra:Telephone_Number)
                    p_web.SSV('jobe2:HubCollection',tra:Hub)

                End ! If tra:Use_Collection_Address = 'YES'

                p_web.SSV('job:Courier',tra:Courier_Outgoing)
                p_web.SSV('job:Exchange_Courier',tra:Courier_Outgoing) ! #13389 Store courier (DBH: 03/10/2014)
                p_web.SSV('job:Loan_Courier',tra:Courier_Outgoing) ! #13389 Store courier (DBH: 03/10/2014)
            Else ! If tra:Use_Sub_Accounts <> 'YES'
                If sub:Use_Delivery_Address = 'YES'
                    p_web.SSV('job:Postcode_Delivery',sub:Postcode)
                    p_web.SSV('job:Company_Name_Delivery',sub:Company_Name)
                    p_web.SSV('job:Address_Line1_Delivery',sub:Address_Line1)
                    p_web.SSV('job:Address_Line2_Delivery',sub:Address_Line2)
                    p_web.SSV('job:Address_Line3_Delivery',sub:Address_Line3)
                    p_web.SSV('job:Telephone_Delivery',sub:Telephone_Number)
                    p_web.SSV('jobe2:HubDelivery',sub:Hub)
                End ! If sub:Use_Delivery_Address = 'YES'
                If sub:Use_Collection_Address = 'YES'
                    p_web.SSV('job:Postcode_Collection',sub:Postcode)
                    p_web.SSV('job:Company_Name_Collection',sub:Company_Name)
                    p_web.SSV('job:Address_Line1_Collection',sub:Address_Line1)
                    p_web.SSV('job:Address_Line2_Collection',sub:Address_Line2)
                    p_web.SSV('job:Address_Line3_Collection',sub:Address_Line3)
                    p_web.SSV('job:Telephone_Collection',sub:Telephone_Number)
                    p_web.SSV('jobe2:HubCollection',sub:Hub)
                End ! If sub:Use_Collection_Address = 'YES'
                p_web.SSV('job:Courier',sub:Courier_Outgoing)
                p_web.SSV('job:Incoming_Courier',sub:Courier_Incoming)
                p_web.SSV('job:Exchange_Courier',sub:Courier_Outgoing) ! #13389 Store courier (DBH: 03/10/2014)
                p_web.SSV('job:Loan_Courier',sub:Courier_Outgoing) ! #13389 Store courier (DBH: 03/10/2014)
                
            End ! If tra:Use_Sub_Accounts <> 'YES'

            If tra:Invoice_Sub_Accounts <> 'YES'
                If tra:Use_Customer_Address <> 'YES'
                    If HubOutOfRegion(p_web.GSV('BookingAccount'),tra:Hub) = 1
                        p_web.SSV('ShowHubWarning',1)
                    End ! If HubOutOfRegion(p_web.GSV('BookingAccount'),tra:Hub) = 1
                Else ! If tra:Use_Customer_Address <> 'YES'
                    If HubOutOfRegion(p_web.GSV('BookingAccount'),sub:Hub) = 1
                        p_web.SSV('ShowHubWarning',1)
                    End ! If HubOutOfRegion(p_web.GSV('BookingAccount'),sub:Hub) = 1
                End ! If tra:Use_Customer_Address <> 'YES'
            End ! If tra:Invoice_Sub_Accounts <> 'YES'

        Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            !Error
        End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign

    Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Error
    End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign

    If local:OnStop = 1
!|        p_web.SSV(p_web.GSV('sub:Account_Number'),'')
        p_web.SSV('Comment:AccountNumber','Account is on stop.')
        p_web.SSV('job:Account_Number','')
        Do Reset:AccountNumber
        Exit
    End ! If local:OnStop = 1

    p_web.SSV('Comment:AccountNumber',sub:Company_Name)

    !TB13307 - need to force Customer name 
    !trade account / Sub Account settings dealt with above
    !Defaults is opened on opening window so can use that here
    !Set(Defaults)
    !Access:Defaults.next()
    !hmm seems we have to use the following clumsy way of opening defaults (copied from elsewhere)    
    Access:DEFAULTS.Clearkey(def:RecordNumberKey)
    def:Record_Number = 1
    Set(def:RecordNumberKey,def:RecordNumberKey)
    Loop ! Begin Loop
        If Access:DEFAULTS.Next()
            Break
        End ! If Access:DEFAULTS.Next()
        Break
    End ! Loop    
    If def:Customer_Name = 'B' then
        ForceCustomerName# = 1
    END
    
!    If ForceCustomerName(p_web.GSV(p_web.GSV('sub:Account_Number')),'B') = Level:Benign
!        p_web.SSV('Required:EndUserName',1)
!        p_web.SSV('Hide:EndUserName','')
!    Else ! If CustomerNameRequired(p_web.GSV('p_web.GSV('sub:Account_Number')'))
!        p_web.SSV('Required:EndUserName','')
!    End ! If ForceCustomerName(p_web.GSV('p_web.GSV('sub:Account_Number')'))

    case ForceCustomerName#
    of 0    !do not show
        p_web.SSV('Hide:EndUserName',1)
        p_web.SSV('Comment:EndUserName','')
        p_web.SSV('Required:EndUserName','')
    of 1    !show and force
        p_web.SSV('Hide:EndUserName','')
        p_web.SSV('Comment:EndUserName','Required')
        p_web.SSV('Required:EndUserName',1)
    of 2    !show but do not force
        p_web.SSV('Hide:EndUserName','')
        p_web.SSV('Comment:EndUserName','')
        p_web.SSV('Required:EndUserName','')
    End ! case ForceCustomerName#
    !end TB13307
    
    If ForceOrderNumber(sub:Account_Number)
        p_web.SSV('Required:OrderNumber',1)
        p_web.SSV('Comment:OrderNumber','Required')
    Else ! If ForceOrderNumber(p_web.GSV('p_web.GSV('sub:Account_Number')'))
        p_web.SSV('Required:OrderNumber','')
        p_web.SSV('Comment:OrderNumber','')
    End ! If ForceOrderNumber(p_web.GSV('p_web.GSV('sub:Account_Number')'))

    If ForceCourier('B')
        p_web.SSV('Required:Courier',1)
        p_web.SSV('Comment:OutgoingCourier','Required')
    Else ! If ForceCourier('B')
        p_web.SSV('Required:Courier','')
        p_web.SSV('Comment:Courier','')
    End ! If ForceCourier('B')


    !p_web.SSV('Comment:AccountNumber','Required')


    If p_web.GSV('job:Transit_Type') <> 'ARRIVED FRANCHISE'
        p_web.SSV('Comment:CourierWaybillNumber','')
    End ! If p_web.GSV('tmp:TransitType') <> 'ARRIVED FRANCHISE'

    IF (INSTRING('CASH',p_web.GSV('job:Account_Number'),1,1))
        Access:TRADEACC_ALIAS.clearkey(tra_ali:Account_Number_Key)
        tra_ali:Account_Number = p_web.GSV('wob:HeadAccountNumber')
        IF (Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign)
            p_web.SSV('jobe2:HubCustomer',tra_ali:Hub)
            p_web.SSV('jobe2:HubCollection',tra_ali:Hub)
            p_web.SSV('jobe2:HubDelivery',tra_ali:Hub)
        END
    END
    
Value:AuthorityNumber       Routine
    Force# = 0
    If p_web.GSV('job:Chargeable_Job') = 'YES'
        Access:CHARTYPE.ClearKey(cha:Warranty_Key)
        cha:Warranty = 'NO'
        cha:Charge_Type = p_web.GSV('job:Charge_Type')
        If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
            !Found
            If cha:ForceAuthorisation
                Force# = 1
            End ! If cha:ForceAuthorisation
        Else ! If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
            !Error
        End ! If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
    End ! If p_web.GSV('tmp:ChargeableJob') = 1

    If p_web.GSV('job:Warranty_Job') = 'YES' And Force# = 0
        Access:CHARTYPE.ClearKey(cha:Warranty_Key)
        cha:Warranty = 'YES'
        cha:Charge_Type = p_web.SSV('job:Warranty_Charge_Type','')
        If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
            !Found
            If cha:ForceAuthorisation
                Force# = 1
            End ! If cha:ForceAuthorisation
        Else ! If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
            !Error
        End ! If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign

    End ! If p_web.GSV('tmp:WarrantyJob') = 1 And Force# = 0

    If Force# = 1
        p_web.SSV('Required:AuthorityNumber',1)
        p_web.SSV('Comment:AuthorityNumber','Required')
    Else ! If Force# = 1
        p_web.SSV('Required:AuthorityNumber',0)
        p_web.SSV('Comment:AuthorityNumber','')
    End ! If Force# = 1
Value:ChargeType      Routine
    If p_web.GSV('job:Chargeable_Job') = 'YES'
        Access:CHARTYPE.ClearKey(cha:Warranty_Key)
        cha:Warranty = 'NO'
        cha:Charge_Type = p_web.GSV('job:Charge_Type')
        If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
            !Found
            If cha:ForceAuthorisation
                p_web.SSV('Required:AuthorityNumber',1)
                p_web.SSV('Comment:AuthorityNumber','Required')
            End ! If cha:ForceAuthorisation
            
            CASE vod.EstimateRequired(p_web.GSV('job:Charge_Type'),p_web.GSV('job:Account_Number'))
            OF 1
                p_web.SSV('job:Estimate','YES')
            OF 2
                p_web.SSV('job:Estimate','NO')
            END
            
        Else ! If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
            !Error
            p_web.SSV('job:Charge_Type','')
        End ! If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
    End ! If p_web.GSV('tmp:ChargeableJob') = 1
Value:DOP       ROutine
    If p_web.GSV('job:DOP') = 0
        Exit
    End ! If p_web.GSV('job:DOP') = ''

    If p_web.GSV('job:DOP') = p_web.GSV('Save:DOP')
        Exit
    End ! If job:DOP = p_web.GSV('Save:DOP')


    If p_web.GSV('job:DOP') > Today()
        p_web.SSV('job:DOP','')
        p_web.SSV('Save:DOP','')
        p_web.SSV('Comment:DOP','Invalid Date (dd/mm/yyyy)')
        Exit
    End ! If job:DOP > Today()

    ! Try and account for spurious characters being placed in the date field (DBH: 15/10/2007)
    If p_web.GSV('job:DOP') < Deformat('01/01/1980','@d06')
        p_web.SSV('job:DOP','')
        p_web.SSV('Save:DOP','')
        p_web.SSV('Comment:DOP','Invalid Date (dd/mm/yyyy)')
        Exit
    End ! If job:DOP < Deformat('01/01/1980','@d06)

    p_web.SSV('Save:DOP',p_web.GSV('job:DOP'))

    If p_web.GSV('OBFValidation') = 'Passed'
        Exit
    End ! If p_web.GSV('OBFValidation') = 'Passed'
    
    IF (p_web.GSV('Hide:LiquidDamage') <> 1)
        ! DBH #10544 - Liquid Damage, job can't be anything but chargeable
        EXIT
    END
    

    Access:MANUFACT.ClearKey(man:Manufacturer_Key)
    man:Manufacturer = p_web.GSV('job:Manufacturer')
    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Found
        If p_web.GSV('job:DOP') < (Today() - man:Warranty_Period)
            If p_web.GSV('job:DOP') < (Today() - 730)
                job:POP = 'C'
            Else ! If job:DOP < (Today() - 730)
                IF (IsOneYearWarranty(p_web.GSV('job:Manufacturer'), |
                    p_web.GSV('job:Model_Number')))
                    job:POP = 'C'
                ELSE
                    job:POP = 'S'
                END
                
            End ! If job:DOP < (Today() - 730)
        Else ! If tmp:DO < (Today() - man:Warranty_Period)
            job:POP = 'F'
        End ! If tmp:DO < (Today() - man:Warranty_Period)
        p_web.SSV('job:POP',job:POP)
        Do Value:POP
    Else ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Error
    End ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign

Value:IMEINumber       Routine
Data
local:Bouncers        Long()
local:LiveBouncers  Long()
locOutFaultCode     String(255)
locOutFaultCodeDescription String(255)
Code

    p_web.SSV('IMEIHistory','')

    Access:DEFAULTS.Clearkey(def:RecordNumberKey)
    def:Record_Number = 1
    Set(def:RecordNumberKey,def:RecordNumberKey)
    Loop ! Begin Loop
        If Access:DEFAULTS.Next()
            Break
        End ! If Access:DEFAULTS.Next()
        Break
    End ! Loop

    local:Bouncers = 0
    local:LiveBouncers = 0
    Access:JOBS_ALIAS.Clearkey(job_ali:ESN_Key)
    job_ali:ESN = job:ESN
    Set(job_ali:ESN_Key,job_ali:ESN_Key)
    Loop
        If Access:JOBS_ALIAS.Next()
            Break
        End ! If Access:JOBS_ALIAS.Next()
        If job_ali:ESN <> job:ESN
            Break
        End ! If job_ali:ESN <> job:ESN
        If job_ali:Cancelled = 'YES'
            Cycle
        End ! If job_ali:Cancelled = 'YES'
        If job_ali:Date_Completed = ''
            local:LiveBouncers += 1
        End ! If job_ali:Date_Completed = ''
        local:Bouncers += 1
        p_web.SSV('tmp:PreviousAddress',Clip(job_ali:Company_Name) & '<13,10>' & Clip(job_ali:Address_Line1) & '<13,10>' & Clip(job_ali:Address_Line2) & '<13,10>' & Clip(job_ali:Address_Line3) & '<13,10>')
        p_web.SSV('locPreviousJobNumber',job_ali:Ref_Number)
        GetTheMainOutFault(job_ali:Ref_Number,locOutFaultCode,locOutFaultCodeDescription)
        IF (locOutFaultCode <> '')
            p_web.SSV('locPreviousOutFault',clip(locOutFaultCode) & ': ' & CLIP(locOutFaultCodeDescription))        
        ELSE
            p_web.SSV('locPreviousOutFault','')
        END            
    End ! Loop

    If local:Bouncers = 0
        If PreviousIMEI(job:ESN)
            p_web.SSV('Comment:Bouncer','The selected IMEI Number has been entered on the system before.')
        End ! If PreviousIMEI(job:ESN)
    End ! If local:Bouncers = 0

    If def:Allow_Bouncer <> 'YES'
        If local:Bouncers
            p_web.SSV('Comment:Bouncer','IMEI Number has been previously entered within the default period.')
            p_web.SSV('job:ESN','')
            Exit
        End ! If local:Bouncers
    Else ! If def:Allow_Bouncer <> 'YES'
        If GETINI('BOUNCER','StopLive',,Clip(Path()) & '\SB2KDEF.INI') = 1
            If local:LiveBouncers > 0
                p_web.SSV('Comment:IMEINumber','IMEI Number already exists on a Live Job.')
                p_web.SSV('job:ESN','')
                Exit
            End ! If LiveBouncers(job:ESN,Today())
        End ! If GETINI('BOUNCER','StopLive',,Clip(Path()) & '\SB2KDEF.INI') = 1
        If local:Bouncers > 0
            p_web.SSV('IMEIHistory','1')
            p_web.SSV('tmp:Bouncer','B')
            p_web.SSV('Comment:Bouncer',local:Bouncers & ' Previous Jobs(s)')
            p_web.SSV('Hide:PreviousAddress',0)
        End ! If local:Bouncers > 0
    End ! If def:Allow_Bouncer <> 'YES'
    ! ____________________________________________________________________________________

    p_web.SSV('Required:CourierWaybillNo',0)
    
    Access:TRANTYPE.Clearkey(trt:Transit_Type_Key)
    trt:Transit_Type = p_web.GSV('job:Transit_Type')
    If Access:TRANTYPE.TryFetch(trt:Transit_Type_Key) = Level:Benign
        p_web.FileToSessionQueue(TRANTYPE)

        If trt:Collection_Address = 'YES'
            p_web.SSV('Hide:CollectionAddress','')
        Else ! If trt:Collection_Address = 'YES'
            p_web.SSV('Hide:CollectionAddress',1)
        End ! If trt:Collection_Address = 'YES'

        If trt:Delivery_Address = 'YES'
            p_web.SSV('Hide:DeliveryAddress','')
        Else ! If trt:Delivery_Address = 'YES'
            p_web.SSV('Hide:DeliveryAddress',1)
        End ! If trt:Delivery_Address = 'YES'

        If trt:InternalLocation <> ''
            p_web.SSV('tmp:Location',trt:InternalLocation)
        End ! If trt:Internal_Location <> ''

        If trt:InWorkshop = 'YES'
            job:Workshop = 'YES'
        Else ! If trt:Workshop = 'YES'
            job:Workshop = 'NO'
        End ! If trt:Workshop = 'YES'
        p_web.SSV('job:Workshop',job:Workshop)


        If trt:Job_Card = 'YES'
            p_web.SSV('Hide:PrintJobCard','')
        Else ! If trt:JobCard = 'YES'
            p_web.SSV('Hide:PrintJobCard',1)
        End ! If trt:JobCard = 'YES'

        If trt:JobReceipt = 'YES'
            p_web.SSV('Hide:PrintJobReceipt','')
        Else ! If trt:JobReceipt = 'YES'
            p_web.SSV('Hide:PrintJobReceipt',1)
        End ! If trt:JobReceipt = 'YES'

        job:Turnaround_Time = trt:Initial_Priority
        p_web.SSV('job:Turnaround_Time',job:Turnaround_Time)
        
        ! #11880 Compulsory-ness is determined by Transit Type optinos. (Bryan: 06/05/2011)
        If (trt:WaybillComBook = 1)
            p_web.SSV('Required:CourierWaybillNo',1)
        END
        
    End ! If Access:TRANTYPE.TryFetch(trt:Transit_Type_Key) = Level:Benign


    IF def:Force_Engineer = 'B' And p_web.GSV('job:Workshop') = 'YES'
        p_web.SSV('Required:Engineer',1)
        p_web.SSV('Comment:Engineer','Required')
    Else ! IF def:Force_Engineer = 'B' And p_web.GSV('tmp:Workshop') = 1 And p_web.GSV('tmp:Engineer') = ''
        p_web.SSV('Required:Engineer',0)
        p_web.SSV('Comment:Engineer','')
    End ! IF def:Force_Engineer = 'B' And p_web.GSV('tmp:Workshop') = 1 And p_web.GSV('tmp:Engineer') = ''

!    Do MQ:Process
Value:Manufacturer      Routine
    If p_web.GSV('job:Manufacturer') = ''
        Exit
    End ! If p_web.GetSessionValue('job:Manufacturer') = ''

    Do Value:MSN
Value:MobileNumber      Routine
    p_web.SSV('job:Mobile_Number',Upper(p_web.GSV('job:Mobile_Number')))
    If p_web.GSV('job:Model_Number') = ''
        Exit
    End ! If p_web.GSV('job:Model_Number') = ''
    If p_web.GSV('job:Mobile_Number') = ''
        Exit
    End ! If p_web.GSV('job:Mobile_Number') = ''

    If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
        p_web.SSV('job:Mobile_Number','')
        p_web.SSV('Comment:MobileNumber','Mobile Number Format/Length Incorrect.')
    Else ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
        p_web.SSV('Comment:MobileNumber','')
    End ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0

    ! Inserting (DBH 07/12/2007) # 9491 - If SMS ALert Number is blank, use the job number
    If p_web.GSV('job:Mobile_Number') <> '' And p_web.GSV('jobe2:SMSAlertNumber') = ''
        p_web.SSV('jobe2:SMSAlertNumber',p_web.GSV('job:Mobile_Number'))
    End ! p_web.GSV('tmp:NotificationMobileNumber') = ''
    ! End (DBH 07/12/2007) #9491

Value:ModelNumber       Routine
Data
local:Colour        String(30)
Code
    Access:MODELNUM.ClearKey(mod:Model_Number_Key)
    mod:Model_Number = p_web.GSV('job:Model_Number')
    If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
        !Found
        If mod:Specify_Unit_Type = 'YES'
            job:Unit_Type = mod:Unit_Type
            p_web.SSV('job:Unit_Type',job:Unit_Type)
        End ! If mod:Specify_Unit_Type = 'YES'

        If p_web.GSV('Allow48Hour') = 1
            p_web.SSV('Hide:48Hour',1)
            Access:ESNMODEL.ClearKey(esn:ESN_Key)
            esn:ESN = Sub(p_web.GSV('job:ESN'),1,6)
            esn:Model_Number = p_web.GSV('job:Model_Number')
            If Access:ESNMODEL.TryFetch(esn:ESN_Key) = Level:Benign
                !Found
                If esn:Include48Hour
                    p_web.SSV('Hide:48Hour',0)
                End ! If esn:Include48Hour
            Else ! If Access:ESNMODEL.TryFetch(esn:ESN_Key) = Level:Benign
                !Error
            End ! If Access:ESNMODEL.TryFetch(esn:ESN_Key) = Level:Benign
        Else ! If p_web.GSV('Hide:48Hour') = 0
            p_web.SSV('Hide:48Hour',1)
        End ! If p_web.GSV('Hide:48Hour') = 0

        If p_web.GSV('Hide:ProductCode') = 1
            If p_web.GSV('job:Manufacturer') = 'SIEMENS' Or |
                p_web.GSV('job:Manufacturer') = 'NOKIA' Or |
                p_web.GSV('job:Manufacturer') = 'MOTOROLA'
                job:Fault_Code1 = mod:Product_Type
                p_web.SSV('job:Fault_Code1',job:Fault_Code1)
            End ! p_web.GSV('job:Manufacturer') = 'MOTOROLA'
        Else ! If p_web.GSV('ProductCodeRequied') = 0
            If p_web.GSV('job:Manufacturer') = 'SIEMENS' Or |
                p_web.GSV('job:Manufacturer') = 'NOKIA' Or |
                p_web.GSV('job:Manufacturer') = 'MOTOROLA'
                job:Fault_Code1 = p_web.GSV('job:Product_Code')
                p_web.SSV('job:Fault_Code1',job:Fault_Code1)
            End ! p_web.GSV('job:Manufacturer') = 'MOTOROLA'
        End ! If p_web.GSV('ProductCodeRequied') = 0

        local:Colour = ''
        Count# = 0
        Access:MODELCOL.Clearkey(moc:Colour_Key)
        moc:Model_Number = p_web.GSV('job:Model_Number')
        Set(moc:Colour_Key,moc:Colour_Key)
        Loop ! Begin Loop
            If Access:MODELCOL.Next()
                Break
            End ! If Access:MODELCOL.Next()
            If moc:Model_Number <> p_web.GSV('job:Model_Number')
                Break
            End ! If moc:Model_Number <> p_web.GSV('job:Model_Number')
            local:Colour = moc:Colour
            Count# += 1
            If Count# > 1
                Break
            End ! If Count# > 1
        End ! Loop
        Case Count#
        Of 0
            p_web.SSV('Hide:Colour',1)
        Of 1
            p_web.SSV('Hide:Colour',0)
            p_web.SSV('job:Colour',local:Colour)
        Else
            p_web.SSV('Hide:Colour',0)
        End ! Case Count#
    Else ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
        !Error
    End ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign

    Do Value:ProductCode
    Do Value:UnitType
Value:MSN           Routine
    if (len(clip(p_web.GSV('job:MSN'))) = 11 and p_web.GSV('job:Manufacturer') = 'ERICSSON') 
        p_web.SSV('job:MSN',sub(p_web.GSV('job:MSN'),2,10))
    end ! if (len(clip(p_web.GSV('tmp:MSN'))) and p_web.GSV('job:Manufacturer') = 'ERICSSON')
    
    job:Manufacturer = p_web.GetSessionValue('job:Manufacturer')
    job:MSN = p_web.GetSessionValue('job:MSN')
    job:Model_Number = p_web.GetSessionValue('job:Model_Number')
    If job:Manufacturer = ''
        Exit
    End ! If job:Manufacturer = ''

    If MSNRequired(job:Manufacturer)
        p_web.SetSessionValue('Hide:MSN',0)
        If ForceMSN(job:Manufacturer,'B')
            p_web.SetSessionValue('Required:MSN',1)
            p_web.SetSessionValue('Comment:MSN','Required')
        Else ! If ForceMSN('B')
            p_web.SetSessionValue('Required:MSN',0)
            p_web.SetSessionValue('Comment:MSN','')
        End ! If ForceMSN('B')
    Else ! If MSNRequired(job:Manufacturer)
        p_web.SetSessionValue('Hide:MSN',1)
    End ! If MSNRequired(job:Manufacturer)

    If local.ValidateMSN()
        Exit
    End ! If local.ValidateMSN()

    p_web.SetSessionValue('DispayPOP','')
    If p_web.GetSessionValue('OBFValidation') <> 'Passed'
        p_web.SetSessionValue('DispayPOP',1)
        Case p_web.GetSessionValue('job:Manufacturer')
        Of 'MOTOROLA' Orof 'SAMSUNG' Orof 'PHILIPS'
            If DateCodeValidation(p_web.GetSessionValue('job:Manufacturer'),p_web.GetSessionValue('job:MSN'),Today())
                p_web.SetSessionValue('DisplayPOP',1)
            Else ! If DateCodeValidation('MOTOROLA',p_web.GetSessionValue('job:MSN'),Today())
                job:Warranty_Job = 'YES'
                p_web.SetSessionValue('job:Warranty_Job',job:Warranty_Job)
                job:Warranty_Charge_Type = 'WARRANTY (MFTR)'
                p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
                If job:POP = ''
                    job:POP = 'F'
                    p_web.SetSessionValue('job:POP',job:POP)
                End ! If job:POP = ''
            End ! If DateCodeValidation('MOTOROLA',p_web.GetSessionValue('job:MSN'),Today())
        End ! Case p_web.GetSessionValue('job:Manufacturer')
    End ! If p_web.GetSessionValue('OBFValidation') <> 'Passed'

Value:POP       Routine
    Case p_web.GetSessionValue('job:POP')
    Of 'C'
        p_web.SetSessionValue('Required:DOP','')
        p_web.SetSessionValue('Comment:DOP','dd/mm/yyyy')
        job:Chargeable_Job = 'YES'
        job:Charge_Type = 'NON-WARRANTY'
        job:Warranty_Job = 'NO'
        job:Warranty_Charge_Type = ''
        p_web.SetSessionValue('ReadOnly:WarrantyJob',1)
        p_web.SetSessionValue('job:Chargeable_Job',job:Chargeable_Job)
        p_web.SetSessionValue('job:Warranty_Job',job:Warranty_Job)
        p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
        p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
        Do CalculateFaultCodes
    Of 'S'
        job:Chargeable_Job = 'NO'
        job:Charge_Type = ''
        job:Warranty_Job = 'YES'
        job:Warranty_Charge_Type = 'WARRANTY (2ND YR)'
        p_web.SetSessionValue('Required:DOP',1)
        p_web.SetSessionValue('Comment:DOP','Required (dd/mm/yyyy)')
        p_web.SetSessionValue('ReadOnly:WarrantyJob','')
        p_web.SetSessionValue('job:Chargeable_Job',job:Chargeable_Job)
        p_web.SetSessionValue('job:Warranty_Job',job:Warranty_Job)
        p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
        p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
        Do CalculateFaultCodes
    Of 'F'
        job:Chargeable_Job = 'NO'
        job:Charge_Type = ''
        job:Warranty_Job = 'YES'
        job:Warranty_Charge_Type = 'WARRANTY (MFTR)'
        p_web.SetSessionValue('Required:DOP',1)
        p_web.SetSessionValue('Comment:DOP','Required (dd/mm/yyyy)')
        p_web.SetSessionValue('ReadOnly:WarrantyJob','')
        p_web.SetSessionValue('job:Chargeable_Job',job:Chargeable_Job)
        p_web.SetSessionValue('job:Warranty_Job',job:Warranty_Job)
        p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
        p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
        Do CalculateFaultCodes
    Of 'O'
        p_web.SetSessionValue('Required:DOP','')
        Exit
    End ! Case tmp:POP

    
Value:ProductCode       Routine
    job:Model_Number = p_web.GetSessionValue('job:Model_Number')
    job:Manufacturer = p_web.GetSessionValue('job:Manufacturer')

    If job:Model_Number = '' Or job:Manufacturer = ''
        p_web.SetSessionValue('Hide:ProductCode',1)
        Exit
    End ! If job:Model_Number = ''

    If ProductCodeRequired(p_web.GetSessionValue('job:Manufacturer'))
        Found# = 0
        Access:MODPROD.Clearkey(mop:ProductCodeKey)
        mop:ModelNumber = job:Model_Number
        Set(mop:ProductCodeKey,mop:ProductCodeKey)
        Loop ! Begin Loop
            If Access:MODPROD.Next()
                Break
            End ! If Access:MODPROD.Next()
            If mop:ModelNumber <> job:Model_Number
                Break
            End ! If mop:ModelNumber <> job:Model_Number
            Found# = 1
            Break
        End ! Loop

        If Found# = 0
            p_web.SetSessionValue('Hide:ProductCode',1)
        Else ! If Found# = 0
            p_web.SetSessionValue('Hide:ProductCode',0)
        End ! If Found# = 0
    Else ! If ProductCodeRequired(job:Manufacturer)
        p_web.SetSessionValue('Hide:ProductCode',1)
    End ! If ProductCodeRequired(job:Manufacturer)
Value:UnitType      Routine
    If ForceUnitType('B')
        p_web.SetSessionValue('Required:UnitType',1)
        p_web.SetSessionValue('Comment:UnitType','Required')
    Else ! If ForceUnitType('B')
        p_web.SetSessionValue('Required:UnitType','')
        p_web.SetSessionValue('Comment:UnitType','')
    End ! If ForceUnitType('B')
Value:WarrantyChargeType        ROutine
    If p_web.GetSessionValue('job:Warranty_Job') = 'YES'
        Access:CHARTYPE.ClearKey(cha:Warranty_Key)
        cha:Warranty = 'YES'
        cha:Charge_Type = p_web.GetSessionValue('job:Warranty_Charge_Type')
        If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
            !Found
            If cha:ForceAuthorisation
                p_web.SetSessionValue('Required:AuthorityNumber',1)
                p_web.SetSessionValue('Comment:AuthorityNumber','Required')
            End ! If cha:ForceAuthorisation
        Else ! If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
            !Error
            p_web.SSV('job:Warranty_Charge_Type','')
        End ! If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign

    End ! If p_web.GetSessionValue('tmp:WarrantyJob') = 1 And Force# = 0

OpenFiles  ROUTINE
  p_web._OpenFile(PREJOB)
  p_web._OpenFile(TRADEACC_ALIAS)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(TRANTYPE)
  p_web._OpenFile(MANUFACT)
  p_web._OpenFile(MODELNUM)
  p_web._OpenFile(TRAHUBAC)
  p_web._OpenFile(DEFAULTS)
  p_web._OpenFile(ESNMODEL)
  p_web._OpenFile(MODPROD)
  p_web._OpenFile(UNITTYPE)
  p_web._OpenFile(MODELCOL)
  p_web._OpenFile(NETWORKS)
  p_web._OpenFile(ACCESSOR)
  p_web._OpenFile(JOBS_ALIAS)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(JOBSE2)
  p_web._OpenFile(CHARTYPE)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(WEBJOBNO)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(CONTHIST)
  p_web._OpenFile(COURIER)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(JOBNOTES)
  p_web._OpenFile(USERS)
  p_web._OpenFile(SUBURB)
  p_web._OpenFile(MANFAULO)
  p_web._OpenFile(MANFAULT)
  p_web._OpenFile(MANFAUEX)
  p_web._OpenFile(LOCATLOG)
  p_web._OpenFile(JOBACC)
  p_web._OpenFile(JOBSOBF)
  p_web._OpenFile(JOBSE_ALIAS)
  p_web._OpenFile(AUDIT)
  p_web._OpenFile(AUDSTATS)
  p_web._OpenFile(STATUS)
  p_web._OpenFile(STAHEAD)
  p_web._OpenFile(JOBSTAGE)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(PREJOB)
  p_Web._CloseFile(TRADEACC_ALIAS)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(TRANTYPE)
  p_Web._CloseFile(MANUFACT)
  p_Web._CloseFile(MODELNUM)
  p_Web._CloseFile(TRAHUBAC)
  p_Web._CloseFile(DEFAULTS)
  p_Web._CloseFile(ESNMODEL)
  p_Web._CloseFile(MODPROD)
  p_Web._CloseFile(UNITTYPE)
  p_Web._CloseFile(MODELCOL)
  p_Web._CloseFile(NETWORKS)
  p_Web._CloseFile(ACCESSOR)
  p_Web._CloseFile(JOBS_ALIAS)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBSE2)
  p_Web._CloseFile(CHARTYPE)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(WEBJOBNO)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(CONTHIST)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(JOBNOTES)
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(SUBURB)
  p_Web._CloseFile(MANFAULO)
  p_Web._CloseFile(MANFAULT)
  p_Web._CloseFile(MANFAUEX)
  p_Web._CloseFile(LOCATLOG)
  p_Web._CloseFile(JOBACC)
  p_Web._CloseFile(JOBSOBF)
  p_Web._CloseFile(JOBSE_ALIAS)
  p_Web._CloseFile(AUDIT)
  p_Web._CloseFile(AUDSTATS)
  p_Web._CloseFile(STATUS)
  p_Web._CloseFile(STAHEAD)
  p_Web._CloseFile(JOBSTAGE)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  IF (p_web.IfExistsValue('PreBookingRefNo'))
      p_web.StoreValue('PreBookingRefNo')
  END  
  p_web.SetValue('NewJobBooking_form:inited_',1)
  p_web.SetValue('UpdateFile','JOBS')
  p_web.SetValue('UpdateKey','job:Ref_Number_Key')
  p_web.SetValue('IDField','job:Ref_Number')
  do RestoreMem

CancelForm  Routine
  Do DeleteVariables
  !lear Multiple Job Booking Variables
  p_web.DeleteSessionValue('mj:InProgress')
  p_web.DeleteSessionValue('mj:Colour')
  p_web.DeleteSessionValue('mj:OrderNumber')
  p_web.DeleteSessionValue('mj:FaultCode')
  p_web.DeleteSessionValue('mj:EngineersNotes')
  p_web.DeleteSessionValue('mj:PreviousJobNumber')
                 
    DuplicateTabCheckDelete(p_web,'NEWJOBBOOKING','') ! Clear SessionID
    DuplicateTabCheckDelete(p_web,'NEWJOBBOOKINGIMEI','')
  IF p_web.GetSessionValue('NewJobBooking:Primed') = 1
    p_web._deleteFile(JOBS)
    p_web.SetSessionValue('NewJobBooking:Primed',0)
  End

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)
      IF (p_web.GSV('Hide:ButtonMSISDNCheck') <> 1)
          do Value::locMobileAverageSpend  !1
          do Value::locMobileIDNumber  !1
          do Value::locMobileLifetimeValue  !1
          do Value::locMobileLoyaltyStatus  !1
          do Value::locMobileUpgradeDate  !1
      END

SetPics        Routine
  p_web.SetValue('UpdateFile','JOBS')
  p_web.SetValue('UpdateKey','job:Ref_Number_Key')
  If p_web.IfExistsValue('job:Transit_Type')
    p_web.SetPicture('job:Transit_Type','@s30')
  End
  p_web.SetSessionPicture('job:Transit_Type','@s30')
  If p_web.IfExistsValue('job:ESN')
    p_web.SetPicture('job:ESN','@s20')
  End
  p_web.SetSessionPicture('job:ESN','@s20')
  If p_web.IfExistsValue('job:Mobile_Number')
    p_web.SetPicture('job:Mobile_Number','@s15')
  End
  p_web.SetSessionPicture('job:Mobile_Number','@s15')
  If p_web.IfExistsValue('job:Unit_Type')
    p_web.SetPicture('job:Unit_Type','@s30')
  End
  p_web.SetSessionPicture('job:Unit_Type','@s30')
  If p_web.IfExistsValue('job:ProductCode')
    p_web.SetPicture('job:ProductCode','@s30')
  End
  p_web.SetSessionPicture('job:ProductCode','@s30')
  If p_web.IfExistsValue('job:Colour')
    p_web.SetPicture('job:Colour','@s30')
  End
  p_web.SetSessionPicture('job:Colour','@s30')
  If p_web.IfExistsValue('job:MSN')
    p_web.SetPicture('job:MSN','@s20')
  End
  p_web.SetSessionPicture('job:MSN','@s20')
  If p_web.IfExistsValue('job:DOP')
    p_web.SetPicture('job:DOP','@d06b')
  End
  p_web.SetSessionPicture('job:DOP','@d06b')
  If p_web.IfExistsValue('job:Charge_Type')
    p_web.SetPicture('job:Charge_Type','@s30')
  End
  p_web.SetSessionPicture('job:Charge_Type','@s30')
  If p_web.IfExistsValue('job:Warranty_Charge_Type')
    p_web.SetPicture('job:Warranty_Charge_Type','@s30')
  End
  p_web.SetSessionPicture('job:Warranty_Charge_Type','@s30')
  If p_web.IfExistsValue('job:Authority_Number')
    p_web.SetPicture('job:Authority_Number','@s30')
  End
  p_web.SetSessionPicture('job:Authority_Number','@s30')
  If p_web.IfExistsValue('job:Account_Number')
    p_web.SetPicture('job:Account_Number','@s15')
  End
  p_web.SetSessionPicture('job:Account_Number','@s15')
  If p_web.IfExistsValue('job:Account_Number')
    p_web.SetPicture('job:Account_Number','@s15')
  End
  p_web.SetSessionPicture('job:Account_Number','@s15')
  If p_web.IfExistsValue('job:Order_Number')
    p_web.SetPicture('job:Order_Number','@s30')
  End
  p_web.SetSessionPicture('job:Order_Number','@s30')
  If p_web.IfExistsValue('job:Title')
    p_web.SetPicture('job:Title','@s4')
  End
  p_web.SetSessionPicture('job:Title','@s4')
  If p_web.IfExistsValue('job:Initial')
    p_web.SetPicture('job:Initial','@s1')
  End
  p_web.SetSessionPicture('job:Initial','@s1')
  If p_web.IfExistsValue('job:Surname')
    p_web.SetPicture('job:Surname','@s30')
  End
  p_web.SetSessionPicture('job:Surname','@s30')
  If p_web.IfExistsValue('job:Courier')
    p_web.SetPicture('job:Courier','@s30')
  End
  p_web.SetSessionPicture('job:Courier','@s30')
  If p_web.IfExistsValue('job:Engineer')
    p_web.SetPicture('job:Engineer','@s3')
  End
  p_web.SetSessionPicture('job:Engineer','@s3')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  If p_web.GSV('Hide:PreviousAddress') <> 1
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'jobe:Network'
    p_web.setsessionvalue('showtab_NewJobBooking',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(NETWORKS)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.')
  Of 'job:Unit_Type'
    p_web.setsessionvalue('showtab_NewJobBooking',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(UNITTYPE)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.job:ProductCode')
  Of 'job:ProductCode'
    p_web.setsessionvalue('showtab_NewJobBooking',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MODPROD)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.job:Colour')
  Of 'job:Colour'
    p_web.setsessionvalue('showtab_NewJobBooking',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MODELCOL)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.job:MSN')
  End
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'job:Charge_Type'
    p_web.setsessionvalue('showtab_NewJobBooking',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(CHARTYPE)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.job:Warranty_Job')
  Of 'job:Warranty_Charge_Type'
    p_web.setsessionvalue('showtab_NewJobBooking',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(CHARTYPE)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.job:Authority_Number')
  End
  If p_web.GSV('Hide:ButtonMSISDNCheck') <> 1
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'job:Account_Number'
    p_web.setsessionvalue('showtab_NewJobBooking',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(SUBTRACC)
      Do Value:AccountNumber
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.job:Account_Number')
  Of 'job:Account_Number'
    p_web.setsessionvalue('showtab_NewJobBooking',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(TRAHUBAC)
      Do Value:AccountNumber
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.job:Account_Number')
  Of 'job:Courier'
    p_web.setsessionvalue('showtab_NewJobBooking',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(COURIER)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.jobe2:CourierWaybillNumber')
  End
  loc:TabNumber += 1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'tmp:FaultCode1'
    p_web.setsessionvalue('showtab_NewJobBooking',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MANFAULO)
      local.FaultCodeDescription(1)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:FaultCode2')
  Of 'tmp:FaultCode2'
    p_web.setsessionvalue('showtab_NewJobBooking',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MANFAULO)
      local.FaultCodeDescription(2)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:FaultCode3')
  Of 'tmp:FaultCode3'
    p_web.setsessionvalue('showtab_NewJobBooking',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MANFAULO)
      local.FaultCodeDescription(3)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:FaultCode4')
  Of 'tmp:FaultCode4'
    p_web.setsessionvalue('showtab_NewJobBooking',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MANFAULO)
      local.FaultCodeDescription(4)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:FaultCode5')
  Of 'tmp:FaultCode5'
    p_web.setsessionvalue('showtab_NewJobBooking',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MANFAULO)
      local.FaultCodeDescription(5)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:FaultCode6')
  Of 'tmp:FaultCode6'
    p_web.setsessionvalue('showtab_NewJobBooking',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MANFAULO)
      local.FaultCodeDescription(6)
      
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.job:Engineer')
  End
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'job:Engineer'
    p_web.setsessionvalue('showtab_NewJobBooking',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(USERS)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.')
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locPreviousJobNumber',locPreviousJobNumber)
  p_web.SetSessionValue('locPreviousOutFault',locPreviousOutFault)
  p_web.SetSessionValue('tmp:PreviousAddress',tmp:PreviousAddress)
  p_web.SetSessionValue('tmp:UsePreviousAddress',tmp:UsePreviousAddress)
  p_web.SetSessionValue('jobe:Network',jobe:Network)
  p_web.SetSessionValue('jobe2:POPConfirmed',jobe2:POPConfirmed)
  p_web.SetSessionValue('locPOPTypePassword',locPOPTypePassword)
  p_web.SetSessionValue('tmp:POPType',tmp:POPType)
  p_web.SetSessionValue('jobe2:WarrantyRefNo',jobe2:WarrantyRefNo)
  p_web.SetSessionValue('locMobileLifetimeValue',locMobileLifetimeValue)
  p_web.SetSessionValue('locMobileAverageSpend',locMobileAverageSpend)
  p_web.SetSessionValue('locMobileLoyaltyStatus',locMobileLoyaltyStatus)
  p_web.SetSessionValue('locMobileUpgradeDate',locMobileUpgradeDate)
  p_web.SetSessionValue('locMobileIDNumber',locMobileIDNumber)
  p_web.SetSessionValue('FranchiseAccount',FranchiseAccount)
  p_web.SetSessionValue('jobe:EndUserTelNo',jobe:EndUserTelNo)
  p_web.SetSessionValue('jobe2:CourierWaybillNumber',jobe2:CourierWaybillNumber)
  p_web.SetSessionValue('tmp:AmendAccessories',tmp:AmendAccessories)
  p_web.SetSessionValue('tmp:ShowAccessory',tmp:ShowAccessory)
  p_web.SetSessionValue('tmp:TheAccessory',tmp:TheAccessory)
  p_web.SetSessionValue('tmp:AmendFaultCodes',tmp:AmendFaultCodes)
  p_web.SetSessionValue('tmp:FaultCode1',tmp:FaultCode1)
  p_web.SetSessionValue('tmp:FaultCode2',tmp:FaultCode2)
  p_web.SetSessionValue('tmp:FaultCode3',tmp:FaultCode3)
  p_web.SetSessionValue('tmp:FaultCode4',tmp:FaultCode4)
  p_web.SetSessionValue('tmp:FaultCode5',tmp:FaultCode5)
  p_web.SetSessionValue('tmp:FaultCode6',tmp:FaultCode6)
  p_web.SetSessionValue('jbn:Fault_Description',jbn:Fault_Description)
  p_web.SetSessionValue('jbn:Engineers_Notes',jbn:Engineers_Notes)
  p_web.SetSessionValue('tmp:ExternalDamageChecklist',tmp:ExternalDamageChecklist)
  p_web.SetSessionValue('jobe2:XNone',jobe2:XNone)
  p_web.SetSessionValue('jobe2:XAntenna',jobe2:XAntenna)
  p_web.SetSessionValue('jobe2:XLens',jobe2:XLens)
  p_web.SetSessionValue('jobe2:XFCover',jobe2:XFCover)
  p_web.SetSessionValue('jobe2:XBCover',jobe2:XBCover)
  p_web.SetSessionValue('jobe2:XKeypad',jobe2:XKeypad)
  p_web.SetSessionValue('jobe2:XBattery',jobe2:XBattery)
  p_web.SetSessionValue('jobe2:XCharger',jobe2:XCharger)
  p_web.SetSessionValue('jobe2:XLCD',jobe2:XLCD)
  p_web.SetSessionValue('jobe2:XSimReader',jobe2:XSimReader)
  p_web.SetSessionValue('jobe2:XSystemConnector',jobe2:XSystemConnector)
  p_web.SetSessionValue('jobe2:XNotes',jobe2:XNotes)
  p_web.SetSessionValue('jobe:Booking48HourOption',jobe:Booking48HourOption)
  p_web.SetSessionValue('jobe:VSACustomer',jobe:VSACustomer)
  p_web.SetSessionValue('jobe2:Contract',jobe2:Contract)
  p_web.SetSessionValue('jobe2:Prepaid',jobe2:Prepaid)

RestoreMem       Routine
  !FormSource=File
  if p_web.IfExistsValue('locPreviousJobNumber')
    locPreviousJobNumber = p_web.GetValue('locPreviousJobNumber')
    p_web.SetSessionValue('locPreviousJobNumber',locPreviousJobNumber)
  End
  if p_web.IfExistsValue('locPreviousOutFault')
    locPreviousOutFault = p_web.GetValue('locPreviousOutFault')
    p_web.SetSessionValue('locPreviousOutFault',locPreviousOutFault)
  End
  if p_web.IfExistsValue('tmp:PreviousAddress')
    tmp:PreviousAddress = p_web.GetValue('tmp:PreviousAddress')
    p_web.SetSessionValue('tmp:PreviousAddress',tmp:PreviousAddress)
  End
  if p_web.IfExistsValue('tmp:UsePreviousAddress')
    tmp:UsePreviousAddress = p_web.GetValue('tmp:UsePreviousAddress')
    p_web.SetSessionValue('tmp:UsePreviousAddress',tmp:UsePreviousAddress)
  End
  if p_web.IfExistsValue('jobe:Network')
    jobe:Network = p_web.GetValue('jobe:Network')
    p_web.SetSessionValue('jobe:Network',jobe:Network)
  End
  if p_web.IfExistsValue('jobe2:POPConfirmed')
    jobe2:POPConfirmed = p_web.GetValue('jobe2:POPConfirmed')
    p_web.SetSessionValue('jobe2:POPConfirmed',jobe2:POPConfirmed)
  End
  if p_web.IfExistsValue('locPOPTypePassword')
    locPOPTypePassword = p_web.GetValue('locPOPTypePassword')
    p_web.SetSessionValue('locPOPTypePassword',locPOPTypePassword)
  End
  if p_web.IfExistsValue('tmp:POPType')
    tmp:POPType = p_web.GetValue('tmp:POPType')
    p_web.SetSessionValue('tmp:POPType',tmp:POPType)
  End
  if p_web.IfExistsValue('jobe2:WarrantyRefNo')
    jobe2:WarrantyRefNo = p_web.GetValue('jobe2:WarrantyRefNo')
    p_web.SetSessionValue('jobe2:WarrantyRefNo',jobe2:WarrantyRefNo)
  End
  if p_web.IfExistsValue('locMobileLifetimeValue')
    locMobileLifetimeValue = p_web.GetValue('locMobileLifetimeValue')
    p_web.SetSessionValue('locMobileLifetimeValue',locMobileLifetimeValue)
  End
  if p_web.IfExistsValue('locMobileAverageSpend')
    locMobileAverageSpend = p_web.GetValue('locMobileAverageSpend')
    p_web.SetSessionValue('locMobileAverageSpend',locMobileAverageSpend)
  End
  if p_web.IfExistsValue('locMobileLoyaltyStatus')
    locMobileLoyaltyStatus = p_web.GetValue('locMobileLoyaltyStatus')
    p_web.SetSessionValue('locMobileLoyaltyStatus',locMobileLoyaltyStatus)
  End
  if p_web.IfExistsValue('locMobileUpgradeDate')
    locMobileUpgradeDate = p_web.GetValue('locMobileUpgradeDate')
    p_web.SetSessionValue('locMobileUpgradeDate',locMobileUpgradeDate)
  End
  if p_web.IfExistsValue('locMobileIDNumber')
    locMobileIDNumber = p_web.GetValue('locMobileIDNumber')
    p_web.SetSessionValue('locMobileIDNumber',locMobileIDNumber)
  End
  if p_web.IfExistsValue('FranchiseAccount')
    FranchiseAccount = p_web.GetValue('FranchiseAccount')
    p_web.SetSessionValue('FranchiseAccount',FranchiseAccount)
  End
  if p_web.IfExistsValue('jobe:EndUserTelNo')
    jobe:EndUserTelNo = p_web.GetValue('jobe:EndUserTelNo')
    p_web.SetSessionValue('jobe:EndUserTelNo',jobe:EndUserTelNo)
  End
  if p_web.IfExistsValue('jobe2:CourierWaybillNumber')
    jobe2:CourierWaybillNumber = p_web.GetValue('jobe2:CourierWaybillNumber')
    p_web.SetSessionValue('jobe2:CourierWaybillNumber',jobe2:CourierWaybillNumber)
  End
  if p_web.IfExistsValue('tmp:AmendAccessories')
    tmp:AmendAccessories = p_web.GetValue('tmp:AmendAccessories')
    p_web.SetSessionValue('tmp:AmendAccessories',tmp:AmendAccessories)
  End
  if p_web.IfExistsValue('tmp:ShowAccessory')
    tmp:ShowAccessory = p_web.GetValue('tmp:ShowAccessory')
    p_web.SetSessionValue('tmp:ShowAccessory',tmp:ShowAccessory)
  End
  if p_web.IfExistsValue('tmp:TheAccessory')
    tmp:TheAccessory = p_web.GetValue('tmp:TheAccessory')
    p_web.SetSessionValue('tmp:TheAccessory',tmp:TheAccessory)
  End
  if p_web.IfExistsValue('tmp:AmendFaultCodes')
    tmp:AmendFaultCodes = p_web.GetValue('tmp:AmendFaultCodes')
    p_web.SetSessionValue('tmp:AmendFaultCodes',tmp:AmendFaultCodes)
  End
  if p_web.IfExistsValue('tmp:FaultCode1')
    tmp:FaultCode1 = p_web.GetValue('tmp:FaultCode1')
    p_web.SetSessionValue('tmp:FaultCode1',tmp:FaultCode1)
  End
  if p_web.IfExistsValue('tmp:FaultCode2')
    tmp:FaultCode2 = p_web.GetValue('tmp:FaultCode2')
    p_web.SetSessionValue('tmp:FaultCode2',tmp:FaultCode2)
  End
  if p_web.IfExistsValue('tmp:FaultCode3')
    tmp:FaultCode3 = p_web.GetValue('tmp:FaultCode3')
    p_web.SetSessionValue('tmp:FaultCode3',tmp:FaultCode3)
  End
  if p_web.IfExistsValue('tmp:FaultCode4')
    tmp:FaultCode4 = p_web.GetValue('tmp:FaultCode4')
    p_web.SetSessionValue('tmp:FaultCode4',tmp:FaultCode4)
  End
  if p_web.IfExistsValue('tmp:FaultCode5')
    tmp:FaultCode5 = p_web.GetValue('tmp:FaultCode5')
    p_web.SetSessionValue('tmp:FaultCode5',tmp:FaultCode5)
  End
  if p_web.IfExistsValue('tmp:FaultCode6')
    tmp:FaultCode6 = p_web.GetValue('tmp:FaultCode6')
    p_web.SetSessionValue('tmp:FaultCode6',tmp:FaultCode6)
  End
  if p_web.IfExistsValue('jbn:Fault_Description')
    jbn:Fault_Description = p_web.GetValue('jbn:Fault_Description')
    p_web.SetSessionValue('jbn:Fault_Description',jbn:Fault_Description)
  End
  if p_web.IfExistsValue('jbn:Engineers_Notes')
    jbn:Engineers_Notes = p_web.GetValue('jbn:Engineers_Notes')
    p_web.SetSessionValue('jbn:Engineers_Notes',jbn:Engineers_Notes)
  End
  if p_web.IfExistsValue('tmp:ExternalDamageChecklist')
    tmp:ExternalDamageChecklist = p_web.GetValue('tmp:ExternalDamageChecklist')
    p_web.SetSessionValue('tmp:ExternalDamageChecklist',tmp:ExternalDamageChecklist)
  End
  if p_web.IfExistsValue('jobe2:XNone')
    jobe2:XNone = p_web.GetValue('jobe2:XNone')
    p_web.SetSessionValue('jobe2:XNone',jobe2:XNone)
  End
  if p_web.IfExistsValue('jobe2:XAntenna')
    jobe2:XAntenna = p_web.GetValue('jobe2:XAntenna')
    p_web.SetSessionValue('jobe2:XAntenna',jobe2:XAntenna)
  End
  if p_web.IfExistsValue('jobe2:XLens')
    jobe2:XLens = p_web.GetValue('jobe2:XLens')
    p_web.SetSessionValue('jobe2:XLens',jobe2:XLens)
  End
  if p_web.IfExistsValue('jobe2:XFCover')
    jobe2:XFCover = p_web.GetValue('jobe2:XFCover')
    p_web.SetSessionValue('jobe2:XFCover',jobe2:XFCover)
  End
  if p_web.IfExistsValue('jobe2:XBCover')
    jobe2:XBCover = p_web.GetValue('jobe2:XBCover')
    p_web.SetSessionValue('jobe2:XBCover',jobe2:XBCover)
  End
  if p_web.IfExistsValue('jobe2:XKeypad')
    jobe2:XKeypad = p_web.GetValue('jobe2:XKeypad')
    p_web.SetSessionValue('jobe2:XKeypad',jobe2:XKeypad)
  End
  if p_web.IfExistsValue('jobe2:XBattery')
    jobe2:XBattery = p_web.GetValue('jobe2:XBattery')
    p_web.SetSessionValue('jobe2:XBattery',jobe2:XBattery)
  End
  if p_web.IfExistsValue('jobe2:XCharger')
    jobe2:XCharger = p_web.GetValue('jobe2:XCharger')
    p_web.SetSessionValue('jobe2:XCharger',jobe2:XCharger)
  End
  if p_web.IfExistsValue('jobe2:XLCD')
    jobe2:XLCD = p_web.GetValue('jobe2:XLCD')
    p_web.SetSessionValue('jobe2:XLCD',jobe2:XLCD)
  End
  if p_web.IfExistsValue('jobe2:XSimReader')
    jobe2:XSimReader = p_web.GetValue('jobe2:XSimReader')
    p_web.SetSessionValue('jobe2:XSimReader',jobe2:XSimReader)
  End
  if p_web.IfExistsValue('jobe2:XSystemConnector')
    jobe2:XSystemConnector = p_web.GetValue('jobe2:XSystemConnector')
    p_web.SetSessionValue('jobe2:XSystemConnector',jobe2:XSystemConnector)
  End
  if p_web.IfExistsValue('jobe2:XNotes')
    jobe2:XNotes = p_web.GetValue('jobe2:XNotes')
    p_web.SetSessionValue('jobe2:XNotes',jobe2:XNotes)
  End
  if p_web.IfExistsValue('jobe:Booking48HourOption')
    jobe:Booking48HourOption = p_web.GetValue('jobe:Booking48HourOption')
    p_web.SetSessionValue('jobe:Booking48HourOption',jobe:Booking48HourOption)
  End
  if p_web.IfExistsValue('jobe:VSACustomer')
    jobe:VSACustomer = p_web.GetValue('jobe:VSACustomer')
    p_web.SetSessionValue('jobe:VSACustomer',jobe:VSACustomer)
  End
  if p_web.IfExistsValue('jobe2:Contract')
    jobe2:Contract = p_web.GetValue('jobe2:Contract')
    p_web.SetSessionValue('jobe2:Contract',jobe2:Contract)
  End
  if p_web.IfExistsValue('jobe2:Prepaid')
    jobe2:Prepaid = p_web.GetValue('jobe2:Prepaid')
    p_web.SetSessionValue('jobe2:Prepaid',jobe2:Prepaid)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('NewJobBooking_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    IF (DuplicateTabCheck(p_web,'NEWJOBBOOKING',1) = 0 OR DuplicateTabCheck(p_web,'JOBUPDATEREFNO',''))
        CreateScript(packet,'alert("Error. A job booking/editing may already be in progress, or a previous booking/editing did not complete correctly.\n\nRe-login if you wish to try again.");window.open("IndexPage","_self")')
        DO SendPacket
        EXIT
    ELSE
        CASE (DuplicateTabCheck(p_web,'NEWJOBBOOKINGIMEI',p_web.GSV('job:ESN')))
        OF 0 ! Not Found
            DuplicateTabCheckAdd(p_web,'NEWJOBBOOKINGIMEI',p_web.GSV('job:ESN'))
        OF 1 ! Found
        OF 2 ! Found Type, but not matching value
            CreateScript(packet,'alert("Error. A job booking/editing may already be in progress, or a previous booking/editing did not complete correctly.\n\nRe-login if you wish to try again.");window.open("IndexPage","_self")')
            DO SendPacket
            EXIT
        END ! CASE
    END ! IF   
  ! Start Of Page (Loaded Each Time)
    
  SET(Defaults)
  Access:Defaults.Next()
  
  
  p_web.site.SaveButton.TextValue = 'Book Job'
  p_web.site.CancelButton.TextValue = 'Quit'
  If p_web.GetSessionValue('ReadyForNewJobBooking') = ''
      loc:Invalid = 'job:ESN'
      loc:Alert = 'An Error Has Occurred. Please "Quit Booking" and try again!'
      !Exit
  End ! If p_web.GetSessionValue('ReadyForNewJobBooking') = ''
  
  If p_web.GSV('OBFValidation') = 'Passed'
      job:POP = 'O'
      job:Warranty_Job = 'YES'
      job:Warranty_Charge_Type = 'WARRANTY (OBF)'
      job:Chargeable_Job = 'NO'
      p_web.SSV('job:POP',job:POP)
      p_web.SSV('job:Warranty_Job',job:Warranty_Job)
      p_web.SSV('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
      p_web.SSV('job:Chargeable_Job',job:Chargeable_Job)
  
      p_web.SSV('jobe:OBFValidated',1)
      p_web.SSV('jobe:OBFValidateDate',Today())
      p_web.SSV('jobe:OBFValidateTime',Clock())
      p_web.SSV('jobe:Booking48HourOption',2)
      p_web.SSV('jobe:HubRepair',1)
      p_web.SSV('jobe:HubRepairDate',Today())
      p_web.SSV('jobe:HubRepairTime',Clock())
      p_web.SSV('ReadOnly:HubRepair',1)
      p_web.SSV('filter:WarrantyChargeType','cha:Charge_Type = ''WARRANTY (OBF)''')
      p_web.SSV('ReadOnly:WarrantyJob',1)
      p_web.SSV('ReadOnly:ChargeableJob',1)
      p_web.SSV('ReadOnly:WarrantyChargeType',1)
      p_web.SSV('Comment:POP','OBF Passed')
      p_web.SSV('Drop:JobType','-----------OBF Passed------------')
      p_web.SSV('Drop:JobTypeDefault','O')
  else
      p_web.SSV('jobe:OBFValidated',0)
      p_web.SSV('jobe:OBFValidateDate',0)
      p_web.SSV('jobe:OBFValidateTime',0)
  End ! If p_web.GSV('OBFValidaton') = 'Passed'
  
  if (p_web.GSV('Hide:ChangeDOP') = 0 and p_web.GSV('tmp:NewDOP') <> 0 and p_web.GSV('tmp:NewDOP') <> p_web.GSV('job:DOP'))
      job:DOP = p_web.GSV('tmp:NewDOP')
      p_web.SSV('job:DOP',job:DOP)
      Do Value:DOP
  end !if (p_web.GSV('tmp:NewDOP') <> p_web.GSV('job:DOP'))
  
  ! DBH #11388 - Show/Hide MSISDN Check Button
  p_web.SSV('Hide:ButtonMSISDNCheck',0)
  Access:TRADEACC.CLearkey(tra:Account_Number_Key)
  tra:Account_Number = p_web.GSV('BookingAccount')
  If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
      If ~tra:DoMSISDNCheck
          p_web.SSV('Hide:ButtonMSISDNCheck',1)
      End ! If ~tra:DoMSISDNCheck
  End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
  
  ! Bouncer History has been viewed and a new account has been applied.
  If (p_web.GSV('NewAccountAuthorised') = 1 AND p_web.GSV('job:Account_Number') <> '')
      DO Value:AccountNumber
  END ! If (p_web.GSV('NewAccountAuthorised') = 1)
  
  p_web.SSV('ReadOnly:POPType',1) ! Disable POP Type
  p_web.SSV('Comment:POPTypePassword',kCommentPOPTypePassword)
  
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locPreviousJobNumber = p_web.RestoreValue('locPreviousJobNumber')
 locPreviousOutFault = p_web.RestoreValue('locPreviousOutFault')
 tmp:PreviousAddress = p_web.RestoreValue('tmp:PreviousAddress')
 tmp:UsePreviousAddress = p_web.RestoreValue('tmp:UsePreviousAddress')
 locPOPTypePassword = p_web.RestoreValue('locPOPTypePassword')
 tmp:POPType = p_web.RestoreValue('tmp:POPType')
 locMobileLifetimeValue = p_web.RestoreValue('locMobileLifetimeValue')
 locMobileAverageSpend = p_web.RestoreValue('locMobileAverageSpend')
 locMobileLoyaltyStatus = p_web.RestoreValue('locMobileLoyaltyStatus')
 locMobileUpgradeDate = p_web.RestoreValue('locMobileUpgradeDate')
 locMobileIDNumber = p_web.RestoreValue('locMobileIDNumber')
 FranchiseAccount = p_web.RestoreValue('FranchiseAccount')
 tmp:AmendAccessories = p_web.RestoreValue('tmp:AmendAccessories')
 tmp:ShowAccessory = p_web.RestoreValue('tmp:ShowAccessory')
 tmp:TheAccessory = p_web.RestoreValue('tmp:TheAccessory')
 tmp:AmendFaultCodes = p_web.RestoreValue('tmp:AmendFaultCodes')
 tmp:FaultCode1 = p_web.RestoreValue('tmp:FaultCode1')
 tmp:FaultCode2 = p_web.RestoreValue('tmp:FaultCode2')
 tmp:FaultCode3 = p_web.RestoreValue('tmp:FaultCode3')
 tmp:FaultCode4 = p_web.RestoreValue('tmp:FaultCode4')
 tmp:FaultCode5 = p_web.RestoreValue('tmp:FaultCode5')
 tmp:FaultCode6 = p_web.RestoreValue('tmp:FaultCode6')
 tmp:ExternalDamageChecklist = p_web.RestoreValue('tmp:ExternalDamageChecklist')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'InsertJob_Finished'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('NewJobBooking_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('NewJobBooking_ChainTo')
    loc:formaction = p_web.GetSessionValue('NewJobBooking_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'IndexPage'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  loc:TabHeight = 100
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  packet = clip(packet) & '<input type="hidden" name="JOBS__FileAction" value="'&p_web.getSessionValue('JOBS:FileAction')&'" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="file" value="JOBS" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateFile" value="JOBS" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateKey" value="job:Ref_Number_Key" ></input><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="NewJobBooking" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="NewJobBooking" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="NewJobBooking" ></input><13,10>'
  end

  do SendPacket
  packet = clip(packet) & p_web.CreateInput('hidden','job:Ref_Number',p_web._jsok(p_web.getSessionValue('job:Ref_Number'))) & '<13,10>'
  If p_web.Translate('New Job Booking') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('New Job Booking',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_NewJobBooking">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_NewJobBooking" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  do GenerateTab4
  do GenerateTab5
  do GenerateTab6
  do GenerateTab7
  do GenerateTab8
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_NewJobBooking')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Initial Details') & ''''
        If p_web.GSV('Hide:PreviousAddress') <> 1
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Bouncer Details') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Unit Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Job Type') & ''''
        If p_web.GSV('Hide:ButtonMSISDNCheck') <> 1
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Customer Classification') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Account Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Accessories') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Fault Codes') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Engineering') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_NewJobBooking')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_NewJobBooking'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='NETWORKS'
            p_web.SetValue('SelectField',clip(loc:formname) & '.job:Mobile_Number')
    End
    If upper(p_web.getvalue('LookupFile'))='UNITTYPE'
        If p_web.GetSessionValue('Hide:ProductCode') <> 1
            p_web.SetValue('SelectField',clip(loc:formname) & '.job:ProductCode')
        End
    End
    If upper(p_web.getvalue('LookupFile'))='MODPROD'
        If p_web.GetSessionValue('Hide:Colour') <> 1
            p_web.SetValue('SelectField',clip(loc:formname) & '.job:Colour')
        End
    End
    If upper(p_web.getvalue('LookupFile'))='MODELCOL'
        If p_web.GetSessionValue('Hide:MSN') <> 1
            p_web.SetValue('SelectField',clip(loc:formname) & '.job:MSN')
        End
    End
    If upper(p_web.getvalue('LookupFile'))='CHARTYPE'
          If Not (p_web.GSV('job:Warranty_Job') <> 'YES')
            p_web.SetValue('SelectField',clip(loc:formname) & '.job:Warranty_Charge_Type')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='CHARTYPE'
          If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0)
            p_web.SetValue('SelectField',clip(loc:formname) & '.job:Authority_Number')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='SUBTRACC'
          If Not (p_web.GSV('FranchiseAccount') <> 2)
            p_web.SetValue('SelectField',clip(loc:formname) & '.job:Account_Number')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='TRAHUBAC'
          If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('FranchiseAccount') = 0)
            p_web.SetValue('SelectField',clip(loc:formname) & '.job:Order_Number')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='COURIER'
          If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0)
            p_web.SetValue('SelectField',clip(loc:formname) & '.jobe2:CourierWaybillNumber')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='MANFAULO'
          If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode2') = 1)
            p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:FaultCode2')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='MANFAULO'
          If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode3') = 1)
            p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:FaultCode3')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='MANFAULO'
          If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode4') = 1)
            p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:FaultCode4')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='MANFAULO'
          If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode5') = 1)
            p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:FaultCode5')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='MANFAULO'
          If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode6') = 1)
            p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:FaultCode6')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='MANFAULO'
          If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0)
            p_web.SetValue('SelectField',clip(loc:formname) & '.job:Engineer')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='USERS'
          If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0)
            p_web.SetValue('SelectField',clip(loc:formname) & '.jbn:Fault_Description')
          End
    End
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.job:ESN')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab8'''
          If p_web.GSV('Hide:PreviousAddress') <> 1
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          If p_web.GSV('Hide:ButtonMSISDNCheck') <> 1
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab11'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab4'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab6'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab7'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab5'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_NewJobBooking')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab8'');'&CRLF
    if p_web.GSV('Hide:PreviousAddress') <> 1
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
    if p_web.GSV('Hide:ButtonMSISDNCheck') <> 1
      packet = clip(packet) & 'roundCorners(''tab11'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab4'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab6'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab7'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab5'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel8">'&CRLF &|
                                    '  <div id="panel8Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Initial Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel8Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_NewJobBooking_8">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Initial Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab8" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab8">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Initial Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab8">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Initial Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab8" style="display:none;">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Initial Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:ESN_Hidden
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:ESN_Hidden
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:ESN_Hidden
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Transit_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Transit_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Transit_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::hidden
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::hidden
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:ESN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:ESN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:ESN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::Text:Unit
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::Text:Unit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::Text:Unit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
  If p_web.GSV('Hide:PreviousAddress') <> 1
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Bouncer Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_NewJobBooking_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Bouncer Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Bouncer Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Bouncer Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1" style="display:none;">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Bouncer Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locPreviousJobNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locPreviousJobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locPreviousJobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::hidden2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::hidden2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locPreviousOutFault
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locPreviousOutFault
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locPreviousOutFault
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:PreviousAddress
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:PreviousAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:PreviousAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:UsePreviousAddress
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:UsePreviousAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:UsePreviousAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::Button:IMEIHistory
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::Button:IMEIHistory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::Button:IMEIHistory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Unit Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_NewJobBooking_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Unit Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Unit Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Unit Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3" style="display:none;">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Unit Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe:Network
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe:Network
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe:Network
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::hidden3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::hidden3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Mobile_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Mobile_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Mobile_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GSV('Hide:ButtonMSISDNCheck') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonMSISDNCheck
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonMSISDNCheck
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Unit_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Unit_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Unit_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GetSessionValue('Hide:ProductCode') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:ProductCode
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:ProductCode
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:ProductCode
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GetSessionValue('Hide:Colour') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Colour
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Colour
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Colour
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GetSessionValue('Hide:MSN') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:MSN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:MSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:MSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab3  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Job Type') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_NewJobBooking_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Type')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Job Type')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Type')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2" style="display:none;">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Type')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
    If p_web.GSV('Hide:ChangeDOP') = 0
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::_DOPOverrideText
      do Comment::_DOPOverrideText
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:ChangeDOP') = 0
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::_DOPOverrideText2
      do Comment::_DOPOverrideText2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:DOP
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:DOP
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:DOP
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::Button:ChangeDOP
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::Button:ChangeDOP
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:POP
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:POP
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:POP
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::hidden4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::hidden4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:POPConfirmed
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:POPConfirmed
      do Comment::jobe2:POPConfirmed
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locPOPTypePassword
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locPOPTypePassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locPOPTypePassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:POPType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:POPType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:POPType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:WarrantyRefNo
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:WarrantyRefNo
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:WarrantyRefNo
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GSV('OBFValidation') <> 'Passed'
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Chargeable_Job
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Chargeable_Job
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Chargeable_Job
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('OBFValidation') <> 'Passed'
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Charge_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Warranty_Job
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Warranty_Job
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Warranty_Job
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Warranty_Charge_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Warranty_Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Warranty_Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Authority_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Authority_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Authority_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab4  Routine
  If p_web.GSV('Hide:ButtonMSISDNCheck') <> 1
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel11">'&CRLF &|
                                    '  <div id="panel11Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Customer Classification') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel11Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_NewJobBooking_11">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Customer Classification')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab11" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab11">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Customer Classification')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab11">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Customer Classification')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab11" style="display:none;">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Customer Classification')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locMobileLifetimeValue
      do Value::locMobileLifetimeValue
      do Comment::locMobileLifetimeValue
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locMobileAverageSpend
      do Value::locMobileAverageSpend
      do Comment::locMobileAverageSpend
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locMobileLoyaltyStatus
      do Value::locMobileLoyaltyStatus
      do Comment::locMobileLoyaltyStatus
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locMobileUpgradeDate
      do Value::locMobileUpgradeDate
      do Comment::locMobileUpgradeDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locMobileIDNumber
      do Value::locMobileIDNumber
      do Comment::locMobileIDNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab5  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel4">'&CRLF &|
                                    '  <div id="panel4Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Account Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel4Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_NewJobBooking_4">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Account Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab4" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab4">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Account Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Account Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab4" style="display:none;">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Account Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::FranchiseAccount
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::FranchiseAccount
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::FranchiseAccount
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::hidden5
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::hidden5
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Account_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Account_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Account_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Account_Number2
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Account_Number2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Account_Number2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::Text:HubWarning
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::Text:HubWarning
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::Text:HubWarning
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Order_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Order_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Order_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Title
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Title
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Title
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Initial
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Initial
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Initial
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Surname
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Surname
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Surname
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe:EndUserTelNo
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe:EndUserTelNo
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe:EndUserTelNo
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Courier
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Courier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Courier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:CourierWaybillNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:CourierWaybillNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:CourierWaybillNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::amendAddresses
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::amendAddresses
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab6  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel6">'&CRLF &|
                                    '  <div id="panel6Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Accessories') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel6Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_NewJobBooking_6">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Accessories')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab6" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab6">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Accessories')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab6">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Accessories')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab6" style="display:none;">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Accessories')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:AmendAccessories
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:AmendAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:AmendAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::Text:AccessoryMessage
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::Text:AccessoryMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::Text:AccessoryMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ShowAccessory
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ShowAccessory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ShowAccessory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::tmp:TheAccessory
      do Comment::tmp:TheAccessory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::Button:AddAccessories
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::Button:AddAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::Button:AddAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::Button:RemoveAccessory
      do Comment::Button:RemoveAccessory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::TheBlank
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::TheBlank
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::TheBlank
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::Button:RemoveAllAccessories
      do Comment::Button:RemoveAllAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab7  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel7">'&CRLF &|
                                    '  <div id="panel7Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Fault Codes') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel7Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_NewJobBooking_7">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Fault Codes')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab7" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab7">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Fault Codes')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab7">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Fault Codes')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab7" style="display:none;">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Fault Codes')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:AmendFaultCodes
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:AmendFaultCodes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:AmendFaultCodes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::hidden6
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::hidden6
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode_1
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode_1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode_1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode_2
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode_2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode_2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode_3
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode_3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode_3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode_4
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode_4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode_4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode_5
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode_5
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode_5
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode_6
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode_6
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode_6
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab8  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel5">'&CRLF &|
                                    '  <div id="panel5Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Engineering') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel5Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_NewJobBooking_5">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Engineering')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab5" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab5">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Engineering')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab5">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Engineering')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab5" style="display:none;">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Engineering')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Engineer
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Engineer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Engineer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::hidden7
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::hidden7
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jbn:Fault_Description
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jbn:Fault_Description
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jbn:Fault_Description
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jbn:Engineers_Notes
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jbn:Engineers_Notes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jbn:Engineers_Notes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::__Line4
      do Comment::__Line4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ExternalDamageChecklist
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ExternalDamageChecklist
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ExternalDamageChecklist
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:XNone
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:XNone
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:XNone
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:XAntenna
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:XAntenna
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:XAntenna
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:XLens
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:XLens
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:XLens
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:XFCover
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:XFCover
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:XFCover
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:XBCover
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:XBCover
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:XBCover
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:XKeypad
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:XKeypad
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:XKeypad
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:XBattery
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:XBattery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:XBattery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:XCharger
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:XCharger
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:XCharger
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:XLCD
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:XLCD
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:XLCD
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:XSimReader
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:XSimReader
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:XSimReader
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:XSystemConnector
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:XSystemConnector
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:XSystemConnector
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:XNotes
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:XNotes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:XNotes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::__Line5
      do Comment::__Line5
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe:Booking48HourOption
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe:Booking48HourOption
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe:Booking48HourOption
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe:VSACustomer
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe:VSACustomer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe:VSACustomer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:Contract
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:Contract
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:Contract
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'22%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:Prepaid
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:Prepaid
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:Prepaid
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::job:ESN_Hidden  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:ESN_Hidden') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('I.M.E.I. Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:ESN_Hidden  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:ESN_Hidden',p_web.GetValue('NewValue'))
    job:ESN = p_web.GetValue('NewValue') !FieldType= STRING Field = job:ESN
    do Value::job:ESN_Hidden
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:ESN',p_web.dFormat(p_web.GetValue('Value'),'@s20'))
    job:ESN = p_web.GetValue('Value')
  End

Value::job:ESN_Hidden  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:ESN_Hidden') & '_value','adiv')
  loc:extra = ''
  ! --- HIDDEN --- job:ESN
    packet = clip(packet) & p_web.CreateInput('hidden','job:ESN',p_web.GetSessionValue('job:ESN')) & '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::job:ESN_Hidden  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:ESN_Hidden') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Transit_Type  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Transit_Type') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Transit Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Transit_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Transit_Type',p_web.GetValue('NewValue'))
    job:Transit_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Transit_Type
    do Value::job:Transit_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Transit_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Transit_Type = p_web.GetValue('Value')
  End
!  If p_web.GetSessionValue('Comment:IMEINumber') = 'Transit Type Required'
!      p_web.SetSessionValue('Comment:IMEINumber','Required')
!  End ! If p_web.GetSessionValue('Comment:IMEINumber') = 'Transit Type Required'

Value::job:Transit_Type  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Transit_Type') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Transit_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('job:Transit_Type'),) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::job:Transit_Type  Routine
    loc:comment = p_web.Translate(p_web.GetSessionValue('Comment:TransitType'))
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Transit_Type') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::hidden  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('hidden',p_web.GetValue('NewValue'))
    do Value::hidden
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::hidden  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('hidden') & '_value','adiv')
  loc:extra = ''
  ! --- HIDDEN --- 
    packet = clip(packet) & p_web.CreateInput('hidden','',p_web.GetSessionValue('')) & '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::hidden  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('hidden') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:ESN  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:ESN') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('I.M.E.I. Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:ESN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:ESN',p_web.GetValue('NewValue'))
    job:ESN = p_web.GetValue('NewValue') !FieldType= STRING Field = job:ESN
    do Value::job:ESN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:ESN',p_web.dFormat(p_web.GetValue('Value'),'@s20'))
    job:ESN = p_web.GetValue('Value')
  End
  Do Value:IMEINumber

Value::job:ESN  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:ESN') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:ESN
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('job:ESN'),) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::job:ESN  Routine
    loc:comment = p_web.Translate(p_web.GetSessionValue('Comment:IMEINumber'))
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:ESN') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::Text:Unit  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('Text:Unit') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Unit')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::Text:Unit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('Text:Unit',p_web.GetValue('NewValue'))
    do Value::Text:Unit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::Text:Unit  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('Text:Unit') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.Translate(p_web.GetSessionValue('job:Manufacturer') & ' - ' & p_web.GetSessionValue('job:Model_Number'),) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::Text:Unit  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('Text:Unit') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locPreviousJobNumber  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('locPreviousJobNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Job Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locPreviousJobNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locPreviousJobNumber',p_web.GetValue('NewValue'))
    locPreviousJobNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locPreviousJobNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locPreviousJobNumber',p_web.GetValue('Value'))
    locPreviousJobNumber = p_web.GetValue('Value')
  End

Value::locPreviousJobNumber  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('locPreviousJobNumber') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locPreviousJobNumber
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locPreviousJobNumber'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locPreviousJobNumber  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('locPreviousJobNumber') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::hidden2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('hidden2',p_web.GetValue('NewValue'))
    do Value::hidden2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::hidden2  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('hidden2') & '_value','adiv')
  loc:extra = ''
  ! --- HIDDEN --- 
    packet = clip(packet) & p_web.CreateInput('hidden','',p_web.GetSessionValue('')) & '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::hidden2  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('hidden2') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locPreviousOutFault  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('locPreviousOutFault') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Out Fault')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locPreviousOutFault  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locPreviousOutFault',p_web.GetValue('NewValue'))
    locPreviousOutFault = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locPreviousOutFault
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locPreviousOutFault',p_web.GetValue('Value'))
    locPreviousOutFault = p_web.GetValue('Value')
  End

Value::locPreviousOutFault  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('locPreviousOutFault') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locPreviousOutFault
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locPreviousOutFault'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locPreviousOutFault  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('locPreviousOutFault') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:PreviousAddress  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:PreviousAddress') & '_prompt',Choose(p_web.GetSessionValue('IMEIHistory') <> 1 Or p_web.GetSessionValue('Hide:PreviousAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Previous Address')
  If p_web.GetSessionValue('IMEIHistory') <> 1 Or p_web.GetSessionValue('Hide:PreviousAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:PreviousAddress  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:PreviousAddress',p_web.GetValue('NewValue'))
    tmp:PreviousAddress = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:PreviousAddress
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:PreviousAddress',p_web.GetValue('Value'))
    tmp:PreviousAddress = p_web.GetValue('Value')
  End
  do Value::tmp:PreviousAddress
  do SendAlert

Value::tmp:PreviousAddress  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:PreviousAddress') & '_value',Choose(p_web.GetSessionValue('IMEIHistory') <> 1 Or p_web.GetSessionValue('Hide:PreviousAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('IMEIHistory') <> 1 Or p_web.GetSessionValue('Hide:PreviousAddress') = 1)
  ! --- TEXT --- tmp:PreviousAddress
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tmp:PreviousAddress')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:PreviousAddress'',''newjobbooking_tmp:previousaddress_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'readonly'
  packet = clip(packet) & p_web.CreateTextArea('tmp:PreviousAddress',p_web.GetSessionValue('tmp:PreviousAddress'),6,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(tmp:PreviousAddress),,Net:Web:Control) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('tmp:PreviousAddress') & '_value')

Comment::tmp:PreviousAddress  Routine
      loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:PreviousAddress') & '_comment',Choose(p_web.GetSessionValue('IMEIHistory') <> 1 Or p_web.GetSessionValue('Hide:PreviousAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('IMEIHistory') <> 1 Or p_web.GetSessionValue('Hide:PreviousAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:UsePreviousAddress  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:UsePreviousAddress') & '_prompt',Choose(p_web.GetSessionValue('IMEIHistory') <> 1 Or p_web.GetSessionValue('Hide:PreviousAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Use Previous Address')
  If p_web.GetSessionValue('IMEIHistory') <> 1 Or p_web.GetSessionValue('Hide:PreviousAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:UsePreviousAddress  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:UsePreviousAddress',p_web.GetValue('NewValue'))
    tmp:UsePreviousAddress = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:UsePreviousAddress
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:UsePreviousAddress',p_web.GetValue('Value'))
    tmp:UsePreviousAddress = p_web.GetValue('Value')
  End
        ! Use previous address details (DBH: 19/02/2008)
        If p_web.GetSessionValue('IMEIHistory') = 1 And |
            p_web.GetSessionValue('Hide:PreviousAddress') = 0 And |
            p_web.GetSessionValue('tmp:UsePreviousAddress') = 1
  
            Access:JOBS_ALIAS.Clearkey(job_ali:ESN_Key)
            job_ali:ESN = p_web.GetSessionValue('job:ESN')
            Set(job_ali:ESN_Key,job_ali:ESN_Key)
            Loop ! Begin Loop
                If Access:JOBS_ALIAS.Next()
                    Break
                End ! If Access:JOBS_ALIAS.Next()
                If job_ali:ESN <> p_web.GetSessionValue('job:ESN')
                    Break
                End ! If job_ali:ESN <> tmp:IMEINumber
                If job_ali:Cancelled = 'YES'
                    Cycle
                End ! If job_ali:Cancelle = 'YES'
                p_web.SetSessionValue('job:Company_Name',job_ali:Company_Name)
                p_web.SetSessionValue('job:Address_Line1',job_ali:Address_Line1)
                p_web.SetSessionValue('job:Address_Line2',job_ali:Address_Line2)
                p_web.SetSessionValue('job:Address_Line3',job_ali:Address_Line3)
                p_web.SetSessionValue('job:Postcode',job_ali:Postcode)
                p_web.SetSessionValue('job:Telephone_Number',job_ali:Telephone_Number)
                p_web.SetSessionValue('job:Fax_Number',job_ali:Fax_Number)
                p_web.SetSessionValue('job:Mobile_Number',job_ali:Mobile_Number)
  
                Access:JOBSE_ALIAS.Clearkey(jobe_ali:RefNumberKey)
                jobe_ali:RefNumber = job_ali:Ref_Number
                If Access:JOBSE_ALIAS.TryFetch(jobe_ali:RefNumberKey) = Level:Benign
                    p_web.SetSessionValue('jobe:EndUserEmailAddress',jobe:EndUserEmailAddress)
                End ! If Access:JOBS_ALIASE.TryFetch(jobe:RefNumberKey) = Level:Benign
            End ! Loop
        End ! If p_web.GetSessionValue('IMEIHistory') = 1 And |
  do Value::tmp:UsePreviousAddress
  do SendAlert
  do Value::job:Mobile_Number  !1

Value::tmp:UsePreviousAddress  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:UsePreviousAddress') & '_value',Choose(p_web.GetSessionValue('IMEIHistory') <> 1 Or p_web.GetSessionValue('Hide:PreviousAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('IMEIHistory') <> 1 Or p_web.GetSessionValue('Hide:PreviousAddress') = 1)
  ! --- RADIO --- tmp:UsePreviousAddress
  loc:fieldclass = Choose(sub(' noBorder',1,1) = ' ',clip('FormEntry') & ' noBorder',' noBorder')
  If lower(loc:invalid) = lower('tmp:UsePreviousAddress')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:UsePreviousAddress') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:UsePreviousAddress'',''newjobbooking_tmp:usepreviousaddress_value'',1,'''&clip(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:UsePreviousAddress')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:UsePreviousAddress',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:UsePreviousAddress_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Yes') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:UsePreviousAddress') = 0
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:UsePreviousAddress'',''newjobbooking_tmp:usepreviousaddress_value'',1,'''&clip(0)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:UsePreviousAddress')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:UsePreviousAddress',clip(0),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:UsePreviousAddress_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('No') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('tmp:UsePreviousAddress') & '_value')

Comment::tmp:UsePreviousAddress  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:UsePreviousAddress') & '_comment',Choose(p_web.GetSessionValue('IMEIHistory') <> 1 Or p_web.GetSessionValue('Hide:PreviousAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('IMEIHistory') <> 1 Or p_web.GetSessionValue('Hide:PreviousAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::Button:IMEIHistory  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('Button:IMEIHistory') & '_prompt',Choose(p_web.GSV('IMEIHistory') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('IMEIHistory') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::Button:IMEIHistory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('Button:IMEIHistory',p_web.GetValue('NewValue'))
    do Value::Button:IMEIHistory
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::Button:IMEIHistory  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('Button:IMEIHistory') & '_value',Choose(p_web.GSV('IMEIHistory') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('IMEIHistory') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','IMEIHistory','Previous Unit History','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('BrowseIMEIHistory?' &'fromURL=NewJobBooking&currentJob=0')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::Button:IMEIHistory  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:Bouncer'))
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('Button:IMEIHistory') & '_comment',Choose(p_web.GSV('IMEIHistory') <> 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('IMEIHistory') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe:Network  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe:Network') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Network')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe:Network  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe:Network',p_web.GetValue('NewValue'))
    jobe:Network = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe:Network
    do Value::jobe:Network
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe:Network',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jobe:Network = p_web.GetValue('Value')
  End
  If jobe:Network = '' and p_web.GetSessionValue('Required:Network') = 1
    loc:Invalid = 'jobe:Network'
    loc:alert = p_web.translate('Network') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    jobe:Network = Upper(jobe:Network)
    p_web.SetSessionValue('jobe:Network',jobe:Network)
      Access:NETWORKS.Clearkey(net:NetworkKey)
      net:Network    = p_web.GSV('jobe:Network')
      if (Access:NETWORKS.TryFetch(net:NetworkKey) = Level:Benign)
          ! Found
      else ! if (Access:NETWORKS.TryFetch(net:NetworkKey) = Level:Benign)
          ! Error
          p_web.SSV('jobe:Network','')
      end ! if (Access:NETWORKS.TryFetch(net:NetworkKey) = Level:Benign)
  p_Web.SetValue('lookupfield','jobe:Network')
  do AfterLookup
  do Value::jobe:Network
  do SendAlert
  do Comment::jobe:Network

Value::jobe:Network  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe:Network') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- jobe:Network
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If (p_web.GetSessionValue('Required:Network') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('jobe:Network')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If jobe:Network = '' and (p_web.GetSessionValue('Required:Network') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe:Network'',''newjobbooking_jobe:network_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe:Network')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','jobe:Network',p_web.GetSessionValue('jobe:Network'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),'Network') & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectNetworks')&'?LookupField=jobe:Network&Tab=8&ForeignField=net:Network&_sort=net:Network&Refresh=sort&LookupFrom=NewJobBooking&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe:Network') & '_value')

Comment::jobe:Network  Routine
    loc:comment = p_web.Translate(p_web.GetSessionValue('Comment:Network'))
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe:Network') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe:Network') & '_comment')

Validate::hidden3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('hidden3',p_web.GetValue('NewValue'))
    do Value::hidden3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::hidden3  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('hidden3') & '_value','adiv')
  loc:extra = ''
  ! --- HIDDEN --- 
    packet = clip(packet) & p_web.CreateInput('hidden','',p_web.GetSessionValue('')) & '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::hidden3  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('hidden3') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Mobile_Number  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Mobile_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Mobile Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Mobile_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Mobile_Number',p_web.GetValue('NewValue'))
    job:Mobile_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Mobile_Number
    do Value::job:Mobile_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Mobile_Number',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    job:Mobile_Number = p_web.GetValue('Value')
  End
  If job:Mobile_Number = '' and p_web.GetSessionValue('Required:MobileNumber') = 1
    loc:Invalid = 'job:Mobile_Number'
    loc:alert = p_web.translate('Mobile Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    job:Mobile_Number = Upper(job:Mobile_Number)
    p_web.SetSessionValue('job:Mobile_Number',job:Mobile_Number)
  Do Value:MobileNumber
  
  do Value::job:Mobile_Number
  do SendAlert
  do Comment::job:Mobile_Number

Value::job:Mobile_Number  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Mobile_Number') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Mobile_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If (p_web.GetSessionValue('Required:MobileNumber') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('job:Mobile_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If job:Mobile_Number = '' and (p_web.GetSessionValue('Required:MobileNumber') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Mobile_Number'',''newjobbooking_job:mobile_number_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Mobile_Number')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Mobile_Number',p_web.GetSessionValueFormat('job:Mobile_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  p_web.SetValue('SelectField',clip(loc:formname) & '.jobe:Network')
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Mobile_Number') & '_value')

Comment::job:Mobile_Number  Routine
    loc:comment = p_web.Translate(p_web.GetSessionValue('Comment:MobileNumber'))
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Mobile_Number') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Mobile_Number') & '_comment')

Validate::buttonMSISDNCheck  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonMSISDNCheck',p_web.GetValue('NewValue'))
    do Value::buttonMSISDNCheck
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do msisdnRoutine
  do Value::buttonMSISDNCheck
  do SendAlert
  do Value::locMobileAverageSpend  !1
  do Value::locMobileIDNumber  !1
  do Value::locMobileLifetimeValue  !1
  do Value::locMobileLoyaltyStatus  !1
  do Value::locMobileUpgradeDate  !1

Value::buttonMSISDNCheck  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('buttonMSISDNCheck') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonMSISDNCheck'',''newjobbooking_buttonmsisdncheck_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ValidateMobileNumber','MSISDN Check','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('buttonMSISDNCheck') & '_value')

Comment::buttonMSISDNCheck  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('buttonMSISDNCheck') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Unit_Type  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Unit_Type') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Unit Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Unit_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Unit_Type',p_web.GetValue('NewValue'))
    job:Unit_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Unit_Type
    do Value::job:Unit_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Unit_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Unit_Type = p_web.GetValue('Value')
  End
  If job:Unit_Type = '' and p_web.GetSessionValue('Required:UnitType') = 1
    loc:Invalid = 'job:Unit_Type'
    loc:alert = p_web.translate('Unit Type') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    job:Unit_Type = Upper(job:Unit_Type)
    p_web.SetSessionValue('job:Unit_Type',job:Unit_Type)
  Access:UNITTYPE.Clearkey(uni:Unit_Type_Key)
  uni:Unit_Type    = p_web.GSV('job:Unit_Type')
  if (Access:UNITTYPE.TryFetch(uni:Unit_Type_Key) = Level:Benign)
      ! Found
  else ! if (Access:UNITTYPE.TryFetch(uni:Unit_Type_Key) = Level:Benign)
      ! Error
      p_web.SSV('job:Unit_Type','')
  end ! if (Access:UNITTYPE.TryFetch(uni:Unit_Type_Key) = Level:Benign)
  p_Web.SetValue('lookupfield','job:Unit_Type')
  do AfterLookup
  do Value::job:Unit_Type
  do SendAlert
  do Comment::job:Unit_Type

Value::job:Unit_Type  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Unit_Type') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Unit_Type
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If (p_web.GetSessionValue('Required:UnitType') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('job:Unit_Type')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If job:Unit_Type = '' and (p_web.GetSessionValue('Required:UnitType') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Unit_Type'',''newjobbooking_job:unit_type_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Unit_Type')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','job:Unit_Type',p_web.GetSessionValue('job:Unit_Type'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectUnitTypes')&'?LookupField=job:Unit_Type&Tab=8&ForeignField=uni:Unit_Type&_sort=&Refresh=sort&LookupFrom=NewJobBooking&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Unit_Type') & '_value')

Comment::job:Unit_Type  Routine
    loc:comment = p_web.Translate(p_web.GetSessionValue('Comment:UnitType'))
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Unit_Type') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Unit_Type') & '_comment')

Prompt::job:ProductCode  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:ProductCode') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Product Code')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:ProductCode  Routine
    If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('Value')
      p_web.GetDescription(MODPROD,,,mop:ProductCode,mop:ProductCode,p_web.GetValue('Value'))
      loc:lookupdone = 1
    Else
      p_web.GetDescription(MODPROD,,,mop:ProductCode,mop:ProductCode,p_web.GetSessionValue('job:ProductCode'))
    End
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:ProductCode',p_web.GetValue('NewValue'))
    job:ProductCode = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::job:ProductCode
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:ProductCode',p_web.GetValue('Value'))
    job:ProductCode = p_web.GetValue('Value')
  End
  If job:ProductCode = '' and p_web.GetSessionValue('Required:ProductCode') = 1
    loc:Invalid = 'job:ProductCode'
    loc:alert = p_web.translate('Product Code') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    job:ProductCode = Upper(job:ProductCode)
    p_web.SetSessionValue('job:ProductCode',job:ProductCode)
  Access:MODPROD.Clearkey(mop:ProductCodeKey)
  mop:ModelNumber    = p_web.GSV('job:Model_Number')
  mop:ProductCode    = p_web.GSV('job:ProductCode')
  if (Access:MODPROD.TryFetch(mop:ProductCodeKey) = Level:Benign)
      ! Found
  else ! if (Access:MODPROD.TryFetch(mop:ProductCodeKey) = Level:Benign)
      ! Error
      p_web.SSV('job:ProductCode','')
  end ! if (Access:MODPROD.TryFetch(mop:ProductCodeKey) = Level:Benign)
  p_Web.SetValue('lookupfield','job:ProductCode')
  do AfterLookup
  do Value::job:ProductCode
  do SendAlert
  do Comment::job:ProductCode

Value::job:ProductCode  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:ProductCode') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:ProductCode
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If (p_web.GetSessionValue('Required:ProductCode') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('job:ProductCode')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If job:ProductCode = '' and (p_web.GetSessionValue('Required:ProductCode') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:ProductCode'',''newjobbooking_job:productcode_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:ProductCode')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','job:ProductCode',p_web.GetSessionValue('job:ProductCode'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('LookupProductCodes')&'?LookupField=job:ProductCode&Tab=8&ForeignField=mop:ProductCode&_sort=mop:ProductCode&Refresh=sort&LookupFrom=NewJobBooking&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:ProductCode') & '_value')

Comment::job:ProductCode  Routine
    loc:comment = p_web.Translate(p_web.GetSessionValue('Comment:ProductCode'))
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:ProductCode') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:ProductCode') & '_comment')

Prompt::job:Colour  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Colour') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Colour')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Colour  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Colour',p_web.GetValue('NewValue'))
    job:Colour = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Colour
    do Value::job:Colour
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Colour',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Colour = p_web.GetValue('Value')
  End
  If job:Colour = '' and p_web.GetSessionValue('Required:Colour') = 1
    loc:Invalid = 'job:Colour'
    loc:alert = p_web.translate('Colour') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    job:Colour = Upper(job:Colour)
    p_web.SetSessionValue('job:Colour',job:Colour)
  Access:MODELCOL.Clearkey(moc:Colour_Key)
  moc:Model_Number    = p_web.GSV('job:Model_Number')
  moc:Colour    = p_web.GSV('job:Colour')
  if (Access:MODELCOL.TryFetch(moc:Colour_Key) = Level:Benign)
      ! Found
  else ! if (Access:MODELCOL.TryFetch(moc:Colour_Key) = Level:Benign)
      ! Error
      p_web.SSV('job:Colour','')
  end ! if (Access:MODELCOL.TryFetch(moc:Colour_Key) = Level:Benign)
  p_Web.SetValue('lookupfield','job:Colour')
  do AfterLookup
  do Value::job:Colour
  do SendAlert
  do Comment::job:Colour

Value::job:Colour  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Colour') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Colour
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If (p_web.GetSessionValue('Required:Colour') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('job:Colour')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If job:Colour = '' and (p_web.GetSessionValue('Required:Colour') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Colour'',''newjobbooking_job:colour_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Colour')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','job:Colour',p_web.GetSessionValue('job:Colour'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectColours')&'?LookupField=job:Colour&Tab=8&ForeignField=moc:Colour&_sort=&Refresh=sort&LookupFrom=NewJobBooking&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Colour') & '_value')

Comment::job:Colour  Routine
    loc:comment = p_web.Translate(p_web.GetSessionValue('Comment:Colour'))
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Colour') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Colour') & '_comment')

Prompt::job:MSN  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:MSN') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('M.S.N.')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:MSN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:MSN',p_web.GetValue('NewValue'))
    job:MSN = p_web.GetValue('NewValue') !FieldType= STRING Field = job:MSN
    do Value::job:MSN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:MSN',p_web.dFormat(p_web.GetValue('Value'),'@s20'))
    job:MSN = p_web.GetValue('Value')
  End
  If job:MSN = '' and p_web.GetSessionValue('Required:MSN') = 1
    loc:Invalid = 'job:MSN'
    loc:alert = p_web.translate('M.S.N.') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    job:MSN = Upper(job:MSN)
    p_web.SetSessionValue('job:MSN',job:MSN)
  Do Value:MSN
  do Value::job:MSN
  do SendAlert
  do Comment::job:MSN

Value::job:MSN  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:MSN') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:MSN
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If (p_web.GetSessionValue('Required:MSN') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('job:MSN')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If job:MSN = '' and (p_web.GetSessionValue('Required:MSN') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:MSN'',''newjobbooking_job:msn_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:MSN')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:MSN',p_web.GetSessionValueFormat('job:MSN'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s20'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:MSN') & '_value')

Comment::job:MSN  Routine
    loc:comment = p_web.Translate(p_web.GetSessionValue('Comment:MSN'))
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:MSN') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:MSN') & '_comment')

Validate::_DOPOverrideText  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('_DOPOverrideText',p_web.GetValue('NewValue'))
    do Value::_DOPOverrideText
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::_DOPOverrideText  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('_DOPOverrideText') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate('Unit booked in previously.',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::_DOPOverrideText  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('_DOPOverrideText') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::_DOPOverrideText2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('_DOPOverrideText2',p_web.GetValue('NewValue'))
    do Value::_DOPOverrideText2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::_DOPOverrideText2  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('_DOPOverrideText2') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate('The Date Of Purchase will be filled identically to the previous job. ',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::_DOPOverrideText2  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('_DOPOverrideText2') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:DOP  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:DOP') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Activation / DOP')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:DOP') & '_prompt')

Validate::job:DOP  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:DOP',p_web.GetValue('NewValue'))
    job:DOP = p_web.GetValue('NewValue') !FieldType= DATE Field = job:DOP
    do Value::job:DOP
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:DOP',p_web.dFormat(p_web.GetValue('Value'),'@d06b'))
    job:DOP = p_web.Dformat(p_web.GetValue('Value'),'@d06b') !
  End
  If job:DOP = '' and p_web.GetSessionValue('Required:DOP') = 1
    loc:Invalid = 'job:DOP'
    loc:alert = 'Date Of Purchase Required'
  End
    Do Value:DOP
  do Value::job:DOP
  do SendAlert
  do Comment::job:DOP
  do Value::job:POP  !1
  do Comment::job:POP
  do Value::job:Chargeable_Job  !1
  do Comment::job:Chargeable_Job
  do Prompt::job:Charge_Type
  do Value::job:Charge_Type  !1
  do Comment::job:Charge_Type
  do Value::job:Warranty_Job  !1
  do Comment::job:Warranty_Job
  do Prompt::job:Warranty_Charge_Type
  do Value::job:Warranty_Charge_Type  !1
  do Comment::job:Warranty_Charge_Type
  do Prompt::tmp:AmendFaultCodes
  do Value::tmp:AmendFaultCodes  !1
  do Comment::tmp:AmendFaultCodes
  do Prompt::tmp:FaultCode_6
  do Value::tmp:FaultCode_6  !1
  do Comment::tmp:FaultCode_6
  do Prompt::tmp:FaultCode_5
  do Value::tmp:FaultCode_5  !1
  do Comment::tmp:FaultCode_5
  do Prompt::tmp:FaultCode_4
  do Value::tmp:FaultCode_4  !1
  do Comment::tmp:FaultCode_4
  do Prompt::tmp:FaultCode_3
  do Value::tmp:FaultCode_3  !1
  do Comment::tmp:FaultCode_3
  do Prompt::tmp:FaultCode_2
  do Value::tmp:FaultCode_2  !1
  do Comment::tmp:FaultCode_2
  do Prompt::tmp:FaultCode_1
  do Value::tmp:FaultCode_1  !1
  do Comment::tmp:FaultCode_1

Value::job:DOP  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:DOP') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:DOP
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('Hide:ChangeDOP') = 0,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('Hide:ChangeDOP') = 0
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GetSessionValue('Required:DOP') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('job:DOP')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If job:DOP = '' and (p_web.GetSessionValue('Required:DOP') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:DOP'',''newjobbooking_job:dop_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:DOP')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:DOP',p_web.GetSessionValue('job:DOP'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06b',loc:javascript,,) & '<13,10>'
  do SendPacket
  !Handcode Date Lookup Button
  if (p_web.GSV('Hide:ChangeDOP') = 1)
      packet = clip(packet) & '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar(job__DOP,''dd/mm/yyyy'',this); Date.disabled=true;sv(''...'',''newjobbooking_pickdate_value'',1,FieldValue(this,1));nextFocus(NewJobBooking_frm,'''',0);" value="Select Date Of Purchase" name="Date" type="button">...</button>'
      do SendPacket
  end ! if (p_web.GSV('Hide:ChangeDOP') = 1)
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:DOP') & '_value')

Comment::job:DOP  Routine
    loc:comment = p_web.Translate(p_web.GetSessionValue('Comment:DOP'))
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:DOP') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:DOP') & '_comment')

Validate::Button:ChangeDOP  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('Button:ChangeDOP',p_web.GetValue('NewValue'))
    do Value::Button:ChangeDOP
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::Button:ChangeDOP  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('Button:ChangeDOP') & '_value',Choose(p_web.GSV('Hide:ChangeDOP') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ChangeDOP') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ChangeDOP','Override DOP','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormChangeDOP?frompage=' & p_web.PageName)) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::Button:ChangeDOP  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('Button:ChangeDOP') & '_comment',Choose(p_web.GSV('Hide:ChangeDOP') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:ChangeDOP') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:POP  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:POP') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Job Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:POP  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:POP',p_web.GetValue('NewValue'))
    job:POP = p_web.GetValue('NewValue') !FieldType= STRING Field = job:POP
    do Value::job:POP
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:POP',p_web.dFormat(p_web.GetValue('Value'),'@s3'))
    job:POP = p_web.GetValue('Value')
  End
  Do Value:POP
  Do CalculateFaultCodes
  do Value::job:POP
  do SendAlert
  do Prompt::job:DOP
  do Value::job:DOP  !1
  do Comment::job:DOP
  do Prompt::job:Charge_Type
  do Value::job:Charge_Type  !1
  do Comment::job:Charge_Type
  do Prompt::job:Chargeable_Job
  do Value::job:Chargeable_Job  !1
  do Comment::job:Chargeable_Job
  do Prompt::job:Warranty_Charge_Type
  do Value::job:Warranty_Charge_Type  !1
  do Comment::job:Warranty_Charge_Type
  do Prompt::job:Warranty_Job
  do Value::job:Warranty_Job  !1
  do Comment::job:Warranty_Job
  do Comment::job:POP
  do Prompt::tmp:FaultCode_1
  do Value::tmp:FaultCode_1  !1
  do Comment::tmp:FaultCode_1
  do Prompt::tmp:FaultCode_2
  do Value::tmp:FaultCode_2  !1
  do Comment::tmp:FaultCode_2
  do Prompt::tmp:FaultCode_3
  do Value::tmp:FaultCode_3  !1
  do Comment::tmp:FaultCode_3
  do Prompt::tmp:FaultCode_4
  do Value::tmp:FaultCode_4  !1
  do Comment::tmp:FaultCode_4
  do Prompt::tmp:FaultCode_5
  do Value::tmp:FaultCode_5  !1
  do Comment::tmp:FaultCode_5
  do Prompt::tmp:FaultCode_6
  do Value::tmp:FaultCode_6  !1
  do Comment::tmp:FaultCode_6
  do Prompt::tmp:AmendFaultCodes
  do Value::tmp:AmendFaultCodes  !1
  do Comment::tmp:AmendFaultCodes

Value::job:POP  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:POP') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('job:POP')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:POP'',''newjobbooking_job:pop_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:POP')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('OBFValidation') = 'Passed' Or p_web.GSV('Hide:LiquidDamage') <> 1,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('job:POP',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  loc:rowstyle = choose(sub('GreyRegular',1,1)=' ',clip(loc:rowstyle) & 'GreyRegular','GreyRegular')
  if p_web.IfExistsSessionValue('job:POP') = 0
    p_web.SetSessionValue('job:POP',p_web.GetSessionValue('Drop:JobTypeDefault'))
  end
    packet = clip(packet) & p_web.CreateOption(p_web.GetSessionValue('Drop:JobType'),p_web.GetSessionValue('Drop:JobTypeDefault'),choose(p_web.GetSessionValue('Drop:JobTypeDefault') = p_web.getsessionvalue('job:POP')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('First Year Warranty','F',choose('F' = p_web.getsessionvalue('job:POP')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('Second Year Warranty','S',choose('S' = p_web.getsessionvalue('job:POP')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('Chargeable','C',choose('C' = p_web.getsessionvalue('job:POP')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:POP') & '_value')

Comment::job:POP  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:POP'))
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:POP') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:POP') & '_comment')

Validate::hidden4  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('hidden4',p_web.GetValue('NewValue'))
    do Value::hidden4
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::hidden4  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('hidden4') & '_value','adiv')
  loc:extra = ''
  ! --- HIDDEN --- 
    packet = clip(packet) & p_web.CreateInput('hidden','',p_web.GetSessionValue('')) & '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::hidden4  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('hidden4') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe2:POPConfirmed  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:POPConfirmed') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('POP Confirmed')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe2:POPConfirmed  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:POPConfirmed',p_web.GetValue('NewValue'))
    jobe2:POPConfirmed = p_web.GetValue('NewValue') !FieldType= BYTE Field = jobe2:POPConfirmed
    do Value::jobe2:POPConfirmed
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('jobe2:POPConfirmed',p_web.GetValue('Value'))
    jobe2:POPConfirmed = p_web.GetValue('Value')
  End
  ! Automatic Dictionary Validation
    If jobe2:POPConfirmed <> 1 and jobe2:POPConfirmed <> 0
      loc:Invalid = 'jobe2:POPConfirmed'
      loc:Alert = clip('POP Confirmed') & ' ' & clip(p_web.site.OneOfText) & ' ' & 1 & ' / ' & 0
    End
  IF (p_web.GSV('jobe2:POPConfirmed') = 1)
      p_web.SSV('tmp:POPType','POP')
  END
  do Value::jobe2:POPConfirmed
  do SendAlert
  do Value::tmp:POPType  !1

Value::jobe2:POPConfirmed  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:POPConfirmed') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- jobe2:POPConfirmed
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe2:POPConfirmed'',''newjobbooking_jobe2:popconfirmed_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:POPConfirmed')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('jobe2:POPConfirmed') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','jobe2:POPConfirmed',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:POPConfirmed') & '_value')

Comment::jobe2:POPConfirmed  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:POPConfirmed') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locPOPTypePassword  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('locPOPTypePassword') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Password')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locPOPTypePassword  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locPOPTypePassword',p_web.GetValue('NewValue'))
    locPOPTypePassword = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locPOPTypePassword
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locPOPTypePassword',p_web.GetValue('Value'))
    locPOPTypePassword = p_web.GetValue('Value')
  End
    locPOPTypePassword = Upper(locPOPTypePassword)
    p_web.SetSessionValue('locPOPTypePassword',locPOPTypePassword)
  p_web.SSV('ReadOnly:POPType',1)
  p_web.SSV('Comment:POPTypePassword',kCommentPOPTypePassword)
  Access:USERS.ClearKey(use:password_key)
  use:Password = p_web.GSV('locPOPTypePassword')
  IF (Access:USERS.TryFetch(use:password_key) = Level:Benign)
      Access:ACCAREAS.ClearKey(acc:Access_level_key)
      acc:User_Level = use:User_Level
      acc:Access_Area = 'AMEND POP TYPE'
      IF (Access:ACCAREAS.TryFetch(acc:Access_level_key) = Level:Benign)
          p_web.SSV('ReadOnly:POPType',0)
          
      ELSE
          p_web.SSV('Comment:POPTypePassword','User Does Not Have Access')
      END
  ELSE
      p_web.SSV('Comment:POPTypePassword','Invalid Password')
  END
  do Value::locPOPTypePassword
  do SendAlert
  do Value::tmp:POPType  !1
  do Comment::locPOPTypePassword

Value::locPOPTypePassword  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('locPOPTypePassword') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locPOPTypePassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('locPOPTypePassword')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(11) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locPOPTypePassword'',''newjobbooking_locpoptypepassword_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locPOPTypePassword')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','locPOPTypePassword',p_web.GetSessionValueFormat('locPOPTypePassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,'Enter Password And Press [TAB]') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('locPOPTypePassword') & '_value')

Comment::locPOPTypePassword  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:POPTypePassword'))
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('locPOPTypePassword') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('locPOPTypePassword') & '_comment')

Prompt::tmp:POPType  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:POPType') & '_prompt',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('POP Type')
  If p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:POPType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:POPType',p_web.GetValue('NewValue'))
    tmp:POPType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:POPType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:POPType',p_web.GetValue('Value'))
    tmp:POPType = p_web.GetValue('Value')
  End
  do SendAlert

Value::tmp:POPType  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:POPType') & '_value',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0)
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:POPType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:POPType'',''newjobbooking_tmp:poptype_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('ReadOnly:POPType') = 1,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:POPType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  loc:rowstyle = choose(sub('GreyRegular',1,1)=' ',clip(loc:rowstyle) & 'GreyRegular','GreyRegular')
  if p_web.IfExistsSessionValue('tmp:POPType') = 0
    p_web.SetSessionValue('tmp:POPType','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('tmp:POPType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('POP','POP',choose('POP' = p_web.getsessionvalue('tmp:POPType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('BASTION','BASTION',choose('BASTION' = p_web.getsessionvalue('tmp:POPType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('NONE','NONE',choose('NONE' = p_web.getsessionvalue('tmp:POPType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('tmp:POPType') & '_value')

Comment::tmp:POPType  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:POPType') & '_comment',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe2:WarrantyRefNo  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:WarrantyRefNo') & '_prompt',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Warranty Ref No')
  If p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe2:WarrantyRefNo  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:WarrantyRefNo',p_web.GetValue('NewValue'))
    jobe2:WarrantyRefNo = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe2:WarrantyRefNo
    do Value::jobe2:WarrantyRefNo
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe2:WarrantyRefNo',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jobe2:WarrantyRefNo = p_web.GetValue('Value')
  End
    jobe2:WarrantyRefNo = Upper(jobe2:WarrantyRefNo)
    p_web.SetSessionValue('jobe2:WarrantyRefNo',jobe2:WarrantyRefNo)
  do Value::jobe2:WarrantyRefNo
  do SendAlert

Value::jobe2:WarrantyRefNo  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:WarrantyRefNo') & '_value',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0)
  ! --- STRING --- jobe2:WarrantyRefNo
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jobe2:WarrantyRefNo')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:WarrantyRefNo'',''newjobbooking_jobe2:warrantyrefno_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:WarrantyRefNo',p_web.GetSessionValueFormat('jobe2:WarrantyRefNo'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Warranty Ref No') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:WarrantyRefNo') & '_value')

Comment::jobe2:WarrantyRefNo  Routine
      loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:WarrantyRefNo') & '_comment',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Chargeable_Job  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Chargeable_Job') & '_prompt',Choose(p_web.GetSessionValue('Hide:ChargeableJob') = 1 ,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Chargeable Job')
  If p_web.GetSessionValue('Hide:ChargeableJob') = 1 
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Chargeable_Job') & '_prompt')

Validate::job:Chargeable_Job  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Chargeable_Job',p_web.GetValue('NewValue'))
    job:Chargeable_Job = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Chargeable_Job
    do Value::job:Chargeable_Job
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Chargeable_Job',p_web.dFormat(p_web.GetValue('Value'),'@s3'))
    job:Chargeable_Job = p_web.GetValue('Value')
  End
  Do CalculateFaultCodes
  do Value::job:Chargeable_Job
  do SendAlert
  do Prompt::job:Charge_Type
  do Value::job:Charge_Type  !1
  do Comment::job:Charge_Type
  do Prompt::job:Authority_Number
  do Value::job:Authority_Number  !1
  do Comment::job:Authority_Number
  do Comment::job:Chargeable_Job
  do Prompt::tmp:FaultCode_1
  do Value::tmp:FaultCode_1  !1
  do Comment::tmp:FaultCode_1
  do Prompt::tmp:FaultCode_2
  do Value::tmp:FaultCode_2  !1
  do Comment::tmp:FaultCode_2
  do Prompt::tmp:FaultCode_3
  do Value::tmp:FaultCode_3  !1
  do Comment::tmp:FaultCode_3
  do Prompt::tmp:FaultCode_4
  do Value::tmp:FaultCode_4  !1
  do Comment::tmp:FaultCode_4
  do Prompt::tmp:FaultCode_5
  do Value::tmp:FaultCode_5  !1
  do Comment::tmp:FaultCode_5
  do Prompt::tmp:FaultCode_6
  do Value::tmp:FaultCode_6  !1
  do Comment::tmp:FaultCode_6
  do Prompt::tmp:AmendFaultCodes
  do Value::tmp:AmendFaultCodes  !1
  do Comment::tmp:AmendFaultCodes

Value::job:Chargeable_Job  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Chargeable_Job') & '_value',Choose(p_web.GetSessionValue('Hide:ChargeableJob') = 1 ,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:ChargeableJob') = 1 )
  ! --- RADIO --- job:Chargeable_Job
  loc:fieldclass = Choose(sub(' noBorder',1,1) = ' ',clip('FormEntry') & ' noBorder',' noBorder')
  If lower(loc:invalid) = lower('job:Chargeable_Job')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:ChargeableJob') = 1,'disabled','')
    if p_web.GetSessionValue('job:Chargeable_Job') = 'YES'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''job:Chargeable_Job'',''newjobbooking_job:chargeable_job_value'',1,'''&clip('YES')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Chargeable_Job')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','job:Chargeable_Job',clip('YES'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,'Chargeable Job','job:Chargeable_Job_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Yes') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:ChargeableJob') = 1,'disabled','')
    if p_web.GetSessionValue('job:Chargeable_Job') = 'NO'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''job:Chargeable_Job'',''newjobbooking_job:chargeable_job_value'',1,'''&clip('NO')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Chargeable_Job')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','job:Chargeable_Job',clip('NO'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,'Chargeable Job','job:Chargeable_Job_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('No') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Chargeable_Job') & '_value')

Comment::job:Chargeable_Job  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Chargeable_Job') & '_comment',Choose(p_web.GetSessionValue('Hide:ChargeableJob') = 1 ,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('Hide:ChargeableJob') = 1 
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Chargeable_Job') & '_comment')

Prompt::job:Charge_Type  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Charge_Type') & '_prompt',Choose(p_web.GetSessionValue('job:Chargeable_Job') <> 'YES' ,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Chargeable Type')
  If p_web.GetSessionValue('job:Chargeable_Job') <> 'YES' 
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Charge_Type') & '_prompt')

Validate::job:Charge_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Charge_Type',p_web.GetValue('NewValue'))
    job:Charge_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Charge_Type
    do Value::job:Charge_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Charge_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Charge_Type = p_web.GetValue('Value')
  End
  If job:Charge_Type = ''
    loc:Invalid = 'job:Charge_Type'
    loc:alert = p_web.translate('Chargeable Type') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    job:Charge_Type = Upper(job:Charge_Type)
    p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  Do Value:ChargeType
  p_Web.SetValue('lookupfield','job:Charge_Type')
  do AfterLookup
  do Value::job:Charge_Type
  do SendAlert
  do Comment::job:Charge_Type
  do Comment::job:Charge_Type
  do Prompt::job:Authority_Number
  do Value::job:Authority_Number  !1
  do Comment::job:Authority_Number

Value::job:Charge_Type  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Charge_Type') & '_value',Choose(p_web.GetSessionValue('job:Chargeable_Job') <> 'YES' ,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('job:Chargeable_Job') <> 'YES' )
  ! --- STRING --- job:Charge_Type
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:ChargeableJob') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GetSessionValue('ReadOnly:ChargeableJob') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('job:Charge_Type')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If job:Charge_Type = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Charge_Type'',''newjobbooking_job:charge_type_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Charge_Type')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','job:Charge_Type',p_web.GetSessionValue('job:Charge_Type'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectChargeTypes?LookupJobType=0')&'?LookupField=job:Charge_Type&Tab=8&ForeignField=cha:Charge_Type&_sort=&Refresh=sort&LookupFrom=NewJobBooking&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Charge_Type') & '_value')

Comment::job:Charge_Type  Routine
    loc:comment = p_web.Translate('Required')
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Charge_Type') & '_comment',Choose(p_web.GetSessionValue('job:Chargeable_Job') <> 'YES' ,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('job:Chargeable_Job') <> 'YES' 
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Charge_Type') & '_comment')

Prompt::job:Warranty_Job  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Warranty_Job') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Warranty Job')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Warranty_Job') & '_prompt')

Validate::job:Warranty_Job  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Warranty_Job',p_web.GetValue('NewValue'))
    job:Warranty_Job = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Warranty_Job
    do Value::job:Warranty_Job
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Warranty_Job',p_web.dFormat(p_web.GetValue('Value'),'@s3'))
    job:Warranty_Job = p_web.GetValue('Value')
  End
  Do CalculateFaultCodes
  do Value::job:Warranty_Job
  do SendAlert
  do Comment::job:Warranty_Job
  do Prompt::job:Warranty_Charge_Type
  do Value::job:Warranty_Charge_Type  !1
  do Comment::job:Warranty_Charge_Type
  do Value::job:DOP  !1
  do Comment::job:DOP
  do Value::job:Authority_Number  !1
  do Comment::job:Authority_Number
  do Prompt::tmp:FaultCode_1
  do Value::tmp:FaultCode_1  !1
  do Comment::tmp:FaultCode_1
  do Prompt::tmp:FaultCode_2
  do Value::tmp:FaultCode_2  !1
  do Comment::tmp:FaultCode_2
  do Prompt::tmp:FaultCode_3
  do Value::tmp:FaultCode_3  !1
  do Comment::tmp:FaultCode_3
  do Prompt::tmp:FaultCode_4
  do Value::tmp:FaultCode_4  !1
  do Comment::tmp:FaultCode_4
  do Prompt::tmp:FaultCode_5
  do Value::tmp:FaultCode_5  !1
  do Comment::tmp:FaultCode_5
  do Prompt::tmp:FaultCode_6
  do Value::tmp:FaultCode_6  !1
  do Comment::tmp:FaultCode_6
  do Prompt::tmp:AmendFaultCodes
  do Value::tmp:AmendFaultCodes  !1
  do Comment::tmp:AmendFaultCodes

Value::job:Warranty_Job  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Warranty_Job') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- job:Warranty_Job
  loc:fieldclass = Choose(sub(' noBorder',1,1) = ' ',clip('FormEntry') & ' noBorder',' noBorder')
  If lower(loc:invalid) = lower('job:Warranty_Job')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(p_web.GSV('ReadOnly:WarrantyJob') = 1 Or p_web.GSV('Hide:LiquidDamage') <> 1,'disabled','')
    if p_web.GetSessionValue('job:Warranty_Job') = 'YES'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''job:Warranty_Job'',''newjobbooking_job:warranty_job_value'',1,'''&clip('YES')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Warranty_Job')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','job:Warranty_Job',clip('YES'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,'Warranty Job','job:Warranty_Job_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Yes') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(p_web.GSV('ReadOnly:WarrantyJob') = 1 Or p_web.GSV('Hide:LiquidDamage') <> 1,'disabled','')
    if p_web.GetSessionValue('job:Warranty_Job') = 'NO'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''job:Warranty_Job'',''newjobbooking_job:warranty_job_value'',1,'''&clip('NO')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Warranty_Job')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','job:Warranty_Job',clip('NO'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,'Warranty Job','job:Warranty_Job_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('No') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Warranty_Job') & '_value')

Comment::job:Warranty_Job  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Warranty_Job') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Warranty_Job') & '_comment')

Prompt::job:Warranty_Charge_Type  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Warranty_Charge_Type') & '_prompt',Choose(p_web.GSV('job:Warranty_Job') <> 'YES','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Warranty Charge Type')
  If p_web.GSV('job:Warranty_Job') <> 'YES'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Warranty_Charge_Type') & '_prompt')

Validate::job:Warranty_Charge_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Warranty_Charge_Type',p_web.GetValue('NewValue'))
    job:Warranty_Charge_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Warranty_Charge_Type
    do Value::job:Warranty_Charge_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Warranty_Charge_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Warranty_Charge_Type = p_web.GetValue('Value')
  End
  If job:Warranty_Charge_Type = ''
    loc:Invalid = 'job:Warranty_Charge_Type'
    loc:alert = p_web.translate('Warranty Charge Type') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    job:Warranty_Charge_Type = Upper(job:Warranty_Charge_Type)
    p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
  Do Value:WarrantyChargeType
  p_Web.SetValue('lookupfield','job:Warranty_Charge_Type')
  do AfterLookup
  do Value::job:Warranty_Charge_Type
  do SendAlert
  do Comment::job:Warranty_Charge_Type
  do Comment::job:Warranty_Charge_Type
  do Value::job:Authority_Number  !1
  do Comment::job:Authority_Number
  do Value::job:DOP  !1
  do Comment::job:DOP

Value::job:Warranty_Charge_Type  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Warranty_Charge_Type') & '_value',Choose(p_web.GSV('job:Warranty_Job') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Warranty_Job') <> 'YES')
  ! --- STRING --- job:Warranty_Charge_Type
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:WarrantyJob') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:WarrantyJob') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('job:Warranty_Charge_Type')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If job:Warranty_Charge_Type = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Warranty_Charge_Type'',''newjobbooking_job:warranty_charge_type_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Warranty_Charge_Type')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','job:Warranty_Charge_Type',p_web.GetSessionValue('job:Warranty_Charge_Type'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectChargeTypes?LookupJobType=1')&'?LookupField=job:Warranty_Charge_Type&Tab=8&ForeignField=cha:Charge_Type&_sort=&Refresh=sort&LookupFrom=NewJobBooking&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Warranty_Charge_Type') & '_value')

Comment::job:Warranty_Charge_Type  Routine
    loc:comment = p_web.Translate('Required')
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Warranty_Charge_Type') & '_comment',Choose(p_web.GSV('job:Warranty_Job') <> 'YES','hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('job:Warranty_Job') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Warranty_Charge_Type') & '_comment')

Prompt::job:Authority_Number  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Authority_Number') & '_prompt',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Authority Number')
  If p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Authority_Number') & '_prompt')

Validate::job:Authority_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Authority_Number',p_web.GetValue('NewValue'))
    job:Authority_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Authority_Number
    do Value::job:Authority_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Authority_Number',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Authority_Number = p_web.GetValue('Value')
  End
  If job:Authority_Number = '' and p_web.GetSessionValue('Required:AuthorityNumber') = 1
    loc:Invalid = 'job:Authority_Number'
    loc:alert = p_web.translate('Authority Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    job:Authority_Number = Upper(job:Authority_Number)
    p_web.SetSessionValue('job:Authority_Number',job:Authority_Number)
  do Value::job:Authority_Number
  do SendAlert

Value::job:Authority_Number  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Authority_Number') & '_value',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0)
  ! --- STRING --- job:Authority_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If (p_web.GetSessionValue('Required:AuthorityNumber') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('job:Authority_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If job:Authority_Number = '' and (p_web.GetSessionValue('Required:AuthorityNumber') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Authority_Number'',''newjobbooking_job:authority_number_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Authority_Number',p_web.GetSessionValueFormat('job:Authority_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Authority_Number') & '_value')

Comment::job:Authority_Number  Routine
    loc:comment = p_web.Translate(p_web.GetSessionValue('Comment:AuthorityNumber'))
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Authority_Number') & '_comment',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Authority_Number') & '_comment')

Prompt::locMobileLifetimeValue  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('locMobileLifetimeValue') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Lifetime Value')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locMobileLifetimeValue  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locMobileLifetimeValue',p_web.GetValue('NewValue'))
    locMobileLifetimeValue = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locMobileLifetimeValue
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locMobileLifetimeValue',p_web.GetValue('Value'))
    locMobileLifetimeValue = p_web.GetValue('Value')
  End

Value::locMobileLifetimeValue  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('locMobileLifetimeValue') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locMobileLifetimeValue
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locMobileLifetimeValue'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('locMobileLifetimeValue') & '_value')

Comment::locMobileLifetimeValue  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('locMobileLifetimeValue') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locMobileAverageSpend  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('locMobileAverageSpend') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Average Spend')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locMobileAverageSpend  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locMobileAverageSpend',p_web.GetValue('NewValue'))
    locMobileAverageSpend = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locMobileAverageSpend
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locMobileAverageSpend',p_web.GetValue('Value'))
    locMobileAverageSpend = p_web.GetValue('Value')
  End

Value::locMobileAverageSpend  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('locMobileAverageSpend') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locMobileAverageSpend
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locMobileAverageSpend'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('locMobileAverageSpend') & '_value')

Comment::locMobileAverageSpend  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('locMobileAverageSpend') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locMobileLoyaltyStatus  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('locMobileLoyaltyStatus') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Loyalty Status')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locMobileLoyaltyStatus  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locMobileLoyaltyStatus',p_web.GetValue('NewValue'))
    locMobileLoyaltyStatus = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locMobileLoyaltyStatus
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locMobileLoyaltyStatus',p_web.GetValue('Value'))
    locMobileLoyaltyStatus = p_web.GetValue('Value')
  End

Value::locMobileLoyaltyStatus  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('locMobileLoyaltyStatus') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locMobileLoyaltyStatus
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locMobileLoyaltyStatus'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('locMobileLoyaltyStatus') & '_value')

Comment::locMobileLoyaltyStatus  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('locMobileLoyaltyStatus') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locMobileUpgradeDate  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('locMobileUpgradeDate') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Upgrade Date')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locMobileUpgradeDate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locMobileUpgradeDate',p_web.GetValue('NewValue'))
    locMobileUpgradeDate = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locMobileUpgradeDate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locMobileUpgradeDate',p_web.GetValue('Value'))
    locMobileUpgradeDate = p_web.GetValue('Value')
  End

Value::locMobileUpgradeDate  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('locMobileUpgradeDate') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locMobileUpgradeDate
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locMobileUpgradeDate'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('locMobileUpgradeDate') & '_value')

Comment::locMobileUpgradeDate  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('locMobileUpgradeDate') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locMobileIDNumber  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('locMobileIDNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('ID Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locMobileIDNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locMobileIDNumber',p_web.GetValue('NewValue'))
    locMobileIDNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locMobileIDNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locMobileIDNumber',p_web.GetValue('Value'))
    locMobileIDNumber = p_web.GetValue('Value')
  End

Value::locMobileIDNumber  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('locMobileIDNumber') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locMobileIDNumber
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locMobileIDNumber'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('locMobileIDNumber') & '_value')

Comment::locMobileIDNumber  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('locMobileIDNumber') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::FranchiseAccount  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('FranchiseAccount') & '_prompt',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Account Type')
  If p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::FranchiseAccount  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('FranchiseAccount',p_web.GetValue('NewValue'))
    FranchiseAccount = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::FranchiseAccount
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('FranchiseAccount',p_web.GetValue('Value'))
    FranchiseAccount = p_web.GetValue('Value')
  End
  If p_web.GetSessionValue('FranchiseAccount') <> p_web.GetSessionValue('Save:FranchiseAccount')
      Do Reset:AccountNumber
      p_web.SetSessionValue('Comment:AccountNumber','Required')
  End ! If p_web.GetSessionValue('FranchiseAccount') <> p_web.GetSessionValue('Save:FranchiseAccount')
  p_web.SetSessionValue('Save:FranchiseAccount',p_web.GetSessionValue('FranchiseAccount'))
  do Value::FranchiseAccount
  do SendAlert
  do Prompt::job:Order_Number
  do Value::job:Order_Number  !1
  do Comment::job:Order_Number
  do Prompt::job:Account_Number
  do Value::job:Account_Number  !1
  do Comment::job:Account_Number
  do Prompt::job:Initial
  do Value::job:Initial  !1
  do Comment::job:Initial
  do Prompt::job:Title
  do Value::job:Title  !1
  do Comment::job:Title
  do Prompt::job:Surname
  do Value::job:Surname  !1
  do Comment::job:Surname
  do Prompt::jobe:EndUserTelNo
  do Value::jobe:EndUserTelNo  !1
  do Comment::jobe:EndUserTelNo
  do Prompt::job:Account_Number2
  do Value::job:Account_Number2  !1
  do Comment::job:Account_Number2

Value::FranchiseAccount  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('FranchiseAccount') & '_value',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0)
  ! --- RADIO --- FranchiseAccount
  loc:fieldclass = Choose(sub(' noBorder',1,1) = ' ',clip('FormEntry') & ' noBorder',' noBorder')
  If lower(loc:invalid) = lower('FranchiseAccount')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('FranchiseAccount') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''FranchiseAccount'',''newjobbooking_franchiseaccount_value'',1,'''&clip(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('FranchiseAccount')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','FranchiseAccount',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'FranchiseAccount_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Franchise') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('FranchiseAccount') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''FranchiseAccount'',''newjobbooking_franchiseaccount_value'',1,'''&clip(2)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('FranchiseAccount')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','FranchiseAccount',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'FranchiseAccount_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Generic') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('FranchiseAccount') & '_value')

Comment::FranchiseAccount  Routine
    loc:comment = p_web.Translate('Required')
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('FranchiseAccount') & '_comment',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::hidden5  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('hidden5',p_web.GetValue('NewValue'))
    do Value::hidden5
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::hidden5  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('hidden5') & '_value','adiv')
  loc:extra = ''
  ! --- HIDDEN --- 
    packet = clip(packet) & p_web.CreateInput('hidden','',p_web.GetSessionValue('')) & '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::hidden5  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('hidden5') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Account_Number  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Account_Number') & '_prompt',Choose(p_web.GetSessionValue('FranchiseAccount') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Franchise Account No')
  If p_web.GetSessionValue('FranchiseAccount') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Account_Number') & '_prompt')

Validate::job:Account_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Account_Number',p_web.GetValue('NewValue'))
    job:Account_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Account_Number
    do Value::job:Account_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Account_Number',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    job:Account_Number = p_web.GetValue('Value')
  End
  If job:Account_Number = ''
    loc:Invalid = 'job:Account_Number'
    loc:alert = 'Account Number Required'
  End
  p_Web.SetValue('lookupfield','job:Account_Number')
  do AfterLookup
  do Value::job:Account_Number
  do SendAlert
  do Comment::job:Account_Number
  do Comment::job:Account_Number
  do Prompt::job:Initial
  do Value::job:Initial  !1
  do Comment::job:Initial
  do Prompt::job:Surname
  do Value::job:Surname  !1
  do Comment::job:Surname
  do Prompt::job:Title
  do Value::job:Title  !1
  do Comment::job:Title
  do Prompt::job:Order_Number
  do Value::job:Order_Number  !1
  do Comment::job:Order_Number
  do Prompt::jobe:EndUserTelNo
  do Value::jobe:EndUserTelNo  !1
  do Comment::jobe:EndUserTelNo
  do Prompt::job:Courier
  do Value::job:Courier  !1
  do Comment::job:Courier
  do Prompt::jobe2:CourierWaybillNumber
  do Value::jobe2:CourierWaybillNumber  !1
  do Comment::jobe2:CourierWaybillNumber
  do Prompt::jobe:Booking48HourOption
  do Value::jobe:Booking48HourOption  !1
  do Comment::jobe:Booking48HourOption

Value::job:Account_Number  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Account_Number') & '_value',Choose(p_web.GetSessionValue('FranchiseAccount') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('FranchiseAccount') <> 1)
  ! --- STRING --- job:Account_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('job:Account_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If job:Account_Number = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Account_Number'',''newjobbooking_job:account_number_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Account_Number')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','job:Account_Number',p_web.GetSessionValue('job:Account_Number'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s15',loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('LookupRRCAccounts')&'?LookupField=job:Account_Number&Tab=8&ForeignField=sub:Account_Number&_sort=&Refresh=sort&LookupFrom=NewJobBooking&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Account_Number') & '_value')

Comment::job:Account_Number  Routine
    loc:comment = p_web.Translate(p_web.GetSessionValue('Comment:AccountNumber'))
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Account_Number') & '_comment',Choose(p_web.GetSessionValue('FranchiseAccount') <> 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('FranchiseAccount') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Account_Number') & '_comment')

Prompt::job:Account_Number2  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Account_Number2') & '_prompt',Choose(p_web.GSV('FranchiseAccount') <> 2,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Generic Account No')
  If p_web.GSV('FranchiseAccount') <> 2
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Account_Number2') & '_prompt')

Validate::job:Account_Number2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Account_Number2',p_web.GetValue('NewValue'))
    job:Account_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Account_Number
    do Value::job:Account_Number2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Account_Number',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    job:Account_Number = p_web.GetValue('Value')
  End
  If job:Account_Number = ''
    loc:Invalid = 'job:Account_Number'
    loc:alert = 'Account Number Required'
  End
  p_Web.SetValue('lookupfield','job:Account_Number2')
  do AfterLookup
  do Value::job:Account_Number2
  do SendAlert
  do Comment::job:Account_Number2
  do Prompt::job:Account_Number2
  do Comment::job:Account_Number2
  do Prompt::job:Initial
  do Value::job:Initial  !1
  do Comment::job:Initial
  do Prompt::job:Surname
  do Value::job:Surname  !1
  do Comment::job:Surname
  do Prompt::job:Title
  do Value::job:Title  !1
  do Comment::job:Title
  do Prompt::job:Order_Number
  do Value::job:Order_Number  !1
  do Comment::job:Order_Number
  do Prompt::jobe:EndUserTelNo
  do Value::jobe:EndUserTelNo  !1
  do Comment::jobe:EndUserTelNo
  do Prompt::job:Courier
  do Value::job:Courier  !1
  do Comment::job:Courier
  do Prompt::jobe2:CourierWaybillNumber
  do Value::jobe2:CourierWaybillNumber  !1
  do Comment::jobe2:CourierWaybillNumber
  do Prompt::jobe:Booking48HourOption
  do Value::jobe:Booking48HourOption  !1
  do Comment::jobe:Booking48HourOption

Value::job:Account_Number2  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Account_Number2') & '_value',Choose(p_web.GSV('FranchiseAccount') <> 2,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('FranchiseAccount') <> 2)
  ! --- STRING --- job:Account_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('job:Account_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If job:Account_Number = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Account_Number2'',''newjobbooking_job:account_number2_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Account_Number')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','job:Account_Number',p_web.GetSessionValue('job:Account_Number'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s15',loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('LookupGenericAccounts')&'?LookupField=job:Account_Number&Tab=8&ForeignField=TRA1:SubAcc&_sort=&Refresh=sort&LookupFrom=NewJobBooking&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Account_Number2') & '_value')

Comment::job:Account_Number2  Routine
    loc:comment = p_web.Translate(p_web.GetSessionValue('Comment:AccountNumber'))
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Account_Number2') & '_comment',Choose(p_web.GSV('FranchiseAccount') <> 2,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('FranchiseAccount') <> 2
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Account_Number2') & '_comment')

Prompt::Text:HubWarning  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('Text:HubWarning') & '_prompt',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 OR p_web.GetSessionValue('ShowHubWarning') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GetSessionValue('IMEINumberAccepted') = 0 OR p_web.GetSessionValue('ShowHubWarning') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::Text:HubWarning  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('Text:HubWarning',p_web.GetValue('NewValue'))
    do Value::Text:HubWarning
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::Text:HubWarning  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('Text:HubWarning') & '_value',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 OR p_web.GetSessionValue('ShowHubWarning') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0 OR p_web.GetSessionValue('ShowHubWarning') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web.Translate('Warning! Outside HUB. You may not be able to despatch this job.',) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::Text:HubWarning  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('Text:HubWarning') & '_comment',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 OR p_web.GetSessionValue('ShowHubWarning') <> 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0 OR p_web.GetSessionValue('ShowHubWarning') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Order_Number  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Order_Number') & '_prompt',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('FranchiseAccount') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Order Number')
  If p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('FranchiseAccount') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Order_Number') & '_prompt')

Validate::job:Order_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Order_Number',p_web.GetValue('NewValue'))
    job:Order_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Order_Number
    do Value::job:Order_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Order_Number',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Order_Number = p_web.GetValue('Value')
  End
  If job:Order_Number = '' and p_web.GetSessionValue('Required:OrderNumber') = 1
    loc:Invalid = 'job:Order_Number'
    loc:alert = 'Order Number Required'
  End
    job:Order_Number = Upper(job:Order_Number)
    p_web.SetSessionValue('job:Order_Number',job:Order_Number)
  do Value::job:Order_Number
  do SendAlert

Value::job:Order_Number  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Order_Number') & '_value',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('FranchiseAccount') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('FranchiseAccount') = 0)
  ! --- STRING --- job:Order_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If (p_web.GetSessionValue('Required:OrderNumber') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('job:Order_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If job:Order_Number = '' and (p_web.GetSessionValue('Required:OrderNumber') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Order_Number'',''newjobbooking_job:order_number_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Order_Number',p_web.GetSessionValueFormat('job:Order_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Order_Number') & '_value')

Comment::job:Order_Number  Routine
    loc:comment = p_web.Translate(p_web.GetSessionValue('Comment:OrderNumber'))
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Order_Number') & '_comment',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('FranchiseAccount') = 0,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('FranchiseAccount') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Order_Number') & '_comment')

Prompt::job:Title  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Title') & '_prompt',Choose(p_web.GetSessionValue('Hide:EndUserName') = 1 OR p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Title')
  If p_web.GetSessionValue('Hide:EndUserName') = 1 OR p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Title') & '_prompt')

Validate::job:Title  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Title',p_web.GetValue('NewValue'))
    job:Title = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Title
    do Value::job:Title
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Title',p_web.dFormat(p_web.GetValue('Value'),'@s4'))
    job:Title = p_web.GetValue('Value')
  End
    job:Title = Upper(job:Title)
    p_web.SetSessionValue('job:Title',job:Title)
  do Value::job:Title
  do SendAlert

Value::job:Title  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Title') & '_value',Choose(p_web.GetSessionValue('Hide:EndUserName') = 1 OR p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:EndUserName') = 1 OR p_web.GetSessionValue('IMEINumberAccepted') = 0)
  ! --- STRING --- job:Title
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('job:Title')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Title'',''newjobbooking_job:title_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Title',p_web.GetSessionValueFormat('job:Title'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s4'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Title') & '_value')

Comment::job:Title  Routine
      loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Title') & '_comment',Choose(p_web.GetSessionValue('Hide:EndUserName') = 1 OR p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('Hide:EndUserName') = 1 OR p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Title') & '_comment')

Prompt::job:Initial  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Initial') & '_prompt',Choose(p_web.GetSessionValue('Hide:EndUserName') = 1 OR p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Initial')
  If p_web.GetSessionValue('Hide:EndUserName') = 1 OR p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Initial') & '_prompt')

Validate::job:Initial  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Initial',p_web.GetValue('NewValue'))
    job:Initial = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Initial
    do Value::job:Initial
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Initial',p_web.dFormat(p_web.GetValue('Value'),'@s1'))
    job:Initial = p_web.GetValue('Value')
  End
    job:Initial = Upper(job:Initial)
    p_web.SetSessionValue('job:Initial',job:Initial)
  do Value::job:Initial
  do SendAlert

Value::job:Initial  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Initial') & '_value',Choose(p_web.GetSessionValue('Hide:EndUserName') = 1 OR p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:EndUserName') = 1 OR p_web.GetSessionValue('IMEINumberAccepted') = 0)
  ! --- STRING --- job:Initial
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('job:Initial')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Initial'',''newjobbooking_job:initial_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Initial',p_web.GetSessionValueFormat('job:Initial'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s1'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Initial') & '_value')

Comment::job:Initial  Routine
      loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Initial') & '_comment',Choose(p_web.GetSessionValue('Hide:EndUserName') = 1 OR p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('Hide:EndUserName') = 1 OR p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Initial') & '_comment')

Prompt::job:Surname  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Surname') & '_prompt',Choose(p_web.GetSessionValue('Hide:EndUserName') = 1 OR p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Surname')
  If p_web.GetSessionValue('Hide:EndUserName') = 1 OR p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Surname') & '_prompt')

Validate::job:Surname  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Surname',p_web.GetValue('NewValue'))
    job:Surname = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Surname
    do Value::job:Surname
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Surname',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Surname = p_web.GetValue('Value')
  End
  If job:Surname = '' and p_web.GetSessionValue('Required:EndUserName') = 1
    loc:Invalid = 'job:Surname'
    loc:alert = p_web.translate('Surname') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    job:Surname = Upper(job:Surname)
    p_web.SetSessionValue('job:Surname',job:Surname)
  do Value::job:Surname
  do SendAlert

Value::job:Surname  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Surname') & '_value',Choose(p_web.GetSessionValue('Hide:EndUserName') = 1 OR p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:EndUserName') = 1 OR p_web.GetSessionValue('IMEINumberAccepted') = 0)
  ! --- STRING --- job:Surname
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If (p_web.GetSessionValue('Required:EndUserName') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('job:Surname')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If job:Surname = '' and (p_web.GetSessionValue('Required:EndUserName') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Surname'',''newjobbooking_job:surname_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Surname',p_web.GetSessionValueFormat('job:Surname'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Surname') & '_value')

Comment::job:Surname  Routine
    loc:comment = p_web.Translate(p_web.GetSessionValue('Comment:EndUserName'))
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Surname') & '_comment',Choose(p_web.GetSessionValue('Hide:EndUserName') = 1 OR p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('Hide:EndUserName') = 1 OR p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Surname') & '_comment')

Prompt::jobe:EndUserTelNo  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe:EndUserTelNo') & '_prompt',Choose(p_web.GetSessionValue('Hide:EndUserName') = 1 OR p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Telephone Number')
  If p_web.GetSessionValue('Hide:EndUserName') = 1 OR p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe:EndUserTelNo') & '_prompt')

Validate::jobe:EndUserTelNo  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe:EndUserTelNo',p_web.GetValue('NewValue'))
    jobe:EndUserTelNo = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe:EndUserTelNo
    do Value::jobe:EndUserTelNo
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe:EndUserTelNo',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    jobe:EndUserTelNo = p_web.GetValue('Value')
  End
    jobe:EndUserTelNo = Upper(jobe:EndUserTelNo)
    p_web.SetSessionValue('jobe:EndUserTelNo',jobe:EndUserTelNo)
  do Value::jobe:EndUserTelNo
  do SendAlert

Value::jobe:EndUserTelNo  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe:EndUserTelNo') & '_value',Choose(p_web.GetSessionValue('Hide:EndUserName') = 1 OR p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:EndUserName') = 1 OR p_web.GetSessionValue('IMEINumberAccepted') = 0)
  ! --- STRING --- jobe:EndUserTelNo
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jobe:EndUserTelNo')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe:EndUserTelNo'',''newjobbooking_jobe:endusertelno_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe:EndUserTelNo',p_web.GetSessionValueFormat('jobe:EndUserTelNo'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe:EndUserTelNo') & '_value')

Comment::jobe:EndUserTelNo  Routine
      loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe:EndUserTelNo') & '_comment',Choose(p_web.GetSessionValue('Hide:EndUserName') = 1 OR p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('Hide:EndUserName') = 1 OR p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe:EndUserTelNo') & '_comment')

Prompt::job:Courier  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Courier') & '_prompt',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Outgoing Courier')
  If p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Courier') & '_prompt')

Validate::job:Courier  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Courier',p_web.GetValue('NewValue'))
    job:Courier = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Courier
    do Value::job:Courier
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Courier',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Courier = p_web.GetValue('Value')
  End
  If job:Courier = '' and p_web.GetSessionValue('Required:Courier') = 1
    loc:Invalid = 'job:Courier'
    loc:alert = p_web.translate('Outgoing Courier') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    job:Courier = Upper(job:Courier)
    p_web.SetSessionValue('job:Courier',job:Courier)
  Access:COURIER.Clearkey(cou:Courier_Key)
  cou:Courier    = p_web.GSV('job:Courier')
  if (Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign)
      ! Found
  else ! if (Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign)
      ! Error
      p_web.SSV('job:Courier','')
  end ! if (Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign)
  p_Web.SetValue('lookupfield','job:Courier')
  do AfterLookup
  do Value::job:Courier
  do SendAlert
  do Comment::job:Courier

Value::job:Courier  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Courier') & '_value',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0)
  ! --- STRING --- job:Courier
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If (p_web.GetSessionValue('Required:Courier') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('job:Courier')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If job:Courier = '' and (p_web.GetSessionValue('Required:Courier') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Courier'',''newjobbooking_job:courier_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Courier')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','job:Courier',p_web.GetSessionValue('job:Courier'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectCouriers')&'?LookupField=job:Courier&Tab=8&ForeignField=cou:Courier&_sort=&Refresh=sort&LookupFrom=NewJobBooking&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Courier') & '_value')

Comment::job:Courier  Routine
    loc:comment = p_web.Translate(p_web.GetSessionValue('Comment:OutgoingCourier'))
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Courier') & '_comment',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Courier') & '_comment')

Prompt::jobe2:CourierWaybillNumber  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:CourierWaybillNumber') & '_prompt',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Courier Waybill No')
  If p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:CourierWaybillNumber') & '_prompt')

Validate::jobe2:CourierWaybillNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:CourierWaybillNumber',p_web.GetValue('NewValue'))
    jobe2:CourierWaybillNumber = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe2:CourierWaybillNumber
    do Value::jobe2:CourierWaybillNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe2:CourierWaybillNumber',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jobe2:CourierWaybillNumber = p_web.GetValue('Value')
  End
  If jobe2:CourierWaybillNumber = '' and p_web.GSV('Required:CourierWaybillNo') = 1
    loc:Invalid = 'jobe2:CourierWaybillNumber'
    loc:alert = 'Courier Waybill Number Required'
  End
    jobe2:CourierWaybillNumber = Upper(jobe2:CourierWaybillNumber)
    p_web.SetSessionValue('jobe2:CourierWaybillNumber',jobe2:CourierWaybillNumber)
  do Value::jobe2:CourierWaybillNumber
  do SendAlert

Value::jobe2:CourierWaybillNumber  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:CourierWaybillNumber') & '_value',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0)
  ! --- STRING --- jobe2:CourierWaybillNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If (p_web.GSV('Required:CourierWaybillNo') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('jobe2:CourierWaybillNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If jobe2:CourierWaybillNumber = '' and (p_web.GSV('Required:CourierWaybillNo') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:CourierWaybillNumber'',''newjobbooking_jobe2:courierwaybillnumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:CourierWaybillNumber',p_web.GetSessionValueFormat('jobe2:CourierWaybillNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Courier Waybill Number') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:CourierWaybillNumber') & '_value')

Comment::jobe2:CourierWaybillNumber  Routine
    loc:comment = p_web.Translate(p_web.GetSessionValue('Comment:CourierWaybillNumber'))
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:CourierWaybillNumber') & '_comment',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:CourierWaybillNumber') & '_comment')

Validate::amendAddresses  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('amendAddresses',p_web.GetValue('NewValue'))
    do Value::amendAddresses
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::amendAddresses  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('amendAddresses') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','AmendAddresses','Amend Addresses','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('AmendAddress?FromURL=NewJobBooking')) & ''','''&clip('_self')&''')',loc:javascript,0,'/images/list.png',,,,)

  do SendPacket
  p_web._DivFooter()

Comment::amendAddresses  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('amendAddresses') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:AmendAccessories  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:AmendAccessories') & '_prompt',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Amend Accessories')
  If p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:AmendAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:AmendAccessories',p_web.GetValue('NewValue'))
    tmp:AmendAccessories = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:AmendAccessories
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('tmp:AmendAccessories',p_web.GetValue('Value'))
    tmp:AmendAccessories = p_web.GetValue('Value')
  End
  do Value::tmp:AmendAccessories
  do SendAlert
  do Value::Text:AccessoryMessage  !1
  do Prompt::TheBlank
  do Value::TheBlank  !1
  do Prompt::tmp:ShowAccessory
  do Value::tmp:ShowAccessory  !1
  do Value::tmp:TheAccessory  !1
  do Prompt::Button:AddAccessories
  do Value::Button:AddAccessories  !1
  do Value::Button:RemoveAccessory  !1
  do Value::Button:RemoveAllAccessories  !1

Value::tmp:AmendAccessories  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:AmendAccessories') & '_value',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0)
  ! --- CHECKBOX --- tmp:AmendAccessories
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:AmendAccessories'',''newjobbooking_tmp:amendaccessories_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:AmendAccessories')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('tmp:AmendAccessories') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:AmendAccessories',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('tmp:AmendAccessories') & '_value')

Comment::tmp:AmendAccessories  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:AmendAccessories') & '_comment',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::Text:AccessoryMessage  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('Text:AccessoryMessage') & '_prompt',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::Text:AccessoryMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('Text:AccessoryMessage',p_web.GetValue('NewValue'))
    do Value::Text:AccessoryMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::Text:AccessoryMessage  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('Text:AccessoryMessage') & '_value',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate('Hold "CTRL" or "SHIFT" to select more than one accessory',) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('Text:AccessoryMessage') & '_value')

Comment::Text:AccessoryMessage  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('Text:AccessoryMessage') & '_comment',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ShowAccessory  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:ShowAccessory') & '_prompt',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('tmp:ShowAccessory') & '_prompt')

Validate::tmp:ShowAccessory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ShowAccessory',p_web.GetValue('NewValue'))
    tmp:ShowAccessory = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ShowAccessory
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ShowAccessory',p_web.GetValue('Value'))
    tmp:ShowAccessory = p_web.GetValue('Value')
  End
  do SendAlert
  do Value::tmp:TheAccessory  !1

Value::tmp:ShowAccessory  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:ShowAccessory') & '_value',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1)
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:ShowAccessory')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ShowAccessory'',''newjobbooking_tmp:showaccessory_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ShowAccessory')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:ShowAccessory',loc:fieldclass,loc:readonly,10,180,1,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  loc:rowstyle = choose(sub('GreyRegular',1,1)=' ',clip(loc:rowstyle) & 'GreyRegular','GreyRegular')
  if p_web.IfExistsSessionValue('tmp:ShowAccessory') = 0
    p_web.SetSessionValue('tmp:ShowAccessory','')
  end
    packet = clip(packet) & p_web.CreateOption('------Available Accessories------','',choose('' = p_web.getsessionvalue('tmp:ShowAccessory')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  Access:ACCESSOR.Clearkey(acr:Accesory_Key)
  acr:Model_NUmber = p_web.GSV('job:Model_Number')
  Set(acr:Accesory_Key,acr:Accesory_Key)
  Loop
      If Access:ACCESSOR.Next()
          Break
      End ! If Access:ACCESSOR.Next()
      If acr:Model_Number <> p_web.GSV('job:Model_Number')
          Break
      End ! If acr:Model_Number <> p_web.GSV('tmp:ModelNumber')
      If Instring(';' & Clip(acr:Accessory),p_web.GSV('tmp:TheJobAccessory'),1,1)
          Cycle
      End ! If Instring(';' & Clip(acr:Accessory) & ';',p_web.GSV('tmp:TheJobAccessory'),1,1)
  
      loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
      packet = clip(packet) & p_web.CreateOption(Clip(acr:Accessory),acr:Accessory,choose(acr:Accessory = p_web.GSV('tmp:ShowAccessory')),clip(loc:rowstyle),,)&CRLF
      loc:even = Choose(loc:even=1,2,1)
  End ! Loop
  
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('tmp:ShowAccessory') & '_value')

Comment::tmp:ShowAccessory  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:ShowAccessory') & '_comment',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::tmp:TheAccessory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:TheAccessory',p_web.GetValue('NewValue'))
    tmp:TheAccessory = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:TheAccessory
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:TheAccessory',p_web.GetValue('Value'))
    tmp:TheAccessory = p_web.GetValue('Value')
  End
  do SendAlert
  do Value::tmp:ShowAccessory  !1

Value::tmp:TheAccessory  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:TheAccessory') & '_value',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1)
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:TheAccessory')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:TheAccessory'',''newjobbooking_tmp:theaccessory_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:TheAccessory')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:TheAccessory',loc:fieldclass,loc:readonly,10,180,0,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  loc:rowstyle = choose(sub('GreyRegular',1,1)=' ',clip(loc:rowstyle) & 'GreyRegular','GreyRegular')
  if p_web.IfExistsSessionValue('tmp:TheAccessory') = 0
    p_web.SetSessionValue('tmp:TheAccessory','')
  end
    packet = clip(packet) & p_web.CreateOption('-------Accessories On Job-------','',choose('' = p_web.getsessionvalue('tmp:TheAccessory')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
      If p_web.GSV('tmp:TheJobAccessory') <> ''
          Loop x# = 1 To 1000
              If Sub(p_web.GSV('tmp:TheJobAccessory'),x#,2) = '|;'
                  Start# = x# + 2
                  Cycle
              End ! If Sub(p_web.GSV('tmp:TheJobAccessory'),x#,2) = '|;'
  
              If Start# > 0
                 If Sub(p_web.GSV('tmp:TheJobAccessory'),x#,2) = ';|'
                     tmp:FoundAccessory = Sub(p_web.GSV('tmp:TheJobAccessory'),Start#,x# - Start#)
  
                     If tmp:FoundAccessory <> ''
                         loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
                         packet = clip(packet) & p_web.CreateOption(tmp:FoundAccessory,tmp:FoundAccessory,choose(tmp:FoundAccessory = p_web.GSV('tmp:TheAccessory')),clip(loc:rowstyle),,)&CRLF
                         loc:even = Choose(loc:even=1,2,1)
                     End ! If tmp:FoundAccessory <> ''
                     Start# = 0
                 End ! If Sub(p_web.GSV('tmp:TheJobAccessory'),x#,2) = ';|'
              End ! If Start# > 0
          End ! Loop x# = 1 To 1000
  
          If Start# > 0
             tmp:FoundAccessory = Clip(Sub(p_web.GSV('tmp:TheJobAccessory'),Start#,30))
             If tmp:FoundAccessory <> ''
                 loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
                 packet = clip(packet) & p_web.CreateOption(tmp:FoundAccessory,tmp:FoundAccessory,choose(tmp:FoundAccessory = p_web.GSV('tmp:TheAccessory')),clip(loc:rowstyle),,)&CRLF
                 loc:even = Choose(loc:even=1,2,1)
             End ! If tmp:FoundAccessory <> ''
          End ! If Start# > 0
      End ! If p_web.GSV('tmp:TheJobAccessory') <> ''
  
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('tmp:TheAccessory') & '_value')

Comment::tmp:TheAccessory  Routine
    loc:comment = p_web.Translate('')
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:TheAccessory') & '_comment',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::Button:AddAccessories  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('Button:AddAccessories') & '_prompt',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('Button:AddAccessories') & '_prompt')

Validate::Button:AddAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('Button:AddAccessories',p_web.GetValue('NewValue'))
    do Value::Button:AddAccessories
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  p_web.SSV('tmp:TheJobAccessory',p_web.GSV('tmp:TheJobAccessory') & p_web.GSV('tmp:ShowAccessory'))
  p_web.SSV('tmp:ShowAccessory','')
  do Value::Button:AddAccessories
  do SendAlert
  do Value::tmp:ShowAccessory  !1
  do Value::tmp:TheAccessory  !1

Value::Button:AddAccessories  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('Button:AddAccessories') & '_value',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon(''&clip('AddAccessories')&'.disabled=true;sv(''Button:AddAccessories'',''newjobbooking_button:addaccessories_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','AddAccessories','Add Accessories','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('Button:AddAccessories') & '_value')

Comment::Button:AddAccessories  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('Button:AddAccessories') & '_comment',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::Button:RemoveAccessory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('Button:RemoveAccessory',p_web.GetValue('NewValue'))
    do Value::Button:RemoveAccessory
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  tmp:TheJobAccessory = p_web.GetSessionValue('tmp:TheJobAccessory') & ';'
  tmp:TheAccessory = '|;' & Clip(p_web.GetSessionValue('tmp:TheAccessory')) & ';'
  Loop x# = 1 To 1000
      pos# = Instring(Clip(tmp:TheAccessory),tmp:TheJobAccessory,1,1)
      If pos# > 0
          tmp:TheJobAccessory = Sub(tmp:TheJobAccessory,1,pos# - 1) & Sub(tmp:TheJobAccessory,pos# + Len(Clip(tmp:TheAccessory)),1000)
          Break
      End ! If pos# > 0#
  End ! Loop x# = 1 To 1000
  p_web.SetSessionValue('tmp:TheJobAccessory',Sub(tmp:TheJobAccessory,1,Len(Clip(tmp:TheJobAccessory)) - 1))
  p_web.SetSessionValue('tmp:TheAccessory','')
  do Value::Button:RemoveAccessory
  do SendAlert
  do Value::tmp:ShowAccessory  !1
  do Value::tmp:TheAccessory  !1

Value::Button:RemoveAccessory  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('Button:RemoveAccessory') & '_value',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon(''&clip('RemoveAccessory')&'.disabled=true;sv(''Button:RemoveAccessory'',''newjobbooking_button:removeaccessory_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','RemoveAccessory','Remove Accessory','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('Button:RemoveAccessory') & '_value')

Comment::Button:RemoveAccessory  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('Button:RemoveAccessory') & '_comment',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::TheBlank  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('TheBlank') & '_prompt',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('TheBlank') & '_prompt')

Validate::TheBlank  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('TheBlank',p_web.GetValue('NewValue'))
    do Value::TheBlank
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::TheBlank  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('TheBlank') & '_value',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('TheBlank') & '_value')

Comment::TheBlank  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('TheBlank') & '_comment',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::Button:RemoveAllAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('Button:RemoveAllAccessories',p_web.GetValue('NewValue'))
    do Value::Button:RemoveAllAccessories
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  p_web.SetSessionValue('tmp:TheJobAccessory','')
  p_web.SetSessionValue('tmp:TheAccessory','')
  p_web.SetSessionValue('tmp:ShowAccessory','')
  do Value::Button:RemoveAllAccessories
  do SendAlert
  do Value::tmp:ShowAccessory  !1
  do Value::tmp:TheAccessory  !1

Value::Button:RemoveAllAccessories  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('Button:RemoveAllAccessories') & '_value',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon(''&clip('RemoveAllAccessories')&'.disabled=true;sv(''Button:RemoveAllAccessories'',''newjobbooking_button:removeallaccessories_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','RemoveAllAccessories','Remove All Accessories','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('Button:RemoveAllAccessories') & '_value')

Comment::Button:RemoveAllAccessories  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('Button:RemoveAllAccessories') & '_comment',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendAccessories') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:AmendFaultCodes  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:AmendFaultCodes') & '_prompt',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Amend Fault Codes')
  If p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('tmp:AmendFaultCodes') & '_prompt')

Validate::tmp:AmendFaultCodes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:AmendFaultCodes',p_web.GetValue('NewValue'))
    tmp:AmendFaultCodes = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:AmendFaultCodes
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('tmp:AmendFaultCodes',p_web.GetValue('Value'))
    tmp:AmendFaultCodes = p_web.GetValue('Value')
  End
  do Value::tmp:AmendFaultCodes
  do SendAlert
  do Prompt::tmp:FaultCode_6
  do Value::tmp:FaultCode_6  !1
  do Comment::tmp:FaultCode_6
  do Prompt::tmp:FaultCode_5
  do Value::tmp:FaultCode_5  !1
  do Comment::tmp:FaultCode_5
  do Prompt::tmp:FaultCode_4
  do Value::tmp:FaultCode_4  !1
  do Comment::tmp:FaultCode_4
  do Prompt::tmp:FaultCode_3
  do Value::tmp:FaultCode_3  !1
  do Comment::tmp:FaultCode_3
  do Prompt::tmp:FaultCode_2
  do Value::tmp:FaultCode_2  !1
  do Comment::tmp:FaultCode_2
  do Prompt::tmp:FaultCode_1
  do Value::tmp:FaultCode_1  !1
  do Comment::tmp:FaultCode_1

Value::tmp:AmendFaultCodes  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:AmendFaultCodes') & '_value',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0)
  ! --- CHECKBOX --- tmp:AmendFaultCodes
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:AmendFaultCodes'',''newjobbooking_tmp:amendfaultcodes_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:AmendFaultCodes')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GetSessionValue('Hide:AmendFaultCodes') = 1,'disabled','')
  If p_web.GetSessionValue('tmp:AmendFaultCodes') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:AmendFaultCodes',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('tmp:AmendFaultCodes') & '_value')

Comment::tmp:AmendFaultCodes  Routine
    loc:comment = p_web.Translate(p_web.GetSessionValue('Comment:AmendFaultCodes'))
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:AmendFaultCodes') & '_comment',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('tmp:AmendFaultCodes') & '_comment')

Validate::hidden6  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('hidden6',p_web.GetValue('NewValue'))
    do Value::hidden6
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::hidden6  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('hidden6') & '_value','adiv')
  loc:extra = ''
  ! --- HIDDEN --- 
    packet = clip(packet) & p_web.CreateInput('hidden','',p_web.GetSessionValue('')) & '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::hidden6  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('hidden6') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode_1  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_1') & '_prompt',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode1') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GetSessionValue('Prompt:FaultCode1'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode1') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_1') & '_prompt')

Validate::tmp:FaultCode_1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode_1',p_web.GetValue('NewValue'))
    tmp:FaultCode1 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode_1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode1',p_web.GetValue('Value'))
    tmp:FaultCode1 = p_web.GetValue('Value')
  End
  If tmp:FaultCode1 = ''
    loc:Invalid = 'tmp:FaultCode1'
    loc:alert = p_web.translate(p_web.GetSessionValue('Prompt:FaultCode1')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  local.FaultCode(p_web.GetSessionValue('Prompt:FaultCode1'),1)
  p_Web.SetValue('lookupfield','tmp:FaultCode_1')
  do AfterLookup
  do Value::tmp:FaultCode_1
  do SendAlert
  do Comment::tmp:FaultCode_1
  do Comment::tmp:FaultCode_1
  do Value::jbn:Fault_Description  !1

Value::tmp:FaultCode_1  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_1') & '_value',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode1') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode1') = 1)
  ! --- STRING --- tmp:FaultCode1
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('tmp:FaultCode1')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode1 = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode_1'',''newjobbooking_tmp:faultcode_1_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:FaultCode1')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode1',p_web.GetSessionValueFormat('tmp:FaultCode1'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectFaultCodes?FaultNumber=1')&'?LookupField=tmp:FaultCode1&Tab=8&ForeignField=mfo:Field&_sort=&Refresh=sort&LookupFrom=NewJobBooking&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_1') & '_value')

Comment::tmp:FaultCode_1  Routine
    loc:comment = p_web.Translate(p_web.GetSessionValue('Comment:FaultCode1'))
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_1') & '_comment',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode1') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode1') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_1') & '_comment')

Prompt::tmp:FaultCode_2  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_2') & '_prompt',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode2') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GetSessionValue('Prompt:FaultCode2'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode2') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_2') & '_prompt')

Validate::tmp:FaultCode_2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode_2',p_web.GetValue('NewValue'))
    tmp:FaultCode2 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode_2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode2',p_web.GetValue('Value'))
    tmp:FaultCode2 = p_web.GetValue('Value')
  End
  If tmp:FaultCode2 = ''
    loc:Invalid = 'tmp:FaultCode2'
    loc:alert = p_web.translate(p_web.GetSessionValue('Prompt:FaultCode2')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  local.FaultCode(p_web.GetSessionValue('Prompt:FaultCode2'),2)
  p_Web.SetValue('lookupfield','tmp:FaultCode_2')
  do AfterLookup
  do Value::tmp:FaultCode_2
  do SendAlert
  do Comment::tmp:FaultCode_2
  do Comment::tmp:FaultCode_2
  do Value::jbn:Fault_Description  !1

Value::tmp:FaultCode_2  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_2') & '_value',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode2') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode2') = 1)
  ! --- STRING --- tmp:FaultCode2
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('tmp:FaultCode2')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode2 = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode_2'',''newjobbooking_tmp:faultcode_2_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:FaultCode2')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode2',p_web.GetSessionValueFormat('tmp:FaultCode2'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectFaultCodes?FaultNumber=2')&'?LookupField=tmp:FaultCode2&Tab=8&ForeignField=mfo:Field&_sort=&Refresh=sort&LookupFrom=NewJobBooking&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_2') & '_value')

Comment::tmp:FaultCode_2  Routine
    loc:comment = p_web.Translate(p_web.GetSessionValue('Comment:FaultCode2'))
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_2') & '_comment',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode2') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode2') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_2') & '_comment')

Prompt::tmp:FaultCode_3  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_3') & '_prompt',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode3') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GetSessionValue('Prompt:FaultCode3'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode3') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_3') & '_prompt')

Validate::tmp:FaultCode_3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode_3',p_web.GetValue('NewValue'))
    tmp:FaultCode3 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode_3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode3',p_web.GetValue('Value'))
    tmp:FaultCode3 = p_web.GetValue('Value')
  End
  If tmp:FaultCode3 = ''
    loc:Invalid = 'tmp:FaultCode3'
    loc:alert = p_web.translate(p_web.GetSessionValue('Prompt:FaultCode3')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  local.FaultCode(p_web.GetSessionValue('Prompt:FaultCode3'),3)
  p_Web.SetValue('lookupfield','tmp:FaultCode_3')
  do AfterLookup
  do Value::tmp:FaultCode_3
  do SendAlert
  do Comment::tmp:FaultCode_3
  do Comment::tmp:FaultCode_3
  do Value::jbn:Fault_Description  !1

Value::tmp:FaultCode_3  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_3') & '_value',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode3') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode3') = 1)
  ! --- STRING --- tmp:FaultCode3
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('tmp:FaultCode3')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode3 = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode_3'',''newjobbooking_tmp:faultcode_3_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:FaultCode3')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode3',p_web.GetSessionValueFormat('tmp:FaultCode3'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectFaultCodes?FaultNumber=3')&'?LookupField=tmp:FaultCode3&Tab=8&ForeignField=mfo:Field&_sort=&Refresh=sort&LookupFrom=NewJobBooking&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_3') & '_value')

Comment::tmp:FaultCode_3  Routine
    loc:comment = p_web.Translate(p_web.GetSessionValue('Comment:FaultCode3'))
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_3') & '_comment',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode3') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode3') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_3') & '_comment')

Prompt::tmp:FaultCode_4  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_4') & '_prompt',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode4') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GetSessionValue('Prompt:FaultCode4'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode4') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_4') & '_prompt')

Validate::tmp:FaultCode_4  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode_4',p_web.GetValue('NewValue'))
    tmp:FaultCode4 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode_4
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode4',p_web.GetValue('Value'))
    tmp:FaultCode4 = p_web.GetValue('Value')
  End
  If tmp:FaultCode4 = ''
    loc:Invalid = 'tmp:FaultCode4'
    loc:alert = p_web.translate(p_web.GetSessionValue('Prompt:FaultCode4')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  local.FaultCode(p_web.GetSessionValue('Prompt:FaultCode4'),4)
  p_Web.SetValue('lookupfield','tmp:FaultCode_4')
  do AfterLookup
  do Value::tmp:FaultCode_4
  do SendAlert
  do Comment::tmp:FaultCode_4
  do Comment::tmp:FaultCode_4
  do Value::jbn:Fault_Description  !1

Value::tmp:FaultCode_4  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_4') & '_value',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode4') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode4') = 1)
  ! --- STRING --- tmp:FaultCode4
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('tmp:FaultCode4')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode4 = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode_4'',''newjobbooking_tmp:faultcode_4_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:FaultCode4')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode4',p_web.GetSessionValueFormat('tmp:FaultCode4'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectFaultCodes?FaultNumber=4')&'?LookupField=tmp:FaultCode4&Tab=8&ForeignField=mfo:Field&_sort=&Refresh=sort&LookupFrom=NewJobBooking&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_4') & '_value')

Comment::tmp:FaultCode_4  Routine
    loc:comment = p_web.Translate(p_web.GetSessionValue('Comment:FaultCode4'))
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_4') & '_comment',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode4') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode4') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_4') & '_comment')

Prompt::tmp:FaultCode_5  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_5') & '_prompt',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode5') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GetSessionValue('Prompt:FaultCode5'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode5') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_5') & '_prompt')

Validate::tmp:FaultCode_5  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode_5',p_web.GetValue('NewValue'))
    tmp:FaultCode5 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode_5
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode5',p_web.GetValue('Value'))
    tmp:FaultCode5 = p_web.GetValue('Value')
  End
  If tmp:FaultCode5 = ''
    loc:Invalid = 'tmp:FaultCode5'
    loc:alert = p_web.translate(p_web.GetSessionValue('Prompt:FaultCode5')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  local.FaultCode(p_web.GetSessionValue('Prompt:FaultCode5'),5)
  p_Web.SetValue('lookupfield','tmp:FaultCode_5')
  do AfterLookup
  do Value::tmp:FaultCode_5
  do SendAlert
  do Comment::tmp:FaultCode_5
  do Comment::tmp:FaultCode_5
  do Value::jbn:Fault_Description  !1

Value::tmp:FaultCode_5  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_5') & '_value',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode5') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode5') = 1)
  ! --- STRING --- tmp:FaultCode5
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('tmp:FaultCode5')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode5 = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode_5'',''newjobbooking_tmp:faultcode_5_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:FaultCode5')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode5',p_web.GetSessionValueFormat('tmp:FaultCode5'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectFaultCodes?FaultNumber=5')&'?LookupField=tmp:FaultCode5&Tab=8&ForeignField=mfo:Field&_sort=&Refresh=sort&LookupFrom=NewJobBooking&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_5') & '_value')

Comment::tmp:FaultCode_5  Routine
    loc:comment = p_web.Translate(p_web.GetSessionValue('Comment:FaultCode5'))
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_5') & '_comment',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode5') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode5') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_5') & '_comment')

Prompt::tmp:FaultCode_6  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_6') & '_prompt',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode6') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GetSessionValue('Prompt:FaultCode6'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode6') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_6') & '_prompt')

Validate::tmp:FaultCode_6  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode_6',p_web.GetValue('NewValue'))
    tmp:FaultCode6 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode_6
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode6',p_web.GetValue('Value'))
    tmp:FaultCode6 = p_web.GetValue('Value')
  End
  If tmp:FaultCode6 = ''
    loc:Invalid = 'tmp:FaultCode6'
    loc:alert = p_web.translate(p_web.GetSessionValue('Prompt:FaultCode6')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  local.FaultCode(p_web.GetSessionValue('Prompt:FaultCode6'),6)
  p_Web.SetValue('lookupfield','tmp:FaultCode_6')
  do AfterLookup
  do Value::tmp:FaultCode_6
  do SendAlert
  do Comment::tmp:FaultCode_6
  do Comment::tmp:FaultCode_6
  do Value::jbn:Fault_Description  !1

Value::tmp:FaultCode_6  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_6') & '_value',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode6') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode6') = 1)
  ! --- STRING --- tmp:FaultCode6
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('tmp:FaultCode6')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode6 = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode_6'',''newjobbooking_tmp:faultcode_6_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:FaultCode6')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode6',p_web.GetSessionValueFormat('tmp:FaultCode6'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectFaultCodes?FaultNumber=6')&'?LookupField=tmp:FaultCode6&Tab=8&ForeignField=mfo:Field&_sort=&Refresh=sort&LookupFrom=NewJobBooking&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_6') & '_value')

Comment::tmp:FaultCode_6  Routine
    loc:comment = p_web.Translate(p_web.GetSessionValue('Comment:FaultCode6'))
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_6') & '_comment',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode6') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode6') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('tmp:FaultCode_6') & '_comment')

Prompt::job:Engineer  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Engineer') & '_prompt',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Engineer')
  If p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Engineer  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Engineer',p_web.GetValue('NewValue'))
    job:Engineer = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Engineer
    do Value::job:Engineer
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Engineer',p_web.dFormat(p_web.GetValue('Value'),'@s3'))
    job:Engineer = p_web.GetValue('Value')
  End
  If job:Engineer = '' and p_web.GetSessionValue('Required:Engineer') = 1
    loc:Invalid = 'job:Engineer'
    loc:alert = p_web.translate('Engineer') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  p_Web.SetValue('lookupfield','job:Engineer')
  do AfterLookup
  do Value::job:Engineer
  do SendAlert
  do Comment::job:Engineer

Value::job:Engineer  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Engineer') & '_value',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0)
  ! --- STRING --- job:Engineer
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If (p_web.GetSessionValue('Required:Engineer') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('job:Engineer')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If job:Engineer = '' and (p_web.GetSessionValue('Required:Engineer') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Engineer'',''newjobbooking_job:engineer_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Engineer')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','job:Engineer',p_web.GetSessionValue('job:Engineer'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s3',loc:javascript,p_web.PicLength('@s3'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectEngineers')&'?LookupField=job:Engineer&Tab=8&ForeignField=use:User_Code&_sort=&Refresh=sort&LookupFrom=NewJobBooking&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Engineer') & '_value')

Comment::job:Engineer  Routine
    loc:comment = p_web.Translate(p_web.GetSessionValue('Comment:Engineer'))
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('job:Engineer') & '_comment',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('job:Engineer') & '_comment')

Validate::hidden7  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('hidden7',p_web.GetValue('NewValue'))
    do Value::hidden7
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::hidden7  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('hidden7') & '_value','adiv')
  loc:extra = ''
  ! --- HIDDEN --- 
    packet = clip(packet) & p_web.CreateInput('hidden','',p_web.GetSessionValue('')) & '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::hidden7  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('hidden7') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jbn:Fault_Description  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jbn:Fault_Description') & '_prompt',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Fault Description')
  If p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jbn:Fault_Description  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jbn:Fault_Description',p_web.GetValue('NewValue'))
    jbn:Fault_Description = p_web.GetValue('NewValue') !FieldType= STRING Field = jbn:Fault_Description
    do Value::jbn:Fault_Description
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jbn:Fault_Description',p_web.dFormat(p_web.GetValue('Value'),'@s255'))
    jbn:Fault_Description = p_web.GetValue('Value')
  End
  If jbn:Fault_Description = '' and p_web.GetSessionValue('Required:FaultDescription')
    loc:Invalid = 'jbn:Fault_Description'
    loc:alert = 'Fault Description Required'
  End
    jbn:Fault_Description = Upper(jbn:Fault_Description)
    p_web.SetSessionValue('jbn:Fault_Description',jbn:Fault_Description)
  !Work Around For Blank
  if (sub(p_web.GSV('jbn:Fault_Description'),1,1) = ' ')
      p_web.SSV('jbn:Fault_Description',sub(p_web.GSV('jbn:Fault_Description'),2,256))
  end ! if (sub(p_web.GSV('jbn:Engineers_Notes'),1,1) = ' ')
  do Value::jbn:Fault_Description
  do SendAlert

Value::jbn:Fault_Description  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jbn:Fault_Description') & '_value',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0)
  ! --- TEXT --- jbn:Fault_Description
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If (p_web.GetSessionValue('Required:FaultDescription'))
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('jbn:Fault_Description')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If jbn:Fault_Description = '' and (p_web.GetSessionValue('Required:FaultDescription'))
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:Fault_Description'',''newjobbooking_jbn:fault_description_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jbn:Fault_Description')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('jbn:Fault_Description',p_web.GetSessionValue('jbn:Fault_Description'),3,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(jbn:Fault_Description),,Net:Web:Control) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jbn:Fault_Description') & '_value')

Comment::jbn:Fault_Description  Routine
    If p_web.GetSessionValue('Required:FaultDescription')
      loc:comment = p_web.translate(p_web.site.RequiredText)
    End
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jbn:Fault_Description') & '_comment',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jbn:Engineers_Notes  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jbn:Engineers_Notes') & '_prompt',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Engineers Notes:')
  If p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jbn:Engineers_Notes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jbn:Engineers_Notes',p_web.GetValue('NewValue'))
    jbn:Engineers_Notes = p_web.GetValue('NewValue') !FieldType= STRING Field = jbn:Engineers_Notes
    do Value::jbn:Engineers_Notes
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jbn:Engineers_Notes',p_web.dFormat(p_web.GetValue('Value'),'@s255'))
    jbn:Engineers_Notes = p_web.GetValue('Value')
  End
    jbn:Engineers_Notes = Upper(jbn:Engineers_Notes)
    p_web.SetSessionValue('jbn:Engineers_Notes',jbn:Engineers_Notes)
  !Work Around For Blank
  if (sub(p_web.GSV('jbn:Engineers_Notes'),1,1) = ' ')
      p_web.SSV('jbn:Engineers_Notes',sub(p_web.GSV('jbn:Engineers_Notes'),2,256))
  end ! if (sub(p_web.GSV('jbn:Engineers_Notes'),1,1) = ' ')
  do Value::jbn:Engineers_Notes
  do SendAlert

Value::jbn:Engineers_Notes  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jbn:Engineers_Notes') & '_value',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0)
  ! --- TEXT --- jbn:Engineers_Notes
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jbn:Engineers_Notes')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:Engineers_Notes'',''newjobbooking_jbn:engineers_notes_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jbn:Engineers_Notes')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('jbn:Engineers_Notes',p_web.GetSessionValue('jbn:Engineers_Notes'),3,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(jbn:Engineers_Notes),,Net:Web:Control) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jbn:Engineers_Notes') & '_value')

Comment::jbn:Engineers_Notes  Routine
      loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jbn:Engineers_Notes') & '_comment',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::__Line4  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('__Line4',p_web.GetValue('NewValue'))
    do Value::__Line4
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::__Line4  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('__Line4') & '_value',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1,'hdiv',''))
  loc:extra = ''
  If Not (p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('__Line4') & '_value')

Comment::__Line4  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('__Line4') & '_comment',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ExternalDamageChecklist  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:ExternalDamageChecklist') & '_prompt',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('External Damage Checklist')
  If p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:ExternalDamageChecklist  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ExternalDamageChecklist',p_web.GetValue('NewValue'))
    tmp:ExternalDamageChecklist = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ExternalDamageChecklist
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('tmp:ExternalDamageChecklist',p_web.GetValue('Value'))
    tmp:ExternalDamageChecklist = p_web.GetValue('Value')
  End
  do Value::tmp:ExternalDamageChecklist
  do SendAlert
  do Value::__Line4  !1
  do Value::__Line5  !1
  do Prompt::jobe2:XNone
  do Value::jobe2:XNone  !1
  do Comment::jobe2:XNone
  do Prompt::jobe2:XAntenna
  do Value::jobe2:XAntenna  !1
  do Comment::jobe2:XAntenna
  do Prompt::jobe2:XBCover
  do Value::jobe2:XBCover  !1
  do Comment::jobe2:XBCover
  do Prompt::jobe2:XBattery
  do Value::jobe2:XBattery  !1
  do Comment::jobe2:XBattery
  do Prompt::jobe2:XCharger
  do Value::jobe2:XCharger  !1
  do Comment::jobe2:XCharger
  do Prompt::jobe2:XFCover
  do Value::jobe2:XFCover  !1
  do Comment::jobe2:XFCover
  do Prompt::jobe2:XKeypad
  do Value::jobe2:XKeypad  !1
  do Comment::jobe2:XKeypad
  do Prompt::jobe2:XKeypad
  do Value::jobe2:XKeypad  !1
  do Comment::jobe2:XKeypad
  do Prompt::jobe2:XLCD
  do Value::jobe2:XLCD  !1
  do Comment::jobe2:XLCD
  do Prompt::jobe2:XLens
  do Value::jobe2:XLens  !1
  do Comment::jobe2:XLens
  do Prompt::jobe2:XNotes
  do Value::jobe2:XNotes  !1
  do Comment::jobe2:XNotes
  do Prompt::jobe2:XSimReader
  do Value::jobe2:XSimReader  !1
  do Comment::jobe2:XSimReader
  do Prompt::jobe2:XSystemConnector
  do Value::jobe2:XSystemConnector  !1
  do Comment::jobe2:XSystemConnector

Value::tmp:ExternalDamageChecklist  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:ExternalDamageChecklist') & '_value',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0)
  ! --- CHECKBOX --- tmp:ExternalDamageChecklist
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:ExternalDamageChecklist'',''newjobbooking_tmp:externaldamagechecklist_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ExternalDamageChecklist')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('tmp:ExternalDamageChecklist') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:ExternalDamageChecklist',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('tmp:ExternalDamageChecklist') & '_value')

Comment::tmp:ExternalDamageChecklist  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('tmp:ExternalDamageChecklist') & '_comment',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe2:XNone  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XNone') & '_prompt',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('None')
  If p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XNone') & '_prompt')

Validate::jobe2:XNone  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:XNone',p_web.GetValue('NewValue'))
    jobe2:XNone = p_web.GetValue('NewValue') !FieldType= BYTE Field = jobe2:XNone
    do Value::jobe2:XNone
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('jobe2:XNone',p_web.GetValue('Value'))
    jobe2:XNone = p_web.GetValue('Value')
  End
  do Value::jobe2:XNone
  do SendAlert
  do Prompt::jobe2:XAntenna
  do Value::jobe2:XAntenna  !1
  do Comment::jobe2:XAntenna
  do Prompt::jobe2:XBCover
  do Value::jobe2:XBCover  !1
  do Comment::jobe2:XBCover
  do Prompt::jobe2:XCharger
  do Value::jobe2:XCharger  !1
  do Comment::jobe2:XCharger
  do Prompt::jobe2:XFCover
  do Value::jobe2:XFCover  !1
  do Comment::jobe2:XFCover
  do Prompt::jobe2:XKeypad
  do Value::jobe2:XKeypad  !1
  do Comment::jobe2:XKeypad
  do Prompt::jobe2:XLCD
  do Value::jobe2:XLCD  !1
  do Comment::jobe2:XLCD
  do Prompt::jobe2:XLens
  do Value::jobe2:XLens  !1
  do Comment::jobe2:XLens
  do Prompt::jobe2:XNotes
  do Value::jobe2:XNotes  !1
  do Comment::jobe2:XNotes
  do Prompt::jobe2:XSimReader
  do Value::jobe2:XSimReader  !1
  do Comment::jobe2:XSimReader
  do Prompt::jobe2:XSystemConnector
  do Value::jobe2:XSystemConnector  !1
  do Comment::jobe2:XSystemConnector
  do Prompt::jobe2:XBattery
  do Value::jobe2:XBattery  !1
  do Comment::jobe2:XBattery

Value::jobe2:XNone  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XNone') & '_value',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1)
  ! --- CHECKBOX --- jobe2:XNone
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe2:XNone'',''newjobbooking_jobe2:xnone_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:XNone')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('jobe2:XNone') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','jobe2:XNone',clip(1),,loc:readonly,,,loc:javascript,,'None') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XNone') & '_value')

Comment::jobe2:XNone  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XNone') & '_comment',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XNone') & '_comment')

Prompt::jobe2:XAntenna  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XAntenna') & '_prompt',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Antenna')
  If p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XAntenna') & '_prompt')

Validate::jobe2:XAntenna  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:XAntenna',p_web.GetValue('NewValue'))
    jobe2:XAntenna = p_web.GetValue('NewValue') !FieldType= BYTE Field = jobe2:XAntenna
    do Value::jobe2:XAntenna
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('jobe2:XAntenna',p_web.GetValue('Value'))
    jobe2:XAntenna = p_web.GetValue('Value')
  End
  do Value::jobe2:XAntenna
  do SendAlert

Value::jobe2:XAntenna  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XAntenna') & '_value',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1)
  ! --- CHECKBOX --- jobe2:XAntenna
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe2:XAntenna'',''newjobbooking_jobe2:xantenna_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('jobe2:XAntenna') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','jobe2:XAntenna',clip(1),,loc:readonly,,,loc:javascript,,'Antenna') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XAntenna') & '_value')

Comment::jobe2:XAntenna  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XAntenna') & '_comment',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XAntenna') & '_comment')

Prompt::jobe2:XLens  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XLens') & '_prompt',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Lens')
  If p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XLens') & '_prompt')

Validate::jobe2:XLens  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:XLens',p_web.GetValue('NewValue'))
    jobe2:XLens = p_web.GetValue('NewValue') !FieldType= BYTE Field = jobe2:XLens
    do Value::jobe2:XLens
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('jobe2:XLens',p_web.GetValue('Value'))
    jobe2:XLens = p_web.GetValue('Value')
  End
  do Value::jobe2:XLens
  do SendAlert

Value::jobe2:XLens  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XLens') & '_value',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1)
  ! --- CHECKBOX --- jobe2:XLens
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe2:XLens'',''newjobbooking_jobe2:xlens_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('jobe2:XLens') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','jobe2:XLens',clip(1),,loc:readonly,,,loc:javascript,,'Lens') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XLens') & '_value')

Comment::jobe2:XLens  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XLens') & '_comment',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XLens') & '_comment')

Prompt::jobe2:XFCover  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XFCover') & '_prompt',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('F/Cover')
  If p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XFCover') & '_prompt')

Validate::jobe2:XFCover  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:XFCover',p_web.GetValue('NewValue'))
    jobe2:XFCover = p_web.GetValue('NewValue') !FieldType= BYTE Field = jobe2:XFCover
    do Value::jobe2:XFCover
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('jobe2:XFCover',p_web.GetValue('Value'))
    jobe2:XFCover = p_web.GetValue('Value')
  End
  do Value::jobe2:XFCover
  do SendAlert

Value::jobe2:XFCover  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XFCover') & '_value',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1)
  ! --- CHECKBOX --- jobe2:XFCover
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe2:XFCover'',''newjobbooking_jobe2:xfcover_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('jobe2:XFCover') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','jobe2:XFCover',clip(1),,loc:readonly,,,loc:javascript,,'F/Cover') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XFCover') & '_value')

Comment::jobe2:XFCover  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XFCover') & '_comment',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XFCover') & '_comment')

Prompt::jobe2:XBCover  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XBCover') & '_prompt',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('B/Cover')
  If p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XBCover') & '_prompt')

Validate::jobe2:XBCover  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:XBCover',p_web.GetValue('NewValue'))
    jobe2:XBCover = p_web.GetValue('NewValue') !FieldType= BYTE Field = jobe2:XBCover
    do Value::jobe2:XBCover
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('jobe2:XBCover',p_web.GetValue('Value'))
    jobe2:XBCover = p_web.GetValue('Value')
  End
  do Value::jobe2:XBCover
  do SendAlert

Value::jobe2:XBCover  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XBCover') & '_value',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1)
  ! --- CHECKBOX --- jobe2:XBCover
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe2:XBCover'',''newjobbooking_jobe2:xbcover_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('jobe2:XBCover') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','jobe2:XBCover',clip(1),,loc:readonly,,,loc:javascript,,'B/Cover') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XBCover') & '_value')

Comment::jobe2:XBCover  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XBCover') & '_comment',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XBCover') & '_comment')

Prompt::jobe2:XKeypad  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XKeypad') & '_prompt',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Keypad')
  If p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XKeypad') & '_prompt')

Validate::jobe2:XKeypad  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:XKeypad',p_web.GetValue('NewValue'))
    jobe2:XKeypad = p_web.GetValue('NewValue') !FieldType= BYTE Field = jobe2:XKeypad
    do Value::jobe2:XKeypad
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('jobe2:XKeypad',p_web.GetValue('Value'))
    jobe2:XKeypad = p_web.GetValue('Value')
  End
  do Value::jobe2:XKeypad
  do SendAlert

Value::jobe2:XKeypad  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XKeypad') & '_value',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1)
  ! --- CHECKBOX --- jobe2:XKeypad
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe2:XKeypad'',''newjobbooking_jobe2:xkeypad_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('jobe2:XKeypad') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','jobe2:XKeypad',clip(1),,loc:readonly,,,loc:javascript,,'Keypad') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XKeypad') & '_value')

Comment::jobe2:XKeypad  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XKeypad') & '_comment',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XKeypad') & '_comment')

Prompt::jobe2:XBattery  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XBattery') & '_prompt',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Battery')
  If p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XBattery') & '_prompt')

Validate::jobe2:XBattery  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:XBattery',p_web.GetValue('NewValue'))
    jobe2:XBattery = p_web.GetValue('NewValue') !FieldType= BYTE Field = jobe2:XBattery
    do Value::jobe2:XBattery
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('jobe2:XBattery',p_web.GetValue('Value'))
    jobe2:XBattery = p_web.GetValue('Value')
  End
  do Value::jobe2:XBattery
  do SendAlert

Value::jobe2:XBattery  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XBattery') & '_value',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1)
  ! --- CHECKBOX --- jobe2:XBattery
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe2:XBattery'',''newjobbooking_jobe2:xbattery_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('jobe2:XBattery') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','jobe2:XBattery',clip(1),,loc:readonly,,,loc:javascript,,'Battery') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XBattery') & '_value')

Comment::jobe2:XBattery  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XBattery') & '_comment',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XBattery') & '_comment')

Prompt::jobe2:XCharger  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XCharger') & '_prompt',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Charger')
  If p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XCharger') & '_prompt')

Validate::jobe2:XCharger  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:XCharger',p_web.GetValue('NewValue'))
    jobe2:XCharger = p_web.GetValue('NewValue') !FieldType= BYTE Field = jobe2:XCharger
    do Value::jobe2:XCharger
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('jobe2:XCharger',p_web.GetValue('Value'))
    jobe2:XCharger = p_web.GetValue('Value')
  End
  do Value::jobe2:XCharger
  do SendAlert

Value::jobe2:XCharger  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XCharger') & '_value',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1)
  ! --- CHECKBOX --- jobe2:XCharger
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe2:XCharger'',''newjobbooking_jobe2:xcharger_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('jobe2:XCharger') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','jobe2:XCharger',clip(1),,loc:readonly,,,loc:javascript,,'Charger') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XCharger') & '_value')

Comment::jobe2:XCharger  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XCharger') & '_comment',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XCharger') & '_comment')

Prompt::jobe2:XLCD  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XLCD') & '_prompt',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('LCD')
  If p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XLCD') & '_prompt')

Validate::jobe2:XLCD  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:XLCD',p_web.GetValue('NewValue'))
    jobe2:XLCD = p_web.GetValue('NewValue') !FieldType= BYTE Field = jobe2:XLCD
    do Value::jobe2:XLCD
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('jobe2:XLCD',p_web.GetValue('Value'))
    jobe2:XLCD = p_web.GetValue('Value')
  End
  do Value::jobe2:XLCD
  do SendAlert

Value::jobe2:XLCD  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XLCD') & '_value',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1)
  ! --- CHECKBOX --- jobe2:XLCD
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe2:XLCD'',''newjobbooking_jobe2:xlcd_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('jobe2:XLCD') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','jobe2:XLCD',clip(1),,loc:readonly,,,loc:javascript,,'LCD') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XLCD') & '_value')

Comment::jobe2:XLCD  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XLCD') & '_comment',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XLCD') & '_comment')

Prompt::jobe2:XSimReader  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XSimReader') & '_prompt',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('SIM Reader')
  If p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XSimReader') & '_prompt')

Validate::jobe2:XSimReader  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:XSimReader',p_web.GetValue('NewValue'))
    jobe2:XSimReader = p_web.GetValue('NewValue') !FieldType= BYTE Field = jobe2:XSimReader
    do Value::jobe2:XSimReader
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('jobe2:XSimReader',p_web.GetValue('Value'))
    jobe2:XSimReader = p_web.GetValue('Value')
  End
  do Value::jobe2:XSimReader
  do SendAlert

Value::jobe2:XSimReader  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XSimReader') & '_value',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1)
  ! --- CHECKBOX --- jobe2:XSimReader
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe2:XSimReader'',''newjobbooking_jobe2:xsimreader_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('jobe2:XSimReader') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','jobe2:XSimReader',clip(1),,loc:readonly,,,loc:javascript,,'Sim Reader') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XSimReader') & '_value')

Comment::jobe2:XSimReader  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XSimReader') & '_comment',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XSimReader') & '_comment')

Prompt::jobe2:XSystemConnector  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XSystemConnector') & '_prompt',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('System Connector')
  If p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XSystemConnector') & '_prompt')

Validate::jobe2:XSystemConnector  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:XSystemConnector',p_web.GetValue('NewValue'))
    jobe2:XSystemConnector = p_web.GetValue('NewValue') !FieldType= BYTE Field = jobe2:XSystemConnector
    do Value::jobe2:XSystemConnector
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('jobe2:XSystemConnector',p_web.GetValue('Value'))
    jobe2:XSystemConnector = p_web.GetValue('Value')
  End
  do Value::jobe2:XSystemConnector
  do SendAlert

Value::jobe2:XSystemConnector  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XSystemConnector') & '_value',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1)
  ! --- CHECKBOX --- jobe2:XSystemConnector
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe2:XSystemConnector'',''newjobbooking_jobe2:xsystemconnector_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('jobe2:XSystemConnector') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','jobe2:XSystemConnector',clip(1),,loc:readonly,,,loc:javascript,,'System Connector') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XSystemConnector') & '_value')

Comment::jobe2:XSystemConnector  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XSystemConnector') & '_comment',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XSystemConnector') & '_comment')

Prompt::jobe2:XNotes  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XNotes') & '_prompt',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Notes')
  If p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XNotes') & '_prompt')

Validate::jobe2:XNotes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:XNotes',p_web.GetValue('NewValue'))
    jobe2:XNotes = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe2:XNotes
    do Value::jobe2:XNotes
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe2:XNotes',p_web.dFormat(p_web.GetValue('Value'),'@s255'))
    jobe2:XNotes = p_web.GetValue('Value')
  End
    jobe2:XNotes = Upper(jobe2:XNotes)
    p_web.SetSessionValue('jobe2:XNotes',jobe2:XNotes)
  !Work Around For Blank
  if (sub(p_web.GSV('jobe2:XNotes'),1,1) = ' ')
      p_web.SSV('jobe2:XNotes',sub(p_web.GSV('jobe2:XNotes'),2,256))
  end ! if (sub(p_web.GSV('jbn:Engineers_Notes'),1,1) = ' ')
  do Value::jobe2:XNotes
  do SendAlert

Value::jobe2:XNotes  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XNotes') & '_value',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1)
  ! --- TEXT --- jobe2:XNotes
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jobe2:XNotes')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:XNotes'',''newjobbooking_jobe2:xnotes_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:XNotes')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('jobe2:XNotes',p_web.GetSessionValue('jobe2:XNotes'),3,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(jobe2:XNotes),'Notes',Net:Web:Control) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XNotes') & '_value')

Comment::jobe2:XNotes  Routine
      loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:XNotes') & '_comment',Choose(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:XNotes') & '_comment')

Validate::__Line5  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('__Line5',p_web.GetValue('NewValue'))
    do Value::__Line5
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::__Line5  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('__Line5') & '_value',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv',''))
  loc:extra = ''
  If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('__Line5') & '_value')

Comment::__Line5  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('__Line5') & '_comment',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe:Booking48HourOption  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe:Booking48HourOption') & '_prompt',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Booking Option')
  If p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe:Booking48HourOption') & '_prompt')

Validate::jobe:Booking48HourOption  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe:Booking48HourOption',p_web.GetValue('NewValue'))
    jobe:Booking48HourOption = p_web.GetValue('NewValue') !FieldType= BYTE Field = jobe:Booking48HourOption
    do Value::jobe:Booking48HourOption
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe:Booking48HourOption',p_web.dFormat(p_web.GetValue('Value'),'@n1'))
    jobe:Booking48HourOption = p_web.GetValue('Value')
  End
  do Value::jobe:Booking48HourOption
  do SendAlert

Value::jobe:Booking48HourOption  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe:Booking48HourOption') & '_value',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0)
  ! --- RADIO --- jobe:Booking48HourOption
  loc:fieldclass = Choose(sub(' noBorder',1,1) = ' ',clip('FormEntry') & ' noBorder',' noBorder')
  If lower(loc:invalid) = lower('jobe:Booking48HourOption')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  if p_web.GetSessionValue('Hide:48Hour') <> 1
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('jobe:Booking48HourOption') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe:Booking48HourOption'',''newjobbooking_jobe:booking48houroption_value'',1,'''&clip(1)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','jobe:Booking48HourOption',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,'48 Hour Option (Booking)','jobe:Booking48HourOption_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('48 Hour Exch') & '<13,10>'
    packet = clip(packet) & p_web.br
  end
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('jobe:Booking48HourOption') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe:Booking48HourOption'',''newjobbooking_jobe:booking48houroption_value'',1,'''&clip(2)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','jobe:Booking48HourOption',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,'48 Hour Option (Booking)','jobe:Booking48HourOption_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('ARC Repair') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('jobe:Booking48HourOption') = 3
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe:Booking48HourOption'',''newjobbooking_jobe:booking48houroption_value'',1,'''&clip(3)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','jobe:Booking48HourOption',clip(3),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,'48 Hour Option (Booking)','jobe:Booking48HourOption_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('7 Day TAT') & '<13,10>'
    packet = clip(packet) & p_web.br
  if p_web.GSV('Hide:LiquidDamage') <> 1
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('jobe:Booking48HourOption') = 4
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe:Booking48HourOption'',''newjobbooking_jobe:booking48houroption_value'',1,'''&clip(4)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','jobe:Booking48HourOption',clip(4),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,'48 Hour Option (Booking)','jobe:Booking48HourOption_4') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Liquid Damage') & '<13,10>'
    packet = clip(packet) & p_web.br
  end
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe:Booking48HourOption') & '_value')

Comment::jobe:Booking48HourOption  Routine
    loc:comment = p_web.Translate('Required')
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe:Booking48HourOption') & '_comment',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe:Booking48HourOption') & '_comment')

Prompt::jobe:VSACustomer  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe:VSACustomer') & '_prompt',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('VSA Customer')
  If p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe:VSACustomer  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe:VSACustomer',p_web.GetValue('NewValue'))
    jobe:VSACustomer = p_web.GetValue('NewValue') !FieldType= BYTE Field = jobe:VSACustomer
    do Value::jobe:VSACustomer
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('jobe:VSACustomer',p_web.GetValue('Value'))
    jobe:VSACustomer = p_web.GetValue('Value')
  End
  do Value::jobe:VSACustomer
  do SendAlert

Value::jobe:VSACustomer  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe:VSACustomer') & '_value',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0)
  ! --- CHECKBOX --- jobe:VSACustomer
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe:VSACustomer'',''newjobbooking_jobe:vsacustomer_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('jobe:VSACustomer') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','jobe:VSACustomer',clip(1),,loc:readonly,,,loc:javascript,,'VSA Customer') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe:VSACustomer') & '_value')

Comment::jobe:VSACustomer  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe:VSACustomer') & '_comment',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe2:Contract  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:Contract') & '_prompt',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Contract')
  If p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe2:Contract  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:Contract',p_web.GetValue('NewValue'))
    jobe2:Contract = p_web.GetValue('NewValue') !FieldType= BYTE Field = jobe2:Contract
    do Value::jobe2:Contract
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('jobe2:Contract',p_web.GetValue('Value'))
    jobe2:Contract = p_web.GetValue('Value')
  End
  If p_web.GetSessionValue('jobe2:Contract') = 1
      p_web.SetSessionValue('jobe2:Prepaid',0)
  End ! If p_web.GetSessionValue('jobe2:Contract') = 1
  do Value::jobe2:Contract
  do SendAlert
  do Value::jobe2:Prepaid  !1

Value::jobe2:Contract  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:Contract') & '_value',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0)
  ! --- CHECKBOX --- jobe2:Contract
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe2:Contract'',''newjobbooking_jobe2:contract_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:Contract')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('jobe2:Contract') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','jobe2:Contract',clip(1),,loc:readonly,,,loc:javascript,,'Contract') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:Contract') & '_value')

Comment::jobe2:Contract  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:Contract') & '_comment',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe2:Prepaid  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:Prepaid') & '_prompt',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Prepaid')
  If p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe2:Prepaid  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:Prepaid',p_web.GetValue('NewValue'))
    jobe2:Prepaid = p_web.GetValue('NewValue') !FieldType= BYTE Field = jobe2:Prepaid
    do Value::jobe2:Prepaid
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('jobe2:Prepaid',p_web.GetValue('Value'))
    jobe2:Prepaid = p_web.GetValue('Value')
  End
  If p_web.GetSessionValue('jobe2:Prepaid') = 1
      p_web.SetSessionValue('jobe2:Contract',0)
  End ! If p_web.GetSessionValue('jobe2:Prepaid') = 1
  do Value::jobe2:Prepaid
  do SendAlert
  do Value::jobe2:Contract  !1

Value::jobe2:Prepaid  Routine
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:Prepaid') & '_value',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('IMEINumberAccepted') = 0)
  ! --- CHECKBOX --- jobe2:Prepaid
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe2:Prepaid'',''newjobbooking_jobe2:prepaid_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:Prepaid')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('jobe2:Prepaid') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','jobe2:Prepaid',clip(1),,loc:readonly,,,loc:javascript,,'Prepaid') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('NewJobBooking_' & p_web._nocolon('jobe2:Prepaid') & '_value')

Comment::jobe2:Prepaid  Routine
    loc:comment = ''
  p_web._DivHeader('NewJobBooking_' & p_web._nocolon('jobe2:Prepaid') & '_comment',Choose(p_web.GetSessionValue('IMEINumberAccepted') = 0,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('IMEINumberAccepted') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('NewJobBooking_tmp:PreviousAddress_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:PreviousAddress
      else
        do Value::tmp:PreviousAddress
      end
  of lower('NewJobBooking_tmp:UsePreviousAddress_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:UsePreviousAddress
      else
        do Value::tmp:UsePreviousAddress
      end
  of lower('NewJobBooking_jobe:Network_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:Network
      else
        do Value::jobe:Network
      end
  of lower('NewJobBooking_job:Mobile_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Mobile_Number
      else
        do Value::job:Mobile_Number
      end
  of lower('NewJobBooking_buttonMSISDNCheck_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonMSISDNCheck
      else
        do Value::buttonMSISDNCheck
      end
  of lower('NewJobBooking_job:Unit_Type_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Unit_Type
      else
        do Value::job:Unit_Type
      end
  of lower('NewJobBooking_job:ProductCode_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:ProductCode
      else
        do Value::job:ProductCode
      end
  of lower('NewJobBooking_job:Colour_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Colour
      else
        do Value::job:Colour
      end
  of lower('NewJobBooking_job:MSN_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:MSN
      else
        do Value::job:MSN
      end
  of lower('NewJobBooking_job:DOP_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:DOP
      else
        do Value::job:DOP
      end
  of lower('NewJobBooking_job:POP_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:POP
      else
        do Value::job:POP
      end
  of lower('NewJobBooking_jobe2:POPConfirmed_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:POPConfirmed
      else
        do Value::jobe2:POPConfirmed
      end
  of lower('NewJobBooking_locPOPTypePassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locPOPTypePassword
      else
        do Value::locPOPTypePassword
      end
  of lower('NewJobBooking_tmp:POPType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:POPType
      else
        do Value::tmp:POPType
      end
  of lower('NewJobBooking_jobe2:WarrantyRefNo_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:WarrantyRefNo
      else
        do Value::jobe2:WarrantyRefNo
      end
  of lower('NewJobBooking_job:Chargeable_Job_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Chargeable_Job
      else
        do Value::job:Chargeable_Job
      end
  of lower('NewJobBooking_job:Charge_Type_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Charge_Type
      else
        do Value::job:Charge_Type
      end
  of lower('NewJobBooking_job:Warranty_Job_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Warranty_Job
      else
        do Value::job:Warranty_Job
      end
  of lower('NewJobBooking_job:Warranty_Charge_Type_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Warranty_Charge_Type
      else
        do Value::job:Warranty_Charge_Type
      end
  of lower('NewJobBooking_job:Authority_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Authority_Number
      else
        do Value::job:Authority_Number
      end
  of lower('NewJobBooking_FranchiseAccount_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::FranchiseAccount
      else
        do Value::FranchiseAccount
      end
  of lower('NewJobBooking_job:Account_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Account_Number
      else
        do Value::job:Account_Number
      end
  of lower('NewJobBooking_job:Account_Number2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Account_Number2
      else
        do Value::job:Account_Number2
      end
  of lower('NewJobBooking_job:Order_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Order_Number
      else
        do Value::job:Order_Number
      end
  of lower('NewJobBooking_job:Title_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Title
      else
        do Value::job:Title
      end
  of lower('NewJobBooking_job:Initial_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Initial
      else
        do Value::job:Initial
      end
  of lower('NewJobBooking_job:Surname_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Surname
      else
        do Value::job:Surname
      end
  of lower('NewJobBooking_jobe:EndUserTelNo_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:EndUserTelNo
      else
        do Value::jobe:EndUserTelNo
      end
  of lower('NewJobBooking_job:Courier_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Courier
      else
        do Value::job:Courier
      end
  of lower('NewJobBooking_jobe2:CourierWaybillNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:CourierWaybillNumber
      else
        do Value::jobe2:CourierWaybillNumber
      end
  of lower('NewJobBooking_tmp:AmendAccessories_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:AmendAccessories
      else
        do Value::tmp:AmendAccessories
      end
  of lower('NewJobBooking_tmp:ShowAccessory_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ShowAccessory
      else
        do Value::tmp:ShowAccessory
      end
  of lower('NewJobBooking_tmp:TheAccessory_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:TheAccessory
      else
        do Value::tmp:TheAccessory
      end
  of lower('NewJobBooking_Button:AddAccessories_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::Button:AddAccessories
      else
        do Value::Button:AddAccessories
      end
  of lower('NewJobBooking_Button:RemoveAccessory_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::Button:RemoveAccessory
      else
        do Value::Button:RemoveAccessory
      end
  of lower('NewJobBooking_Button:RemoveAllAccessories_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::Button:RemoveAllAccessories
      else
        do Value::Button:RemoveAllAccessories
      end
  of lower('NewJobBooking_tmp:AmendFaultCodes_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:AmendFaultCodes
      else
        do Value::tmp:AmendFaultCodes
      end
  of lower('NewJobBooking_tmp:FaultCode_1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode_1
      else
        do Value::tmp:FaultCode_1
      end
  of lower('NewJobBooking_tmp:FaultCode_2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode_2
      else
        do Value::tmp:FaultCode_2
      end
  of lower('NewJobBooking_tmp:FaultCode_3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode_3
      else
        do Value::tmp:FaultCode_3
      end
  of lower('NewJobBooking_tmp:FaultCode_4_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode_4
      else
        do Value::tmp:FaultCode_4
      end
  of lower('NewJobBooking_tmp:FaultCode_5_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode_5
      else
        do Value::tmp:FaultCode_5
      end
  of lower('NewJobBooking_tmp:FaultCode_6_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode_6
      else
        do Value::tmp:FaultCode_6
      end
  of lower('NewJobBooking_job:Engineer_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Engineer
      else
        do Value::job:Engineer
      end
  of lower('NewJobBooking_jbn:Fault_Description_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:Fault_Description
      else
        do Value::jbn:Fault_Description
      end
  of lower('NewJobBooking_jbn:Engineers_Notes_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:Engineers_Notes
      else
        do Value::jbn:Engineers_Notes
      end
  of lower('NewJobBooking_tmp:ExternalDamageChecklist_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ExternalDamageChecklist
      else
        do Value::tmp:ExternalDamageChecklist
      end
  of lower('NewJobBooking_jobe2:XNone_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:XNone
      else
        do Value::jobe2:XNone
      end
  of lower('NewJobBooking_jobe2:XAntenna_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:XAntenna
      else
        do Value::jobe2:XAntenna
      end
  of lower('NewJobBooking_jobe2:XLens_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:XLens
      else
        do Value::jobe2:XLens
      end
  of lower('NewJobBooking_jobe2:XFCover_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:XFCover
      else
        do Value::jobe2:XFCover
      end
  of lower('NewJobBooking_jobe2:XBCover_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:XBCover
      else
        do Value::jobe2:XBCover
      end
  of lower('NewJobBooking_jobe2:XKeypad_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:XKeypad
      else
        do Value::jobe2:XKeypad
      end
  of lower('NewJobBooking_jobe2:XBattery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:XBattery
      else
        do Value::jobe2:XBattery
      end
  of lower('NewJobBooking_jobe2:XCharger_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:XCharger
      else
        do Value::jobe2:XCharger
      end
  of lower('NewJobBooking_jobe2:XLCD_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:XLCD
      else
        do Value::jobe2:XLCD
      end
  of lower('NewJobBooking_jobe2:XSimReader_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:XSimReader
      else
        do Value::jobe2:XSimReader
      end
  of lower('NewJobBooking_jobe2:XSystemConnector_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:XSystemConnector
      else
        do Value::jobe2:XSystemConnector
      end
  of lower('NewJobBooking_jobe2:XNotes_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:XNotes
      else
        do Value::jobe2:XNotes
      end
  of lower('NewJobBooking_jobe:Booking48HourOption_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:Booking48HourOption
      else
        do Value::jobe:Booking48HourOption
      end
  of lower('NewJobBooking_jobe:VSACustomer_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:VSACustomer
      else
        do Value::jobe:VSACustomer
      end
  of lower('NewJobBooking_jobe2:Contract_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:Contract
      else
        do Value::jobe2:Contract
      end
  of lower('NewJobBooking_jobe2:Prepaid_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:Prepaid
      else
        do Value::jobe2:Prepaid
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('NewJobBooking_form:ready_',1)
  !Multiple Job Booking
  p_web.DeleteSessionValue('job:Account_Number')
  DO MultipleJobBookingSetup
  p_web.SetSessionValue('NewJobBooking_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_NewJobBooking',0)
  If p_web.IfExistsValue('job:date_booked')
    job:date_booked = p_web.GetSessionValue('job:date_booked')
  else
    job:date_booked = Today()
    p_web.SetSessionValue('job:date_booked',job:date_booked)
  end
  If p_web.IfExistsValue('job:time_booked')
    job:time_booked = p_web.GetSessionValue('job:time_booked')
  else
    job:time_booked = Clock()
    p_web.SetSessionValue('job:time_booked',job:time_booked)
  end
  If p_web.IfExistsValue('job:Who_Booked')
    job:Who_Booked = p_web.GetSessionValue('job:Who_Booked')
  else
    job:Who_Booked = p_web.GetSessionValue('BookingUserCode')
    p_web.SetSessionValue('job:Who_Booked',job:Who_Booked)
  end
  If p_web.IfExistsValue('job:Workshop')
    job:Workshop = p_web.GetSessionValue('job:Workshop')
  else
    job:Workshop = 'YES'
    p_web.SetSessionValue('job:Workshop',job:Workshop)
  end
  If p_web.IfExistsValue('job:Cancelled')
    job:Cancelled = p_web.GetSessionValue('job:Cancelled')
  else
    job:Cancelled = 'NO'
    p_web.SetSessionValue('job:Cancelled',job:Cancelled)
  end
  If p_web.IfExistsValue('job:Completed')
    job:Completed = p_web.GetSessionValue('job:Completed')
  else
    job:Completed = 'NO'
    p_web.SetSessionValue('job:Completed',job:Completed)
  end
  If p_web.IfExistsValue('job:Paid')
    job:Paid = p_web.GetSessionValue('job:Paid')
  else
    job:Paid = 'NO'
    p_web.SetSessionValue('job:Paid',job:Paid)
  end
  If p_web.IfExistsValue('job:Paid_Warranty')
    job:Paid_Warranty = p_web.GetSessionValue('job:Paid_Warranty')
  else
    job:Paid_Warranty = 'NO'
    p_web.SetSessionValue('job:Paid_Warranty',job:Paid_Warranty)
  end
  If p_web.IfExistsValue('wob:HeadAccountNumber')
    wob:HeadAccountNumber = p_web.GetSessionValue('wob:HeadAccountNumber')
  else
    wob:HeadAccountNumber = p_web.GetSessionValue('BookingAccount')
    p_web.SetSessionValue('wob:HeadAccountNumber',wob:HeadAccountNumber)
  end
  If p_web.IfExistsValue('wob:DateBooked')
    wob:DateBooked = p_web.GetSessionValue('wob:DateBooked')
  else
    wob:DateBooked = job:Date_Booked
    p_web.SetSessionValue('wob:DateBooked',wob:DateBooked)
  end
  If p_web.IfExistsValue('wob:TimeBooked')
    wob:TimeBooked = p_web.GetSessionValue('wob:TimeBooked')
  else
    wob:TimeBooked = job:Time_Booked
    p_web.SetSessionValue('wob:TimeBooked',wob:TimeBooked)
  end
  If p_web.IfExistsValue('jobe2:SMSNotification')
    jobe2:SMSNotification = p_web.GetSessionValue('jobe2:SMSNotification')
  else
    jobe2:SMSNotification = 1
    p_web.SetSessionValue('jobe2:SMSNotification',jobe2:SMSNotification)
  end
  job:Transit_Type = p_web.GSV('tmp:TransitType')
  p_web.SetSessionValue('job:Transit_Type',job:Transit_Type)
  If p_web.IfExistsValue('job:Location_Type')
    job:Location_Type = p_web.GetSessionValue('job:Location_Type')
  else
    job:Location_Type = 'INTERNAL'
    p_web.SetSessionValue('job:Location_Type',job:Location_Type)
  end
  job:ESN = p_web.GSV('tmp:ESN')
  p_web.SetSessionValue('job:ESN',job:ESN)
  job:Manufacturer = p_web.GSV('tmp:Manufacturer')
  p_web.SetSessionValue('job:Manufacturer',job:Manufacturer)
  job:Model_Number = p_web.GSV('tmp:ModelNumber')
  p_web.SetSessionValue('job:Model_Number',job:Model_Number)
  job:POP = ''
  p_web.SetSessionValue('job:POP',job:POP)
  tmp:AmendFaultCodes = 0
  p_web.SetSessionValue('tmp:AmendFaultCodes',tmp:AmendFaultCodes)
  job:MSN = ''
  p_web.SetSessionValue('job:MSN',job:MSN)
  job:DOP = ''
  p_web.SetSessionValue('job:DOP',job:DOP)
  !Prime On Insert
      p_web.SSV('IMEINumberAccepted',1)
  
      If ForceColour('B')
          p_web.SSV('Required:Colour',1)
          p_web.SSV('Comment:Colour','Required')
      Else ! If ForceColour('B')
          p_web.SSV('Required:Colour','')
          p_web.SSV('Comment:Colour','')
      End ! If ForceColour('B')
  
      If ForceMobileNumber('B') Or GETINI(p_web.GSV('BookingAccount'),'ForceMobileNo',,CLIP(PATH())&'\SB2KDEF.INI') = 1
          p_web.SSV('Required:MobileNumber',1)
          p_web.SSV('Comment:MobileNumber','Required')
      Else ! If ForceMobileNumber('B')
          p_web.SSV('Required:MobileNumber','')
          p_web.SSV('Comment:MobileNumber','')
      End ! If ForceMobileNumber('B')
  
      If ForceNetwork('B')
          p_web.SSV('Required:Network',1)
          p_web.SSV('Comment:Network','Required')
      Else ! If ForceNetwork('B')
          p_web.SSV('Required:Network','')
          p_web.SSV('Comment:Network','')
      End ! If ForceNetwork('B')
  
      If ForceUnitType('B')
          p_web.SSV('Required:UnitType',1)
          p_web.SSV('Comment:UnitTYpe','Required')
      Else ! If ForceUnitType('B')
          p_web.SSV('Required:UnitType','')
          p_web.SSV('Comment:UnitType','')
      End ! If ForceUnitType('B')
  
  
      p_web.SSV('Required:OrderNumber','')
  
      p_web.SSV('Required:EndUserName','')
  
      p_web.SSV('Save:FranchiseAccount','')
      p_web.SSV('FranchiseAccount','')
  
      p_web.SSV('ShowHubWarning','')
  
  ! Prime Engineer Drop Down (DBH: 28/03/2008)
      Access:TRADEACC.ClearKey(tra:Account_Number_Key)
      tra:Account_Number = p_web.GSV('BookingAccount')
      If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
      !Found
          p_web.SSV('filter:Engineer','Upper(use:Active) = <39>YES<39> And Upper(use:Location) = Upper(<39>' & Clip(tra:SiteLocation) & '<39>) And Upper(use:User_Type) = <39>ENGINEER<39>')
      Else ! If Access:TRADEACC.TryFetch(tra:Account_Number) = Level:Benign
      !Error
      End ! If Access:TRADEACC.TryFetch(tra:Account_Number) = Level:Benign
  
      p_web.SSV('Required:FaultDescription','')
      If ForceFaultDescription('B')
          p_web.SSV('Required:FaultDescription',1)
      End ! If ForceFaultDescription('B')
  
  
      p_web.SSV('tmp:AmendAccessories','')
  
      If p_web.GSV('BookingSite') = 'ARC'
          p_web.SSV('jobe:HubRepair',1)
          p_web.SSV('jobe:HubRepairDate',Today())
          p_web.SSV('jobe:HubRepairTime',Clock())
          p_web.SSV('jobe:WebJob',0)
      Else
          p_web.SSV('jobe:HubRepair',0)
          p_web.SSV('jobe:HubRepairDate',0)
          p_web.SSV('jobe:HubRepairTime',0)
          p_web.SSV('jobe:WebJob',1)
      End ! If p_web.GSV('BookingSite') = 'ARC'
  
      set(Defaults)
      access:Defaults.next()
  
      If def:ChaChargeType <> ''
          p_web.SSV('job:Charge_Type',def:ChaChargeType)
      End ! If def:ChaChargeType <> ''
  
      If def:WarChargeType <> ''
          p_web.SSV('job:Warranty_Charge_Type',def:WarChargeType)    
      End ! If def:WarChargeType <> ''
  
      ! #11383 = Force the network field.
      IF (p_web.GSV('jobe:Network') = '')
          p_web.SSV('jobe:Network','082 VODACOM')
      END
  !Prime Comments
  p_web.SetSessionValue('Comment:AccountNumber','Required')
  p_web.SetSessionValue('Comment:EndUserName','')
  p_web.SetSessionValue('Comment:OutgoingCourier','')
  p_web.SetSessionValue('Comment:CourierWaybillNumber','')
  p_web.SetSessionValue('Comment:Engineer','')
  !Prime Hides/Read Onlys
  p_web.SetSessionValue('ReadOnly:IMEINumber',0)
  p_web.SetSessionValue('Hide:ProductCode',1)
  p_web.SetSessionValue('Hide:Colour',1)
  p_web.SetSessionValue('Hide:IMEINumberButton',1)
  p_web.SetSessionValue('Hide:OBFValidationButton',1)
  p_web.SetSessionValue('Hide:MSN',1)
  p_web.SetSessionValue('Required:MSN',0)
  p_web.SetSessionValue('Hide:EndUserName',1)
  p_web.SetSessionValue('Hide:CollectionAddress',1)
  p_web.SetSessionValue('Hide:DeliveryAddress',1)
  p_web.SetSessionValue('Hide:PrintJobCard',1)
  p_web.SetSessionValue('Hide:PrintJobReceipt',1)
  p_web.SetSessionValue('Hide:48Hour','')
  
  
  p_web.SetSessionValue('ReadOnly:Manufacturer','')
  p_web.SetSessionValue('ReadOnly:ModelNumber','')
  
  IF (p_web.GSV('mj:InProgress') <> 1)
      ! Only clear fault codes if not multiple booking. (Bryan: 06/12/2010)
      Loop x# = 1 To 6
          p_web.SetSessionValue('Filter:FaultCode' & x#,0)
          p_web.SetSessionValue('Hide:FaultCode' & x#,1)
          p_web.SetSessionValue('Comment:FaultCode' & x#,'')
          p_web.SetSessionValue('Prompt:FaultCode' & x#,'Fault Code ' & x#)
          p_web.SetSessionValue('tmp:FaultCode' & x#,'')
      End ! Loop x# = 1 To 6
  
  END !IF (p_web.GSV('mj:InProgress') <> 1)
      p_web.SetSessionValue('Hide:AmendFaultCodes',1)
  
          
  ! Fill In MQ Details
      If p_web.GSV('mq:WarrantyJob') <> ''
          job:Warranty_job = p_web.GSV('mq:WarrantyJob')
          p_web.SSV('job:Warranty_Job',job:Warranty_JOb)
      End ! If p_web.GSV('mq:WarrantyJob') <> ''
      If p_web.GSV('mq:Warranty_Charge_Type') <> ''
          job:Warranty_Charge_Type = p_web.GSV('mq:Warranty_Charge_Type')
          p_web.SSV('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
      End ! If p_web.GSV('mq:Warranty_Charge_Type') <> ''
      If p_web.GSV('mq:ChargeableJob') <> ''
          job:Chargeable_job = p_web.GSV('mq:ChargeableJob')
          p_web.SSV('job:Chargeable_Job',job:Chargeable_JOb)
      End ! If p_web.GSV('mq:WarrantyJob') <> ''
      If p_web.GSV('mq:Charge_Type') <> ''
          job:Charge_Type = p_web.GSV('mq:Charge_Type')
          p_web.SSV('job:Charge_Type',job:Charge_Type)
      End ! If p_web.GSV('mq:Warranty_Charge_Type') <> ''
  
      If p_web.GSV('mq:DOP') <> ''
          job:DOP = p_web.GSV('mq:DOP')
          p_web.SSV('job:DOP',job:DOP)
          
          p_web.SSV('mq:DOP',0)
          !p_web.SSV('tmp:DOP',Format(job:DOP,@d06))
          
      End ! If p_web.GSV('mq:DOP') <> ''
      If p_web.GSV('mq:POP') <> ''
          job:POP = p_web.GSV('mq:POP')
          p_web.SSV('job:POP',job:POP)
          p_web.SSV('save:DOP',p_web.GSV('job:DOP'))
          
      End ! If p_web.GSV('mq:POP') <> ''
      If p_web.GSV('mq:IMEIValidation') <> ''
          p_web.SSV('IMEIValidation',p_web.GSV('mq:IMEIValidation'))
      End ! If p_web.GSV('mq:IMEIValidation') <> ''
      If p_web.GSV('mq:IMEIValidationText') <> ''
          p_web.SSV('IMEIValidationText',p_web.GSV('mq:IMEIValidationText'))
      End ! If p_web.GSV('mq:IMEIValidationText') <> ''
  
      IF (vod.IsUnitLiquidDamage(p_web.GSV('job:ESN'),'',TRUE) = TRUE)
          p_web.SSV('Hide:LiquidDamage',0)
      ELSE
          p_web.SSV('Hide:LiquidDamage',1)
      END
  ! OBF Check
  If p_web.GSV('OBFValidation') = 'Passed' Or p_web.GSV('OBFValidation') = 'Failed'
      ! Fill in the fields, whether passed or failed (DBH: 03/04/2008)
      p_web.SSV('jobe:BoxESN',p_web.GSV('tmp:BOXIMEINumber'))
  
      If p_web.GSV('tmp:ProofOfPurchase') = 1
          p_web.SSV('jobe:ValidPOP','YES')
      End ! If p_web.GSV('tmp:ProofOfPurchase') = 1
      If p_web.GSV('tmp:ProofOfPurchase') = 0
          p_web.SSV('jobe:ValidPOP','NO')
      End ! If p_web.GSV('tmp:ProofOfPurchase') = 1

      !p_web.SSV('jobe:ReturnDate',Deformat(p_web.GSV('tmp:ReturnDate'),'@d06'))
      p_web.SSV('jobe:ReturnDate',p_web.GSV('tmp:ReturnDate')) ! #11626 Record Retun Date in the right format (DBH: 12/08/2010)

      p_web.SSV('jobe:TalkTime',p_web.GSV('tmp:TalkTime'))

      If p_web.GSV('tmp:OriginalPackaging') = 1
          p_web.SSV('jobe:OriginalPackaging',1)
      End ! If p_web.GSV('tmp:OriginalPackaging') = 1
      If p_web.GSV('tmp:OriginalPackaging') = 2
          p_web.SSV('jobe:OriginalPackaging',0)
      End ! If p_web.GSV('tmp:OriginalPackaging') = 1
      If p_web.GSV('tmp:OriginalAccessories') = 1
          p_web.SSV('jobe:OriginalBattery',1)
      End ! If p_web.GSV('tmp:OriginalAccessories') = 1
      If p_web.GSV('tmp:OriginalAccessories') = 0
          p_web.SSV('jobe:OriginalBattery',0)
      End ! If p_web.GSV('tmp:OriginalAccessories') = 1

      p_web.SSV('jobe:OriginalCharger','')
      p_web.SSV('jobe:OriginalAntenna','')
      If p_web.GSV('tmp:OriginalManuals') = 1
          p_web.SSV('jobe:OriginalManuals',1)
      End ! If p_web.GSV('tmp:OriginalManuals') = 1
      If p_web.GSV('tmp:OriginalManuals') = 0
          p_web.SSV('jobe:OriginalManuals',1)
      End ! If p_web.GSV('tmp:OriginalManuals') = 1
      If p_web.GSV('tmp:PhysicalDamage') = 1
          p_web.SSV('jobe:PhysicalDamage',1)
      End ! If p_web.GSV('tmp:PhysicalDamage') = 1
      If p_web.GSV('tmp:PhysicalDamage') = 0
          p_web.SSV('jobe:PhysicalDamage',1)
      End ! If p_web.GSV('tmp:PhysicalDamage') = 1

      p_web.SSV('jobe:OriginalDealer',p_web.GSV('tmp:OriginalDealer'))
      p_web.SSV('jobe:BranchOfReturn',p_web.GSV('tmp:BranchOfReturn'))
  
      p_web.SSV('jobe:Network',p_web.GSV('tmp:Network'))
      !p_web.SSV('tmp:DOP',Deformat(p_web.GSV('tmp:DOP'),'@d06'))
  
      p_web.SSV('jof:StoreReferenceNumber',p_web.GSV('tmp:StoreReferenceNumber'))
      p_web.SSV('jof:Replacement',p_web.GSV('tmp:Replacement'))
      p_web.SSV('jof:LAccountNumber',p_web.GSV('LAccountNumber'))
      p_web.SSV('jof:ReplacementIMEINumber',p_web.GSV('ReplacementIMEINumber'))
  
      p_web.SSV('job:DOP',p_web.GSV('tmp:DateOfPurchase'))
      job:DOP = p_web.GSV('job:DOP')
  
      If p_web.GSV('OBFValidation') = 'Passed'
          job:POP = 'O'
          job:Warranty_Job = 'YES'
          job:Warranty_Charge_Type = 'WARRANTY (OBF)'
          job:Chargeable_Job = 'NO'
          p_web.SSV('job:POP',job:POP)
          p_web.SSV('job:Warranty_Job',job:Warranty_Job)
          p_web.SSV('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
          p_web.SSV('job:Chargeable_Job',job:Chargeable_Job)
  
          p_web.SSV('jobe:OBFValidated',1)
          p_web.SSV('jobe:OBFValidateDate',Today())
          p_web.SSV('jobe:OBFValidateTime',Clock())
          p_web.SSV('jobe:Booking48HourOption',2)
          p_web.SSV('jobe:Engineer48HourOption',2)
          p_web.SSV('jobe:HubRepair',1)
          p_web.SSV('jobe:HubRepairDate',Today())
          p_web.SSV('jobe:HubRepairTime',Clock())
          p_web.SSV('ReadOnly:HubRepair',1)
          p_web.SSV('filter:WarrantyChargeType','cha:Charge_Type = ''WARRANTY (OBF)''')
          p_web.SSV('ReadOnly:WarrantyJob',1)
          p_web.SSV('ReadOnly:ChargeableJob',1)
          p_web.SSV('ReadOnly:WarrantyChargeType',1)
          p_web.SSV('Comment:POP','OBF Passed')
          p_web.SSV('Drop:JobType','-----------OBF Passed------------')
          p_web.SSV('Drop:JobTypeDefault','O')
      Else
          p_web.SSV('jobe:Engineer48HourOption',0)
      End ! If p_web.GSV('OBFValidaton') = 'Passed'
  Else
      p_web.SSV('jobe:Engineer48HourOption',0)
  End ! If p_web.GSV('OBFValidation') <> ''


  p_web.SSV('job:Account_Number',GETINI(p_web.GSV('BookingAccount'), 'TradeAccount',, CLIP(PATH()) & '\SB2KDEF.INI'))
  job:Account_Number = p_web.GSV('job:Account_Number')
  If p_web.GSV('job:Account_Number') <> ''
      p_web.SSV('FranchiseAccount',1)
      Do Value:AccountNumber
      job:Postcode = p_web.GSV('job:Postcode')
      job:Company_Name = p_web.GSV('job:Company_Name')
      job:Address_Line1 = p_web.GSV('job:Address_Line1')
      job:Address_Line2 = p_web.GSV('job:Address_Line2')
      job:Address_Line3 = p_web.GSV('job:Address_Line3')
      job:Telephone_Number = p_web.GSV('job:Telephone_Number')
      job:Fax_Number = p_web.GSV('job:Fax_Number')
      jobe:EndUserEmailAddress = p_web.GSV('jobe:EndUserEmailAddress')
      jobe2:HubCustomer = p_web.GSV('jobe2:HubCustomer')
      job:Postcode_Delivery = p_web.GSV('job:Postcode_Delivery')
      job:Company_Name_Delivery = p_web.GSV('job:Company_Name_Delivery')
      job:Address_Line1_Delivery = p_web.GSV('job:Address_Line1_Delivery')
      job:Address_Line2_Delivery = p_web.GSV('job:Address_Line2_Delivery')
      job:Address_Line3_Delivery = p_web.GSV('job:Address_Line3_Delivery')
      job:Telephone_Delivery = p_web.GSV('job:Telephone_Delivery')
      jobe2:HubDelivery= p_web.GSV('jobe2:HubDelivery')
      job:Postcode_Collection = p_web.GSV('job:Postcode_Collection')
      job:Company_Name_Collection = p_web.GSV('job:Company_Name_Collection')
      job:Address_Line1_Collection = p_web.GSV('job:Address_Line1_Collection')
      job:Address_Line2_Collection = p_web.GSV('job:Address_Line2_Collection')
      job:Address_Line3_Collection = p_web.GSV('job:Address_Line3_Collection')
      job:Telephone_Collection = p_web.GSV('job:Telephone_Collection')
      jobe2:HubCollection = p_web.GSV('jobe2:HubCollection')
      job:Courier = p_web.GSV('job:Courier')
        job:Incoming_Courier = p_web.GSV('job:Incoming_Courier')
        job:Loan_Courier = p_web.GSV('job:Loan_Courier') ! #13389 Store courier (DBH: 03/10/2014)
        job:Exchange_Courier = p_web.GSV('job:Exchange_Courier') ! #13389 Store courier (DBH: 03/10/2014)
  End ! If p_web.GSV('job:Account_Number') <> ''
  
    
    
    Do Value:IMEINumber
    Do Value:DOP
    IF (p_web.GSV('mj:InProgress') <> 1)
        ! If Multiple Job Booking, the this has already been called
        Do CalculateFaultCodes
    END
  
    Do Value:ModelNumber
    Do Value:MSN
    
    DO PreBooking

PreCopy  Routine
  p_web.SetValue('NewJobBooking_form:ready_',1)
  p_web.SetSessionValue('NewJobBooking_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_NewJobBooking',0)
  p_web._PreCopyRecord(JOBS,job:Ref_Number_Key)
  ! here we need to copy the non-unique fields across
  If p_web.IfExistsValue('job:date_booked')
    job:date_booked = p_web.GetSessionValue('job:date_booked')
  else
    p_web.SetSessionValue('job:date_booked',Today())
    job:date_booked = Today()
  end
  If p_web.IfExistsValue('job:time_booked')
    job:time_booked = p_web.GetSessionValue('job:time_booked')
  else
    p_web.SetSessionValue('job:time_booked',Clock())
    job:time_booked = Clock()
  end
  If p_web.IfExistsValue('job:Who_Booked')
    job:Who_Booked = p_web.GetSessionValue('job:Who_Booked')
  else
    p_web.SetSessionValue('job:Who_Booked',p_web.GetSessionValue('BookingUserCode'))
    job:Who_Booked = p_web.GetSessionValue('BookingUserCode')
  end
  If p_web.IfExistsValue('job:Workshop')
    job:Workshop = p_web.GetSessionValue('job:Workshop')
  else
    p_web.SetSessionValue('job:Workshop','YES')
    job:Workshop = 'YES'
  end
  If p_web.IfExistsValue('job:Cancelled')
    job:Cancelled = p_web.GetSessionValue('job:Cancelled')
  else
    p_web.SetSessionValue('job:Cancelled','NO')
    job:Cancelled = 'NO'
  end
  If p_web.IfExistsValue('job:Completed')
    job:Completed = p_web.GetSessionValue('job:Completed')
  else
    p_web.SetSessionValue('job:Completed','NO')
    job:Completed = 'NO'
  end
  If p_web.IfExistsValue('job:Paid')
    job:Paid = p_web.GetSessionValue('job:Paid')
  else
    p_web.SetSessionValue('job:Paid','NO')
    job:Paid = 'NO'
  end
  If p_web.IfExistsValue('job:Paid_Warranty')
    job:Paid_Warranty = p_web.GetSessionValue('job:Paid_Warranty')
  else
    p_web.SetSessionValue('job:Paid_Warranty','NO')
    job:Paid_Warranty = 'NO'
  end
  If p_web.IfExistsValue('wob:HeadAccountNumber')
    wob:HeadAccountNumber = p_web.GetSessionValue('wob:HeadAccountNumber')
  else
    p_web.SetSessionValue('wob:HeadAccountNumber',p_web.GetSessionValue('BookingAccount'))
    wob:HeadAccountNumber = p_web.GetSessionValue('BookingAccount')
  end
  If p_web.IfExistsValue('wob:DateBooked')
    wob:DateBooked = p_web.GetSessionValue('wob:DateBooked')
  else
    p_web.SetSessionValue('wob:DateBooked',job:Date_Booked)
    wob:DateBooked = job:Date_Booked
  end
  If p_web.IfExistsValue('wob:TimeBooked')
    wob:TimeBooked = p_web.GetSessionValue('wob:TimeBooked')
  else
    p_web.SetSessionValue('wob:TimeBooked',job:Time_Booked)
    wob:TimeBooked = job:Time_Booked
  end
  If p_web.IfExistsValue('jobe2:SMSNotification')
    jobe2:SMSNotification = p_web.GetSessionValue('jobe2:SMSNotification')
  else
    p_web.SetSessionValue('jobe2:SMSNotification',1)
    jobe2:SMSNotification = 1
  end
  If p_web.IfExistsValue('job:Location_Type')
    job:Location_Type = p_web.GetSessionValue('job:Location_Type')
  else
    p_web.SetSessionValue('job:Location_Type','INTERNAL')
    job:Location_Type = 'INTERNAL'
  end

PreUpdate       Routine
  p_web.SetValue('NewJobBooking_form:ready_',1)
  p_web.SetSessionValue('NewJobBooking_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('NewJobBooking:Primed',0)

PreDelete       Routine
  p_web.SetValue('NewJobBooking_form:ready_',1)
  p_web.SetSessionValue('NewJobBooking_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('NewJobBooking:Primed',0)
  p_web.setsessionvalue('showtab_NewJobBooking',0)

LoadRelatedRecords  Routine
    p_web.GetDescription(MODPROD,,,mop:ProductCode,,p_web.GetSessionValue('job:ProductCode'))
    p_web.FileToSessionQueue(MODPROD)
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If p_web.GSV('Hide:PreviousAddress') <> 1
  End
          If p_web.IfExistsValue('jobe2:POPConfirmed') = 0
            p_web.SetValue('jobe2:POPConfirmed',0)
            jobe2:POPConfirmed = 0
          End
  If p_web.GSV('Hide:ButtonMSISDNCheck') <> 1
  End
      If(p_web.GetSessionValue('IMEINumberAccepted') = 0)
          If p_web.IfExistsValue('tmp:AmendAccessories') = 0
            p_web.SetValue('tmp:AmendAccessories',0)
            tmp:AmendAccessories = 0
          End
      End
      If(p_web.GetSessionValue('IMEINumberAccepted') = 0)
        If (p_web.GetSessionValue('Hide:AmendFaultCodes') = 1)
          If p_web.IfExistsValue('tmp:AmendFaultCodes') = 0
            p_web.SetValue('tmp:AmendFaultCodes',0)
            tmp:AmendFaultCodes = 0
          End
        End
      End
      If(p_web.GetSessionValue('IMEINumberAccepted') = 0)
          If p_web.IfExistsValue('tmp:ExternalDamageChecklist') = 0
            p_web.SetValue('tmp:ExternalDamageChecklist',0)
            tmp:ExternalDamageChecklist = 0
          End
      End
      If(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1)
          If p_web.IfExistsValue('jobe2:XNone') = 0
            p_web.SetValue('jobe2:XNone',0)
            jobe2:XNone = 0
          End
      End
      If(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1)
          If p_web.IfExistsValue('jobe2:XAntenna') = 0
            p_web.SetValue('jobe2:XAntenna',0)
            jobe2:XAntenna = 0
          End
      End
      If(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1)
          If p_web.IfExistsValue('jobe2:XLens') = 0
            p_web.SetValue('jobe2:XLens',0)
            jobe2:XLens = 0
          End
      End
      If(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1)
          If p_web.IfExistsValue('jobe2:XFCover') = 0
            p_web.SetValue('jobe2:XFCover',0)
            jobe2:XFCover = 0
          End
      End
      If(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1)
          If p_web.IfExistsValue('jobe2:XBCover') = 0
            p_web.SetValue('jobe2:XBCover',0)
            jobe2:XBCover = 0
          End
      End
      If(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1)
          If p_web.IfExistsValue('jobe2:XKeypad') = 0
            p_web.SetValue('jobe2:XKeypad',0)
            jobe2:XKeypad = 0
          End
      End
      If(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1)
          If p_web.IfExistsValue('jobe2:XBattery') = 0
            p_web.SetValue('jobe2:XBattery',0)
            jobe2:XBattery = 0
          End
      End
      If(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1)
          If p_web.IfExistsValue('jobe2:XCharger') = 0
            p_web.SetValue('jobe2:XCharger',0)
            jobe2:XCharger = 0
          End
      End
      If(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1)
          If p_web.IfExistsValue('jobe2:XLCD') = 0
            p_web.SetValue('jobe2:XLCD',0)
            jobe2:XLCD = 0
          End
      End
      If(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1)
          If p_web.IfExistsValue('jobe2:XSimReader') = 0
            p_web.SetValue('jobe2:XSimReader',0)
            jobe2:XSimReader = 0
          End
      End
      If(p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1)
          If p_web.IfExistsValue('jobe2:XSystemConnector') = 0
            p_web.SetValue('jobe2:XSystemConnector',0)
            jobe2:XSystemConnector = 0
          End
      End
      If(p_web.GetSessionValue('IMEINumberAccepted') = 0)
          If p_web.IfExistsValue('jobe:VSACustomer') = 0
            p_web.SetValue('jobe:VSACustomer',0)
            jobe:VSACustomer = 0
          End
      End
      If(p_web.GetSessionValue('IMEINumberAccepted') = 0)
          If p_web.IfExistsValue('jobe2:Contract') = 0
            p_web.SetValue('jobe2:Contract',0)
            jobe2:Contract = 0
          End
      End
      If(p_web.GetSessionValue('IMEINumberAccepted') = 0)
          If p_web.IfExistsValue('jobe2:Prepaid') = 0
            p_web.SetValue('jobe2:Prepaid',0)
            jobe2:Prepaid = 0
          End
      End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('NewJobBooking_ChainTo')
  ! Check for restricted child records
  p_web._OpenFile(JOBSWARR)
  jow:RefNumber = job:Ref_Number
  Set(jow:RefNumberKey,jow:RefNumberKey)
  Next(JOBSWARR)
  If |
    jow:RefNumber = job:Ref_Number and |
    ErrorCode() = 0
      loc:Invalid = 'restrict'
      loc:Alert = clip(p_web.site.RestrictText) & ' JOBSWARR'
  End
  p_web._CloseFile(JOBSWARR)
  p_web._OpenFile(INVOICE)
  inv:Invoice_Number = job:Invoice_Number
  Set(inv:Invoice_Number_Key,inv:Invoice_Number_Key)
  Next(INVOICE)
  If |
    inv:Invoice_Number = job:Invoice_Number and |
    ErrorCode() = 0
      loc:Invalid = 'restrict'
      loc:Alert = clip(p_web.site.RestrictText) & ' INVOICE'
  End
  p_web._CloseFile(INVOICE)

ValidateRecord  Routine
  p_web.DeleteSessionValue('NewJobBooking_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 8
    loc:InvalidTab += 1
  ! tab = 1
  If p_web.GSV('Hide:PreviousAddress') <> 1
    loc:InvalidTab += 1
  End
  ! tab = 3
    loc:InvalidTab += 1
        If jobe:Network = '' and p_web.GetSessionValue('Required:Network') = 1
          loc:Invalid = 'jobe:Network'
          loc:alert = p_web.translate('Network') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          jobe:Network = Upper(jobe:Network)
          p_web.SetSessionValue('jobe:Network',jobe:Network)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
        If job:Mobile_Number = '' and p_web.GetSessionValue('Required:MobileNumber') = 1
          loc:Invalid = 'job:Mobile_Number'
          loc:alert = p_web.translate('Mobile Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          job:Mobile_Number = Upper(job:Mobile_Number)
          p_web.SetSessionValue('job:Mobile_Number',job:Mobile_Number)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
        If job:Unit_Type = '' and p_web.GetSessionValue('Required:UnitType') = 1
          loc:Invalid = 'job:Unit_Type'
          loc:alert = p_web.translate('Unit Type') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          job:Unit_Type = Upper(job:Unit_Type)
          p_web.SetSessionValue('job:Unit_Type',job:Unit_Type)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
    If p_web.GetSessionValue('Hide:ProductCode') <> 1
        If job:ProductCode = '' and p_web.GetSessionValue('Required:ProductCode') = 1
          loc:Invalid = 'job:ProductCode'
          loc:alert = p_web.translate('Product Code') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          job:ProductCode = Upper(job:ProductCode)
          p_web.SetSessionValue('job:ProductCode',job:ProductCode)
        If loc:Invalid <> '' then exit.
    End
  If Loc:Invalid <> '' then exit.
    If p_web.GetSessionValue('Hide:Colour') <> 1
        If job:Colour = '' and p_web.GetSessionValue('Required:Colour') = 1
          loc:Invalid = 'job:Colour'
          loc:alert = p_web.translate('Colour') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          job:Colour = Upper(job:Colour)
          p_web.SetSessionValue('job:Colour',job:Colour)
        If loc:Invalid <> '' then exit.
    End
  If Loc:Invalid <> '' then exit.
    If p_web.GetSessionValue('Hide:MSN') <> 1
        If job:MSN = '' and p_web.GetSessionValue('Required:MSN') = 1
          loc:Invalid = 'job:MSN'
          loc:alert = p_web.translate('M.S.N.') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          job:MSN = Upper(job:MSN)
          p_web.SetSessionValue('job:MSN',job:MSN)
        If loc:Invalid <> '' then exit.
    End
  ! tab = 2
    loc:InvalidTab += 1
  If Loc:Invalid <> '' then exit.
        If job:DOP = '' and p_web.GetSessionValue('Required:DOP') = 1
          loc:Invalid = 'job:DOP'
          loc:alert = 'Date Of Purchase Required'
        End
        If loc:Invalid <> '' then exit.
          locPOPTypePassword = Upper(locPOPTypePassword)
          p_web.SetSessionValue('locPOPTypePassword',locPOPTypePassword)
        If loc:Invalid <> '' then exit.
      If not (p_web.GetSessionValue('IMEINumberAccepted') = 0)
          jobe2:WarrantyRefNo = Upper(jobe2:WarrantyRefNo)
          p_web.SetSessionValue('jobe2:WarrantyRefNo',jobe2:WarrantyRefNo)
        If loc:Invalid <> '' then exit.
      End
  If Loc:Invalid <> '' then exit.
    If p_web.GSV('OBFValidation') <> 'Passed'
      If not (p_web.GetSessionValue('job:Chargeable_Job') <> 'YES' )
        If job:Charge_Type = ''
          loc:Invalid = 'job:Charge_Type'
          loc:alert = p_web.translate('Chargeable Type') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          job:Charge_Type = Upper(job:Charge_Type)
          p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
        If loc:Invalid <> '' then exit.
      End
    End
  If Loc:Invalid <> '' then exit.
      If not (p_web.GSV('job:Warranty_Job') <> 'YES')
        If job:Warranty_Charge_Type = ''
          loc:Invalid = 'job:Warranty_Charge_Type'
          loc:alert = p_web.translate('Warranty Charge Type') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          job:Warranty_Charge_Type = Upper(job:Warranty_Charge_Type)
          p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
        If loc:Invalid <> '' then exit.
      End
  If Loc:Invalid <> '' then exit.
      If not (p_web.GetSessionValue('IMEINumberAccepted') = 0)
        If job:Authority_Number = '' and p_web.GetSessionValue('Required:AuthorityNumber') = 1
          loc:Invalid = 'job:Authority_Number'
          loc:alert = p_web.translate('Authority Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          job:Authority_Number = Upper(job:Authority_Number)
          p_web.SetSessionValue('job:Authority_Number',job:Authority_Number)
        If loc:Invalid <> '' then exit.
      End
  ! tab = 11
  If p_web.GSV('Hide:ButtonMSISDNCheck') <> 1
    loc:InvalidTab += 1
  End
  ! tab = 4
    loc:InvalidTab += 1
  If Loc:Invalid <> '' then exit.
      If not (p_web.GetSessionValue('FranchiseAccount') <> 1)
        If job:Account_Number = ''
          loc:Invalid = 'job:Account_Number'
          loc:alert = 'Account Number Required'
        End
        If loc:Invalid <> '' then exit.
      End
  If Loc:Invalid <> '' then exit.
      If not (p_web.GSV('FranchiseAccount') <> 2)
        If job:Account_Number = ''
          loc:Invalid = 'job:Account_Number'
          loc:alert = 'Account Number Required'
        End
        If loc:Invalid <> '' then exit.
      End
  If Loc:Invalid <> '' then exit.
      If not (p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('FranchiseAccount') = 0)
        If job:Order_Number = '' and p_web.GetSessionValue('Required:OrderNumber') = 1
          loc:Invalid = 'job:Order_Number'
          loc:alert = 'Order Number Required'
        End
          job:Order_Number = Upper(job:Order_Number)
          p_web.SetSessionValue('job:Order_Number',job:Order_Number)
        If loc:Invalid <> '' then exit.
      End
  If Loc:Invalid <> '' then exit.
      If not (p_web.GetSessionValue('Hide:EndUserName') = 1 OR p_web.GetSessionValue('IMEINumberAccepted') = 0)
          job:Title = Upper(job:Title)
          p_web.SetSessionValue('job:Title',job:Title)
        If loc:Invalid <> '' then exit.
      End
  If Loc:Invalid <> '' then exit.
      If not (p_web.GetSessionValue('Hide:EndUserName') = 1 OR p_web.GetSessionValue('IMEINumberAccepted') = 0)
          job:Initial = Upper(job:Initial)
          p_web.SetSessionValue('job:Initial',job:Initial)
        If loc:Invalid <> '' then exit.
      End
  If Loc:Invalid <> '' then exit.
      If not (p_web.GetSessionValue('Hide:EndUserName') = 1 OR p_web.GetSessionValue('IMEINumberAccepted') = 0)
        If job:Surname = '' and p_web.GetSessionValue('Required:EndUserName') = 1
          loc:Invalid = 'job:Surname'
          loc:alert = p_web.translate('Surname') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          job:Surname = Upper(job:Surname)
          p_web.SetSessionValue('job:Surname',job:Surname)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GetSessionValue('Hide:EndUserName') = 1 OR p_web.GetSessionValue('IMEINumberAccepted') = 0)
          jobe:EndUserTelNo = Upper(jobe:EndUserTelNo)
          p_web.SetSessionValue('jobe:EndUserTelNo',jobe:EndUserTelNo)
        If loc:Invalid <> '' then exit.
      End
  If Loc:Invalid <> '' then exit.
      If not (p_web.GetSessionValue('IMEINumberAccepted') = 0)
        If job:Courier = '' and p_web.GetSessionValue('Required:Courier') = 1
          loc:Invalid = 'job:Courier'
          loc:alert = p_web.translate('Outgoing Courier') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          job:Courier = Upper(job:Courier)
          p_web.SetSessionValue('job:Courier',job:Courier)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GetSessionValue('IMEINumberAccepted') = 0)
        If jobe2:CourierWaybillNumber = '' and p_web.GSV('Required:CourierWaybillNo') = 1
          loc:Invalid = 'jobe2:CourierWaybillNumber'
          loc:alert = 'Courier Waybill Number Required'
        End
          jobe2:CourierWaybillNumber = Upper(jobe2:CourierWaybillNumber)
          p_web.SetSessionValue('jobe2:CourierWaybillNumber',jobe2:CourierWaybillNumber)
        If loc:Invalid <> '' then exit.
      End
  ! tab = 6
    loc:InvalidTab += 1
  ! tab = 7
    loc:InvalidTab += 1
      If not (p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode1') = 1)
        If tmp:FaultCode1 = ''
          loc:Invalid = 'tmp:FaultCode1'
          loc:alert = p_web.translate(p_web.GetSessionValue('Prompt:FaultCode1')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode2') = 1)
        If tmp:FaultCode2 = ''
          loc:Invalid = 'tmp:FaultCode2'
          loc:alert = p_web.translate(p_web.GetSessionValue('Prompt:FaultCode2')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode3') = 1)
        If tmp:FaultCode3 = ''
          loc:Invalid = 'tmp:FaultCode3'
          loc:alert = p_web.translate(p_web.GetSessionValue('Prompt:FaultCode3')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode4') = 1)
        If tmp:FaultCode4 = ''
          loc:Invalid = 'tmp:FaultCode4'
          loc:alert = p_web.translate(p_web.GetSessionValue('Prompt:FaultCode4')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode5') = 1)
        If tmp:FaultCode5 = ''
          loc:Invalid = 'tmp:FaultCode5'
          loc:alert = p_web.translate(p_web.GetSessionValue('Prompt:FaultCode5')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GetSessionValue('IMEINumberAccepted') = 0 Or p_web.GetSessionValue('tmp:AmendFaultCodes') <> 1 Or p_web.GetSessionValue('Hide:FaultCode6') = 1)
        If tmp:FaultCode6 = ''
          loc:Invalid = 'tmp:FaultCode6'
          loc:alert = p_web.translate(p_web.GetSessionValue('Prompt:FaultCode6')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
  ! tab = 5
    loc:InvalidTab += 1
  If Loc:Invalid <> '' then exit.
      If not (p_web.GetSessionValue('IMEINumberAccepted') = 0)
        If job:Engineer = '' and p_web.GetSessionValue('Required:Engineer') = 1
          loc:Invalid = 'job:Engineer'
          loc:alert = p_web.translate('Engineer') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GetSessionValue('IMEINumberAccepted') = 0)
        If jbn:Fault_Description = '' and p_web.GetSessionValue('Required:FaultDescription')
          loc:Invalid = 'jbn:Fault_Description'
          loc:alert = 'Fault Description Required'
        End
          jbn:Fault_Description = Upper(jbn:Fault_Description)
          p_web.SetSessionValue('jbn:Fault_Description',jbn:Fault_Description)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GetSessionValue('IMEINumberAccepted') = 0)
          jbn:Engineers_Notes = Upper(jbn:Engineers_Notes)
          p_web.SetSessionValue('jbn:Engineers_Notes',jbn:Engineers_Notes)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GetSessionValue('tmp:ExternalDamageCheckList') <> 1 OR p_web.GetSessionValue('jobe2:XNone') = 1)
          jobe2:XNotes = Upper(jobe2:XNotes)
          p_web.SetSessionValue('jobe2:XNotes',jobe2:XNotes)
        If loc:Invalid <> '' then exit.
      End
  ! The following fields are not on the form, but need to be checked anyway.
  ! Automatic Dictionary Validation
  If p_web.GetSessionValue('NewJobBooking:Primed') = 1 ! Autonumbered fields are not validated unless the record has been pre-primed
    If job:Ref_Number = 0
      loc:Invalid = 'job:Ref_Number'
      loc:Alert = clip('job:Ref_Number') & ' ' & p_web.site.NotZeroText
      !exit
    End
  End
  If Loc:Invalid <> '' then exit.
    ! Validate Record Before Write
    IF ~(DuplicateTabCheck(p_web,'NEWJOBBOOKING',1) = 1 AND DuplicateTabCheck(p_web,'NEWJOBBOOKINGIMEI',p_web.GSV('job:ESN')) = 1)
        CreateScript(packet,'alert("Error. Make sure that you do not login, or edit/insert another job while you in the middle of booking.");window.open("IndexPage","_self")')
        DO SendPacket
        EXIT
    END ! IF
    
    BHAddToDebugLog('NewJobBooking: Save. p_web.GSV(job:ESN) = ' & p_web.GSV('job:ESN'))
    BHAddToDebugLog('NewJobBooking: Save. p_web.GetValue(job:ESN) = ' & p_web.GetValue('job:ESN'))
        
    If p_web.GSV('ReadyForNewJobBooking') = ''
        loc:Invalid = 'jobe:Booking48HourOption'
        loc:Alert = 'An Error Has Occurred. Please "Quit Booking" and try again!'
        Exit
    End ! If p_web.GSV('ReadyForNewJobBooking') = ''
    
    IF (p_web.GSV('Hide:ButtonMSISDNCheck') <> 1)
        IF (p_web.GSV('MSISDNCheckRun') <> 1)
            loc:Invalid = 'job:Mobile_Number'
            loc:Alert = 'MSISDN Check Is Required'
            Exit
        END
    END
      
      ! Check for bouncers. (DBH: 11/04/2008)
    Access:DEFAULTS.Clearkey(def:RecordNumberKey)
    def:Record_Number = 1
    Set(def:RecordNumberKey,def:RecordNumberKey)
    Loop ! Begin Loop
        If Access:DEFAULTS.Next()
            Break
        End ! If Access:DEFAULTS.Next()
        Break
    End ! Loop
      
    Bouncers# = 0
    LiveBouncers# = 0
    Access:JOBS_ALIAS.Clearkey(job_ali:ESN_Key)
    job_ali:ESN = p_web.GSV('job:ESN')
    Set(job_ali:ESN_Key,job_ali:ESN_Key)
    Loop
        If Access:JOBS_ALIAS.Next()
            Break
        End ! If Access:JOBS_ALIAS.Next()
        If job_ali:ESN <> p_web.GSV('job:ESN')
            Break
        End ! If job_ali:ESN <> p_web.GSV('job:ESN')
        If job_ali:Cancelled = 'YES'
            Cycle
        End ! If job_ali:Cancelled = 'YES'
        If job_ali:Date_Completed = ''
            LiveBouncers# += 1
        End ! If job_ali:Date_Completed = ''
        Bouncers# += 1
    End ! Loop
      
    If def:Allow_Bouncer <> 'YES'
        If Bouncers#
            loc:Invalid = 'jobe:Booking48HourOption'
            loc:Alert = 'A job already exists with this IMEI Number. Please "Quit Booking" and start again.'
            Exit
        End ! If Bouncers#
    Else ! If def:Allow_Bouncer <> 'YES'
        If GETINI('BOUNCER','StopLive',,Clip(Path()) & '\SB2KDEF.INI') = 1
            If LiveBouncers# > 0
                loc:Invalid = 'jobe:Booking48HourOption'
                loc:Alert = 'A job already exists with this IMEI Number. Please "Quit Booking" and start again.'
                Exit
            End ! If LiveBouncers(p_web.GSV('job:ESN'),Today())
        End ! If GETINI('BOUNCER','StopLive',,Clip(Path()) & '\SB2KDEF.INI') = 1
    End ! If def:Allow_Bouncer <> 'YES'
      
    If def:Allow_Bouncer <> 'YES'
        If Bouncers#
            loc:Invalid = 'jobe:Booking48HourOption'
            loc:Alert = 'A job already exists with this IMEI Number. Please "Quit Booking" and start again.'
            Exit
        End ! If Bouncers#
    Else ! If def:Allow_Bouncer <> 'YES'
        If GETINI('BOUNCER','StopLive',,Clip(Path()) & '\SB2KDEF.INI') = 1
            If LiveBouncers# > 0
                loc:Invalid = 'jobe:Booking48HourOption'
                loc:Alert = 'A job already exists with this IMEI Number. Please "Quit Booking" and start again.'
                Exit
            End ! If LiveBouncers(job:ESN,Today())
        End ! If GETINI('BOUNCER','StopLive',,Clip(Path()) & '\SB2KDEF.INI') = 1
    End ! If def:Allow_Bouncer <> 'YES'
      
      
    If job:Transit_Type = ''
        loc:Invalid = 'job:Transit_Type'
        loc:Alert = 'Transit Type ' & p_web.site.RequiredText
        Exit
    End ! If job:Transit_Type = ''
      
    If p_web.GSV('Required:Network') = 1
        If p_web.GSV('jobe:Network') = ''
            loc:Invalid = 'jobe:Network'
            loc:Alert = 'Network ' & p_web.site.RequiredText
            Exit
        End ! If p_web.GSV('jobe:Network') = ''
    End ! If p_web.GSV('Required:Network') = 1
      
    If p_web.GSV('Required:Colour') = 1 And p_web.GSV('Hide:Colour') <> 1
        If p_web.GSV('job:Colour') = ''
            loc:Invalid = 'job:Colour'
            loc:Alert = 'Colour ' & p_web.site.RequiredText
            Exit
        End ! If p_web.GSV('job:Colour') = ''
    End ! If p_web.GSV('Required:Colour') = 1
      
    If p_web.GSV('Required:OrderNumber') = 1
        If p_web.GSV('job:Order_Number') = ''
            loc:Invalid = 'job:Order_Number'
            loc:Alert = 'Order Number ' & p_web.site.RequiredText
            Exit
        End ! If p_web.GSV('job:Order_Number') = ''
    End ! If p_web.GSV('Required:OrderNumber') = 1
      
    If p_web.GSV('Required:EndUserName') = 1 And  p_web.GSV('Hide:EndUserName') = 0
        If p_web.GSV('job:Surname') = ''
            loc:Invalid = 'job:Surname'
            loc:Alert = 'End User Name ' & p_web.site.RequiredText
            Exit
        End ! If p_web.GSV('job:Surname') = ''
    End ! If p_web.GSV('Required:EndUserName') = 1
      
      
    If p_web.GSV('Required:Courier') = 1
        If p_web.GSV('job:Courier') = ''
            loc:Invalid = 'job:Courier'
            loc:Alert = 'Outgoing Courier ' & p_web.site.RequiredText
            Exit
        End ! If p_web.GSV('job:Courier') = ''
    End ! If p_web.GSV('Required:Courier') = 1
      
      
    If p_web.GSV('Required:Engineer') = 1
        If p_web.GSV('job:Engineer') = ''
            loc:Invalid = 'job:Engineer'
            loc:Alert = 'Engineer ' & p_web.site.RequiredText
            Exit
        End ! If p_web.GSV('job:Engineer') = ''
    End ! If p_web.GSV('Required:Engineer') = 1
      
    If p_web.GSV('tmp:TheJobAccessory') = ''
        loc:Invalid = 'job:Engineer'
        loc:Alert = 'Accessories Required'
        Exit
    End ! If p_web.GSV('tmp:TheJobAccessory') = ''
      
    IF (p_web.GSV('Hide:AmendFaultCodes') <> 1) ! #12641 No point forcing the codes if you can't access them (DBH: 20/06/2012)
        Loop x# = 1 To 6
            If p_web.GSV('Hide:FaultCode' & x#) = ''
                If p_web.GSV('tmp:FaultCode' & x#) = ''
                    loc:Invalid = 'tmp:FaultCode' & x#
                    loc:Alert = 'Fault Code ' & p_web.GSV('Prompt:FaultCode' & x#) & ' Required'
                    Exit
                End ! If p_web.GSV('tmp:FaultCode' & x#) = ''
            End ! If p_web.GSV('Hide:FaultCode' & x#) = ''
        End ! Loop x# = 1 To 6
    END !
    
      
      
      ! --- Bit of a workaround to stop job:pop & dop & warranty being lost ----
    !  ! Inserting:  DBH 21/01/2009 #10365
    !  If p_web.GSV('OBFValidation') = 'Passed'
    !      p_web.SSV('tmp:DOP',p_web.GSV('tmp:DateOfPurchase'))
    !      job:POP = 'O'
    !  Else
    !  ! End: DBH 21/01/2009 #10365
    !  ! -----------------------------------------
    If job:POP = ''
        loc:Invalid = 'job:POP'
        loc:Alert = 'Job Type Required'
        Exit
    End ! If job:POP = ''
    !  End ! If job:POP = ''
      
    If p_web.GSV('job:Chargeable_Job') <> 'YES' And |
        p_web.GSV('job:Warranty_Job') <> 'YES'
        loc:Invalid = 'job:Chargeable_Job'
        loc:Alert = 'You must "Chargeable" or "Warranty" job.'
        Exit
    End ! If p_web.GSV('job:Chargeable_Job') <> 'YES' And |
      
    If p_web.GSV('job:Chargeable_Job') = 'YES' And |
        p_web.GSV('job:Charge_Type') = ''
        loc:Invalid = 'job:Charge_Type'
        loc:Alert = 'Chargeable Charge Type Required'
        Exit
    End ! p_web.GSV('job:Charge_Type') = ''
      
    If p_web.GSV('job:Warranty_Job') = 'YES' And |
        p_web.GSV('job:Warranty_Charge_Type') = ''    
        loc:Invalid = 'job:Warranty_Charge_Type'
        loc:Alert = 'Warranty Charge Type Required'
        Exit
    End ! If p_web.GSV('job:Warranty_Job') = 'YES'
      
    If p_web.GSV('Required:Courier') = 1
        If p_web.GSV('job:Courier') = ''
            loc:Invalid = 'job:Courier'
            loc:Alert = 'Courier Required'
            Exit
        End ! If p_web.GSV('job:Courier') = ''
    End ! If p_web.GSV('Required:OutgoingCourier') = 1
      
    If p_web.GSV('jobe:Booking48HourOption') < 1
        loc:Invalid = 'jobe:Booking48HourOption'
        loc:Alert = 'Booking Option Required'
        Exit
    End ! If p_web.GSV('jobe:Booking48HourOption') = ''
      
    If p_web.GSV('jobe2:SMSNotification') = 1
        If p_web.GSV('jobe2:SMSAlertNumber') = ''
            If p_web.GSV('job:Mobile_Number') = ''
                loc:Invalid = 'job:Account_Number'
                loc:Alert = 'SMS Alert Number Required'
                Exit
            Else ! If p_web.GSV('job:Mobile_Number') = ''
                p_web.SSV('jobe2:SMSAlertNumber',p_web.GSV('job:Mobile_Number'))
            End ! If p_web.GSV('job:Mobile_Number') = ''
        End ! If p_web.GSV('jobe2:SMSAlertNumber') = ''
    End ! If p_web.GSV('jobe2:SMSNotification') = 1
      
! Write Remaining Job Fields
    If p_web.GSV('BookingSite') = 'ARC'
        job:Location = GETINI('RRC','ARCLocation',,Clip(Path()) & '\SB2KDEF.INI')
        p_web.SSV('job:Location',job:Location)
    Else ! If p_web.GSV('BookingSite') = 'ARC'
        job:Location = GETINI('RRC','RRCLocation',,CLIP(PATH())&'\SB2KDEF.INI')
        p_web.SSV('job:Location',job:Location)
    End ! If p_web.GSV('BookingSite') = 'ARC'

!      job:DOP = Deformat(p_web.GSV('tmp:DOP'),@d06)
!      p_web.SSV('job:DOP',job:DOP)


    ! Insert --- This is done above. But doesn't work, and I'm not delving into why (DBH: 19/03/2009) #10671
    job:MSN = Upper(p_web.GSV('job:MSN'))
    ! end --- (DBH: 19/03/2009) #10671

    !Write Fault Code

    Loop x# = 1 To 6
        If p_web.GSV('Hide:FaultCode' & x#) <> 1
            Access:MANFAULT.Clearkey(maf:Field_Number_Key)
            maf:Manufacturer = p_web.GSV('job:Manufacturer')
            Set(maf:Field_Number_Key,maf:Field_Number_Key)
            Loop
                If Access:MANFAULT.Next()
                    Break
                End ! If Access:MANFAULT.Next()
                If maf:Manufacturer <> p_web.GSV('job:Manufacturer')
                    Break
                End ! If maf:Manufacturer <> p_web.GSV('job:Manufacturer')
                If maf:Field_Name = p_web.GSV('Prompt:FaultCode' & x#)
                    If maf:Field_Number < 13
                        Case maf:Field_Number
                        Of 1
                            job:Fault_Code1 = p_web.GSV('tmp:FaultCode' & x#)
                        Of 2
                            job:Fault_Code2 = p_web.GSV('tmp:FaultCode' & x#)
                        Of 3
                            job:Fault_Code3 = p_web.GSV('tmp:FaultCode' & x#)
                        Of 4
                            job:Fault_Code4 = p_web.GSV('tmp:FaultCode' & x#)
                        Of 5
                            job:Fault_Code5 = p_web.GSV('tmp:FaultCode' & x#)
                        Of 6
                            job:Fault_Code6 = p_web.GSV('tmp:FaultCode' & x#)
                        Of 7
                            job:Fault_Code7 = p_web.GSV('tmp:FaultCode' & x#)
                        Of 8
                            job:Fault_Code8 = p_web.GSV('tmp:FaultCode' & x#)
                        Of 9
                            job:Fault_Code9 = p_web.GSV('tmp:FaultCode' & x#)
                        Of 10
                            job:Fault_Code10 = p_web.GSV('tmp:FaultCode' & x#)
                        Of 11
                            job:Fault_Code11 = p_web.GSV('tmp:FaultCode' & x#)
                        Of 12
                            job:Fault_Code12 = p_web.GSV('tmp:FaultCode' & x#)
                        End ! Case maf:Field_Number
                        !p_web.SSV('job:Fault_Code' & maf:Field_Number,p_web.GSV('tmp:FaultCode') & x#)
                        
                    End ! If maf:Field_Number < 13
                    If maf:Field_Number > 12
                        p_web.SSV('wob:FaultCode' & maf:Field_Number,p_web.GSV('tmp:FaultCOde' & x#))
                    End ! If maf:Field_Number > 12
                    Break
                End ! If maf:Field_Name = p_web.GSV('Prompt:FaultCode' & x#)
            End ! Loop
        End ! If p_web.GSV('Hide:FaultCode' & x#) <> 1
    End ! Loop x# = 1 To 6

    !Estimate Check
    Access:CHARTYPE.ClearKey(cha:Warranty_Key)
    cha:Charge_Type = p_web.GSV('job:Charge_Type')
    cha:Warranty = 'NO'
    If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
        !Found
        If cha:Force_Estimate = 'YES'
            p_web.SSV('job:Estimate','YES')
            job:Estimate = p_web.GSV('job:Estimate')
        Else ! If cha:Force_Estimate = 'YES'
            Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
            sub:Account_Number = p_web.GSV('job:Account_Number')
            If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
                !Found
                Access:TRADEACC.ClearKey(tra:Account_Number_Key)
                tra:Account_Number = sub:Main_account_Number
                If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                    !Found
                    If tra:Use_Sub_Accounts = 'YES' And tra:Invoice_Sub_Accounts = 'YES'
                        If sub:ForceEstimate
                            p_web.SSV('job:Estimate_If_Over',sub:EstimateIfOver)
                            p_web.SSV('job:Estimate','YES')
                        End ! If sub:ForceEstimate
                    Else ! If tra:Use_Sub_Accounts = 'YES' And tra:Invoice_Sub_Accounts = 'YES'
                        If tra:Force_Estimate = 'YES'
                            p_web.SSV('job:Estimate_If_Over',tra:Estimate_If_Over)
                            p_web.SSV('job:Estimate','YES')
                        End ! If tra:Force_Estimate = 'YES'
                    End ! If tra:Use_Sub_Accounts = 'YES' And tra:Invoice_Sub_Accounts = 'YES'
                    p_web.SSV('job:Estimate_If_Over',job:Estimate_If_Over)
                    p_web.SSV('job:Estimate',job:Estimate)
                Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                    !Error
                End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign

            Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
                !Error
            End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        End ! If cha:Force_Estimate = 'YES'
    Else ! If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
        !Error
    End ! If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign


    DuplicateTabCheckDelete(p_web,'NEWJOBBOOKING','') ! Clear SessionID
    DuplicateTabCheckDelete(p_web,'NEWJOBBOOKINGIMEI','')
! NET:WEB:StagePOST
PostInsert      Routine
  !Write Child Records After JOBS
      If job:Ref_Number > 0
          p_web.SetSessionValue('job:Ref_Number',job:Ref_Number)
  
          local.SetStatus('JOB',0)
  
          Access:TRANTYPE.Clearkey(trt:Transit_Type_Key)
          trt:Transit_Type = p_web.GSV('job:Transit_Type')
          If Access:TRANTYPE.TryFetch(trt:Transit_Type_Key) = Level:Benign
              ! OK
  
          Else ! If Access:TRANTYPE.TryFetch(trt:Transit_Type_Key) = Level:Benign
              ! Error
          End ! If Access:TRANTYPE.TryFetch(trt:Transit_Type_Key) = Level:Benign
  
          local.SetStatus('JOB',Sub(trt:Initial_Status,1,3))
          local.SetStatus('EXC',Sub(trt:ExchangeStatus,1,3))
          local.SetStatus('LOA',Sub(trt:LoanStatus,1,3))
  
          If p_web.GSV('jobe:HubRepair') = 1 And p_web.GSV('BookingSite') = 'RRC'
              local.SetStatus('JOB',Sub(GETINI('RRC','StatusSendToARC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3))
          End ! If jobe:HubRepair And p_web.GSV('BookingSite') = 'ARC'
  
          If p_web.GSV('job:Estimate') = 'YES' AND p_web.GSV('job:Chargeable_Job') = 'YES'
              local.SetStatus('JOB',505)
              
          End ! If job:Estimate = 'YES'
  
          ! DBH #10544 - If Liquid Damage, send to Hub
          IF (p_web.GSV('jobe:Booking48HourOption') = 4)
              p_web.SSV('jobe:HubRepair',1)
              p_web.SSV('jobe:HubRepairDate',TODAY())
              p_web.SSV('jobe:HubRepairTime',CLOCK())
              local.SetStatus('JOB',Sub(GETINI('RRC','StatusSendToARC',,clip(path()) & '\SB2KDEF.INI'), 1, 3))
          END ! IF (p_web.GSV('jobe:Booking48HourOption') = 4)        
          
          job:Current_Status = p_web.GSV('job:Current_Status')
          job:PreviousStatus = p_web.GSV('job:PreviousStatus')
  
          job:StatusUser = p_web.GSV('job:StatusUser')
          job:Status_End_Time = p_web.GSV('job:Status_End_Time')
          job:Status_End_Date = p_web.GSV('job:Status_End_Date')
          job:Exchange_Status = p_web.GSV('job:Exchange_Status')
          job:Loan_Status = p_web.GSV('job:Loan_Status')
          job:Warranty_job = p_web.GSV('job:Warranty_Job')
  
  
  
          
          If p_web.GSV('jobe:HubRepair') = 1
              Access:REPTYDEF.Clearkey(rtd:ManRepairTypeKey)
              rtd:Manufacturer    = job:Manufacturer
              Set(rtd:ManRepairTypeKey,rtd:ManRepairTypeKey)
              Loop
                  If Access:REPTYDEF.Next()
                      Break
                  End ! If Access:REPTYDEF.Next()
                  If rtd:Manufacturer    <> job:Manufacturer
                      Break
                  End ! If rtd:Manufacturer    <> job:Manufacturer
                  If rtd:BER = 11
                      If job:Chargeable_Job = 'YES' And job:Repair_Type = ''
                          job:Repair_Type = rtd:Repair_Type
                      End ! If job:Chargeable_Job = 'YES' and job:Repair_Type = ''
                      If job:Warranty_Job = 'YES' And job:Repair_Type_Warranty = ''
                          job:Repair_Type_Warranty = rtd:Repair_Type
                      End ! If job:Warranty_Job = 'YES' And job:Repair_Type_Warranty = ''
  
                      Break
                  End ! If rtd:BER = 11
              End ! Loop
          End ! If p_web.GSV('jobe:HubRepair') = 1
          
            IF (p_web.GSV('PreBookingRefNo') > 0) ! #12628 Save prejob refnumber (DBH: 10/10/2012)
                ! Prebooked job. Save Ref No
                Access:PREJOB.ClearKey(PRE:keyRefNumber)
                PRE:RefNumber = p_web.GSV('PreBookingRefNo')
                IF (Access:PREJOB.TryFetch(PRE:keyRefNumber) = Level:Benign)
                    PRE:JobRef_Number = job:Ref_Number
                    Access:PREJOB.TryUpdate()
                END ! IF
                
            END ! IF
          
  
          Access:JOBS.Update()
  !
          If Access:LOCATLOG.PrimeRecord() = Level:Benign
              lot:RefNumber = job:Ref_Number
              lot:TheDate = Today()
              lot:TheTime = Clock()
              lot:UserCode = p_web.GSV('BookingUserCode')
              lot:PreviousLocation = ''
              lot:NewLocation = job:Location
              If Access:LOCATLOG.TryInsert() = Level:Benign
  
              Else ! If Access:LOCATLOG.TryInsert() = Level:Benign
                  Access:LOCATLOG.CancelAutoInc()
              End ! If Access:LOCATLOG.TryInsert() = Level:Benign
  
          End ! If Access:LOCATLOG.PrimeRecord() = Level:Benign
  !        pushpack(p_web,'<span class="NoShow">x</span>')
          If Access:JOBSE.PrimeRecord() = Level:Benign
              rec# = jobe:RecordNumber
              jobe:EndUserEmailAddress    = p_web.GSV('jobe:EndUserEmailAddress')
              jobe:HubRepair              = p_web.GSV('jobe:HubRepair')
              jobe:Network                = p_web.GSV('jobe:Network')
              jobe:HubRepairDate          = p_web.GSV('jobe:HubRepairDate')
              jobe:HubRepairTime          = p_web.GSV('jobe:HubRepairTime')
              jobe:BoxESN                 = p_web.GSV('jobe:BoxESN')
              jobe:ValidPOP               = p_web.GSV('jobe:ValidPOP')
              jobe:ReturnDate             = p_web.GSV('jobe:ReturnDate')
              jobe:TalkTime               = p_web.GSV('jobe:TalkTime')
              jobe:OriginalPackaging      = p_web.GSV('jobe:OriginalPackaging')
              jobe:OriginalBattery        = p_web.GSV('jobe:OriginalBattery')
              jobe:OriginalCharger        = p_web.GSV('jobe:OriginalCharger')
              jobe:OriginalAntenna        = p_web.GSV('jobe:OriginalAntenna ')
              jobe:OriginalManuals        = p_web.GSV('jobe:OriginalManuals')
              jobe:PhysicalDamage         = p_web.GSV('jobe:PhysicalDamage')
              jobe:OriginalDealer         = p_web.GSV('jobe:OriginalDealer')
              jobe:BranchOfReturn         = p_web.GSV('jobe:BranchOfReturn')
              jobe:WebJob                 = p_web.GSV('jobe:WebJob')
              jobe:OBFvalidated           = p_web.GSV('jobe:OBFvalidated')
              jobe:OBFvalidateDate        = p_web.GSV('jobe:OBFvalidateDate')
              jobe:OBFvalidateTime        = p_web.GSV('jobe:OBFvalidateTime')
              jobe:EndUserTelNo           = p_web.GSV('jobe:EndUserTelNo')
              jobe:Booking48HourOption    = p_web.GSV('jobe:Booking48HourOption')
              jobe:Engineer48HourOption   = p_web.GSV('jobe:Engineer48HourOption')
              jobe:POPType                = p_web.GSV('tmp:PopType')
              
  
  !            p_web.SessionQueueToFile(JOBSE)
              jobe:RecordNumber = rec#
              jobe:RefNumber = job:Ref_Number
  
              If jobe:OBFValidated = 1
                  If Access:JOBACC.PrimeRecord() = Level:Benign
                      jac:Ref_Number = job:Ref_Number
                      jac:Accessory = 'MANUALS'
                      jac:Damaged = 0
                      If jobe:WebJob
                          jac:Attached = False
                      Else ! If jobe:WebJob
                          jac:Attached = True
                      End ! If jobe:WebJob
                      If Access:JOBACC.TryInsert() = Level:Benign
                          !Insert
                      Else ! If Access:JOBACC.TryInsert() = Level:Benign
                          Access:JOBACC.CancelAutoInc()
                      End ! If Access:JOBACC.TryInsert() = Level:Benign
                  End ! If Access.JOBACC.PrimeRecord() = Level:Benign
  
                  If Access:JOBACC.PrimeRecord() = Level:Benign
                      jac:Ref_Number = job:Ref_Number
                      jac:Accessory = 'ORIGINAL PACKAGING'
                      jac:Damaged = 0
                      If jobe:WebJob
                          jac:Attached = False
                      Else ! If jobe:WebJob
                          jac:Attached = True
                      End ! If jobe:WebJob
                      If Access:JOBACC.TryInsert() = Level:Benign
                          !Insert
                      Else ! If Access:JOBACC.TryInsert() = Level:Benign
                          Access:JOBACC.CancelAutoInc()
                      End ! If Access:JOBACC.TryInsert() = Level:Benign
                  End ! If Access.JOBACC.PrimeRecord() = Level:Benign
  
                  AddToAudit(p_web,job:Ref_Number,'JOB',|
                                   'O.B.F. VALIDATION ACCEPTED',|
                                   'ORIGINAL DEALER <13,10>'& jobe:OriginalDealer & '<13,10>BRANCH OF RETURN<13,10>'&  jobe:BranchOfReturn)
  
                  If Access:JOBSOBF.PrimeRecord() = Level:Benign
                      jof:RefNumber            = job:Ref_Number
                      jof:IMEINumber           = job:ESN
                      jof:Status               = 1
                      If p_web.GSV('tmp:Replacement') = 1
                          jof:Replacement          = 1
                      End ! If p_web.GSV('tmp:Replacement') = 1
                      If p_web.GSV('tmp:Replacement') = 0
                          jof:Replacement          = 0
                      End ! If p_web.GSV('tmp:Replacement') = 1
                      jof:UserCode             = job:Who_Booked
                      jof:HeadAccountNumber    = p_web.GSV('BookingAccount')
                      If Access:JOBSOBF.TryInsert() = Level:Benign
                          !Insert
                      Else ! If Access:JOBSOBF.TryInsert() = Level:Benign
                          Access:JOBSOBF.CancelAutoInc()
                      End ! If Access:JOBSOBF.TryInsert() = Level:Benign
                  End ! If Access.JOBSOBF.PrimeRecord() = Level:Benign
  
  
              Else ! If jobe:OBFValidated = 1
                  If p_web.GSV('OBFValidation') = 'Failed'
                      AddToAudit(p_web,job:Ref_Number,'JOB','O.B.F. VALIDATION FAILED',Clip(p_web.GSV('Error:Validation')))
                  End ! If p_web.GSV('OBFValidation') = 'Failed'
              End ! If jobe:OBFValidated = 1
  
              If jobe:HubRepair = 1 And p_web.GSV('BookingSite') = 'RRC'
                  ! Add to waybill list (DBH: 06/03/2008)
                  Access:WAYBAWT.Clearkey(wya:AccountJobNumberKey)
                  wya:AccountNumber = p_web.GSV('BookingAccount')
                  wya:JobNumber = job:Ref_Number
                  If Access:WAYBAWT.TryFetch(wya:AccountJobNumberKey)
                      If Access:WAYBAWT.PrimeRecord() = Level:Benign
                          wya:JobNumber = job:Ref_Number
                          wya:AccountNumber = p_web.GSV('BookingAccount')
                          wya:Manufacturer = job:Manufacturer
                          wya:ModelNumber = job:Model_Number
                          wya:IMEINumber = job:ESN
                          If Access:WAYBAWT.TryUpdate() = Level:Benign
  
                          Else ! If Access:WAYBAWT.TryUpdate() = Level:Benign
                              Access:WAYBAWT.CancelAutoInc()
                          End ! If Access:WAYBAWT.TryUpdate() = Level:Benign
                      End ! If Access:WAYBAWT.PrimeRecord() = Level:Benign
                  End ! If Access:WAYBAWT.TryFetch(wya:AccountJobNumberKey)
              End ! If jobe:HubRepair = 1 And p_web.GSV('BookingSite') = 'RRC'
  
              If Access:JOBSE.TryInsert() = Level:Benign
  
              Else ! If Access:JOBSE.TryInsert() = Level:Benign
                  Access:JOBSE.CancelAutoInc()
              End ! If Access:JOBSE.TryInsert() = Level:Benign
          End ! If Access:JOBSE.PrimeRecord() = Level:Benign
  !
          Access:WEBJOBNO.ClearKey(wej:HeadAccountNumberKey)
          wej:HeadAccountNumber = p_web.GSV('BookingAccount')
          If Access:WEBJOBNO.TryFetch(wej:HeadAccountNumberKey) = Level:Benign
              !Found
              wej:LastWEBJOBNumber += 1
              Access:WEBJOBNO.Update()
              JobNumber# = wej:LastWEBJOBNumber
          Else ! If Access:WEBJOBNO.TryFetch(wej:HeadAccountNumberKy) = Level:Benign
              !Error
              If Access:WEBJOBNO.PrimeRecord() = Level:Benign
                  wej:HeadAccountNumber = p_web.GSV('BookingAccount')
                  wej:LastWEBJOBNumber = 1
                  JobNumber# = 1
                  If Access:WEBJOBNO.TryInsert() = Level:Benign
                      !Insert
                  Else ! If Access:WEBJOBNO.TryInsert() = Level:Benign
                      Access:WEBJOBNO.CancelAutoInc()
                  End ! If Access:WEBJOBNO.TryInsert() = Level:Benign
              End ! If Access.WEBJOBNO.PrimeRecord() = Level:Benign
          End ! If Access:WEBJOBNO.TryFetch(wej:HeadAccountNumberKy) = Level:Benign
  !
  !        pushpack(p_web,'<span class="NoShow">x</span>')
  !
          If Access:WEBJOB.PrimeRecord() = Level:Benign
              rec# = wob:RecordNumber
  
              wob:JobNumber = JobNumber#
              wob:RecordNumber = rec#
              wob:RefNumber = job:Ref_Number
              wob:HeadAccountNumber    = p_web.GSV('BookingAccount')
              wob:SubAcountNumber = job:Account_Number
              wob:OrderNumber = job:Order_Number
              wob:IMEINumber = job:ESN
              wob:MSN = job:MSN
              wob:Manufacturer = job:Manufacturer
              wob:ModelNumber = job:Model_Number
              wob:MobileNumber = job:Mobile_Number
              wob:EDI = job:EDI
              wob:Current_Status = job:Current_Status
              wob:Exchange_Status = job:Exchange_Status
              wob:Current_Status_Date = TODAY()
              wob:Loan_Status = job:Loan_Status
              wob:DateBooked = job:Date_Booked
              wob:TimeBooked = job:Time_Booked
              wob:FaultCode13 = p_web.GSV('wob:FaultCode13')
              wob:FaultCode14 = p_web.GSV('wob:FaultCode14')
              wob:FaultCode15 = p_web.GSV('wob:FaultCode15')
              wob:FaultCode16 = p_web.GSV('wob:FaultCode16')
              wob:FaultCode17 = p_web.GSV('wob:FaultCode17')
              wob:FaultCode18 = p_web.GSV('wob:FaultCode18')
              wob:FaultCode19 = p_web.GSV('wob:FaultCode19')
              wob:FaultCode20 = p_web.GSV('wob:FaultCode20')
  
              If Access:WEBJOB.TryInsert() = Level:Benign
  
              Else ! If Access:WEBJOB.TryInsert() = Level:Benign
                  Access:WEBJOB.CancelAutoInc()
              End ! If Access:WEBJOB.TryInsert() = Level:Benign
          End ! If Access:WEBJOB.PrimeRecord() = Level:Benign
  !
          If Access:JOBSE2.PrimeRecord() = Level:Benign
              rec# = jobe2:RecordNumber
              jobe2:RecordNumber = rec#
              jobe2:RefNumber = job:Ref_Number
              jobe2:IDNumber  = p_web.GSV('jobe2:IDNumber')
              jobe2:Contract  = p_web.GSV('jobe2:Contract')
              jobe2:Prepaid  = p_web.GSV('jobe2:Prepaid')
              jobe2:WarrantyRefNo  = p_web.GSV('jobe2:WarrantyRefNo')
              jobe2:XAntenna  = p_web.GSV('jobe2:XAntenna')
              jobe2:XLens  = p_web.GSV('jobe2:XLens')
              jobe2:XFCover  = p_web.GSV('jobe2:XFCover')
              jobe2:XBCover  = p_web.GSV('jobe2:XBCover')
              jobe2:XKeypad  = p_web.GSV('jobe2:XKeypad')
              jobe2:XBattery  = p_web.GSV('jobe2:XBattery')
              jobe2:XCharger  = p_web.GSV('jobe2:XCharger')
              jobe2:XLCD  = p_web.GSV('jobe2:XLCD')
              jobe2:XSimReader  = p_web.GSV('jobe2:XSimReader')
              jobe2:XSystemConnector  = p_web.GSV('jobe2:XSystemConnector')
              jobe2:XNone  = p_web.GSV('jobe2:XNone')
              jobe2:XNotes  = p_web.GSV('jobe2:XNotes')
              jobe2:SMSNotification  = p_web.GSV('jobe2:SMSNotification')
              jobe2:EmailNotification  = p_web.GSV('jobe2:EmailNotification')
              jobe2:SMSAlertNumber  = p_web.GSV('jobe2:SMSAlertNumber')
              jobe2:EmailAlertAddress  = p_web.GSV('jobe2:EmailAlertAddress')
              jobe2:CourierWaybillNumber  = p_web.GSV('jobe2:CourierWaybillNumber')
              jobe2:HubCustomer  = p_web.GSV('jobe2:HubCustomer')
              jobe2:HubCollection  = p_web.GSV('jobe2:HubCollection')
              jobe2:HubDelivery  = p_web.GSV('jobe2:HubDelivery')
              jobe2:POPConfirmed = p_web.GSV('jobe2:POPConfirmed')
              
              If Access:JOBSE2.TryInsert() = Level:Benign
                  If jobe2:SMSNotification
                      AddEmailSMS(job:Ref_Number,wob:HeadAccountNumber,'BOOK','SMS',jobe2:SMSAlertNumber,'',0,'')
                  End ! If jobe2:SMSNotification
  
                  If jobe2:EmailNotification
                      AddEmailSMS(job:Ref_Number,wob:HeadAccountNumber,'BOOK','EMAIL','',jobe2:EmailAlertAddress,0,'')
                  End ! If jobe2:EmailNotification
              Else ! If Access:JOBSE2.TryInsert() = Level:Benign
                  Access:JOBSE2.CancelAutoInc()
              End ! If Access:JOBSE2.TryInsert() = Level:Benign
          End ! If Access:JOBSE2.PrimeRecord() = Level:Benign
  !
          If Access:JOBNOTES.PrimeRecord() = Level:Benign
              jbn:RefNumber = job:Ref_Number
              jbn:Fault_Description = p_web.GSV('jbn:Fault_Description')
              jbn:Engineers_Notes = p_web.GSV('jbn:Engineers_Notes')
              If Access:JOBNOTES.TryInsert() = Level:Benign
  
              End ! If Access:JOBNOTES.TryInsert() = Level:Benign
          End ! If Access:JOBSNOTES.PrimeRecord() = Level:Benign
  !
  !        pushpack(p_web,'<span class="NoShow">x</span>')
  !
          tmp:AuditNotes = ''
          tmp:AuditNotes         = 'UNIT DETAILS: ' & Clip(job:manufacturer) & ' ' & Clip(job:model_number) & ' - ' & Clip(job:unit_type) & |
                                  '<13,10>I.M.E.I.: ' & Clip(job:esn) & |
                                  '<13,10>M.S.N.: ' & Clip(job:msn)
          If job:Chargeable_job = 'YES'
              tmp:AuditNotes = Clip(tmp:AuditNotes) & '<13,10>CHA. CHARGE TYPE: ' & Clip(job:Charge_Type)
          End !If job:Chargeable_job = 'YES'
          If job:Warranty_Job = 'YES'
              tmp:AuditNotes = Clip(tmp:AuditNotes) & '<13,10>WAR. CHARGE TYPE: ' & Clip(job:Warranty_Charge_Type)
          End !If job:Warranty_Job = 'YES'
  
          tmp:AuditNotes = Clip(tmp:AuditNotes) & '<13,10>BOOKING OPTION: '
          Case jobe:Booking48HourOption
              Of 1
                  tmp:AuditNotes = Clip(tmp:AuditNotes) & ' 48 HOUR EXCHANGE'
              Of 2
                  tmp:AuditNotes = Clip(tmp:AuditNotes) & ' ARC REPAIR'
              Of 3
                  tmp:AuditNotes = Clip(tmp:AuditNotes) & ' 7 DAY TAT'
          End !Case tmp:Booking48HourOption
  
          If jobe2:Contract = 1
              tmp:AuditNotes = Clip(tmp:AuditNotes) & '<13,10>CONTRACT'
          Else ! If jobe2:Contract = 1
              If jobe2:Prepaid = 1
                  tmp:AuditNotes = Clip(tmp:AuditNotes) & '<13,10>PREPAID'
              End ! If jobe2:Prepaid = 1
          End ! If jobe2:Contract = 1
  
          If Clip(jobe2:IDNumber) <> ''
              tmp:AuditNotes = Clip(tmp:AuditNotes) & '<13,10>ID NO: ' & Clip(jobe2:IDNumber)
          End !If Clip(jobe2:IDNumber) <> ''
  
          If Clip(p_web.GSV('tmp:TheJobAccessory')) <> ''
              Loop x# = 1 To 1000
                  If Sub(p_web.GSV('tmp:TheJobAccessory'),x#,2) = '|;'
                      Start# = x# + 2
                      Cycle
                  End ! If Sub(p_web.GSV('tmp:TheJobAccessory'),x#,2) = '|;'
  
                  If Start# > 0
                      If Sub(p_web.GSV('tmp:TheJobAccessory'),x#,2) = ';|'
                          tmp:FoundAccessory = Sub(p_web.GSV('tmp:TheJobAccessory'),Start#,x# - Start#)
  
                          If tmp:FoundAccessory <> ''
                              If Access:JOBACC.PrimeRecord() = Level:Benign
                                  jac:Ref_Number = job:Ref_Number
                                  jac:Accessory = Clip(tmp:FoundAccessory)
                                  jac:Damaged = 0
                                  
                                  If jobe:WebJob
                                      jac:Attached = False
                                  Else !If jobe:WebJob
                                      jac:Attached = True
                                  End !If jobe:WebJob
                                  If Access:JOBACC.TryInsert() = Level:Benign
                                  Else !If Access:JOBACC.TryInsert() = Level:Benign
                                      Access:JOBACC.CancelAutoInc()
                                  End !If Access:JOBACC.TryInsert() = Level:Benign
  
                              End ! If Access:JOBACC.PrimeRcord() = Level:Benign
  
                          End ! If tmp:FoundAccessory <> ''
                          Start# = 0
                      End ! If Sub(p_web.GSV('tmp:TheJobAccessory'),x#,2) = ';|'
                  End ! If Start# > 0
              End ! Loop x# = 1 To 1000
  
              If Start# > 0
                  tmp:FoundAccessory = Clip(Sub(p_web.GSV('tmp:TheJobAccessory'),Start#,30))
                  If tmp:FoundAccessory <> ''
                      If Access:JOBACC.PrimeRecord() = Level:Benign
                          jac:Ref_Number = job:Ref_Number
                          jac:Accessory = Clip(tmp:FoundAccessory)
                          jac:Damaged = 0
                          If jobe:WebJob
                              jac:Attached = False
                          Else !If jobe:WebJob
                              jac:Attached = True
                          End !If jobe:WebJob
                          If Access:JOBACC.TryInsert() = Level:Benign
                          Else !If Access:JOBACC.TryInsert() = Level:Benign
                              Access:JOBACC.CancelAutoInc()
                          End !If Access:JOBACC.TryInsert() = Level:Benign
  
                      End ! If Access:JOBACC.PrimeRcord() = Level:Benign
                  End ! If tmp:FoundAccessory <> ''
              End ! If Start# > 0
          End ! If Clip(p_web.GSV('tmp:TheJobAccessory') <> ''
  
          count# = 0
          access:jobacc.clearkey(jac:ref_number_key)
          jac:ref_number = job:ref_number
          set(jac:ref_number_key,jac:ref_number_key)
          loop
              if access:jobacc.next()
                 break
              end !if
              if jac:ref_number <> job:ref_number      |
                  then break.  ! end if
              count# += 1
              tmp:AuditNotes = Clip(tmp:AuditNotes) & '<13,10>ACC. ' & Clip(count#) & ': ' & Clip(jac:accessory)
          end !loop
  
          AddToAudit(p_web,job:Ref_Number,'JOB','ONLINE NEW JOB BOOKING INTIAL ENTRY',Clip(tmp:AuditNotes))
  
          
          If (jobe2:POPConfirmed) ! #11912 New field (Bryan: 01/02/2011)
              AddToAudit(p_web,job:Ref_Number,'JOB','POP CONFIRMED','')
          END
          
  
          If jobe:VSACustomer
              CID_XML(job:Mobile_Number,p_web.GSV('BookingAccount'),1)
          End ! If jobe:VSACustomer
  
          If p_web.GSV('IMEIValidation') = 'Failed'
              AddToAudit(p_web,job:Ref_Number,'JOB','IMEI VALIDATION FAILED','AN ERROR OCCURED DURING THE AUTOMATIC VALIDATION PROCESS. PLEASE VALIDATE THIS I.M.E.I. NUMBER MANUALLY')
  
              If Access:JOBOUTFL.PrimeRecord() = Level:Benign
                  joo:JobNumber = job:Ref_Number
                  joo:FaultCode = 0
                  joo:Description = 'IMEI VALIDATION: AN ERROR OCCURED DURING THE AUTOMATIC VALIDATION PROCESS. PLEASE VALIDATE THIS I.M.E.I. MANUALLY'
                  joo:Level = 0
                  If Access:JOBOUTFL.TryInsert() = Level:Benign
                      !Insert
                  Else ! If Access:JOBOUTFL.TryInsert() = Level:Benign
                      Access:JOBOUTFL.CancelAutoInc()
                  End ! If Access:JOBOUTFL.TryInsert() = Level:Benign
              End ! If Access.JOBOUTFL.PrimeRecord() = Level:Benign
          End ! If p_web.GSV('IMEIValidation') = 'Failed'
  
          If p_web.GSV('IMEIValidation') = 'Passed'
              If Access:JOBOUTFL.PrimeRecord() = Level:Benign
                  joo:JobNumber = job:Ref_Number
                  joo:FaultCode = 0
                  joo:Description = 'IMEI VALIDATION: ' & Upper(Clip(p_web.GSV('IMEIValidationText'))) & '<13,10>ACTIVATION DATE: ' & p_web.GSV('IMEIActivationDate')
                  joo:Level = 0
                  If Access:JOBOUTFL.TryInsert() = Level:Benign
                      ! Insert Successful
                  Else ! If Access:JOBOUTFL.TryInsert() = Level:Benign
                      ! Insert Failed
                      Access:JOBOUTFL.CancelAutoInc()
                  End ! If Access:JOBOUTFL.TryInsert() = Level:Benign
              End !If Access:JOBOUTFL.PrimeRecord() = Level:Benign
              Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
              jbn:RefNumber = job:Ref_Number
              If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
                  !Found
                  If Clip(jbn:Fault_Description) <> ''
                      jbn:Fault_Description = Clip(jbn:Fault_Description) & '<13,10>' & p_web.GSV('IMEIValidationText')
                  Else ! If Clip(jbn:Fault_Description) <> ''
                      jbn:Fault_Description = p_web.GSV('IMEIValidationText')
                  End ! If Clip(jbn:Fault_Description) <> ''
              Else ! If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
                  !Error
              End ! If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
          End ! If p_web.GSV('IMEIValidation') = 'Passed'
  
          If Access:JOBSOBF.PrimeRecord() = Level:Benign
              rec# = jof:RecordNumber
              jof:RecordNumber = rec#
              jof:RefNumber = job:Ref_Number
              jof:IMEINumber = p_web.GSV('job:ESN')
              jof:Status = 1
              jof:UserCode = p_web.GSV('BookingUserCode')
              jof:HeadAccountNumber = p_web.GSV('BookingAccountNumber')
              jof:Replacement = p_web.GSV('jof:Replacement')
              jof:StoreReferenceNumber = p_web.GSV('jof:StoreReferenceNumber')
              jof:RNumber     = p_web.GSV('jof:RNumber')
              jof:RejectionReason  = p_web.GSV('jof:RejectionReason')
              jof:ReplacementIMEI  = p_web.GSV('jof:ReplacementIMEI')
              jof:LAccountNumber = p_web.GSV('jof:LAccountNumber')
              If Access:JOBSOBF.TryInsert() = Level:Benign
                  ! OK
              Else ! If Access:JOBSOBF.TryInsert() = Level:Benign
                  ! Error
                  Access:JOBSOBF.CancelAutoInc()
              End ! If Access:JOBSOBF.TryInsert() = Level:Benign
          End ! If Access:JOBSOBF.PrimeRecord() = Level:Benign
  
          if (p_web.GSV('Hide:ChangeDOP') = 0 and p_web.GSV('tmp:ChangeReason') <> '')
              AddToAudit(p_web,job:Ref_Number,'JOB','DOP OVERRIDE','PREV DOP: ' & format(p_web.GSV('tmp:OldDOP'),@d06) & |
                                                                  '<13,10>NEW DOP: ' & format(p_web.GSV('job:DOP'),@d06) & |
                                                                  '<13,10>REASON: ' & clip(p_web.GSV('tmp:ChangeReason')))
          end ! if (p_web.GSV('Hide:ChangeDOP') = 0)
          
          IF (p_web.GSV('locMobileValidated') = 1)
              ! DBH #11388 - MSISDN Check
              AddToAudit(p_web,job:Ref_Number,'JOB','MSISDN VALIDATION','ACTIVATION DATE: ' & p_web.GSV('locMobileActivateDate') & |
                  '<13,10>PHONE NAME: ' & Clip(p_web.GSV('locMobilePhoneName')) & |
                  '<13,10>LOYALTY STATUS: ' & Clip(p_web.GSV('locMobileLoyaltyStatus')) & |
                  '<13,10>LIFETIME VALUE: ' & Clip(p_web.GSV('loc:MobileLifeTimeValue')) & |
                  '<13,10>AVE SPEND: ' & p_web.GSV('locMobileAverageSpend') & |
                  '<13,10>UPGRADE DATE: ' & p_web.GSV('locMobileUpgradeDate') & |
                  '<13,10>ID NO: ' & Clip(p_web.GSV('locMobileIDNumber')))
              
          END ! IF (p_web.GSV('locMobileValidated') = 1)
          
          !Multiple Job Booking, add any previous contact history
            IF (p_web.GSV('mj:InProgress') = 1 AND p_web.GSV('mj:ContactHistory') <> 1 AND |
                p_web.GSV('mj:PreviousJobNumber') > 0)
              Access:CONTHIST.ClearKey(cht:Ref_Number_Key)
              cht:Ref_Number = p_web.GSV('mj:PreviousJobNumber')
              SET(cht:Ref_Number_Key,cht:Ref_Number_Key)
              LOOP UNTIL Access:CONTHIST.Next()
                  IF (cht:Ref_Number <> p_web.GSV('mj:PreviousJobNumber'))
                      BREAK
                  END
                  qContactHistory.chtRecord :=: cht:Record
                  Add(qContactHistory)
              END
              
              LOOP xx# = 1 TO RECORDS(qContactHistory)
                  GET(qContactHistory,xx#)
                  IF (Access:CONTHIST.PrimeRecord() = Level:Benign)
                      recNo# = cht:Record_Number
                      cht:Record :=: qContactHistory.chtRecord
                      cht:Record_Number = recNo#
                      cht:Ref_Number = job:Ref_Number
                      IF (Access:CONTHIST.TryInsert())
                          Access:CONTHIST.CancelAutoInc()
                      END
                  END
              END
                      
          END
            ! #12477 All done. Check to see if a SMS is required on any of the statuses. (DBH: 06/06/2012)
            SendSMSText('J','Y','N',p_web)
            SendSMSText('E','Y','N',p_web)
            SendSMSText('L','Y','N',p_web)
      End ! If job:Ref_Numer > 0
  
      Do DeleteVariables

PostCopy        Routine
  p_web.SetSessionValue('NewJobBooking:Primed',0)

PostUpdate      Routine
  p_web.SetSessionValue('NewJobBooking:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locPreviousJobNumber')
  p_web.StoreValue('')
  p_web.StoreValue('locPreviousOutFault')
  p_web.StoreValue('tmp:PreviousAddress')
  p_web.StoreValue('tmp:UsePreviousAddress')
  p_web.StoreValue('')
  p_web.StoreValue('jobe:Network')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('jobe2:POPConfirmed')
  p_web.StoreValue('locPOPTypePassword')
  p_web.StoreValue('tmp:POPType')
  p_web.StoreValue('jobe2:WarrantyRefNo')
  p_web.StoreValue('locMobileLifetimeValue')
  p_web.StoreValue('locMobileAverageSpend')
  p_web.StoreValue('locMobileLoyaltyStatus')
  p_web.StoreValue('locMobileUpgradeDate')
  p_web.StoreValue('locMobileIDNumber')
  p_web.StoreValue('FranchiseAccount')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('jobe:EndUserTelNo')
  p_web.StoreValue('jobe2:CourierWaybillNumber')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:AmendAccessories')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:ShowAccessory')
  p_web.StoreValue('tmp:TheAccessory')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:AmendFaultCodes')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:FaultCode1')
  p_web.StoreValue('tmp:FaultCode2')
  p_web.StoreValue('tmp:FaultCode3')
  p_web.StoreValue('tmp:FaultCode4')
  p_web.StoreValue('tmp:FaultCode5')
  p_web.StoreValue('tmp:FaultCode6')
  p_web.StoreValue('')
  p_web.StoreValue('jbn:Fault_Description')
  p_web.StoreValue('jbn:Engineers_Notes')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:ExternalDamageChecklist')
  p_web.StoreValue('jobe2:XNone')
  p_web.StoreValue('jobe2:XAntenna')
  p_web.StoreValue('jobe2:XLens')
  p_web.StoreValue('jobe2:XFCover')
  p_web.StoreValue('jobe2:XBCover')
  p_web.StoreValue('jobe2:XKeypad')
  p_web.StoreValue('jobe2:XBattery')
  p_web.StoreValue('jobe2:XCharger')
  p_web.StoreValue('jobe2:XLCD')
  p_web.StoreValue('jobe2:XSimReader')
  p_web.StoreValue('jobe2:XSystemConnector')
  p_web.StoreValue('jobe2:XNotes')
  p_web.StoreValue('')
  p_web.StoreValue('jobe:Booking48HourOption')
  p_web.StoreValue('jobe:VSACustomer')
  p_web.StoreValue('jobe2:Contract')
  p_web.StoreValue('jobe2:Prepaid')

PostDelete      Routine
local.CheckLength       Procedure(String f:Type)
local:String            CString(31)
local:LengthFrom        Long()
local:LengthTo          Long()
Code
    Case f:Type
    Of 'MOBILE'
        If p_web.GetSessionValue('job:Mobile_Number') = ''
            Return 0
        End ! If p_web.GetSessionValue('job:Mobile_Number') = ''
        local:LengthFrom = GETINI('COMPULSORY','MobileLengthFrom',,Clip(Path()) & '\SB2KDEF.INI')
        local:LengthTo = GETINI('COMPULSORY','MobileLengthTo',,Clip(Path()) & '\SB2KDEF.INI')
        local:String = p_web.GetSessionValue('job:Mobile_Number')
        If Len(local:String) < local:LengthFrom Or Len(local:String) > local:LengthTo
            If local:LengthFrom = local:LengthTo
                p_web.SetSessionValue('Comment:MobileNumber','Mobile Number should be ' & Clip(local:LengthFrom) & ' characters long.')
            Else ! If local:LengthFrom = local:LengthTo
                p_web.SetSessionValue('Comment:MobileNumber','Mobile Number should be ' & Clip(local:LengthFrom) & ' to ' & Clip(local:LengthTo) & ' characters long.')
            End ! If local:LengthFrom = local:LengthTo
            p_web.SetSessionValue('job:Mobile_Number','')
            Return 1
        End ! If Len(local:String) < local:LengthFrom Or Len(local:String) > local:LengthTo
    Of 'IMEI'
        job:Model_Number = p_web.GetSessionValue('job:Model_Number')
        If job:Model_Number = ''
            Return 0
        End ! If p_web.GetSessionValue('job:Model_Number') = '' Or p_web.GetSessionValue('job:Model_Number') = '- Select Model Number -'
        Access:MODELNUM.ClearKey(mod:Model_Number_Key)
        mod:Model_Number = job:Model_Number
        If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
            !Found
            local:String = p_web.GetSessionValue('job:ESN')
            If Len(local:String) < mod:ESN_Length_From Or Len(local:String) > mod:ESN_Length_To
                If mod:ESN_Length_To = mod:ESN_Length_From
                    p_web.SetSessionValue('Comment:IMEINumber','IMEI Number should be ' & Clip(mod:ESN_Length_To) & ' characters long.')
                Else ! If mod:ESN_Length_To = mod:ESN_Length_From
                    p_web.SetSessionValue('Comment:IMEINumber','IMEI Number should be ' & Clip(mod:ESN_Length_From) & ' to ' & |
                                                                Clip(mod:ESN_Length_To) & ' characters long.')
                End ! If mod:ESN_Length_To = mod:ESN_Length_From
                Return 1
            End ! If Len(local:String) < mod:ESN_Length_From Or Len(local:String) > mod:ESN_Length_To
        Else ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
            !Error
        End ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign

    Of 'MSN'
        If p_web.GetSessionValue('job:Model_Number') = '' Or p_web.GetSessionValue('job:Model_Number') = '- Select Model Number -'
            Return 0
        End ! If p_web.GetSessionValue('job:Model_Number') = '' Or p_web.GetSessionValue('job:Model_Number') = '- Select Model Number -'
        Access:MODELNUM.ClearKey(mod:Model_Number_Key)
        mod:Model_Number = p_web.GetSessionValue('job:Model_Number')
        If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
            !Found
            local:String = p_web.GetSessionValue('job:MSN')
            If Len(local:String) < mod:MSN_Length_From Or Len(local:String) > mod:MSN_Length_To
                If mod:MSN_Length_To = mod:MSN_Length_From
                    p_web.SetSessionValue('Comment:MSN','MSN should be ' & Clip(mod:MSN_Length_To) & ' characters long.')
                Else ! If mod:ESN_Length_To = mod:ESN_Length_From
                    p_web.SetSessionValue('Comment:MSN','MSN should be ' & Clip(mod:MSN_Length_From) & ' to ' & |
                                                                Clip(mod:MSN_Length_To) & ' characters long.')
                End ! If mod:ESN_Length_To = mod:ESN_Length_From
                p_web.SetSessionValue('job:MSN','')
                Return 1
            End ! If Len(local:String) < mod:ESN_Length_From Or Len(local:String) > mod:ESN_Length_To
        Else ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
            !Error
        End ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign

    End ! Case f:Type

    Return 0
local.FaultCode        Procedure(String f:FieldName,Byte f:FieldNumber)
Code
!    Access:MANFAULT.Clearkey(maf:Field_Number_Key)
!    maf:Manufacturer = p_web.GetSessionValue('job:Manufacturer')
!    Set(maf:Field_Number_Key,maf:Field_Number_Key)
!    Loop
!        If Access:MANFAULT.Next()
!            Break
!        End ! If Access:MANFAULT.Next()
!        If maf:Manufacturer <> p_web.GetSessionValue('job:Manufacturer')
!            Break
!        End ! If maf:Manufacturer <> p_web.GetSessionValue('job:Manufacturer')
!        If maf:Field_Name = f:FieldName
!            Access:MANFAULO.ClearKey(mfo:HideFieldKey)
!            mfo:NotAvailable = False
!            mfo:Manufacturer = maf:Manufacturer
!            mfo:Field_Number = maf:Field_Number
!            mfo:Field = p_web.GetSessionValue('tmp:FaultCode' & f:FieldNumber)
!            If Access:MANFAULO.TryFetch(mfo:HideFieldKey) = Level:Benign
!                !Found
!                Access:MANFAUEX.ClearKey(max:ModelNumberKey)
!                max:MANFAULORecordNumber = mfo:RecordNumber
!                max:ModelNumber = p_web.GetSessionValue('job:Model_Number')
!                If Access:MANFAUEX.TryFetch(max:ModelNumberKey) = Level:Benign
!                    !Found
!                    p_web.SetSessionValue('Comment:FieldNumber' & f:FieldNumber,'<span class="RedBold">Model Number Excluded For Fault Code</span>')
!                    p_web.SetSessionValue('tmp:FaultCode' & f:FieldNumber,'')
!                Else ! If Access:MANFAUEX.TryFetch(max:ModelNumberKey) = Level:Benign
!                    !Error
!                    p_web.SetSessionValue('Comment:FieldNumber' & f:FieldNumber,'')
!                    If maf:ReplicateFault = 'YES'
!                        If p_web.GetSessionValue('jbn:Fault_Description') = ''
!                            p_web.SetSessionValue('jbn:Fault_Description',Clip(mfo:Description))
!                        Else ! If p_web.GetSessionValue('jbn:Fault_Description') = ''
!                            If ~Instring(Clip(mfo:Description),p_web.GetSessionValue('jbn:Fault_Description'),1,1)
!                                p_web.SetSessionValue('jbn:Fault_Description',Clip(p_web.GetSessionValue('jbn:Fault_Description')) & '<13,10>' & Clip(mfo:Description))
!                            End ! If ~Instring(Clip(mfo:Description),p_web.GetSessionValue('jbn:Fault_Description'),1,1)
!                        End ! If p_web.GetSessionValue('jbn:Fault_Description') = ''
!                    End ! If maf:ReplicateFault = 'YES'
!                End ! If Access:MANFAUEX.TryFetch(max:ModelNumberKey) = Level:Benign
!
!            Else ! If Access:MANFAULO.TryFetch(mfo:HideFieldKey) = Level:Benign
!                !Error
!            End ! If Access:MANFAULO.TryFetch(mfo:HideFieldKey) = Level:Benign
!            Break
!        End ! If maf:Field_Name = f:FieldNumber
!        
!    End ! Loop
!
local.FaultCodeDescription     Procedure(Byte f:FieldNumber)
Code
    Access:MANFAUEX.ClearKey(max:ModelNumberKey)
    max:MANFAULORecordNumber = p_web.GetSessionValue('mfo:RecordNumber')
    max:ModelNumber = p_web.GetSessionValue('job:Model_Number')
    If Access:MANFAUEX.TryFetch(max:ModelNumberKey) = Level:Benign
        !Found
        p_web.SetSessionValue('Comment:FieldNumber' & f:FieldNumber,'Model Number Excluded For Fault Code')
        p_web.SetSessionValue('tmp:FaultCode' & f:FieldNumber,'')
    Else ! If Access:MANFAUEX.TryFetch(max:ModelNumberKey) = Level:Benign
        !Error
        p_web.SetSessionValue('Comment:FieldNumber' & f:FieldNumber,'')
        Access:MANFAULT.Clearkey(maf:Field_Number_Key)
        maf:Manufacturer = p_web.GetSessionValue('job:Manufacturer')
        maf:Field_Number = p_web.GetSessionValue('mfo:Field_Number')
        If Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign
            If maf:ReplicateFault = 'YES'
                If p_web.GetSessionValue('jbn:Fault_Description') = ''
                    p_web.SetSessionValue('jbn:Fault_Description',p_web.GetSessionValue('mfo:Description'))
                Else ! If p_web.GetSessionValue('jbn:Fault_Description') = ''
                    If ~Instring(Clip(mfo:Description),p_web.GetSessionValue('jbn:Fault_Description'),1,1)
                        p_web.SetSessionValue('jbn:Fault_Description',Clip(p_web.GetSessionValue('jbn:Fault_Description')) & '<13,10>' & p_web.GetSessionValue('mfo:Description'))
                    End ! If ~Instring(Clip(mfo:Description),p_web.GetSessionValue('jbn:Fault_Description'),1,1)
                End ! If p_web.GetSessionValue('jbn:Fault_Description') = ''
            End ! If maf:ReplicateFault = 'YES'
        End ! If Access:MANFAULT.Clearkey(maf:Field_Number_Key) = Level:Benign
    End ! If Access:MANFAUEX.TryFetch(max:ModelNumberKey) = Level:Benign
local.SetStatus     Procedure(String f:type,Long f:Number)
Code
    Access:DEFAULTS.Clearkey(def:RecordNumberKey)
    def:Record_Number = 0
    Set(def:RecordNumberKey,def:RecordNumberKey)
    Loop
        If Access:DEFAULTS.Next()
            Break
        End ! If Access:DEFAULTS.Next()
        Break
    End ! Loop

    Case f:Type
    Of 'JOB'
        If p_web.GSV('job:Cancelled') <> 'YES'
            Access:STATUS.Clearkey(sts:Ref_Number_Only_Key)
            sts:Ref_Number = f:Number
            If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key)
                p_web.SSV('job:Current_Status','ERROR (' & Clip(f:Number) & ')')
            Else ! If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key)
                If sts:Status <> p_web.GSV('job:Current_Status')
                    p_web.SSV('job:PreviousStatus',p_web.GSV('job:Current_Status'))
                    p_web.SSV('job:StatusUser',p_web.GSV('BookingUserCode'))
                    p_web.SSV('job:Current_Status',sts:Status)
                    p_web.SSV('wob:Current_Status_Date',Today())
                    p_web.SSV('wob:Current_Status',sts:Status)
                    If Access:AUDSTATS.PrimeRecord() = Level:Benign
                        aus:RefNumber = p_web.GSV('job:Ref_Number')
                        aus:Type = 'JOB'
                        aus:DateChanged = Today()
                        aus:TimeChanged = Clock()
                        aus:OldStatus = p_web.GSV('job:PreviousStatus')
                        aus:NewStatus = p_web.GSV('job:Current_Status')
                        aus:UserCode = p_web.GSV('BookingUserCode')
                        If Access:AUDSTATS.TryInsert() = Level:Benign

                        Else ! If Access:AUDSTATS.TryInsert() = Level:Benign
                            Access:AUDSTATS.CancelAutoInc()
                        End ! If Access:AUDSTATS.TryInsert() = Level:Benign
                    End ! If Access:AUDSTATS.PrimeRecord() = Level:Benign

                    Access:STAHEAD.Clearkey(sth:Ref_Number_Key)
                    sth:Ref_Number = sts:Heading_Ref_Number
                    If Access:STAHEAD.TryFetch(sth:Ref_Number_Key) = Level:Benign
                        Access:JOBSTAGE.Clearkey(jst:Job_Stage_Key)
                        jst:Ref_Number = p_web.GSV('job:Ref_Number')
                        jst:Job_Stage = sth:Heading
                        If Access:JOBSTAGE.TryFetch(jst:Job_Stage_Key)
                            If Access:JOBSTAGE.PrimeRecord() = Level:Benign
                                jst:Ref_Number = p_web.GSV('job:Ref_Number')
                                jst:Job_Stage = sth:Heading
                                jst:Time = CLock()
                                jst:Date = Today()
                                jst:User = p_web.GSV('BookingUserCode')
                                If Access:JOBSTAGE.TryInsert() = Level:Benign

                                Else ! If Access:JOBSTAGE.TryInsert() = Level:Benign
                                    Access:JOBSTAGE.CancelAutoInc()
                                End ! If Access:JOBSTAGE.TryInsert() = Level:Benign
                            End ! If Access:JOBSTAGE.PrimeRecord() = Level:Benign
                        End ! If Access:JOBSTAGE.TryFetch()
                    End ! If Access:STAHEAD.TryFetch(sth:Ref_Number_Key) = Level:Benign

                    If sts:use_turnaround_time  = 'YES'
                        EndDays# = Today()
                        x# = 0
                        count# = 0
                        If sts:Turnaround_Days <> 0
                            Loop
                                count# += 1
                                day_number# = (Today() + count#) % 7
                                If def:include_saturday <> 'YES'
                                    If day_number# = 6
                                        EndDays# += 1
                                        Cycle
                                    End
                                End
                                If def:include_sunday <> 'YES'
                                    If day_number# = 0
                                        EndDays# += 1
                                        Cycle
                                    End
                                End
                                EndDays# += 1
                                x# += 1
                                If x# >= sts:Turnaround_Days
                                    Break
                                End!If x# >= sts:turnaround_days
                            End!Loop
                        End!If f_days <> ''

                        EndHours# = Clock()
                        If sts:Turnaround_Hours <> 0
                            start_time_temp# = Clock()
                            new_day# = 0
                            Loop x# = 1 To sts:Turnaround_Hours
                                EndHours# += 360000
                                If def:Start_Work_Hours <> '' And def:End_Work_Hours <> ''
                                    If EndHours# > def:End_Work_Hours or EndHours# < def:Start_Work_Hours
                                        EndHours# = def:Start_Work_Hours
                                        EndDays# += 1
                                    End !If tmp:End_Hours > def:End_Work_Hours
                                End !If def:Start_Work_Hours <> '' And def:End_Work_Hours <> ''

                            End!Loop x# = 1 To Abs(sts:turnaround_hours/6000)
                        End!If f_hours <> ''

                        p_web.SSV('job:Status_End_Time',EndHours#)
                        p_web.SSV('job:status_end_date',EndDays#)
                    Else!If sts:use_turnaround_time = 'YES'
                        p_web.SSV('job:status_end_date',Deformat('1/1/2050',@d6))
                        p_web.SSV('job:status_end_Time',Clock())
                    End!If sts:use_turnaround_time  = 'YES'
                Else ! If sts:Status <> job:Current_Status
                End ! If sts:Status <> job:Current_Status
            End ! If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key)
        End ! If job:Cancelled <> 'YES'
    Of 'EXC'
        Access:STATUS.Clearkey(sts:Ref_Number_Only_Key)
        sts:Ref_Number = f:Number
        If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key)
            p_web.SSV('job:Exchange_Status','ERROR (' & Clip(f:Number) & ')')
        Else ! If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key)
            If sts:Status <> p_web.GSV('job:Exchange_Status')
                PreviousStatus" = p_web.GSV('job:Exchange_Status')
                p_web.SSV('job:Exchange_Status',sts:Status)
                p_web.SSV('wob:Exchange_Status_Date',Today())
                p_web.SSV('wob:Exchange_Status',sts:Status)
                If Access:AUDSTATS.PrimeRecord() = Level:Benign
                    aus:RefNumber = p_web.GSV('job:Ref_Number')
                    aus:Type = 'EXC'
                    aus:DateChanged = Today()
                    aus:TimeChanged = Clock()
                    aus:OldStatus = PreviousStatus"
                    aus:NewStatus = p_web.GSV('job:Exchange_Status')
                    aus:UserCode = p_web.GSV('BookingUserCode')
                    If Access:AUDSTATS.TryInsert() = Level:Benign

                    Else ! If Access:AUDSTATS.TryInsert() = Level:Benign
                        Access:AUDSTATS.CancelAutoInc()
                    End ! If Access:AUDSTATS.TryInsert() = Level:Benign
                End ! If Access:AUDSTATS.PrimeRecord() = Level:Benign

            Else ! If sts:Status <> job:Current_Status
            End ! If sts:Status <> job:Current_Status
        End ! If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key)

    Of 'LOA'
        Access:STATUS.Clearkey(sts:Ref_Number_Only_Key)
        sts:Ref_Number = f:Number
        If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key)
            p_web.SSV('job:Loan_Status','ERROR (' & Clip(f:Number) & ')')
        Else ! If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key)
            If sts:Status <> p_web.GSV('job:Loan_Status')
                PreviousStatus" = p_web.GSV('job:Loan_Status')
                p_web.SSV('job:Loan_Status',sts:Status)
                p_web.SSV('wob:Loan_Status_Date',Today())
                p_web.SSV('wob:Loan_Status',sts:Status)
                If Access:AUDSTATS.PrimeRecord() = Level:Benign
                    aus:RefNumber = p_web.GSV('job:Ref_Number')
                    aus:Type = 'LOA'
                    aus:DateChanged = Today()
                    aus:TimeChanged = Clock()
                    aus:OldStatus = PreviousStatus"
                    aus:NewStatus = p_web.GSV('job:Loan_Status')
                    aus:UserCode = p_web.GSV('BookingUserCode')
                    If Access:AUDSTATS.TryInsert() = Level:Benign

                    Else ! If Access:AUDSTATS.TryInsert() = Level:Benign
                        Access:AUDSTATS.CancelAutoInc()
                    End ! If Access:AUDSTATS.TryInsert() = Level:Benign
                End ! If Access:AUDSTATS.PrimeRecord() = Level:Benign

            Else ! If sts:Status <> job:Current_Status
            End ! If sts:Status <> job:Current_Status
        End ! If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key)

    End ! Case f:Type
local.ValidateMSN     Procedure()
Code
    If p_web.GetSessionValue('job:MSN') = ''
        Return 0
    End ! If p_web.GetSessionValue('job:MSN') = ''
    If p_web.GetSessionValue('Hide:MSN') = 1
        Return 0
    End ! If p_web.GetSessionValue('job:MSN') = '' Or p_web.GetSessionValue('Hide:MSN') = 1


    Access:MANUFACT.ClearKey(man:Manufacturer_Key)
    man:Manufacturer = p_web.GetSessionValue('job:Manufacturer')
    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Found
        If man:ApplyMSNFormat
            If CheckFaultFormat(p_web.GetSessionValue('job:MSN'),man:MSNFormat)
                p_web.SetSessionValue('job:MSN','')
                p_web.SetSessionValue('Comment:MSN','Invalid Format.')
                Return 1
            End ! If CheckFaultFormat(job:MSN,man:MSNFormat)
        Else ! If man:ApplyMSNFormat
            If CheckLength('MSN',p_web.GetSessionValue('job:Model_Number'),p_web.GetSessionValue('job:MSN'))
                p_web.SetSessionValue('job:MSN','')
                p_web.SetSessionValue('Comment:MSN','Invalid Length.')
                Return 1
            End ! If CheckLength('MSN',p_web.GetSessionValue('tmp:Manufacturer'),p_web.GetSessionValue('job:MSN'))

        End ! If man:ApplyMSNFormat
    Else ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Error
    End ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign

    Return 0
