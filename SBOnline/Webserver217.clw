

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER217.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! Muchos Validation
!!! </summary>
CountBouncer         PROCEDURE  (f_RefNumber,f_DateBooked,f_IMEI,func:Cjob,func:CCharge,func:CRepair,func:WJob,func:WCharge,func:WRepair) ! Declare Procedure
tmp:DateBooked       DATE                                  !
tmp:count            LONG                                  !
save_job2_id         USHORT,AUTO                           !
save_joo_id          USHORT,AUTO                           !
save_mfo_id          USHORT,AUTO                           !
save_wpr_id          USHORT,AUTO                           !
save_par_id          USHORT,AUTO                           !
save_wob_id          USHORT,AUTO                           !
tmp:IMEI             STRING(30)                            !
tmp:Manufacturer     STRING(30)                            !
tmp:IgnoreChargeable BYTE(0)                               !Ignore Chargeable
tmp:IgnoreWarranty   BYTE(0)                               !Ignore Warranty
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
local       Class
OutFaultExcluded    Procedure(Long local:JobNumber,String local:WarrantyJob,String local:ChargeableJob),Byte
            End

  CODE
    RETURN vod.CountJobBouncers()

!!Pass the Job Number. Use this to get the job's IMEI Number, Date Booked and Manufacturer
!!From that count how many times the IMEI number has been booked in before
!tmp:IgnoreChargeable = GETINI('BOUNCER','IgnoreChargeable',,CLIP(PATH())&'\SB2KDEF.INI')
!tmp:IgnoreWarranty  = GETINI('BOUNCER','IgnoreWarranty',,CLIP(PATH())&'\SB2KDEF.INI')
!
!CheckBouncer# = 1
!If func:CJob = 'YES'
!    If tmp:IgnoreChargeable
!        CheckBouncer# = 0
!    Else !If GETINI('BOUNCER','IgnoreChargeable',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!        Access:CHARTYPE.ClearKey(cha:Charge_Type_Key)
!        cha:Charge_Type = func:CCharge
!        If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!            !Found
!            If cha:ExcludeFromBouncer
!                CheckBouncer# = 0
!            Else !If cha:ExcludeFromBouncer
!                Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
!                rtd:Manufacturer = job:Manufacturer
!                rtd:Chargeable   = 'YES'
!                rtd:Repair_Type  = func:CRepair
!                If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
!                    !Found
!                    If rtd:ExcludeFromBouncer
!                        CheckBouncer# = 0
!                    End !If rtd:ExcludeFromBouncer
!                Else!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!                    !Error
!                    !Assert(0,'<13,10>Fetch Error<13,10>')
!                End!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!            End !If cha:ExcludeFromBouncer
!        Else!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!            !Error
!            !Assert(0,'<13,10>Fetch Error<13,10>')
!        End!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!    End !If GETINI('BOUNCER','IgnoreChargeable',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!End !If func:CJob = 'YES'
!
!!Is the job's outfault excluded?
!If CheckBouncer#
!    If Local.OutFaultExcluded(job:Ref_Number,job:Warranty_job,job:Chargeable_Job)
!        CheckBouncer# = 0
!    End !If Local.OutFaultExcluded()
!End !If CheckBouncer#
!
!!Is the job's infault excluded?
!If CheckBouncer#
!    Access:MANFAULT.ClearKey(maf:InFaultKey)
!    maf:Manufacturer = job:Manufacturer
!    maf:InFault      = 1
!    If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
!        Access:MANFAULO.ClearKey(mfo:Field_Key)
!        mfo:Manufacturer = job:Manufacturer
!        mfo:Field_Number = maf:Field_Number
!        Case maf:Field_Number
!        Of 1
!            mfo:Field        = job:Fault_Code1
!        Of 2
!            mfo:Field        = job:Fault_Code2
!        Of 3
!            mfo:Field        = job:Fault_Code3
!        Of 4
!            mfo:Field        = job:Fault_Code4
!        Of 5
!            mfo:Field        = job:Fault_Code5
!        Of 6
!            mfo:Field        = job:Fault_Code6
!        Of 7
!            mfo:Field        = job:Fault_Code7
!        Of 8
!            mfo:Field        = job:Fault_Code8
!        Of 9
!            mfo:Field        = job:Fault_Code9
!        Of 10
!            mfo:Field        = job:Fault_Code10
!        Of 11
!            mfo:Field        = job:Fault_Code11
!        Of 12
!            mfo:Field        = job:Fault_Code12
!        End !Case maf:Field_Number
!        If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
!            !Found
!            If mfo:ExcludeFromBouncer
!                CheckBouncer# = 0
!            End !If mfo:ExcludeBouncer
!        Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
!            !Error
!            !Assert(0,'<13,10>Fetch Error<13,10>')
!        End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
!
!    Else!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
!        !Error
!        !Assert(0,'<13,10>Fetch Error<13,10>')
!    End!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
!End !If CheckBouncer#
!
!!Lets write the bouncers into a memory queue
!Free(glo:Queue20)
!Clear(glo:Queue20)
!
!
!If CheckBouncer#
!    If func:WJob = 'YES'
!        If tmp:IgnoreWarranty
!            CheckBouncer# = 0
!        Else !If GETINI('BOUNCER','IgnoreChargeable',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!
!            Access:CHARTYPE.ClearKey(cha:Charge_Type_Key)
!            cha:Charge_Type = func:WCharge
!            If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!                !Found
!                If cha:ExcludeFromBouncer
!                    CheckBouncer# = 0
!                Else !If cha:ExcludeFromBouncer
!                    Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
!                    rtd:Manufacturer = job:Manufacturer
!                    rtd:Warranty     = 'YES'
!                    rtd:Repair_Type  = func:WRepair
!                    If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
!                        !Found
!                        If rtd:ExcludeFromBouncer
!                            CheckBouncer# = 0
!                        End !If rtd:ExcludeFromBouncer
!                    Else!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!                        !Error
!                        !Assert(0,'<13,10>Fetch Error<13,10>')
!                    End!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!                End !If cha:ExcludeFromBouncer
!            Else!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!                !Error
!                !Assert(0,'<13,10>Fetch Error<13,10>')
!            End!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!        End !If GETINI('BOUNCER','IgnoreChargeable',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!    End !If func:CJob = 'YES'
!End !If CheckBouncer#
!
!If CheckBouncer#
!    tmp:count    = 0
!    If f_IMEI <> 'N/A' And f_IMEI <> ''
!        Set(defaults)
!        Access:Defaults.Next()
!
!        setcursor(cursor:wait)
!        save_job2_id = access:jobs2_alias.savefile()
!        access:jobs2_alias.clearkey(job2:esn_key)
!        job2:esn = f_imei
!        set(job2:esn_key,job2:esn_key)
!        loop
!            if access:jobs2_alias.next()
!                break
!            end !if
!            if job2:esn <> f_imei      |
!                then break.  ! end if
!            yldcnt# += 1
!            if yldcnt# > 25
!                yield() ; yldcnt# = 0
!            end !if
!
!            If job2:Cancelled = 'YES'
!                Cycle
!            End !If job2:Cancelled = 'YES'
!
!            If job2:Exchange_Unit_Number <> ''
!                Cycle
!            End !If job2:Exchange_Unit_Number <> ''
!
!            If job2:Chargeable_Job = 'YES'
!                If tmp:IgnoreChargeable
!                    Cycle
!                End !If GETINI('BOUNCER','IgnoreChargeable',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!
!                Access:CHARTYPE.ClearKey(cha:Charge_Type_Key)
!                cha:Charge_Type = job2:Charge_Type
!                If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!                    !Found
!                    If cha:ExcludeFromBouncer
!                        Cycle
!                    Else !If cha:ExcludeFromBouncer
!                        If job2:Repair_Type <> ''
!                            Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
!                            rtd:Manufacturer = job:Manufacturer
!                            rtd:Chargeable   = 'YES'
!                            rtd:Repair_Type  = job2:Repair_Type
!                            If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
!                                !Found
!                                If rtd:ExcludeFromBouncer
!                                    Cycle
!                                End !If rtd:ExcludeFromBouncer
!                            Else!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!                                !Error
!                                !Assert(0,'<13,10>Fetch Error<13,10>')
!                            End!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!
!                        End !If job2:Repair_Type <> ''
!                    End !If cha:ExcludeFromBouncer
!                Else!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!                    !Error
!                    !Assert(0,'<13,10>Fetch Error<13,10>')
!                End!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!            End !If func:CJob = 'YES'
!
!
!            If job2:Warranty_Job = 'YES'
!                If tmp:IgnoreWarranty
!                    Cycle
!                End !If tmp:IgnoreWarranty
!
!                Access:CHARTYPE.ClearKey(cha:Charge_Type_Key)
!                cha:Charge_Type = job2:Warranty_Charge_Type
!                If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!                    !Found
!                    If cha:ExcludeFromBouncer
!                        Cycle
!                    Else !If cha:ExcludeFromBouncer
!                        If job2:Repair_Type_Warranty <> ''
!                            Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
!                            rtd:Manufacturer = job:Manufacturer
!                            rtd:Warranty     = 'YES'
!                            rtd:Repair_Type  = job2:Repair_Type_Warranty
!                            If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
!                                !Found
!                                If rtd:ExcludeFromBouncer
!                                    Cycle
!                                End !If rtd:ExcludeFromBouncer
!                            Else!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!                                !Error
!                                !Assert(0,'<13,10>Fetch Error<13,10>')
!                            End!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!                        End !If job2:Repair_Type_Warranty <> ''
!                    End !If cha:ExcludeFromBouncer
!                Else!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!                    !Error
!                    !Assert(0,'<13,10>Fetch Error<13,10>')
!                End!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!            End !If func:CJob = 'YES'
!
!            !Has job got the same In Fault?
!
!            Access:MANFAULT.ClearKey(maf:InFaultKey)
!            maf:Manufacturer = job:Manufacturer
!            maf:InFault      = 1
!            If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
!                !Found
!                Case maf:Field_Number
!                Of 1
!                    If job2:Fault_Code1 <> job:Fault_Code1
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                Of 2
!                    If job2:Fault_Code2 <> job:Fault_Code2
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                Of 3
!                    If job2:Fault_Code3 <> job:Fault_Code3
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                Of 4
!                    If job2:Fault_Code4 <> job:Fault_Code4
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                Of 5
!                    If job2:Fault_Code5 <> job:Fault_Code5
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                Of 6
!                    If job2:Fault_Code6 <> job:Fault_Code6
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                Of 7
!                    If job2:Fault_Code7 <> job:Fault_Code7
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                Of 8
!                    If job2:Fault_Code8 <> job:Fault_Code8
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                Of 9
!                    If job2:Fault_Code9 <> job:Fault_Code9
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                Of 10
!                    If job2:Fault_Code10 <> job:Fault_Code10
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                Of 11
!                    If job2:Fault_Code11 <> job:Fault_Code11
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                Of 12
!                    If job2:Fault_Code12 <> job:Fault_Code12
!                        Cycle
!                    End !If job2:Fault_Code1 <> job:Fault_Code1
!                End !Case maf:Field_Number
!                !Is the infault excluded?
!                Access:MANFAULO.ClearKey(mfo:Field_Key)
!                mfo:Manufacturer = job:Manufacturer
!                mfo:Field_Number = maf:Field_Number
!                Case maf:Field_Number
!                Of 1
!                    mfo:Field        = job2:Fault_Code1
!                Of 2
!                    mfo:Field        = job2:Fault_Code2
!                Of 3
!                    mfo:Field        = job2:Fault_Code3
!                Of 4
!                    mfo:Field        = job2:Fault_Code4
!                Of 5
!                    mfo:Field        = job2:Fault_Code5
!                Of 6
!                    mfo:Field        = job2:Fault_Code6
!                Of 7
!                    mfo:Field        = job2:Fault_Code7
!                Of 8
!                    mfo:Field        = job2:Fault_Code8
!                Of 9
!                    mfo:Field        = job2:Fault_Code9
!                Of 10
!                    mfo:Field        = job2:Fault_Code10
!                Of 11
!                    mfo:Field        = job2:Fault_Code11
!                Of 12
!                    mfo:Field        = job2:Fault_Code12
!                End !Case maf:Field_Number
!                If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
!                    !Found
!                    If mfo:ExcludeFromBouncer
!                        Cycle
!                    End !If mfo:ExcludeBouncer
!                Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
!                    !Error
!                    !Assert(0,'<13,10>Fetch Error<13,10>')
!                End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
!
!            Else!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
!                !Error
!                !Assert(0,'<13,10>Fetch Error<13,10>')
!            End!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
!
!            !Are the outfaults excluded?
!            If Local.OutFaultExcluded(job2:Ref_Number,job2:Warranty_job,job2:Chargeable_Job)
!                Cycle
!            End !If Local.OutFaultExcluded()
!
!            If job2:ref_number <> f_refnumber
!                Case GETINI('BOUNCER','BouncerType',,CLIP(PATH())&'\SB2KDEF.INI')
!                Of 2 !Despatched Date
!                    If job2:Date_Despatched <> ''
!                        If job2:date_despatched + def:bouncertime > f_datebooked And job2:date_despatched <= f_datebooked
!                            tmp:count += 1
!                            GLO:Pointer20 = job2:Ref_Number
!                            ADd(glo:Queue20)
!                        End!If job2:date_booked + man:warranty_period < Today()
!
!                    Else !If job2:Date_Despatched <> ''
!
!                        !Need to get the webjob record for the bouncer job
!                        !Will save the file, and then restore it afterwards.
!                        !Therefore, no records should be lost.
!
!                        Save_wob_ID = Access:WEBJOB.SaveFile()
!
!                        Access:WEBJOB.Clearkey(wob:RefNumberKey)
!                        wob:RefNumber   = job2:Ref_Number
!                        If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!                            !Found
!
!                        Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!                            !Error
!                        End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!
!
!                        If wob:DateJobDespatched <> ''
!                            If wob:DateJobDespatched + def:bouncertime > f_datebooked And wob:DateJobDespatched <= f_datebooked
!
!                                tmp:count += 1
!                                GLO:Pointer20 = job2:Ref_Number
!                                ADd(glo:Queue20)
!                            End!If job2:date_booked + man:warranty_period < Today()
!
!                        End !If wob:JobDateDespatched <> ''
!
!                        Access:WEBJOB.RestoreFile(Save_wob_ID)
!
!                    End !If job2:Date_Despatched <> ''
!                Of 1 !Completed Date
!                    If job2:date_completed + def:bouncertime > f_datebooked And job2:date_completed <= f_datebooked
!                        tmp:count += 1
!                        GLO:Pointer20 = job2:Ref_Number
!                        ADd(glo:Queue20)
!
!                    End!If job2:date_booked + man:warranty_period < Today()
!                Else !Booking Date
!                    If job2:date_booked + def:bouncertime > f_datebooked And job2:date_booked <= f_datebooked
!                        tmp:count += 1
!                        GLO:Pointer20 = job2:Ref_Number
!                        ADd(glo:Queue20)
!                    End!If job2:date_booked + man:warranty_period < Today()
!
!                End !Case GETINI('BOUNCER','BouncerType',,CLIP(PATH())&'\SB2KDEF.INI')
!                If GETINI('BOUNCER','BouncerType',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!                Else !If GETINI('BOUNCER','BouncerType',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!                End !If GETINI('BOUNCER','BouncerType',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!            End!If job2:esn <> job2:ref_number
!        end !loop
!        access:jobs2_alias.restorefile(save_job2_id)
!        setcursor()
!    End!If access:jobs2_alias.clearkey(job2:RefNumberKey) = Level:Benign
!End !If CheckBouncer#
!Return tmp:count
Local.OutFaultExcluded       Procedure(Long local:JobNumber,String local:WarrantyJob,String local:ChargeableJob)
local:FaultCode     String(30)
Code
    !Loop through outfaults
    Save_joo_ID = Access:JOBOUTFL.SaveFile()
    Access:JOBOUTFL.ClearKey(joo:JobNumberKey)
    joo:JobNumber = local:JobNumber
    Set(joo:JobNumberKey,joo:JobNumberKey)
    Loop
        If Access:JOBOUTFL.NEXT()
           Break
        End !If
        If joo:JobNumber <> local:JobNumber      |
            Then Break.  ! End If
        !Which is the main out fault?
        Access:MANFAULT.ClearKey(maf:MainFaultKey)
        maf:Manufacturer = job:Manufacturer
        maf:MainFault    = 1
        If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
            !Lookup the Fault Code lookup to see if it's excluded
            Save_mfo_ID = Access:MANFAULO.SaveFile()
            Access:MANFAULO.ClearKey(mfo:Field_Key)
            mfo:Manufacturer = job:Manufacturer
            mfo:Field_Number = maf:Field_Number
            mfo:Field        = joo:FaultCode
            Set(mfo:Field_Key,mfo:Field_Key)
            Loop
                If Access:MANFAULO.NEXT()
                   Break
                End !If
                If mfo:Manufacturer <> job:Manufacturer      |
                Or mfo:Field_Number <> maf:Field_Number      |
                Or mfo:Field        <> joo:FaultCode      |
                    Then Break.  ! End If
                If Clip(mfo:Description) = Clip(joo:Description)
                    !Make sure the descriptions match in case of duplicates
                    If mfo:ExcludeFromBouncer
                        Return Level:Fatal
                    End !If mfo:ExcludeFromBoucer
                    Break
                End !If Clip(mfo:Description) = Clip(joo:Description)
            End !Loop
            Access:MANFAULO.RestoreFile(Save_mfo_ID)

        Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
            !Error
        End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign

    End !Loop
    Access:JOBOUTFL.RestoreFile(Save_joo_ID)

    !Is an outfault records on parts for this manufacturer
    Access:MANFAUPA.ClearKey(map:MainFaultKey)
    map:Manufacturer = job:Manufacturer
    map:MainFault    = 1
    If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
        !Found
        !Loop through the parts as see if any of the faults codes are excluded
        If local:WarrantyJob = 'YES'

            Save_wpr_ID = Access:WARPARTS.SaveFile()
            Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
            wpr:Ref_Number  = local:JobNumber
            Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
            Loop
                If Access:WARPARTS.NEXT()
                   Break
                End !If
                If wpr:Ref_Number  <> local:JobNumber      |
                    Then Break.  ! End If
                !Which is the main out fault?
                Access:MANFAULT.ClearKey(maf:MainFaultKey)
                maf:Manufacturer = job:Manufacturer
                maf:MainFault    = 1
                If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                    !Lookup the Fault Code lookup to see if it's excluded

                    !Work out which part fault code is the outfault
                    !and use that for the lookup
                    Case map:Field_Number
                        Of 1
                            local:FaultCode        = wpr:Fault_Code1
                        Of 2
                            local:FaultCode        = wpr:Fault_Code2
                        Of 3
                            local:FaultCode        = wpr:Fault_Code3
                        Of 4
                            local:FaultCode        = wpr:Fault_Code4
                        Of 5
                            local:FaultCode        = wpr:Fault_Code5
                        Of 6
                            local:FaultCode        = wpr:Fault_Code6
                        Of 7
                            local:FaultCode        = wpr:Fault_Code7
                        Of 8
                            local:FaultCode        = wpr:Fault_Code8
                        Of 9
                            local:FaultCode        = wpr:Fault_Code9
                        Of 10
                            local:FaultCode        = wpr:Fault_Code10
                        Of 11
                            local:FaultCode        = wpr:Fault_Code11
                        Of 12
                            local:FaultCode        = wpr:Fault_Code12
                    End !Case map:Field_Number

                    Save_mfo_ID = Access:MANFAULO.SaveFile()
                    Access:MANFAULO.ClearKey(mfo:Field_Key)
                    mfo:Manufacturer = job:Manufacturer
                    mfo:Field_Number = maf:Field_Number
                    mfo:Field        = local:FaultCode
                    Set(mfo:Field_Key,mfo:Field_Key)
                    Loop
                        If Access:MANFAULO.NEXT()
                           Break
                        End !If
                        If mfo:Manufacturer <> job:Manufacturer      |
                        Or mfo:Field_Number <> maf:Field_Number      |
                        Or mfo:Field        <> local:FaultCode      |
                            Then Break.  ! End If
                        !This fault relates to a specific part fault code number??
                        If mfo:RelatedPartCode <> 0 And map:UseRelatedJobCode
                            If mfo:RelatedPartCode <> maf:Field_Number
                                Cycle
                            End !If mfo:RelatedPartCode <> maf:Field_Number
                        End !If mfo:RelatedPartCode <> 0
                        IF mfo:ExcludeFromBouncer
                            Return Level:Fatal
                        End !IF mfo:ExcludeFromBouncer
                    End !Loop
                    Access:MANFAULO.RestoreFile(Save_mfo_ID)

                Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                    !Error
                End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign

            End !Loop
            Access:WARPARTS.RestoreFile(Save_wpr_ID)
        End !If job:Warranty_Job = 'YES'

        If local:ChargeableJob = 'YES'
            !Loop through the parts as see if any of the faults codes are excluded
            Save_par_ID = Access:PARTS.SaveFile()
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number  = local:JobNumber
            Set(par:Part_Number_Key,par:Part_Number_Key)
            Loop
                If Access:PARTS.NEXT()
                   Break
                End !If
                If par:Ref_Number  <> local:JobNumber      |
                    Then Break.  ! End If
                !Which is the main out fault?
                Access:MANFAULT.ClearKey(maf:MainFaultKey)
                maf:Manufacturer = job:Manufacturer
                maf:MainFault    = 1
                If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                    !Lookup the Fault Code lookup to see if it's excluded

                    !Work out which part fault code is the outfault
                    !and use that for the lookup
                    Case map:Field_Number
                        Of 1
                            local:FaultCode        = par:Fault_Code1
                        Of 2
                            local:FaultCode        = par:Fault_Code2
                        Of 3
                            local:FaultCode        = par:Fault_Code3
                        Of 4
                            local:FaultCode        = par:Fault_Code4
                        Of 5
                            local:FaultCode        = par:Fault_Code5
                        Of 6
                            local:FaultCode        = par:Fault_Code6
                        Of 7
                            local:FaultCode        = par:Fault_Code7
                        Of 8
                            local:FaultCode        = par:Fault_Code8
                        Of 9
                            local:FaultCode        = par:Fault_Code9
                        Of 10
                            local:FaultCode        = par:Fault_Code10
                        Of 11
                            local:FaultCode        = par:Fault_Code11
                        Of 12
                            local:FaultCode        = par:Fault_Code12
                    End !Case map:Field_Number

                    Save_mfo_ID = Access:MANFAULO.SaveFile()
                    Access:MANFAULO.ClearKey(mfo:Field_Key)
                    mfo:Manufacturer = job:Manufacturer
                    mfo:Field_Number = maf:Field_Number
                    mfo:Field        = local:FaultCode
                    Set(mfo:Field_Key,mfo:Field_Key)
                    Loop
                        If Access:MANFAULO.NEXT()
                           Break
                        End !If
                        If mfo:Manufacturer <> job:Manufacturer      |
                        Or mfo:Field_Number <> maf:Field_Number      |
                        Or mfo:Field        <> local:FaultCode      |
                            Then Break.  ! End If
                        !This fault relates to a specific part fault code number??
                        If mfo:RelatedPartCode <> 0 And map:UseRelatedJobCode
                            If mfo:RelatedPartCode <> maf:Field_Number
                                Cycle
                            End !If mfo:RelatedPartCode <> maf:Field_Number
                        End !If mfo:RelatedPartCode <> 0
                        IF mfo:ExcludeFromBouncer
                            Return Level:Fatal
                        End !IF mfo:ExcludeFromBouncer
                    End !Loop
                    Access:MANFAULO.RestoreFile(Save_mfo_ID)

                Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                    !Error
                End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign

            End !Loop
            Access:PARTS.RestoreFile(Save_par_ID)
        End !If job:Chargeable_Job = 'YES'
    Else!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
        !Error
    End!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign

    Return Level:Benign
