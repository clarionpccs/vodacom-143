

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER228.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
DateCodeValidation   PROCEDURE  (STRING pManufacturer,STRING pDateCode,DATE pDateBooked) ! Declare Procedure
doWeekCheck                 LONG()
yearCode                    STRING(30)
monthCode                   STRING(30)
yearValue                   STRING(30)
monthValue                  STRING(30)
startOfYear                 LONG()
dayOfYear                   LONG()
i                           LONG()
retValue                    LONG(Level:Benign)
tmp:YearCode         STRING(30)                            !Year COde
tmp:MonthCode        STRING(30)                            !Month Code
tmp:Year             STRING(30)                            !Year
tmp:Month            STRING(30)                            !Month
FilesOpened     BYTE(0)

  CODE
        DO OpenFiles
        LOOP 1 TIMES
            Access:MANUFACT.ClearKey(man:Manufacturer_Key)
            man:Manufacturer = pManufacturer
            IF (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
                
            ELSE ! IF
            END ! IF
            
            doWeekCheck = 0
            
            CASE pManufacturer
            OF 'ALCATEL' 
                yearCode = SUB(pDateCode,3,1)
                monthCode = SUB(pDateCode,2,1)
            OF 'ERICSSON'
                yearCode = SUB(pDateCode,1,2)
                monthCode = SUB(pDateCode,3,2)
                doWeekCheck = 1
            Of 'PHILIPS'
                yearCode    = SUB(pDateCode,5,2)
                monthCode   = SUB(pDateCode,7,2)
                doWeekCheck = 1
            Of 'SAMSUNG'
                yearCode    = SUB(pDateCode,4,1)
                monthCode   = SUB(pDateCode,5,1)
            Of 'BOSCH'
                yearCode    = SUB(pDateCode,1,1)
                monthCode   = SUB(pDateCode,2,1)
            Of 'SIEMENS'
                yearCode    = SUB(pDateCode,1,1)
                monthCode    = SUB(pDateCode,2,1)
            Of 'MOTOROLA'
                yearCode    = SUB(pDateCode,5,1)
                monthCode   = SUB(pDateCode,6,1)            
            END ! CASE
            
            IF (doWeekCheck)
                yearValue   = yearCode
                
                startOfYear = DEFORMAT('1/1/' & yearValue,@d5)
                dayOfYear = startOfYear + (monthCode * 7)
                
                yearValue = YEAR(dayOfYear)
                monthValue = MONTH(dayOfYear)
            ELSE ! IF
                Access:MANUDATE.ClearKey(mad:DateCodeKey)
                mad:Manufacturer = pManufacturer
                IF (pManufacturer = 'ALCATEL')
                    mad:DateCode = SUB(pDateCode,1,1) & CLIP(monthCode) & CLIP(yearCode)
                ELSE ! IF
                    mad:DateCode = CLIP(yearCode) & CLIP(monthCode)
                END ! IF
                IF (Access:MANUDATE.TryFetch(mad:DateCodeKey) = Level:Benign)
                    yearValue = mad:TheYear
                    monthValue = mad:TheMonth
                ELSE ! IF
                    retValue = Level:Fatal
                    BREAK
                END ! IF
            END ! IF
            
            LOOP i = 1 TO man:ClaimPeriod
                monthValue += 1
                IF (monthValue > 12)
                    monthValue = 1
                    yearValue += 1
                END ! IF
            END ! LOOP
            
            IF (yearValue < YEAR(pDateBooked))
                retValue = Level:Fatal
                BREAK
            END ! IF
            
            IF (yearValue = YEAR(pDateBooked) AND monthValue < MONTH(pDateBooked))
                retValue = Level:Fatal
                BREAK
            END ! IF
        END ! LOOP
        DO CloseFiles
        
        RETURN retValue
!--------------------------------------
OpenFiles  ROUTINE
  Access:MANUDATE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUDATE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:MANUDATE.Close
     Access:MANUFACT.Close
     FilesOpened = False
  END
