

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER495.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! Check for saved entry. pAdd to add if missing. pDelete to delete any matching.
!!! </summary>
DuplicateTabCheckAdd PROCEDURE  (NetwebServerWorker p_web,STRING pType,STRING pValue) ! Declare Procedure
RetValue                    LONG(Level:Benign)
FilesOpened     BYTE(0)

  CODE
        !region ProcessedCode
    DO OpenFiles
        
    IF (Access:SBO_DupCheck.PrimeRecord() = Level:Benign)
        sbodup:SessionID = p_web.SessionID
        sbodup:DupType = pType
        sbodup:ValueField = pValue
        IF (Access:SBO_DupCheck.TryInsert())
            Access:SBO_DupCheck.CancelAutoInc()
        END ! IF
    END ! IF
        
    DO CloseFiles
        !endregion  
!--------------------------------------
OpenFiles  ROUTINE
  Access:SBO_DupCheck.Open                                 ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SBO_DupCheck.UseFile                              ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:SBO_DupCheck.Close
     FilesOpened = False
  END
