

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('WEBSERVER313.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
AllocationReport PROCEDURE (<NetWebServerWorker p_web>)

  ! The NetTalk Extension to ABC report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
PrintersName         STRING(50)                            !
EngineerName         STRING(50)                            !
disp:Company_Name    STRING(30)                            !
disp:Address_Line1   STRING(30)                            !
disp:Address_Line2   STRING(30)                            !
disp:Address_Line3   STRING(30)                            !
disp:Postcode        STRING(20)                            !
disp:Telephone_Number STRING(15)                           !
disp:Fax_Number      STRING(15)                            !
disp:EmailAddress    STRING(255)                           !
Disp:DateStamp       STRING(20)                            !
disp:ImeiNo          STRING(20)                            !
disp:ModelNumber     STRING(20)                            !
Disp:Accessories     STRING(100)                           !
TempAddingString     STRING(50)                            !
LimitID              LONG                                  !
JobCount             LONG                                  !
Process:View         VIEW(TagFile)
                       PROJECT(tag:sessionID)
                       PROJECT(tag:taggedValue)
                     END
ReportPageNumber     LONG,AUTO
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

Report               REPORT,AT(1000,3052,6250,6958),PRE(RPT),PAPER(PAPER:A4),FONT('Arial',8,,FONT:regular,CHARSET:DEFAULT), |
  THOUS
                       HEADER,AT(1000,1000,6250,1990),USE(?Header)
                         STRING(@N3),AT(4354,615),USE(ReportPageNumber),FONT(,,,FONT:bold),LEFT,TRN
                         STRING('Page Number:'),AT(3510,615),USE(?STRING1),TRN
                         STRING('Allocations Report'),AT(3510,83),USE(?STRING2),FONT(,10,,FONT:bold,CHARSET:DEFAULT)
                         STRING('Printed By:'),AT(3510,448),USE(?STRING3),TRN
                         STRING('Engineer:'),AT(3510,969),USE(?STRING4),TRN
                         STRING(@s50),AT(4354,448),USE(PrintersName),FONT(,,,FONT:bold),TRN
                         STRING(@s50),AT(4354,969),USE(EngineerName),FONT(,,,FONT:bold),TRN
                         STRING(@s30),AT(187,83,2979),USE(disp:Company_Name),FONT(,10,,FONT:bold),LEFT,TRN
                         STRING(@s30),AT(187,271,2979),USE(disp:Address_Line1)
                         STRING(@s30),AT(187,448,2979),USE(disp:Address_Line2)
                         STRING(@s30),AT(187,615,2979),USE(disp:Address_Line3)
                         STRING('Tel:'),AT(187,969),USE(?STRING5)
                         STRING('Fax:'),AT(187,1146),USE(?STRING6)
                         STRING('Email:'),AT(187,1323),USE(?STRING7)
                         STRING(@s15),AT(187,792),USE(disp:Postcode),LEFT,TRN
                         STRING(@s15),AT(667,969),USE(disp:Telephone_Number),LEFT,TRN
                         STRING(@s15),AT(667,1146),USE(disp:Fax_Number),LEFT,TRN
                         STRING(@s255),AT(667,1323,5531),USE(disp:EmailAddress),LEFT,TRN
                         STRING('Date Printed:'),AT(3510,792),USE(?STRING9),TRN
                         STRING(@s20),AT(4354,792),USE(Disp:DateStamp),FONT(,,,FONT:bold),TRN
                         STRING('Job'),AT(187,1760),USE(?STRING8),FONT(,,,FONT:bold),TRN
                         STRING('IMEI'),AT(875,1760),USE(?STRING10),FONT(,,,FONT:bold),TRN
                         STRING('Model'),AT(2208,1760),USE(?STRING11),FONT(,,,FONT:bold),TRN
                         STRING('Accessories'),AT(3510,1760),USE(?STRING12),FONT(,,,FONT:bold),TRN
                       END
Detail                 DETAIL,AT(0,0,6250,229),USE(?Detail)
                         STRING(@s10),AT(187,21),USE(tag:taggedValue)
                         STRING(@s20),AT(875,21),USE(disp:ImeiNo,,?disp:ImeiNo:2)
                         STRING(@s20),AT(2208,21),USE(disp:ModelNumber)
                         STRING(@s100),AT(3510,21,2635),USE(Disp:Accessories)
                       END
totals                 DETAIL,AT(0,0,6250,417),USE(?Totals)
                         STRING('Total Number of Jobs'),AT(83,167),USE(?STRING13),FONT(,,,FONT:bold)
                         STRING(@n10),AT(1417,167,792),USE(JobCount),FONT(,,,FONT:bold),LEFT
                         STRING('Processed By: _{11} Received By: _{11}'),AT(2198,167,3917),USE(?STRING14),FONT(,,, |
  FONT:bold)
                         LINE,AT(135,94,5792,0),USE(?LINE1)
                       END
                       FOOTER,AT(1000,10073,6250,83),USE(?Footer)
                       END
                       FORM,AT(1000,1000,6250,9688),USE(?Form)
                         BOX,AT(3333,333,2833,1000),USE(?BOX1),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1)
                         BOX,AT(42,1708,6125,250),USE(?BOX2),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1)
                         BOX,AT(42,2042,6125,7542),USE(?BOX3),COLOR(COLOR:Black),LINEWIDTH(1)
                       END
                     END
!*** PDF-Tools v4.1 Start (All)
!--- ClarioNET NOT Enabled
!--- PDFXTABCTemplate: True  Level: 4
!--- PDFXTABCTemplate: RPM: False
!--- PDFXTABCTemplate: Icetips: False
PDFXTR1               PDFXToolsReportClass                 !  Added by PDF-Tools Templates v4.1
PDFXTR1:rtn           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXTR1:vpf           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Orientation      BYTE(0)                      !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:TopMargin        REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:BottomMargin     REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:LeftMargin       REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:RightMargin      REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Selection        STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PDFXT4:Papers:Queue           QUEUE,PRE()                  !  Added by PDF-Tools Templates v4.1
PLQ:Description                 STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PLQ:PaperCode                   BYTE                       !  Added by PDF-Tools Templates v4.1
                              END                          !  Added by PDF-Tools Templates v4.1
!*** PDF-Tools v4.1 End
ThisWindow           CLASS(ReportManager)
EndReport              PROCEDURE(),BYTE,DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepStringClass                       ! Progress Manager
Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.EndReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  PRINT(rpt:Totals)
  ReturnValue = PARENT.EndReport()
  RETURN ReturnValue


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('AllocationReport')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:JOBACC.SetOpenRelated()
  Relate:JOBACC.Open                                       ! File JOBACC used by this procedure, so make sure it's RelationManager is open
  Relate:TagFile.Open                                      ! File TagFile used by this procedure, so make sure it's RelationManager is open
  Access:JOBS.UseFile                                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:USERS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  !preliminaries
  
      ReportPageNumber = 1
  
      !EngineerName 
      Access:Users.clearkey(use:User_Code_Key)
      use:User_Code = p_web.gsv('NewEngineer')
      if access:users.fetch(use:User_Code_Key)
          EngineerName = 'NOT FOUND'
      ELSE
          EngineerName = clip(use:Forename)&' '&clip(use:Surname)
      END
  
      !PrintersName 
      access:users.clearkey(use:User_Code_Key)
      use:User_Code = p_web.GSV('BookingUserCode') !glo:password ! #13433 Do not use Global Vars!! (DBH: 21/11/2014)
      if access:users.fetch(use:User_Code_Key)
          PrintersName = 'NOT FOUND'
      ELSE
          PrintersName =  clip(use:Forename)&' '&clip(use:Surname)
      End 
  
      !find the correct tradeacc whilst we have printer as user
      Access:TRADEACC.Clearkey(tra:SiteLocationKey)
      tra:SiteLocation = use:Location
      IF Access:TRADEACC.Tryfetch(tra:SiteLocationKey) 
          !error
          tra:Company_Name = 'NOT FOUND'
      END
      disp:Company_Name       = tra:Company_Name
      disp:Address_Line1      = tra:Address_Line1
      disp:Address_Line2      = tra:Address_Line2
      disp:Address_Line3      = tra:Address_Line3
      disp:Postcode           = tra:Postcode
      disp:Telephone_Number   = tra:Telephone_Number
      disp:Fax_Number         = tra:Fax_Number
      disp:EmailAddress       = tra:EmailAddress
  
      Disp:DateStamp = format(today(),@d06)&' at '&format(clock(),@t1)
  
      LimitID = p_web.SessionID
      JobCount = 0
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('AllocationReport',ProgressWindow)          ! Restore window settings from non-volatile store
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    loc:pdfname = '.\$$$' & format(random(1,99999),@n05) &'.pdf'
    ProgressWindow{prop:hide} = 1
  End
  ProgressMgr.Init(ScrollSort:AllowAlpha+ScrollSort:AllowNumeric,ScrollBy:RunTime)
  ThisReport.Init(Process:View, Relate:TagFile, ?Progress:PctText, Progress:Thermometer, ProgressMgr, tag:taggedValue)
  ThisReport.CaseSensitiveValue = FALSE
  ThisReport.AddSortOrder(tag:keyTagged)
  ThisReport.AddRange(tag:sessionID,LimitID)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:TagFile.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      Loc:html = '<script type="text/javascript">alert('''&clip('No Records')&''');top.close();</script>'
      p_web.ParseHTML(loc:html)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBACC.Close
    Relate:TagFile.Close
  END
  IF SELF.Opened
    INIMgr.Update('AllocationReport',ProgressWindow)       ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.OpenReport()
  !Main loop to fetch records goes in here?
       
  !    Access:TagFile.clearkey(tag:keyTagged)
  !    tag:sessionID = LimitID
  !    set(tag:keyTagged,tag:keyTagged)
  !    LOOP
  !        if access:Tagfile.next() then break.
  !        if tag:sessionID <> LimitID then break.  
  !        
  !    END !Loop through tagfile
  
  !no - in report porperties - range limit
  !*** PDF-Tools v4.1 Start (ABC)
  IF ~ReturnValue                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.Init(Report, loc:PDFName,,PDFA_OFF)            !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPaper(CHOOSE(SYSTEM{PROP:Autopaper}=True,Report{PROPPRINT:Paper},PAPER:USER), CHOOSE(Report{PROP:Thous}=True,PROP:Thous,CHOOSE(Report{PROP:MM}=True,PROP:MM,CHOOSE(Report{PROP:Points}=True,PROP:Points,0))), Report{PROPPRINT:PaperWidth}, Report{PROPPRINT:PaperHeight}, Report{PROP:Landscape}) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetCompression(TRUE,TRUE,FALSE,COMPRTYPE_C_AUTO,75,COMPRTYPE_I_AUTO,COMPRTYPE_M_AUTO) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetAuthor('ServiceBase 3g')          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetTitle('Allocation Report')        !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetSubject('ServiceBase 3g')         !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPdfCreator('ServiceBase 3g')      !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetKeywords()                        !  Added by PDF-Tools Templates v4.1
    PDFXTR1.EnableLinkAnalyzer(True)             !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetSpecVersion(SPECVERSION_15,TRUE)  !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPageLayout(PAGELAYOUT_SINGLEPAGE) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPageMode(PAGEMODE_NONE)           !  Added by PDF-Tools Templates v4.1
    IF PAGEMODE_NONE = PAGEMODE_FULLSCREEN       !  Added by PDF-Tools Templates v4.1
      PDFXTR1:vpf = VP_FSPM_NONE                 !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
      PDFXTR1:vpf = 0                            !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDETOOLBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDEMENUBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDEWINDOWUI). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_FITWINDOW). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_CENTERWINDOW). !  Added by PDF-Tools Templates v4.1
      IF TRUE                                     THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_DISPLAYDOCTITLE). !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetViewerPrefs(PDFXTR1:vpf)          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPreview(False)                    !  Added by PDF-Tools Templates v4.1
  END
  !*** PDF-Tools v4.1 End
  IF ReturnValue = Level:Benign
    Report$?ReportPageNumber{PROP:PageNo} = True
  END
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeCloseEvent()
  !*** PDF-Tools v4.1 Start (ABC)
  PDFXTR1.Kill()
  !*** PDF-Tools v4.1 End
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
      !when each record is fetched this code is run.
      JobCount += 1          
      
      Access:jobs.clearkey(job:Ref_Number_Key)
      job:Ref_Number = deformat(tag:taggedValue)
      if access:jobs.fetch(job:Ref_Number_Key) 
        !error
      END !if fetch worked
        
      !imei number
      disp:ImeiNo = job:esn
      
      !model Number
      disp:ModelNumber = Clip(job:Model_Number) & ' ' & Clip(job:Manufacturer)
      
      !accessories from jobacc where ref_number = ref_number
      Disp:Accessories = ''   !reset it for the new handset
      
      Access:JOBACC.clearkey(jac:Ref_Number_Key)
      jac:Ref_Number = job:Ref_Number
      set(jac:Ref_Number_Key,jac:Ref_Number_Key)
      LOOP
          if access:jobacc.next() then break.
          if jac:Ref_Number <> job:Ref_Number then break.
          if jac:Damaged then 
              TempAddingString = 'DAMAGED '&clip(jac:Accessory)
          ELSE
              TempAddingString = jac:Accessory
          END
          if clip(Disp:Accessories) = '' THEN
              Disp:Accessories = TempAddingString
          ELSE
              Disp:Accessories = clip(Disp:Accessories)&', '&clip(TempAddingString)
          END        
      END !loop through jobacc        
  
      !print and count   
      PRINT(RPT:Detail)
      
  ReturnValue = PARENT.TakeRecord()
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  !*** PDF-Tools v4.1 Start (ABC)
  ReturnValue = False                            !  Added by PDF-Tools Templates v4.1
    PDFXTR1:rtn = PDFXTR1.Generate(SELF.ImageQueue,True, , ) !  Added by PDF-Tools Templates v4.1
    IF PDFXTR1:rtn THEN                          !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
  RETURN ReturnValue
  !*** PDF-Tools v4.1 End
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
  RETURN ReturnValue

