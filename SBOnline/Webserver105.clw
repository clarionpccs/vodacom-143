

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER105.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
DefaultLabourCost    PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
SUBCHRGE::State  USHORT
TRACHRGE::State  USHORT
STDCHRGE::State  USHORT
FilesOpened     BYTE(0)

  CODE
    do OpenFiles
    Do saveFiles
    p_web.SSV('DefaultLabourCost',0)
    If InvoiceSubAccounts(p_web.GSV('job:Account_Number'))
        access:subchrge.clearkey(suc:model_repair_type_key)
        suc:account_number = p_web.GSV('job:account_number')
        suc:model_number   = p_web.GSV('job:model_number')
        suc:charge_type    = p_web.GSV('job:charge_type')
        suc:unit_type      = p_web.GSV('job:unit_type')
        suc:repair_type    = p_web.GSV('job:repair_type')
        if access:subchrge.fetch(suc:model_repair_type_key)
            access:trachrge.clearkey(trc:account_charge_key)
            trc:account_number = sub:main_account_number
            trc:model_number   = p_web.GSV('job:model_number')
            trc:charge_type    = p_web.GSV('job:charge_type')
            trc:unit_type      = p_web.GSV('job:unit_type')
            trc:repair_type    = p_web.GSV('job:repair_type')
            if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
                p_web.SSV('DefaultLabourCost',trc:RRCRate)
            End!if access:trachrge.fetch(trc:account_charge_key)
        Else
            p_web.SSV('DefaultLabourCost',suc:RRCRate)
        End!if access:subchrge.fetch(suc:model_repair_type_key)
    Else !If InvoiceSubAccounts(job:Account_Number)
        access:trachrge.clearkey(trc:account_charge_key)
        trc:account_number = sub:main_account_number
        trc:model_number   = p_web.GSV('job:model_number')
        trc:charge_type    = p_web.GSV('job:charge_type')
        trc:unit_type      = p_web.GSV('job:unit_type')
        trc:repair_type    = p_web.GSV('job:repair_type')
        if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
            p_web.SSV('DefaultLabourCost',trc:RRCRate)
        End!if access:trachrge.fetch(trc:account_charge_key)
    
    End !If InvoiceSubAccounts(job:Account_Number)
    
    If p_web.GSV('DefaultLabourCost') = 0 then
        access:stdchrge.clearkey(sta:model_number_charge_key)
        sta:model_number = p_web.GSV('job:model_number')
        sta:charge_type  = p_web.GSV('job:charge_type')
        sta:unit_type    = p_web.GSV('job:unit_type')
        sta:repair_type  = p_web.GSV('job:repair_type')
        if access:stdchrge.fetch(sta:model_number_charge_key)
        Else !if access:stdchrge.fetch(sta:model_number_charge_key)
            p_web.SSV('DefaultLabourCost',sta:RRCRate)
        end !if access:stdchrge.fetch(sta:model_number_charge_key)
    End !LocalDefaultLabour = 0 then
    do RestoreFiles
    do CloseFiles
SaveFiles  ROUTINE
  SUBCHRGE::State = Access:SUBCHRGE.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  TRACHRGE::State = Access:TRACHRGE.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  STDCHRGE::State = Access:STDCHRGE.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF SUBCHRGE::State <> 0
    Access:SUBCHRGE.RestoreFile(SUBCHRGE::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF TRACHRGE::State <> 0
    Access:TRACHRGE.RestoreFile(TRACHRGE::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF STDCHRGE::State <> 0
    Access:STDCHRGE.RestoreFile(STDCHRGE::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:SUBCHRGE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBCHRGE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRACHRGE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRACHRGE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:STDCHRGE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STDCHRGE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:SUBCHRGE.Close
     Access:TRACHRGE.Close
     Access:STDCHRGE.Close
     FilesOpened = False
  END
