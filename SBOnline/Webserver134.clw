

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER134.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER022.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER024.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER140.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER141.INC'),ONCE        !Req'd for module callout resolution
                     END


FormEngineeringOption PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locUserPassword      STRING(30)                            !
locEngineeringOption STRING(30)                            !
locExchangeManufacturer STRING(30)                         !
locExchangeModelNumber STRING(30)                          !
locExchangeNotes     STRING(255)                           !
locNewEngineeringOption STRING(20)                         !
locSplitJob          BYTE                                  !
FilesOpened     Long
USERS::State  USHORT
ACCAREAS::State  USHORT
MODELNUM::State  USHORT
JOBS::State  USHORT
MANUFACT::State  USHORT
STOCK::State  USHORT
WARPARTS::State  USHORT
PARTS::State  USHORT
STOMODEL::State  USHORT
AUDIT::State  USHORT
STOCKALL::State  USHORT
JOBSE::State  USHORT
JOBSENG::State  USHORT
REPTYDEF::State  USHORT
WEBJOB::State  USHORT
EXCHOR48::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormEngineeringOption')
  loc:formname = 'FormEngineeringOption_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormEngineeringOption','Change')
    p_web._DivHeader('FormEngineeringOption',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormEngineeringOption',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormEngineeringOption',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormEngineeringOption',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormEngineeringOption',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormEngineeringOption',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormEngineeringOption',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
add:ExchangeOrderPart         Routine
data
local:foundUnit             Byte(0)
code
    !Check to see if a part already exists

    If p_web.GSV('job:Warranty_Job') = 'YES'
        Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
        wpr:Ref_Number  = p_web.GSV('job:Ref_Number')
        wpr:Part_Number = 'EXCH'
        Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
        Loop
            If Access:WARPARTS.NEXT()
               Break
            End !If
            If wpr:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
            Or wpr:Part_Number <> 'EXCH'      |
                Then Break.  ! End If
            local:FoundUnit = True
            If local:FoundUnit
                wpr:Status  = 'ORD'
                Access:WARPARTS.Update()

                Access:STOCKALL.Clearkey(stl:PartRecordTypeKey)
                stl:PartType    = 'WAR'
                stl:PartRecordNumber    = wpr:Record_Number
                if (Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign)
                    ! Found
                    relate:STOCKALL.delete(0)
                else ! if (Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign)
                    ! Error
                end ! if (Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign)
                Break
            End !If local:FoundUnit
        End !Loop

    End !If job:Warranty_Job = 'YES'

    If local:FoundUnit = True
        If p_web.GSV('ob:Chargeable_Job') = 'YES'
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number  = p_web.GSV('job:Ref_Number')
            par:Part_Number = 'EXCH'
            Set(par:Part_Number_Key,par:Part_Number_Key)
            Loop
                If Access:PARTS.NEXT()
                   Break
                End !If
                If par:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
                Or par:Part_Number <> 'EXCH'      |
                    Then Break.  ! End If
                local:FoundUnit = True
                If local:FoundUnit
                    par:Status  = 'ORD'
                    Access:PARTS.Update()
                    Access:STOCKALL.Clearkey(stl:PartRecordTypeKey)
                    stl:PartType    = 'CHA'
                    stl:PartRecordNumber    = par:Record_Number
                    if (Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign)
                        ! Found
                        relate:STOCKALL.delete(0)
                    else ! if (Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign)
                        ! Error
                    end ! if (Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign)
                    Break
                End !If local:FoundUnit
            End !Loop
        End !If job:Chargeable_Job = 'YES'
    End !If FoundEXCHPart# = 0

    If local:FoundUnit = False
        If p_web.GSV('job:Engineer') = ''
            Access:USERS.Clearkey(use:Password_Key)
            use:Password    = p_web.GSV('BookingUserPassword')
            If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                !Found

            Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                !Error
            End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        Else !If job:Engineer = ''
            Access:USERS.Clearkey(use:User_Code_Key)
            use:User_Code   = p_web.GSV('job:Engineer')
            If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign

            End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        End !If job:Engineer = ''

         !Found
        Access:STOCK.Clearkey(sto:Location_Key)
        sto:Location    = use:Location
        sto:Part_Number = 'EXCH'
        If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
            !Found
            Access:LOCATION.Clearkey(loc:Location_Key)
            loc:Location    = sto:Location
            If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
                !Found
            Else ! If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
                !Error
            End !If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
            If p_web.GSV('job:Warranty_Job') = 'YES'
               get(warparts,0)
               if access:warparts.primerecord() = level:benign
                   wpr:PArt_Ref_Number      = sto:Ref_Number
                   wpr:ref_number            = p_web.GSV('job:ref_number')
                   wpr:adjustment            = 'YES'
                   wpr:part_number           = 'EXCH'
                   wpr:description           = p_web.GSV('job:Manufacturer') & ' EXCHANGE UNIT'
                   wpr:quantity              = 1
                   wpr:warranty_part         = 'NO'
                   wpr:exclude_from_order    = 'YES'
                   wpr:PartAllocated         = 1
                   wpr:Status                = 'ORD'
                   wpr:ExchangeUnit          = True
                   wpr:SecondExchangeUnit    = 0
                   If sto:Assign_Fault_Codes = 'YES'
                       !Try and get the fault codes. This key should get the only record
                       Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                       stm:Ref_Number  = sto:Ref_Number
                       stm:Part_Number = sto:Part_Number
                       stm:Location    = sto:Location
                       stm:Description = sto:Description
                       If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                           !Found
                           wpr:Fault_Code1  = stm:FaultCode1
                           wpr:Fault_Code2  = stm:FaultCode2
                           wpr:Fault_Code3  = stm:FaultCode3
                           wpr:Fault_Code4  = stm:FaultCode4
                           wpr:Fault_Code5  = stm:FaultCode5
                           wpr:Fault_Code6  = stm:FaultCode6
                           wpr:Fault_Code7  = stm:FaultCode7
                           wpr:Fault_Code8  = stm:FaultCode8
                           wpr:Fault_Code9  = stm:FaultCode9
                           wpr:Fault_Code10 = stm:FaultCode10
                           wpr:Fault_Code11 = stm:FaultCode11
                           wpr:Fault_Code12 = stm:FaultCode12
                       Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                       !Error
                       !Assert(0,'<13,10>Fetch Error<13,10>')
                       End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                   end !If sto:Assign_Fault_Codes = 'YES'
                   if access:warparts.insert()
                       access:warparts.cancelautoinc()
                   end
                End !If Prime

             Else !If job:Warranty_Job = 'YES'
                If p_web.GSV('job:Chargeable_Job') = 'YES'
                    get(parts,0)
                    if access:parts.primerecord() = level:benign
                        !message('At break2')
                        par:PArt_Ref_Number      = sto:Ref_Number
                        par:ref_number            = p_web.GSV('job:ref_number')
                        par:adjustment            = 'YES'
                        par:part_number           = 'EXCH'
                        par:description           = p_web.GSV('job:Manufacturer') & ' EXCHANGE UNIT'
                        par:quantity              = 1
                        par:warranty_part         = 'NO'
                        par:exclude_from_order    = 'YES'
                        par:PartAllocated         = 1
                        par:Status                = 'ORD'
                        par:ExchangeUnit          = True
                        par:SecondExchangeUnit    = 0
                        If sto:Assign_Fault_Codes = 'YES'
                           !Try and get the fault codes. This key should get the only record
                           Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                           stm:Ref_Number  = sto:Ref_Number
                           stm:Part_Number = sto:Part_Number
                           stm:Location    = sto:Location
                           stm:Description = sto:Description
                           If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                               !Found
                               par:Fault_Code1  = stm:FaultCode1
                               par:Fault_Code2  = stm:FaultCode2
                               par:Fault_Code3  = stm:FaultCode3
                               par:Fault_Code4  = stm:FaultCode4
                               par:Fault_Code5  = stm:FaultCode5
                               par:Fault_Code6  = stm:FaultCode6
                               par:Fault_Code7  = stm:FaultCode7
                               par:Fault_Code8  = stm:FaultCode8
                               par:Fault_Code9  = stm:FaultCode9
                               par:Fault_Code10 = stm:FaultCode10
                               par:Fault_Code11 = stm:FaultCode11
                               par:Fault_Code12 = stm:FaultCode12
                           Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                               !Error
                               !Assert(0,'<13,10>Fetch Error<13,10>')
                           End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                        End !If sto:Assign_Fault_Codes = 'YES'
                        if access:parts.insert()
                            access:parts.cancelautoinc()
                        end
                    End !If access:Prime

                End !If job:Chargeable_Job = 'YES'
             End !If job:Warranty_Job = 'YES'

        Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
             !Error
        End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
    End !If FoundEXCHPart# = 0
create:ExchangeOrder        Routine
    if (Access:EXCHOR48.PrimeRecord() = Level:Benign)
        ex4:Location    = p_web.GSV('Default:SiteLocation')
        ex4:Manufacturer    = p_web.GSV('locExchangeManufacturer')
        ex4:ModelNumber    = p_web.GSV('locExchangeModelNumber')
        ex4:Received    = 0
        ex4:Notes    = p_web.GSV('locExchangeNotes')
        ex4:JobNumber    = p_web.GSV('wob:RefNumber')
                
        if (Access:EXCHOR48.TryInsert() = Level:Benign)
                    ! Inserted

            p_web.SSV('jobe:Engineer48HourOption',1)
            p_web.SSV('locEngineeringOption','48 Hour Exchange')

!            p_web.SSV('AddToAudit:Type','JOB')
!            p_web.SSV('AddToAudit:Action','48 HOUR EXCHANGE ORDER CREATED')
!            p_web.SSV('AddToAudit:Notes','MANUFACTURER: ' & p_web.GSV('locExchangeManufacturer') & |
!                '<13,10>MODEL NUMBER: ' & p_web.GSV('locExchangeModelNumber'))
            AddToAudit(p_web,p_web.GSV('jobe:RefNumber'),'JOB','48 HOUR EXCHANGE ORDER CREATED','MANUFACTURER: ' & p_web.GSV('locExchangeManufacturer') & |
                '<13,10>MODEL NUMBER: ' & p_web.GSV('locExchangeModelNumber'))

!            p_web.SSV('AddToAudit:Type','JOB')
!            p_web.SSV('AddToAudit:Action','ENGINEERING OPTION SELECTED: 48 HOUR EXCHANGE')
!            p_web.SSV('AddToAudit:Notes','')
            AddToAudit(p_web,p_web.GSV('jobe:RefNumber'),'JOB','ENGINEERING OPTION SELECTED: 48 HOUR EXCHANGE','')


            p_web.SSV('GetStatus:StatusNumber',360)
            p_web.SSV('GetStatus:Type','EXC')

            GetStatus(360,0,'EXC',p_web)

            p_web.SSV('GetStatus:StatusNumber',355)
            p_web.SSV('GetStatus:Type','JOB')

            GetStatus(355,0,'JOB',p_web)

! #11939 Don't force split (even though original spec states you should) (Bryan: 01/03/2011)
!            !If warranty job, auto change to spilt chargeable - 3876 (DBH: 29-03-2004)
!            if (p_web.GSV('job:Warranty_Job') = 'YES' and p_web.GSV('job:Chargeable_Job') <> 'YES')
!                p_web.SSV('job:Chargeable_Job','YES')
!                if p_web.GSV('BookingSite') = 'RRC'
!                    p_web.SSV('job:Charge_Type','NON-WARR SERVICE FEE')
!                else ! if p_web.GSV('BookingSite') = 'RRC'
!                    p_web.SSV('job:Charge_Type','NON-WARRANTY')
!                end ! if p_web.GSV('BookingSite') = 'RRC'
!                p_web.SSV('job:Repair_Type','48-HOUR SERVICE FEE')
!            end ! if ( p_web.GSV('job:Warranty_Job') = 'YES' and p_web.GSV('job:Chargeable_Job') <> 'YES')
            
            CASE (p_web.GSV('locSplitJob'))
            OF 1 ! Chargeable Only
                p_web.SSV('job:Chargeable_Job','YES')
                p_web.SSV('job:Warranty_Job','NO')
            OF 2 ! Warranty Only
                p_web.SSV('job:Chargeable_Job','NO')
                p_web.SSV('job:Warranty_Job','YES')
            OF 3 ! Split
                p_web.SSV('job:Chargeable_Job','YES')
                p_web.SSV('job:Warranty_Job','YES')
            END

            IF (p_web.GSV('job:Chargeable_Job') = 'YES')
                if p_web.GSV('BookingSite') = 'RRC'
                    p_web.SSV('job:Charge_Type','NON-WARR SERVICE FEE')
                else ! if p_web.GSV('BookingSite') = 'RRC'
                    p_web.SSV('job:Charge_Type','NON-WARRANTY')
                end ! if p_web.GSV('BookingSite') = 'RRC'
                p_web.SSV('job:Repair_Type','48-HOUR SERVICE FEE')
            END

            do add:ExchangeOrderPart

            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber    = p_web.GSV('wob:RefNumber')
            if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                ! Found
                p_web.SessionQueueToFile(JOBSE)
                Access:JOBSE.TryUpdate()
            else ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                ! Error
            end ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)

            Access:JOBS.Clearkey(job:Ref_Number_Key)
            job:Ref_Number    = p_web.GSV('wob:RefNumber')
            if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                            ! Found
                p_web.SessionQueueToFile(JOBS)
                Access:JOBS.TryUpdate()
            else ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                            ! Error
            end ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
        else ! if (Access:EXCHOR48.TryInsert() = Level:Benign)
                    ! Error
            Access:EXCHOR48.CancelAutoInc()
        end ! if (Access:EXCHOR48.TryInsert() = Level:Benign)
    end ! if (Access:EXCHOR48.PrimeRecord() = Level:Benign)

    p_web.SSV('exchangeOrderCreated',1)
    p_web.SSV('ExchangeOrder:RecordNumber',ex4:RecordNumber)
    p_web.SSV('locWarningMessage','Exchange Order Created')

set:HubRepair      routine
    p_web.SSV('jobe:HubRepairDate',Today())
    p_web.SSV('jobe:HubRepairTime',Clock())

    p_web.SSV('GetStatus:StatusNumber',sub(GETINI('RRC','StatusSendToARC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3))
    p_web.SSV('GetStatus:Type','JOB')
    GetStatus(sub(GETINI('RRC','StatusSendToARC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3),0,'JOB',p_web)

    if (p_web.GSV('BookingSite') = 'RRC')
        if (p_web.GSV('job:Exchange_Unit_Number') > 0)
            Access:REPTYDEF.Clearkey(rtd:ManRepairTypeKey)
            rtd:Manufacturer    = p_web.GSV('job:Manufacturer')
            set(rtd:ManRepairTypeKey,rtd:ManRepairTypeKey)
            loop
                if (Access:REPTYDEF.Next())
                    Break
                end ! if (Access:REPTYDEF.Next())
                if (rtd:Manufacturer    <> p_web.GSV('job:Manufacturer'))
                    Break
                end ! if (rtd:Manufacturer    <> p_web.GSV('job:Manufacturer'))
                if (rtd:BER = 11)
                    if (p_web.GSV('job:Chargeable_Job') = 'YES' and p_web.GSV('job:Repair_Type') = '')
                        p_web.SSV('job:Repair_Type',rtd:Repair_Type)
                    end ! if (p_web.GSV('job:Chargeable_Job') = 'YES' and p_web.GSV('job:Repair_Type') = '')
                    if (p_web.GSV('job:Warranty_Job') = 'YES' and p_web.GSV('job:Repair_Type_Warranty') = '')
                        p_web.SSV('job:Repair_Type_Warranty',rtd:Repair_Type)
                    end ! if (p_web.GSV('job:Chargeable_Job') = 'YES' and p_web.GSV('job:Repair_Type') = '')
                    break
                end ! if (rtd:BER = 11)
            end ! loop
        end ! if (p_web.GSV('job:Exchange_Unit_Number') > 0)
    end ! if (p_web.GSV('BookingSite') = 'RRC')


    Access:JOBSENG.Clearkey(joe:UserCodeKey)
    joe:JobNumber    = p_web.GSV('job:Ref_Number')
    joe:UserCode    = p_web.GSV('job:Engineer')
    joe:DateAllocated    = Today()
    set(joe:UserCodeKey,joe:UserCodeKey)
    loop
        if (Access:JOBSENG.Previous())
            Break
        end ! if (Access:JOBSENG.Next())
        if (joe:JobNumber    <> p_web.GSV('job:Ref_Number'))
            Break
        end ! if (joe:JobNumber    <> p_web.GSV('job:Ref_Number'))
        if (joe:UserCode    <> p_web.GSV('job:Engineer'))
            Break
        end ! if (joe:UserCode    <> p_web.GSV('job:Engineer'))
        if (joe:DateAllocated    > Today())
            Break
        end ! if (joe:DateAllocated    <> Today())
        joe:Status = 'HUB'
        joe:StatusDate = Today()
        joe:StatusTime = Clock()
        access:JOBSENG.update()
        break
    end ! loop


    if (p_web.GSV('jobe2:SMSNotification'))
        if (p_web.GSV('job:Who_Booked') = 'WEB')
            AddEmailSMS(p_web.GSV('job:Ref_Number'),p_web.GSV('job:Account_Number'),'2ARC','SMS',p_web.GSV('jobe2:SMSAlertNumber'),'',0,'')
        else ! if (p_web.GSV('job:Who_Booked') = 'WEB')
            AddEmailSMS(p_web.GSV('job:Ref_Number'),p_web.GSV('BookingAccount'),'2ARC','SMS',p_web.GSV('jobe2:SMSAlertNumber'),'',0,'')
        end ! if (p_web.GSV('job:Who_Booked') = 'WEB')
    end ! if (jobe2:SMSNotification)
    if (p_web.GSV('jobe2:EmailNotification'))
        if (p_web.GSV('job:Who_Booked') = 'WEB')
            AddEmailSMS(p_web.GSV('job:Ref_Number'),p_web.GSV('job:Account_Number'),'2ARC','EMAIL','',p_web.GSV('jobe2:EmailAlertNumber'),0,'')
        else ! if (p_web.GSV('job:Who_Booked') = 'WEB')
            AddEmailSMS(p_web.GSV('job:Ref_Number'),p_web.GSV('BookingAccount'),'2ARC','EMAIL','',p_web.GSV('jobe2:EmailAlertNumber'),0,'')
        end ! if (p_web.GSV('job:Who_Booked') = 'WEB')
    end ! if (jobe2:SMSNotification)
Validate:locEngineeringOption           Routine
    p_web.SSV('locErrorMessage','')
    case p_web.GSV('locNewEngineeringOption')
    of 'Standard Repair'
        if (p_web.GSV('BookingSite') = 'RRC')
            Access:MODELNUM.Clearkey(mod:Model_Number_Key)
            mod:Model_Number    = p_web.GSV('job:Model_Number')
            if (Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign)
                ! Found
                if (mod:ExcludedRRCRepair)
                    p_web.SSV('locErrorMessage','Warning! This model should not be repaired by an RRC and should be sent to the Hub.')
                    if (isUnitLiquidDamaged(p_web.GSV('job:Ref_Number'),p_web.GSV('job:ESN')))
                        p_web.SSV('locErrorMessage','Error! Cannot send to ARC. This Unit Is Liquid Damaged.')
                        p_web.SSV('locValidationFailed',1)
                        p_web.SSV('locNewEngineeringOption','')
                        Exit
                    end ! if (isUnitLiquidDamage(p_web.GSV('job:Ref_Number'),p_web.GSV('job:ESN'))

                end ! if (mod:ExcludedRRCRepair)
            else ! if (Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign)
                ! Error
            end ! if (Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign)
        end ! if (p_web.GSV('BookingSite') = 'RRC')
    of '48 Hour Exchange'
        if (is48HourOrderCreated(p_web.GSV('BookingSiteLocation'),p_web.GSV('job:Ref_Number')) = 1)
            p_web.SSV('locErrorMessage','Error! A 48 Hour Exchange has already been requested for this job.')
            p_web.SSV('locValidationFailed',1)
            Exit
        end ! if (is48HourOrderCreated(p_web.GSV('BookingSiteLocation'),p_web.GSV('job:Ref_Number')) = 1)
        if (is48HourOrderProcessed(p_web.GSV('BookingSiteLocation'),p_web.GSV('job:Ref_Number')) = 1)
            p_web.SSV('locErrorMessage','Error! A 48 Hour Exchange Unit has already been requested and despatched for this job.')
            p_web.SSV('locValidationFailed',1)
            Exit
        end ! if (is48HourOrderProcessed(p_web.GSV('BookingSiteLocation'),p_web.GSV('job:Ref_Number')) = 1)
        IF (p_web.GSV('job:Chargeable_Job') <> 'YES')
            ! #11939 Job is warranty only, show the "split job" option. (Bryan: 01/03/2011)
            p_web.SSV('locSplitJob',0)
            p_web.SSV('Hide:SplitJob',0)
        ELSE
            ! #11980 Job is chargeable so no need to show split option. (Bryan: 01/03/2011)
            IF (p_web.GSV('job:Warranty_Job') = 'YES')
                p_web.SSV('locSplitJob',3)
            ELSE
                p_web.SSV('locSplitJob',1)
            END
        END
        
    of 'ARC Repair'
        if (isUnitLiquidDamaged(p_web.GSV('job:Ref_Number'),p_web.GSV('job:ESN')))
            p_web.SSV('locErrorMessage','Error! Cannot send to ARC. This Unit Is Liquid Damaged.')
            p_web.SSV('locValidationFailed',1)
            p_web.SSV('locNewEngineeringOption','')
            Exit
        end ! if (isUnitLiquidDamage(p_web.GSV('job:Ref_Number'),p_web.GSV('job:ESN'))
    of '7 Day TAT'
        if (isUnitLiquidDamaged(p_web.GSV('job:Ref_Number'),p_web.GSV('job:ESN')))
            p_web.SSV('locErrorMessage','Error! Cannot send to ARC. This Unit Is Liquid Damaged.')
            p_web.SSV('locValidationFailed',1)
            p_web.SSV('locNewEngineeringOption','')
            Exit
        end ! if (isUnitLiquidDamage(p_web.GSV('job:Ref_Number'),p_web.GSV('job:ESN'))
    end ! case p_web.GSV('locEngineeringOption')
Validate:locExchangeModelNumber           Routine
    if (p_web.GSV('locExchangeModelNumber') <> p_web.GSV('job:Model_Number'))
        if (isThisModelAlternative(p_web.GSV('job:Model_Number'),p_web.GSV('locExchangeModelNumber')) = 0)
            p_web.SSV('locWarningMessage','Warning! The selected unit is not an "alternative" Model Number for this job!')                        
        else ! if (isThisModelAlternative(p_web.GSV('job:Model_Number'),p_web.GSV('locExchangeModelNumber')) = 0)
            p_web.SSV('locWarningMessage','Warning! You have selected a different Model Number for this job!')
        end ! if (isThisModelAlternative(p_web.GSV('job:Model_Number'),p_web.GSV('locExchangeModelNumber')) = 0)
    else ! if (p_web.GSV('locExchangeModelNumber') <> p_web.GSV('job:Model_Number'))
        p_web.SSV('locWarningMessage','')
    end ! if (p_web.GSV('locExchangeModelNumber') <> p_web.GSV('job:Model_Number'))

OpenFiles  ROUTINE
  p_web._OpenFile(USERS)
  p_web._OpenFile(ACCAREAS)
  p_web._OpenFile(MODELNUM)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(MANUFACT)
  p_web._OpenFile(STOCK)
  p_web._OpenFile(WARPARTS)
  p_web._OpenFile(PARTS)
  p_web._OpenFile(STOMODEL)
  p_web._OpenFile(AUDIT)
  p_web._OpenFile(STOCKALL)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(JOBSENG)
  p_web._OpenFile(REPTYDEF)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(EXCHOR48)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(ACCAREAS)
  p_Web._CloseFile(MODELNUM)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(MANUFACT)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(STOMODEL)
  p_Web._CloseFile(AUDIT)
  p_Web._CloseFile(STOCKALL)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBSENG)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(EXCHOR48)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  !Init Form
  if (p_web.GSV('FormEngineeringOption:FirstTime') = 1)
      p_web.SSV('locPasswordValidated',0)
      p_web.SSV('locValidationFailed',0)
      p_web.SSV('locUserPassword','')
      p_web.SSV('locErrorMessage','')
      p_web.SSV('locWarningMessage','')
      p_web.SSV('Comment:UserPassword','Enter User Password and press [TAB]')
      p_web.SSV('filter:Manufacturer','')
      p_web.SSV('locNewEngineeringOption','')
      p_web.SSV('locExchangeManufacturer',p_web.GSV('job:Manufacturer'))
      p_web.SSV('locExchangeModelNumber',p_web.GSV('job:Model_number'))
      p_web.SSV('locExchangeNotes','')
      p_web.SSV('locSplitJob',0)
      p_web.SSV('Hide:SplitJob',1)
  
      p_web.SSV('exchangeOrderCreated',0)
  
      p_web.SSV('FormEngineeringOption:FirstTime',0)
  
      ! Activate the 48 Hour option and show the booking option (DBH: 24-03-2005)
      p_web.SSV('hide:48HourOption',0)
      If AccountActivate48Hour(p_web.GSV('wob:HeadAccountNumber')) = True
          If Allow48Hour(p_web.GSV('job:ESN'),p_web.GSV('job:Model_Number'),p_web.GSV('wob:HeadAccountNumber')) = True
  
          Else ! If Allow48Hour(job:ESN,job:Model_Number) = True
              p_web.SSV('hide:48HourOption',1)
          End ! If Allow48Hour(job:ESN,job:Model_Number) = True
      Else ! AccountActivate48Hour(wob:HeadAccountNumber) = True
          p_web.SSV('hide:48HourOption',1)
      End ! AccountActivate48Hour(wob:HeadAccountNumber) = True
  
  end ! if (p_web.GSV('FormEngineeringOption:FirstTime') = 1)
  p_web.SetValue('FormEngineeringOption_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('job:DOP')
    p_web.SetPicture('job:DOP',p_web.site.DatePicture)
  End
  p_web.SetSessionPicture('job:DOP',p_web.site.DatePicture)
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'locExchangeManufacturer'
    p_web.setsessionvalue('showtab_FormEngineeringOption',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MANUFACT)
      p_web.SSV('filter:ModelNumber','Upper(mod:Manufacturer) = Upper(<39>' & clip(man:Manufacturer) & '<39>)')
      
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locExchangeModelNumber')
  Of 'locExchangeModelNumber'
    p_web.setsessionvalue('showtab_FormEngineeringOption',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MODELNUM)
      do Validate:locExchangeModelNumber
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locExchangeNotes')
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locUserPassword',locUserPassword)
  p_web.SetSessionValue('locEngineeringOption',locEngineeringOption)
  p_web.SetSessionValue('locNewEngineeringOption',locNewEngineeringOption)
  p_web.SetSessionValue('job:DOP',job:DOP)
  p_web.SetSessionValue('locExchangeManufacturer',locExchangeManufacturer)
  p_web.SetSessionValue('locExchangeModelNumber',locExchangeModelNumber)
  p_web.SetSessionValue('locExchangeNotes',locExchangeNotes)
  p_web.SetSessionValue('locSplitJob',locSplitJob)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locUserPassword')
    locUserPassword = p_web.GetValue('locUserPassword')
    p_web.SetSessionValue('locUserPassword',locUserPassword)
  End
  if p_web.IfExistsValue('locEngineeringOption')
    locEngineeringOption = p_web.GetValue('locEngineeringOption')
    p_web.SetSessionValue('locEngineeringOption',locEngineeringOption)
  End
  if p_web.IfExistsValue('locNewEngineeringOption')
    locNewEngineeringOption = p_web.GetValue('locNewEngineeringOption')
    p_web.SetSessionValue('locNewEngineeringOption',locNewEngineeringOption)
  End
  if p_web.IfExistsValue('job:DOP')
    job:DOP = p_web.dformat(clip(p_web.GetValue('job:DOP')),p_web.site.DatePicture)
    p_web.SetSessionValue('job:DOP',job:DOP)
  End
  if p_web.IfExistsValue('locExchangeManufacturer')
    locExchangeManufacturer = p_web.GetValue('locExchangeManufacturer')
    p_web.SetSessionValue('locExchangeManufacturer',locExchangeManufacturer)
  End
  if p_web.IfExistsValue('locExchangeModelNumber')
    locExchangeModelNumber = p_web.GetValue('locExchangeModelNumber')
    p_web.SetSessionValue('locExchangeModelNumber',locExchangeModelNumber)
  End
  if p_web.IfExistsValue('locExchangeNotes')
    locExchangeNotes = p_web.GetValue('locExchangeNotes')
    p_web.SetSessionValue('locExchangeNotes',locExchangeNotes)
  End
  if p_web.IfExistsValue('locSplitJob')
    locSplitJob = p_web.GetValue('locSplitJob')
    p_web.SetSessionValue('locSplitJob',locSplitJob)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormEngineeringOption_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locUserPassword = p_web.RestoreValue('locUserPassword')
 locEngineeringOption = p_web.RestoreValue('locEngineeringOption')
 locNewEngineeringOption = p_web.RestoreValue('locNewEngineeringOption')
 locExchangeManufacturer = p_web.RestoreValue('locExchangeManufacturer')
 locExchangeModelNumber = p_web.RestoreValue('locExchangeModelNumber')
 locExchangeNotes = p_web.RestoreValue('locExchangeNotes')
 locSplitJob = p_web.RestoreValue('locSplitJob')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormEngineeringOption_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormEngineeringOption_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormEngineeringOption_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormEngineeringOption" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormEngineeringOption" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormEngineeringOption" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Change Engineering Option') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Change Engineering Option',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormEngineeringOption">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormEngineeringOption" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormEngineeringOption')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Confirm User') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Engineering Option') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormEngineeringOption')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormEngineeringOption'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='MANUFACT'
          If Not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
            p_web.SetValue('SelectField',clip(loc:formname) & '.locExchangeModelNumber')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='MODELNUM'
          If Not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
            p_web.SetValue('SelectField',clip(loc:formname) & '.locExchangeNotes')
          End
    End
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locUserPassword')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormEngineeringOption')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Confirm User') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormEngineeringOption_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm User')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm User')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm User')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm User')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locUserPassword
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locUserPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locUserPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Engineering Option') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormEngineeringOption_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Engineering Option')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Engineering Option')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Engineering Option')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Engineering Option')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::locErrorMessage
      do Comment::locErrorMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locEngineeringOption
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locEngineeringOption
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locEngineeringOption
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locNewEngineeringOption
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locNewEngineeringOption
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locNewEngineeringOption
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormEngineeringOption_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::text:ExchangeOrder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::text:ExchangeOrder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:DOP
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:DOP
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:DOP
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locExchangeManufacturer
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locExchangeManufacturer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locExchangeManufacturer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locExchangeModelNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locExchangeModelNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locExchangeModelNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locExchangeNotes
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locExchangeNotes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locExchangeNotes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locWarningMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locWarningMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locSplitJob
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locSplitJob
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locSplitJob
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::button:CreateOrder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::button:CreateOrder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::button:PrintOrder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::button:PrintOrder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locUserPassword  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locUserPassword') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Enter Password')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locUserPassword  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locUserPassword',p_web.GetValue('NewValue'))
    locUserPassword = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locUserPassword
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locUserPassword',p_web.GetValue('Value'))
    locUserPassword = p_web.GetValue('Value')
  End
      p_web.SSV('locPasswordValidated',0)
      p_web.SSV('Comment:UserPassword','User Does Not Have Access')
  
      Access:USERS.Clearkey(use:Password_Key)
      use:Password    = p_web.GSV('locUserPassword')
      if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
          ! Found
          Access:ACCAREAS.Clearkey(acc:Access_Level_Key)
          acc:User_Level  = use:User_Level
          acc:Access_Area = 'AMEND ENGINEERING OPTION'
          If Access:ACCAREAS.Tryfetch(acc:Access_Level_Key) = Level:Benign
              !Found
              p_web.SSV('locPasswordValidated',1)
              p_web.SSV('Comment:UserPassword','')
              p_web.SSV('headingExchangeOrder','Exchange Order')
          Else ! If Access:ACCAREAS.Tryfetch(Access_Level_Key) = Level:Benign
              !Error
          End !If Access:ACCAREAS.Tryfetch(Access_Level_Key) = Level:Benign
      else ! if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
          ! Error
      end ! if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
  do Value::locUserPassword
  do SendAlert
  do Prompt::locNewEngineeringOption
  do Value::locNewEngineeringOption  !1
  do Comment::locNewEngineeringOption
  do Comment::locUserPassword
  do Prompt::locEngineeringOption
  do Value::locEngineeringOption  !1
  do Comment::locEngineeringOption

Value::locUserPassword  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locUserPassword') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locUserPassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locUserPassword')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locUserPassword'',''formengineeringoption_locuserpassword_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locUserPassword')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','locUserPassword',p_web.GetSessionValueFormat('locUserPassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locUserPassword') & '_value')

Comment::locUserPassword  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:UserPassword'))
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locUserPassword') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locUserPassword') & '_comment')

Validate::locErrorMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locErrorMessage',p_web.GetValue('NewValue'))
    do Value::locErrorMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::locErrorMessage  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locErrorMessage') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBoldLarge')&'">' & p_web.Translate(p_web.GSV('locErrorMessage'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locErrorMessage') & '_value')

Comment::locErrorMessage  Routine
    loc:comment = ''
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locErrorMessage') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locEngineeringOption  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locEngineeringOption') & '_prompt',Choose(p_web.GSV('locPasswordValidated') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Current Option')
  If p_web.GSV('locPasswordValidated') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locEngineeringOption') & '_prompt')

Validate::locEngineeringOption  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locEngineeringOption',p_web.GetValue('NewValue'))
    locEngineeringOption = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locEngineeringOption
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locEngineeringOption',p_web.GetValue('Value'))
    locEngineeringOption = p_web.GetValue('Value')
  End

Value::locEngineeringOption  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locEngineeringOption') & '_value',Choose(p_web.GSV('locPasswordValidated') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locPasswordValidated') = 0)
  ! --- DISPLAY --- locEngineeringOption
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locEngineeringOption'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locEngineeringOption') & '_value')

Comment::locEngineeringOption  Routine
    loc:comment = ''
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locEngineeringOption') & '_comment',Choose(p_web.GSV('locPasswordValidated') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locPasswordValidated') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locEngineeringOption') & '_comment')

Prompt::locNewEngineeringOption  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locNewEngineeringOption') & '_prompt',Choose(p_web.GSV('locPasswordValidated') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('New Engineering Option')
  If p_web.GSV('locPasswordValidated') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locNewEngineeringOption') & '_prompt')

Validate::locNewEngineeringOption  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locNewEngineeringOption',p_web.GetValue('NewValue'))
    locNewEngineeringOption = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locNewEngineeringOption
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locNewEngineeringOption',p_web.GetValue('Value'))
    locNewEngineeringOption = p_web.GetValue('Value')
  End
  If locNewEngineeringOption = ''
    loc:Invalid = 'locNewEngineeringOption'
    loc:alert = p_web.translate('New Engineering Option') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do validate:locEngineeringOption
  p_web.SSV('locExchangeNotes',BHStripReplace(p_web.GSV('locExchangeNotes'),'<9>',''))
  p_web.SSV('locExchangeNotes',BHStripReplace(p_web.GSV('locExchangeNotes'),'<</DIV>',''))
  
  
  do Value::locNewEngineeringOption
  do SendAlert
  do Value::locErrorMessage  !1
  do Prompt::job:DOP
  do Value::job:DOP  !1
  do Comment::job:DOP
  do Prompt::locExchangeManufacturer
  do Value::locExchangeManufacturer  !1
  do Comment::locExchangeManufacturer
  do Prompt::locExchangeModelNumber
  do Value::locExchangeModelNumber  !1
  do Comment::locExchangeModelNumber
  do Prompt::locExchangeNotes
  do Value::locExchangeNotes  !1
  do Comment::locExchangeNotes
  do Value::text:ExchangeOrder  !1
  do Value::button:CreateOrder  !1
  do Prompt::locSplitJob
  do Value::locSplitJob  !1

Value::locNewEngineeringOption  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locNewEngineeringOption') & '_value',Choose(p_web.GSV('locPasswordValidated') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locPasswordValidated') = 0)
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locNewEngineeringOption')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locNewEngineeringOption = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locNewEngineeringOption'',''formengineeringoption_locnewengineeringoption_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locNewEngineeringOption')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('exchangeOrderCreated') = 1,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('locNewEngineeringOption',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locNewEngineeringOption') = 0
    p_web.SetSessionValue('locNewEngineeringOption','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('locNewEngineeringOption')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('locEngineeringOption') <> 'Standard Repair'
    packet = clip(packet) & p_web.CreateOption('Standard Repair','Standard Repair',choose('Standard Repair' = p_web.getsessionvalue('locNewEngineeringOption')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('locEngineeringOption') <> '48 Hour Exchange'  and p_web.GSV('hide:48HourOption') <> 1
    packet = clip(packet) & p_web.CreateOption('48 Hour Exchange','48 Hour Exchange',choose('48 Hour Exchange' = p_web.getsessionvalue('locNewEngineeringOption')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('locEngineeringOption') <> 'ARC Repair'
    packet = clip(packet) & p_web.CreateOption('ARC Repair','ARC Repair',choose('ARC Repair' = p_web.getsessionvalue('locNewEngineeringOption')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('locEngineeringOption') <> '7 Day TAT'
    packet = clip(packet) & p_web.CreateOption('7 Day TAT','7 Day TAT',choose('7 Day TAT' = p_web.getsessionvalue('locNewEngineeringOption')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locNewEngineeringOption') & '_value')

Comment::locNewEngineeringOption  Routine
    loc:comment = ''
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locNewEngineeringOption') & '_comment',Choose(p_web.GSV('locPasswordValidated') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locPasswordValidated') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locNewEngineeringOption') & '_comment')

Validate::text:ExchangeOrder  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('text:ExchangeOrder',p_web.GetValue('NewValue'))
    do Value::text:ExchangeOrder
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::text:ExchangeOrder  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('text:ExchangeOrder') & '_value',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('BlueBold')&'">' & p_web.Translate('Exchange Order',) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('text:ExchangeOrder') & '_value')

Comment::text:ExchangeOrder  Routine
    loc:comment = ''
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('text:ExchangeOrder') & '_comment',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:DOP  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('job:DOP') & '_prompt',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Date Of Purchase')
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('job:DOP') & '_prompt')

Validate::job:DOP  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:DOP',p_web.GetValue('NewValue'))
    job:DOP = p_web.GetValue('NewValue') !FieldType= DATE Field = job:DOP
    do Value::job:DOP
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:DOP',p_web.dformat(clip(p_web.GetValue('Value')),p_web.site.DatePicture))
    job:DOP = p_web.dformat(clip(p_web.GetValue('Value')),p_web.site.DatePicture)
  End
  do Value::job:DOP
  do SendAlert

Value::job:DOP  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('job:DOP') & '_value',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
  ! --- DATE --- job:DOP
    loc:AutoComplete = 'autocomplete="off"'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('job:DOP')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:DOP'',''formengineeringoption_job:dop_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:lookuponly = ''
  packet = clip(packet) & p_web.CreateInput('text','job:DOP',p_web.GetSessionValue('job:DOP'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.site.datepicture,loc:javascript,,) & '<13,10>'
  if not loc:readonly
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' '&p_web._nocolon('sv(''job:DOP'',''formengineeringoption_job:dop_value'',1,FieldValue(this,1))')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:DateLookupButton,loc:formname,,,|
    'sd('''&clip(loc:formName)&''','''&p_web._nocolon('job:DOP')&''','''&clip(upper(p_web.site.datepicture))&''','&lower(p_web._nocolon('''FormEngineeringOption_job:DOP_value'''))&');','onfocus="rad();" ')

  End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('job:DOP') & '_value')

Comment::job:DOP  Routine
    loc:comment = p_web._DateFormat(p_web.site.DatePicture)
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('job:DOP') & '_comment',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('job:DOP') & '_comment')

Prompt::locExchangeManufacturer  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeManufacturer') & '_prompt',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Manufacturer')
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locExchangeManufacturer') & '_prompt')

Validate::locExchangeManufacturer  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locExchangeManufacturer',p_web.GetValue('NewValue'))
    locExchangeManufacturer = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locExchangeManufacturer
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locExchangeManufacturer',p_web.GetValue('Value'))
    locExchangeManufacturer = p_web.GetValue('Value')
  End
  If locExchangeManufacturer = ''
    loc:Invalid = 'locExchangeManufacturer'
    loc:alert = p_web.translate('Manufacturer') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  p_Web.SetValue('lookupfield','locExchangeManufacturer')
  do AfterLookup
  do Value::locExchangeManufacturer
  do SendAlert
  do Comment::locExchangeManufacturer

Value::locExchangeManufacturer  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeManufacturer') & '_value',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
  ! --- STRING --- locExchangeManufacturer
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('exchangeOrderCreated') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('exchangeOrderCreated') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locExchangeManufacturer')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locExchangeManufacturer = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locExchangeManufacturer'',''formengineeringoption_locexchangemanufacturer_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locExchangeManufacturer')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','locExchangeManufacturer',p_web.GetSessionValueFormat('locExchangeManufacturer'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectManufacturers')&'?LookupField=locExchangeManufacturer&Tab=2&ForeignField=man:Manufacturer&_sort=man:Manufacturer&Refresh=sort&LookupFrom=FormEngineeringOption&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locExchangeManufacturer') & '_value')

Comment::locExchangeManufacturer  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeManufacturer') & '_comment',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locExchangeManufacturer') & '_comment')

Prompt::locExchangeModelNumber  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeModelNumber') & '_prompt',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Model Number')
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locExchangeModelNumber') & '_prompt')

Validate::locExchangeModelNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locExchangeModelNumber',p_web.GetValue('NewValue'))
    locExchangeModelNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locExchangeModelNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locExchangeModelNumber',p_web.GetValue('Value'))
    locExchangeModelNumber = p_web.GetValue('Value')
  End
  If locExchangeModelNumber = ''
    loc:Invalid = 'locExchangeModelNumber'
    loc:alert = p_web.translate('Model Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  p_Web.SetValue('lookupfield','locExchangeModelNumber')
  do AfterLookup
  do Value::locExchangeModelNumber
  do SendAlert
  do Comment::locExchangeModelNumber
  do Value::locWarningMessage  !1

Value::locExchangeModelNumber  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeModelNumber') & '_value',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
  ! --- STRING --- locExchangeModelNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('exchangeOrderCreated') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('exchangeOrderCreated') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locExchangeModelNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locExchangeModelNumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locExchangeModelNumber'',''formengineeringoption_locexchangemodelnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locExchangeModelNumber')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','locExchangeModelNumber',p_web.GetSessionValueFormat('locExchangeModelNumber'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectModelNumbers')&'?LookupField=locExchangeModelNumber&Tab=2&ForeignField=mod:Model_Number&_sort=mod:Model_Number&Refresh=sort&LookupFrom=FormEngineeringOption&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locExchangeModelNumber') & '_value')

Comment::locExchangeModelNumber  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeModelNumber') & '_comment',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locExchangeModelNumber') & '_comment')

Prompt::locExchangeNotes  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeNotes') & '_prompt',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Notes')
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locExchangeNotes') & '_prompt')

Validate::locExchangeNotes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locExchangeNotes',p_web.GetValue('NewValue'))
    locExchangeNotes = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locExchangeNotes
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locExchangeNotes',p_web.GetValue('Value'))
    locExchangeNotes = p_web.GetValue('Value')
  End
    locExchangeNotes = Upper(locExchangeNotes)
    p_web.SetSessionValue('locExchangeNotes',locExchangeNotes)
  p_web.SSV('locExchangeNotes',BHStripReplace(p_web.GSV('locExchangeNotes'),'<9>',''))
  do Value::locExchangeNotes
  do SendAlert

Value::locExchangeNotes  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeNotes') & '_value',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
  ! --- TEXT --- locExchangeNotes
  loc:fieldclass = Choose(sub('TextEntry',1,1) = ' ',clip('FormEntry') & 'TextEntry','TextEntry')
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('locExchangeNotes')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locExchangeNotes'',''formengineeringoption_locexchangenotes_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locExchangeNotes')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('locExchangeNotes',p_web.GetSessionValue('locExchangeNotes'),3,40,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(locExchangeNotes),,Net:Web:Control) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locExchangeNotes') & '_value')

Comment::locExchangeNotes  Routine
      loc:comment = ''
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeNotes') & '_comment',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locExchangeNotes') & '_comment')

Validate::locWarningMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locWarningMessage',p_web.GetValue('NewValue'))
    do Value::locWarningMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::locWarningMessage  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locWarningMessage') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(p_web.GSV('locWarningMessage'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locWarningMessage') & '_value')

Comment::locWarningMessage  Routine
    loc:comment = ''
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locWarningMessage') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locSplitJob  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locSplitJob') & '_prompt',Choose(p_web.GSV('Hide:SplitJob') = 1 OR p_web.GSV('exchangeOrderCreated') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Select Job Type')
  If p_web.GSV('Hide:SplitJob') = 1 OR p_web.GSV('exchangeOrderCreated') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locSplitJob') & '_prompt')

Validate::locSplitJob  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locSplitJob',p_web.GetValue('NewValue'))
    locSplitJob = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locSplitJob
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locSplitJob',p_web.GetValue('Value'))
    locSplitJob = p_web.GetValue('Value')
  End
  do Value::locSplitJob
  do SendAlert
  do Value::button:CreateOrder  !1

Value::locSplitJob  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locSplitJob') & '_value',Choose(p_web.GSV('Hide:SplitJob') = 1 OR p_web.GSV('exchangeOrderCreated') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:SplitJob') = 1 OR p_web.GSV('exchangeOrderCreated') = 1)
  ! --- RADIO --- locSplitJob
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locSplitJob')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locSplitJob') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locSplitJob'',''formengineeringoption_locsplitjob_value'',1,'''&clip(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locSplitJob')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locSplitJob',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locSplitJob_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Chargeable Only') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locSplitJob') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locSplitJob'',''formengineeringoption_locsplitjob_value'',1,'''&clip(2)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locSplitJob')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locSplitJob',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locSplitJob_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Warranty Only') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locSplitJob') = 3
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locSplitJob'',''formengineeringoption_locsplitjob_value'',1,'''&clip(3)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locSplitJob')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locSplitJob',clip(3),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locSplitJob_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Split Warranty/Chargeable') & '<13,10>'
    packet = clip(packet) & p_web.br
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('locSplitJob') & '_value')

Comment::locSplitJob  Routine
    loc:comment = ''
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('locSplitJob') & '_comment',Choose(p_web.GSV('Hide:SplitJob') = 1 OR p_web.GSV('exchangeOrderCreated') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:SplitJob') = 1 OR p_web.GSV('exchangeOrderCreated') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::button:CreateOrder  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('button:CreateOrder',p_web.GetValue('NewValue'))
    do Value::button:CreateOrder
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  if (p_web.GSV('locExchangeManufacturer') <> '' and p_web.GSV('locExchangeModelNumber') <> '')
      do create:ExchangeOrder
  end ! if (p_web.GSV('locExchangeManufacturer') <> '' and |
  do Value::button:CreateOrder
  do SendAlert
  do Value::button:PrintOrder  !1
  do Value::locExchangeManufacturer  !1
  do Value::locExchangeModelNumber  !1
  do Value::locExchangeNotes  !1
  do Value::locWarningMessage  !1
  do Prompt::locSplitJob
  do Value::locSplitJob  !1

Value::button:CreateOrder  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('button:CreateOrder') & '_value',Choose(p_web.GSV('exchangeOrderCreated') = 1 Or p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange' OR p_web.GSV('locSplitJob') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('exchangeOrderCreated') = 1 Or p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange' OR p_web.GSV('locSplitJob') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''button:CreateOrder'',''formengineeringoption_button:createorder_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','CreateExchangeOrder','Create Exch. Order','button-entryfield',loc:formname,,,,loc:javascript,0,'images\star.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('button:CreateOrder') & '_value')

Comment::button:CreateOrder  Routine
    loc:comment = ''
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('button:CreateOrder') & '_comment',Choose(p_web.GSV('exchangeOrderCreated') = 1 Or p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange' OR p_web.GSV('locSplitJob') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('exchangeOrderCreated') = 1 Or p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange' OR p_web.GSV('locSplitJob') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::button:PrintOrder  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('button:PrintOrder',p_web.GetValue('NewValue'))
    do Value::button:PrintOrder
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::button:PrintOrder  Routine
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('button:PrintOrder') & '_value',Choose(p_web.GSV('exchangeOrderCreated') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('exchangeOrderCreated') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PrintOrder','Print Order','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('ExchangeOrder')) & ''','''&clip('_blank')&''')',loc:javascript,0,'images\printer.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEngineeringOption_' & p_web._nocolon('button:PrintOrder') & '_value')

Comment::button:PrintOrder  Routine
    loc:comment = ''
  p_web._DivHeader('FormEngineeringOption_' & p_web._nocolon('button:PrintOrder') & '_comment',Choose(p_web.GSV('exchangeOrderCreated') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('exchangeOrderCreated') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormEngineeringOption_locUserPassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locUserPassword
      else
        do Value::locUserPassword
      end
  of lower('FormEngineeringOption_locNewEngineeringOption_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locNewEngineeringOption
      else
        do Value::locNewEngineeringOption
      end
  of lower('FormEngineeringOption_job:DOP_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:DOP
      else
        do Value::job:DOP
      end
  of lower('FormEngineeringOption_locExchangeManufacturer_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locExchangeManufacturer
      else
        do Value::locExchangeManufacturer
      end
  of lower('FormEngineeringOption_locExchangeModelNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locExchangeModelNumber
      else
        do Value::locExchangeModelNumber
      end
  of lower('FormEngineeringOption_locExchangeNotes_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locExchangeNotes
      else
        do Value::locExchangeNotes
      end
  of lower('FormEngineeringOption_locSplitJob_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locSplitJob
      else
        do Value::locSplitJob
      end
  of lower('FormEngineeringOption_button:CreateOrder_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::button:CreateOrder
      else
        do Value::button:CreateOrder
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormEngineeringOption_form:ready_',1)
  p_web.SetSessionValue('FormEngineeringOption_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormEngineeringOption',0)

PreCopy  Routine
  p_web.SetValue('FormEngineeringOption_form:ready_',1)
  p_web.SetSessionValue('FormEngineeringOption_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormEngineeringOption',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormEngineeringOption_form:ready_',1)
  p_web.SetSessionValue('FormEngineeringOption_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormEngineeringOption:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormEngineeringOption_form:ready_',1)
  p_web.SetSessionValue('FormEngineeringOption_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormEngineeringOption:Primed',0)
  p_web.setsessionvalue('showtab_FormEngineeringOption',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord
      ! Validate Form
      if (p_web.GSV('locPasswordValidated') = 0)
          loc:Invalid = 'locUserPassword'
          loc:Alert = 'Your password has not yet been validated'
          p_web.SSV('locUserPassword','')
          exit
      end ! if (p_web.GSV('local:PasswordValidated') = 0)
  
      if (p_web.GSV('locValidationFailed') = 1)
          loc:Invalid = 'locUserPassword'
          loc:Alert = p_web.GSV('locErrorMessage')
          exit
      end ! if (p_web.GSV('locValidationFailed') = 1)
  
  
  
      if ((p_web.GSV('locEngineeringOption') <> p_web.GSV('locNewEngineeringOption')) and p_web.GSV('locNewEngineeringOption') <> '')
          p_web.SSV('locEngineeringOption',p_web.GSV('locNewEngineeringOption'))
  
          case p_web.GSV('locNewEngineeringOption')
          of 'ARC Repair'
              if (p_web.GSV('BookingSite') = 'RRC' and p_web.GSV('jobe:HubRepair') = 0)
                  p_web.SSV('jobe:HubRepair',1)
                  do set:HubRepair
              end ! if (p_web.GSV('BookingSite') = 'RRC' and p_web.GSV('jobe:HubRepair') = 0)
  !                  p_web.SSV('AddToAudit:Type','JOB')
  !                  p_web.SSV('AddToAudit:Action','ENGINEERING OPTION SELECTED: ARC REPAIR')
  !                  p_web.SSV('AddToAudit:Notes','')
                  AddToAudit(p_web,p_web.GSV('jobe:RefNumber'),'JOB','ENGINEERING OPTION SELECTED: ARC REPAIR','')
  
                  p_web.SSV('jobe:Engineer48HourOption',2)
  
              
          of '7 Day TAT'
              if (p_web.GSV('BookingSite') = 'RRC' and p_web.GSV('jobe:HubRepair') = 0)
                  p_web.SSV('jobe:HubRepair',1)
                  do set:HubRepair
              end ! if (p_web.GSV('BookingSite') = 'RRC' and p_web.GSV('jobe:HubRepair') = 0)
  !                  p_web.SSV('AddToAudit:Type','JOB')
  !                  p_web.SSV('AddToAudit:Action','ENGINEERING OPTION SELECTED: 7 DAY TAT')
  !                  p_web.SSV('AddToAudit:Notes','')
                  AddToAudit(p_web,p_web.GSV('jobe:RefNumber'),'JOB','ENGINEERING OPTION SELECTED: 7 DAY TAT','')
  
                  p_web.SSV('jobe:Engineer48HourOption',3)
              
          of 'Standard Repair'
  !              p_web.SSV('AddToAudit:Type','JOB')
  !              p_web.SSV('AddToAudit:Action','ENGINEERING OPTION SELECTED: STANDARD REPAIR')
  !              p_web.SSV('AddToAudit:Notes','')
              AddToAudit(p_web,p_web.GSV('jobe:RefNumber'),'JOB','ENGINEERING OPTION SELECTED: STANDARD REPAIR','')
              p_web.SSV('jobe:Engineer48HourOption',4)
          end !case p_web.GSV('locNewEngineeringOption')
  
          Access:JOBSE.Clearkey(jobe:RefNumberKey)
          jobe:RefNumber    = p_web.GSV('wob:RefNumber')
          if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
              p_web.SessionQueueToFile(JOBSE)
              Access:JOBSE.tryUpdate()
          else ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
              ! Error
          end ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
  
          Access:JOBS.Clearkey(job:Ref_Number_Key)
          job:Ref_Number    = p_web.GSV('wob:RefNumber')
          if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
              ! Found
              p_web.SessionQueueToFile(JOBS)
              Access:JOBS.tryUpdate()
          else ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
              ! Error
          end ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
          
          Access:WEBJOB.Clearkey(wob:RefNumberKey)
          wob:RefNumber = p_web.GSV('wob:RefNumber')
          if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
              ! Found
              p_web.SessionQueueToFile(WEBJOB)
              Access:WEBJOB.TryUpdate()
          end ! if (Access:WEBJOB.TryFetch(wob:Ref_Number_Key) = Level:Benign)
          
  
          p_web.SSV('FirstTime',1)
  
  !
  !        p_web.SSV('locMessage','Your changes have been saved')
  !        p_web.SSV('loc:Alert','Your changes have been saved')
  
      end ! if (p_web.GSV('locEngineeringOption') <> p_web.GSV('locNewEngineeringOption'))
  
  
  
      

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormEngineeringOption_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormEngineeringOption_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
  ! tab = 1
    loc:InvalidTab += 1
      If not (p_web.GSV('locPasswordValidated') = 0)
        If locNewEngineeringOption = ''
          loc:Invalid = 'locNewEngineeringOption'
          loc:alert = p_web.translate('New Engineering Option') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
  ! tab = 3
    loc:InvalidTab += 1
      If not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
        If locExchangeManufacturer = ''
          loc:Invalid = 'locExchangeManufacturer'
          loc:alert = p_web.translate('Manufacturer') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
        If locExchangeModelNumber = ''
          loc:Invalid = 'locExchangeModelNumber'
          loc:alert = p_web.translate('Model Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
          locExchangeNotes = Upper(locExchangeNotes)
          p_web.SetSessionValue('locExchangeNotes',locExchangeNotes)
        If loc:Invalid <> '' then exit.
      End
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormEngineeringOption:Primed',0)
  p_web.StoreValue('locUserPassword')
  p_web.StoreValue('')
  p_web.StoreValue('locEngineeringOption')
  p_web.StoreValue('locNewEngineeringOption')
  p_web.StoreValue('')
  p_web.StoreValue('job:DOP')
  p_web.StoreValue('locExchangeManufacturer')
  p_web.StoreValue('locExchangeModelNumber')
  p_web.StoreValue('locExchangeNotes')
  p_web.StoreValue('')
  p_web.StoreValue('locSplitJob')
  p_web.StoreValue('')
  p_web.StoreValue('')
