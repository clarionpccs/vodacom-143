

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER308.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER021.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER307.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER309.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER310.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER311.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER312.INC'),ONCE        !Req'd for module callout resolution
                     END


UpdateExchangeStatus PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
SendingResult        STRING(255)                           !
NewJobNumber         STRING(20)                            !
NewStatus            STRING(30)                            !
NewChargeType        STRING(30)                            !
NewWarrantyType      STRING(30)                            !
ChangeChargeType     STRING('''')                          !
ResubmitFlag         STRING(1)                             !
FilesOpened     Long
WEBJOB::State  USHORT
USERS::State  USHORT
AUDIT::State  USHORT
JOBS::State  USHORT
TagFile::State  USHORT
CHARTYPE::State  USHORT
STATUS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  GlobalErrors.SetProcedureName('UpdateExchangeStatus')
  loc:formname = 'UpdateExchangeStatus_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('UpdateExchangeStatus','')
    p_web._DivHeader('UpdateExchangeStatus',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferUpdateExchangeStatus',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferUpdateExchangeStatus',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferUpdateExchangeStatus',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_UpdateExchangeStatus',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferUpdateExchangeStatus',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_UpdateExchangeStatus',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
ExchangeStatus      ROUTINE
    
    Do ExchangeStatusDetails    !so I can use "exit"

    !show the result
    p_web.Message('Alert',clip(SendingResult),p_web._MessageClass,Net:Send)
    !Objections: "Do not show the action string if sucessful, it wastes the users time clicking [OK]"
    !reason for keeping message
    !if you don't show the message you lose the focus and the user has to click back in the "newjobref" field
    !Harvey and J agree that having the message and having to touch [enter] is easier than using mouse to get focus back
    !so the message stays!!    

    
    !Set the CompletedJobs in tagfile to show in list
    if SendingResult[1:5] = 'ERROR' THEN
        !dont update the completed jobs list ...

    ELSE 
        !don't show the message - just update the list of completed
        Access:Tagfile.PrimeRecord()
        tag:sessionID = p_web.SessionID
        tag:taggedValue = clip(p_web.gsv('NewJobNumber'))
        tag:tagged = 1
        access:Tagfile.insert()
        
    END !if sendingresult was an error
    
    !a few resets
    if ResubmitFlag <> 'Y' THEN
        p_web.ssv('NewJobNumber','')
        NewJobNumber = ''
    END !if not resubmitting the same data



ExchangeStatusDetails      ROUTINE

    if clip(p_web.gsv('NewJobNumber')) = '' THEN
        SendingResult = 'ERROR - You must give a job reference number'
        EXIT    
    END !if job ref blank
    !Sucess
    SendingResult = 'Job Number: '&clip(p_web.gsv('NewJobNumber'))& ' updated successfully'

    if not NUMERIC(p_web.gsv('NewJobNumber')) THEN
        SendingResult = 'ERROR - The job reference number '&clip(p_web.gsv('NewJobNumber'))&' must contain only numbers'
        EXIT    
    END !if job ref contains non numberic characters
    !No change to successful "sending result"

    if INSTRING('.',p_web.gsv('NewJobNumber'),1,1) THEN
        SendingResult = 'ERROR - The job reference number '&clip(p_web.gsv('NewJobNumber'))&' must not contain decimals'
        EXIT    
    END !if job ref contains '.'
    !No change to successful "sending result"  
    
    if clip(p_web.gsv('NewStatus')) = '' THEN
        SendingResult = 'ERROR - You must provide a new status for the exchange'
        EXIT    
    END !if new status exists
    !SendingResult = clip(SendingResult)&' Change Exchange Status to '&clip(p_web.gsv('NewStatus')) 

    access:jobs.clearkey(job:ref_number_key)
    job:ref_number = p_web.gsv('NewJobNumber')
    if access:jobs.fetch(job:ref_number_key) 
        SendingResult = 'ERROR - Unable to find job '&clip(p_web.gsv('NewJobNumber'))&'. Please check Job Number and try again'
        EXIT    
    END !if job ref contains '.'

    !will need webjob as well
    Access:WEBJOB.ClearKey(wob:RefNumberKey)
    wob:RefNumber = job:Ref_Number
    if access:webjob.fetch(wob:RefNumberKey)
        SendingResult = 'ERROR - Unable to find Webjob record '&clip(p_web.gsv('NewJobNumber'))&'. Please check Job Number and try again'
        EXIT    
    END !
    
    IF job:Exchange_Unit_Number = 0
        SendingResult = 'ERROR - There is no exchange unit attached to this job.'
        EXIT        
    ELSE
        !this is not appliable in this place so removed it all
!        if ResubmitFlag = 'Y' THEN
!            !ignore this - it was checked last time
!            ResubmitFlag = ''
!        ELSE
!            ResubmitFlag = 'Y'
!            SendingResult = 'ERROR - An exchange unit has been allocated to this job '&clip(p_web.gsv('NewJobNumber'))&'. If you wish to continue to change the status, click [SUBMIT]'
!            EXIT
!        END !if being resubmitted            
    END !if exchange unit number not zero
    
    !has it already been changed in this session
    Access:TagFile.clearkey(tag:keyID)
    tag:taggedValue = p_web.gsv('NewJobNumber')
    tag:sessionID   = p_web.SessionID
    if Access:TagFile.fetch(tag:keyID) = Level:Benign THEN
        SendingResult = 'ERROR - This job has already been processed '&clip(p_web.gsv('NewJobNumber'))&'. An Exchange Status has already been allocated during this session.'
        EXIT
    END 
    
    !account number is 'AA20' for this to be the ARC
    !tra:Account_Number <> p_web.GSV('loc:BranchID')
    if p_web.gsv('Loc:BranchID') = 'AA20' THEN
        !If at the ARC, should only be able to allocate if the job is AT the ARC - 3474 (DBH: 06-01-2004)
        If jobe:HubRepair <> True
            SendingResult = 'ERROR - The selected job '&clip(p_web.gsv('NewJobNumber'))&' is not at the ARC.'
            EXIT
        End !If jobe:HubRepair <> True

        !Check the job is at the ARC Location - L921 (DBH: 13-08-2003)
        Access:LOCATLOG.ClearKey(lot:NewLocationKey)
        lot:RefNumber   = job:Ref_Number
        lot:NewLocation = Clip(GETINI('RRC','ARCLocation',,CLIP(PATH())&'\SB2KDEF.INI'))
        If Access:LOCATLOG.TryFetch(lot:NewLocationKey) 
            !Error
            SendingResult =  'ERROR - This job '&clip(p_web.gsv('NewJobNumber'))&' is not at the ARC Location. Please ensure that the Waybill Confirmation procedure has been completed.'
            EXIT
        End !If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign       
        
    ELSE
        !this is at rrc
        !If at the RRC, should only be able to allocate a job from the SAME RRC  
        If wob:HeadAccountNumber <> p_web.gsv('Loc:BranchID')
            SendingResult = 'ERROR - The selected job'&clip(p_web.gsv('NewJobNumber'))&'  was booked by a different RRC.'
            EXIT            
        End !If wob:HeadAccountNumber <> ClarioNET:Global.Param2

        If jobe:HubRepair
            SendingResult = 'ERROR - Cannot update. The selected job '&clip(p_web.gsv('NewJobNumber'))&' is not at the RRC.'
            EXIT            
        End !If jobe:HubRepair
    End
    
    
    !before you make the status change check if you can do the charge changes
    if p_web.gsv('ChangeChargeType') = 'Y' THEN
        if clip( p_web.gsv('NewChargeType') & p_web.gsv('NewWarrantyType') ) = '' THEN
            SendingResult = 'ERROR - a change in the charge type has been requested but there are no new types to apply.'
            EXIT
        END !if nothing was set up
        
        if p_web.gsv('NewChargeType') <> '' and p_web.gsv('NewChargeType') <> job:Charge_Type
            !Charge type needs changing
            If CheckPricing('C') = true THEN
                SendingResult = 'ERROR - '&clip(glo:ErrorText)  
                EXIT
            END            
            !SendingResult = clip(SendingResult)&' Charge type changed to '&p_web.gsv('NewChargeType')
        END !if chargeable type not blank and now same as already in place
        
        if p_web.gsv('NewWarrantyType') <> '' and p_web.gsv('NewWarrantyType') <> job:Warranty_Charge_Type
            !warranty type needs changing
            If CheckPricing('W') = true THEN
                SendingResult = 'ERROR - '&clip(glo:ErrorText)  
                EXIT
            END   
            !SendingResult = clip(SendingResult)&' Warranty charge type changed to '&p_web.gsv('NewWarrantyType')
        END !if new warranty type not blank and not same as already in place        
    END
        
    !make the status change - first prep the audit trail
    AddToAudit(p_web,job:Ref_Number,'EXC','RAPID EXCHANGE STATUS UPDATE: ' &  Clip(p_web.gsv('NewStatus')),'PREVIOUS STATUS: ' & Clip(job:exchange_status) & '<13,10>NEW STATUS: ' & Clip(p_web.gsv('NewStatus')))
!    if access:audit.primerecord() = level:benign
!        aud:notes         = 'PREVIOUS STATUS: ' & Clip(job:exchange_status) & '<13,10>NEW STATUS: ' & Clip(p_web.gsv('NewStatus'))
!        aud:ref_number    = job:ref_number
!        aud:date          = today()
!        aud:time          = clock()
!        
!        !find the user code
!        access:users.clearkey(use:password_key)
!        use:password = glo:password
!        access:users.fetch(use:password_key)
!        
!        aud:user = use:user_code
!        aud:Type    = 'EXC'
!        aud:action  = 'RAPID EXCHANGE STATUS UPDATE: ' &  Clip(p_web.gsv('NewStatus'))
!        access:audit.insert()
!    end  !if access:audit.primerecord() = level:benign
    
    GetStatus(Sub(p_web.gsv('NewStatus'),1,3),1,'EXC')
    
    !charget types to change - were checked above so these are ready to go
    if p_web.gsv('NewChargeType') <> '' and p_web.gsv('NewChargeType') <> job:Charge_Type
        !Charge type needs changing
        job:Charge_Type = p_web.gsv('NewChargeType')
    END !if chargeable type not blank and now same as already in place
    
    if p_web.gsv('NewWarrantyType') <> '' and p_web.gsv('NewWarrantyType') <> job:Warranty_Charge_Type
        !warranty type needs changing
        job:Warranty_Charge_Type = p_web.gsv('NewWarrantyType')        
    END !if new warranty type not blank and not same as already in place        
    
    Access:jobs.update()

    EXIT    !final exit from the routine    
OpenFiles  ROUTINE
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(USERS)
  p_web._OpenFile(AUDIT)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(TagFile)
  p_web._OpenFile(CHARTYPE)
  p_web._OpenFile(STATUS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(AUDIT)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(TagFile)
  p_Web._CloseFile(CHARTYPE)
  p_Web._CloseFile(STATUS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('UpdateExchangeStatus_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'NewStatus'
    p_web.setsessionvalue('showtab_UpdateExchangeStatus',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(STATUS)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.ChangeChargeType')
  Of 'NewChargeType'
    p_web.setsessionvalue('showtab_UpdateExchangeStatus',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(CHARTYPE)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.NewWarrantyType')
  Of 'NewWarrantyType'
    p_web.setsessionvalue('showtab_UpdateExchangeStatus',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(CHARTYPE)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.NewJobNumber')
  End
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('NewStatus',NewStatus)
  p_web.SetSessionValue('ChangeChargeType',ChangeChargeType)
  p_web.SetSessionValue('NewChargeType',NewChargeType)
  p_web.SetSessionValue('NewWarrantyType',NewWarrantyType)
  p_web.SetSessionValue('NewJobNumber',NewJobNumber)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('NewStatus')
    NewStatus = p_web.GetValue('NewStatus')
    p_web.SetSessionValue('NewStatus',NewStatus)
  End
  if p_web.IfExistsValue('ChangeChargeType')
    ChangeChargeType = p_web.GetValue('ChangeChargeType')
    p_web.SetSessionValue('ChangeChargeType',ChangeChargeType)
  End
  if p_web.IfExistsValue('NewChargeType')
    NewChargeType = p_web.GetValue('NewChargeType')
    p_web.SetSessionValue('NewChargeType',NewChargeType)
  End
  if p_web.IfExistsValue('NewWarrantyType')
    NewWarrantyType = p_web.GetValue('NewWarrantyType')
    p_web.SetSessionValue('NewWarrantyType',NewWarrantyType)
  End
  if p_web.IfExistsValue('NewJobNumber')
    NewJobNumber = p_web.GetValue('NewJobNumber')
    p_web.SetSessionValue('NewJobNumber',NewJobNumber)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('UpdateExchangeStatus_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  !following defaults are only set up if this is the first time it has been called
  !check the first call variable (set true at the previous window)
  if p_web.gsv('AllocationFirstCall') = TRUE THEN
      
      !reset the first call variable so this does not get used again
      p_web.ssv('AllocationFirstCall',False)
  
      ChangeChargeType = 'N'
      P_Web.ssv('ChangeChargeType',ChangeChargeType)
  
      !To be absolutely sure
      NewJobNumber = ''
      NewStatus = ''
      NewChargeType = ''
      NewWarrantyType = ''
  
      p_web.ssv('NewJobNumber',NewJobNumber)
      p_web.ssv('NewStatus',NewStatus)
      p_web.ssv('NewChargeType',NewChargeType)
      p_web.ssv('NewWarrantyType',NewWarrantyType)    
      
  END !if this is the first call to the window
  
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 NewStatus = p_web.RestoreValue('NewStatus')
 ChangeChargeType = p_web.RestoreValue('ChangeChargeType')
 NewChargeType = p_web.RestoreValue('NewChargeType')
 NewWarrantyType = p_web.RestoreValue('NewWarrantyType')
 NewJobNumber = p_web.RestoreValue('NewJobNumber')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'IndexPage'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('UpdateExchangeStatus_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('UpdateExchangeStatus_ChainTo')
    loc:formaction = p_web.GetSessionValue('UpdateExchangeStatus_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'IndexPage'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="UpdateExchangeStatus" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="UpdateExchangeStatus" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="UpdateExchangeStatus" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Rapid Exchange Status Update') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Rapid Exchange Status Update',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_UpdateExchangeStatus">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_UpdateExchangeStatus" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_UpdateExchangeStatus')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Exchange Update') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Jobs Updated') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_UpdateExchangeStatus')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_UpdateExchangeStatus'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('UpdateExchangeStatus_BrowseTagFile_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='STATUS'
          If Not (p_web.gsv('ChangeChargeType') = 'N')
            p_web.SetValue('SelectField',clip(loc:formname) & '.NewChargeType')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='CHARTYPE'
          If Not (p_web.gsv('ChangeChargeType') = 'N')
            p_web.SetValue('SelectField',clip(loc:formname) & '.NewWarrantyType')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='CHARTYPE'
            p_web.SetValue('SelectField',clip(loc:formname) & '.NewJobNumber')
    End
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.NewStatus')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_UpdateExchangeStatus')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Exchange Update') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_UpdateExchangeStatus_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Exchange Update')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Exchange Update')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Exchange Update')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Exchange Update')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::NewStatus
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::NewStatus
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::NewStatus
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::ChangeChargeType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::ChangeChargeType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::ChangeChargeType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::NewChargeType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::NewChargeType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::NewChargeType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::NewWarrantyType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::NewWarrantyType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::NewWarrantyType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::NewJobNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::NewJobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::NewJobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::ButtonCommit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::ButtonCommit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Jobs Updated') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_UpdateExchangeStatus_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Jobs Updated')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Jobs Updated')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Jobs Updated')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Jobs Updated')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::BrowseTagFile
      do Comment::BrowseTagFile
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_UpdateExchangeStatus_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::ButtonFinish
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::ButtonFinish
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::NewStatus  Routine
  p_web._DivHeader('UpdateExchangeStatus_' & p_web._nocolon('NewStatus') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('New Status')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::NewStatus  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('NewStatus',p_web.GetValue('NewValue'))
    NewStatus = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::NewStatus
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('NewStatus',p_web.GetValue('Value'))
    NewStatus = p_web.GetValue('Value')
  End
      p_web.ssv('NewStatus',NewStatus)
  p_Web.SetValue('lookupfield','NewStatus')
  do AfterLookup
  do Value::NewStatus
  do SendAlert
  do Comment::NewStatus

Value::NewStatus  Routine
  p_web._DivHeader('UpdateExchangeStatus_' & p_web._nocolon('NewStatus') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- NewStatus
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('NewStatus')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''NewStatus'',''updateexchangestatus_newstatus_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('NewStatus')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','NewStatus',p_web.GetSessionValueFormat('NewStatus'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,'Select new status using the button') & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('UES_SelectStatus?LookupField=NewStatus&Tab=2&ForeignField=sts:Status&_sort=&Refresh=sort&LookupFrom=UpdateExchangeStatus&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('UpdateExchangeStatus_' & p_web._nocolon('NewStatus') & '_value')

Comment::NewStatus  Routine
      loc:comment = ''
  p_web._DivHeader('UpdateExchangeStatus_' & p_web._nocolon('NewStatus') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('UpdateExchangeStatus_' & p_web._nocolon('NewStatus') & '_comment')

Prompt::ChangeChargeType  Routine
  p_web._DivHeader('UpdateExchangeStatus_' & p_web._nocolon('ChangeChargeType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Change Charge Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::ChangeChargeType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('ChangeChargeType',p_web.GetValue('NewValue'))
    ChangeChargeType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::ChangeChargeType
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value','N')
    end
    p_web.SetSessionValue('ChangeChargeType',p_web.GetValue('Value'))
    ChangeChargeType = p_web.GetValue('Value')
  End
      p_web.ssv('ChangeChargeType',ChangeChargeType)
  do Value::ChangeChargeType
  do SendAlert
  do Prompt::NewChargeType
  do Value::NewChargeType  !1
  do Prompt::NewWarrantyType
  do Value::NewWarrantyType  !1

Value::ChangeChargeType  Routine
  p_web._DivHeader('UpdateExchangeStatus_' & p_web._nocolon('ChangeChargeType') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- ChangeChargeType
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''ChangeChargeType'',''updateexchangestatus_changechargetype_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('ChangeChargeType')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('ChangeChargeType') = 'Y'
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','ChangeChargeType',clip('Y'),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('UpdateExchangeStatus_' & p_web._nocolon('ChangeChargeType') & '_value')

Comment::ChangeChargeType  Routine
    loc:comment = ''
  p_web._DivHeader('UpdateExchangeStatus_' & p_web._nocolon('ChangeChargeType') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::NewChargeType  Routine
  p_web._DivHeader('UpdateExchangeStatus_' & p_web._nocolon('NewChargeType') & '_prompt',Choose(p_web.gsv('ChangeChargeType') = 'N','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('New Charge Type')
  If p_web.gsv('ChangeChargeType') = 'N'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('UpdateExchangeStatus_' & p_web._nocolon('NewChargeType') & '_prompt')

Validate::NewChargeType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('NewChargeType',p_web.GetValue('NewValue'))
    NewChargeType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::NewChargeType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('NewChargeType',p_web.GetValue('Value'))
    NewChargeType = p_web.GetValue('Value')
  End
  p_Web.SetValue('lookupfield','NewChargeType')
  do AfterLookup
  do Value::NewChargeType
  do SendAlert
  do Comment::NewChargeType

Value::NewChargeType  Routine
  p_web._DivHeader('UpdateExchangeStatus_' & p_web._nocolon('NewChargeType') & '_value',Choose(p_web.gsv('ChangeChargeType') = 'N','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.gsv('ChangeChargeType') = 'N')
  ! --- STRING --- NewChargeType
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('NewChargeType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''NewChargeType'',''updateexchangestatus_newchargetype_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('NewChargeType')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','NewChargeType',p_web.GetSessionValueFormat('NewChargeType'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('UES_SelectChargeType?LookupField=NewChargeType&Tab=2&ForeignField=cha:Charge_Type&_sort=&Refresh=sort&LookupFrom=UpdateExchangeStatus&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('UpdateExchangeStatus_' & p_web._nocolon('NewChargeType') & '_value')

Comment::NewChargeType  Routine
      loc:comment = ''
  p_web._DivHeader('UpdateExchangeStatus_' & p_web._nocolon('NewChargeType') & '_comment',Choose(p_web.gsv('ChangeChargeType') = 'N','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.gsv('ChangeChargeType') = 'N'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('UpdateExchangeStatus_' & p_web._nocolon('NewChargeType') & '_comment')

Prompt::NewWarrantyType  Routine
  p_web._DivHeader('UpdateExchangeStatus_' & p_web._nocolon('NewWarrantyType') & '_prompt',Choose(p_web.gsv('ChangeChargeType') = 'N','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('New Warranty Charge Type')
  If p_web.gsv('ChangeChargeType') = 'N'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('UpdateExchangeStatus_' & p_web._nocolon('NewWarrantyType') & '_prompt')

Validate::NewWarrantyType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('NewWarrantyType',p_web.GetValue('NewValue'))
    NewWarrantyType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::NewWarrantyType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('NewWarrantyType',p_web.GetValue('Value'))
    NewWarrantyType = p_web.GetValue('Value')
  End
  p_Web.SetValue('lookupfield','NewWarrantyType')
  do AfterLookup
  do Value::NewWarrantyType
  do SendAlert
  do Comment::NewWarrantyType

Value::NewWarrantyType  Routine
  p_web._DivHeader('UpdateExchangeStatus_' & p_web._nocolon('NewWarrantyType') & '_value',Choose(p_web.gsv('ChangeChargeType') = 'N','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.gsv('ChangeChargeType') = 'N')
  ! --- STRING --- NewWarrantyType
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('NewWarrantyType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''NewWarrantyType'',''updateexchangestatus_newwarrantytype_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('NewWarrantyType')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','NewWarrantyType',p_web.GetSessionValueFormat('NewWarrantyType'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('UES_SelectWarrantyType?LookupField=NewWarrantyType&Tab=2&ForeignField=cha:Charge_Type&_sort=&Refresh=sort&LookupFrom=UpdateExchangeStatus&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('UpdateExchangeStatus_' & p_web._nocolon('NewWarrantyType') & '_value')

Comment::NewWarrantyType  Routine
      loc:comment = ''
  p_web._DivHeader('UpdateExchangeStatus_' & p_web._nocolon('NewWarrantyType') & '_comment',Choose(p_web.gsv('ChangeChargeType') = 'N','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.gsv('ChangeChargeType') = 'N'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('UpdateExchangeStatus_' & p_web._nocolon('NewWarrantyType') & '_comment')

Prompt::NewJobNumber  Routine
  p_web._DivHeader('UpdateExchangeStatus_' & p_web._nocolon('NewJobNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Job Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::NewJobNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('NewJobNumber',p_web.GetValue('NewValue'))
    NewJobNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::NewJobNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('NewJobNumber',p_web.GetValue('Value'))
    NewJobNumber = p_web.GetValue('Value')
  End
      p_web.ssv('NewJobNumber',NewJobNumber)
  
      !now to deal with the changes to be made
      DO ExchangeStatus   !Shared routine
  do Value::NewJobNumber
  do SendAlert
  do Value::BrowseTagFile  !1
  do Comment::BrowseTagFile
  do Value::ButtonCommit  !1

Value::NewJobNumber  Routine
  p_web._DivHeader('UpdateExchangeStatus_' & p_web._nocolon('NewJobNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- NewJobNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('NewJobNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''NewJobNumber'',''updateexchangestatus_newjobnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('NewJobNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','NewJobNumber',p_web.GetSessionValueFormat('NewJobNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,'Enter Job Number and press [TAB]') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('UpdateExchangeStatus_' & p_web._nocolon('NewJobNumber') & '_value')

Comment::NewJobNumber  Routine
    loc:comment = p_web.Translate('Enter Job Number and press [TAB]')
  p_web._DivHeader('UpdateExchangeStatus_' & p_web._nocolon('NewJobNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::ButtonCommit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('ButtonCommit',p_web.GetValue('NewValue'))
    do Value::ButtonCommit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  !now to deal with the changes to be made
  
      DO ExchangeStatus   !Shared routine
  do Value::ButtonCommit
  do SendAlert
  do Value::BrowseTagFile  !1
  do Comment::BrowseTagFile
  do Value::NewJobNumber  !1

Value::ButtonCommit  Routine
  p_web._DivHeader('UpdateExchangeStatus_' & p_web._nocolon('ButtonCommit') & '_value',Choose(p_web.gsv('NewJobNumber') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.gsv('NewJobNumber') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''ButtonCommit'',''updateexchangestatus_buttoncommit_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ButtonCommit','Commit','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('UpdateExchangeStatus_' & p_web._nocolon('ButtonCommit') & '_value')

Comment::ButtonCommit  Routine
    loc:comment = ''
  p_web._DivHeader('UpdateExchangeStatus_' & p_web._nocolon('ButtonCommit') & '_comment',Choose(p_web.gsv('NewJobNumber') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.gsv('NewJobNumber') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::BrowseTagFile  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('BrowseTagFile',p_web.GetValue('NewValue'))
    do Value::BrowseTagFile
  Else
    p_web.StoreValue('tag:sessionID')
  End

Value::BrowseTagFile  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseTagFile --
  p_web.SetValue('BrowseTagFile:NoForm',1)
  p_web.SetValue('BrowseTagFile:FormName',loc:formname)
  p_web.SetValue('BrowseTagFile:parentIs','Form')
  p_web.SetValue('_parentProc','UpdateExchangeStatus')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('UpdateExchangeStatus_BrowseTagFile_embedded_div')&'"><!-- Net:BrowseTagFile --></div><13,10>'
    p_web._DivHeader('UpdateExchangeStatus_' & lower('BrowseTagFile') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('UpdateExchangeStatus_' & lower('BrowseTagFile') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseTagFile --><13,10>'
  end
  do SendPacket

Comment::BrowseTagFile  Routine
    loc:comment = ''
  p_web._DivHeader('UpdateExchangeStatus_' & p_web._nocolon('BrowseTagFile') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('UpdateExchangeStatus_' & p_web._nocolon('BrowseTagFile') & '_comment')

Validate::ButtonFinish  Routine
  !clear all sessionvariables as we cancel the form
  p_web.ssv('SendingResult','')
  p_web.ssv('NewJobNumber','')
  p_web.ssv('NewStatus','')
  p_web.ssv('NewChargeType','')
  p_web.ssv('NewWarrantyType','')
  p_web.ssv('ChangeChargeType','')
  
  !and the local variables
  SendingResult = ''
  NewJobNumber = ''
  NewStatus = ''
  NewChargeType = ''
  NewWarrantyType = ''
  ChangeChargeType = ''
  
  !clear out the tag file used to hold jobs updated
  LOOP
      Access:TagFile.clearkey(tag:keyTagged)
      tag:sessionID = p_web.SessionID
      set(tag:keyTagged,tag:keyTagged)
      if Access:TagFile.next() then break.
      if tag:sessionID <> p_web.SessionID then break.
      Access:TagFile.DeleteRecord(0)
  END !loop
  
  
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('ButtonFinish',p_web.GetValue('NewValue'))
    do Value::ButtonFinish
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do Value::ButtonFinish
  do SendAlert

Value::ButtonFinish  Routine
  p_web._DivHeader('UpdateExchangeStatus_' & p_web._nocolon('ButtonFinish') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''ButtonFinish'',''updateexchangestatus_buttonfinish_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ButtonFinish','Finish','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('IndexPage')) & ''','''&clip('_self')&''')',loc:javascript,0,'/images/accept.png',,,,'Return to Index Page')

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('UpdateExchangeStatus_' & p_web._nocolon('ButtonFinish') & '_value')

Comment::ButtonFinish  Routine
    loc:comment = ''
  p_web._DivHeader('UpdateExchangeStatus_' & p_web._nocolon('ButtonFinish') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('UpdateExchangeStatus_NewStatus_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::NewStatus
      else
        do Value::NewStatus
      end
  of lower('UpdateExchangeStatus_ChangeChargeType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::ChangeChargeType
      else
        do Value::ChangeChargeType
      end
  of lower('UpdateExchangeStatus_NewChargeType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::NewChargeType
      else
        do Value::NewChargeType
      end
  of lower('UpdateExchangeStatus_NewWarrantyType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::NewWarrantyType
      else
        do Value::NewWarrantyType
      end
  of lower('UpdateExchangeStatus_NewJobNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::NewJobNumber
      else
        do Value::NewJobNumber
      end
  of lower('UpdateExchangeStatus_ButtonCommit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::ButtonCommit
      else
        do Value::ButtonCommit
      end
  of lower('UpdateExchangeStatus_ButtonFinish_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::ButtonFinish
      else
        do Value::ButtonFinish
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('UpdateExchangeStatus_form:ready_',1)
  p_web.SetSessionValue('UpdateExchangeStatus_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_UpdateExchangeStatus',0)

PreCopy  Routine
  p_web.SetValue('UpdateExchangeStatus_form:ready_',1)
  p_web.SetSessionValue('UpdateExchangeStatus_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_UpdateExchangeStatus',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('UpdateExchangeStatus_form:ready_',1)
  p_web.SetSessionValue('UpdateExchangeStatus_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('UpdateExchangeStatus:Primed',0)

PreDelete       Routine
  p_web.SetValue('UpdateExchangeStatus_form:ready_',1)
  p_web.SetSessionValue('UpdateExchangeStatus_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('UpdateExchangeStatus:Primed',0)
  p_web.setsessionvalue('showtab_UpdateExchangeStatus',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
          If p_web.IfExistsValue('ChangeChargeType') = 0
            p_web.SetValue('ChangeChargeType','N')
            ChangeChargeType = 'N'
          End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('UpdateExchangeStatus_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('UpdateExchangeStatus_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
  ! tab = 3
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('UpdateExchangeStatus:Primed',0)
  p_web.StoreValue('NewStatus')
  p_web.StoreValue('ChangeChargeType')
  p_web.StoreValue('NewChargeType')
  p_web.StoreValue('NewWarrantyType')
  p_web.StoreValue('NewJobNumber')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
