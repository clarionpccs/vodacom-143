

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('WEBSERVER389.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! Report the WIPAUI File
!!! </summary>
WIPAuditReport_NotScanned PROCEDURE (NetWebServerWorker p_web)

  ! The NetTalk Extension to ABC report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
locSiteLocation      STRING(30)                            !
tmp:printedby        STRING(60)                            !
tmp:TelephoneNumber  STRING(20)                            !
tmp:FaxNumber        STRING(20)                            !
Make_Model           STRING(60)                            !
tmp:Job_Number       STRING(20)                            !
tmp:DefaultTelephone STRING(30)                            !
tmp:DefaultFax       STRING(30)                            !
TheAddress           GROUP,PRE(address)                    !
TheName              STRING(30)                            !
AddressLine1         STRING(30)                            !
AddressLine2         STRING(30)                            !
AddressLine3         STRING(30)                            !
Postcode             STRING(30)                            !
Telephone            STRING(30)                            !
Fax                  STRING(30)                            !
EmailAddress         STRING(255)                           !
                     END                                   !
tmp:AuditNumber      LONG                                  !
tmp:DateTimeCompleted STRING(20)                           !
scanStatus           STRING(30)                            !
statusType           STRING(30)                            !
StatusDays           LONG                                  !
StartingRef          LONG                                  !
locIMEINumber        STRING(30)                            !
PrintThisLine        BYTE                                  !
Total_No_Of_Lines    LONG                                  !
GrandTotalOfLines    LONG                                  !
StatusUsedQ          QUEUE,PRE(SUQ)                        !
Status               STRING(30)                            !
                     END                                   !
Process:View         VIEW(STATUS)
                       PROJECT(sts:Status)
                     END
ProgressWindow       WINDOW('Report WIPAUI'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:DEFAULT), |
  DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT,AT(396,2688,7521,8104),PRE(RPT),PAPER(PAPER:A4),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(385,531,7521,1802),USE(?unnamed:2)
                         STRING(@s30),AT(94,52),USE(address:TheName),FONT(,14,,FONT:bold),TRN
                         STRING('<<-- Date Stamp -->'),AT(6240,1333,927,135),USE(?ReportDateStamp:2),FONT('Arial',8, |
  ,FONT:regular),TRN
                         STRING(@s30),AT(94,313,3531,198),USE(address:AddressLine1),FONT(,9),TRN
                         STRING(@s30),AT(94,469,3531,198),USE(address:AddressLine2),FONT(,9),TRN
                         STRING(@s30),AT(94,625,3531,198),USE(address:AddressLine3),FONT(,9),TRN
                         STRING(@s30),AT(94,781),USE(address:Postcode),FONT(,9),TRN
                         STRING('Date Completed:'),AT(4948,969),USE(?StrDateCompleted),FONT('Arial',8,COLOR:Black,, |
  CHARSET:ANSI),TRN
                         STRING(@s20),AT(6208,969),USE(tmp:DateTimeCompleted),FONT('Arial',8,COLOR:Black,FONT:bold, |
  CHARSET:ANSI),LEFT,TRN
                         STRING('Tel: '),AT(94,990),USE(?String15),FONT(,9),TRN
                         STRING(@s30),AT(563,990),USE(address:Telephone),FONT(,9),TRN
                         STRING('WIP Audit Number:'),AT(4948,1135),USE(?StrAuditNumber),FONT('Arial',8,COLOR:Black, |
  ,CHARSET:ANSI),TRN
                         STRING(@n10),AT(6146,1135),USE(tmp:AuditNumber),FONT('Arial',8,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  RIGHT,TRN
                         STRING('Fax:'),AT(94,1146),USE(?String16),FONT(,9),TRN
                         STRING(@s30),AT(563,1146),USE(address:Fax),FONT(,9),TRN
                         STRING(@s255),AT(573,1302,3531,198),USE(address:EmailAddress),FONT(,9),TRN
                         STRING('Date Printed:'),AT(4948,1302),USE(?String16:9),FONT('Arial',8,,,CHARSET:ANSI),TRN
                         STRING('Page Number:'),AT(4948,1458),USE(?String16:2),FONT('Arial',8,,,CHARSET:ANSI),TRN
                         STRING(@N3),AT(6177,1458),USE(?PageNo),FONT(,8,,FONT:bold),PAGENO,TRN
                         STRING('Email:'),AT(104,1302),USE(?String16:4),FONT(,9),TRN
                         STRING('WIP Audit Report'),AT(3656,0,3438,260),USE(?String21),FONT('Arial',14,,FONT:bold), |
  RIGHT(10),TRN
                       END
DETAIL                 DETAIL,AT(0,0,,167),USE(?DetailBand)
                         STRING(@s30),AT(5490,0),USE(scanStatus),FONT('Arial',8,,,CHARSET:ANSI),LEFT,TRN
                         STRING(@s60),AT(2229,10),USE(Make_Model),FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@s8),AT(10,10),USE(wia:Ref_Number),FONT('Arial',8,,,CHARSET:ANSI),RIGHT(1),TRN
                         STRING(@s30),AT(1052,10),USE(locIMEINumber),FONT('Arial',8,,,CHARSET:ANSI)
                       END
detail3                DETAIL,AT(0,0,7521,344),USE(?DETAIL3)
                         STRING('No Records Found'),AT(2948,73,1573,177),USE(?StringNoRecords),FONT('Arial',12,,FONT:bold, |
  CHARSET:ANSI),TRN
                       END
detail1                DETAIL,AT(0,0,,490),USE(?unnamed:6)
                         LINE,AT(94,62,2146,0),USE(?Line3),COLOR(COLOR:Black)
                         STRING('Total Number Of Lines:'),AT(94,135),USE(?String29),FONT('Arial',8,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING(@n-10),AT(1635,135),USE(Total_No_Of_Lines),FONT('Arial',8,,FONT:bold,CHARSET:ANSI), |
  RIGHT(1),TRN
                       END
detail2                DETAIL,AT(0,0,,333),USE(?unnamed:7)
                         STRING(@s30),AT(115,73,7094,208),USE(statusType),FONT(,12,,FONT:bold),CENTER,TRN
                       END
                       FOOTER,AT(396,10833,7521,500),USE(?unnamed:3)
                       END
                       FORM,AT(365,510,7521,10802),USE(?unnamed)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,10802),USE(?Image1)
                         STRING('Job Number'),AT(104,1979),USE(?String23),FONT('Arial',8,,FONT:bold),TRN
                         STRING('IMEI'),AT(1083,1979),USE(?String5),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),TRN
                         STRING('Make/Model'),AT(2260,1979),USE(?String6),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),TRN
                         STRING('Scan Status'),AT(5521,1979),USE(?String31),FONT('Arial',8,,FONT:bold),TRN
                       END
                     END
!*** PDF-Tools v4.1 Start (All)
!--- ClarioNET NOT Enabled
!--- PDFXTABCTemplate: True  Level: 4
!--- PDFXTABCTemplate: RPM: False
!--- PDFXTABCTemplate: Icetips: False
PDFXTR3               PDFXToolsReportClass                 !  Added by PDF-Tools Templates v4.1
PDFXTR3:rtn           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXTR3:vpf           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Orientation      BYTE(0)                      !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:TopMargin        REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:BottomMargin     REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:LeftMargin       REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:RightMargin      REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Selection        STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PDFXT4:Papers:Queue           QUEUE,PRE()                  !  Added by PDF-Tools Templates v4.1
PLQ:Description                 STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PLQ:PaperCode                   BYTE                       !  Added by PDF-Tools Templates v4.1
                              END                          !  Added by PDF-Tools Templates v4.1
!*** PDF-Tools v4.1 End
ThisWindow           CLASS(ReportManager)
EndReport              PROCEDURE(),BYTE,DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepStringClass                       ! Progress Manager
Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.EndReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
      IF (Total_No_Of_Lines > 0)
          PRINT(rpt:Detail1)  
          GrandTotalOfLines += Total_no_Of_Lines
      END !IF    
  
      !TB12740 J - 23/07/14 - often this may be an empty report - this stops it from erroring
      if (GrandTotalOfLines = 0) then    
          Print(rpt:Detail3)        
      END !if grand total is zero
  ReturnValue = PARENT.EndReport()
  RETURN ReturnValue


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('WIPAuditReport_NotScanned')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:EXCHANGE.Open                                     ! File EXCHANGE used by this procedure, so make sure it's RelationManager is open
  Relate:JOBS.SetOpenRelated()
  Relate:JOBS.Open                                         ! File JOBS used by this procedure, so make sure it's RelationManager is open
  Relate:STATUS.SetOpenRelated()
  Relate:STATUS.Open                                       ! File STATUS used by this procedure, so make sure it's RelationManager is open
  Relate:WEBJOB.Open                                       ! File WEBJOB used by this procedure, so make sure it's RelationManager is open
  Relate:WIPEXC.Open                                       ! File WIPEXC used by this procedure, so make sure it's RelationManager is open
  Access:JOBSE.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:WIPAUI.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
      locSiteLocation = p_web.GSV('BookingSiteLocation')
      tmp:DateTimeCompleted = format(p_web.GSV('wim:Date_Completed'),@d06)
      tmp:AuditNumber = p_web.GSV('wim:Audit_Number')
      !GrandTotalOfLines = 0
      p_web.ssv('GrandTotalOfLines','0')
        
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = p_web.GSV('BookingAccount')
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
            address:TheName      = tra:Company_Name
            address:AddressLine1 = tra:Address_Line1
            address:AddressLine2 = tra:Address_Line2
            address:AddressLine3 = tra:Address_Line3
            address:Postcode     = tra:Postcode
            address:Telephone    = tra:Telephone_Number
            address:Fax          = tra:Fax_Number
            address:EmailAddress = tra:EmailAddress
        END ! IF
        
        p_web.SSV('lastStatus','')
        
      !TB12740 - need a starting point of so many days ago - J - 23/07/14
      StatusDays = getini(locSiteLocation,'StatusDays',0,clip(path())&'\WIPAudit.ini')
      If StatusDays = 0 then
          StartingRef = 0
      ELSE
          Access:jobs.clearkey(job:Date_Booked_Key)
          job:date_booked =  today() - StatusDays
          set(job:Date_Booked_Key,job:Date_Booked_Key)
          if Access:jobs.previous() then             
              !No jobs in system?
              StartingRef = 0
          ELSE
              StartingRef = job:Ref_Number
          END
      END
      !TB12740 - end of find a starting refnumber
  
      !TB12740 - J - 23/07/14 - Will need a list of status in audit so can include status even if excluded
      Access:WIPAUI.ClearKey(wia:Audit_Number_Key)
      wia:Audit_Number = p_web.GSV('wim:Audit_Number')
      set(wia:Audit_Number_Key,wia:Audit_Number_Key)
      loop
          IF Access:WIPAUI.next() then break.
          StatusUsedQ.SUQ:Status = wia:Status
          get(StatusUsedQ,SUQ:Status)
          if error() then
              !not found add it
              StatusUsedQ.SUQ:Status = wia:Status
              add(StatusUsedQ)
          END !if error()           
      END ! loop through wipaui    
      !TB12740 - end of creating list of statuses
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('WIPAuditReport_NotScanned',ProgressWindow) ! Restore window settings from non-volatile store
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    loc:pdfname = '.\$$$' & format(random(1,99999),@n05) &'.pdf'
    ProgressWindow{prop:hide} = 1
  End
  ProgressMgr.Init(ScrollSort:AllowAlpha+ScrollSort:AllowNumeric,ScrollBy:RunTime)
  ThisReport.Init(Process:View, Relate:STATUS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, sts:Status)
  ThisReport.CaseSensitiveValue = FALSE
  ThisReport.AddSortOrder(sts:Status_Key)
  ThisReport.SetFilter('UPPER(sts:Loan) <<> ''YES''')
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:STATUS.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      Loc:html = '<script type="text/javascript">alert('''&clip('No Records')&''');top.close();</script>'
      p_web.ParseHTML(loc:html)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHANGE.Close
    Relate:JOBS.Close
    Relate:STATUS.Close
    Relate:WEBJOB.Close
    Relate:WIPEXC.Close
  END
  IF SELF.Opened
    INIMgr.Update('WIPAuditReport_NotScanned',ProgressWindow) ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.OpenReport()
  !*** PDF-Tools v4.1 Start (ABC)
  IF ~ReturnValue                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.Init(Report, loc:PDFName,,PDFA_OFF)            !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPaper(CHOOSE(SYSTEM{PROP:Autopaper}=True,Report{PROPPRINT:Paper},PAPER:USER), CHOOSE(Report{PROP:Thous}=True,PROP:Thous,CHOOSE(Report{PROP:MM}=True,PROP:MM,CHOOSE(Report{PROP:Points}=True,PROP:Points,0))), Report{PROPPRINT:PaperWidth}, Report{PROPPRINT:PaperHeight}, Report{PROP:Landscape}) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetCompression(TRUE,TRUE,FALSE,COMPRTYPE_C_AUTO,75,COMPRTYPE_I_AUTO,COMPRTYPE_M_AUTO) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetAuthor('ServiceBase 3g')          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetTitle('WIP Audit Report')         !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetSubject('ServiceBase 3g')         !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPdfCreator('ServiceBase 3g')      !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetKeywords()                        !  Added by PDF-Tools Templates v4.1
    PDFXTR3.EnableLinkAnalyzer(True)             !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetSpecVersion(SPECVERSION_15,TRUE)  !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPageLayout(PAGELAYOUT_SINGLEPAGE) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPageMode(PAGEMODE_NONE)           !  Added by PDF-Tools Templates v4.1
    IF PAGEMODE_NONE = PAGEMODE_FULLSCREEN       !  Added by PDF-Tools Templates v4.1
      PDFXTR3:vpf = VP_FSPM_NONE                 !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
      PDFXTR3:vpf = 0                            !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDETOOLBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDEMENUBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDEWINDOWUI). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_FITWINDOW). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_CENTERWINDOW). !  Added by PDF-Tools Templates v4.1
      IF TRUE                                     THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_DISPLAYDOCTITLE). !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetViewerPrefs(PDFXTR3:vpf)          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPreview(False)                    !  Added by PDF-Tools Templates v4.1
  END
  !*** PDF-Tools v4.1 End
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp:2{PROP:Text} = FORMAT(TODAY(),@d06)
  END
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeCloseEvent()
  !*** PDF-Tools v4.1 Start (ABC)
  PDFXTR3.Kill()
  !*** PDF-Tools v4.1 End
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

firstJob                    LONG(0)
SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
      !TB12740 - J - 23/07/14
      !for the status to be used in this report it must NOT be in the excluded list
      PrintThisLine = true
      Access:WIPEXC.clearkey(wix:StatusTypeKey)
      wix:Location = locSiteLocation
      wix:Status = sts:Status
      if NOT access:WIPEXC.fetch(wix:StatusTypeKey)
          !found in exclusions don't use it - Unless a job in that status has been scanned in this audit.
          SUQ:Status = wix:Status
          get(StatusUsedQ,SUQ:Status)
          if error() then !cycle.      !not found in the used list either
              !do nothing
              PrintThisLine = false
          !ELSE left as above
          !    PrintThisLine = true
          END !if error on get from used status queue !TB12740 - J - 23/07/14  
      END !if found in the exclusions !TB12740 - J - 23/07/14
              
      If PrintThisLine = true                        
          !Do the normal stuff
          scanStatus = 'NOT SCANNED'
  
          firstJob = 1
          Total_No_Of_Lines = 0
          IF (sts:Exchange = 'YES')
              Access:WEBJOB.ClearKey(wob:HeadExchangeStatus)
              wob:HeadAccountNumber = p_web.GSV('BookingAccount')
              wob:Exchange_Status   = sts:Status
              wob:RefNumber         = StartingRef       !TB12740 - J - 23/07/14 - added to limit search - this is part of existing key
              SET(wob:HeadExchangeStatus, wob:HeadExchangeStatus)
          else
              Access:WEBJOB.ClearKey(wob:HeadCurrentStatusKey)
              wob:HeadAccountNumber = p_web.GSV('BookingAccount')
              wob:Current_Status    = sts:Status
              wob:RefNumber         = StartingRef       !TB12740 - J - 23/07/14 - added to limit search - this is part of existing key
              SET(wob:HeadCurrentStatusKey, wob:HeadCurrentStatusKey)
          end
          LOOP UNTIL Access:WEBJOB.Next() <> Level:Benign
              IF (wob:HeadAccountNumber <> p_web.GSV('BookingAccount'))
                  BREAK
              END ! IF
              IF (sts:Exchange = 'YES')
                  IF (wob:Exchange_Status <> sts:Status)
                      BREAK
                  END ! IF
              ELSE
                  IF (wob:Current_Status <> sts:Status)
                      BREAK
                  END ! IF
              END ! IF
              
              Access:JOBSE.ClearKey(jobe:RefNumberKey)
              jobe:RefNumber = wob:RefNumber
              if Access:JOBSE.Fetch(jobe:RefNumberKey) = Level:Benign
                  if jobe:HubRepair = 1 then cycle.
              end
  
              Access:JOBS.ClearKey(job:Ref_Number_Key)
              job:Ref_Number = wob:RefNumber
              if Access:JOBS.Fetch(job:Ref_Number_Key) then cycle.
              
              ! Already On This Audit?
              Access:WIPAUI.ClearKey(wia:AuditStatusRefNoKey)
              wia:Audit_Number = p_web.GSV('wim:Audit_Number')
              wia:Status = sts:Status
              wia:Ref_Number = wob:RefNumber
              IF (Access:WIPAUI.TryFetch(wia:AuditStatusRefNoKey) = Level:Benign)
                  CYCLE
              END ! IF
              
              IF (sts:Exchange = 'YES')
                  Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                  xch:Ref_Number = job:Exchange_Unit_Number
                  IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                      locIMEINumber = xch:ESN
                      Make_Model = CLIP(xch:Manufacturer) & ' - ' & CLIP(xch:Model_Number)
                  ELSE ! IF
                      CYCLE
                  END ! IF
              ELSE
                  locIMEINumber = job:ESN
                  Make_Model = CLIP(job:Manufacturer) & ' - ' & CLIP(job:Model_Number)
              END !I F
              IF (firstJob = 1)
                  statusType = sts:Status
                  PRINT(rpt:Detail2)
                  firstJob = 0
              END ! IF
              Total_No_Of_Lines += 1
              PRINT(rpt:Detail)
          END ! IF
          
          IF (Total_No_Of_Lines > 0)
              PRINT(rpt:Detail1)
              GrandTotalOfLines = 10            
          END ! IF
          
      END !if PrintThisLine = true
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  !*** PDF-Tools v4.1 Start (ABC)
  ReturnValue = False                            !  Added by PDF-Tools Templates v4.1
    PDFXTR3:rtn = PDFXTR3.Generate(SELF.ImageQueue,True, , ) !  Added by PDF-Tools Templates v4.1
    IF PDFXTR3:rtn THEN                          !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
  RETURN ReturnValue
  !*** PDF-Tools v4.1 End
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
  RETURN ReturnValue

