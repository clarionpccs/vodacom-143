

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER363.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER017.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER022.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER025.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER027.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER031.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER037.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER047.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER081.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER098.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER102.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER103.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER132.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER142.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER144.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER147.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER158.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER311.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER323.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER344.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER364.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER365.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER366.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER367.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER368.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER369.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER370.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER494.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER495.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER496.INC'),ONCE        !Req'd for module callout resolution
                     END


CustomerServicesForm PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locAuditText         STRING(255)                           !
locExchangedIMEI     STRING(30)                            !
locExchangeMSN       STRING(30)                            !
locCustomerName      STRING(60)                            !
locIncomingIMEI      STRING(30)                            !
locIncomingMSN       STRING(30)                            !
loc2ndExchangeIMEI   STRING(30)                            !
loc2ndMSN            STRING(30)                            !
locEngineer          STRING(60)                            !
FranchiseAccount     LONG                                  !
locJobTimeRemain     STRING(30)                            !
locStatusTimeRemain  STRING(30)                            !
locPassedPassword    STRING(100)                           !
                    MAP
BuildModelFilter        PROCEDURE(STRING pIMEINumber,LONG pChangeModel=0)
CheckForRequiredHiddenFields  PROCEDURE()
CheckSecurity           PROCEDURE()
DidAccessoriesChange        PROCEDURE()
DidAnythingChange       PROCEDURE()
DisplayEngineerDetails      PROCEDURE()
DisplayEstimateDetails      PROCEDURE()
DisplayNumberOfBouncers PROCEDURE()
ISIMEIValid             PROCEDURE(STRING pIMEINumber,STRING pModelNumber,*STRING pError),LONG
PopulateAccessoryList   PROCEDURE()
SaveDataToCheckForChanges       PROCEDURE()
ValidateDOP             PROCEDURE(*STRING pError),LONG
ValidateTradeAccount    PROCEDURE(*STRING pError),LONG
WorkOutIMEINumbers      PROCEDURE()
WorkOutStatusAndTurnaroundTimes PROCEDURE()
                    END ! MAP

wc                          CLASS(NetWebClient)                   ! Generated by NetTalk Extension (Class Definition)
                            END ! CLASS
FilesOpened     Long
WEBJOB::State  USHORT
MANUFACT::State  USHORT
ESNMODEL::State  USHORT
JOBSE2::State  USHORT
TURNARND::State  USHORT
MODELCOL::State  USHORT
TRANTYPE::State  USHORT
MODELNUM::State  USHORT
MODPROD::State  USHORT
SUBTRACC::State  USHORT
JOBACC::State  USHORT
TRADEACC_ALIAS::State  USHORT
JOBNOTES::State  USHORT
USERS::State  USHORT
TRADEACC::State  USHORT
DEFAULTS::State  USHORT
JOBTHIRD::State  USHORT
UNITTYPE::State  USHORT
TRAHUBAC::State  USHORT
JOBS_ALIAS::State  USHORT
EXCHANGE::State  USHORT
JOBSE::State  USHORT
JOBS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('CustomerServicesForm')
  loc:formname = 'CustomerServicesForm_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('CustomerServicesForm','')
      do SendPacket
      Do checkPassword
      do SendPacket
    p_web._DivHeader('CustomerServicesForm',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferCustomerServicesForm',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferCustomerServicesForm',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferCustomerServicesForm',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_CustomerServicesForm',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferCustomerServicesForm',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_CustomerServicesForm',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(MANUFACT)
  p_web._OpenFile(ESNMODEL)
  p_web._OpenFile(JOBSE2)
  p_web._OpenFile(TURNARND)
  p_web._OpenFile(MODELCOL)
  p_web._OpenFile(TRANTYPE)
  p_web._OpenFile(MODELNUM)
  p_web._OpenFile(MODPROD)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(JOBACC)
  p_web._OpenFile(TRADEACC_ALIAS)
  p_web._OpenFile(JOBNOTES)
  p_web._OpenFile(USERS)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(DEFAULTS)
  p_web._OpenFile(JOBTHIRD)
  p_web._OpenFile(UNITTYPE)
  p_web._OpenFile(TRAHUBAC)
  p_web._OpenFile(JOBS_ALIAS)
  p_web._OpenFile(EXCHANGE)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(JOBS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(MANUFACT)
  p_Web._CloseFile(ESNMODEL)
  p_Web._CloseFile(JOBSE2)
  p_Web._CloseFile(TURNARND)
  p_Web._CloseFile(MODELCOL)
  p_Web._CloseFile(TRANTYPE)
  p_Web._CloseFile(MODELNUM)
  p_Web._CloseFile(MODPROD)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(JOBACC)
  p_Web._CloseFile(TRADEACC_ALIAS)
  p_Web._CloseFile(JOBNOTES)
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(DEFAULTS)
  p_Web._CloseFile(JOBTHIRD)
  p_Web._CloseFile(UNITTYPE)
  p_Web._CloseFile(TRAHUBAC)
  p_Web._CloseFile(JOBS_ALIAS)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('CustomerServicesForm_form:inited_',1)
  do RestoreMem

CancelForm  Routine
    !F (p_web.GSV('Amend') = 1)
        LockRecord(p_web.GSV('job:Ref_Number'),p_web.SessionID,1) ! Remove Lock
    !END ! IF  
    DidAccessoriesChange()
    DuplicateTabCheckDelete(p_web,'JOBUPDATEREFNO','')

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('job:Current_Status')
    p_web.SetPicture('job:Current_Status','@s30')
  End
  p_web.SetSessionPicture('job:Current_Status','@s30')
  If p_web.IfExistsValue('job:Exchange_Status')
    p_web.SetPicture('job:Exchange_Status','@s30')
  End
  p_web.SetSessionPicture('job:Exchange_Status','@s30')
  If p_web.IfExistsValue('job:Loan_Status')
    p_web.SetPicture('job:Loan_Status','@s30')
  End
  p_web.SetSessionPicture('job:Loan_Status','@s30')
  If p_web.IfExistsValue('job:Account_Number')
    p_web.SetPicture('job:Account_Number','@s15')
  End
  p_web.SetSessionPicture('job:Account_Number','@s15')
  If p_web.IfExistsValue('job:Order_Number')
    p_web.SetPicture('job:Order_Number','@s30')
  End
  p_web.SetSessionPicture('job:Order_Number','@s30')
  If p_web.IfExistsValue('job:ESN')
    p_web.SetPicture('job:ESN','@s20')
  End
  p_web.SetSessionPicture('job:ESN','@s20')
  If p_web.IfExistsValue('job:MSN')
    p_web.SetPicture('job:MSN','@s20')
  End
  p_web.SetSessionPicture('job:MSN','@s20')
  If p_web.IfExistsValue('job:Transit_Type')
    p_web.SetPicture('job:Transit_Type','@s30')
  End
  p_web.SetSessionPicture('job:Transit_Type','@s30')
  If p_web.IfExistsValue('job:Manufacturer')
    p_web.SetPicture('job:Manufacturer','@s30')
  End
  p_web.SetSessionPicture('job:Manufacturer','@s30')
  If p_web.IfExistsValue('job:Workshop')
    p_web.SetPicture('job:Workshop','@s3')
  End
  p_web.SetSessionPicture('job:Workshop','@s3')
  If p_web.IfExistsValue('job:Unit_Type')
    p_web.SetPicture('job:Unit_Type','@s30')
  End
  p_web.SetSessionPicture('job:Unit_Type','@s30')
  If p_web.IfExistsValue('job:Mobile_Number')
    p_web.SetPicture('job:Mobile_Number','@s15')
  End
  p_web.SetSessionPicture('job:Mobile_Number','@s15')
  If p_web.IfExistsValue('job:DOP')
    p_web.SetPicture('job:DOP','@d06b')
  End
  p_web.SetSessionPicture('job:DOP','@d06b')
  If p_web.IfExistsValue('job:Location')
    p_web.SetPicture('job:Location','@s30')
  End
  p_web.SetSessionPicture('job:Location','@s30')
  If p_web.IfExistsValue('job:Turnaround_Time')
    p_web.SetPicture('job:Turnaround_Time','@s30')
  End
  p_web.SetSessionPicture('job:Turnaround_Time','@s30')
  If p_web.IfExistsValue('job:Third_Party_Site')
    p_web.SetPicture('job:Third_Party_Site','@s30')
  End
  p_web.SetSessionPicture('job:Third_Party_Site','@s30')
  If p_web.IfExistsValue('job:Charge_Type')
    p_web.SetPicture('job:Charge_Type','@s30')
  End
  p_web.SetSessionPicture('job:Charge_Type','@s30')
  If p_web.IfExistsValue('job:Special_Instructions')
    p_web.SetPicture('job:Special_Instructions','@s30')
  End
  p_web.SetSessionPicture('job:Special_Instructions','@s30')
  If p_web.IfExistsValue('job:Warranty_Charge_Type')
    p_web.SetPicture('job:Warranty_Charge_Type','@s30')
  End
  p_web.SetSessionPicture('job:Warranty_Charge_Type','@s30')
  If p_web.IfExistsValue('job:Insurance_Reference_Number')
    p_web.SetPicture('job:Insurance_Reference_Number','@s30')
  End
  p_web.SetSessionPicture('job:Insurance_Reference_Number','@s30')
  If p_web.IfExistsValue('job:Incoming_Courier')
    p_web.SetPicture('job:Incoming_Courier','@s30')
  End
  p_web.SetSessionPicture('job:Incoming_Courier','@s30')
  If p_web.IfExistsValue('job:Courier')
    p_web.SetPicture('job:Courier','@s30')
  End
  p_web.SetSessionPicture('job:Courier','@s30')
  If p_web.IfExistsValue('job:Account_Number')
    p_web.SetPicture('job:Account_Number','@s15')
  End
  p_web.SetSessionPicture('job:Account_Number','@s15')
  If p_web.IfExistsValue('job:ESN')
    p_web.SetPicture('job:ESN','@s20')
  End
  p_web.SetSessionPicture('job:ESN','@s20')
  If p_web.IfExistsValue('job:Account_Number')
    p_web.SetPicture('job:Account_Number','@s15')
  End
  p_web.SetSessionPicture('job:Account_Number','@s15')
  If p_web.IfExistsValue('job:MSN')
    p_web.SetPicture('job:MSN','@s20')
  End
  p_web.SetSessionPicture('job:MSN','@s20')
  If p_web.IfExistsValue('job:Order_Number')
    p_web.SetPicture('job:Order_Number','@s30')
  End
  p_web.SetSessionPicture('job:Order_Number','@s30')
  If p_web.IfExistsValue('job:ProductCode')
    p_web.SetPicture('job:ProductCode','@s30')
  End
  p_web.SetSessionPicture('job:ProductCode','@s30')
  If p_web.IfExistsValue('job:Title')
    p_web.SetPicture('job:Title','@s4')
  End
  p_web.SetSessionPicture('job:Title','@s4')
  If p_web.IfExistsValue('job:Initial')
    p_web.SetPicture('job:Initial','@s1')
  End
  p_web.SetSessionPicture('job:Initial','@s1')
  If p_web.IfExistsValue('job:Model_Number')
    p_web.SetPicture('job:Model_Number','@s30')
  End
  p_web.SetSessionPicture('job:Model_Number','@s30')
  If p_web.IfExistsValue('job:Surname')
    p_web.SetPicture('job:Surname','@s30')
  End
  p_web.SetSessionPicture('job:Surname','@s30')
  If p_web.IfExistsValue('job:Unit_Type')
    p_web.SetPicture('job:Unit_Type','@s30')
  End
  p_web.SetSessionPicture('job:Unit_Type','@s30')
  If p_web.IfExistsValue('job:Colour')
    p_web.SetPicture('job:Colour','@s30')
  End
  p_web.SetSessionPicture('job:Colour','@s30')
  If p_web.IfExistsValue('job:Transit_Type')
    p_web.SetPicture('job:Transit_Type','@s30')
  End
  p_web.SetSessionPicture('job:Transit_Type','@s30')
  If p_web.IfExistsValue('job:DOP')
    p_web.SetPicture('job:DOP','@d06b')
  End
  p_web.SetSessionPicture('job:DOP','@d06b')
  If p_web.IfExistsValue('job:Mobile_Number')
    p_web.SetPicture('job:Mobile_Number','@s15')
  End
  p_web.SetSessionPicture('job:Mobile_Number','@s15')
  If p_web.IfExistsValue('job:Turnaround_Time')
    p_web.SetPicture('job:Turnaround_Time','@s30')
  End
  p_web.SetSessionPicture('job:Turnaround_Time','@s30')
  If p_web.IfExistsValue('job:Third_Party_Site')
    p_web.SetPicture('job:Third_Party_Site','@s30')
  End
  p_web.SetSessionPicture('job:Third_Party_Site','@s30')
  If p_web.IfExistsValue('job:ThirdPartyDateDesp')
    p_web.SetPicture('job:ThirdPartyDateDesp',p_web.site.DatePicture)
  End
  p_web.SetSessionPicture('job:ThirdPartyDateDesp',p_web.site.DatePicture)
  If p_web.IfExistsValue('job:Date_Paid')
    p_web.SetPicture('job:Date_Paid',p_web.site.DatePicture)
  End
  p_web.SetSessionPicture('job:Date_Paid',p_web.site.DatePicture)
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  If p_web.GSV('Amend') = 0
    loc:TabNumber += 1
  End
  If p_web.GSV('Amend') <> 0
    loc:TabNumber += 1
  End
  Case p_Web.GetValue('lookupfield')
  Of 'job:Account_Number'
    p_web.setsessionvalue('showtab_CustomerServicesForm',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(SUBTRACC)
        IF (sub:Account_Number <> p_web.GSV('save:AccountNumber'))
            IF (ValidateTradeAccount(loc:alert))
                p_web.SSV('job:Account_Number',p_web.GSV('save:AccountNumber'))
            END ! IF
        END ! IF
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locIncomingIMEI')
  Of 'job:Account_Number'
    p_web.setsessionvalue('showtab_CustomerServicesForm',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(TRAHUBAC)
        IF (sub:Account_Number <> p_web.GSV('save:AccountNumber'))
            IF (ValidateTradeAccount(loc:alert))
                p_web.SSV('job:Account_Number',p_web.GSV('save:AccountNumber'))
            END ! IF
        END ! IF
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locIncomingIMEI')
  Of 'job:ProductCode'
    p_web.setsessionvalue('showtab_CustomerServicesForm',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MODPROD)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.job:Title')
  Of 'job:Model_Number'
    p_web.setsessionvalue('showtab_CustomerServicesForm',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MODELNUM)
        p_web.setsessionvalue('job:Manufacturer',mod:Manufacturer)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.job:Surname')
  Of 'job:Unit_Type'
    p_web.setsessionvalue('showtab_CustomerServicesForm',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(UNITTYPE)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.jobe:Network')
  Of 'job:Colour'
    p_web.setsessionvalue('showtab_CustomerServicesForm',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MODELCOL)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.job:Transit_Type')
  Of 'job:Transit_Type'
    p_web.setsessionvalue('showtab_CustomerServicesForm',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(TRANTYPE)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.')
  Of 'job:Turnaround_Time'
    p_web.setsessionvalue('showtab_CustomerServicesForm',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(TURNARND)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.job:Third_Party_Site')
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('wob:RecordNumber',wob:RecordNumber)
  p_web.SetSessionValue('job:Current_Status',job:Current_Status)
  p_web.SetSessionValue('job:Exchange_Status',job:Exchange_Status)
  p_web.SetSessionValue('job:Loan_Status',job:Loan_Status)
  p_web.SetSessionValue('locJobTimeRemain',locJobTimeRemain)
  p_web.SetSessionValue('locStatusTimeRemain',locStatusTimeRemain)
  p_web.SetSessionValue('jobe:HubRepair',jobe:HubRepair)
  p_web.SetSessionValue('jobe:OBFvalidated',jobe:OBFvalidated)
  p_web.SetSessionValue('job:Account_Number',job:Account_Number)
  p_web.SetSessionValue('locIncomingIMEI',locIncomingIMEI)
  p_web.SetSessionValue('locIncomingMSN',locIncomingMSN)
  p_web.SetSessionValue('job:Order_Number',job:Order_Number)
  p_web.SetSessionValue('locExchangedIMEI',locExchangedIMEI)
  p_web.SetSessionValue('locExchangeMSN',locExchangeMSN)
  p_web.SetSessionValue('locCustomerName',locCustomerName)
  p_web.SetSessionValue('job:ESN',job:ESN)
  p_web.SetSessionValue('job:MSN',job:MSN)
  p_web.SetSessionValue('loc2ndExchangeIMEI',loc2ndExchangeIMEI)
  p_web.SetSessionValue('loc2ndMSN',loc2ndMSN)
  p_web.SetSessionValue('job:Transit_Type',job:Transit_Type)
  p_web.SetSessionValue('job:Manufacturer',job:Manufacturer)
  p_web.SetSessionValue('job:Workshop',job:Workshop)
  p_web.SetSessionValue('job:Unit_Type',job:Unit_Type)
  p_web.SetSessionValue('jobe:Network',jobe:Network)
  p_web.SetSessionValue('job:Mobile_Number',job:Mobile_Number)
  p_web.SetSessionValue('job:DOP',job:DOP)
  p_web.SetSessionValue('job:Location',job:Location)
  p_web.SetSessionValue('job:Turnaround_Time',job:Turnaround_Time)
  p_web.SetSessionValue('job:Third_Party_Site',job:Third_Party_Site)
  p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  p_web.SetSessionValue('job:Special_Instructions',job:Special_Instructions)
  p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
  p_web.SetSessionValue('locEngineer',locEngineer)
  p_web.SetSessionValue('jbn:Engineers_Notes',jbn:Engineers_Notes)
  p_web.SetSessionValue('job:Insurance_Reference_Number',job:Insurance_Reference_Number)
  p_web.SetSessionValue('job:Incoming_Courier',job:Incoming_Courier)
  p_web.SetSessionValue('jbn:Fault_Description',jbn:Fault_Description)
  p_web.SetSessionValue('job:Courier',job:Courier)
  p_web.SetSessionValue('FranchiseAccount',FranchiseAccount)
  p_web.SetSessionValue('job:Account_Number',job:Account_Number)
  p_web.SetSessionValue('job:ESN',job:ESN)
  p_web.SetSessionValue('job:Account_Number',job:Account_Number)
  p_web.SetSessionValue('job:MSN',job:MSN)
  p_web.SetSessionValue('job:Order_Number',job:Order_Number)
  p_web.SetSessionValue('job:ProductCode',job:ProductCode)
  p_web.SetSessionValue('job:Title',job:Title)
  p_web.SetSessionValue('job:Initial',job:Initial)
  p_web.SetSessionValue('job:Model_Number',job:Model_Number)
  p_web.SetSessionValue('job:Surname',job:Surname)
  p_web.SetSessionValue('job:Unit_Type',job:Unit_Type)
  p_web.SetSessionValue('jobe:EndUserTelNo',jobe:EndUserTelNo)
  p_web.SetSessionValue('job:Colour',job:Colour)
  p_web.SetSessionValue('job:Transit_Type',job:Transit_Type)
  p_web.SetSessionValue('job:DOP',job:DOP)
  p_web.SetSessionValue('job:Mobile_Number',job:Mobile_Number)
  p_web.SetSessionValue('job:Turnaround_Time',job:Turnaround_Time)
  p_web.SetSessionValue('job:Third_Party_Site',job:Third_Party_Site)
  p_web.SetSessionValue('job:ThirdPartyDateDesp',job:ThirdPartyDateDesp)
  p_web.SetSessionValue('job:Date_Paid',job:Date_Paid)
  p_web.SetSessionValue('jobe:VSACustomer',jobe:VSACustomer)
  p_web.SetSessionValue('jbn:Fault_Description',jbn:Fault_Description)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('wob:RecordNumber')
    wob:RecordNumber = p_web.GetValue('wob:RecordNumber')
    p_web.SetSessionValue('wob:RecordNumber',wob:RecordNumber)
  End
  if p_web.IfExistsValue('job:Current_Status')
    job:Current_Status = p_web.GetValue('job:Current_Status')
    p_web.SetSessionValue('job:Current_Status',job:Current_Status)
  End
  if p_web.IfExistsValue('job:Exchange_Status')
    job:Exchange_Status = p_web.GetValue('job:Exchange_Status')
    p_web.SetSessionValue('job:Exchange_Status',job:Exchange_Status)
  End
  if p_web.IfExistsValue('job:Loan_Status')
    job:Loan_Status = p_web.GetValue('job:Loan_Status')
    p_web.SetSessionValue('job:Loan_Status',job:Loan_Status)
  End
  if p_web.IfExistsValue('locJobTimeRemain')
    locJobTimeRemain = p_web.GetValue('locJobTimeRemain')
    p_web.SetSessionValue('locJobTimeRemain',locJobTimeRemain)
  End
  if p_web.IfExistsValue('locStatusTimeRemain')
    locStatusTimeRemain = p_web.GetValue('locStatusTimeRemain')
    p_web.SetSessionValue('locStatusTimeRemain',locStatusTimeRemain)
  End
  if p_web.IfExistsValue('jobe:HubRepair')
    jobe:HubRepair = p_web.GetValue('jobe:HubRepair')
    p_web.SetSessionValue('jobe:HubRepair',jobe:HubRepair)
  End
  if p_web.IfExistsValue('jobe:OBFvalidated')
    jobe:OBFvalidated = p_web.GetValue('jobe:OBFvalidated')
    p_web.SetSessionValue('jobe:OBFvalidated',jobe:OBFvalidated)
  End
  if p_web.IfExistsValue('job:Account_Number')
    job:Account_Number = p_web.GetValue('job:Account_Number')
    p_web.SetSessionValue('job:Account_Number',job:Account_Number)
  End
  if p_web.IfExistsValue('locIncomingIMEI')
    locIncomingIMEI = p_web.GetValue('locIncomingIMEI')
    p_web.SetSessionValue('locIncomingIMEI',locIncomingIMEI)
  End
  if p_web.IfExistsValue('locIncomingMSN')
    locIncomingMSN = p_web.GetValue('locIncomingMSN')
    p_web.SetSessionValue('locIncomingMSN',locIncomingMSN)
  End
  if p_web.IfExistsValue('job:Order_Number')
    job:Order_Number = p_web.GetValue('job:Order_Number')
    p_web.SetSessionValue('job:Order_Number',job:Order_Number)
  End
  if p_web.IfExistsValue('locExchangedIMEI')
    locExchangedIMEI = p_web.GetValue('locExchangedIMEI')
    p_web.SetSessionValue('locExchangedIMEI',locExchangedIMEI)
  End
  if p_web.IfExistsValue('locExchangeMSN')
    locExchangeMSN = p_web.GetValue('locExchangeMSN')
    p_web.SetSessionValue('locExchangeMSN',locExchangeMSN)
  End
  if p_web.IfExistsValue('locCustomerName')
    locCustomerName = p_web.GetValue('locCustomerName')
    p_web.SetSessionValue('locCustomerName',locCustomerName)
  End
  if p_web.IfExistsValue('job:ESN')
    job:ESN = p_web.GetValue('job:ESN')
    p_web.SetSessionValue('job:ESN',job:ESN)
  End
  if p_web.IfExistsValue('job:MSN')
    job:MSN = p_web.GetValue('job:MSN')
    p_web.SetSessionValue('job:MSN',job:MSN)
  End
  if p_web.IfExistsValue('loc2ndExchangeIMEI')
    loc2ndExchangeIMEI = p_web.GetValue('loc2ndExchangeIMEI')
    p_web.SetSessionValue('loc2ndExchangeIMEI',loc2ndExchangeIMEI)
  End
  if p_web.IfExistsValue('loc2ndMSN')
    loc2ndMSN = p_web.GetValue('loc2ndMSN')
    p_web.SetSessionValue('loc2ndMSN',loc2ndMSN)
  End
  if p_web.IfExistsValue('job:Transit_Type')
    job:Transit_Type = p_web.GetValue('job:Transit_Type')
    p_web.SetSessionValue('job:Transit_Type',job:Transit_Type)
  End
  if p_web.IfExistsValue('job:Manufacturer')
    job:Manufacturer = p_web.GetValue('job:Manufacturer')
    p_web.SetSessionValue('job:Manufacturer',job:Manufacturer)
  End
  if p_web.IfExistsValue('job:Workshop')
    job:Workshop = p_web.GetValue('job:Workshop')
    p_web.SetSessionValue('job:Workshop',job:Workshop)
  End
  if p_web.IfExistsValue('job:Unit_Type')
    job:Unit_Type = p_web.GetValue('job:Unit_Type')
    p_web.SetSessionValue('job:Unit_Type',job:Unit_Type)
  End
  if p_web.IfExistsValue('jobe:Network')
    jobe:Network = p_web.GetValue('jobe:Network')
    p_web.SetSessionValue('jobe:Network',jobe:Network)
  End
  if p_web.IfExistsValue('job:Mobile_Number')
    job:Mobile_Number = p_web.GetValue('job:Mobile_Number')
    p_web.SetSessionValue('job:Mobile_Number',job:Mobile_Number)
  End
  if p_web.IfExistsValue('job:DOP')
    job:DOP = p_web.dformat(clip(p_web.GetValue('job:DOP')),'@d06b')
    p_web.SetSessionValue('job:DOP',job:DOP)
  End
  if p_web.IfExistsValue('job:Location')
    job:Location = p_web.GetValue('job:Location')
    p_web.SetSessionValue('job:Location',job:Location)
  End
  if p_web.IfExistsValue('job:Turnaround_Time')
    job:Turnaround_Time = p_web.GetValue('job:Turnaround_Time')
    p_web.SetSessionValue('job:Turnaround_Time',job:Turnaround_Time)
  End
  if p_web.IfExistsValue('job:Third_Party_Site')
    job:Third_Party_Site = p_web.GetValue('job:Third_Party_Site')
    p_web.SetSessionValue('job:Third_Party_Site',job:Third_Party_Site)
  End
  if p_web.IfExistsValue('job:Charge_Type')
    job:Charge_Type = p_web.GetValue('job:Charge_Type')
    p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  End
  if p_web.IfExistsValue('job:Special_Instructions')
    job:Special_Instructions = p_web.GetValue('job:Special_Instructions')
    p_web.SetSessionValue('job:Special_Instructions',job:Special_Instructions)
  End
  if p_web.IfExistsValue('job:Warranty_Charge_Type')
    job:Warranty_Charge_Type = p_web.GetValue('job:Warranty_Charge_Type')
    p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
  End
  if p_web.IfExistsValue('locEngineer')
    locEngineer = p_web.GetValue('locEngineer')
    p_web.SetSessionValue('locEngineer',locEngineer)
  End
  if p_web.IfExistsValue('jbn:Engineers_Notes')
    jbn:Engineers_Notes = p_web.GetValue('jbn:Engineers_Notes')
    p_web.SetSessionValue('jbn:Engineers_Notes',jbn:Engineers_Notes)
  End
  if p_web.IfExistsValue('job:Insurance_Reference_Number')
    job:Insurance_Reference_Number = p_web.GetValue('job:Insurance_Reference_Number')
    p_web.SetSessionValue('job:Insurance_Reference_Number',job:Insurance_Reference_Number)
  End
  if p_web.IfExistsValue('job:Incoming_Courier')
    job:Incoming_Courier = p_web.GetValue('job:Incoming_Courier')
    p_web.SetSessionValue('job:Incoming_Courier',job:Incoming_Courier)
  End
  if p_web.IfExistsValue('jbn:Fault_Description')
    jbn:Fault_Description = p_web.GetValue('jbn:Fault_Description')
    p_web.SetSessionValue('jbn:Fault_Description',jbn:Fault_Description)
  End
  if p_web.IfExistsValue('job:Courier')
    job:Courier = p_web.GetValue('job:Courier')
    p_web.SetSessionValue('job:Courier',job:Courier)
  End
  if p_web.IfExistsValue('FranchiseAccount')
    FranchiseAccount = p_web.GetValue('FranchiseAccount')
    p_web.SetSessionValue('FranchiseAccount',FranchiseAccount)
  End
  if p_web.IfExistsValue('job:Account_Number')
    job:Account_Number = p_web.GetValue('job:Account_Number')
    p_web.SetSessionValue('job:Account_Number',job:Account_Number)
  End
  if p_web.IfExistsValue('job:ESN')
    job:ESN = p_web.GetValue('job:ESN')
    p_web.SetSessionValue('job:ESN',job:ESN)
  End
  if p_web.IfExistsValue('job:Account_Number')
    job:Account_Number = p_web.GetValue('job:Account_Number')
    p_web.SetSessionValue('job:Account_Number',job:Account_Number)
  End
  if p_web.IfExistsValue('job:MSN')
    job:MSN = p_web.GetValue('job:MSN')
    p_web.SetSessionValue('job:MSN',job:MSN)
  End
  if p_web.IfExistsValue('job:Order_Number')
    job:Order_Number = p_web.GetValue('job:Order_Number')
    p_web.SetSessionValue('job:Order_Number',job:Order_Number)
  End
  if p_web.IfExistsValue('job:ProductCode')
    job:ProductCode = p_web.GetValue('job:ProductCode')
    p_web.SetSessionValue('job:ProductCode',job:ProductCode)
  End
  if p_web.IfExistsValue('job:Title')
    job:Title = p_web.GetValue('job:Title')
    p_web.SetSessionValue('job:Title',job:Title)
  End
  if p_web.IfExistsValue('job:Initial')
    job:Initial = p_web.GetValue('job:Initial')
    p_web.SetSessionValue('job:Initial',job:Initial)
  End
  if p_web.IfExistsValue('job:Model_Number')
    job:Model_Number = p_web.GetValue('job:Model_Number')
    p_web.SetSessionValue('job:Model_Number',job:Model_Number)
  End
  if p_web.IfExistsValue('job:Surname')
    job:Surname = p_web.GetValue('job:Surname')
    p_web.SetSessionValue('job:Surname',job:Surname)
  End
  if p_web.IfExistsValue('job:Unit_Type')
    job:Unit_Type = p_web.GetValue('job:Unit_Type')
    p_web.SetSessionValue('job:Unit_Type',job:Unit_Type)
  End
  if p_web.IfExistsValue('jobe:EndUserTelNo')
    jobe:EndUserTelNo = p_web.GetValue('jobe:EndUserTelNo')
    p_web.SetSessionValue('jobe:EndUserTelNo',jobe:EndUserTelNo)
  End
  if p_web.IfExistsValue('job:Colour')
    job:Colour = p_web.GetValue('job:Colour')
    p_web.SetSessionValue('job:Colour',job:Colour)
  End
  if p_web.IfExistsValue('job:Transit_Type')
    job:Transit_Type = p_web.GetValue('job:Transit_Type')
    p_web.SetSessionValue('job:Transit_Type',job:Transit_Type)
  End
  if p_web.IfExistsValue('job:DOP')
    job:DOP = p_web.dformat(clip(p_web.GetValue('job:DOP')),'@d06b')
    p_web.SetSessionValue('job:DOP',job:DOP)
  End
  if p_web.IfExistsValue('job:Mobile_Number')
    job:Mobile_Number = p_web.GetValue('job:Mobile_Number')
    p_web.SetSessionValue('job:Mobile_Number',job:Mobile_Number)
  End
  if p_web.IfExistsValue('job:Turnaround_Time')
    job:Turnaround_Time = p_web.GetValue('job:Turnaround_Time')
    p_web.SetSessionValue('job:Turnaround_Time',job:Turnaround_Time)
  End
  if p_web.IfExistsValue('job:Third_Party_Site')
    job:Third_Party_Site = p_web.GetValue('job:Third_Party_Site')
    p_web.SetSessionValue('job:Third_Party_Site',job:Third_Party_Site)
  End
  if p_web.IfExistsValue('job:ThirdPartyDateDesp')
    job:ThirdPartyDateDesp = p_web.dformat(clip(p_web.GetValue('job:ThirdPartyDateDesp')),p_web.site.DatePicture)
    p_web.SetSessionValue('job:ThirdPartyDateDesp',job:ThirdPartyDateDesp)
  End
  if p_web.IfExistsValue('job:Date_Paid')
    job:Date_Paid = p_web.dformat(clip(p_web.GetValue('job:Date_Paid')),p_web.site.DatePicture)
    p_web.SetSessionValue('job:Date_Paid',job:Date_Paid)
  End
  if p_web.IfExistsValue('jobe:VSACustomer')
    jobe:VSACustomer = p_web.GetValue('jobe:VSACustomer')
    p_web.SetSessionValue('jobe:VSACustomer',jobe:VSACustomer)
  End
  if p_web.IfExistsValue('jbn:Fault_Description')
    jbn:Fault_Description = p_web.GetValue('jbn:Fault_Description')
    p_web.SetSessionValue('jbn:Fault_Description',jbn:Fault_Description)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('CustomerServicesForm_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  ! Build Data
    IF (p_web.IfExistsValue('Amend'))
        p_web.StoreValue('Amend')
        
    END ! IF
    IF (p_web.IfExistsValue('wob:RecordNumber') AND p_web.IfExistsValue('Change_btn'))
        p_web.SSV('ViewOnly','')
        IF (p_web.IfExistsValue('ViewOnly'))
            p_web.StoreValue('ViewOnly')
        END !
    END
    
    IF (p_web.IfExistsValue('CustServReturnURL'))
        p_web.StoreValue('CustServReturnURL')
    END ! IF
    
    IF (DuplicateTabCheck(p_web,'NEWJOBBOOKING',1) = 1)
        CreateScript(packet,'alert("Error! It appears that another job is being booking, or was not closed correctly.\n\nIf this is not the case, then re-login and try again.");window.open("' & p_web.GSV('CustServReturnURL') & '","_self")')
        DO SendPacket
        EXIT
    ELSE        
        CASE DuplicateTabCheck(p_web,'JOBUPDATEREFNO',p_web.GSV('wob:RecordNumber'))
        OF 1 ! Ok
        OF 0
            DuplicateTabCheckAdd(p_web,'JOBUPDATEREFNO',p_web.GSV('wob:RecordNumber'))
        OF 2
        ! Editing another job
            CreateScript(packet,'alert("Error! It appears that another job is being edited, or was not closed correctly.\n\nIf this is not the case, then re-login and try again.");window.open("' & p_web.GSV('CustServReturnURL') & '","_self")')
            DO SendPacket
            EXIT
        END ! CASe
    END ! IF
    
    
    IF (p_web.IfExistsValue('FirstTime'))
        IF (p_web.IfExistsValue('wob:RecordNumber'))
            ! Get a specific job
            Access:WEBJOB.ClearKey(wob:RecordNumberKey)
            wob:RecordNumber = p_web.GSV('wob:RecordNumber')
            IF (Access:WEBJOB.TryFetch(wob:RecordNumberKey) = Level:Benign)
                Access:JOBS.ClearKey(job:Ref_Number_Key)
                job:Ref_Number = wob:RefNumber
                IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                    Access:JOBSE.ClearKey(jobe:RefNumberKey)
                    jobe:RefNumber = wob:RefNumber
                    IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                        p_web.FileToSessionQueue(WEBJOB)
                        p_web.FileToSessionQueue(JOBS)
                        p_web.FileToSessionQueue(JOBSE)
                    ELSE ! IF
                    END ! IF
                ELSE ! IF
                END ! IF
            ELSE ! IF
            END ! IF
        END ! IF        
        
        ! Only populate everything the first time the window is opened
        Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
        jbn:RefNumber = p_web.GSV('job:Ref_Number')
        IF (Access:JOBNOTES.TryFetcH(jbn:RefNumberKey) = Level:Benign)
            p_web.FileToSessionQueue(JOBNOTES)
        END ! IF
        
        Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
        jobe2:RefNumber = p_web.GSV('job:Ref_Number')
        IF (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
            p_web.FileToSessionQueue(JOBSE2)
        END ! IF
        
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
            p_web.SSV('txtJobNumber',p_web.GSV('job:Ref_Number') & '-' & p_web.GSV('tra:BranchIdentification') & | 
                p_web.GSV('wob:JobNumber') & ' Head Acc: ' & p_web.GSV('wob:HeadAccountNumber'))
        END ! IF
        
        p_web.SSV('txtBookingDetails',FORMAT(p_web.GSV('job:Date_Booked'),@d06b) & ' ' & | 
            FORMAT(p_web.GSV('job:Time_Booked'),@t01b) & '-' & p_web.GSV('job:Who_Booked'))
        
        WorkOutIMEINumbers()
        
        WorkOutStatusAndTurnaroundTimes()
      
        DisplayNumberOfBouncers()
      
        DisplayEstimateDetails()
        
        DisplayEngineerDetails()
        
        PopulateAccessoryList()
        
        p_web.SSV('locCustomerName',CLIP(p_web.GSV('job:Title') & ' ') & CLIP(p_web.GSV('job:Initial') & ' ') & |
            p_web.GSV('job:Surname'))
        
    END ! IF
  
    
  ! Edit Job Settings
    IF (p_web.GSV('Amend') = 0)
            ! Only populate everything the first time
        LOOP 1 TIMES ! LOOP TO BREAK OUT
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = p_web.GSV('job:Ref_Number')
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
            ! Reget job to check for locks
                IF (JobInUse(p_web.SessionID))
                    packet = CLIP(packet) & '<script>alert("This job is in use by another station.");</script>'
                    p_web.SSV('Job:ViewOnly',1)
                    BREAK
                ELSE
                    
                END ! IF
            END ! IF
        
            CheckSecurity()
        
            SaveDataToCheckForChanges()
        
            BuildModelFilter(p_web.GSV('job:ESN'))
            
            CheckForRequiredHiddenFields()
                
        END ! LOOP
        
    END ! IF
  
    p_web.SSV('Job:ViewOnly',0)
    
    IF (p_web.GSV('ViewOnly') = 1)
        p_web.SSV('Job:ViewOnly',1)
    END !I F
  
    
    ! #13088 Do not allow to amend the job that is not in control (DBH: 14/03/2014)
    if (p_web.GSV('BookingSite') = 'RRC' And |
        ((p_web.GSV('job:Location') <> p_web.GSV('Default:RRCLocation') And |
        p_web.GSV('job:Location') <> p_web.GSV('Default:DespatchToCustomer') And |
        p_web.GSV('job:Location') <> p_web.GSV('Default:InTransitPUP') And |
        p_web.GSV('job:Location') <> p_web.GSV('Default:PUPLocation')) Or |
        (p_web.GSV('jobe:HubRepair') = 1) Or |
        (p_web.GSV('wob:HeadAccountNumber') <> p_web.GSV('BookingAccount'))))
  
        !ShowAlert('This job is not in this RRC''s control and cannot be amended.')
  
        p_web.SSV('Job:ViewOnly',1)
    ELSE
        
        SentToHub(p_web)
        IF (p_web.GSV('job:warranty_job') = 'YES' AND p_web.GSV('wob:EDI') <> 'XXX')
            IF (p_web.GSV('job:date_Completed') > 0 AND p_web.GSV('SentToHub') = 0)
                IF (p_web.GSV('wob:EDI') = 'NO' OR p_web.GSV('wob:EDI') = 'YES' OR p_web.GSV('wob:EDI') = 'PAY' OR p_web.GSV('wob:EDI') = 'APP')
                    p_web.SSV('Job:ViewOnly',1)
                ELSE
                    ! Rejected warranty is handeled outsite this screen
                    IF (p_web.GSV('Job:ViewOnly') <> 1)
                        IF (NOT (p_web.IfExistsValue('valpass')))
                            CreateScript(packet,'showJobPassword()')
                            p_web.SSV('SecondTime',0)
                            DO SendPacket
                            p_web.SSV('Job:ViewOnly',1)
                            EXIT
                            
                        ELSE
                            ! #13390 Check the passed password
                            ! See if they have the access level (DBH: 08/10/2014)
                            !locPassedPassword = UPPER(VodacomClass.ScrambleUn(wc.DecodeWebString(p_web.GetValue('valpass'))))
                            locPassedPassword = p_web.GetValue('valpass')
                            
                            BHAddToDebugLog('valpass = ' & locPassedPassword)
                            
                            foundUser# = 0
                            Access:USERS.ClearKey(use:Surname_Active_Key)
                            use:Active = 'YES'
                            use:Surname = ''
                            SET(use:Surname_Active_Key,use:Surname_Active_Key)
                            LOOP UNTIL Access:USERS.Next() <> Level:Benign
                                IF (use:Active <> 'YES')
                                    BREAK
                                END ! IF
                                
                                BHAddToDebugLog('user password (' & CLIP(use:Password) & ') = ' & UPPER(VodacomClass.ScrambleUn(use:Password)))
                                
                                IF (UPPER(VodacomClass.ScrambleUn(use:Password)) = UPPER(CLIP(locPassedPassword)))
                                    IF (SecurityCheckFailed(use:Password,'EDIT REJECTED WARRANTY JOB'))
                                        CreateScript(packet,'alert("You do not have access to edit this job!");window.open("' & p_web.GSV('CustServReturnURL') & '","_self")')
                                        DO SendPacket
                                    !EXIT
                                        p_web.SSV('Job:ViewOnly',1)
                                    END ! IF
  
                                    foundUser# = 1  
                                    BREAK
                                END ! IF
                                
                            END ! LOOP
                            
                            BHAddToDebugLog('found user = ' & foundUser#)
                            IF (foundUser# = 0)
                                CreateScript(packet,'alert("Invalid Password!");window.open("' & p_web.GSV('CustServReturnURL') & '","_self")')
                                DO SendPacket
                                p_web.SSV('Job:ViewOnly',1)
                                EXIT
                            END !IF
                            
                        END ! IF
                    END ! IF                        
                END
            END
        END       
    END ! if
    
    IF (p_web.GSV('Job:ViewOnly') <> 1)
        LockRecord(job:Ref_Number,p_web.SessionID,0)
    END ! IF
    
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locJobTimeRemain = p_web.RestoreValue('locJobTimeRemain')
 locStatusTimeRemain = p_web.RestoreValue('locStatusTimeRemain')
 locIncomingIMEI = p_web.RestoreValue('locIncomingIMEI')
 locIncomingMSN = p_web.RestoreValue('locIncomingMSN')
 locExchangedIMEI = p_web.RestoreValue('locExchangedIMEI')
 locExchangeMSN = p_web.RestoreValue('locExchangeMSN')
 locCustomerName = p_web.RestoreValue('locCustomerName')
 loc2ndExchangeIMEI = p_web.RestoreValue('loc2ndExchangeIMEI')
 loc2ndMSN = p_web.RestoreValue('loc2ndMSN')
 locEngineer = p_web.RestoreValue('locEngineer')
 FranchiseAccount = p_web.RestoreValue('FranchiseAccount')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('CustServReturnURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('CustomerServicesForm_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('CustomerServicesForm_ChainTo')
    loc:formaction = p_web.GetSessionValue('CustomerServicesForm_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('CustServReturnURL')
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="CustomerServicesForm" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="CustomerServicesForm" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="CustomerServicesForm" ></input><13,10>'
  end

  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_CustomerServicesForm">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_CustomerServicesForm" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_CustomerServicesForm')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Job Details') & ''''
        If p_web.GSV('Amend') = 0
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('View Job Details') & ''''
        End
        If p_web.GSV('Amend') <> 0
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Edit Job Details') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Actions') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_CustomerServicesForm')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_CustomerServicesForm'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('CustomerServicesForm_CustServJobStageBrowse_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('CustomerServicesForm_BrowseChargeableParts_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('CustomerServicesForm_BrowseWarrantyParts_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('CustomerServicesForm_CustServJobStageBrowse_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('CustomerServicesForm_BrowseChargeableParts_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('CustomerServicesForm_BrowseWarrantyParts_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('CustomerServicesForm_CustServJobStageBrowse_embedded_div')&''');'
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('CustomerServicesForm_BrowseChargeableParts_embedded_div')&''');'
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('CustomerServicesForm_BrowseWarrantyParts_embedded_div')&''');'
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='SUBTRACC'
      If p_web.GSV('Amend') <> 0
            p_web.SetValue('SelectField',clip(loc:formname) & '.job:ESN')
      End
    End
    If upper(p_web.getvalue('LookupFile'))='TRAHUBAC'
      If p_web.GSV('Amend') <> 0
          If Not (p_web.GSV('Hide:MSN') = 1)
            p_web.SetValue('SelectField',clip(loc:formname) & '.job:MSN')
          End
      End
    End
    If upper(p_web.getvalue('LookupFile'))='MODPROD'
      If p_web.GSV('Amend') <> 0
          If Not (p_web.GSV('Hide:EndUser') = 1)
            p_web.SetValue('SelectField',clip(loc:formname) & '.job:Title')
          End
      End
    End
    If upper(p_web.getvalue('LookupFile'))='MODELNUM'
      If p_web.GSV('Amend') <> 0
          If Not (p_web.GSV('Hide:EndUser') = 1)
            p_web.SetValue('SelectField',clip(loc:formname) & '.job:Surname')
          End
      End
    End
    If upper(p_web.getvalue('LookupFile'))='UNITTYPE'
      If p_web.GSV('Amend') <> 0
            p_web.SetValue('SelectField',clip(loc:formname) & '.jobe:EndUserTelNo')
      End
    End
    If upper(p_web.getvalue('LookupFile'))='MODELCOL'
      If p_web.GSV('Amend') <> 0
            p_web.SetValue('SelectField',clip(loc:formname) & '.job:Transit_Type')
      End
    End
    If upper(p_web.getvalue('LookupFile'))='TRANTYPE'
      If p_web.GSV('Amend') <> 0
            p_web.SetValue('SelectField',clip(loc:formname) & '.job:DOP')
      End
    End
    If upper(p_web.getvalue('LookupFile'))='TURNARND'
      If p_web.GSV('Amend') <> 0
            p_web.SetValue('SelectField',clip(loc:formname) & '.jbn:Fault_Description')
      End
    End
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.wob:RecordNumber')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab4'''
          If p_web.GSV('Amend') = 0
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          End
          If p_web.GSV('Amend') <> 0
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_CustomerServicesForm')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab4'');'&CRLF
    if p_web.GSV('Amend') = 0
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
    end
    if p_web.GSV('Amend') <> 0
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel4">'&CRLF &|
                                    '  <div id="panel4Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Job Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel4Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_CustomerServicesForm_4">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab4" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab4">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Job Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::wob:RefNumber_Hidden
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::wob:RefNumber_Hidden
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::txtJobNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::txtJobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::txtBookingDetails
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::txtBookingDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::txtBlank
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::txtBlank
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::txtStatus
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::txtStatus
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Current_Status
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Current_Status
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Exchange_Status
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Exchange_Status
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Loan_Status
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Loan_Status
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locJobTimeRemain
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locJobTimeRemain
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locStatusTimeRemain
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locStatusTimeRemain
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe:HubRepair
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe:HubRepair
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::txtHubRepairDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe:OBFvalidated
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe:OBFvalidated
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::txtOBFDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwJobStage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GSV('Job:ViewOnly') <> 1 AND p_web.GSV('Amend') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnAmendJob
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
  If p_web.GSV('Amend') = 0
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('View Job Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_CustomerServicesForm_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('View Job Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('View Job Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('View Job Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('View Job Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Account_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Account_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locIncomingIMEI
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locIncomingIMEI
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locIncomingMSN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locIncomingMSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Order_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Order_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locExchangedIMEI
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locExchangedIMEI
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locExchangeMSN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locExchangeMSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locCustomerName
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locCustomerName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:ESN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:ESN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:MSN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:MSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::spacer1
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::spacer1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::loc2ndExchangeIMEI
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::loc2ndExchangeIMEI
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::loc2ndMSN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::loc2ndMSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Transit_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Transit_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::txtModelProductCode
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::txtModelProductCode
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::spacer2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Manufacturer
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Manufacturer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Workshop
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Workshop
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Unit_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Unit_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe:Network
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe:Network
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Mobile_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Mobile_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:DOP
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:DOP
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Location
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Location
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Turnaround_Time
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Turnaround_Time
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Third_Party_Site
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Third_Party_Site
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Charge_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Special_Instructions
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Special_Instructions
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Warranty_Charge_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Warranty_Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::txtEstimate
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&' colspan="2">'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::txtEstimate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GSV('textBouncer') <> ''
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&2&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::textBouncer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    end
    do SendPacket
    If p_web.GSV('textBouncer') <> ''
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnPreviousJobs
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::__line1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locEngineer
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locEngineer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& ' rowspan="4">'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jbn:Engineers_Notes
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&' valign="top" colspan="3" rowspan="4">'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jbn:Engineers_Notes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::btnEngineerHistory
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnEngineerHistory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::_hidden1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::_hidden2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Insurance_Reference_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Insurance_Reference_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Incoming_Courier
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Incoming_Courier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& ' rowspan="4">'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jbn:Fault_Description
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&' valign="top" colspan="3" rowspan="4">'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jbn:Fault_Description
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Courier
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Courier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::txtChargeableInvoiceNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::txtChargeableInvoiceNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::txtWarrantyInvoiceNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::txtWarrantyInvoiceNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::_hidden3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GSV('job:Chargeable_Job') = 'YES'
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwChargeableParts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('job:Warranty_Job') = 'YES'
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwWarrantyParts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab2  Routine
  If p_web.GSV('Amend') <> 0
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Edit Job Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_CustomerServicesForm_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Edit Job Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Edit Job Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Edit Job Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Edit Job Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::FranchiseAccount
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::FranchiseAccount
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Account_Number_Franchise
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Account_Number_Franchise
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:ESN_Edit
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:ESN_Edit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Account_Number_Generic
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Account_Number_Generic
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:MSN_Edit
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:MSN_Edit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Order_Number_edit
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Order_Number_edit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:ProductCode_Edit
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:ProductCode_Edit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Title_Edit
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Title_Edit
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Initial_Edit
      do Value::job:Initial_Edit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Model_Number_Edit
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Model_Number_Edit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Surname_Edit
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Surname_Edit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Unit_Type_Edit
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Unit_Type_Edit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe:EndUserTelNo
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe:EndUserTelNo
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Colour
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Colour
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Transit_Type_Edit
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Transit_Type_Edit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:DOP_Edit
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:DOP_Edit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Mobile_Number_Edit
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Mobile_Number_Edit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Turnaround_Time_Edit
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Turnaround_Time_Edit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Third_Party_Site_Edit
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Third_Party_Site_Edit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:ThirdPartyDateDesp
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:ThirdPartyDateDesp
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Date_Paid
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Date_Paid
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe:VSACustomer
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe:VSACustomer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jbn:Fault_Description_edit
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jbn:Fault_Description_edit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab3  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Actions') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_CustomerServicesForm_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnFaultCodes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnConsignmentHistory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnThirdPartyDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnValidatePOP
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnViewCosts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnAuditTrail
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnStatusChanges
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnLocationChanges
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnContactHistory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnExchangeUnit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnLoanUnit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnEditAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnValidateSerialNo
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::wob:RefNumber_Hidden  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('wob:RefNumber_Hidden') & '_prompt',Choose(1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Job Number')
  If 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::wob:RefNumber_Hidden  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('wob:RefNumber_Hidden',p_web.GetValue('NewValue'))
    wob:RecordNumber = p_web.GetValue('NewValue') !FieldType= LONG Field = wob:RecordNumber
    do Value::wob:RefNumber_Hidden
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('wob:RecordNumber',p_web.dFormat(p_web.GetValue('Value'),'@s8'))
    wob:RecordNumber = p_web.GetValue('Value')
  End

Value::wob:RefNumber_Hidden  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('wob:RefNumber_Hidden') & '_value',Choose(1,'hdiv','adiv'))
  loc:extra = ''
  If Not (1)
  ! --- HIDDEN --- wob:RecordNumber
    packet = clip(packet) & p_web.CreateInput('hidden','wob:RecordNumber',p_web.GetSessionValue('wob:RecordNumber')) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Prompt::txtJobNumber  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('txtJobNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Repair Details')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::txtJobNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtJobNumber',p_web.GetValue('NewValue'))
    do Value::txtJobNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtJobNumber  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('txtJobNumber') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(p_web.GSV('txtJobNumber'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::txtBookingDetails  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('txtBookingDetails') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Date Booked')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::txtBookingDetails  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtBookingDetails',p_web.GetValue('NewValue'))
    do Value::txtBookingDetails
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtBookingDetails  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('txtBookingDetails') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(p_web.GSV('txtBookingDetails'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::txtBlank  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('txtBlank') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::txtBlank  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtBlank',p_web.GetValue('NewValue'))
    do Value::txtBlank
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtBlank  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('txtBlank') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::txtStatus  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('txtStatus') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Status Date')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::txtStatus  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtStatus',p_web.GetValue('NewValue'))
    do Value::txtStatus
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtStatus  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('txtStatus') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(FORMAT(p_web.GSV('job:Status_End_Date'),@d06b) & ' ' & FORMAT(p_web.GSV('job:Status_End_Time'),@t01b),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::job:Current_Status  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Current_Status') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Current Status')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Current_Status  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Current_Status',p_web.GetValue('NewValue'))
    job:Current_Status = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Current_Status
    do Value::job:Current_Status
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Current_Status',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Current_Status = p_web.GetValue('Value')
  End

Value::job:Current_Status  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Current_Status') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Current_Status
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Current_Status'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::job:Exchange_Status  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Exchange_Status') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Exchange Status')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Exchange_Status  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Exchange_Status',p_web.GetValue('NewValue'))
    job:Exchange_Status = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Exchange_Status
    do Value::job:Exchange_Status
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Exchange_Status',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Exchange_Status = p_web.GetValue('Value')
  End

Value::job:Exchange_Status  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Exchange_Status') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Exchange_Status
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Exchange_Status'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::job:Loan_Status  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Loan_Status') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Loan Status')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Loan_Status  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Loan_Status',p_web.GetValue('NewValue'))
    job:Loan_Status = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Loan_Status
    do Value::job:Loan_Status
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Loan_Status',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Loan_Status = p_web.GetValue('Value')
  End

Value::job:Loan_Status  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Loan_Status') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Loan_Status
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Loan_Status'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locJobTimeRemain  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('locJobTimeRemain') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Job Time Remain')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locJobTimeRemain  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locJobTimeRemain',p_web.GetValue('NewValue'))
    locJobTimeRemain = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locJobTimeRemain
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locJobTimeRemain',p_web.GetValue('Value'))
    locJobTimeRemain = p_web.GetValue('Value')
  End

Value::locJobTimeRemain  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('locJobTimeRemain') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locJobTimeRemain
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locJobTimeRemain'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locStatusTimeRemain  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('locStatusTimeRemain') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Status Time Remain')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locStatusTimeRemain  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locStatusTimeRemain',p_web.GetValue('NewValue'))
    locStatusTimeRemain = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locStatusTimeRemain
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locStatusTimeRemain',p_web.GetValue('Value'))
    locStatusTimeRemain = p_web.GetValue('Value')
  End

Value::locStatusTimeRemain  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('locStatusTimeRemain') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locStatusTimeRemain
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locStatusTimeRemain'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::jobe:HubRepair  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('jobe:HubRepair') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Hub Repair')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe:HubRepair  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe:HubRepair',p_web.GetValue('NewValue'))
    jobe:HubRepair = p_web.GetValue('NewValue') !FieldType= BYTE Field = jobe:HubRepair
    do Value::jobe:HubRepair
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('jobe:HubRepair',p_web.GetValue('Value'))
    jobe:HubRepair = p_web.GetValue('Value')
  End
  do Value::jobe:HubRepair
  do SendAlert

Value::jobe:HubRepair  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('jobe:HubRepair') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- jobe:HubRepair
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe:HubRepair'',''customerservicesform_jobe:hubrepair_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  If p_web.GetSessionValue('jobe:HubRepair') = true
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','jobe:HubRepair',clip(true),,loc:readonly,,,loc:javascript,,'Hub Repair') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CustomerServicesForm_' & p_web._nocolon('jobe:HubRepair') & '_value')


Validate::txtHubRepairDate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtHubRepairDate',p_web.GetValue('NewValue'))
    do Value::txtHubRepairDate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtHubRepairDate  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('txtHubRepairDate') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(FORMAT(p_web.GSV('jobe:HubRepairDate'),@d06b) & ' ' & FORMAT(p_web.GSV('jobe:HubRepairTime'),@t01b),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::jobe:OBFvalidated  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('jobe:OBFvalidated') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('OBF Validated')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe:OBFvalidated  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe:OBFvalidated',p_web.GetValue('NewValue'))
    jobe:OBFvalidated = p_web.GetValue('NewValue') !FieldType= BYTE Field = jobe:OBFvalidated
    do Value::jobe:OBFvalidated
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('jobe:OBFvalidated',p_web.GetValue('Value'))
    jobe:OBFvalidated = p_web.GetValue('Value')
  End
  do Value::jobe:OBFvalidated
  do SendAlert

Value::jobe:OBFvalidated  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('jobe:OBFvalidated') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- jobe:OBFvalidated
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe:OBFvalidated'',''customerservicesform_jobe:obfvalidated_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  If p_web.GetSessionValue('jobe:OBFvalidated') = true
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','jobe:OBFvalidated',clip(true),,loc:readonly,,,loc:javascript,,'Out Of Box Validation') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CustomerServicesForm_' & p_web._nocolon('jobe:OBFvalidated') & '_value')


Validate::txtOBFDate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtOBFDate',p_web.GetValue('NewValue'))
    do Value::txtOBFDate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtOBFDate  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('txtOBFDate') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.Translate(FORMAT(p_web.GSV('jobe:OBFvalidateDate'),@d06b) & ' ' & FORMAT(p_web.GSV('jobe:OBFValidateTime'),@t01b),) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::brwJobStage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwJobStage',p_web.GetValue('NewValue'))
    do Value::brwJobStage
  Else
    p_web.StoreValue('jst:Ref_Number')
  End

Value::brwJobStage  Routine
  loc:extra = ''
  ! --- BROWSE ---  CustServJobStageBrowse --
  p_web.SetValue('CustServJobStageBrowse:NoForm',1)
  p_web.SetValue('CustServJobStageBrowse:FormName',loc:formname)
  p_web.SetValue('CustServJobStageBrowse:parentIs','Form')
  p_web.SetValue('_parentProc','CustomerServicesForm')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('CustomerServicesForm_CustServJobStageBrowse_embedded_div')&'"><!-- Net:CustServJobStageBrowse --></div><13,10>'
    p_web._DivHeader('CustomerServicesForm_' & lower('CustServJobStageBrowse') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('CustomerServicesForm_' & lower('CustServJobStageBrowse') & '_value')
  else
    packet = clip(packet) & '<!-- Net:CustServJobStageBrowse --><13,10>'
  end
  do SendPacket


Validate::btnAmendJob  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnAmendJob',p_web.GetValue('NewValue'))
    do Value::btnAmendJob
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnAmendJob  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('btnAmendJob') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnAmendJob','Amend Job','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('CustomerServicesForm?' &'Amend=1')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()


Prompt::job:Account_Number  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Account_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Account Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Account_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Account_Number',p_web.GetValue('NewValue'))
    job:Account_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Account_Number
    do Value::job:Account_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Account_Number',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    job:Account_Number = p_web.GetValue('Value')
  End

Value::job:Account_Number  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Account_Number') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Account_Number
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Account_Number'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locIncomingIMEI  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('locIncomingIMEI') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Incoming IMEI')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locIncomingIMEI  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locIncomingIMEI',p_web.GetValue('NewValue'))
    locIncomingIMEI = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locIncomingIMEI
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locIncomingIMEI',p_web.GetValue('Value'))
    locIncomingIMEI = p_web.GetValue('Value')
  End

Value::locIncomingIMEI  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('locIncomingIMEI') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locIncomingIMEI
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locIncomingIMEI'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locIncomingMSN  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('locIncomingMSN') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('MSN')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locIncomingMSN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locIncomingMSN',p_web.GetValue('NewValue'))
    locIncomingMSN = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locIncomingMSN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locIncomingMSN',p_web.GetValue('Value'))
    locIncomingMSN = p_web.GetValue('Value')
  End

Value::locIncomingMSN  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('locIncomingMSN') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locIncomingMSN
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locIncomingMSN'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::job:Order_Number  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Order_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Order Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Order_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Order_Number',p_web.GetValue('NewValue'))
    job:Order_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Order_Number
    do Value::job:Order_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Order_Number',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Order_Number = p_web.GetValue('Value')
  End

Value::job:Order_Number  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Order_Number') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Order_Number
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Order_Number'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locExchangedIMEI  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('locExchangedIMEI') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Exchanged IMEI')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locExchangedIMEI  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locExchangedIMEI',p_web.GetValue('NewValue'))
    locExchangedIMEI = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locExchangedIMEI
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locExchangedIMEI',p_web.GetValue('Value'))
    locExchangedIMEI = p_web.GetValue('Value')
  End

Value::locExchangedIMEI  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('locExchangedIMEI') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locExchangedIMEI
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locExchangedIMEI'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locExchangeMSN  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('locExchangeMSN') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('MSN')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locExchangeMSN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locExchangeMSN',p_web.GetValue('NewValue'))
    locExchangeMSN = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locExchangeMSN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locExchangeMSN',p_web.GetValue('Value'))
    locExchangeMSN = p_web.GetValue('Value')
  End

Value::locExchangeMSN  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('locExchangeMSN') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locExchangeMSN
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locExchangeMSN'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locCustomerName  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('locCustomerName') & '_prompt',Choose(p_web.GSV('Hide:EndUserName') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Customer Name')
  If p_web.GSV('Hide:EndUserName') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locCustomerName  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locCustomerName',p_web.GetValue('NewValue'))
    locCustomerName = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locCustomerName
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locCustomerName',p_web.GetValue('Value'))
    locCustomerName = p_web.GetValue('Value')
  End

Value::locCustomerName  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('locCustomerName') & '_value',Choose(p_web.GSV('Hide:EndUserName') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:EndUserName') = 1)
  ! --- DISPLAY --- locCustomerName
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locCustomerName'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Prompt::job:ESN  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:ESN') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Final IMEI')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:ESN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:ESN',p_web.GetValue('NewValue'))
    job:ESN = p_web.GetValue('NewValue') !FieldType= STRING Field = job:ESN
    do Value::job:ESN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:ESN',p_web.dFormat(p_web.GetValue('Value'),'@s20'))
    job:ESN = p_web.GetValue('Value')
  End

Value::job:ESN  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:ESN') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:ESN
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:ESN'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::job:MSN  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:MSN') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('MSN')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:MSN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:MSN',p_web.GetValue('NewValue'))
    job:MSN = p_web.GetValue('NewValue') !FieldType= STRING Field = job:MSN
    do Value::job:MSN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:MSN',p_web.dFormat(p_web.GetValue('Value'),'@s20'))
    job:MSN = p_web.GetValue('Value')
  End

Value::job:MSN  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:MSN') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:MSN
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:MSN'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::spacer1  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('spacer1') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::spacer1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('spacer1',p_web.GetValue('NewValue'))
    do Value::spacer1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::spacer1  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('spacer1') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::loc2ndExchangeIMEI  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('loc2ndExchangeIMEI') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('2nd Exch IMEI')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::loc2ndExchangeIMEI  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('loc2ndExchangeIMEI',p_web.GetValue('NewValue'))
    loc2ndExchangeIMEI = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::loc2ndExchangeIMEI
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('loc2ndExchangeIMEI',p_web.GetValue('Value'))
    loc2ndExchangeIMEI = p_web.GetValue('Value')
  End

Value::loc2ndExchangeIMEI  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('loc2ndExchangeIMEI') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- loc2ndExchangeIMEI
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('loc2ndExchangeIMEI'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::loc2ndMSN  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('loc2ndMSN') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('MSN')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::loc2ndMSN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('loc2ndMSN',p_web.GetValue('NewValue'))
    loc2ndMSN = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::loc2ndMSN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('loc2ndMSN',p_web.GetValue('Value'))
    loc2ndMSN = p_web.GetValue('Value')
  End

Value::loc2ndMSN  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('loc2ndMSN') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- loc2ndMSN
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('loc2ndMSN'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::job:Transit_Type  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Transit_Type') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Transit Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Transit_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Transit_Type',p_web.GetValue('NewValue'))
    job:Transit_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Transit_Type
    do Value::job:Transit_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Transit_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Transit_Type = p_web.GetValue('Value')
  End

Value::job:Transit_Type  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Transit_Type') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Transit_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Transit_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::txtModelProductCode  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('txtModelProductCode') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Model No / Product Code')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::txtModelProductCode  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtModelProductCode',p_web.GetValue('NewValue'))
    do Value::txtModelProductCode
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtModelProductCode  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('txtModelProductCode') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(p_web.GSV('job:Model_Number') & ' ' & p_web.GSV('job:ProductCode'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::spacer2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('spacer2',p_web.GetValue('NewValue'))
    do Value::spacer2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::spacer2  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('spacer2') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::job:Manufacturer  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Manufacturer') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Manufacturer')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CustomerServicesForm_' & p_web._nocolon('job:Manufacturer') & '_prompt')

Validate::job:Manufacturer  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Manufacturer',p_web.GetValue('NewValue'))
    job:Manufacturer = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Manufacturer
    do Value::job:Manufacturer
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Manufacturer',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Manufacturer = p_web.GetValue('Value')
  End

Value::job:Manufacturer  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Manufacturer') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Manufacturer
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Manufacturer'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CustomerServicesForm_' & p_web._nocolon('job:Manufacturer') & '_value')


Prompt::job:Workshop  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Workshop') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Workshop')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Workshop  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Workshop',p_web.GetValue('NewValue'))
    job:Workshop = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Workshop
    do Value::job:Workshop
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Workshop',p_web.dFormat(p_web.GetValue('Value'),'@s3'))
    job:Workshop = p_web.GetValue('Value')
  End

Value::job:Workshop  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Workshop') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Workshop
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Workshop'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::job:Unit_Type  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Unit_Type') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Unit Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Unit_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Unit_Type',p_web.GetValue('NewValue'))
    job:Unit_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Unit_Type
    do Value::job:Unit_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Unit_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Unit_Type = p_web.GetValue('Value')
  End

Value::job:Unit_Type  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Unit_Type') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Unit_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Unit_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::jobe:Network  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('jobe:Network') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Network')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe:Network  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe:Network',p_web.GetValue('NewValue'))
    jobe:Network = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe:Network
    do Value::jobe:Network
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe:Network',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jobe:Network = p_web.GetValue('Value')
  End

Value::jobe:Network  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('jobe:Network') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- jobe:Network
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('jobe:Network'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::job:Mobile_Number  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Mobile_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Mobile Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Mobile_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Mobile_Number',p_web.GetValue('NewValue'))
    job:Mobile_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Mobile_Number
    do Value::job:Mobile_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Mobile_Number',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    job:Mobile_Number = p_web.GetValue('Value')
  End

Value::job:Mobile_Number  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Mobile_Number') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Mobile_Number
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Mobile_Number'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::job:DOP  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:DOP') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('DOP')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:DOP  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:DOP',p_web.GetValue('NewValue'))
    job:DOP = p_web.GetValue('NewValue') !FieldType= DATE Field = job:DOP
    do Value::job:DOP
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:DOP',p_web.dFormat(p_web.GetValue('Value'),'@d06b'))
    job:DOP = p_web.Dformat(p_web.GetValue('Value'),'@d06b') !
  End

Value::job:DOP  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:DOP') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:DOP
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(format(p_web.GetSessionValue('job:DOP'),'@d06b')) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::job:Location  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Location') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Internal Location')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Location  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Location',p_web.GetValue('NewValue'))
    job:Location = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Location
    do Value::job:Location
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Location',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Location = p_web.GetValue('Value')
  End

Value::job:Location  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Location') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Location
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Location'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::job:Turnaround_Time  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Turnaround_Time') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Job Turnaround Time')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Turnaround_Time  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Turnaround_Time',p_web.GetValue('NewValue'))
    job:Turnaround_Time = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Turnaround_Time
    do Value::job:Turnaround_Time
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Turnaround_Time',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Turnaround_Time = p_web.GetValue('Value')
  End

Value::job:Turnaround_Time  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Turnaround_Time') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Turnaround_Time
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Turnaround_Time'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::job:Third_Party_Site  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Third_Party_Site') & '_prompt',Choose(p_web.GSV('job:Third_Party_Site') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('3rd Party Site')
  If p_web.GSV('job:Third_Party_Site') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Third_Party_Site  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Third_Party_Site',p_web.GetValue('NewValue'))
    job:Third_Party_Site = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Third_Party_Site
    do Value::job:Third_Party_Site
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Third_Party_Site',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Third_Party_Site = p_web.GetValue('Value')
  End

Value::job:Third_Party_Site  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Third_Party_Site') & '_value',Choose(p_web.GSV('job:Third_Party_Site') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Third_Party_Site') = '')
  ! --- DISPLAY --- job:Third_Party_Site
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Third_Party_Site'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Prompt::job:Charge_Type  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Charge_Type') & '_prompt',Choose(p_web.GSV('job:Chargeable_Job') <> 'YES','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Cha Charge Type')
  If p_web.GSV('job:Chargeable_Job') <> 'YES'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Charge_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Charge_Type',p_web.GetValue('NewValue'))
    job:Charge_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Charge_Type
    do Value::job:Charge_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Charge_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Charge_Type = p_web.GetValue('Value')
  End

Value::job:Charge_Type  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Charge_Type') & '_value',Choose(p_web.GSV('job:Chargeable_Job') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Chargeable_Job') <> 'YES')
  ! --- DISPLAY --- job:Charge_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Charge_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Prompt::job:Special_Instructions  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Special_Instructions') & '_prompt',Choose(p_web.GSV('job:Special_Instructions') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Special Instructions')
  If p_web.GSV('job:Special_Instructions') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Special_Instructions  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Special_Instructions',p_web.GetValue('NewValue'))
    job:Special_Instructions = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Special_Instructions
    do Value::job:Special_Instructions
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Special_Instructions',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Special_Instructions = p_web.GetValue('Value')
  End

Value::job:Special_Instructions  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Special_Instructions') & '_value',Choose(p_web.GSV('job:Special_Instructions') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Special_Instructions') = '')
  ! --- DISPLAY --- job:Special_Instructions
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Special_Instructions'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Prompt::job:Warranty_Charge_Type  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Warranty_Charge_Type') & '_prompt',Choose(p_web.GSV('job:Warranty_Job') <> 'YES','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Warr Charge Type')
  If p_web.GSV('job:Warranty_Job') <> 'YES'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Warranty_Charge_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Warranty_Charge_Type',p_web.GetValue('NewValue'))
    job:Warranty_Charge_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Warranty_Charge_Type
    do Value::job:Warranty_Charge_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Warranty_Charge_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Warranty_Charge_Type = p_web.GetValue('Value')
  End

Value::job:Warranty_Charge_Type  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Warranty_Charge_Type') & '_value',Choose(p_web.GSV('job:Warranty_Job') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Warranty_Job') <> 'YES')
  ! --- DISPLAY --- job:Warranty_Charge_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Warranty_Charge_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Prompt::txtEstimate  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('txtEstimate') & '_prompt',Choose(p_web.GSV('txtEstimate') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Estimate')
  If p_web.GSV('txtEstimate') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::txtEstimate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtEstimate',p_web.GetValue('NewValue'))
    do Value::txtEstimate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtEstimate  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('txtEstimate') & '_value',Choose(p_web.GSV('txtEstimate') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('txtEstimate') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(p_web.GSV('txtEstimate'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Validate::textBouncer  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('textBouncer',p_web.GetValue('NewValue'))
    do Value::textBouncer
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::textBouncer  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('textBouncer') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold LargeText')&'">' & p_web.Translate(p_web.GSV('textBouncer'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::btnPreviousJobs  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnPreviousJobs',p_web.GetValue('NewValue'))
    do Value::btnPreviousJobs
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnPreviousJobs  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('btnPreviousJobs') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnPreviousJobs','Previous Unit History','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('BrowseIMEIHistory?' &'fromURL=CustomerServicesForm&currentJob=' & p_web.GSV('job:Ref_number'))) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()


Validate::__line1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('__line1',p_web.GetValue('NewValue'))
    do Value::__line1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::__line1  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('__line1') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locEngineer  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('locEngineer') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Engineer')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locEngineer  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locEngineer',p_web.GetValue('NewValue'))
    locEngineer = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locEngineer
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locEngineer',p_web.GetValue('Value'))
    locEngineer = p_web.GetValue('Value')
  End

Value::locEngineer  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('locEngineer') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locEngineer
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locEngineer'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::jbn:Engineers_Notes  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('jbn:Engineers_Notes') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Engineers Notes')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jbn:Engineers_Notes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jbn:Engineers_Notes',p_web.GetValue('NewValue'))
    jbn:Engineers_Notes = p_web.GetValue('NewValue') !FieldType= STRING Field = jbn:Engineers_Notes
    do Value::jbn:Engineers_Notes
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jbn:Engineers_Notes',p_web.dFormat(p_web.GetValue('Value'),'@s255'))
    jbn:Engineers_Notes = p_web.GetValue('Value')
  End
  do Value::jbn:Engineers_Notes
  do SendAlert

Value::jbn:Engineers_Notes  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('jbn:Engineers_Notes') & '_value','adiv')
  loc:extra = ''
  ! --- TEXT --- jbn:Engineers_Notes
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('jbn:Engineers_Notes')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:Engineers_Notes'',''customerservicesform_jbn:engineers_notes_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'readonly'
  packet = clip(packet) & p_web.CreateTextArea('jbn:Engineers_Notes',p_web.GetSessionValue('jbn:Engineers_Notes'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(jbn:Engineers_Notes),,Net:Web:Control) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CustomerServicesForm_' & p_web._nocolon('jbn:Engineers_Notes') & '_value')


Prompt::btnEngineerHistory  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('btnEngineerHistory') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::btnEngineerHistory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnEngineerHistory',p_web.GetValue('NewValue'))
    do Value::btnEngineerHistory
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnEngineerHistory  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('btnEngineerHistory') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnEngineerHistory','Engineer History','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('BrowseEngineerHistory?' &'ReturnURL=CustomerServicesForm')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()


Validate::_hidden1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('_hidden1',p_web.GetValue('NewValue'))
    do Value::_hidden1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::_hidden1  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('_hidden1') & '_value','adiv')
  loc:extra = ''
  ! --- HIDDEN --- 
    packet = clip(packet) & p_web.CreateInput('hidden','',p_web.GetSessionValue('')) & '<13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::_hidden2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('_hidden2',p_web.GetValue('NewValue'))
    do Value::_hidden2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::_hidden2  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('_hidden2') & '_value','adiv')
  loc:extra = ''
  ! --- HIDDEN --- 
    packet = clip(packet) & p_web.CreateInput('hidden','',p_web.GetSessionValue('')) & '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::job:Insurance_Reference_Number  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Insurance_Reference_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Insurance Ref No')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Insurance_Reference_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Insurance_Reference_Number',p_web.GetValue('NewValue'))
    job:Insurance_Reference_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Insurance_Reference_Number
    do Value::job:Insurance_Reference_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Insurance_Reference_Number',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Insurance_Reference_Number = p_web.GetValue('Value')
  End

Value::job:Insurance_Reference_Number  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Insurance_Reference_Number') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Insurance_Reference_Number
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Insurance_Reference_Number'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::job:Incoming_Courier  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Incoming_Courier') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Courier Incoming')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Incoming_Courier  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Incoming_Courier',p_web.GetValue('NewValue'))
    job:Incoming_Courier = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Incoming_Courier
    do Value::job:Incoming_Courier
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Incoming_Courier',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Incoming_Courier = p_web.GetValue('Value')
  End

Value::job:Incoming_Courier  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Incoming_Courier') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Incoming_Courier
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Incoming_Courier'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::jbn:Fault_Description  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('jbn:Fault_Description') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Fault Description')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jbn:Fault_Description  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jbn:Fault_Description',p_web.GetValue('NewValue'))
    jbn:Fault_Description = p_web.GetValue('NewValue') !FieldType= STRING Field = jbn:Fault_Description
    do Value::jbn:Fault_Description
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jbn:Fault_Description',p_web.dFormat(p_web.GetValue('Value'),'@s255'))
    jbn:Fault_Description = p_web.GetValue('Value')
  End
    jbn:Fault_Description = Upper(jbn:Fault_Description)
    p_web.SetSessionValue('jbn:Fault_Description',jbn:Fault_Description)
  do Value::jbn:Fault_Description
  do SendAlert

Value::jbn:Fault_Description  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('jbn:Fault_Description') & '_value','adiv')
  loc:extra = ''
  ! --- TEXT --- jbn:Fault_Description
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('jbn:Fault_Description')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:Fault_Description'',''customerservicesform_jbn:fault_description_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'readonly'
  packet = clip(packet) & p_web.CreateTextArea('jbn:Fault_Description',p_web.GetSessionValue('jbn:Fault_Description'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(jbn:Fault_Description),,Net:Web:Control) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CustomerServicesForm_' & p_web._nocolon('jbn:Fault_Description') & '_value')


Prompt::job:Courier  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Courier') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Outgoing Courier')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Courier  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Courier',p_web.GetValue('NewValue'))
    job:Courier = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Courier
    do Value::job:Courier
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Courier',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Courier = p_web.GetValue('Value')
  End

Value::job:Courier  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Courier') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Courier
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Courier'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::txtChargeableInvoiceNumber  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('txtChargeableInvoiceNumber') & '_prompt',Choose(p_web.GSV('job:Invoice_Number') = 0 AND p_web.GSV('job:Chargeable_Job') <> 'YES','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Char Invoice No')
  If p_web.GSV('job:Invoice_Number') = 0 AND p_web.GSV('job:Chargeable_Job') <> 'YES'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::txtChargeableInvoiceNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtChargeableInvoiceNumber',p_web.GetValue('NewValue'))
    do Value::txtChargeableInvoiceNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtChargeableInvoiceNumber  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('txtChargeableInvoiceNumber') & '_value',Choose(p_web.GSV('job:Invoice_Number') = 0 AND p_web.GSV('job:Chargeable_Job') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Invoice_Number') = 0 AND p_web.GSV('job:Chargeable_Job') <> 'YES')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(FORMAT(p_web.GSV('job:Invoice_Number'),@n_8b) & ' ' & FORMAT(p_web.GSV('job:Invoice_Date'),@d06b),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Prompt::txtWarrantyInvoiceNumber  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('txtWarrantyInvoiceNumber') & '_prompt',Choose(p_web.GSV('job:Invoice_Number_Warranty') = 0 OR p_web.GSV('job:Warranty_Job') <> 'YES','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Warr Invoice Number')
  If p_web.GSV('job:Invoice_Number_Warranty') = 0 OR p_web.GSV('job:Warranty_Job') <> 'YES'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::txtWarrantyInvoiceNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtWarrantyInvoiceNumber',p_web.GetValue('NewValue'))
    do Value::txtWarrantyInvoiceNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtWarrantyInvoiceNumber  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('txtWarrantyInvoiceNumber') & '_value',Choose(p_web.GSV('job:Invoice_Number_Warranty') = 0 OR p_web.GSV('job:Warranty_Job') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Invoice_Number_Warranty') = 0 OR p_web.GSV('job:Warranty_Job') <> 'YES')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(FORMAT(p_web.GSV('job:Invoice_Number_Warranty'),@n_8b) & ' ' & FORMAT(p_web.GSV('job:Invoice_Date_Warranty'),@d06b),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Validate::_hidden3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('_hidden3',p_web.GetValue('NewValue'))
    do Value::_hidden3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::_hidden3  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('_hidden3') & '_value','adiv')
  loc:extra = ''
  ! --- HIDDEN --- 
    packet = clip(packet) & p_web.CreateInput('hidden','',p_web.GetSessionValue('')) & '<13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::brwChargeableParts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwChargeableParts',p_web.GetValue('NewValue'))
    do Value::brwChargeableParts
  Else
    p_web.StoreValue('par:Record_Number')
  End

Value::brwChargeableParts  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseChargeableParts --
  p_web.SetValue('BrowseChargeableParts:NoForm',1)
  p_web.SetValue('BrowseChargeableParts:FormName',loc:formname)
  p_web.SetValue('BrowseChargeableParts:parentIs','Form')
  p_web.SetValue('_parentProc','CustomerServicesForm')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('CustomerServicesForm_BrowseChargeableParts_embedded_div')&'"><!-- Net:BrowseChargeableParts --></div><13,10>'
    p_web._DivHeader('CustomerServicesForm_' & lower('BrowseChargeableParts') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('CustomerServicesForm_' & lower('BrowseChargeableParts') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseChargeableParts --><13,10>'
  end
  do SendPacket


Validate::brwWarrantyParts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwWarrantyParts',p_web.GetValue('NewValue'))
    do Value::brwWarrantyParts
  Else
    p_web.StoreValue('wpr:Record_Number')
  End

Value::brwWarrantyParts  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseWarrantyParts --
  p_web.SetValue('BrowseWarrantyParts:NoForm',1)
  p_web.SetValue('BrowseWarrantyParts:FormName',loc:formname)
  p_web.SetValue('BrowseWarrantyParts:parentIs','Form')
  p_web.SetValue('_parentProc','CustomerServicesForm')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('CustomerServicesForm_BrowseWarrantyParts_embedded_div')&'"><!-- Net:BrowseWarrantyParts --></div><13,10>'
    p_web._DivHeader('CustomerServicesForm_' & lower('BrowseWarrantyParts') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('CustomerServicesForm_' & lower('BrowseWarrantyParts') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseWarrantyParts --><13,10>'
  end
  do SendPacket


Prompt::FranchiseAccount  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('FranchiseAccount') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Account Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::FranchiseAccount  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('FranchiseAccount',p_web.GetValue('NewValue'))
    FranchiseAccount = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::FranchiseAccount
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('FranchiseAccount',p_web.GetValue('Value'))
    FranchiseAccount = p_web.GetValue('Value')
  End
  do Value::FranchiseAccount
  do SendAlert
  do Prompt::job:Account_Number_Franchise
  do Value::job:Account_Number_Franchise  !1
  do Prompt::job:Account_Number_Generic
  do Value::job:Account_Number_Generic  !1

Value::FranchiseAccount  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('FranchiseAccount') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- FranchiseAccount
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('FranchiseAccount')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('FranchiseAccount') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''FranchiseAccount'',''customerservicesform_franchiseaccount_value'',1,'''&clip(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('FranchiseAccount')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','FranchiseAccount',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'FranchiseAccount_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Franchise') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('FranchiseAccount') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''FranchiseAccount'',''customerservicesform_franchiseaccount_value'',1,'''&clip(2)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('FranchiseAccount')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','FranchiseAccount',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'FranchiseAccount_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Generic') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CustomerServicesForm_' & p_web._nocolon('FranchiseAccount') & '_value')


Prompt::job:Account_Number_Franchise  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Account_Number_Franchise') & '_prompt',Choose(p_web.GSV('FranchiseAccount') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Franchise Account No')
  If p_web.GSV('FranchiseAccount') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CustomerServicesForm_' & p_web._nocolon('job:Account_Number_Franchise') & '_prompt')

Validate::job:Account_Number_Franchise  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Account_Number_Franchise',p_web.GetValue('NewValue'))
    job:Account_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Account_Number
    do Value::job:Account_Number_Franchise
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Account_Number',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    job:Account_Number = p_web.GetValue('Value')
  End
  If job:Account_Number = ''
    loc:Invalid = 'job:Account_Number'
    loc:alert = p_web.translate('Franchise Account No') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    job:Account_Number = Upper(job:Account_Number)
    p_web.SetSessionValue('job:Account_Number',job:Account_Number)
  p_Web.SetValue('lookupfield','job:Account_Number_Franchise')
  do AfterLookup
  do Value::job:Account_Number_Franchise
  do SendAlert

Value::job:Account_Number_Franchise  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Account_Number_Franchise') & '_value',Choose(p_web.GSV('FranchiseAccount') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('FranchiseAccount') <> 1)
  ! --- STRING --- job:Account_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:AccountNumber') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:AccountNumber') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('job:Account_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If job:Account_Number = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(19) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Account_Number_Franchise'',''customerservicesform_job:account_number_franchise_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Account_Number')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','job:Account_Number',p_web.GetSessionValue('job:Account_Number'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s15',loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('LookupRRCAccounts?LookupField=job:Account_Number&Tab=3&ForeignField=sub:Account_Number&_sort=sub:Account_Number&Refresh=sort&LookupFrom=CustomerServicesForm&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('CustomerServicesForm_' & p_web._nocolon('job:Account_Number_Franchise') & '_value')


Prompt::job:ESN_Edit  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:ESN_Edit') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('I.M.E.I. Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:ESN_Edit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:ESN_Edit',p_web.GetValue('NewValue'))
    job:ESN = p_web.GetValue('NewValue') !FieldType= STRING Field = job:ESN
    do Value::job:ESN_Edit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:ESN',p_web.dFormat(p_web.GetValue('Value'),'@s20'))
    job:ESN = p_web.GetValue('Value')
  End
  If job:ESN = '' and p_web.GSV('Req:IMEI') = 1
    loc:Invalid = 'job:ESN'
    loc:alert = p_web.translate('I.M.E.I. Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    job:ESN = Upper(job:ESN)
    p_web.SetSessionValue('job:ESN',job:ESN)
    IF (p_web.GSV('job:ESN') <> p_web.GSV('save:IMEINumber'))
        IF (IsIMEIValid(p_web.GSV('job:ESN'),p_web.GSV('job:Model_Number'),loc:alert))
            p_web.SSV('job:ESN',p_web.GSV('save:IMEINumber'))
        ELSE
            p_web.SSV('save:IMEINumber',p_web.GSV('job:ESN'))
        END ! IF
    END ! IF    
  do Value::job:ESN_Edit
  do SendAlert

Value::job:ESN_Edit  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:ESN_Edit') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:ESN
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:IMEINumber') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:IMEINumber') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:IMEI') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('job:ESN')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If job:ESN = '' and (p_web.GSV('Req:IMEI') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(25) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&clip('areYouSureYouWantToChange(this,''' & p_web.GSV('save:IMEINumber') & ''',''IMEI Number'')')&';'
  loc:javascript = clip(loc:javascript) & ' ' &p_web._nocolon('sv(''job:ESN_Edit'',''customerservicesform_job:esn_edit_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:ESN',p_web.GetSessionValueFormat('job:ESN'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s20'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CustomerServicesForm_' & p_web._nocolon('job:ESN_Edit') & '_value')


Prompt::job:Account_Number_Generic  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Account_Number_Generic') & '_prompt',Choose(p_web.GSV('FranchiseAccount') <> 2,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Generic Account No')
  If p_web.GSV('FranchiseAccount') <> 2
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CustomerServicesForm_' & p_web._nocolon('job:Account_Number_Generic') & '_prompt')

Validate::job:Account_Number_Generic  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Account_Number_Generic',p_web.GetValue('NewValue'))
    job:Account_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Account_Number
    do Value::job:Account_Number_Generic
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Account_Number',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    job:Account_Number = p_web.GetValue('Value')
  End
  If job:Account_Number = ''
    loc:Invalid = 'job:Account_Number'
    loc:alert = p_web.translate('Generic Account No') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    job:Account_Number = Upper(job:Account_Number)
    p_web.SetSessionValue('job:Account_Number',job:Account_Number)
  p_Web.SetValue('lookupfield','job:Account_Number_Generic')
  do AfterLookup
  do Value::job:Account_Number_Generic
  do SendAlert

Value::job:Account_Number_Generic  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Account_Number_Generic') & '_value',Choose(p_web.GSV('FranchiseAccount') <> 2,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('FranchiseAccount') <> 2)
  ! --- STRING --- job:Account_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:AccountNumber') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:AccountNumber') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('job:Account_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If job:Account_Number = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(19) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Account_Number_Generic'',''customerservicesform_job:account_number_generic_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Account_Number')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','job:Account_Number',p_web.GetSessionValue('job:Account_Number'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s15',loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('LookupGenericAccounts?LookupField=job:Account_Number&Tab=3&ForeignField=TRA1:SubAcc&_sort=TRA1:SubAcc&Refresh=sort&LookupFrom=CustomerServicesForm&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('CustomerServicesForm_' & p_web._nocolon('job:Account_Number_Generic') & '_value')


Prompt::job:MSN_Edit  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:MSN_Edit') & '_prompt',Choose(p_web.GSV('Hide:MSN') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('M.S.N.')
  If p_web.GSV('Hide:MSN') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:MSN_Edit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:MSN_Edit',p_web.GetValue('NewValue'))
    job:MSN = p_web.GetValue('NewValue') !FieldType= STRING Field = job:MSN
    do Value::job:MSN_Edit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:MSN',p_web.dFormat(p_web.GetValue('Value'),'@s20'))
    job:MSN = p_web.GetValue('Value')
  End
    job:MSN = Upper(job:MSN)
    p_web.SetSessionValue('job:MSN',job:MSN)
  do Value::job:MSN_Edit
  do SendAlert

Value::job:MSN_Edit  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:MSN_Edit') & '_value',Choose(p_web.GSV('Hide:MSN') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:MSN') = 1)
  ! --- STRING --- job:MSN
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('job:MSN')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(25) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:MSN_Edit'',''customerservicesform_job:msn_edit_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:MSN',p_web.GetSessionValueFormat('job:MSN'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s20'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('CustomerServicesForm_' & p_web._nocolon('job:MSN_Edit') & '_value')


Prompt::job:Order_Number_edit  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Order_Number_edit') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Order Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Order_Number_edit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Order_Number_edit',p_web.GetValue('NewValue'))
    job:Order_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Order_Number
    do Value::job:Order_Number_edit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Order_Number',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Order_Number = p_web.GetValue('Value')
  End
  If job:Order_Number = '' and p_web.GSV('Req:OrderNumber') = 1
    loc:Invalid = 'job:Order_Number'
    loc:alert = p_web.translate('Order Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    job:Order_Number = Upper(job:Order_Number)
    p_web.SetSessionValue('job:Order_Number',job:Order_Number)
  do Value::job:Order_Number_edit
  do SendAlert

Value::job:Order_Number_edit  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Order_Number_edit') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Order_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If (p_web.GSV('Req:OrderNumber') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('job:Order_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If job:Order_Number = '' and (p_web.GSV('Req:OrderNumber') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(25) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Order_Number_edit'',''customerservicesform_job:order_number_edit_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Order_Number',p_web.GetSessionValueFormat('job:Order_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CustomerServicesForm_' & p_web._nocolon('job:Order_Number_edit') & '_value')


Prompt::job:ProductCode_Edit  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:ProductCode_Edit') & '_prompt',Choose(p_web.GSV('Hide:ProductCode'),'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Product Code')
  If p_web.GSV('Hide:ProductCode')
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:ProductCode_Edit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:ProductCode_Edit',p_web.GetValue('NewValue'))
    job:ProductCode = p_web.GetValue('NewValue') !FieldType= STRING Field = job:ProductCode
    do Value::job:ProductCode_Edit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:ProductCode',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:ProductCode = p_web.GetValue('Value')
  End
    job:ProductCode = Upper(job:ProductCode)
    p_web.SetSessionValue('job:ProductCode',job:ProductCode)
  p_Web.SetValue('lookupfield','job:ProductCode_Edit')
  do AfterLookup
  do Value::job:ProductCode_Edit
  do SendAlert

Value::job:ProductCode_Edit  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:ProductCode_Edit') & '_value',Choose(p_web.GSV('Hide:ProductCode'),'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ProductCode'))
  ! --- STRING --- job:ProductCode
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('job:ProductCode')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(25) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:ProductCode_Edit'',''customerservicesform_job:productcode_edit_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:ProductCode')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','job:ProductCode',p_web.GetSessionValue('job:ProductCode'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('LookupProductCodes?LookupField=job:ProductCode&Tab=3&ForeignField=mop:ProductCode&_sort=&Refresh=sort&LookupFrom=CustomerServicesForm&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('CustomerServicesForm_' & p_web._nocolon('job:ProductCode_Edit') & '_value')


Prompt::job:Title_Edit  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Title_Edit') & '_prompt',Choose(p_web.GSV('Hide:EndUser') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('End User Title')
  If p_web.GSV('Hide:EndUser') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Title_Edit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Title_Edit',p_web.GetValue('NewValue'))
    job:Title = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Title
    do Value::job:Title_Edit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Title',p_web.dFormat(p_web.GetValue('Value'),'@s4'))
    job:Title = p_web.GetValue('Value')
  End
    job:Title = Upper(job:Title)
    p_web.SetSessionValue('job:Title',job:Title)
  do Value::job:Title_Edit
  do SendAlert

Value::job:Title_Edit  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Title_Edit') & '_value',Choose(p_web.GSV('Hide:EndUser') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:EndUser') = 1)
  ! --- STRING --- job:Title
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('job:Title')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(8) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Title_Edit'',''customerservicesform_job:title_edit_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Title',p_web.GetSessionValueFormat('job:Title'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s4'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('CustomerServicesForm_' & p_web._nocolon('job:Title_Edit') & '_value')


Prompt::job:Initial_Edit  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Initial_Edit') & '_prompt',Choose(p_web.GSV('Hide:EndUser') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Initial')
  If p_web.GSV('Hide:EndUser') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Initial_Edit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Initial_Edit',p_web.GetValue('NewValue'))
    job:Initial = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Initial
    do Value::job:Initial_Edit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Initial',p_web.dFormat(p_web.GetValue('Value'),'@s1'))
    job:Initial = p_web.GetValue('Value')
  End
    job:Initial = Upper(job:Initial)
    p_web.SetSessionValue('job:Initial',job:Initial)
  do Value::job:Initial_Edit
  do SendAlert

Value::job:Initial_Edit  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Initial_Edit') & '_value',Choose(p_web.GSV('Hide:EndUser') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:EndUser') = 1)
  ! --- STRING --- job:Initial
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('job:Initial')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(3) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Initial_Edit'',''customerservicesform_job:initial_edit_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Initial',p_web.GetSessionValueFormat('job:Initial'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s1'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('CustomerServicesForm_' & p_web._nocolon('job:Initial_Edit') & '_value')


Prompt::job:Model_Number_Edit  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Model_Number_Edit') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Model Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Model_Number_Edit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Model_Number_Edit',p_web.GetValue('NewValue'))
    job:Model_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Model_Number
    do Value::job:Model_Number_Edit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Model_Number',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Model_Number = p_web.GetValue('Value')
  End
  If job:Model_Number = '' and p_web.GSV('Req:ModelNumber') = 1
    loc:Invalid = 'job:Model_Number'
    loc:alert = p_web.translate('Model Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    job:Model_Number = Upper(job:Model_Number)
    p_web.SetSessionValue('job:Model_Number',job:Model_Number)
  p_Web.SetValue('lookupfield','job:Model_Number_Edit')
  do AfterLookup
  do Value::job:Manufacturer
  do Value::job:Model_Number_Edit
  do SendAlert

Value::job:Model_Number_Edit  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Model_Number_Edit') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Model_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If (p_web.GSV('Req:ModelNumber') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('job:Model_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If job:Model_Number = '' and (p_web.GSV('Req:ModelNumber') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(19) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Model_Number_Edit'',''customerservicesform_job:model_number_edit_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Model_Number')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','job:Model_Number',p_web.GetSessionValue('job:Model_Number'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('SelectModelNumbers?LookupField=job:Model_Number&Tab=3&ForeignField=mod:Model_Number&_sort=&Refresh=sort&LookupFrom=CustomerServicesForm&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CustomerServicesForm_' & p_web._nocolon('job:Model_Number_Edit') & '_value')


Prompt::job:Surname_Edit  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Surname_Edit') & '_prompt',Choose(p_web.GSV('Hide:EndUser') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Surname')
  If p_web.GSV('Hide:EndUser') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Surname_Edit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Surname_Edit',p_web.GetValue('NewValue'))
    job:Surname = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Surname
    do Value::job:Surname_Edit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Surname',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Surname = p_web.GetValue('Value')
  End
  If job:Surname = '' and p_web.GSV('Req:Surname') = 1
    loc:Invalid = 'job:Surname'
    loc:alert = p_web.translate('Surname') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    job:Surname = Upper(job:Surname)
    p_web.SetSessionValue('job:Surname',job:Surname)
  do Value::job:Surname_Edit
  do SendAlert

Value::job:Surname_Edit  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Surname_Edit') & '_value',Choose(p_web.GSV('Hide:EndUser') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:EndUser') = 1)
  ! --- STRING --- job:Surname
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If (p_web.GSV('Req:Surname') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('job:Surname')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If job:Surname = '' and (p_web.GSV('Req:Surname') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(25) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Surname_Edit'',''customerservicesform_job:surname_edit_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Surname',p_web.GetSessionValueFormat('job:Surname'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('CustomerServicesForm_' & p_web._nocolon('job:Surname_Edit') & '_value')


Prompt::job:Unit_Type_Edit  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Unit_Type_Edit') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Unit Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Unit_Type_Edit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Unit_Type_Edit',p_web.GetValue('NewValue'))
    job:Unit_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Unit_Type
    do Value::job:Unit_Type_Edit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Unit_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Unit_Type = p_web.GetValue('Value')
  End
  If job:Unit_Type = '' and p_web.GSV('Req:UnitType') = 1
    loc:Invalid = 'job:Unit_Type'
    loc:alert = p_web.translate('Unit Type') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    job:Unit_Type = Upper(job:Unit_Type)
    p_web.SetSessionValue('job:Unit_Type',job:Unit_Type)
  p_Web.SetValue('lookupfield','job:Unit_Type_Edit')
  do AfterLookup
  do Value::job:Unit_Type_Edit
  do SendAlert

Value::job:Unit_Type_Edit  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Unit_Type_Edit') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Unit_Type
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If (p_web.GSV('Req:UnitType') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('job:Unit_Type')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If job:Unit_Type = '' and (p_web.GSV('Req:UnitType') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(19) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Unit_Type_Edit'',''customerservicesform_job:unit_type_edit_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Unit_Type')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','job:Unit_Type',p_web.GetSessionValue('job:Unit_Type'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('SelectUnitTypes?LookupField=job:Unit_Type&Tab=3&ForeignField=uni:Unit_Type&_sort=&Refresh=sort&LookupFrom=CustomerServicesForm&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CustomerServicesForm_' & p_web._nocolon('job:Unit_Type_Edit') & '_value')


Prompt::jobe:EndUserTelNo  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('jobe:EndUserTelNo') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('End User Tel No')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe:EndUserTelNo  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe:EndUserTelNo',p_web.GetValue('NewValue'))
    jobe:EndUserTelNo = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe:EndUserTelNo
    do Value::jobe:EndUserTelNo
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe:EndUserTelNo',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    jobe:EndUserTelNo = p_web.GetValue('Value')
  End
    jobe:EndUserTelNo = Upper(jobe:EndUserTelNo)
    p_web.SetSessionValue('jobe:EndUserTelNo',jobe:EndUserTelNo)
  do Value::jobe:EndUserTelNo
  do SendAlert

Value::jobe:EndUserTelNo  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('jobe:EndUserTelNo') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- jobe:EndUserTelNo
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jobe:EndUserTelNo')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(25) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe:EndUserTelNo'',''customerservicesform_jobe:endusertelno_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe:EndUserTelNo',p_web.GetSessionValueFormat('jobe:EndUserTelNo'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CustomerServicesForm_' & p_web._nocolon('jobe:EndUserTelNo') & '_value')


Prompt::job:Colour  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Colour') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Colour')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Colour  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Colour',p_web.GetValue('NewValue'))
    job:Colour = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Colour
    do Value::job:Colour
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Colour',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Colour = p_web.GetValue('Value')
  End
  If job:Colour = '' and p_web.GSV('Req:Colour') = 1
    loc:Invalid = 'job:Colour'
    loc:alert = p_web.translate('Colour') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    job:Colour = Upper(job:Colour)
    p_web.SetSessionValue('job:Colour',job:Colour)
  p_Web.SetValue('lookupfield','job:Colour')
  do AfterLookup
  do Value::job:Colour
  do SendAlert

Value::job:Colour  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Colour') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Colour
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If (p_web.GSV('Req:Colour') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('job:Colour')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If job:Colour = '' and (p_web.GSV('Req:Colour') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(19) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Colour'',''customerservicesform_job:colour_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Colour')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','job:Colour',p_web.GetSessionValue('job:Colour'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('SelectColours?LookupField=job:Colour&Tab=3&ForeignField=moc:Colour&_sort=&Refresh=sort&LookupFrom=CustomerServicesForm&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CustomerServicesForm_' & p_web._nocolon('job:Colour') & '_value')


Prompt::job:Transit_Type_Edit  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Transit_Type_Edit') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Transit Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Transit_Type_Edit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Transit_Type_Edit',p_web.GetValue('NewValue'))
    job:Transit_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Transit_Type
    do Value::job:Transit_Type_Edit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Transit_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Transit_Type = p_web.GetValue('Value')
  End
  If job:Transit_Type = ''
    loc:Invalid = 'job:Transit_Type'
    loc:alert = p_web.translate('Transit Type') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    job:Transit_Type = Upper(job:Transit_Type)
    p_web.SetSessionValue('job:Transit_Type',job:Transit_Type)
  p_Web.SetValue('lookupfield','job:Transit_Type_Edit')
  do AfterLookup
  do Value::job:Transit_Type_Edit
  do SendAlert

Value::job:Transit_Type_Edit  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Transit_Type_Edit') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Transit_Type
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('job:Transit_Type')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If job:Transit_Type = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(19) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Transit_Type_Edit'',''customerservicesform_job:transit_type_edit_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Transit_Type')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','job:Transit_Type',p_web.GetSessionValue('job:Transit_Type'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('SelectTransitTypes?LookupField=job:Transit_Type&Tab=3&ForeignField=trt:Transit_Type&_sort=&Refresh=sort&LookupFrom=CustomerServicesForm&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CustomerServicesForm_' & p_web._nocolon('job:Transit_Type_Edit') & '_value')


Prompt::job:DOP_Edit  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:DOP_Edit') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Date Of Purchase')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:DOP_Edit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:DOP_Edit',p_web.GetValue('NewValue'))
    job:DOP = p_web.GetValue('NewValue') !FieldType= DATE Field = job:DOP
    do Value::job:DOP_Edit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:DOP',p_web.dFormat(p_web.GetValue('Value'),'@d06b'))
    job:DOP = p_web.Dformat(p_web.GetValue('Value'),'@d06b') !
  End
  If job:DOP = '' and p_web.GSV('Req:DOP') = 1
    loc:Invalid = 'job:DOP'
    loc:alert = p_web.translate('Date Of Purchase') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    IF (ValidateDOP(loc:Alert) = 0)
        p_web.SSV('save:DOP',p_web.GSV('job:DOP'))
    ELSE
        p_web.SSV('job:DOP',p_web.GSV('save:DOP'))
    END ! IF
  do Value::job:DOP_Edit
  do SendAlert

Value::job:DOP_Edit  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:DOP_Edit') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:DOP
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If (p_web.GSV('Req:DOP') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('job:DOP')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If job:DOP = '' and (p_web.GSV('Req:DOP') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(19) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:DOP_Edit'',''customerservicesform_job:dop_edit_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:DOP')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:DOP',p_web.GetSessionValue('job:DOP'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06b',loc:javascript,,) & '<13,10>'
  do SendPacket
  packet = clip(packet) & '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar' & |
                  '(job__DOP,''dd/mm/yyyy'',this); Date.disabled=true;' & |
                  'sv(''...'',''CustomerServicesForm_pickdate_value'',1,FieldValue(this,1));nextFocus(CustomerServicesForm_frm,'''',0);" ' & |
                  'value="Select Date" name="Date" type="button">...</button>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CustomerServicesForm_' & p_web._nocolon('job:DOP_Edit') & '_value')


Prompt::job:Mobile_Number_Edit  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Mobile_Number_Edit') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Mobile Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Mobile_Number_Edit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Mobile_Number_Edit',p_web.GetValue('NewValue'))
    job:Mobile_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Mobile_Number
    do Value::job:Mobile_Number_Edit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Mobile_Number',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    job:Mobile_Number = p_web.GetValue('Value')
  End
  If job:Mobile_Number = '' and p_web.GSV('Req:MobileNumber') = 1
    loc:Invalid = 'job:Mobile_Number'
    loc:alert = p_web.translate('Mobile Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    job:Mobile_Number = Upper(job:Mobile_Number)
    p_web.SetSessionValue('job:Mobile_Number',job:Mobile_Number)
  do Value::job:Mobile_Number_Edit
  do SendAlert

Value::job:Mobile_Number_Edit  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Mobile_Number_Edit') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Mobile_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If (p_web.GSV('Req:MobileNumber') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('job:Mobile_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If job:Mobile_Number = '' and (p_web.GSV('Req:MobileNumber') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(25) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Mobile_Number_Edit'',''customerservicesform_job:mobile_number_edit_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Mobile_Number',p_web.GetSessionValueFormat('job:Mobile_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CustomerServicesForm_' & p_web._nocolon('job:Mobile_Number_Edit') & '_value')


Prompt::job:Turnaround_Time_Edit  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Turnaround_Time_Edit') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Turnaround Time')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Turnaround_Time_Edit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Turnaround_Time_Edit',p_web.GetValue('NewValue'))
    job:Turnaround_Time = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Turnaround_Time
    do Value::job:Turnaround_Time_Edit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Turnaround_Time',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Turnaround_Time = p_web.GetValue('Value')
  End
    job:Turnaround_Time = Upper(job:Turnaround_Time)
    p_web.SetSessionValue('job:Turnaround_Time',job:Turnaround_Time)
  p_Web.SetValue('lookupfield','job:Turnaround_Time_Edit')
  do AfterLookup
  do Value::job:Turnaround_Time_Edit
  do SendAlert

Value::job:Turnaround_Time_Edit  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Turnaround_Time_Edit') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Turnaround_Time
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('job:Turnaround_Time')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(19) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Turnaround_Time_Edit'',''customerservicesform_job:turnaround_time_edit_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Turnaround_Time')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','job:Turnaround_Time',p_web.GetSessionValue('job:Turnaround_Time'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('LookupTurnaroundTime?LookupField=job:Turnaround_Time&Tab=3&ForeignField=tur:Turnaround_Time&_sort=&Refresh=sort&LookupFrom=CustomerServicesForm&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CustomerServicesForm_' & p_web._nocolon('job:Turnaround_Time_Edit') & '_value')


Prompt::job:Third_Party_Site_Edit  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Third_Party_Site_Edit') & '_prompt',Choose(p_web.GSV('job:Third_Party_Site') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Third Party Repair Site')
  If p_web.GSV('job:Third_Party_Site') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Third_Party_Site_Edit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Third_Party_Site_Edit',p_web.GetValue('NewValue'))
    job:Third_Party_Site = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Third_Party_Site
    do Value::job:Third_Party_Site_Edit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Third_Party_Site',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Third_Party_Site = p_web.GetValue('Value')
  End

Value::job:Third_Party_Site_Edit  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Third_Party_Site_Edit') & '_value',Choose(p_web.GSV('job:Third_Party_Site') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Third_Party_Site') = '')
  ! --- DISPLAY --- job:Third_Party_Site
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Third_Party_Site'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Prompt::job:ThirdPartyDateDesp  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:ThirdPartyDateDesp') & '_prompt',Choose(p_web.GSV('job:Third_Party_Site') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Despatch Date')
  If p_web.GSV('job:Third_Party_Site') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:ThirdPartyDateDesp  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:ThirdPartyDateDesp',p_web.GetValue('NewValue'))
    job:ThirdPartyDateDesp = p_web.GetValue('NewValue') !FieldType= DATE Field = job:ThirdPartyDateDesp
    do Value::job:ThirdPartyDateDesp
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:ThirdPartyDateDesp',p_web.dformat(clip(p_web.GetValue('Value')),p_web.site.DatePicture))
    job:ThirdPartyDateDesp = p_web.dformat(clip(p_web.GetValue('Value')),p_web.site.DatePicture)
  End
  do Value::job:ThirdPartyDateDesp
  do SendAlert

Value::job:ThirdPartyDateDesp  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:ThirdPartyDateDesp') & '_value',Choose(p_web.GSV('job:Third_Party_Site') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Third_Party_Site') = '')
  ! --- DATE --- job:ThirdPartyDateDesp
    loc:AutoComplete = 'autocomplete="off"'
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('job:ThirdPartyDateDesp')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:ThirdPartyDateDesp'',''customerservicesform_job:thirdpartydatedesp_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:lookuponly = ''
  packet = clip(packet) & p_web.CreateInput('text','job:ThirdPartyDateDesp',p_web.GetSessionValue('job:ThirdPartyDateDesp'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.site.datepicture,loc:javascript,,) & '<13,10>'
  if not loc:readonly
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' '&p_web._nocolon('sv(''job:ThirdPartyDateDesp'',''customerservicesform_job:thirdpartydatedesp_value'',1,FieldValue(this,1))')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:DateLookupButton,loc:formname,,,|
    'sd('''&clip(loc:formName)&''','''&p_web._nocolon('job:ThirdPartyDateDesp')&''','''&clip(upper(p_web.site.datepicture))&''','&lower(p_web._nocolon('''CustomerServicesForm_job:ThirdPartyDateDesp_value'''))&');','onfocus="rad();" ')

  End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('CustomerServicesForm_' & p_web._nocolon('job:ThirdPartyDateDesp') & '_value')


Prompt::job:Date_Paid  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Date_Paid') & '_prompt',Choose(p_web.GSV('job:Third_Party_Site') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Returned')
  If p_web.GSV('job:Third_Party_Site') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Date_Paid  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Date_Paid',p_web.GetValue('NewValue'))
    job:Date_Paid = p_web.GetValue('NewValue') !FieldType= DATE Field = job:Date_Paid
    do Value::job:Date_Paid
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Date_Paid',p_web.dformat(clip(p_web.GetValue('Value')),p_web.site.DatePicture))
    job:Date_Paid = p_web.dformat(clip(p_web.GetValue('Value')),p_web.site.DatePicture)
  End
  do Value::job:Date_Paid
  do SendAlert

Value::job:Date_Paid  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('job:Date_Paid') & '_value',Choose(p_web.GSV('job:Third_Party_Site') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Third_Party_Site') = '')
  ! --- DATE --- job:Date_Paid
    loc:AutoComplete = 'autocomplete="off"'
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('job:Date_Paid')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Date_Paid'',''customerservicesform_job:date_paid_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:lookuponly = ''
  packet = clip(packet) & p_web.CreateInput('text','job:Date_Paid',p_web.GetSessionValue('job:Date_Paid'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.site.datepicture,loc:javascript,,) & '<13,10>'
  if not loc:readonly
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' '&p_web._nocolon('sv(''job:Date_Paid'',''customerservicesform_job:date_paid_value'',1,FieldValue(this,1))')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:DateLookupButton,loc:formname,,,|
    'sd('''&clip(loc:formName)&''','''&p_web._nocolon('job:Date_Paid')&''','''&clip(upper(p_web.site.datepicture))&''','&lower(p_web._nocolon('''CustomerServicesForm_job:Date_Paid_value'''))&');','onfocus="rad();" ')

  End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('CustomerServicesForm_' & p_web._nocolon('job:Date_Paid') & '_value')


Prompt::jobe:VSACustomer  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('jobe:VSACustomer') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('VSA Customer')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe:VSACustomer  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe:VSACustomer',p_web.GetValue('NewValue'))
    jobe:VSACustomer = p_web.GetValue('NewValue') !FieldType= BYTE Field = jobe:VSACustomer
    do Value::jobe:VSACustomer
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('jobe:VSACustomer',p_web.GetValue('Value'))
    jobe:VSACustomer = p_web.GetValue('Value')
  End
    IF (p_web.GSV('jobe:VSACustomer') = 1)
        IF (CheckLength('MOBILE','',p_web.GSV('job:Mobile_Number')))
            loc:Alert = 'Invalid Mobile Number Length'
            loc:Invalid = 'job:Mobile_Number'
            p_web.SSV('jobe:VSACustomer',0)
        ELSE ! IF
            CID_XML(p_web.GSV('job:Mobile_Number'),p_web.GSV('wob:HeadAccountNumber'),1)
        END ! IF
    END ! IF  
  do Value::jobe:VSACustomer
  do SendAlert

Value::jobe:VSACustomer  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('jobe:VSACustomer') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- jobe:VSACustomer
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe:VSACustomer'',''customerservicesform_jobe:vsacustomer_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe:VSACustomer')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('jobe:VSACustomer') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','jobe:VSACustomer',clip(1),,loc:readonly,,,loc:javascript,,'VSA Customer') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CustomerServicesForm_' & p_web._nocolon('jobe:VSACustomer') & '_value')


Prompt::jbn:Fault_Description_edit  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('jbn:Fault_Description_edit') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Fault Description')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jbn:Fault_Description_edit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jbn:Fault_Description_edit',p_web.GetValue('NewValue'))
    jbn:Fault_Description = p_web.GetValue('NewValue') !FieldType= STRING Field = jbn:Fault_Description
    do Value::jbn:Fault_Description_edit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jbn:Fault_Description',p_web.dFormat(p_web.GetValue('Value'),'@s255'))
    jbn:Fault_Description = p_web.GetValue('Value')
  End
    jbn:Fault_Description = Upper(jbn:Fault_Description)
    p_web.SetSessionValue('jbn:Fault_Description',jbn:Fault_Description)
  do Value::jbn:Fault_Description_edit
  do SendAlert

Value::jbn:Fault_Description_edit  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('jbn:Fault_Description_edit') & '_value','adiv')
  loc:extra = ''
  ! --- TEXT --- jbn:Fault_Description
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jbn:Fault_Description')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:Fault_Description_edit'',''customerservicesform_jbn:fault_description_edit_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('jbn:Fault_Description',p_web.GetSessionValue('jbn:Fault_Description'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(jbn:Fault_Description),,Net:Web:Control) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CustomerServicesForm_' & p_web._nocolon('jbn:Fault_Description_edit') & '_value')


Validate::btnFaultCodes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnFaultCodes',p_web.GetValue('NewValue'))
    do Value::btnFaultCodes
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnFaultCodes  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('btnFaultCodes') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnFaultCodes','Fault Codes','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('ViewFaultCodes')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()


Validate::btnConsignmentHistory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnConsignmentHistory',p_web.GetValue('NewValue'))
    do Value::btnConsignmentHistory
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnConsignmentHistory  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('btnConsignmentHistory') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnConsignmentHistory','Consignment History','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('BrowseConsignmentHistory')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()


Validate::btnThirdPartyDetails  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnThirdPartyDetails',p_web.GetValue('NewValue'))
    do Value::btnThirdPartyDetails
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnThirdPartyDetails  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('btnThirdPartyDetails') & '_value',Choose(p_web.GSV('job:Third_Party_Site') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Third_Party_Site') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnThirdPartyDetails','Third Party Details','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('BrowseThirdPartyTransactions')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()


Validate::btnValidatePOP  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnValidatePOP',p_web.GetValue('NewValue'))
    do Value::btnValidatePOP
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnValidatePOP  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('btnValidatePOP') & '_value',Choose(p_web.GSV('ReadOnly:ValidatePOP') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('ReadOnly:ValidatePOP') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnValidatePOP','Validate POP','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('ProofOfPurchase?' &'popNextURL=CustomerServicesForm')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()


Validate::btnAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnAccessories',p_web.GetValue('NewValue'))
    do Value::btnAccessories
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnAccessories  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('btnAccessories') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnAccessories','Accessories','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('JobAccessories?' &'jobaccReturnURL=CustomerServicesForm')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()


Validate::btnViewCosts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnViewCosts',p_web.GetValue('NewValue'))
    do Value::btnViewCosts
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnViewCosts  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('btnViewCosts') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnViewCosts','View Costs','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('ViewCosts?' &'ViewCostsReturnURL=CustomerServicesForm')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()


Validate::btnAuditTrail  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnAuditTrail',p_web.GetValue('NewValue'))
    do Value::btnAuditTrail
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnAuditTrail  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('btnAuditTrail') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnAuditTrail','Audit Trail','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('BrowseAuditFilterSimple')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()


Validate::btnStatusChanges  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnStatusChanges',p_web.GetValue('NewValue'))
    do Value::btnStatusChanges
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnStatusChanges  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('btnStatusChanges') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnStatusChanges','Status Changes','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('BrowseStatusFilterSimple?' &'ReturnURL=CustomerServicesForm')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()


Validate::btnLocationChanges  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnLocationChanges',p_web.GetValue('NewValue'))
    do Value::btnLocationChanges
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnLocationChanges  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('btnLocationChanges') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnLocationChanges','Location Changes','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('BrowseLocationHistory')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()


Validate::btnContactHistory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnContactHistory',p_web.GetValue('NewValue'))
    do Value::btnContactHistory
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnContactHistory  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('btnContactHistory') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnContactHistory','Contact History','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('BrowseContactHistory?' &'bchNextURL=CustomerServicesForm')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()


Validate::btnExchangeUnit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnExchangeUnit',p_web.GetValue('NewValue'))
    do Value::btnExchangeUnit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnExchangeUnit  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('btnExchangeUnit') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnExchangeUnit','View Exchange Unit','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PickExchangeUnit?' &'ReturnURL=CustomerServicesForm')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()


Validate::btnLoanUnit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnLoanUnit',p_web.GetValue('NewValue'))
    do Value::btnLoanUnit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnLoanUnit  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('btnLoanUnit') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnLoanUnit','View Loan Unit','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PickLoanUnit?' &'ReturnURL=CustomerServicesForm')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()


Validate::btnEditAddress  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnEditAddress',p_web.GetValue('NewValue'))
    do Value::btnEditAddress
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnEditAddress  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('btnEditAddress') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnEditAddress','Edit Address','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('AmendAddress?' &'FromURL=CustomerServicesForm')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()


Validate::btnValidateSerialNo  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnValidateSerialNo',p_web.GetValue('NewValue'))
    do Value::btnValidateSerialNo
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnValidateSerialNo  Routine
  p_web._DivHeader('CustomerServicesForm_' & p_web._nocolon('btnValidateSerialNo') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnValidateSerialNo','Validate Serial No','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('SerialNumberValidation')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('CustomerServicesForm_jobe:HubRepair_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:HubRepair
      else
        do Value::jobe:HubRepair
      end
  of lower('CustomerServicesForm_jobe:OBFvalidated_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:OBFvalidated
      else
        do Value::jobe:OBFvalidated
      end
  of lower('CustomerServicesForm_jbn:Engineers_Notes_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:Engineers_Notes
      else
        do Value::jbn:Engineers_Notes
      end
  of lower('CustomerServicesForm_jbn:Fault_Description_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:Fault_Description
      else
        do Value::jbn:Fault_Description
      end
  of lower('CustomerServicesForm_FranchiseAccount_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::FranchiseAccount
      else
        do Value::FranchiseAccount
      end
  of lower('CustomerServicesForm_job:Account_Number_Franchise_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Account_Number_Franchise
      else
        do Value::job:Account_Number_Franchise
      end
  of lower('CustomerServicesForm_job:ESN_Edit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:ESN_Edit
      else
        do Value::job:ESN_Edit
      end
  of lower('CustomerServicesForm_job:Account_Number_Generic_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Account_Number_Generic
      else
        do Value::job:Account_Number_Generic
      end
  of lower('CustomerServicesForm_job:MSN_Edit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:MSN_Edit
      else
        do Value::job:MSN_Edit
      end
  of lower('CustomerServicesForm_job:Order_Number_edit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Order_Number_edit
      else
        do Value::job:Order_Number_edit
      end
  of lower('CustomerServicesForm_job:ProductCode_Edit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:ProductCode_Edit
      else
        do Value::job:ProductCode_Edit
      end
  of lower('CustomerServicesForm_job:Title_Edit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Title_Edit
      else
        do Value::job:Title_Edit
      end
  of lower('CustomerServicesForm_job:Initial_Edit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Initial_Edit
      else
        do Value::job:Initial_Edit
      end
  of lower('CustomerServicesForm_job:Model_Number_Edit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Model_Number_Edit
      else
        do Value::job:Model_Number_Edit
      end
  of lower('CustomerServicesForm_job:Surname_Edit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Surname_Edit
      else
        do Value::job:Surname_Edit
      end
  of lower('CustomerServicesForm_job:Unit_Type_Edit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Unit_Type_Edit
      else
        do Value::job:Unit_Type_Edit
      end
  of lower('CustomerServicesForm_jobe:EndUserTelNo_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:EndUserTelNo
      else
        do Value::jobe:EndUserTelNo
      end
  of lower('CustomerServicesForm_job:Colour_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Colour
      else
        do Value::job:Colour
      end
  of lower('CustomerServicesForm_job:Transit_Type_Edit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Transit_Type_Edit
      else
        do Value::job:Transit_Type_Edit
      end
  of lower('CustomerServicesForm_job:DOP_Edit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:DOP_Edit
      else
        do Value::job:DOP_Edit
      end
  of lower('CustomerServicesForm_job:Mobile_Number_Edit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Mobile_Number_Edit
      else
        do Value::job:Mobile_Number_Edit
      end
  of lower('CustomerServicesForm_job:Turnaround_Time_Edit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Turnaround_Time_Edit
      else
        do Value::job:Turnaround_Time_Edit
      end
  of lower('CustomerServicesForm_job:ThirdPartyDateDesp_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:ThirdPartyDateDesp
      else
        do Value::job:ThirdPartyDateDesp
      end
  of lower('CustomerServicesForm_job:Date_Paid_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Date_Paid
      else
        do Value::job:Date_Paid
      end
  of lower('CustomerServicesForm_jobe:VSACustomer_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:VSACustomer
      else
        do Value::jobe:VSACustomer
      end
  of lower('CustomerServicesForm_jbn:Fault_Description_edit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:Fault_Description_edit
      else
        do Value::jbn:Fault_Description_edit
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('CustomerServicesForm_form:ready_',1)
  p_web.SetSessionValue('CustomerServicesForm_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_CustomerServicesForm',0)

PreCopy  Routine
  p_web.SetValue('CustomerServicesForm_form:ready_',1)
  p_web.SetSessionValue('CustomerServicesForm_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_CustomerServicesForm',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('CustomerServicesForm_form:ready_',1)
  p_web.SetSessionValue('CustomerServicesForm_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('CustomerServicesForm:Primed',0)

PreDelete       Routine
  p_web.SetValue('CustomerServicesForm_form:ready_',1)
  p_web.SetSessionValue('CustomerServicesForm_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('CustomerServicesForm:Primed',0)
  p_web.setsessionvalue('showtab_CustomerServicesForm',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If p_web.GSV('Amend') = 0
  End
  If p_web.GSV('Amend') <> 0
          If p_web.IfExistsValue('jobe:VSACustomer') = 0
            p_web.SetValue('jobe:VSACustomer',0)
            jobe:VSACustomer = 0
          End
  End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord
  ! Check For Changes And Save Jobs
    IF ~(DuplicateTabCheck(p_web,'JOBUPDATEREFNO',p_web.GSV('wob:RecordNumber')) = 1)
        CreateScript(packet,'alert("Error. Make sure that you do not login, or edit/insert another job while you in the middle of editing.");window.open("IndexPage","_self")')
        DO SendPacket
        EXIT
    END ! IF  
    
    IF (p_web.GSV('Amend') = 1)
        UpdateDateTimeStamp(wob:RefNumber)
  
        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = p_web.GSV('job:Ref_Number')
        IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
        END ! IF
  
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
        END ! IF
  
        Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
        jbn:RefNumber = job:Ref_Number
        IF (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)
        END ! IF
  
        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = job:Ref_Number
        IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
        END ! IF
        
        Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
        jobe2:RefNumber = job:Ref_Number
        IF (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
        END ! IF
        
  
        DidAnythingChange()
  
        p_web.SessionQueueToFile(JOBS)
        p_web.SessionQueueToFile(WEBJOB)
        p_web.SessionQueueToFile(JOBSE)
        p_web.SessionQueueToFile(JOBNOTES)
        p_web.SessionQueueToFile(JOBSE2)
  
        IF (Access:JOBS.TryUpdate() = Level:Benign)
            
  
            Access:JOBSE2.TryUpdate()
            Access:JOBSE.TryUpdate()
            
            ! Write back duplicate jobs fields to webjob
            wob:SubAcountNumber = job:Account_Number
            wob:OrderNumber = job:Order_Number
            wob:IMEINumber = job:ESN
            wob:MSN = job:MSN
            wob:Manufacturer = job:Manufacturer
            wob:ModelNumber = job:Model_Number
            wob:MobileNumber = job:Mobile_Number
            wob:Current_Status = job:Current_Status
            wob:Exchange_Status = job:Exchange_Status
            wob:Loan_Status = job:Loan_Status
            
            Access:WEBJOB.TryUpdate()
            Access:JOBNOTES.TryUpdate()
        END!  IF
    END ! IF
    
    DidAccessoriesChange() ! Can be altered, even if the job hasn't changed
    LockRecord(p_web.GSV('job:Ref_Number'),p_web.SessionID,1) ! Remove Lock
  
    DuplicateTabCheckDelete(p_web,'JOBUPDATEREFNO','')

ValidateDelete  Routine
  p_web.DeleteSessionValue('CustomerServicesForm_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('CustomerServicesForm_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 4
    loc:InvalidTab += 1
  ! tab = 1
  If p_web.GSV('Amend') = 0
    loc:InvalidTab += 1
          jbn:Fault_Description = Upper(jbn:Fault_Description)
          p_web.SetSessionValue('jbn:Fault_Description',jbn:Fault_Description)
        If loc:Invalid <> '' then exit.
  End
  ! tab = 3
  If p_web.GSV('Amend') <> 0
    loc:InvalidTab += 1
      If not (p_web.GSV('FranchiseAccount') <> 1)
        If job:Account_Number = ''
          loc:Invalid = 'job:Account_Number'
          loc:alert = p_web.translate('Franchise Account No') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          job:Account_Number = Upper(job:Account_Number)
          p_web.SetSessionValue('job:Account_Number',job:Account_Number)
        If loc:Invalid <> '' then exit.
      End
        If job:ESN = '' and p_web.GSV('Req:IMEI') = 1
          loc:Invalid = 'job:ESN'
          loc:alert = p_web.translate('I.M.E.I. Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          job:ESN = Upper(job:ESN)
          p_web.SetSessionValue('job:ESN',job:ESN)
        If loc:Invalid <> '' then exit.
      If not (p_web.GSV('FranchiseAccount') <> 2)
        If job:Account_Number = ''
          loc:Invalid = 'job:Account_Number'
          loc:alert = p_web.translate('Generic Account No') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          job:Account_Number = Upper(job:Account_Number)
          p_web.SetSessionValue('job:Account_Number',job:Account_Number)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:MSN') = 1)
          job:MSN = Upper(job:MSN)
          p_web.SetSessionValue('job:MSN',job:MSN)
        If loc:Invalid <> '' then exit.
      End
        If job:Order_Number = '' and p_web.GSV('Req:OrderNumber') = 1
          loc:Invalid = 'job:Order_Number'
          loc:alert = p_web.translate('Order Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          job:Order_Number = Upper(job:Order_Number)
          p_web.SetSessionValue('job:Order_Number',job:Order_Number)
        If loc:Invalid <> '' then exit.
      If not (p_web.GSV('Hide:ProductCode'))
          job:ProductCode = Upper(job:ProductCode)
          p_web.SetSessionValue('job:ProductCode',job:ProductCode)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:EndUser') = 1)
          job:Title = Upper(job:Title)
          p_web.SetSessionValue('job:Title',job:Title)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:EndUser') = 1)
          job:Initial = Upper(job:Initial)
          p_web.SetSessionValue('job:Initial',job:Initial)
        If loc:Invalid <> '' then exit.
      End
        If job:Model_Number = '' and p_web.GSV('Req:ModelNumber') = 1
          loc:Invalid = 'job:Model_Number'
          loc:alert = p_web.translate('Model Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          job:Model_Number = Upper(job:Model_Number)
          p_web.SetSessionValue('job:Model_Number',job:Model_Number)
        If loc:Invalid <> '' then exit.
      If not (p_web.GSV('Hide:EndUser') = 1)
        If job:Surname = '' and p_web.GSV('Req:Surname') = 1
          loc:Invalid = 'job:Surname'
          loc:alert = p_web.translate('Surname') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          job:Surname = Upper(job:Surname)
          p_web.SetSessionValue('job:Surname',job:Surname)
        If loc:Invalid <> '' then exit.
      End
        If job:Unit_Type = '' and p_web.GSV('Req:UnitType') = 1
          loc:Invalid = 'job:Unit_Type'
          loc:alert = p_web.translate('Unit Type') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          job:Unit_Type = Upper(job:Unit_Type)
          p_web.SetSessionValue('job:Unit_Type',job:Unit_Type)
        If loc:Invalid <> '' then exit.
          jobe:EndUserTelNo = Upper(jobe:EndUserTelNo)
          p_web.SetSessionValue('jobe:EndUserTelNo',jobe:EndUserTelNo)
        If loc:Invalid <> '' then exit.
        If job:Colour = '' and p_web.GSV('Req:Colour') = 1
          loc:Invalid = 'job:Colour'
          loc:alert = p_web.translate('Colour') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          job:Colour = Upper(job:Colour)
          p_web.SetSessionValue('job:Colour',job:Colour)
        If loc:Invalid <> '' then exit.
        If job:Transit_Type = ''
          loc:Invalid = 'job:Transit_Type'
          loc:alert = p_web.translate('Transit Type') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          job:Transit_Type = Upper(job:Transit_Type)
          p_web.SetSessionValue('job:Transit_Type',job:Transit_Type)
        If loc:Invalid <> '' then exit.
        If job:DOP = '' and p_web.GSV('Req:DOP') = 1
          loc:Invalid = 'job:DOP'
          loc:alert = p_web.translate('Date Of Purchase') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
        If job:Mobile_Number = '' and p_web.GSV('Req:MobileNumber') = 1
          loc:Invalid = 'job:Mobile_Number'
          loc:alert = p_web.translate('Mobile Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          job:Mobile_Number = Upper(job:Mobile_Number)
          p_web.SetSessionValue('job:Mobile_Number',job:Mobile_Number)
        If loc:Invalid <> '' then exit.
          job:Turnaround_Time = Upper(job:Turnaround_Time)
          p_web.SetSessionValue('job:Turnaround_Time',job:Turnaround_Time)
        If loc:Invalid <> '' then exit.
          jbn:Fault_Description = Upper(jbn:Fault_Description)
          p_web.SetSessionValue('jbn:Fault_Description',jbn:Fault_Description)
        If loc:Invalid <> '' then exit.
  End
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('CustomerServicesForm:Primed',0)
  p_web.StoreValue('wob:RecordNumber')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('job:Current_Status')
  p_web.StoreValue('job:Exchange_Status')
  p_web.StoreValue('job:Loan_Status')
  p_web.StoreValue('locJobTimeRemain')
  p_web.StoreValue('locStatusTimeRemain')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('job:Account_Number')
  p_web.StoreValue('locIncomingIMEI')
  p_web.StoreValue('locIncomingMSN')
  p_web.StoreValue('job:Order_Number')
  p_web.StoreValue('locExchangedIMEI')
  p_web.StoreValue('locExchangeMSN')
  p_web.StoreValue('locCustomerName')
  p_web.StoreValue('job:ESN')
  p_web.StoreValue('job:MSN')
  p_web.StoreValue('')
  p_web.StoreValue('loc2ndExchangeIMEI')
  p_web.StoreValue('loc2ndMSN')
  p_web.StoreValue('job:Transit_Type')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('job:Manufacturer')
  p_web.StoreValue('job:Workshop')
  p_web.StoreValue('job:Unit_Type')
  p_web.StoreValue('job:Mobile_Number')
  p_web.StoreValue('job:DOP')
  p_web.StoreValue('job:Location')
  p_web.StoreValue('job:Turnaround_Time')
  p_web.StoreValue('job:Third_Party_Site')
  p_web.StoreValue('job:Charge_Type')
  p_web.StoreValue('job:Special_Instructions')
  p_web.StoreValue('job:Warranty_Charge_Type')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locEngineer')
  p_web.StoreValue('jbn:Engineers_Notes')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('job:Insurance_Reference_Number')
  p_web.StoreValue('job:Incoming_Courier')
  p_web.StoreValue('jbn:Fault_Description')
  p_web.StoreValue('job:Courier')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('FranchiseAccount')
  p_web.StoreValue('job:Account_Number')
  p_web.StoreValue('job:ESN')
  p_web.StoreValue('job:Account_Number')
  p_web.StoreValue('job:MSN')
  p_web.StoreValue('job:Order_Number')
  p_web.StoreValue('job:ProductCode')
  p_web.StoreValue('job:Title')
  p_web.StoreValue('job:Initial')
  p_web.StoreValue('job:Model_Number')
  p_web.StoreValue('job:Surname')
  p_web.StoreValue('job:Unit_Type')
  p_web.StoreValue('job:Colour')
  p_web.StoreValue('job:Transit_Type')
  p_web.StoreValue('job:DOP')
  p_web.StoreValue('job:Mobile_Number')
  p_web.StoreValue('job:Turnaround_Time')
  p_web.StoreValue('job:Third_Party_Site')
  p_web.StoreValue('job:ThirdPartyDateDesp')
  p_web.StoreValue('job:Date_Paid')
  p_web.StoreValue('jbn:Fault_Description')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
checkPassword  Routine
  packet = clip(packet) & |
    '<<div id="div-background-warrantyjobalert" style="display:none;"><13,10>'&|
    '<<div id="div-back-warrantyjobalert"><</div><13,10>'&|
    '  <<div id="div-front-warrantyjobalert"><13,10>'&|
    '    <<p class="SubHeading">Warranty Claim Rejected<</p><13,10>'&|
    '    <<p style="color:red;font-weight:bold"><13,10>'&|
    '      The selected Warranty job has been rejected.<<br/><13,10>'&|
    '      <13,10>'&|
    '    <</p><13,10>'&|
    '    <<button id="viewButton" onclick="validateJobPassword(1);" class="MessageBoxButton">View Job<</button><13,10>'&|
    '    <<br/><13,10>'&|
    '    <<br/><13,10>'&|
    '    <<hr/><13,10>'&|
    '    <<p>To edit the job, enter your password below<<br/><13,10>'&|
    '      and click "Edit Job".<<br/><13,10>'&|
    '    <</p><13,10>'&|
    '    <<input id="userpassword" name="userpassword" type="password" action="" class="Upper FormEntry formrqd"/><13,10>'&|
    '    <<br/><13,10>'&|
    '    <<br/><13,10>'&|
    '    <13,10>'&|
    '    <<button id="okButton" onclick="validateJobPassword(0);" class="MessageBoxButton">Edit Job<</button><13,10>'&|
    '  <</div><13,10>'&|
    '<</div><13,10>'&|
    ''
BuildModelFilter        PROCEDURE(STRING pIMEINumber,LONG pChangeModel=0)
qManufacturer   QUEUE(),PRE(qManufacturer)
Manufacturer        STRING(30)
                END ! QUEUE
qModelNumber    QUEUE(),pre(qModelNumber)
Manufacturer        STRING(30)
ModelNumber         STRING(30)
                END !  QUEUE
    CODE
        LOOP l# = 1 TO 2
            Access:ESNMODEL.ClearKey(esn:ESN_Only_Key)
            IF (l# = 1)
                esn:ESN = SUB(pIMEINumber,1,6)
            ELSE
                esn:ESN = SUB(pIMEINumber,1,8)
            END ! IF
            SET(esn:ESN_Only_Key,esn:ESN_Only_Key)
            LOOP UNTIL Access:ESNMODEL.Next() <> Level:Benign
                IF (l# = 1)
                    IF (esn:ESN <> SUB(pIMEINumber,1,6))
                        BREAK
                    END ! IF (esn:ESN <> p_web.GSV('job:ESN'))
                ELSE
                    IF (esn:ESN <> SUB(pIMEINumber,1,8))
                        BREAK
                    END ! IF (esn:ESN <> p_web.GSV('job:ESN'))
                END ! IF

                IF (esn:Active <> 'Y')
                    CYCLE
                END ! IF

                Access:MODELNUM.ClearKey(mod:Manufacturer_Key)
                mod:Manufacturer = esn:Manufacturer
                mod:Model_Number = esn:Model_Number
                IF (Access:MODELNUM.TryFetch(mod:Manufacturer_Key) = Level:Benign)
                    IF (mod:Active <> 'Y')
                        CYCLE
                    END ! IF
                END ! IF
                Access:MANUFACT.ClearKey(man:Manufacturer_Key)
                man:Manufacturer = mod:Manufacturer
                IF (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
                    !IF (man:Notes[1:8] = 'INACTIVE')
                    IF (man:Inactive = 1)
                        CYCLE
                    END ! IF
                END ! IF

                qManufacturer.Manufacturer = esn:Manufacturer
                GET(qManufacturer,qManufacturer.Manufacturer)
                IF (ERROR())
                    ADD(qManufacturer)
                END ! IF

                qModelNumber.ModelNumber = esn:Model_Number
                GET(qModelNumber,qModelNumber.ModelNumber)
                IF (ERROR())
                    qModelNumber.ModelNumber = esn:Model_Number
                    ADD(qModelNumber)
                END ! IF

            END ! LOOP UNTIL Access:ESNMODEL.Next() <> Level:Benign
        END ! LOOP

        p_web.SSV('filter:ModelNumber','')

        CASE RECORDS(qModelNumber)
        OF 0
            RETURN
        OF 1
            GET(qModelNumber,1)
            p_web.SSV('filter:ModelNumber','mod:Model_Number = ''' & CLIP(qModelNumber.ModelNumber) & '''')

        ELSE
            LOOP i# = 1 TO RECORDS(qModelNumber)
                GET(qModelNumber,i#)
                p_web.SSV('filter:ModelNumber',p_web.GSV('filter:ModelNumber') & ' OR UPPER(mod:Model_Number) = UPPER(''' & CLIP(qModelNumber.ModelNumber) & ''')')
                
            END ! LOOP
            p_web.SSV('filter:ModelNumber',SUB(p_web.GSV('filter:ModelNumber'),5,1000))
            
        END ! CASE
        
        IF (pChangeModel = 1)
            qModelNumber.ModelNumber = p_web.GSV('job:Model_Number')
            GET(qModelNumber,qModelNumber.ModelNumber)
            IF (ERROR())
                ! Pick a new Model Number
                p_web.SSV('job:Model_Number','')
            END ! IF
        END ! IF
        
CheckSecurity       PROCEDURE()
    CODE
        
        p_web.SSV('ReadOnly:AccountNumber',0)
        IF (DoesUserHaveAccess(p_web,'AMEND COMPLETED JOBS') = 0)
            IF (p_web.GSV('job:Date_Completed') > 0)
                p_web.SSV('ReadOnly:AccountNumber',1)
            END ! IF
        END ! IF
        
        p_web.SSV('ReadOnly:AccountNumber',0)
        IF (DoesUserHaveAccess(p_web,'AMEND INVOICED JOBS') = 0)
            IF (p_web.GSV('job:Invoice_Number') > 0)
                p_web.SSV('ReadOnly:AccountNumber',1)
            END ! IF
        END ! IF
        
        p_web.SSV('ReadOnly:IMEINumber',0)
        IF (DoesUserHaveAccess(p_web,'JOBS - AMEND IMEI NUMBER') = 0)
            p_web.SSV('ReadOnly:IMEINumber',1)
        END !I F
            
        IF (DoesUserHaveAccess(p_web,'JOBS - AMEND POP DETAILS'))
            p_web.SSV('ReadOnly:ValidatePOP',0)
        ELSE ! IF
            p_web.SSV('ReadOnly:ValidatePOP',1)
        END ! IF


CheckForRequiredHiddenFields        PROCEDURE()
    CODE
        p_web.SSV('Req:TransitType',ForceTransitType('B'))
        p_web.SSV('Req:IMEI',ForceIMEI('B'))
        p_web.SSV('Req:ModelNumber',ForceModelNumber('B'))
        p_web.SSV('Req:UnitType',ForceUnitType('B'))
        p_web.SSV('Req:Colour',ForceColour('B'))
        p_web.SSV('Req:DOP',ForceDOP(p_web.GSV('job:Transit_Type'),p_web.GSV('job:Manufacturer'),p_web.GSV('job:Warranty_Job'),'B'))
        p_web.SSV('Req:MobileNumber',ForceMobileNumber('B'))
        p_web.SSV('Req:OrderNumber',ForceOrderNumber('B'))
        p_web.SSV('Req:Surname',ForceCustomerName(p_web.GSV('job:Account_Number'),'B'))

        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = p_web.GSV('job:Account_Number')
        IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
            p_web.SSV('FranchiseAccount',1)
            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = sub:Main_Account_Number
            IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                IF (tra:Use_Contact_Name = 'YES')
                    p_web.SSV('Hide:EndUserName',0)
                ELSE
                    p_web.SSV('Hide:EndUserName',1)
                END

                IF (tra:Use_Sub_Accounts = 'YES' AND tra:Invoice_Sub_Accounts = 'YES')
                    IF (sub:ForceOrderNumber)
                        p_web.SSV('Req:OrderNumber',1)
                    ELSE ! IF
                        p_web.SSV('Req:OrderNumber',0)
                    END ! IF
                ELSE ! IF
                    IF (tra:ForceOrderNumber)
                        p_web.SSV('Req:OrderNumber',1)
                    ELSE ! IF
                        p_web.SSV('Req:OrderNumber',0)
                    END ! IF
                END ! IF
            ELSE ! IF
            END ! IF
        ELSE ! IF

            Access:TRAHUBAC.ClearKey(tra1:HeadAccSubAccKey)
            tra1:HeadAcc = p_web.GSV('BookingAccount')
            tra1:SubAcc = p_web.GSV('job:Account_Number')
            IF (Access:TRAHUBAC.TryFetch(tra1:HeadAccSubAccKey) = Level:Benign)
                p_web.SSV('FranchiseAccount',2)
            END ! IF
        END ! IF
        IF (MSNRequired(p_web.GSV('job:Manufacturer')))
            p_web.SSV('Hide:MSN',0)
        ELSE !
            p_web.SSV('Hide:MSN',1)
        END ! IF

        IF (ProductCodeRequired(p_web.GSV('job:Manufacturer')))
            p_web.SSV('Hide:ProductCode',0)
        ELSE ! IF
            p_web.SSV('Hide:ProductCode',1)
        END ! IF       
DidAnythingChange   PROCEDURE()
    CODE

        locAuditText = ''

        IF (job:Account_Number <> p_web.GSV('job:Account_Number'))
            locAuditText = CLIP(locAuditText) & '<13,10>ACCOUNT NO: ' & p_web.GSV('job:Account_Number')
        END ! IF
        IF (job:ESN <> p_web.GSV('job:ESN'))
            locAuditText = CLIP(locAuditText) & '<13,10>IMEI NO: ' & p_web.GSV('job:ESN')
        END ! IF
        IF (job:Order_Number <> p_web.GSV('job:Order_Number'))
            locAuditText = CLIP(locAuditText) & '<13,10>ORDER NO: ' & p_web.GSV('job:Order_Number')
        END ! IF
        IF (job:Model_Number <> p_web.GSV('job:Model_Number'))
            locAuditText = CLIP(locAuditText) & '<13,10>ORDER NO: ' & p_web.GSV('job:Order_Number')
        END ! IF
        IF (job:Surname <> p_web.GSV('job:Surname'))
            locAuditText = CLIP(locAuditText) & '<13,10>SURNAME: ' & p_web.GSV('job:Surname')
        END ! IF
        IF (job:Unit_Type <> p_web.GSV('job:Unit_Type'))
            locAuditText = CLIP(locAuditText) & '<13,10>UNIT TYPE: ' & p_web.GSV('job:Unit_Type')
        END ! IF
        IF (job:Telephone_Number <> p_web.GSV('job:Telephone_Number'))
            locAuditText = CLIP(locAuditText) & '<13,10>TEL NO: ' & p_web.GSV('job:Telephone_Number')
        END ! IF
        IF (job:Colour <> p_web.GSV('job:Colour'))
            locAuditText = CLIP(locAuditText) & '<13,10>COLOUR: ' & p_web.GSV('job:Colour')
        END ! IF
        IF (job:Transit_Type <> p_web.GSV('job:Transit_Type'))
            locAuditText = CLIP(locAuditText) & '<13,10>TRANSIT TYPE: ' & p_web.GSV('job:Colour')
        END ! IF
        IF (job:Mobile_Number <> p_web.GSV('job:Mobile_Number'))
            locAuditText = CLIP(locAuditText) & '<13,10>MOBILE NO: ' & p_web.GSV('job:Mobile_Number')
        END ! IF
        IF (job:DOP <> p_web.GSV('job:DOP'))
            locAuditText = CLIP(locAuditText) & '<13,10>DOP: ' & FORMAT(p_web.GSV('job:DOP'),@d06b)
        END ! IF

        IF (locAuditText <> '')
            locAuditText = 'JOB DETAILS CHANGED: ' & CLIP(locAuditText)
            AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','JOB UPDATED',CLIP(locAuditText))
        END ! IF

        locAuditText = ''

        IF (job:Fault_Code1 <> p_web.GSV('job:Fault_Code1'))
            locAuditText = CLIP(locAuditText) & '<13,10>FAULT CODE 1: ' & p_web.GSV('job:Fault_Code1')
        END ! IF
        IF (job:Fault_Code2 <> p_web.GSV('job:Fault_Code2'))
            locAuditText = CLIP(locAuditText) & '<13,10>FAULT CODE 2: ' & p_web.GSV('job:Fault_Code2')
        END ! IF
        IF (job:Fault_Code3 <> p_web.GSV('job:Fault_Code3'))
            locAuditText = CLIP(locAuditText) & '<13,10>FAULT CODE 3: ' & p_web.GSV('job:Fault_Code3')
        END ! IF
        IF (job:Fault_Code4 <> p_web.GSV('job:Fault_Code4'))
            locAuditText = CLIP(locAuditText) & '<13,10>FAULT CODE 4: ' & p_web.GSV('job:Fault_Code4')
        END ! IF
        IF (job:Fault_Code5 <> p_web.GSV('job:Fault_Code5'))
            locAuditText = CLIP(locAuditText) & '<13,10>FAULT CODE 5: ' & p_web.GSV('job:Fault_Code5')
        END ! IF
        IF (job:Fault_Code6 <> p_web.GSV('job:Fault_Code6'))
            locAuditText = CLIP(locAuditText) & '<13,10>FAULT CODE 6: ' & p_web.GSV('job:Fault_Code6')
        END ! IF
        IF (job:Fault_Code7 <> p_web.GSV('job:Fault_Code7'))
            locAuditText = CLIP(locAuditText) & '<13,10>FAULT CODE 7: ' & p_web.GSV('job:Fault_Code7')
        END ! IF
        IF (job:Fault_Code8 <> p_web.GSV('job:Fault_Code8'))
            locAuditText = CLIP(locAuditText) & '<13,10>FAULT CODE 8: ' & p_web.GSV('job:Fault_Code8')
        END ! IF
        IF (job:Fault_Code9 <> p_web.GSV('job:Fault_Code9'))
            locAuditText = CLIP(locAuditText) & '<13,10>FAULT CODE 9: ' & p_web.GSV('job:Fault_Code9')
        END ! IF
        IF (job:Fault_Code10 <> p_web.GSV('job:Fault_Code10'))
            locAuditText = CLIP(locAuditText) & '<13,10>FAULT CODE 10: ' & p_web.GSV('job:Fault_Code10')
        END ! IF
        IF (job:Fault_Code11 <> p_web.GSV('job:Fault_Code11'))
            locAuditText = CLIP(locAuditText) & '<13,10>FAULT CODE 11: ' & p_web.GSV('job:Fault_Code11')
        END ! IF
        IF (job:Fault_Code12 <> p_web.GSV('job:Fault_Code12'))
            locAuditText = CLIP(locAuditText) & '<13,10>FAULT CODE 12: ' & p_web.GSV('job:Fault_Code12')
        END ! IF
        IF (wob:FaultCode13 <> p_web.GSV('wob:FaultCode13'))
            locAuditText = CLIP(locAuditText) & '<13,10>FAULT CODE 13: ' & p_web.GSV('wob:FaultCode13')
        END ! IF
        IF (wob:FaultCode14 <> p_web.GSV('wob:FaultCode14'))
            locAuditText = CLIP(locAuditText) & '<13,10>FAULT CODE 14: ' & p_web.GSV('wob:FaultCode14')

        END ! IF
        IF (wob:FaultCode15 <> p_web.GSV('wob:FaultCode15'))
            locAuditText = CLIP(locAuditText) & '<13,10>FAULT CODE 15: ' & p_web.GSV('wob:FaultCode15')

        END ! IF
        IF (wob:FaultCode16 <> p_web.GSV('wob:FaultCode16'))
            locAuditText = CLIP(locAuditText) & '<13,10>FAULT CODE 16: ' & p_web.GSV('wob:FaultCode16')

        END ! IF
        IF (wob:FaultCode17 <> p_web.GSV('wob:FaultCode17'))
            locAuditText = CLIP(locAuditText) & '<13,10>FAULT CODE 17: ' & p_web.GSV('wob:FaultCode17')

        END ! IF
        IF (wob:FaultCode18 <> p_web.GSV('wob:FaultCode18'))
            locAuditText = CLIP(locAuditText) & '<13,10>FAULT CODE 18: ' & p_web.GSV('wob:FaultCode18')

        END ! IF
        IF (wob:FaultCode19 <> p_web.GSV('wob:FaultCode19'))
            locAuditText = CLIP(locAuditText) & '<13,10>FAULT CODE 19: ' & p_web.GSV('wob:FaultCode19')

        END ! IF
        IF (wob:FaultCode20 <> p_web.GSV('wob:FaultCode20'))
            locAuditText = CLIP(locAuditText) & '<13,10>FAULT CODE 20: ' & p_web.GSV('wob:FaultCode20')

        END ! IF

        IF (locAuditText <> '')
            locAuditText = 'JOB DETAILS CHANGED: ' & CLIP(locAuditText)
            AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','JOB UPDATED: FAULT CODES',CLIP(locAuditText))
        END ! IF
        locAuditText = ''

        IF (CLIP(jbn:Fault_Description) <> p_web.GSV('jbn:Fault_Description'))
            AddToAudit(p_web,job:Ref_Number,'JOB','JOB UPDATED: FAULT DESCRIPTION',|
                'JOB DETAILS CHANGED: <13,10>FAULT DESC: ' & p_web.GSV('jbn:Fault_Description'))
        END ! IF

        IF (job:Company_Name <> p_web.GSV('job:Company_Name') OR |
            job:Address_Line1 <> p_web.GSV('job:Address_Line1') OR |
            job:Address_Line2 <> p_web.GSV('job:Address_Line2') OR |
            job:Address_Line3 <> p_web.GSV('job:Address_Line3') OR |
            job:Postcode <> p_web.GSV('job:Postcode') OR |
            job:Telephone_Number <> p_web.GSV('job:Telephone_Number') OR |
            job:Fax_Number <> p_web.GSV('job:Fax_Number'))
            locAuditText = 'PREVIOUS: <13,10>' & CLIP(job:Company_Name) & |
                '<13,10>' & CLIP(job:Address_Line1) & |
                '<13,10>' & CLIP(job:Address_Line2) & |
                '<13,10>' & CLIP(job:Address_Line3) & |
                '<13,10>' & CLIP(job:Postcode) & |
                '<13,10>TEL: ' & CLIP(job:Telephone_Number) & |
                '<13,10>FAX: ' & CLIP(job:Fax_Number)
            AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','ADDRESS DETAILS CHANGED - CUSTOMER',CLIP(locAuditText))
        END ! IF

        IF (job:Company_Name_Delivery <> p_web.GSV('job:Company_Name_Delivery') OR |
            job:Address_Line1_Delivery <> p_web.GSV('job:Address_Line1_Delivery') OR |
            job:Address_Line2_Delivery <> p_web.GSV('job:Address_Line2_Delivery') OR |
            job:Address_Line3_Delivery <> p_web.GSV('job:Address_Line3_Delivery') OR |
            job:Postcode_Delivery <> p_web.GSV('job:Postcode_Delivery') OR |
            job:Telephone_Delivery <> p_web.GSV('job:Telephone_Delivery'))
            locAuditText = '<13,10>PREVIOUS: <13,10>' & CLIP(job:Company_Name_Delivery) & |
                '<13,10>' & CLIP(job:Address_Line1_Delivery) & |
                '<13,10>' & CLIP(job:Address_Line2_Delivery) & |
                '<13,10>' & CLIP(job:Address_Line3_Delivery) & |
                '<13,10>' & CLIP(job:Postcode_Delivery) & |
                '<13,10>TEL: ' & CLIP(job:Telephone_Delivery)
            AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','ADDRESS DETAILS CHANGED - DESPATCH',CLIP(locAuditText))
        END ! IF
            
        
        
DidAccessoriesChange        PROCEDURE()
i                               LONG()
startPos                        LONG()
locAccessory                    STRING(30)
foundAccessory                  LONG(0)
recs                            LONG()
    CODE
        VodacomClass.CompareJobAccessoriesWithQueue(p_web.SessionID,p_web.GSV('job:Ref_Number'),locAuditText)
        !Check For Accessory Changes

        IF (locAuditText <> '')
            AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','JOB ACCESSORIES AMENDED',locAuditText)
        END
DisplayEngineerDetails      PROCEDURE()
    CODE
    !region Engineer Details
    IF (p_web.GSV('job:Engineer') <> '')
        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = p_web.GSV('job:Engineer')
        IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
            locEngineer = CLIP(use:Forename) & ' ' & CLIP(use:Surname) 
        END ! IF
    END ! IF
    !endregion

DisplayEstimateDetails      PROCEDURE()
    CODE
    !region Estimate Check
    p_web.SSV('txtEstimate','')
  
    IF (p_web.GSV('job:Estimate') = 'YES')
        p_web.SSV('txtEstimate','YES - If Over ' & p_web.GSV('job:Estimate_If_Over') & ' Status: ')
        IF (p_web.GSV('job:Estimate_Accepted') = 'YES')
            p_web.SSV('txtEstimate',p_web.GSV('txtEstimate') & ' Accepted')
        ELSE ! IF
            IF (p_web.GSV('job:Estimate_Rejected') = 'YES')
                p_web.SSV('txtEstimate',p_web.GSV('txtEstimate') & ' Rejected')
            ELSE ! IF
                IF (p_web.GSV('job:Estimate_Ready') = 'YES')
                    IF (SUB(p_web.GSV('job:Current_Status'),1,3) = '520')
                        p_web.SSV('txtEstimate',p_web.GSV('txtEstimate') & ' Sent')
                    ELSE ! IF
                        p_web.SSV('txtEstimate',p_web.GSV('txtEstimate') & ' Ready')
                    END ! IF
                ELSE ! IF
                    p_web.SSV('txtEstimate',p_web.GSV('txtEstimate') & ' In Progress')
                END ! IF
            END ! IF
        END ! IF
    ELSE
        ! DEBUG p_web.SSV('txtEstimate','YES - If Over ' & p_web.GSV('job:Estimate_If_Over') & ' Status: Accepted')
    END ! IF
        
    !endregion

DisplayNumberOfBouncers     PROCEDURE()
    CODE
        
    p_web.SSV('textBouncer','')
  
    CountHistory(p_web)
  
    IF (p_web.GSV('CountHistory') > 0)
        p_web.SSV('textBouncer','There are ' & p_web.GSV('CountHistory') & ' Previous Jobs')
    ELSE
        ! DEBUG p_web.SSV('textBouncer','There are 200 Previous Jobs')
    END ! if (x# > 0)

ISIMEIValid         PROCEDURE(STRING pIMEINumber,STRING pModelNumber,*STRING pError)!,LONG
locFoundAlpha           LONG(0)
kInvalidFormat          EQUATE('IMEI Number Format Is Invalid')
kInvalidLength          EQUATE('IMEI Number Invalid Length')
kBouncer                EQUATE('IMEI Number has been previously entered')
kLiveBouncer            EQUATE('IMEI Number has been previously entered on a live job')
kNoModel                EQUATE('A Model has not been recognised for this IMEI Number')
kNewModel               EQUATE('IMEI Number has been changed. Please select a new corresponding Model Number')
locBouncers             LONG()
locLiveBouncers         LONG()
retValue                LONG(0)
    CODE

        LOOP
            locFoundAlpha    = 0
            LOOP i# = 1 TO LEN(CLIP(pIMEINumber))
                IF (ISALPHA(pIMEINumber[i#]))
                    locFoundAlpha = 1
                    BREAK
                END ! IF
            END ! LOOP

            IF (locFoundAlpha)
                Access:MODELNUM.ClearKey(mod:Model_Number_Key)
                mod:Model_Number = pModelNumber
                IF (Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign)
                    IF (mod:AllowIMEICharacters)
                    ELSE ! I
                        retValue = Level:Fatal
                        pError = kInvalidFormat
                        BREAK

                    END ! IF
                END ! IF (Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign)
            END ! IF
            

            IF (CheckLength('IMEI',pModelNumber,pIMEINumber))
                retValue = Level:Fatal
                pError = kInvalidLength
                BREAK
            END ! IF

            ! Look For Bouncers
            Access:DEFAULTS.Clearkey(def:RecordNumberKey)
            def:Record_Number = 1
            Set(def:RecordNumberKey,def:RecordNumberKey)
            Loop ! Begin Loop
                If Access:DEFAULTS.Next()
                    Break
                End ! If Access:DEFAULTS.Next()
                Break
            End ! Loop

            Access:JOBS_ALIAS.ClearKey(job_ali:ESN_Key)
            job_ali:ESN = p_web.GSV('job:ESN')
            SET(job_ali:ESN_Key,job_ali:ESN_Key)
            LOOP UNTIL Access:JOBS_ALIAS.Next() <> Level:Benign
                IF (job_ali:ESN <> p_web.GSV('job:ESN'))
                    BREAK
                END ! IF (job_ali:ESN <> p_web.GSV('job:ESN'))
                IF (job_ali:Cancelled = 'YES')
                    CYCLE
                END ! IF
                IF (job_ali:Ref_Number = p_web.GSV('job:Ref_Number'))
                    CYCLE
                END ! IF
                locBouncers += 1
                IF (job_ali:Date_Completed = '')
                    locLiveBouncers += 1
                END ! IF
            END ! LOOP UNTIL Access:JOBS_ALIAS.Next() <> Level:Benign

            IF (def:Allow_Bouncer <> 'YES')
                IF (locBouncers)
                    retValue = Level:Fatal
                    pError = kBouncer
                    BREAK
                END ! IF
            ELSE ! IF
                If GETINI('BOUNCER','StopLive',,Clip(Path()) & '\SB2KDEF.INI') = 1
                    IF (locLiveBouncers)
                        retValue = Level:Fatal
                        pError = kLiveBouncer
                        BREAK
                    END ! IF
                END ! IF
            END ! IF

            BuildModelFilter(pIMEINumber,1)

            IF (p_web.GSV('filter:ModelNumber') = '')
                retValue = Level:Fatal
                pError = kNoModel
                BREAK                                
            END ! IF
            
            IF (p_web.GSV('job:Model_Number') = '')
                pError = kNewModel
            END ! IF
            
        END ! LOOP

        RETURN retValue
PopulateAccessoryList       PROCEDURE()
recs                            LONG()
i                               LONG
    CODE
        VodacomClass.SaveJobsAccessoriesToQueue(p_web.SessionID,p_web.GSV('job:Ref_Number'))

SaveDataToCheckForChanges   PROCEDURE()
    CODE
        p_web.SSV('save:AccountNumber',p_web.GSV('job:Account_Number'))
        p_web.SSV('save:IMEINumber',p_web.GSV('job:ESN'))
        p_web.SSV('save:DOP',p_web.GSV('job:DOP'))
ValidateTradeAccount        PROCEDURE(*STRING pError)
kOnStop EQUATE('Error! Selected Account is on Stop')
kOutOfRegion EQUATE('Warning! The selected account is outsite your Hub.<13,10,13,10>If you continue you may not be able to despatch this job.')
kPricingError EQUATE('Error! No Pricing Structure. The selected combination of Trade Account, Charge Type, Model Number, Unit Type and Repair Type does not have a pricing structure setup.')
kJobSaved                       EQUATE('Trade Account Changed. The job has been saved.')
kFileError EQUATE('Error! Unable to save job record')
    CODE
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = p_web.GSV('job:Account_Number')
        IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = sub:Main_Account_Number
            IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
            END ! IF

            IF (tra:Use_Sub_Accounts = 'YES' AND tra:Invoice_Sub_Accounts = 'YES')
                IF (sub:Stop_Account = 'YES')
                    pError = kOnStop
                    RETURN Level:Fatal
                END ! IF

                IF (sub:Use_Customer_Address <> 'YES')
                    IF (HubOutOfRegion(p_web.GSV('wob:HeadAccountNumber'),sub:Hub) = 1)
                        pError = kOutOfRegion
                    END ! IF
                END ! IF
            ELSE
                IF (tra:Stop_Account = 'YES')
                    pError = kOnStop
                    RETURN Level:Fatal
                END ! IF


                IF (tra:Use_Customer_Address <> 'YES')
                    IF (HubOutOfRegion(p_web.GSV('wob:HeadAccountNumber'),tra:Hub) = 1)
                        pError = kOutOfRegion
                    END ! IF
                ELSE
                    
                END ! IF
            END ! IF

            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = p_web.GSV('job:Ref_Number')
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                IF (CheckPricing('C') = 1 OR CheckPricing('W') = 1)
                    pError = kPricingError
                    RETURN Level:Fatal
                END ! IF
            END ! IF

            IF (tra:Invoice_Sub_Accounts <> 'YES')
                IF (tra:Use_Customer_Address <> 'YES')
                    p_web.SSV('job:PostCode',tra:Postcode)
                    p_web.SSV('job:Company_Name',tra:Company_Name)
                    p_web.SSV('job:Address_Line1',tra:Address_Line1)
                    p_web.SSV('job:Address_Line2',tra:Address_Line2)
                    p_web.SSV('job:Address_Line3',tra:Address_Line3)
                    p_web.SSV('job:Telephone_Number',tra:Telephone_Number)
                    p_web.SSV('job:Fax_Number',tra:Fax_Number)
                    p_web.SSV('jobe:EndUserEmailAddress',tra:EmailAddress)
                    p_web.SSV('jobe2:HubCustomer',tra:Hub)

                END ! IF
                IF (tra:Courier_Cost <> '')
                    p_web.SSV('job:Courier_Cost',tra:Courier_Cost)
                    p_web.SSV('job:Courier_Cost_Warranty',tra:Courier_Cost)
                END ! IF
            ELSE ! IF
                IF (sub:Use_Customer_Address <> 'YES')
                    p_web.SSV('job:PostCode',sub:Postcode)
                    p_web.SSV('job:Company_Name',sub:Company_Name)
                    p_web.SSV('job:Address_Line1',sub:Address_Line1)
                    p_web.SSV('job:Address_Line2',sub:Address_Line2)
                    p_web.SSV('job:Address_Line3',sub:Address_Line3)
                    p_web.SSV('job:Telephone_Number',sub:Telephone_Number)
                    p_web.SSV('job:Fax_Number',sub:Fax_Number)
                    p_web.SSV('jobe:EndUserEmailAddress',sub:EmailAddress)
                    p_web.SSV('jobe2:HubCustomer',sub:Hub)

                END ! IF
                IF (tra:Courier_Cost <> '')
                    p_web.SSV('job:Courier_Cost',sub:Courier_Cost)
                    p_web.SSV('job:Courier_Cost_Warranty',sub:Courier_Cost)
                END ! IF
            END ! IF

            IF (tra:Use_Sub_Accounts <> 'YES')
                IF (tra:Use_Delivery_Address = 'YES')
                    p_web.SSV('job:PostCode_Delivery',tra:Postcode)
                    p_web.SSV('job:Company_Name_Delivery',tra:Company_Name)
                    p_web.SSV('job:Address_Line1_Delivery',tra:Address_Line1)
                    p_web.SSV('job:Address_Line2_Delivery',tra:Address_Line2)
                    p_web.SSV('job:Address_Line3_Delivery',tra:Address_Line3)
                    p_web.SSV('job:Telephone_Number_Delivery',tra:Telephone_Number)
                    p_web.SSV('jobe2:HubDelivery',tra:Hub)
                END ! IF
                IF (tra:Use_Collection_Address = 'YES')
                    p_web.SSV('job:PostCode_Collection',tra:Postcode)
                    p_web.SSV('job:Company_Name_Collection',tra:Company_Name)
                    p_web.SSV('job:Address_Line1_Collection',tra:Address_Line1)
                    p_web.SSV('job:Address_Line2_Collection',tra:Address_Line2)
                    p_web.SSV('job:Address_Line3_Collection',tra:Address_Line3)
                    p_web.SSV('job:Telephone_Number_Collection',tra:Telephone_Number)
                    p_web.SSV('jobe2:HubCollection',tra:Hub)
                END ! IF
                p_web.SSV('job:Courier',tra:Courier_Outgoing)
                p_web.SSV('job:Incoming_Courier',tra:Courier_Incoming)
                p_web.SSV('job:Exchange_Courier',tra:Courier_Outgoing)
                p_web.SSV('job:Loan_courier',tra:Courier_Outgoing)

            ELSE ! !IF
                IF (sub:Use_Delivery_Address = 'YES')
                    p_web.SSV('job:PostCode_Delivery',sub:Postcode)
                    p_web.SSV('job:Company_Name_Delivery',sub:Company_Name)
                    p_web.SSV('job:Address_Line1_Delivery',sub:Address_Line1)
                    p_web.SSV('job:Address_Line2_Delivery',sub:Address_Line2)
                    p_web.SSV('job:Address_Line3_Delivery',sub:Address_Line3)
                    p_web.SSV('job:Telephone_Number_Delivery',sub:Telephone_Number)
                    p_web.SSV('jobe2:HubDelivery',sub:Hub)
                END ! IF
                IF (sub:Use_Collection_Address = 'YES')
                    p_web.SSV('job:PostCode_Collection',sub:Postcode)
                    p_web.SSV('job:Company_Name_Collection',sub:Company_Name)
                    p_web.SSV('job:Address_Line1_Collection',sub:Address_Line1)
                    p_web.SSV('job:Address_Line2_Collection',sub:Address_Line2)
                    p_web.SSV('job:Address_Line3_Collection',sub:Address_Line3)
                    p_web.SSV('job:Telephone_Number_Collection',sub:Telephone_Number)
                    p_web.SSV('jobe2:HubCollection',sub:Hub)
                END ! IF
                p_web.SSV('job:Courier',sub:Courier_Outgoing)
                p_web.SSV('job:Incoming_Courier',sub:Courier_Incoming)
                p_web.SSV('job:Exchange_Courier',sub:Courier_Outgoing)
                p_web.SSV('job:Loan_courier',sub:Courier_Outgoing)
            END ! IF

            IF (INSTRING('CASH',UPPER(job:Account_Number),1,1))
                Access:TRADEACC_ALIAS.ClearKey(tra_ali:Account_Number_Key )
                tra_ali:Account_Number = p_web.GSV('wob:HeadAccountNumber')
                IF (Access:TRADEACC_ALIAS.TryFetch(tra:Account_Number_Key) = Level:Benign)
                    p_web.SSV('jobe2:HubCustomer',tra_ali:Hub)
                    p_web.SSV('jobe2:HubCollection',tra_ali:Hub)
                    p_web.SSV('jobe2:HubDelivery',tra_ali:Hub)
                END !I F
            END ! IF

            IF (tra:Force_Estimate = 'YES')
                p_web.SSV('job:Estimate_If_Over',tra:Estimate_If_Over)
                p_web.SSV('job:Estimate','YES')
            END ! IF
            IF (tra:Turnaround_Time <> '')
                p_web.SSV('job:Turnaround_Time',tra:Turnaround_Time)
            END ! IF

            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = p_web.GSV('job:Ref_Number')
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                p_web.SessionQueueToFile(JOBS)
                IF (Access:JOBS.TryUpdate() = Level:Benign)
                    Access:JOBSE.ClearKey(jobe:RefNumberKey)
                    jobe:RefNumber = job:Ref_Number
                    IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                        p_web.SessionQueueToFile(JOBSE)
                        Access:JOBS.TryUpdate()
                    END ! IF
                    Access:WEBJOB.ClearKey(wob:RefNumberKey)
                    wob:RefNumber = job:Ref_Number
                    IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                        p_web.SessionQueueToFile(WEBJOB)
                        Access:WEBJOB.TryUpdate()
                    END ! IF
                    Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
                    jobe2:RefNumber = job:Ref_Number
                    IF (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
                        p_web.SessionQueueToFile(JOBSE2)
                        Access:JOBSE2.TryUpdate()
                    END ! IF
                    pError = kJobSaved
                ELSE
                    pError = kFileError
                    RETURN Level:Fatal
                END ! IF
            END ! IF

            

        END ! IF

        RETURN Level:Benign
ValidateDOP  PROCEDURE(*STRING pError)!,LONG
retValue LONG(Level:Benign)
kFutureDate EQUATE('Invalid Date.')
kOutsideWarranty EQUATE('The unit is outside the Manufacturer''s Warranty Period.')
    CODE
        LOOP 1 TIMES
            IF (p_web.GSV('job:DOP') > TODAY())
                pError = kFutureDate
                retValue = Level:Fatal
                BREAK
            END ! IF

            IF (p_web.GSV('job:Warranty_Job') <> 'YES')
                BREAK
            END ! IF

            Access:MANUFACT.ClearKey(man:Manufacturer_Key)
            man:Manufacturer = p_web.GSV('job:Manufacturer')
            IF (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
                IF (p_web.GSV('job:Warranty_Charge_Type') = 'WARRANTY (2ND YR)')
                    man:Warranty_Period = 730
                END ! IF

                IF (p_web.GSV('job:DOP') < (p_web.GSV('job:Date_Booked') - man:Warranty_Period) AND man:Warranty_Period > 0 AND p_web.GSV('job:DOP') <> '')
                    pError = kOutsideWarranty
                    GetStatus(130,0,'JOB',p_web) ! DOP QUERY
                    AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','D.O.P. OVER WARRANTY PERIOD BY ' & (p_web.GSV('job:date_booked') - (p_web.GSV('job:dop') + man:warranty_period)) & 'DAY(S)','')                    
                ELSE ! IF
                    ! If the job is at 130 DOP QUERY, and the job is ok,
                    ! then set it back to the previous status
                    IF (SUB(p_web.GSV('job:Current_Status'),1,3))
                        IF (p_web.GSV('job:Engineer') = '')
                            GetStatus(305,0,'JOB',p_web) ! AWAITING ALLOCATION
                        ELSE ! IF
                            GetStatus(SUB(p_web.GSV('job:Previous_Status'),1,3),0,'JOB')
                        END ! IF
                    END ! IF
                END ! IF
            ELSE ! IF
            END ! IF

        END ! LOOP
        RETURN retValue
WorkOutStatusAndTurnaroundTimes PROCEDURE()
    CODE
    
    p_web.SSV('locJobTimeRemain','')
    p_web.SSV('locStatusTimeRemain','')
    IF (p_web.GSV('job:Date_Completed') > 0)
        p_web.SSV('locJobTimeRemain','Job Completed')
        p_web.SSV('locStatusTimeRemain','Job Completed')
    ELSE ! IF
        IF (p_web.GSV('job:Status_End_Date') = DEFORMAT('1/1/2050',@d6) OR p_web.GSV('job:Status_end_Date') = '')
            p_web.SSV('locStatusTimeRemain','Never Expire')
        ELSE ! IF
            IF (p_web.GSV('job:Status_End_Date') > TODAY())
                count# = 0
                dayCount# = 0
                LOOP
                    count# += 1
                    dayNumber# = TODAY() + count#
                    IF (def:Include_Saturday <> 'YES')
                        IF (dayNumber# % 7 = 6)
                            CYCLE
                        END ! IF
                    END ! IF
                    IF (def:Include_Sunday <> 'YES')
                        IF (dayNumber# % 7 = 0)
                            CYCLE
                        END ! IF
                    END ! IF
                    dayCount# += 1
                    IF (dayNumber# >= p_web.GSV('job:Status_End_Date'))
                        BREAK
                    END ! IF
                END!  LOOP
  
                IF (dayCount# = 1)
                    p_web.SSV('locStatusTimeRemain',dayCount# & ' Day')
                ELSE ! IF
                    p_web.SSV('locStatusTimeRemain',dayCount# & ' Days')
                END ! IF
            END ! IF
  
            IF (p_web.GSV('job:Status_End_Date') < TODAY())
                count# = 0
                dayCount# = 0
                LOOP
                    count# += 1
                    dayNumber# = p_web.GSV('job:Status_End_Date') + count#
                    IF (def:Include_Saturday <> 'YES')
                        IF (dayNumber# % 7 = 6)
                            CYCLE
                        END ! IF
                    END ! IF
                    IF (def:Include_Sunday <> 'YES')
                        IF (dayNumber# % 7 = 0)
                            CYCLE
                        END ! IF
                    END ! IF
                    dayCount# += 1
                    IF (dayNumber# >= TODAY())
                        BREAK
                    END ! IF
                END!  LOOP
  
                IF (dayCount# = 1)
                    p_web.SSV('locStatusTimeRemain',dayCount# & ' Day Over')
                ELSE ! IF
                    p_web.SSV('locStatusTimeRemain',dayCount# & ' Days Over')
                END ! IF
            END ! IF
  
            IF (p_web.GSV('job:Status_End_Date') < TODAY())
                IF (p_web.GSV('job:Status_End_Time') > CLOCK())
                    p_web.SSV('locStatusTimeRemain',FORMAT(p_web.GSV('job:Status_End_Time') - CLOCK(),@t1) & ' Hrs')
                END ! IF
                IF (p_web.GSV('job:Status_End_Time') < CLOCK())
                    p_web.SSV('locStatusTimeRemain',FORMAT(CLOCK() - p_web.GSV('job:Status_End_Time'),@t1) & ' Hrs Over')
                END ! IF
                IF (p_web.GSV('job:Status_End_Time') = CLOCK())
                    p_web.SSV('locStatusTimeRemain','Due Now')
                END ! IF
            END ! IF
            
            IF (p_web.GSV('job:turnaround_end_date') > TODAY())
                count# = 0
                dayCount# = 0
                LOOP
                    count# += 1
                    dayNumber# = TODAY() + count#
                    IF (def:Include_Saturday <> 'YES')
                        IF (dayNumber# % 7 = 6)
                            CYCLE
                        END ! IF
                    END ! IF
                    IF (def:Include_Sunday <> 'YES')
                        IF (dayNumber# % 7 = 0)
                            CYCLE
                        END ! IF
                    END ! IF
                    dayCount# += 1
                    IF (dayNumber# >= p_web.GSV('job:turnaround_end_date'))
                        BREAK
                    END ! IF
                END!  LOOP
  
                IF (dayCount# = 1)
                    p_web.SSV('locJobTimeRemain',dayCount# & ' Day')
                ELSE ! IF
                    p_web.SSV('locJobTimeRemain',dayCount# & ' Days')
                END ! IF
            END ! IF
  
            IF (p_web.GSV('job:turnaround_end_date') < TODAY())
                count# = 0
                dayCount# = 0
                LOOP
                    count# += 1
                    dayNumber# = p_web.GSV('job:turnaround_end_date') + count#
                    IF (def:Include_Saturday <> 'YES')
                        IF (dayNumber# % 7 = 6)
                            CYCLE
                        END ! IF
                    END ! IF
                    IF (def:Include_Sunday <> 'YES')
                        IF (dayNumber# % 7 = 0)
                            CYCLE
                        END ! IF
                    END ! IF
                    dayCount# += 1
                    IF (dayNumber# >= TODAY())
                        BREAK
                    END ! IF
                END!  LOOP
  
                IF (dayCount# = 1)
                    p_web.SSV('locJobTimeRemain',dayCount# & ' Day Over')
                ELSE ! IF
                    p_web.SSV('locJobTimeRemain',dayCount# & ' Days Over')
                END ! IF
            END ! IF
  
            IF (p_web.GSV('job:turnaround_end_date') < TODAY())
                IF (p_web.GSV('job:turnaround_end_time') > CLOCK())
                    p_web.SSV('locJobTimeRemain',FORMAT(p_web.GSV('job:turnaround_end_time') - CLOCK(),@t1) & ' Hrs')
                END ! IF
                IF (p_web.GSV('job:turnaround_end_time') < CLOCK())
                    p_web.SSV('locJobTimeRemain',FORMAT(CLOCK() - p_web.GSV('job:turnaround_end_time'),@t1) & ' Hrs Over')
                END ! IF
                IF (p_web.GSV('job:turnaround_end_time') = CLOCK())
                    p_web.SSV('locJobTimeRemain','Due Now')
                END ! IF
            END ! IF
            
        END!  IF
    END ! IF
    
    
WorkOutIMEINumbers  PROCEDURE()
    CODE
        p_web.SSV('locExchangedIMEI','N/A')
        p_web.SSV('locExchangeMSN','N/A')
        p_web.SSV('locIncomingIMEI',p_web.GSV('job:ESN'))
        p_web.SSV('locIncomingMSN',p_web.GSV('job:MSN'))
        p_web.SSV('loc2ndExchangeIMEI','N/A')
        p_web.SSV('loc2ndMSN','N/A')
    
        SET(DEFAULTS,0)
        Access:DEFAULTS.Next()
    
        IF (p_web.GSV('job:Exchange_Unit_Number') > 0)
            Access:EXCHANGE.CleaRKey(xch:Ref_Number_Key)
            xch:Ref_Number = p_web.GSV('job:Exchange_Unit_Number')
            IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                p_web.SSV('locExchangedIMEI',xch:ESN)
                p_web.SSV('locExchangedMSN',xch:MSN)
            END ! IF
        END ! IF
    
        IF (p_web.GSV('jobe:SecondExchangeNumber') > 0)
            Access:EXCHANGE.CleaRKey(xch:Ref_Number_Key)
            xch:Ref_Number = p_web.GSV('jobe:SecondExchangeNumber')
            IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                p_web.SSV('loc2ndExchangeIMEI',xch:ESN)
                p_web.SSV('loc2ndMSN',xch:MSN)
            END ! IF
        END ! IF
    
        Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
        jot:RefNumber = p_web.GSV('job:Ref_Number')
        SET(jot:RefNumberKey,jot:RefNumberKey)
        LOOP UNTIL Access:JOBTHIRD.Next() <> Level:Benign
            IF (jot:RefNumber <> p_web.GSV('job:Ref_Number'))
                BREAK
            END ! IF
            IF (jot:OriginalIMEI <> '')
                p_web.SSV('locIncomingIMEI',jot:OriginalIMEI)
            END ! IF
            IF (jot:OriginalMSN <> '')
                p_web.SSV('locIncomingMSN',jot:OriginalMSN)
            END ! IF
            BREAK
        END ! LOOP
