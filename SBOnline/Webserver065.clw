

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER065.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Despatch:Exc         PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)

  CODE
    Do OpenFiles
    if (p_web.GSV('BookingSite') = 'RRC')
        IF (p_web.GSV('job:Who_Booked') = 'WEB')
            p_web.SSV('GetStatus:StatusNumber',468)
            ! Add To COnsignment History
            IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
                joc:RefNumber = p_web.GSV('job:Ref_Number')
                joc:TheDate = TODAY()
                joc:TheTime = CLOCK()
                joc:UserCode = p_web.GSV('BookingUserCode')
                joc:DespatchFrom = 'RRC'
                joc:DespatchTo = 'PUP'
                joc:Courier = p_web.GSV('job:Exchange_Courier')
                joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
                joc:DespatchType = 'EXC'
                IF (Access:JOBSCONS.TryInsert())
                    Access:JOBSCONS.CancelAutoInc()
                END
            END
        ELSE ! IF (p_web.GSV('job:Who_Booked') = 'WEB')
            p_web.SSV('GetStatus:StatusNumber',901)
            ! Add To COnsignment History
            IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
                joc:RefNumber = p_web.GSV('job:Ref_Number')
                joc:TheDate = TODAY()
                joc:TheTime = CLOCK()
                joc:UserCode = p_web.GSV('BookingUserCode')
                joc:DespatchFrom = 'RRC'
                joc:DespatchTo = 'CUSTOMER'
                joc:Courier = p_web.GSV('job:Exchange_Courier')
                joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
                joc:DespatchType = 'EXC'
                IF (Access:JOBSCONS.TryInsert())
                    Access:JOBSCONS.CancelAutoInc()
                END
            END
        END ! IF (p_web.GSV('job:Who_Booked') = 'WEB')
        p_web.SSV('GetStatus:Type','EXC')
        GetStatus(p_web.GSV('GetStatus:StatusNumber'),0,'EXC',p_web)
    
        p_web.SSV('wob:ReadyToDespatch',0)
        p_web.SSV('jobe:Despatched','YES')
    
    
        p_web.SSV('AddToAudit:Action','DESPATCH FROM RRC')
        p_web.SSV('AddToAudit:Notes','UNIT NO: ' & p_web.GSV('job:Exchange_Unit_Number') & |
            '<13,10>COURIER: ' & p_web.GSV('job:Loan_Courier') & |
            '<13,10>WAYBILL NO: ' & p_web.GSV('locWaybillNumber'))
    
    
        IF (p_web.GSV('jobe:VSACustomer') = 1)
            CID_XML(p_web.GSV('job:Mobile_Number'),p_web.GSV('wob:HeadAccountNumber'),2)
        END
    ELSE ! if (p_web.GSV('BookingSite') = 'RRC')
        p_web.SSV('job:Exchange_Consignment_Number',p_web.GSV('locWaybillNumber'))
        p_web.SSV('job:Exchange_Despatched',TOday())
        p_web.SSV('job:Despatched','')
        p_web.SSV('job:Exchange_Despatched_User',p_web.GSV('BookingUserCode'))
    
        if (p_web.GSV('jobe:WebJob') = 1)
            p_web.SSV('wob:ExcWaybillNumber',p_web.GSV('locWaybillNumber'))
    
            p_web.SSV('GetStatus:StatusNumber',Sub(GETINI('RRC','ExchangeStatusDespatchToRRC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3))
    
            ! Add To COnsignment History
            IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
                joc:RefNumber = p_web.GSV('job:Ref_Number')
                joc:TheDate = TODAY()
                joc:TheTime = CLOCK()
                joc:UserCode = p_web.GSV('BookingUserCode')
                joc:DespatchFrom = 'ARC'
                joc:DespatchTo = 'RRC'
                joc:Courier = p_web.GSV('job:Exchange_Courier')
                joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
                joc:DespatchType = 'EXC'
                IF (Access:JOBSCONS.TryInsert())
                    Access:JOBSCONS.CancelAutoInc()
                END
            END
        ELSE ! if (p_web.GSV('jobe:WebJob') = 1)
            p_web.SSV('GetStatus:StatusNumber',901)
            ! Add To COnsignment History
            IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
                joc:RefNumber = p_web.GSV('job:Ref_Number')
                joc:TheDate = TODAY()
                joc:TheTime = CLOCK()
                joc:UserCode = p_web.GSV('BookingUserCode')
                joc:DespatchFrom = 'ARC'
                joc:Courier = p_web.GSV('job:Exchange_Courier')
                joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
                joc:DespatchType = 'EXC'
                IF (Access:JOBSCONS.TryInsert())
                    Access:JOBSCONS.CancelAutoInc()
                END
            END
        END ! if (p_web.GSV('jobe:WebJob') = 1)
    
        p_web.SSV('AddToAudit:Action','EXCHANGE UNIT DESPATCH VIA ' & p_web.GSV('job:Exchange_Courier'))
        p_web.SSV('AddToAudit:Notes','CONSIGNMENT NOTE NUMBER: ' & p_web.GSV('locWaybillNumber'))
    
        Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
        xch:Ref_Number = p_web.GSV('job:Exchange_Unit_Number')
        if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
            xch:Available = 'DES'
            xch:StatusChangeDate = TODAY() ! #12127 Record status change. (Bryan: 13/07/2011)
            if (Access:EXCHANGE.TryUpdate() = Level:Benign)
    
                if (Access:EXCHHIST.PrimeRecord() = Level:Benign)
                    exh:Ref_Number = xch:Ref_Number
                    exh:Date = Today()
                    exh:Time = Clock()
                    exh:User = p_web.GSV('BookingUserCode')
                    exh:Status = 'UNIT DESPATCHED ON JOB: ' & p_web.GSV('job:ref_Number')
                    Access:EXCHHIST.TryInsert()
                END
            END
        END
    
    END ! if (p_web.GSV('BookingSite') = 'RRC')
    
    p_web.SSV('jobe:ExcSecurityPackNo',p_web.GSV('locSecurityPackID'))
    
    p_web.SSV('GetStatus:Type','EXC')
    GetStatus(p_web.GSV('GetStatus:StatusNumber'),0,'EXC',p_web)
    
    ! Add To Audit
    p_web.SSV('AddToAudit:Type','EXC')
    IF (p_web.GSV('locSecurityPackID') <> '')
        p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & |
            '<13,10>SECURITY PACK NO: ' & p_web.GSV('locSecurityPackID'))
    END
    AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'EXC',p_web.GSV('AddToAudit:Action'),p_web.GSV('AddToAudit:Notes'))
    DO CloseFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:EXCHHIST.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:EXCHHIST.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSCONS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSCONS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:EXCHANGE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:EXCHANGE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:EXCHHIST.Close
     Access:JOBSCONS.Close
     Access:EXCHANGE.Close
     FilesOpened = False
  END
