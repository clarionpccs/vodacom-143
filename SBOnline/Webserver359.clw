

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER359.INC'),ONCE        !Local module procedure declarations
                     END


RapEngUpdViewJob     PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locSerialNumber      STRING(30)                            !
locWarningText       STRING(255)                           !
FilesOpened     Long
WEBJOB::State  USHORT
JOBSE::State  USHORT
JOBS::State  USHORT
SBO_GenericFile::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('RapEngUpdViewJob')
  loc:formname = 'RapEngUpdViewJob_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('RapEngUpdViewJob','')
    p_web._DivHeader('RapEngUpdViewJob',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferRapEngUpdViewJob',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferRapEngUpdViewJob',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferRapEngUpdViewJob',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_RapEngUpdViewJob',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferRapEngUpdViewJob',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_RapEngUpdViewJob',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(SBO_GenericFile)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(SBO_GenericFile)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('RapEngUpdViewJob_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locWarningText',locWarningText)
  p_web.SetSessionValue('locSerialNumber',locSerialNumber)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locWarningText')
    locWarningText = p_web.GetValue('locWarningText')
    p_web.SetSessionValue('locWarningText',locWarningText)
  End
  if p_web.IfExistsValue('locSerialNumber')
    locSerialNumber = p_web.GetValue('locSerialNumber')
    p_web.SetSessionValue('locSerialNumber',locSerialNumber)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('RapEngUpdViewJob_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    p_web.SSV('locSerialNumber','')  
    locWarningText = ''
    
    IF (p_web.IfExistsValue('sbogen:RecordNumber'))
        p_web.StoreValue('sbogen:RecordNumber')
    END ! IF
    
    p_web.SSV('NextURL','')
    Access:SBO_GenericFile.ClearKey(sbogen:RecordNumberKey)
    sbogen:RecordNumber = p_web.GSV('sbogen:RecordNumber')
    IF (Access:SBO_GenericFile.TryFetch(sbogen:RecordNumberKey) = Level:Benign)
        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = sbogen:Long1
        IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
            IF (job:Date_Completed <> '')
                packet = CLIP(packet) & '<script>areYouSure("Warning! This job has been completed\n\nAre you sure you want to continue?","#","RapidEngineerUpdate");</script>'
                DO SendPacket
            END ! IF
            
            IF (job:Workshop <> 'YES')
                locWarningText = 'This job is not in the workshop'
            END ! IF
            IF job:Chargeable_Job = 'YES' And job:Estimate = 'YES' and (job:Estimate_Accepted <> 'YES' Or job:Estimate_Rejected <> 'YES')
                IF (locWarningText <> '')
                    locWarningText = CLIP(locWarningText) & '<<br/>This job requires an Estimate'
                ELSE
                    locWarningText = 'This job requies an Estimate'
                END ! IF
            END ! IF
            
            Access:WEBJOB.ClearKey(wob:RefNumberKey)
            wob:RefNumber = job:Ref_Number
            IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
            END
                
            
            p_web.SSV('NextURL','ViewJob?wob__RecordNumber=' & wob:RecordNumber & '&Change_btn=Change&ViewJobReturnURL=RapidEngineerUpdate')
            
            p_web.FileToSessionQueue(JOBS)
        ELSE
            packet = CLIP(packet) & '<script>alert("An error occurred finding the job. Try again!");window.open("RapidEngineerUpdate","_self");</script>'
            DO SendPacket
        END ! IF
    ELSE
        packet = CLIP(packet) & '<script>alert("An error occurred finding the job. Try again!");window.open("RapidEngineerUpdate","_self");</script>'
        DO SendPacket
    END ! IF
    
    IF (locWarningText <> '')
        p_web.SSV('locWarningText',locWarningText)
    END ! IF    
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locWarningText = p_web.RestoreValue('locWarningText')
 locSerialNumber = p_web.RestoreValue('locSerialNumber')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('NextURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('RapEngUpdViewJob_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('RapEngUpdViewJob_ChainTo')
    loc:formaction = p_web.GetSessionValue('RapEngUpdViewJob_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'RapidEngineerUpdate'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="RapEngUpdViewJob" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="RapEngUpdViewJob" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="RapEngUpdViewJob" ></input><13,10>'
  end

  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_RapEngUpdViewJob">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_RapEngUpdViewJob" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_RapEngUpdViewJob')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Insert Serial Number') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_RapEngUpdViewJob')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_RapEngUpdViewJob'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locSerialNumber')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_RapEngUpdViewJob')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Insert Serial Number') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_RapEngUpdViewJob_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Insert Serial Number')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Insert Serial Number')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Insert Serial Number')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Insert Serial Number')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
    If p_web.GSV('locWarningText') <> ''
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locWarningText
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locWarningText
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locWarningText
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locSerialNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locSerialNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locSerialNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locWarningText  Routine
  p_web._DivHeader('RapEngUpdViewJob_' & p_web._nocolon('locWarningText') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Note:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locWarningText  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locWarningText',p_web.GetValue('NewValue'))
    locWarningText = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locWarningText
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locWarningText',p_web.GetValue('Value'))
    locWarningText = p_web.GetValue('Value')
  End

Value::locWarningText  Routine
  p_web._DivHeader('RapEngUpdViewJob_' & p_web._nocolon('locWarningText') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locWarningText
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locWarningText'),1) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locWarningText  Routine
    loc:comment = ''
  p_web._DivHeader('RapEngUpdViewJob_' & p_web._nocolon('locWarningText') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locSerialNumber  Routine
  p_web._DivHeader('RapEngUpdViewJob_' & p_web._nocolon('locSerialNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Serial Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locSerialNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locSerialNumber',p_web.GetValue('NewValue'))
    locSerialNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locSerialNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locSerialNumber',p_web.GetValue('Value'))
    locSerialNumber = p_web.GetValue('Value')
  End
  If locSerialNumber = ''
    loc:Invalid = 'locSerialNumber'
    loc:alert = p_web.translate('Serial Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locSerialNumber = Upper(locSerialNumber)
    p_web.SetSessionValue('locSerialNumber',locSerialNumber)
    IF (p_web.GSV('locSerialNumber') <> p_web.GSV('job:ESN'))
        IF (p_web.GSV('job:Exchange_Unit_Number') > 0)
            Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
            xch:Ref_Number = p_web.GSV('job:Exchange_unit_Number')
            IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                IF (p_web.GSV('locSerialNumber') = xch:ESN)
                ELSE
                      ! Wrong serial number
                    loc:invalid = 'locSerialNumber'
                    loc:alert = 'The selected Serial Number does not match the selected Job'
                    p_web.SSV('locSerialNumber','')
                    !EXIT
                END ! IF
                  
            ELSE ! END
                  ! Wrong serial number
                loc:invalid = 'locSerialNumber'
                loc:alert = 'The selected Serial Number does not match the selected Job'
                p_web.SSV('locSerialNumber','')
                !EXIT
                  
            END ! IF
              
        ELSE !
              ! Wrong serial number
            loc:invalid = 'locSerialNumber'
            loc:alert = 'The selected Serial Number does not match the selected Job'
            p_web.SSV('locSerialNumber','')
  
            !EXIT
        END ! IF
          
    ELSE ! IF 
    END ! IF
  do Value::locSerialNumber
  do SendAlert

Value::locSerialNumber  Routine
  p_web._DivHeader('RapEngUpdViewJob_' & p_web._nocolon('locSerialNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locSerialNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locSerialNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locSerialNumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locSerialNumber'',''rapengupdviewjob_locserialnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locSerialNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locSerialNumber',p_web.GetSessionValueFormat('locSerialNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('RapEngUpdViewJob_' & p_web._nocolon('locSerialNumber') & '_value')

Comment::locSerialNumber  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('RapEngUpdViewJob_' & p_web._nocolon('locSerialNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('RapEngUpdViewJob_locSerialNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locSerialNumber
      else
        do Value::locSerialNumber
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('RapEngUpdViewJob_form:ready_',1)
  p_web.SetSessionValue('RapEngUpdViewJob_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_RapEngUpdViewJob',0)

PreCopy  Routine
  p_web.SetValue('RapEngUpdViewJob_form:ready_',1)
  p_web.SetSessionValue('RapEngUpdViewJob_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_RapEngUpdViewJob',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('RapEngUpdViewJob_form:ready_',1)
  p_web.SetSessionValue('RapEngUpdViewJob_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('RapEngUpdViewJob:Primed',0)

PreDelete       Routine
  p_web.SetValue('RapEngUpdViewJob_form:ready_',1)
  p_web.SetSessionValue('RapEngUpdViewJob_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('RapEngUpdViewJob:Primed',0)
  p_web.setsessionvalue('showtab_RapEngUpdViewJob',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('RapEngUpdViewJob_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('RapEngUpdViewJob_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
        If locSerialNumber = ''
          loc:Invalid = 'locSerialNumber'
          loc:alert = p_web.translate('Serial Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locSerialNumber = Upper(locSerialNumber)
          p_web.SetSessionValue('locSerialNumber',locSerialNumber)
        If loc:Invalid <> '' then exit.
  ! The following fields are not on the form, but need to be checked anyway.
  ! region Validate
    IF (p_web.GSV('job:Chargeable_Job') = 'YES' AND p_web.GSV('job:Estimate') = 'YES' AND | 
        (p_web.GSV('job:Estimate_Accepted') <> 'YES' AND p_web.GSV('job:Estimate_Rejected') <> 'YES'))
        loc:alert = 'Note: The selected job requires an Estimate'
    END ! IF
      
  
    IF (INLIST(SUB(p_web.GSV('job:Current_Status'),1,3),310,535,540,320,345,420))
        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = p_web.GSV('job:Ref_Number')
        IF (Access:JOBSE.TryFetch(jobe:RefNumberKey))
        END ! IF
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = jobe:RefNumber
        IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
        END !I F
          ! Update job to "In Repair" if set to above statuses
        p_web.FileToSessionQueue(WEBJOB)
        p_web.FileToSessionQueue(JOBSE)
    
        GetStatus(315,1,'JOB',p_web)
        p_web.SessionQueueToFile(JOBS)
        p_web.SessionQueueToFile(WEBJOB)
        p_web.SessionQueueToFile(JOBSE)
          
        Access:JOBS.TryUpdate()
        Access:WEBJOB.TryUpdate()
        Access:JOBSE.TryUpdate()
    END!  IF
  !endregion  
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('RapEngUpdViewJob:Primed',0)
  p_web.StoreValue('locWarningText')
  p_web.StoreValue('locSerialNumber')
