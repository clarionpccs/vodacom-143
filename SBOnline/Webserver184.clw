

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER184.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER183.INC'),ONCE        !Req'd for module callout resolution
                     END


BrowseStatusFilter   PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:StatusTypeFilter STRING(3)                             !Status Type Filter
tmp:JOB              STRING('JOB')                         !
tmp:EXC              STRING('EXC')                         !
tmp:2NE              STRING('2NE')                         !
tmp:LOA              STRING('LOA')                         !
FilesOpened     Long
AUDSTATS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('BrowseStatusFilter')
  loc:formname = 'BrowseStatusFilter_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('BrowseStatusFilter','None')
    p_web._DivHeader('BrowseStatusFilter',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferBrowseStatusFilter',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBrowseStatusFilter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBrowseStatusFilter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_BrowseStatusFilter',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBrowseStatusFilter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_BrowseStatusFilter',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(AUDSTATS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(AUDSTATS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SSV('tmp:StatusTypeFilter','JOB')
  p_web.site.CancelButton.TextValue = 'Back'
  p_web.site.CancelButton.image = '/images/listback.png'
  
  p_web.SetValue('BrowseStatusFilter_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('tmp:StatusTypeFilter',tmp:StatusTypeFilter)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:StatusTypeFilter')
    tmp:StatusTypeFilter = p_web.GetValue('tmp:StatusTypeFilter')
    p_web.SetSessionValue('tmp:StatusTypeFilter',tmp:StatusTypeFilter)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('BrowseStatusFilter_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 tmp:StatusTypeFilter = p_web.RestoreValue('tmp:StatusTypeFilter')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('BrowseStatusFilter_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('BrowseStatusFilter_ChainTo')
    loc:formaction = p_web.GetSessionValue('BrowseStatusFilter_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  ! "Back Hyperlink" at top of screen
  !packet = CLIP(packet) & '<<a href="' & loc:formactioncancel & '" target="_self" >Back<</a>'
  !do sendPacket
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="BrowseStatusFilter" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="BrowseStatusFilter" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="BrowseStatusFilter" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Browse Status Changes') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Browse Status Changes',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_BrowseStatusFilter">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_BrowseStatusFilter" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_BrowseStatusFilter')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Browse') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Browse Filter') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_BrowseStatusFilter')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_BrowseStatusFilter'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('BrowseStatusFilter_BrowseStatusChanges_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_BrowseStatusFilter')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Browse') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_BrowseStatusFilter_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Browse')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::link:AmendAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::link:AmendAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::link:AuditTrail
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::link:AuditTrail
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::link:EngineerHistory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::link:EngineerHistory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::link:LocationHistory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::link:LocationHistory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Browse Filter') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_BrowseStatusFilter_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Filter')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Filter')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Filter')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Filter')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:StatusTypeFilter
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:StatusTypeFilter
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:StatusTypeFilter
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_BrowseStatusFilter_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::browseStatusChanges
      do Comment::browseStatusChanges
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::link:AmendAddress  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('link:AmendAddress',p_web.GetValue('NewValue'))
    do Value::link:AmendAddress
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::link:AmendAddress  Routine
  p_web._DivHeader('BrowseStatusFilter_' & p_web._nocolon('link:AmendAddress') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.CreateHyperLink(p_web.Translate('Amend Address'),'AmendAddress','_self',,loc:javascript,,0) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::link:AmendAddress  Routine
    loc:comment = ''
  p_web._DivHeader('BrowseStatusFilter_' & p_web._nocolon('link:AmendAddress') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::link:AuditTrail  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('link:AuditTrail',p_web.GetValue('NewValue'))
    do Value::link:AuditTrail
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::link:AuditTrail  Routine
  p_web._DivHeader('BrowseStatusFilter_' & p_web._nocolon('link:AuditTrail') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.CreateHyperLink(p_web.Translate('Audit Trail'),'BrowseAuditFilter','_sefl',,loc:javascript,,0) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::link:AuditTrail  Routine
    loc:comment = ''
  p_web._DivHeader('BrowseStatusFilter_' & p_web._nocolon('link:AuditTrail') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::link:EngineerHistory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('link:EngineerHistory',p_web.GetValue('NewValue'))
    do Value::link:EngineerHistory
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::link:EngineerHistory  Routine
  p_web._DivHeader('BrowseStatusFilter_' & p_web._nocolon('link:EngineerHistory') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.CreateHyperLink(p_web.Translate('Engineer History'),'FormBrowseEngineerHistory','_self',,loc:javascript,,0) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::link:EngineerHistory  Routine
    loc:comment = ''
  p_web._DivHeader('BrowseStatusFilter_' & p_web._nocolon('link:EngineerHistory') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::link:LocationHistory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('link:LocationHistory',p_web.GetValue('NewValue'))
    do Value::link:LocationHistory
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::link:LocationHistory  Routine
  p_web._DivHeader('BrowseStatusFilter_' & p_web._nocolon('link:LocationHistory') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.CreateHyperLink(p_web.Translate('Location History'),'FormBrowseLocationHistory','_self',,loc:javascript,,0) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::link:LocationHistory  Routine
    loc:comment = ''
  p_web._DivHeader('BrowseStatusFilter_' & p_web._nocolon('link:LocationHistory') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:StatusTypeFilter  Routine
  p_web._DivHeader('BrowseStatusFilter_' & p_web._nocolon('tmp:StatusTypeFilter') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Select Status Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:StatusTypeFilter  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:StatusTypeFilter',p_web.GetValue('NewValue'))
    tmp:StatusTypeFilter = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:StatusTypeFilter
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:StatusTypeFilter',p_web.GetValue('Value'))
    tmp:StatusTypeFilter = p_web.GetValue('Value')
  End
  do Value::tmp:StatusTypeFilter
  do SendAlert
  do Value::browseStatusChanges  !1

Value::tmp:StatusTypeFilter  Routine
  p_web._DivHeader('BrowseStatusFilter_' & p_web._nocolon('tmp:StatusTypeFilter') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:StatusTypeFilter')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:StatusTypeFilter'',''browsestatusfilter_tmp:statustypefilter_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:StatusTypeFilter')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:StatusTypeFilter',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:StatusTypeFilter') = 0
    p_web.SetSessionValue('tmp:StatusTypeFilter','JOB')
  end
    packet = clip(packet) & p_web.CreateOption('Job','JOB',choose('JOB' = p_web.getsessionvalue('tmp:StatusTypeFilter')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('Exchange','EXC',choose('EXC' = p_web.getsessionvalue('tmp:StatusTypeFilter')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('Loan','LOA',choose('LOA' = p_web.getsessionvalue('tmp:StatusTypeFilter')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('2nd Exchange','2NE',choose('2NE' = p_web.getsessionvalue('tmp:StatusTypeFilter')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('All','',choose('' = p_web.getsessionvalue('tmp:StatusTypeFilter')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('BrowseStatusFilter_' & p_web._nocolon('tmp:StatusTypeFilter') & '_value')

Comment::tmp:StatusTypeFilter  Routine
    loc:comment = ''
  p_web._DivHeader('BrowseStatusFilter_' & p_web._nocolon('tmp:StatusTypeFilter') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::browseStatusChanges  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('browseStatusChanges',p_web.GetValue('NewValue'))
    do Value::browseStatusChanges
  Else
    p_web.StoreValue('aus:RecordNumber')
  End

Value::browseStatusChanges  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseStatusChanges --
  p_web.SetValue('BrowseStatusChanges:NoForm',1)
  p_web.SetValue('BrowseStatusChanges:FormName',loc:formname)
  p_web.SetValue('BrowseStatusChanges:parentIs','Form')
  p_web.SetValue('_parentProc','BrowseStatusFilter')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('BrowseStatusFilter_BrowseStatusChanges_embedded_div')&'"><!-- Net:BrowseStatusChanges --></div><13,10>'
    p_web._DivHeader('BrowseStatusFilter_' & lower('BrowseStatusChanges') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('BrowseStatusFilter_' & lower('BrowseStatusChanges') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseStatusChanges --><13,10>'
  end
  do SendPacket

Comment::browseStatusChanges  Routine
    loc:comment = ''
  p_web._DivHeader('BrowseStatusFilter_' & p_web._nocolon('browseStatusChanges') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('BrowseStatusFilter_tmp:StatusTypeFilter_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:StatusTypeFilter
      else
        do Value::tmp:StatusTypeFilter
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('BrowseStatusFilter_form:ready_',1)
  p_web.SetSessionValue('BrowseStatusFilter_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_BrowseStatusFilter',0)

PreCopy  Routine
  p_web.SetValue('BrowseStatusFilter_form:ready_',1)
  p_web.SetSessionValue('BrowseStatusFilter_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_BrowseStatusFilter',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('BrowseStatusFilter_form:ready_',1)
  p_web.SetSessionValue('BrowseStatusFilter_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('BrowseStatusFilter:Primed',0)

PreDelete       Routine
  p_web.SetValue('BrowseStatusFilter_form:ready_',1)
  p_web.SetSessionValue('BrowseStatusFilter_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('BrowseStatusFilter:Primed',0)
  p_web.setsessionvalue('showtab_BrowseStatusFilter',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('BrowseStatusFilter_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('BrowseStatusFilter_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 3
    loc:InvalidTab += 1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('BrowseStatusFilter:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:StatusTypeFilter')
  p_web.StoreValue('')
