

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER219.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
IsThisModelAlternative PROCEDURE  (func:OriginalModel,func:NewModel) ! Declare Procedure
ESNMODEL::State  USHORT
ESNMODAL::State  USHORT
FilesOpened     BYTE(0)

  CODE
    Do OpenFiles
    Do SaveFiles

    Return# = 0

    Access:ESNMODEL.ClearKey(esn:Model_Number_Key)
    esn:Model_Number = func:OriginalModel
    Set(esn:Model_Number_Key,esn:Model_Number_Key)
    Loop
        If Access:ESNMODEL.NEXT()
           Break
        End !If
        If esn:Model_Number <> func:OriginalModel      |
            Then Break.  ! End If
        !Now check the alternative models to see if the
        !scanned model matches -  (DBH: 29-10-2003)
        Access:ESNMODAL.ClearKey(esa:RefModelNumberKey)
        esa:RefNumber   = esn:Record_Number
        esa:ModelNumber = func:NewModel
        If Access:ESNMODAL.TryFetch(esa:RefModelNumberKey) = Level:Benign
            !Found
            Return# = 1
            Break
        Else !If Access:ESNMODAL.TryFetch(esa:RefModelNumberKey) = Level:Benign
            !Error
        End !If Access:ESNMODAL.TryFetch(esa:RefModelNumberKey) = Level:Benign

    End !Loop

    Do RestoreFiles
    Do CloseFiles
    Return Return#
SaveFiles  ROUTINE
  ESNMODEL::State = Access:ESNMODEL.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  ESNMODAL::State = Access:ESNMODAL.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF ESNMODEL::State <> 0
    Access:ESNMODEL.RestoreFile(ESNMODEL::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF ESNMODAL::State <> 0
    Access:ESNMODAL.RestoreFile(ESNMODAL::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:ESNMODEL.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:ESNMODEL.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:ESNMODAL.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:ESNMODAL.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:ESNMODEL.Close
     Access:ESNMODAL.Close
     FilesOpened = False
  END
