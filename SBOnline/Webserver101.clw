

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER101.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
jobInvoiced          PROCEDURE  (fRefNumber,fSite)         ! Declare Procedure
returnValue          BYTE                                  !
JOBS_ALIAS::State  USHORT
INVOICE::State  USHORT
FilesOpened     BYTE(0)

  CODE
    do openFiles
    do saveFiles

    returnValue = 0
    Access:JOBS_ALIAS.Clearkey(job_ali:Ref_Number_Key)
    job_ali:Ref_Number    = fRefNumber
    if (Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign)
        ! Found
        Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
        inv:Invoice_Number = job_ali:Invoice_Number
        if (Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign)
            ! Found
            if (fSite = 'RRC')
                if (inv:Invoice_Number > 0 and inv:RRCInvoiceDate > 0)
                    returnValue = 1
                end ! if (inv:Invoice_Number > 0 and inv:RRCInvoiceDate > 0)
            else ! if fSite = 'RRC'
                if (inv:Invoice_Number > 0 and inv:ARCInvoiceDate > 0)
                    returnValue = 1
                end ! if (inv:Invoice_Number > 0 and inv:ARCInvoiceDate > 0)
            end ! if fSite = 'RRC'
        else ! if (Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign)
            ! Error
        end ! if (Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign)
    else ! if (Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign)
        ! Error
    end ! if (Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign)

    do restoreFiles
    do closeFiles

    return returnValue
SaveFiles  ROUTINE
  JOBS_ALIAS::State = Access:JOBS_ALIAS.SaveFile()         ! Save File referenced in 'Other Files' so need to inform its FileManager
  INVOICE::State = Access:INVOICE.SaveFile()               ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF JOBS_ALIAS::State <> 0
    Access:JOBS_ALIAS.RestoreFile(JOBS_ALIAS::State)       ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF INVOICE::State <> 0
    Access:INVOICE.RestoreFile(INVOICE::State)             ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:JOBS_ALIAS.Open                                   ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS_ALIAS.UseFile                                ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:INVOICE.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:INVOICE.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:JOBS_ALIAS.Close
     Access:INVOICE.Close
     FilesOpened = False
  END
